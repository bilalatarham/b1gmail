<?php 
/*
 * b1gMail7
 * (c) 2002-2008 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: webdav_server.class.php,v 1.20 2013/03/18 16:02:30 patrick Exp $
 *
 */

/*
 * http://www.ietf.org/rfc/rfc2518.txt
 *
 * WebDAV test suite:
 * 	http://www.webdav.org/neon/litmus/
 *
 */

define('WEBDAV_DATE_FORMAT',		'Y-m-d\\TH:i:s\\Z');

/**
 * WebDAV response builder class
 *
 */
class WebDAV_ResponseBuilder
{
	var $_encoding;
	var $_param;
	var $_out;
	var $_uri;
	
	/**
	 * constructor
	 *
	 * @param array $param
	 * @param string $encoding
	 * @param string $uri
	 * @return WebDAV_ResponseBuilder
	 */
	function WebDAV_ResponseBuilder($param, $encoding, $uri)
	{
		$this->_param = $param;
		$this->_encoding = $encoding;
		$this->_uri = $uri;
	}
	
	/**
	 * generate response
	 *
	 * @param string $type
	 * @return string
	 */
	function Response($type)
	{
		$this->_out  = '<?xml version="1.0" encoding="'.$this->_encoding.'"?>' . "\r\n";
		
		if(in_array($type, array('propfind')))
			$this->_out .= '<D:multistatus xmlns:D="DAV:" xmlns:b="urn:uuid:c2f41010-65b3-11d1-a29f-00aa00c14882/">' . "\r\n";
		
		if($type == 'propfind')
		{
			foreach($this->_param as $param)
			{
				$this->_out .= '	<D:response>' . "\r\n";
				if(isset($param['_href']))
					$this->_out .= '		<D:href>' . $param['_href'] . '</D:href>' . "\r\n";
					
				foreach($param['propstat'] as $status=>$items)
				{
					if(count($items) > 0)
					{
						$this->_out .= '		<D:propstat>' . "\r\n";
						$this->_out .= '			<D:prop>' . "\r\n";
						foreach($items as $key=>$val)
						{
							if(substr($key, 0, 1) != '_')
							{
								$ns = '';
								if(strtoupper($key) == 'GETLASTMODIFIED'
									|| strtoupper($key) == 'LASTACCESSED')
									$ns = ' b:dt="dateTime.rfc1123"';
								else if(strtoupper($key) == 'CREATIONDATE')
									$ns = ' b:dt="dateTime.tz"';
								
								if(!is_array($val) && trim($val) == '')
								{
									$this->_out .= '				<D:'.XMLEncode($key).$ns.'/>' . "\r\n";		
								}
								else 
								{
									if(strtoupper($key) == 'RESOURCETYPE')
										$theVal = $val;
									else if(is_array($val))
										$theVal = Array2XML($val, '', false, true);
									else
										$theVal = XMLEncode($val);
									$this->_out .= '				<D:'.XMLEncode($key).$ns.'>' . $theVal . '</D:'.$key.'>' . "\r\n";
								}
							}
						}
						$this->_out .= '			</D:prop>' . "\r\n";
						$this->_out .= '			<D:status>' . $status . '</D:status>' . "\r\n";
						$this->_out .= '		</D:propstat>' . "\r\n";
					}
				}
				
				$this->_out .= '	</D:response>' . "\r\n";
			}
		}
		
		else if($type == 'lock')
		{
			$this->_out .= '<D:prop xmlns:D="DAV:">' . "\r\n";
			$this->_out .= '	<D:lockdiscovery>' . "\r\n";
			$this->_out .= '		<D:activelock>' . "\r\n";
			$this->_out .= '			<D:lockscope><D:' . $this->_param['scope'] . '/></D:lockscope>' . "\r\n";
			$this->_out .= '			<D:locktype><D:' . $this->_param['type'] . '/></D:locktype>' . "\r\n";
			$this->_out .= '			<D:depth>0</D:depth>' . "\r\n";
			$this->_out .= '			<D:owner>' . $this->_param['owner'] . '</D:owner>' . "\r\n";
			$this->_out .= '			<D:timeout>' . ($this->_param['expires'] == 0 ? 'inifinite' : 'Second-' . ($this->_param['expires']-time())) . '</D:timeout>' . "\r\n";
			$this->_out .= '			<D:locktoken><D:href>' . $this->_param['token'] . '</D:href></D:locktoken>' . "\r\n";
			$this->_out .= '		</D:activelock>' . "\r\n";
			$this->_out .= '	</D:lockdiscovery>' . "\r\n";
			$this->_out .= '</D:prop>' . "\r\n";
		}
		
		if(in_array($type, array('propfind')))
			$this->_out .= '</D:multistatus>';
		
		// debug?
		if(DEBUG)
		{
			if($fp = fopen(B1GMAIL_DIR . 'logs/webdav.log', 'a'))
			{
				fwrite($fp, sprintf("[%s] WebDAV response:\n%s\n\n\n",
					date('r'),
					$this->_out));
				fclose($fp);
			}
		}
		
		return($this->_out);	
	}
}

/**
 * WebDAV input parser class
 *
 */
class WebDAV_InputParser
{	
	var $_input;
	var $_array;
	var $_current;
	var $_p;
	var $error;
	
	/**
	 * constructor
	 *
	 * @param string $input
	 * @return WebDAV_InputParser
	 */
	function WebDAV_InputParser(&$input)
	{
		$this->_input = $input;
		$this->_array = array();
		$this->_p = array();
		$this->error = false;
	}
	
	/**
	 * expat startElement callback
	 *
	 * @param resource $parser
	 * @param string $name
	 * @param array $attrs
	 */
	function _startElement($parser, $name, $attrs)
	{
		if(($dPos = strpos($name, ':')) !== false)
			$name = substr($name, $dPos+1);
			
		$this->_p[] = &$this->_current;
		if(isset($this->_current[$name]))
			$name = $name . count($this->_current);
		
		$this->_current[$name] = array();
		$this->_current = &$this->_current[$name];
		
		if(count($attrs) > 0)
		{
			foreach($attrs as $key=>$val)
				if(substr($key, 0, 6) == 'xmlns:' && $val == '')
					$this->error = true;
			
			$this->_current['attrs'] = $attrs;
		}
	}
	
	/**
	 * expat endElement callback
	 *
	 * @param resource $parser
	 * @param string $name
	 */
	function _endElement($parser, $name)
	{
		$this->_current = &$this->_p[count($this->_p)-1];
		array_pop($this->_p);
	}
	
	/**
	 * expat characterData callback
	 *
	 * @param resource $parser
	 * @param string $data
	 */
	function _characterData($parser, $data)
	{
		if(trim($data) != '') $this->_current['data'] = $data;
	}
	
	/**
	 * parse the input
	 *
	 * @return array
	 */
	function Parse()
	{
		$this->_current = &$this->_array;
		
		$parser = xml_parser_create();
		xml_set_object($parser, $this);
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, false);
		xml_set_element_handler($parser, '_startElement', '_endElement');
		xml_set_character_data_handler($parser, '_characterData');
		if(xml_parse($parser, $this->_input, true) != 1)
			$this->error = true;
		xml_parser_free($parser);
		
		unset($this->_p);
		unset($this->_current);
		
		return($this->_array);
	}
}

/**
 * WebDAV server class
 *
 */
class WebDAV_Server
{
	var $_encoding;
	var $_self;
	var $_self_url;
	var $_self_uri;
	var $_crlf;
	var $_uri;
	var $_method;
	var $_input;
	var $_user;
	var $_pass;
	var $_parsedInput;
	var $_path;
	
	//
	// functions to be overridden
	//
	function CheckLogin()
	{
		return(false);
	}
	function CheckLock($path)
	{
		return(false);
	}
	function Handler_Get($ranges)
	{
		return(false);
	}
	function Handler_Lock($lockScope, $lockType, $lockOwner, $lockToken, $lockExpires, $update = false)
	{
		return(false);
	}
	function Handler_Unlock($lockToken)
	{
		return(false);
	}
	function Handler_Propfind($requested, $depth)
	{
		return(array());
	}
	function Handler_Proppatch()
	{
		return(array());
	}
	function Handler_MKCol($parent, $dir)
	{
		return(false);
	}
	function Handler_Delete($parent, $name, $isDir)
	{
		return(false);
	}
	function Handler_Put($fileSize, $contentType)
	{
		return(false);
	}
	function Handler_Copy($depth, $dest)
	{
		return(false);
	}
	function Handler_Move($depth, $dest)
	{
		return(false);
	}
	function Handler_Options(&$extraDAV, &$extraAllow)
	{
		$extraDAV = array();
		$extraAllow = array();
		return(false);
	}
	
	//
	// code
	//
			
	/**
	 * constructor
	 *
	 * @param string $self My filename
	 * @param string $selfurl My URL
	 * @return WebDAV_Server
	 */
	function WebDAV_Server($self, $selfurl)
	{
		global $currentCharset;
		
		// paths
		$this->_self = $self;
		if(substr($selfurl, -1) != '/')
		{
			$this->_self_url = $selfurl . '/interface/';
		}
		else
		{
			$this->_self_url = $selfurl . 'interface/';
		}
		
		// encoding
		$this->_encoding = $currentCharset;
		
		// line feed
		$this->_crlf = "\r\n";
		
		// URLs
		$this->_uri = $_SERVER['REQUEST_URI'];
		$this->_method = $_SERVER['REQUEST_METHOD'];
		$this->_self_uri = $this->_self_url . substr($this->_uri, strpos($this->_uri, $this->_self));
		
		// read HTTP input
		if($this->_method != 'PUT' && $this->_method != 'POST')
		{
			$fp = fopen('php://input', 'r');
			while(!feof($fp))
				$this->_input .= rtrim(fgets2($fp)) . "\r\n";
			fclose($fp);
		}
		
		// path
		$this->_path = urldecode(isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO'] != ''
			? $_SERVER['PATH_INFO']
			: '/');
		
		// check login!
		$this->_user = $this->_pass = '';
		if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
		{
			$this->_user = $_SERVER['PHP_AUTH_USER'];
			$this->_pass = $_SERVER['PHP_AUTH_PW'];
		}
		else if(isset($_SERVER['HTTP_AUTHORIZATION']) && substr(strtolower($_SERVER['HTTP_AUTHORIZATION'], 0, 5)) == 'basic')
		{
			list($this->_user, $this->_pass) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
		}
		
		if(!(($this->_method == 'OPTIONS' && $this->_path == '/') || $this->CheckLogin()))
		{
			Header('WWW-Authenticate: Basic realm="WebDAV"');
			$this->_headers('401 Unauthorized');
			exit();
		}
	}
	
	/**
	 * process client request
	 *
	 */
	function ProcessRequest()
	{
		// debug logging
		PutLog(sprintf('WebDAV request: <%s %s>',
			$this->_method,
			$this->_uri),
			PRIO_DEBUG,
			__FILE__,
			__LINE__);
			
		// parse input
		$this->_parsedInput = array();
		if(in_array($this->_method, array('PROPFIND', 'PROPPATCH', 'LOCK')))
		{
			if(trim($this->_input) != '')
			{
				$parser = _new('WebDAV_InputParser', array($this->_input));
				$this->_parsedInput = $parser->Parse();
				if($parser->error)
				{
					$this->_headers('400 Bad request');
					exit();
				}
			}
		}
		$reg = array();
		
		// debug?
		if(DEBUG)
		{
			if($fp = fopen(B1GMAIL_DIR . 'logs/webdav.log', 'a'))
			{
				fwrite($fp, sprintf("%s\n[%s] WebDAV request: %s %s\n%s\n%s\n\n",
					str_repeat('-', 75),
					date('r'),
					$this->_method,
					$this->_uri,
					print_r($_SERVER, true),
					$this->_input));
				fclose($fp);
			}
		}
		
		// what to do?
		switch($this->_method)
		{
		case 'OPTIONS':
			$this->Options();
			break;
			
		case 'PROPFIND':
			$this->Propfind();
			break;
			
		case 'PROPPATCH':
			$this->Proppatch();
			break;

		case 'HEAD':
		case 'GET':
			$this->Get();
			break;
			
		case 'PUT':
			$this->Put();
			break;
		
		case 'MKCOL':
			$this->MKCol();
			break;
			
		case 'DELETE':
			$this->Delete();
			break;
			
		case 'COPY':
			$this->Copy();
			break;
			
		case 'MOVE':
			$this->Move();
			break;
		
		case 'LOCK':
			$this->Lock();
			break;
			
		case 'UNLOCK':
			$this->Unlock();
			break;
		}
		
		$this->_headers('405 Method not allowed');
	}
	
	/**
	 * UNLOCK implementation
	 *
	 */
	function Unlock()
	{
		$token = substr(array_shift(ExtractMessageIDs($_SERVER['HTTP_LOCK_TOKEN'])), 1, -1);
		
		$this->Handler_Unlock($token);
		
		exit();
	}
	
	/**
	 * LOCK implementation
	 *
	 */
	function Lock()
	{
		$result = false;
		
        // timeout?
        $timeout = isset($_SERVER['HTTP_TIMEOUT'])
        	? $_SERVER['HTTP_TIMEOUT']
        	: 'Second-300';
        	
        // convert timeout
        if(strtolower(substr($timeout, 0, 7)) == 'second-')
        	$expires = time() + (int)substr($timeout, 7);
        else if(strtolower($timeout) == 'infinite')
        	$expires = 0;
        else 
        	$expires = time() + 300;
        
		if((!isset($_SERVER['CONTENT_LENGTH']) || $_SERVER['CONTENT_LENGTH'] == 0)
			&& (isset($_SERVER['HTTP_IF']) && trim($_SERVER['HTTP_IF']) != ''))
		{
			// already locked?
			if($this->_isLocked($this->_path))
			{
				$this->_headers('423 Resource is locked');
				exit();
			}
			
			// lock
			$result = $this->Handler_Lock('exclusive',
						'write',
						'',
						substr(array_shift(ExtractMessageIDs($_SERVER['HTTP_IF'])), 1, -1),
						$expires,
						true);
		}

		// use xml input
		else  
		{
			// check input
			if(!is_array($this->_parsedInput) || !isset($this->_parsedInput['lockinfo']))
			{
				$this->_headers('400 Bad request');
				exit();
			}
			
			// already locked?
			if($this->_isLocked($this->_path, isset($this->_parsedInput['lockinfo']['lockscope']['shared'])))
			{
				$this->_headers('423 Resource is locked');
				exit();
			}
			
			// owner?
			$lockOwner = 'unknown';
			$ownerArray = explode('<D:owner>', $this->_input);
			if(count($ownerArray) > 1)
				list($lockOwner) = explode('</D:owner>', $ownerArray[1]);
			
			// lock
			$result = $this->Handler_Lock(isset($this->_parsedInput['lockinfo']['lockscope']['shared']) ? 'shared' : 'exclusive',
					isset($this->_parsedInput['lockinfo']['locktype']['write']) ? 'write' : '?',
					$lockOwner,
					$this->_lockToken(),
					$expires,
					false);
		}
		
		if($result === false)
		{
			$this->_headers('409 Conflict');
		}
		else 
		{
            header('Lock-Token: <' . $result['token'] . '>');
			$this->_build_response($result, 'lock');
		}
		
		exit();
	}
	
	/**
	 * generate a random lock token
	 *
	 * @return string
	 */
	function _lockToken()
	{
		$key = GenerateRandomKey('webdiskLockToken');
		$token = sprintf('opaquelocktoken:%s-%s-%s-%s-%s',
			substr($key, 0, 8),
			substr($key, 8, 4),
			substr($key, 12, 4),
			substr($key, 16, 4),
			substr($key, 20, 12));
		return($token);
	}
	
	/**
	 * check if a resource is locked
	 *
	 * @param string $path Path
	 * @param bool $onlyExclusive Only exclusive locks?
	 * @return bool
	 */
	function _isLocked($path, $onlyExclusive = false)
	{
		$res = $this->CheckLock($path);
		
		if($res !== false && is_array($res))
			if(!isset($_SERVER['HTTP_IF']) || strpos($_SERVER['HTTP_IF'], $res['token']) === false)
			{
				if($res['scope'] != 'shared' || !$onlyExclusive)
				{
					PutLog(sprintf('Resource <%s> is locked (Token: %s; Scope: %s; If: %s)', 
						$path,
						$res['token'],
						$res['scope'],
						$_SERVER['HTTP_IF']),
						PRIO_DEBUG,
						__FILE__,
						__LINE__);
					return(true);
				}
			}
		return(false);
	}
	
	/**
	 * MOVE implementation
	 *
	 */
	function Move()
	{
		// locked?
		if($this->_isLocked($this->_path))
		{
			$this->_headers('423 Resource is locked');
			exit();
		}
		
		if(isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > 0)
		{
			$this->_headers('415 Unsupported media type');
			exit();
		}
		
		if(!isset($_SERVER['HTTP_DESTINATION']))
		{
			$this->_headers('400 Bad request');
			exit();
		}
		
        // depth?
        $depth = isset($_SERVER['HTTP_DEPTH'])
        	? $_SERVER['HTTP_DEPTH']
        	: 'infinity';
        
		// destination
		$dest = $_SERVER['HTTP_DESTINATION'];
		if(strpos($dest, '://') !== false && strpos(strtolower($dest), strtolower($_SERVER['HTTP_HOST'])) === false)
		{
			$this->_headers('502 Bad gateway');
			exit();
		}
		$dest = parse_url($dest);
		$dest = urldecode(substr($dest['path'], strlen($_SERVER['SCRIPT_NAME'])));
		
		// locked?
		if($this->_isLocked($dest))
		{
			$this->_headers('423 Resource is locked');
			exit();
		}
		
		if(!$this->Handler_Move($depth, $dest))
			$this->_headers('500 Internal server error');
		
		exit();
	}
	
	/**
	 * COPY implementation
	 *
	 */
	function Copy()
	{
		if(isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] > 0)
		{
			$this->_headers('415 Unsupported media type');
			exit();
		}
		
		if(!isset($_SERVER['HTTP_DESTINATION']))
		{
			$this->_headers('400 Bad request');
			exit();
		}
		
        // depth?
        $depth = isset($_SERVER['HTTP_DEPTH'])
        	? $_SERVER['HTTP_DEPTH']
        	: 'infinity';
        
		// destination
		$dest = $_SERVER['HTTP_DESTINATION'];
		if(strpos($dest, '://') !== false && strpos(strtolower($dest), strtolower($_SERVER['HTTP_HOST'])) === false)
		{
			$this->_headers('502 Bad gateway');
			exit();
		}
		$dest = parse_url($dest);
		$dest = urldecode(substr($dest['path'], strlen($_SERVER['SCRIPT_NAME'])));
		
		// locked?
		if($this->_isLocked($dest))
		{
			$this->_headers('423 Resource is locked');
			exit();
		}
		
		if(!$this->Handler_Copy($depth, $dest))
			$this->_headers('500 Internal server error');
		
		exit();
	}
	
	/**
	 * MKCOL implementation
	 *
	 */
	function MKCol()
	{
		if(isset($_SERVER['CONTENT_LENGTH'])
			&& $_SERVER['CONTENT_LENGTH'] != 0)
		{
			$this->_headers('415 Unsupported');
			exit();	
		}
		
		if(substr($this->_path, -1) == '/')
			$this->_path = substr($this->_path, 0, -1);
		
		$parent = dirname($this->_path);
		$dir = basename($this->_path);
		
		$this->Handler_MKCol($parent, $dir);
		exit();
	}
	
	/**
	 * DELETE implementation
	 *
	 */
	function Delete()
	{
		// locked?
		if($this->_isLocked($this->_path))
		{
			$this->_headers('423 Resource is locked');
			exit();
		}
		
        // depth?
        $depth = isset($_SERVER['HTTP_DEPTH'])
        	? $_SERVER['HTTP_DEPTH']
        	: 'infinity';
        if($depth != 'infinity')
        {
        	$this->_headers('400 Bad request');
        	exit();
        }
        
        $isDir = substr($this->_path, -1) == '/';
		if($isDir)
			$this->_path = substr($this->_path, 0, -1);
		
		$parent = dirname($this->_path);
		$name = basename($this->_path);
		
		$this->Handler_Delete($parent, $name, $isDir);
		exit();
	}
	
	/**
	 * OPTIONS implementation
	 *
	 */
	function Options()
	{
		$this->_headers('200 OK');
		
		$this->Handler_Options($extraDAV, $extraAllow);
		
		header('DAV: 1, 2'
		       . (count($extraDAV) ? ', ' . implode(', ', $extraDAV) : ''));
		header('Allow: OPTIONS, GET, HEAD, PUT, PROPFIND, PROPPATCH, MKCOL, DELETE, LOCK, UNLOCK'
		       . (count($extraAllow) ? ', ' . implode(', ', $extraAllow) : ''));
		exit();
	}
	
	/**
	 * PUT implementation
	 *
	 */
	function Put()
	{
		// locked?
		if($this->_isLocked($this->_path))
		{
			$this->_headers('423 Resource is locked');
			exit();
		}
		
		// get content type, reject multipart stuff as we do not support it
		if(isset($_SERVER['CONTENT_TYPE']) && strpos($_SERVER['contenttype'], 'multipart/') !== false)
		{
			$this->_headers('501 Not implemented');
			exit();
		}
		else if(isset($_SERVER['CONTENT_TYPE']))
			$contentType = $_SERVER['CONTENT_TYPE'];
		else 
			$contentType = 'application/octet-stream';
		
		// reject content-headers
		foreach($_SERVER as $key=>$val)
		{
			if(strtolower(substr($key, 0, 12)) == 'HTTP_CONTENT_'
				&& !in_array($key, array('HTTP_CONTENT_LANGUAGE', 'HTTP_CONTENT_LOCATION')))
			{
				$this->_headers('501 Not implemented');
				exit();
			}
		}
		
		// get content length
		$fileSize = isset($_SERVER['CONTENT_LENGTH'])
			? $_SERVER['CONTENT_LENGTH']
			: (isset($_SERVER['HTTP_X_EXPECTED_ENTITY_LENGTH']) 
				? $_SERVER['HTTP_X_EXPECTED_ENTITY_LENGTH']
				: 0);
			
		// do it!
		if(!$this->Handler_Put($fileSize, $contentType))
			$this->_headers('403 Forbidden');
		
		exit();
	}
	
	/**
	 * GET implementation
	 *
	 */
	function Get()
	{
		$ranges = array();
		if(!$this->Handler_Get($ranges))
			$this->_headers('404 Not found');
		exit();
	}
	
	/**
	 * PROPPATCH implementation
	 *
	 */
	function Proppatch()
	{
		$this->_headers('501 Not yet implemented');
		exit();
	}
	
	/**
	 * PROPFIND implementation
	 *
	 */
	function Propfind()
	{
	// depth?
        $depth = isset($_SERVER['HTTP_DEPTH'])
        	? $_SERVER['HTTP_DEPTH']
        	: 'infinity';
        
        // requested props?
		if(isset($this->_parsedInput['propfind'])
			&& isset($this->_parsedInput['propfind']['prop']))
		{
			$requested = array();
			foreach($this->_parsedInput['propfind']['prop'] as $key => $val)
				$requested[] = $key;
		}
		else 
		{
			$requested = array('creationdate', 'getcontentlength', 'getlastmodified', 'resourcetype', 'lastaccessed',
								'getcontenttype');
		}
			
		// propfind!
		$result = $this->Handler_Propfind($requested, $depth);
		
		// output
		if($result !== false)
		{
			$resultArray = array();
			
			// prepare result array
			foreach($result as $href=>$item)
			{
				$ok = $notFound = array();
				
				foreach($requested as $rProp)
					if(!isset($item[$rProp]))
						$notFound[$rProp] = '';
					else 
						$ok[$rProp] = $item[$rProp];
										
				$resultArray[] = array(
					'_href'		=> $href,
					'propstat' 	=> array(
						'HTTP/1.1 200 OK'			=> $ok,
						'HTTP/1.1 404 Not found'	=> $notFound
					)
				);
			}
			
			$this->_build_response($resultArray, 'propfind');
		}
		else 
		{
			$lock = $this->CheckLock($this->_path);
			if($lock !== false && is_array($lock))
			{
				$resultArray = array();
				$ok = $notFound = array();
				$item = array(
					'displayname'		=> $this->_path,
					'creationdate'		=> $lock['created'],
					'getlastmodified'	=> $lock['modified'],
					'getcontenttype'	=> '',
					'getcontentlength'	=> 0,
					'resourcetype'		=> ''
				);
				
				foreach($requested as $rProp)
					if(!isset($item[$rProp]))
						$notFound[$rProp] = '';
					else 
						$ok[$rProp] = $item[$rProp];
				
				$resultArray[] = array(
					'_href'		=> $_SERVER['SCRIPT_NAME'] . $this->_path,
					'propstat' 	=> array(
						'HTTP/1.1 200 OK'			=> $ok,
						'HTTP/1.1 404 Not found'	=> $notFound
					)
				);
			}
			else
				$this->_headers('404 Not found');
		}
		
		exit();
	}
		
	/**
	 * build response
	 *
	 * @param array $param
	 * @param string $type
	 * @param string $status
	 */
	function _build_response($param, $type, $status = '207 Multi-status')
	{
		$this->_headers($status, true);
		$builder = _new('WebDAV_ResponseBuilder', array($param, $this->_encoding, $this->_self_uri));
		$response = $builder->Response($type);
		echo($response);
		exit();
	}
	
	/**
	 * send http headers
	 *
	 * @param string $status Status
	 * @param bool $xml Send text/xml Content-Type?
	 */
	function _headers($status = '200 OK', $xml = false)
	{
		global $currentCharset;
		
		header('Server: b1gMail/' . B1GMAIL_VERSION);
        header('MS-Author-Via: DAV');
		header('X-WebDAV-Status: ' . $status);
		if($xml)
			header('Content-Type: text/xml; charset='.$currentCharset);
		header('Pragma: no-cache');
		header('Cache-Control: no-cache');
		header('Connection: close');
		header('HTTP/1.1 ' . $status);
	}
}
?>