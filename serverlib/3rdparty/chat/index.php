<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license GNU Affero General Public License
 * @link https://blueimp.net/ajax/
 */

include ("_db.inc.php");

if(isset($_GET['chatkey']))
{
	$chatkey = $_GET['chatkey'];
	$user = $_GET['userName'] . "@aikq.de";
	$url = $_SERVER['HTTP_HOST'];
	$arr = parse_url($_SERVER['HTTP_REFERER']);
	$sid = substr($arr['query'], -32);
	// cookie erzeugen
	setcookie('chat_token', $chatkey . '#' . $user . '#' . $sid . '#' . $url, time()+1800);
}
else if(isset($_COOKIE['chat_token']))
{
	// erneuert das cookie bei jedem request
	setcookie('chat_token', $_COOKIE['chat_token'], time()+1800);
	// splittet die 3 variablen anhand des trennzeichens # aus dem cookie und weist den variablen die werte aus dem cookie zu
	list($chatkey, $user, $sid, $url) = explode('#', $_COOKIE['chat_token']);
}
else
{
	$chatkey = '';
	$user = '';
	$sid = '';
	$url = '';
}

@mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS) or die("Keine Verbindung zur Datenbank. Fehlermeldung: " . mysql_error());
mysql_select_db(MYSQL_DATABASE) or die("Konnte Datenbank nicht benutzen, Fehlermeldung: " . mysql_error());

$sql = "SELECT chatkey, email FROM bm60_users WHERE MD5(chatkey) = '" . mysql_escape_string($chatkey) . "'";
$result = mysql_query($sql) or die(mysql_error());

while ($row = mysql_fetch_assoc($result)) 
{
	if(($chatkey == md5($row['chatkey'])) && ($user == $row['email']))
	{
		// Path to the chat directory:
		define('AJAX_CHAT_PATH', dirname($_SERVER['SCRIPT_FILENAME']).'/');

		// Include custom libraries and initialization code:
		require(AJAX_CHAT_PATH.'lib/custom.php');

		// Include Class libraries:
		require(AJAX_CHAT_PATH.'lib/classes.php');

		// Initialize the chat:
		$ajaxChat = new CustomAJAXChat();

		exit();
	}
}

header('Location: http://www.google.de/'); 

?>