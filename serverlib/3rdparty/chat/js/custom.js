/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license GNU Affero General Public License
 * @link https://blueimp.net/ajax/
 */

// Overriding client side functionality:

/*
// Example - Overriding the replaceCustomCommands method:
ajaxChat.replaceCustomCommands = function(text, textParts) {
	return text;
}
 */

// Return replaced text for custom commands
// text contains the whole message, textParts the message split up as words array
ajaxChat.replaceCustomCommands = function(text, textParts) {
   switch(textParts[0]) {
      case '/myip':
         return '<span class="chatBotMessage">' + this.lang['myip'].replace(/%s/, textParts[1]) + '</span>';
      default:
         return text;
   }
}

ajaxChat.customInitialize = function(text) {
	ajaxChat.addChatBotMessageToChatList('Willkommen im Smart-Mail.de - Chat!');
		return text;
}