<?php 
/*
 * b1gMail7
 * (c) 2002-2012 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: bmstoreclient.class.php,v 1.1 2012/09/23 17:53:48 patrick Exp $
 *
 */

define('BMSTORE_ASSOC',				1);
define('BMSTORE_NUM',				2);
define('BMSTORE_BOTH',				3);

define('BMSTORE_TYPE_NULL',			0);
define('BMSTORE_TYPE_EOR',			1);
define('BMSTORE_TYPE_EOD',			2);
define('BMSTORE_TYPE_INT',			10);
define('BMSTORE_TYPE_DOUBLE',		11);
define('BMSTORE_TYPE_TEXT',			12);
define('BMSTORE_TYPE_BLOB',			13);

class BMStoreClient_Result
{
	var $cols;
	var $rows;
	var $pos;
	
	function BMStoreClient_Result($fp)
	{
		$this->pos 		= 0;
		$this->rows 	= array();
		$this->cols 	= array();
		
		$row = array();
		
		while(!feof($fp))
		{
			$type = ord(fgetc($fp));
			
			if($type == BMSTORE_TYPE_EOD || $type == BMSTORE_TYPE_EOR)
			{
				if(count($row) > 0)
				{
					if(count($this->cols) == 0)
						$this->cols = $row;
					else
						$this->rows[] = $row;
				}
				
				$row = array();
			}
			
			if($type == BMSTORE_TYPE_EOD)
			{
				break;
			}
			
			else if($type == BMSTORE_TYPE_EOR)
			{
				continue;
			}
			
			else if($type == BMSTORE_TYPE_NULL)
			{
				$row[] = NULL;
			}
				
			else if($type == BMSTORE_TYPE_INT)
			{
				$raw = fread($fp, 4);
				$row[] = (int)implode('', unpack('l', $raw));
			}
				
			else if($type == BMSTORE_TYPE_DOUBLE)
			{
				$raw = fread($fp, 8);
				$row[] = (double)implode('', unpack('d', $raw));
			}
			
			else if($type == BMSTORE_TYPE_TEXT || $type == BMSTORE_TYPE_BLOB)
			{
				$raw = fread($fp, 4);
				$length = (int)implode('', unpack('V', $raw));
				
				if($length == 0)
					$row[] = '';
				else
				{
					$data = '';
					
					$readBytes = 0;
					while($readBytes < $length && !feof($fp))
					{
						$chunk = fread($fp, $length-$readBytes);
						$data .= $chunk;
						$readBytes += strlen($chunk);
					}
					
					$row[] = $data;
				}
			}
			
			else
			{
				trigger_error('b1gMailStore server returned unknown field type ' . $type, E_USER_ERROR);
			}
		}
	}
	
	function RowCount()
	{
		return(count($this->rows));
	}
	
	function FieldCount()
	{
		return(count($this->cols));
	}
	
	function FetchArray($resultType = BMSTORE_BOTH)
	{
		if($this->pos >= count($this->rows))
			return(false);
		
		$result = array();
		if($resultType == BMSTORE_NUM || $resultType == BMSTORE_BOTH)
		{
			$result = array_merge($result, $this->rows[$this->pos]);
		}
		if($resultType == BMSTORE_ASSOC || $resultType == BMSTORE_BOTH)
		{
			foreach($this->rows[$this->pos] as $key=>$val)
				$result[$this->cols[$key]] = $val;
		}
		
		$this->pos++;
		
		return($result);
	}
	
	function FieldName($index)
	{
		return($this->cols[$index]);
	}
	
	function Free()
	{
		
	}
}

class BMStoreClient
{
	var $host;
	var $port;
	var $authToken;
	var $fp;
	var $lastError;
	var $lastResult;
	
	function BMStoreClient($host, $port, $authToken)
	{
		$this->host 		= $host;
		$this->port			= $port;
		$this->authToken	= $authToken;
		$this->fp			= false;
		$this->lastError	= false;
		$this->lastResult	= false;
	}
	
	function LastError()
	{
		return($this->lastError);
	}
	
	function Connect()
	{
		if($this->fp !== false)
			return(true);
		
		$this->fp = @fsockopen($this->host, $this->port);
		
		if(!is_resource($this->fp))
		{
			$this->fp = false;
			$this->lastError = 'Failed to connect to b1gMailStore server';
			trigger_error('Failed to connect to b1gMailStore server', E_USER_WARNING);
			return(false);
		}
		
		$line = fgets($this->fp);
		if(substr($line, 0, 3) != '200')
		{
			$this->lastError = 'b1gMailStore server not ready';
			$this->Disconnect();
			trigger_error('Failed to connect to b1gMailStore server: server not ready', E_USER_WARNING);
			return(false);
		}
		
		fprintf($this->fp, "AUTH %s\n", $this->authToken);
		$line = fgets($this->fp);
		if(substr($line, 0, 3) != '200')
		{
			$this->lastError = substr($line, 4);
			$this->Disconnect();
			trigger_error('Failed to connect to b1gMailStore server: authentication failed', E_USER_WARNING);
			return(false);
		}
		
		return(true);
	}
	
	function Disconnect()
	{
		if($this->fp === false)
			return;
		
		fwrite($this->fp, "QUIT\n");
		fgets($this->fp);
		fclose($this->fp);
		
		$this->fp = false;
	}
	
	function UserExists($userID)
	{
		fprintf($this->fp, "USER_EXISTS %d\n", $userID);
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return(true);
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
	}
	
	function UserCreate($userID)
	{
		fprintf($this->fp, "USER_CREATE %d\n", $userID);
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return(true);
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
	}

	function DBOpen($userID, $type)
	{
		fprintf($this->fp, "DB_OPEN %d %d\n", $userID, $type);
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return(true);
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
	}
	
	function DBClose()
	{
		fwrite($this->fp, "DB_CLOSE\n");
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return(true);
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
	}
	
	function DBStructVer()
	{
		fwrite($this->fp, "DB_STRUCTVER\n");
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return((int)substr($line, 4));
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
		
		return(0);
	}
	
	function InsertId()
	{
		fwrite($this->fp, "DB_INSERTID\n");
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return((int)substr($line, 4));
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
		
		return(0);
	}
	
	function AffectedRows()
	{
		fwrite($this->fp, "DB_AFFECTEDROWS\n");
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return((int)substr($line, 4));
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
		
		return(0);
	}
	
	function StatsCounterLastMinute($counterID)
	{
		fprintf($this->fp, "STATS_COUNTER_LASTMINUTE %d\n", $counterID);
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return((int)substr($line, 4));
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
		
		return(0);
	}
	
	function StatsUserCount()
	{
		fprintf($this->fp, "STATS_USER_COUNT\n");
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			return((int)substr($line, 4));
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
		
		return(0);
	}
	
	function StatsDiskUsage()
	{
		fwrite($this->fp, "STATS_DISKUSAGE\n");
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			$data = explode(' ', trim(substr($line, 4)));
			return(array($data[0], $data[1], $data[2]));
		}
		else
		{
			$this->lastError = substr($line, 4);
			return(false);
		}
		
		return(0);
	}
	
	function Query($query)
	{
		$queryData = pack('V', strlen($query)) . $query;
		
		if(func_num_args() > 1)
		{
			$args = func_get_args();
			
			for($i=1; $i<func_num_args(); $i++)
			{
				$arg = $args[$i];
				
				if(is_int($arg))
				{
					$queryData .= pack('Cl', BMSTORE_TYPE_INT, $arg);
				}
				else if(is_double($arg) || is_float($arg))
				{
					$queryData .= pack('Cd', BMSTORE_TYPE_DOUBLE, $arg);
				}
				else
				{
					$queryData .= pack('CV', BMSTORE_TYPE_TEXT, strlen($arg)) . $arg;
				}
			}
		}
		
		fprintf($this->fp, "DB_QUERY %d\n", strlen($queryData));
		$line = fgets($this->fp);
		
		if(substr($line, 0, 3) == '200')
		{
			fwrite($this->fp, $queryData);
			$line = fgets($this->fp);
			
			if(substr($line, 0, 3) == '200')
			{
				return(new BMStoreClient_Result($this->fp));
			}
			else
			{
				$this->lastError = substr($line, 4);
			}
		}
		else
		{
			$this->lastError = substr($line, 4);
		}
		
		if(DEBUG)
		{
			// debug mode -> error page!
			DisplayError(0x101, 'Storage node SQL error', 'Failed to execute query on storage node.',
								sprintf("Process:\n%s\n\nQuery:\n%s\n\nError description:\n%s",
								'Query',
								$query,
								$this->lastError),
						__FILE__,
						__LINE__);
			die();
		}
		
		PutLog("Storage node SQL error at '" . $_SERVER['SCRIPT_NAME'] . "': '" . $this->lastError . "', tried to execute '" . $query . "'", PRIO_ERROR, __FILE__, __LINE__);
		
		return(false);
	}
}
?>