<?php 
class b1gMailModul
{
	// Vom Modul bereitgestellt
	var $titel;
	var $autor;
	var $web;
	var $mail;
	var $version;
	var $designedfor;
	var $user_pages = false;
	var $user_page_array = array();
	var $admin_pages = false;
	var $admin_page_title = "";
	var $internal_name = "";
	var $features = array();
	var $installed = false;
	var $_groupOptions = array();
	
	/**
	 * Modul-Konstruktor, Modulinfovariablen setzen
	 *
	 * @return b1gMailModul
	 */
	function b1gMailModul()
	{
		global $b1gmail_version;
		
		$this->titel		= 'Basismodul';
		$this->autor		= 'B1G Software';
		$this->web			= 'http://www.b1g.de/';
		$this->mail			= 'info@b1g.de';
		$this->version		= $b1gmail_version;
		$this->designedfor	= $b1gmail_version;
		$this->user_pages 	= false;
		$this->user_page_array = array();
		$this->admin_pages	= false;
		$this->admin_page_title = "";
	}
	
	/**
	 * Gruppenoption registrieren
	 * 
	 */
	function RegisterGroupOption($key, $typ = FIELD_TEXT, $desc, $options = '', $default = '')
	{
		$this->_groupOptions[$key] = array(
			'typ'		=> $typ,
			'options'	=> $options,
			'desc'		=> $desc,
			'default'	=> $default
		);
	}
	
	/**
	 * Gruppenoption auslesen
	 * 
	 */
	function GetGroupOptionValue($key, $group=0)
	{
		global $s_group;
		return(getGroupOption($group == 0 ? $s_group : $group,
			$this->internal_name,
			$key,
			$this->_groupOptions[$key]['default']));
	}
	
	/**
	 * Gibt an, ob das Modul das Features $feature
	 * als Unique Feature unterstuetzt (intern genutzt)
	 *
	 */
	function Features($feature)
	{
		return(in_array($feature, $this->features));
	}
	
	/**
	 * Installiert das Modul
	 *
	 */
	function Install()
	{
		return(true);
	}
	
	/**
	 * Deinstalliert das Modul
	 *
	 */
	function Uninstall()
	{
		return(true);
	}
	
	/**
	 * Generiert einen Data-Dateinamen
	 *
	 */
	function DataFilename($id, $ext)
	{
		return(false);
	}
	
	/**
	 * Wird nach functions.inc.php-Ausfuehrung aufgerufen
	 *
	 */
	function AfterFunctions()
	{
		return(false);
	}
	
	/**
	 * Gibt eine MySQL-Verbindung zum Ausfuehren von $query aus
	 *
	 */
	function MySQLHandle($query, $default, $user)
	{
		return($default);
	}
	
	/**
	 * Wird bei Aufruf im Adminbereich aufgerufen
	 *
	 */
	function AdminHandler()
	{
		return(true);
	}
	
	/**
	 * Wird nach Speicherung einer Mail aufgerufen
	 *
	 * @param integer $id
	 * @param object $mail
	 * @param object $user
	 * @param object $stream
	 */
	function AfterPutMail($id, &$mail, &$user, &$stream)
	{
		return(true);
	}
	
	/**
	 * Sollten voraussichtlich mehrere Aufrufe von OnGetMail folgen, wird vorher diese Funktion
	 * aufgerufen
	 * 
	 * @param array $user User-Row
	 * @param bool $draftList True, wenn Entwurf- statt Mail-Liste
	 */
	function OnStartMailList($user, $draftList = false)
	{
		return(true);
	}
	
	/**
	 * Wird nach dem letzten OnGetMail einer Liste aufgerufen
	 * 
	 * @param array $user User-Row
	 * @param bool $draftList True, wenn Entwurf- statt Mail-Liste
	 */
	function OnEndMailList($user, $draftList = false)
	{
		return(true);
	}
	
	/**
	 * Wird bei jedem Abruf eines Entwurfes aufgerufen (auch wenn nur teilweise abgerufen)
	 * 
	 * @param integer $id
	 * @param array $user User-Row
	 */
	function OnGetDraft($id, $user)
	{
		return(true);
	}
	
	/**
	 * Wird nach dem Speichern eines Entwurfes aufgerufen
	 * 
	 * @param integer $id
	 * @param array $user User-Row
	 */
	function OnSaveDraft($id, $user)
	{
		return(true);
	}
	
	/**
	 * Wird bei jedem Abruf einer Mail aufgerufen (auch wenn nur teilweise abgerufen)
	 * 
	 * @param integer $id
	 * @param array $user User-Row
	 */
	function OnGetMail($id, $user)
	{
		return(true);
	}
	
	/**
	 * Wird nach jedem Senden einer Mail von einem User aufgerufen
	 * 
	 * @param integer $id
	 * @param array $user User-Row
	 * @param array $mailinfo
	 * @param resource $fp
	 */
	function AfterSendMail($id, $user, $mailinfo, $fp)
	{
		return(true);
	}
	
	/**
	 * Wird nach Registrierung aufgerufen
	 *
	 * @param integer $userid
	 * @param string $usermail
	 */
	function OnSignup($userid, $usermail)
	{
		return(true);
	}
	
	/**
	 * Wird bei Erstellung eines Smarty-Objektes aufgerufen
	 *
	 * @param Smarty $tpl
	 */
	function OnCreateTemplate(&$tpl)
	{
		return(true);
	}
	
	/**
	 * Wird beim Einlesen der Sprachdatei aufgerufen
	 *
	 * @param array $lang_main
	 * @param array $lang_admin
	 * @param string $lang
	 */
	function OnReadLang(&$lang_main, &$lang_admin, $lang)
	{
		return(true);
	}
	
	/**
	 * Wird bei Aufruf einer Hauptdatei aufgerufen
	 *
	 * @param string $file
	 * @param string $action
	 */
	function FileHandler($file, $action)
	{
		return(true);
	}
	
	/**
	 * Wird beim Versenden aufgerufen
	 *
	 * @param string $mail
	 * @param bool $html
	 * @return unknown
	 */
	function OnSend(&$mail, $html)
	{
		return(true);
	}
	
	/**
	 * Wird beim Versenden einer SMS aufgerufen
	 *
	 * @param string $text
	 * @param int $typ
	 * @param string $from
	 * @param string $to
	 */
	function OnSendSMS(&$text, $typ, $from, $to)
	{
		return(true);
	}
	
	/**
	 * Wird bei erfolgreichem Login aufgerufen
	 *
	 * @param integer $user_id
	 */
	function OnLogin($user_id)
	{
		return(true);
	}
	
	/**
	 * Wird bei fehlgeschlagenem Login aufgerufen
	 *
	 * @param string $usermail
	 * @param string $passwort
	 * @param integer $reason
	 */
	function OnLoginFailed($usermail, $passwort, $reason)
	{
		return(true);
	}
	
	/**
	 * Wird bei Logout aufgerufen
	 *
	 * @param integer $user_id
	 */
	function OnLogout($user_id)
	{
		return(true);
	}
	
	/**
	 * Wird aufgerufen, wenn eine E-Mail ohne korrekten Empfaenger eintrifft
	 * NEUE VERSION - wird ab b1gMail 6.3 PL10 unterstuetzt!
	 * Wenn false zurueckgegeben wird, erfolgt KEIN Versand einer Bounce-Mail!
	 *
	 * @param object $mail
	 * @param object $user
	 * @return bool
	 */
	function OnMailWithoutValidRecipient(&$m, &$s)
	{
		return(true);
	}
	
	/**
	 * Wird bei Verarbeitung einer eingehenden E-Mail aufgerufen
	 * VERALTERT - wird ab b1gMail 6.3 nicht mehr unterstuetzt (siehe OnReceive2)!
	 *
	 * @param array $mail
	 * @param string $user
	 * @return integer
	 */
	function OnReceive(&$mail, $user)
	{
		/*
		 * Rueckgabe:
		 *		BM_OK				=> Mail normal weiterverarbeiten
		 *		BM_BLOCK			=> Mail blocken
		 *		BM_DELETE			=> Mail loeschen
		 *		BM_IS_INFECTED		=> Mail ist mit Virus infiziert
		 *		BM_IS_SPAM			=> Mail ist Spam
		 */	
		return(BM_OK);
	}
	
	/**
	 * Wird bei Verarbeitung einer eingehenden E-Mail aufgerufen
	 * NEUE VERSION - wird ab b1gMail 6.3 unterstuetzt!
	 *
	 * @param object $mail
	 * @param object $user
	 * @param object $s
	 * @return integer
	 */
	function OnReceive2(&$mail, &$user, &$s)
	{
		/*
		 * Rueckgabe:
		 *		BM_OK				=> Mail normal weiterverarbeiten
		 *		BM_BLOCK			=> Mail blocken
		 *		BM_DELETE			=> Mail loeschen
		 *		BM_IS_INFECTED		=> Mail ist mit Virus infiziert
		 *		BM_IS_SPAM			=> Mail ist Spam
		 */	
		return(BM_OK);
	}
	
	/**
	 * Payment Handler - verarbeitet ein Payment
	 *
	 * @param array $row
	 */
	function HandlePayment($row)
	{
		return(false);
	}
	
	/**
	 * Destruktor - wird aufgerufen, wenn b1gMail beendet wird
	 *
	 */
	function Destructor()
	{
		return(false);
	}
	
	/**
	 * Session schliessen / loeschen
	 * 
	 */
	function SessionKill($sid)
	{
		return(false);
	}
	
	/**
	 * Session-Liste holen
	 * 
	 */
	function SessionList()
	{
		return(array());
	}
	
	/**
	 * Sessions aelter als $time killen
	 * 
	 */
	function SessionKillOld($time)
	{
		return(false);
	}
	
	/**
	 * Session schreiben
	 * 
	 */
	function SessionWrite($var, $sid)
	{
		return(false);
	}
	
	/**
	 * Session lesen
	 * 
	 */
	function SessionRead($sid)
	{
		return(array());
	}
	
	/**
	 * Einzigartige SessionID erzeugen
	 * 
	 */
	function SessionIDGen()
	{
		return(false);
	}
	
	/**
	 * Session erstellen
	 * 
	 */
	function SessionCreate()
	{
		return(false);
	}
	
	/**
	 * Session touch
	 * 
	 */
	function SessionTouch($sid)
	{
		return(false);
	}
	
	/**
	 * Wird beim Loeschen eines Benutzers aufgerufen (seit PL10)
	 * 
	 */
	function OnDeleteUser($id)
	{
		return(true);
	}
}
?>