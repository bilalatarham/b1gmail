#!/usr/bin/php -q
<?php
/*
* b1gMail
* (c) 2002-2016 B1G Software
*
* Redistribution of this code without explicit permission
* is forbidden!
*
*/

if(isset($_SERVER['SENDER']) && isset($_SERVER['RECIPIENT']))
{
	$_SERVER['argv'] = array();
	$_SERVER['argv'][] = $_SERVER['SENDER'];
	$_SERVER['argv'][] = '--';
	$_SERVER['argv'][] = $_SERVER['RECIPIENT'];
}
include("pipe.php");
