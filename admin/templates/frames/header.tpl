<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>b1gMail - {lng p="acp"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/common.css?{fileDateSig file="style/common.css"}" rel="stylesheet" type="text/css" />
</head>

<body id="headerBody">

	<div id="headerDiv">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td id="logoTD"><img src="{$tpldir}images/logo_text{if !$isGerman}_en{/if}.png" border="0" alt="" /></td>
				<td id="letterTD"><img src="{$tpldir}images/logo_letter.png" border="0" alt="" /></td>
			</tr>
		</table>
	</div>

</body>

</html>
