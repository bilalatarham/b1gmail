<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>b1gMail - {lng p="acp"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/common.css" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script src="../clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="../clientlib/md5.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" type="text/javascript" language="javascript"></script>
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="../clientlib/pngfix.js"></script>
	<![endif]-->
</head>

<body id="menuBody" onload="menuItem({$item})">

	<table cellspacing="0" cellpadding="0" id="menuTable">
		<!-- welcome -->
		<tr>
			<td class="menuBar" id="menu_item_0"><a href="javascript:menuItem(0);">{lng p="welcome"}</a></td>
		</tr>
		<tbody id="menu_tbody_0" style="display:none;">
			<tr>
				<td class="menuTBody">
					<a href="welcome.php?sid={$sid}" target="content"><img src="./templates/images/ico_license.png" />{lng p="welcome"}</a>
					<a href="admins.php?sid={$sid}" target="content"><img src="./templates/images/ico_users.png" />{lng p="admins"}</a>
				</td>
			</tr>
		</tbody>
		
		{if $adminRow.type==0}
		<!-- prefs -->
		<tr>
			<td class="menuBar" id="menu_item_1"><a href="javascript:menuItem(1);">{lng p="prefs"}</a></td>
		</tr>
		<tbody id="menu_tbody_1" style="display:none;">
			<tr>
				<td class="menuTBody">
					<a href="prefs.common.php?sid={$sid}" target="content"><img src="./templates/images/ico_prefs_common.png" />{lng p="common"}</a>
					<a href="prefs.email.php?sid={$sid}" target="content"><img src="./templates/images/ico_prefs_email.png" />{lng p="email"}</a>
					<a href="prefs.recvrules.php?sid={$sid}" target="content"><img src="./templates/images/rule32.png" />{lng p="recvrules"}</a>
					<a href="prefs.webdisk.php?sid={$sid}" target="content"><img src="./templates/images/ico_disk.png" />{lng p="webdisk"}</a>
					<a href="prefs.sms.php?sid={$sid}" target="content"><img src="./templates/images/gateway32.png" />{lng p="sms"}</a>
					<a href="prefs.coupons.php?sid={$sid}" target="content"><img src="./templates/images/coupon.png" />{lng p="coupons"}</a>
					<a href="prefs.profilefields.php?sid={$sid}" target="content"><img src="./templates/images/field32.png" />{lng p="profilefields"}</a>
					<a href="prefs.languages.php?sid={$sid}" target="content"><img src="./templates/images/lang32.png" />{lng p="languages"}</a>
					<a href="prefs.templates.php?sid={$sid}" target="content"><img src="./templates/images/template.png" />{lng p="templates"}</a>
					<a href="prefs.extensions.php?sid={$sid}" target="content"><img src="./templates/images/extension.png" />{lng p="webdiskicons"}</a>
					<a href="prefs.ads.php?sid={$sid}" target="content"><img src="./templates/images/ad32.png" />{lng p="ads"}</a>
					<a href="prefs.faq.php?sid={$sid}" target="content"><img src="./templates/images/faq32.png" />{lng p="faq"}</a>
					<a href="prefs.countries.php?sid={$sid}" target="content"><img src="./templates/images/country.png" />{lng p="countries"}</a>
					<a href="prefs.widgetlayouts.php?sid={$sid}" target="content"><img src="./templates/images/wlayout_add.png" />{lng p="widgetlayouts"}</a>
					<a href="prefs.payments.php?sid={$sid}" target="content"><img src="./templates/images/ico_prefs_payments.png" />{lng p="payments"}</a>
					<a href="toolbox.php?sid={$sid}" target="content"><img src="./templates/images/toolbox.png" />{lng p="toolbox"}</a>
				</td>
			</tr>
		</tbody>
		{/if}
		
		{if $adminRow.type==0||$adminRow.privileges.users||$adminRow.privileges.groups||$adminRow.privileges.workgroups||$adminRow.privileges.activity||$adminRow.privileges.newsletter||$adminRow.privileges.payments}
		<!-- users & groups -->
		<tr>
			<td class="menuBar" id="menu_item_2"><a href="javascript:menuItem(2);">{lng p="usersgroups"}</a></td>
		</tr>
		<tbody id="menu_tbody_2" style="display:none;">
			<tr>
				<td class="menuTBody">
					{if $adminRow.type==0||$adminRow.privileges.users}<a href="users.php?sid={$sid}" target="content"><img src="./templates/images/user_action.png" />{lng p="users"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.groups}<a href="groups.php?sid={$sid}" target="content"><img src="./templates/images/ico_group.png" />{lng p="groups"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.workgroups}<a href="workgroups.php?sid={$sid}" target="content"><img src="./templates/images/ico_workgroup.png" />{lng p="workgroups"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.activity}<a href="activity.php?sid={$sid}" target="content"><img src="./templates/images/activity.png" />{lng p="activity"}</a>{/if}
					{*{if $adminRow.type==0||$adminRow.privileges.abuse}<a href="abuse.php?sid={$sid}" target="content"><img src="./templates/images/abuse.png" />{lng p="abuseprotect"}</a>{/if}*}
					{if $adminRow.type==0||$adminRow.privileges.newsletter}<a href="newsletter.php?sid={$sid}" target="content"><img src="./templates/images/newsletter.png" />{lng p="newsletter"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.payments}<a href="payments.php?sid={$sid}" target="content"><img src="./templates/images/ico_prefs_payments.png" />{lng p="payments"}</a>{/if}
				</td>
			</tr>
		</tbody>
		{/if}
		
		{if $adminRow.type==0||$adminRow.privileges.optimize||$adminRow.privileges.maintenance||$adminRow.privileges.stats||$adminRow.privileges.logs}
		<!-- tools -->
		<tr>
			<td class="menuBar" id="menu_item_3"><a href="javascript:menuItem(3);">{lng p="tools"}</a></td>
		</tr>
		<tbody id="menu_tbody_3" style="display:none;">
			<tr>
				<td class="menuTBody">
					{if $adminRow.type==0||$adminRow.privileges.optimize}<a href="optimize.php?sid={$sid}" target="content"><img src="./templates/images/db_optimize.png" />{lng p="optimize"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.maintenance}<a href="maintenance.php?sid={$sid}" target="content"><img src="./templates/images/orphans32.png" />{lng p="maintenance"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.stats}<a href="stats.php?sid={$sid}" target="content"><img src="./templates/images/stats.png" />{lng p="stats"}</a>{/if}
					{if $adminRow.type==0||$adminRow.privileges.logs}<a href="logs.php?sid={$sid}" target="content"><img src="./templates/images/filter.png" />{lng p="logs"}</a>{/if}
					{if $adminRow.type==0}<a href="updates.php?sid={$sid}" target="content"><img src="./templates/images/update.png" />{lng p="updates"}</a>{/if}
				</td>
			</tr>
		</tbody>
		{/if}
		
		{if $adminRow.type==0||$adminRow.privileges.plugins}
		<!-- plugins -->
		<tr>
			<td class="menuBar" id="menu_item_4"><a href="javascript:menuItem(4);">{lng p="plugins"}</a></td>
		</tr>
		<tbody id="menu_tbody_4" style="display:none;">
			<tr>
				<td class="menuTBody">
					{if $adminRow.type==0}<a href="plugins.php?sid={$sid}" target="content"><img src="./templates/images/plugin.png" />{lng p="plugins"}</a>{/if}
					{foreach from=$pluginMenuItems item=pluginInfo key=plugin}
					{if $adminRow.type==0||$adminRow.privileges.plugins.$plugin}
					<a href="plugin.page.php?sid={$sid}&plugin={$plugin}" target="content"><img src="{if $pluginInfo.icon}../plugins/templates/images/{$pluginInfo.icon}{else}./templates/images/wlayout_add.png{/if}" />{$pluginInfo.title}</a>
					{/if}
					{/foreach}{pluginMenus}
				</td>
			</tr>
		</tbody>
		{/if}
		
		<!-- logout -->
		<tr>
			<td class="menuBar" id="menu_item_5"><a href="index.php?sid={$sid}&action=logout" target="_top" onclick="return confirm('{lng p="logoutquestion"}');">{lng p="logout"}</a></td>
		</tr>
	</table>
</body>

</html>
