<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>b1gMail - {lng p="acp"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/common.css?{fileDateSig file="style/common.css"}" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script src="../clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="../clientlib/md5.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" type="text/javascript" language="javascript"></script>
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="../clientlib/pngfix.js"></script>
	<![endif]-->
</head>

<body id="titleBody">

	<div id="copyrightDiv">
		<a href="welcome.php?action=about&sid={$sid}" target="content">b1gMail {$bmver}</a>
	</div>

	<div id="helpDiv">	
		{if $adminRow.type==0}
			<a href="welcome.php?action=phpinfo&sid={$sid}" target="content"><img src="{$tpldir}images/phpinfo.png" border="0" alt="" align="absmiddle" width="16" height="16" />
			{lng p="phpinfo"}</a> &nbsp;
			<a href="prefs.common.php?action=license&sid={$sid}" target="content"><img src="{$tpldir}images/license.png" border="0" alt="" align="absmiddle" width="16" height="16" />
			{lng p="license"}</a> &nbsp;
		{/if}
		<a href="javascript:showHelp();"><img src="{$tpldir}images/help.png" border="0" alt="" align="absmiddle" width="16" height="16" />
		{lng p="help"}</a> &nbsp;
		<span style="color:#FFF;">|</span> &nbsp;
		<img src="{$tpldir}images/user_active.png" border="0" alt="" align="absmiddle" width="16" height="16" />
		<a href="admins.php?sid={$sid}" target="content">{text value=$adminRow.username}</a> &nbsp;
		<a href="index.php?sid={$sid}&action=logout" target="_top" onclick="return confirm('{lng p="logoutquestion"}');"><img src="{$tpldir}images/logout.png" border="0" alt="" align="absmiddle" width="16" height="16" />
		{lng p="logout"}</a>
		&nbsp; &nbsp;
	</div>
	
</body>

</html>

