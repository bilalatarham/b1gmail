<form action="updates.php?action=prefs&save=true&sid={$sid}" method="post" onsubmit="spin(this)">	
	<fieldset>
		<legend>{lng p="autoperms"}</legend>
	
		<p>
			{lng p="autoperms_desc"}
		</p>
		
		<table width="90%">
			<tr>
				<td align="left" rowspan="7" valign="top" width="40"><img src="{$tpldir}images/updates.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220">{lng p="enable"}?</td>
				<td class="td2"><input name="ftp_active"{if $bm_prefs.ftp_active=='yes'} checked="checked"{/if} type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="ftphost"}:</td>
				<td class="td2"><input type="text" name="ftp_host" value="{$bm_prefs.ftp_host}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="ftpport"}:</td>
				<td class="td2"><input type="text" name="ftp_port" value="{$bm_prefs.ftp_port}" size="4" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="ftpuser"}:</td>
				<td class="td2"><input type="text" name="ftp_user" value="{$bm_prefs.ftp_user}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="ftppass"}:</td>
				<td class="td2"><input type="password" name="ftp_pass" value="{$bm_prefs.ftp_pass}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="ftpdir"}:</td>
				<td class="td2"><input type="text" name="ftp_dir" value="{$bm_prefs.ftp_dir}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="ftpperms"}:</td>
				<td class="td2"><input type="text" name="ftp_permissions" value="{$bm_prefs.ftp_permissions}" size="4" /></td>
			</tr>
		</table>
		
		<p>
			<div style="float:right">
				<input class="button" type="submit" value=" {lng p="save"} " />
			</div>
		</p>
	</fieldset>
	
	<fieldset>
		<legend>{lng p="patchlevel"}</legend>
	
		<p>
			{lng p="patchlevel_desc"}
		</p>
		
		<table width="90%">
			<tr>
				<td align="left" valign="top" width="40"><img src="{$tpldir}images/updates_pl.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220">{lng p="patchlevel"}:</td>
				<td class="td2">{$bm_prefs.patchlevel}
								<input{if $bm_prefs.patchlevel==0} disabled="disabled"{/if} class="button" type="button" value=" {lng p="reset"} " onclick="document.location.href='updates.php?action=prefs&resetPL=true&sid={$sid}';" /></td>
			</tr>
		</table>
	</fieldset>
</form>
