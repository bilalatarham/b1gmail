<form action="prefs.common.php?action=license&save=true&sid={$sid}" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend>{lng p="licensedetails"}</legend>
	
		<table>
			<tr>
				<td align="left" rowspan="6" valign="top" width="40"><img src="{$tpldir}images/ico_license.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220">{lng p="licenseno"}:</td>
				<td class="td2">{$licenseNo}</td>
			</tr>
			<tr>
				<td class="td1">{lng p="licensedto"}:</td>
				<td class="td2">{$licenseDomain}</td>
			</tr>
			<tr>
				<td class="td1">{lng p="dldate"}:</td>
				<td class="td2">{$licenseDate}</td>
			</tr>
			<tr>
				<td class="td1">{lng p="updateaccess"}:</td>
				<td class="td2"><div id="updateAccess">{lng p="pleasewait"}</div></td>
			</tr>
			<tr>
				<td class="td1">{lng p="copyrightnote"}:</td>
				<td class="td2"><div id="copyrightNote">{lng p="pleasewait"}</div></td>
			</tr>
			<tr>
				<td class="td1">{lng p="serial"}:</td>
				<td class="td2"><input type="text" name="serial_1" value="{$serial_1}" size="5" maxlength="4" /> -
								<input type="text" name="serial_2" value="{$serial_2}" size="5" maxlength="4" /> -
								<input type="text" name="serial_3" value="{$serial_3}" size="5" maxlength="4" /> -
								<input type="text" name="serial_4" value="{$serial_4}" size="5" maxlength="4" /></td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" {lng p="save"} " />
		</div>
	</p>
</form>

<script language="javascript" src="https://ssl.b1g.de/service.b1gmail.com/licenseinfo/?do=js&version={$version}&key={$serial}&lic={$licenseNo}&lang={$lang}"></script>
