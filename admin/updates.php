<?php 
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

include('../serverlib/admin.inc.php');
RequestPrivileges(PRIVILEGES_ADMIN);
AdminRequirePrivilege('updates');

if(!isset($_REQUEST['action']))
	$_REQUEST['action'] = 'updates';

$tabs = array(
	0 => array(
		'title'		=> $lang_admin['updates'],
		'relIcon'	=> 'updates.png',
		'link'		=> 'updates.php?',
		'active'	=> $_REQUEST['action'] == 'updates'
	),
	1 => array(
		'title'		=> $lang_admin['prefs'],
		'relIcon'	=> 'ico_prefs_common.png',
		'link'		=> 'updates.php?action=prefs&',
		'active'	=> $_REQUEST['action'] == 'prefs'
	)
);

/**
 * updates
 */
if($_REQUEST['action'] == 'updates')
{
	$step = isset($_REQUEST['step'])
				? (int)$_REQUEST['step']
				: 0;
	
	//
	// step 1
	//
	if($step == 1)
	{
		$res = QueryUpdateServer('get_patchlevel');
		
		if($res['type'] == 'response')
		{
			if($res['text'] > $bm_prefs['patchlevel'])
			{
				$tpl->assign('version',		B1GMAIL_VERSION);
				$tpl->assign('pl',			$bm_prefs['patchlevel']);
				$tpl->assign('updates',		$res['available_patches']);
			}
		}
		else 
		{
			$tpl->assign('error',		true);
			$tpl->assign('error_desc',	$res['text']);
		}
	}
	
	//
	// step 2
	//
	else if($step == 2)
	{
		$res = QueryUpdateServer('get_patchlevel');
		
		if($res['type'] == 'response')
		{
			// next pl?
			foreach($res['available_patches'] as $patchLevel)
				if($patchLevel > $bm_prefs['patchlevel'])
				{
					$updateTo = $patchLevel;
					break;
				}
			
			// get pl info
			if(isset($updateTo))
			{
				$res = QueryUpdateServer('get_updateinfo', array('updateto' => $updateTo));
				if($res['type'] == 'response')
				{
					$files = array();
					foreach($res['info']['changed_files'] as $file)
						$files[] = array(
								'file'		=> $file,
								'writeable'	=> is_writeable('../' . $file)
							);
					$tpl->assign('files',		$files);
					$tpl->assign('readme',		$res['info']['readme']);
				}
				else 
				{
					$tpl->assign('error',		true);
					$tpl->assign('error_desc',	$res['text']);
				}
			}
		}
		else 
		{
			$tpl->assign('error',		true);
			$tpl->assign('error_desc',	$res['text']);
		}
	}
	
	//
	// step 3
	//
	else if($step == 3)
	{
		$res = QueryUpdateServer('get_patchlevel');
		
		if($res['type'] == 'response')
		{
			// next pl?
			foreach($res['available_patches'] as $patchLevel)
				if($patchLevel > $bm_prefs['patchlevel'])
				{
					$updateTo = $patchLevel;
					break;
				}
			$latestPatchLevel = $res['text'];
				
			// get pl
			if(isset($updateTo))
			{
				$res = QueryUpdateServer('get_update', array('updateto' => $updateTo));
				if($res['type'] == 'response')
				{
					// check integrity
					$checksumError = false;
					foreach($res['update'] as $file=>$contents)
						if($res['checksums'][$file] !== md5($contents))
						{
							$checksumError = true;
							break;
						}
					
					// checksum error?
					if($checksumError)
					{
						$tpl->assign('error',		true);
						$tpl->assign('error_desc',	'Checksum comparison failed. Update package seems to be broken. Please try again.');
					}
					else 
					{
					
						// auto-chmod?
						if($bm_prefs['ftp_active'] == 'yes')
						{
							$chmodFiles = array();
							foreach($res['update'] as $file=>$contents)
								if(!is_writeable('../' . $file))
									$chmodFiles[] = $file;
							if(count($chmodFiles) > 0)
								SetFTPPermissions($chmodFiles, true);
						}
						
						// all files writeable?
						foreach($res['update'] as $file=>$contents)
							if(!is_writeable('../' . $file))
							{
								header('Location: updates.php?step=2&sid=' . session_id());
								exit();
							}
						
						// update!
						$status = array();
						foreach($res['update'] as $file=>$contents)
						{
							$rel = '../' . $file;
							
							if(file_exists($rel))
							{
								$oldFileContents = getFileContents($rel);
								if(substr($rel, 0, -4) == '.php')
									$oldFileContents = '<?php exit(); ?>' . $oldFileContents;
									
								// create backup
								$fp = fopen(sprintf('../data/backup_%s_patchlevel_%d_%s',
												preg_replace('/([^0-9a-zA-Z]*)/', '', B1GMAIL_VERSION),
												$bm_prefs['patchlevel'],
												str_replace('/', '--', $file)),
												'wb');
								if($fp)
								{
									fwrite($fp, $oldFileContents);
									fclose($fp);
									
									// update file
									$fp = fopen($rel, 'wb');
									if($fp)
									{
										fwrite($fp, $contents);
										fclose($fp);
										
										$status[$file] = 0;
									}
									else 
									{
										$status[$file] = 3;
									}
								}
								else 
								{
									$status[$file] = 2;
								}
							}
							else 
							{
								$status[$file] = 1;
							}
						}
						
						// execute update script
						if(trim($res['script']) != '')
							eval($res['script']);
							
						// update patchlevel
						$db->Query('UPDATE {pre}prefs SET patchlevel=?',
							$updateTo);
						
						// reset auto-chmod
						if($bm_prefs['ftp_active'] == 'yes'
							&& isset($chmodFiles)
							&& count($chmodFiles) > 0)
							SetFTPPermissions($chmodFiles, false);
						
						// assign
						$tpl->assign('moreUpdates',		$updateTo < $latestPatchLevel);
						$tpl->assign('status',			$status);
					}
				}
				else 
				{
					$tpl->assign('error',		true);
					$tpl->assign('error_desc',	$res['text']);
				}
			}
		}
	}
	
	$tpl->assign('step', 	$step);
	$tpl->assign('page', 	'updates.tpl');
}

/**
 * prefs
 */
else if($_REQUEST['action'] == 'prefs')
{
	// save?
	if(isset($_REQUEST['save']))
	{
		$db->Query('UPDATE {pre}prefs SET ftp_active=?, ftp_host=?, ftp_port=?, ftp_user=?, ftp_pass=?, ftp_dir=?, ftp_permissions=?',
			isset($_REQUEST['ftp_active']) ? 'yes' : 'no',
			$_REQUEST['ftp_host'],
			(int)$_REQUEST['ftp_port'],
			$_REQUEST['ftp_user'],
			$_REQUEST['ftp_pass'],
			$_REQUEST['ftp_dir'],
			(int)$_REQUEST['ftp_permissions']);
		ReadConfig();
	}
	
	// reset PL?
	if(isset($_REQUEST['resetPL']))
	{
		$db->Query('UPDATE {pre}prefs SET patchlevel=0');
		ReadConfig();
	}
	
	$tpl->assign('bm_prefs',  $bm_prefs);
	$tpl->assign('page', 'updates.prefs.tpl');
}

/**
 * rpc
 */
else if($_REQUEST['action'] == 'updateCheck')
{
	$res = QueryUpdateServer('get_patchlevel');
	
	if($res['type'] == 'response')
	{
		if($res['text'] > $bm_prefs['patchlevel'])
		{
			echo($res['text']);
			exit();
		}
	}
	
	echo('0');
	exit();
}

$tpl->assign('tabs', $tabs);
$tpl->assign('title', $lang_admin['tools'] . ' &raquo; ' . $lang_admin['updates']);
$tpl->display('page.tpl');
?>