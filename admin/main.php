<?php 
/*
 * b1gMail7
 * (c) 2002-2012 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: main.php,v 1.9 2012/09/23 17:49:00 patrick Exp $
 *
 */

include('../serverlib/admin.inc.php');
RequestPrivileges(PRIVILEGES_ADMIN);

if(!isset($_REQUEST['action']))
	$_REQUEST['action'] = 'main';
	
if($_REQUEST['action'] == 'main')
{
	if(isset($_REQUEST['jump']))
		$tpl->assign('jump', $_REQUEST['jump']);
	$tpl->display('main.tpl');
}
else if($_REQUEST['action'] == 'title')
{
	$bmVer = B1GMAIL_VERSION;
	
	if($bm_prefs['patchlevel'] > 0)
		$bmVer .= '-PL' . $bm_prefs['patchlevel'];
	
	$tpl->assign('bmver', $bmVer);
	$tpl->display('frames/title.tpl');
}
else if($_REQUEST['action'] == 'menu')
{
	$pluginMenuItems = array();
	foreach($plugins->_plugins as $className=>$pluginInfo)
		if($plugins->getParam('admin_pages', $className))	
			$pluginMenuItems[$className] = array('title' => $plugins->getParam('admin_page_title', $className),
													'icon' => $plugins->getParam('admin_page_icon', $className),);
	
	asort($pluginMenuItems);
	
	$tpl->assign('item', isset($_REQUEST['item']) ? (int)$_REQUEST['item'] : 0);
	$tpl->assign('pluginMenuItems', $pluginMenuItems);
	$tpl->display('frames/menu.tpl');
}
else if($_REQUEST['action'] == 'header')
{
	$tpl->assign('isGerman', strpos(strtolower($currentLanguage), 'deutsch') !== false);
	$tpl->display('frames/header.tpl');
}
?>