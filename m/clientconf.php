<?php
/**
 * B1GMail
 *
 * E-Mail Client Konfigurator
 *
 * PHP version 5.6
 *
 * @link        https://www.onesystems.ch
 * @copyright   OneSystems GmbH 2009-2018
 * @author      Michael Kleger <michael.kleger@onesystems.ch>
 * @version     1.1.1
 *
 * @Datum       23.07.2015 --> Erstelldatum
 * @Update:     10.07.2018 --> MySQLi Support
 */

include('../serverlib/init.inc.php');

/**
 * delete no redirect cookie, if exists
 */
if(isset($_COOKIE['noMobileRedirect']))
{
	setcookie('noMobileRedirect', false, time()-TIME_ONE_HOUR, '/');
	unset($_COOKIE['noMobileRedirect']);
}

/**
 * default action = ''
 */
if(!isset($_REQUEST['do']))
	$_REQUEST['do'] = '';

/**
 * iOS oder OSX Gerät
 */
if(isset($_SERVER['HTTP_USER_AGENT'])){
	$agent = $_SERVER['HTTP_USER_AGENT'];
}
if (strstr($agent, " AppleWebKit/") && strstr($agent, " Safari/") && !strstr($agent, " CriOS")) {
	$tpl->assign('browser', TRUE);
}


/**
 * Funktionen
 */
function getMailAdress($email) {
    global $db;

    $resMail    = array();
    $resAlias   = array();
    $res = $db->Query("SELECT email FROM {pre}users WHERE email = ?", $email);
    while($row = $res->FetchArray(MYSQLI_ASSOC)) {
        $resMail = $row['email'];
    }
    $res->Free();

    $res = $db->Query("SELECT email FROM {pre}aliase WHERE email = ?", $email);
    while($row = $res->FetchArray(MYSQLI_ASSOC)) {
        $resAlias = $row['email'];
    }
    $res->Free();

    if (!is_array($resMail)) {
        return TRUE;
    } else if (!is_array($resAlias)) {
        return TRUE;
    }
}

function getMailProto($email) {
    global $db;

    $alias = NULL;
    $res = $db->Query("SELECT user FROM {pre}aliase WHERE email = ?", $email);
    while($row = $res->FetchArray(MYSQLI_ASSOC)) {
        $alias = $row['user'];
    }
    $res->Free();

    if (empty($alias)) {
        $gid = NULL;
        $resUsers = $db->Query("SELECT gruppe FROM {pre}users WHERE email = ?", $email);
        while($row = $resUsers->FetchArray(MYSQLI_ASSOC)) {
            $gid = $row['gruppe'];
        }
        $resUsers->Free();
    } else {
        $gid = NULL;
        $resUsers = $db->Query("SELECT gruppe FROM {pre}users WHERE id = ?", $alias);
        while($row = $resUsers->FetchArray(MYSQLI_ASSOC)) {
            $gid = $row['gruppe'];
        }
        $resUsers->Free();
    }

    $info = NULL;
    $resGruppen = $db->Query("SELECT * FROM {pre}gruppen WHERE id = ?", $gid);
    while($row = $resGruppen->FetchArray(MYSQLI_ASSOC)) {
        $info = array(
            'pop3'  => $row['pop3'],
            'imap'  => $row['imap']
        );
    }
    $resGruppen->Free();

    if ($info['imap'] == 'yes' && $info['pop3'] == 'yes') {
        $proto = 'imap';
    } else if ($info['imap'] == 'yes' && $info['pop3'] == 'no') {
        $proto = 'imap';
    } else if ($info['imap'] == 'no' && $info['pop3'] == 'yes') {
        $proto = 'pop3';
    } else {
        $proto = 'none';
    }

    if (isset($proto)) {
        return $proto;
    }
}

/**
 * Mail Adresse prüfen
 */
if(isset($_REQUEST['do']) && $_REQUEST['do']=='Step1') {

	// E-Mail Prüfen
	$email      	= htmlspecialchars(trim($_REQUEST['email']));

} else {
	$ShowStep		= $_REQUEST['do'];
	$email			= '';
}


/**
 * Daten ausgeben
 */
$mailproto = getMailProto($_REQUEST['email']);

$tpl->assign('pageTitle',  		$bm_prefs['titel']);
$tpl->assign('showmsg',			$ShowMSG);
$tpl->assign('do', 				$ShowStep);
$tpl->assign('step',			$ShowStep);
$tpl->assign('email', 			$_REQUEST['email']);
if ($mailproto == 'imap') {
    $tpl->assign('profile', 	'index.php?action=ClientConf&do=step2&down=ios&email='. $_REQUEST['email']);
} else {
    $tpl->assign('profile', 	'javascript:alert(\'Um Ihr Profile einzurichten benötigen Sie die IMAP Option die leider bei Ihnen nicht aktiviert ist. Falls Sie die Option benötigen wenden Sie sich bitte an Ihrem Administrator.\');');
}
$tpl->assign('page', 			'm/clientconf.tpl');
$tpl->display('m/index.tpl');


?>
