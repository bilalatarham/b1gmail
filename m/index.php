<?php 
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

include('../serverlib/init.inc.php');

/**
 * delete no redirect cookie, if exists
 */
if(isset($_COOKIE['noMobileRedirect']))
{
	setcookie('noMobileRedirect', false, time()-TIME_ONE_HOUR, '/');
	unset($_COOKIE['noMobileRedirect']);
}

/**
 * default action = login 
 */
if(!isset($_REQUEST['action']))
	$_REQUEST['action'] = 'login';

/**
 * login
 */
if($_REQUEST['action'] == 'login')
{
	if(isset($_REQUEST['do']) && $_REQUEST['do']=='login')
	{
		// get login
		$password 	= $_REQUEST['password'];
		$email 		= $_REQUEST['email'];
		
		// login
		list($result, $param) = BMUser::Login($email, $password);
		
		// login ok?
		if($result == USER_OK)
		{
			// allowed to access mobile interface?
			$user = _new('BMUser', array(BMUser::GetID($email)));
			$group = $user->GetGroup();
			if($group->_row['wap'] != 'yes')
			{
				$tpl->assign('msg', 		$lang_user['mobiledenied']);
				$tpl->assign('backLink',	'./');
				$tpl->assign('page',		'm/message.tpl');
				$tpl->assign('pageTitle', 	$bm_prefs['titel']);
				$tpl->display('m/index.tpl');
				exit();
			}
			
			// stats
			Add2Stat('mobile_login');
			
			// save login?
			if(isset($_POST['savelogin']))
			{
				// set cookies
				setcookie('bm_msavedUser', 		$email, 		time() + TIME_ONE_YEAR);
				setcookie('bm_msavedPassword', 	$password, 		time() + TIME_ONE_YEAR);
			}
			else 
			{
				// delete cookies
				setcookie('bm_msavedUser', 		'', 			time() - TIME_ONE_HOUR);
				setcookie('bm_msavedPassword', 	'', 			time() - TIME_ONE_HOUR);
			}
						
			// redirect to target page
			header('Location: email.php?sid=' . $param);
			exit();
		}
		else 
		{
			// tell user what happened
			switch($result)
			{
			case USER_BAD_PASSWORD:
				$tpl->assign('msg',	sprintf($lang_user['badlogin'], $param));
				break;
			case USER_DOES_NOT_EXIST:
				$tpl->assign('msg', $lang_user['baduser']);
				break;
			case USER_LOCKED:
				$tpl->assign('msg', $lang_user['userlocked']);
				break;
			case USER_LOGIN_BLOCK:
				$tpl->assign('msg', sprintf($lang_user['loginblocked'], FormatDate($param)));
				break;
			}
			$tpl->assign('backLink',	'./');
			$tpl->assign('page',		'm/message.tpl');
		}
	}
	else 
	{
		$tpl->assign('page', 'm/login.tpl');
	}
	
	// assign
	$tpl->assign('pageTitle', $bm_prefs['titel']);
	$tpl->display('m/index.tpl');
}
?>