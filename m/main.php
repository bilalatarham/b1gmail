<?php 
/*
 * b1gMail7
 * (c) 2002-2008 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: main.php,v 1.11 2008/10/09 08:43:49 patrick Exp $
 *
 */

include('../serverlib/init.inc.php');
include('../serverlib/mailbox.class.php');
include('../serverlib/mailbuilder.class.php');
RequestPrivileges(PRIVILEGES_USER | PRIVILEGES_MOBILE);

/**
 * open mailbox
 */ 
$mailbox = _new('BMMailbox', array($userRow['id'], $userRow['email'], $thisUser));
$folderList = $mailbox->GetFolderList(true);

/**
 * default action = inbox
 */
if(!isset($_REQUEST['action']))
	$_REQUEST['action'] = 'inbox';

/**
 * inbox
 */
if($_REQUEST['action'] == 'inbox')
{
	// get folder id (default = inbox)
	$folderID = (isset($_REQUEST['folder']) && isset($folderList[(int)$_REQUEST['folder']]))
					? (int)$_REQUEST['folder']
					: FOLDER_INBOX;
	$folderName = $folderList[$folderID]['title'];
	
	// page stuff
	$mailsPerPage = $mailbox->GetMailsPerPage($folderID);
	$pageNo = (isset($_REQUEST['page']))
					? (int)$_REQUEST['page']
					: 1;
	$mailCount = $mailbox->GetMailCount($folderID);
	$pageCount = max(1, ceil($mailCount / max(1, $mailsPerPage)));
	$pageNo = min($pageCount, max(1, $pageNo));
	
	// get mail list
	$mailList = $mailbox->GetMailList($folderID,
										$pageNo,
										$mailsPerPage);
	
	// assign
	$tpl->assign('perPage', $mailsPerPage);
	$tpl->assign('pageNo', $pageNo);
	$tpl->assign('pageCount', $pageCount);
	$tpl->assign('folderID', $folderID);
	$tpl->assign('mails', $mailList);
	$tpl->assign('titleIcon', 'li/menu_ico_' . $folderList[$folderID]['type'] . '.png');
	$tpl->assign('titleLink', 'main.php?action=folders&sid=' . session_id());
	$tpl->assign('pageTitle', $folderName);
	$tpl->assign('page', 'm/folder.tpl');
	$tpl->display('m/index.tpl');
}

/**
 * folders
 */
else if($_REQUEST['action'] == 'folders')
{
	// assign
	$tpl->assign('folders', $folderList);
	$tpl->assign('pageTitle', $lang_user['folders']);
	$tpl->assign('page', 'm/folders.tpl');
	$tpl->display('m/index.tpl');
}

/**
 * read
 */
else if($_REQUEST['action'] == 'read'
		&& isset($_REQUEST['id']))
{
	$mail = $mailbox->GetMail((int)$_REQUEST['id']);
	
	if($mail !== false)
	{
		// unread? => mark as read
		if(($mail->flags & FLAG_UNREAD) != 0)
			$mailbox->FlagMail(FLAG_UNREAD, false, (int)$_REQUEST['id']);
		
		// get text part
		$textParts = $mail->GetTextParts();
		if(isset($textParts['text']))
		{
			$textMode = 'text';
			$text = formatEMailText($textParts['text']);
		}
		else if(isset($textParts['html']))
		{
			$textMode = 'html';
			$text = $textParts['html'];
		}
		else 
		{
			$textMode = 'text';
			$text = '';
		}
		
		// prev & next mail
		list($prevID, $nextID) = $mailbox->GetPrevNextMail($mail->_row['folder'], (int)$_REQUEST['id']);
		if($prevID != -1)
			$tpl->assign('prevID', $prevID);
		if($nextID != -1)
			$tpl->assign('nextID', $nextID);
			
		// reply to
		if(($replyTo = $mail->GetHeaderValue('reply-to')) && $replyTo != '')
			$replyTo = $replyTo;
		else 
			$replyTo = $mail->GetHeaderValue('from');

		// assign
		$tpl->assign('subject', $mail->GetHeaderValue('subject'));
		$tpl->assign('folderID', $mail->_row['folder']);
		$tpl->assign('mailID', (int)$_REQUEST['id']);
		$tpl->assign('pageTitle', htmlentities(strlen($mail->GetHeaderValue('subject')) > 25 
			? substr($mail->GetHeaderValue('subject'), 0, 23) . '...' 
			: $mail->GetHeaderValue('subject')));
		$tpl->assign('replyTo', ExtractMailAddress($replyTo));
		$tpl->assign('replySubject', urlencode($userRow['re'] . ' ' . $mail->GetHeaderValue('subject')));
		$tpl->assign('from', $mail->GetHeaderValue('from'));
		$tpl->assign('to', $mail->GetHeaderValue('to'));
		$tpl->assign('cc', $mail->GetHeaderValue('cc'));
		$tpl->assign('date', $mail->date);
		$tpl->assign('text', $text);
		$tpl->assign('page', 'm/read.tpl');
		$tpl->display('m/index.tpl');
	}
}

/**
 * compose
 */
else if($_REQUEST['action'] == 'compose')
{
	$mail = array();
	if(isset($_REQUEST['to']))
		$mail['to'] = $_REQUEST['to'];
	if(isset($_REQUEST['subject']))
		$mail['subject'] = $_REQUEST['subject'];
	
	// assign
	$tpl->assign('mail', $mail);
	$tpl->assign('pageTitle', $lang_user['sendmail']);
	$tpl->assign('page', 'm/compose.tpl');
	$tpl->display('m/index.tpl');
}

/**
 * delete mail
 */
else if($_REQUEST['action'] == 'deleteMail'
		&& isset($_REQUEST['id']))
{
	$mail = $mailbox->GetMail((int)$_REQUEST['id']);
	
	if($mail !== false)
	{
		$folderID = $mail->_row['folder'];
		$mailbox->DeleteMail($_REQUEST['id']);
		header('Location: main.php?folder=' . $folderID . '&sid=' . session_id());
	}
}

/**
 * send mail
 */
else if($_REQUEST['action'] == 'sendMail')
{
	$tpl->assign('backLink', 'main.php?action=compose&sid=' . session_id());
	
	// wait time?
	if(($userRow['last_send'] + $groupRow['send_limit']) > time())
	{
		$tpl->assign('msg', sprintf($lang_user['waituntil3'], ($userRow['last_send'] + $groupRow['send_limit']) - time()));
	}
	else 
	{
		// no recipients?
		$recipients = ExtractMailAddresses($_REQUEST['to'] . ' ' . $_REQUEST['cc']);
		if(count($recipients) > 0)
		{
			// too much recipients?
			if(count($recipients) > $bm_prefs['max_bcc'])
			{
				$tpl->assign('msg', sprintf($lang_user['toomanyrecipients'], $bm_prefs['max_bcc'], count($recipients)));
			}
			else 
			{
				//
				// headers
				//
				$to 	= $_REQUEST['to'];
				$cc 	= $_REQUEST['cc'];
				
				// sender?
				$senderAddresses = $thisUser->GetPossibleSenders();
				$from = $senderAddresses[0];
				
				// prepare header fields
				$to = trim(str_replace(array("\r", "\t", "\n"), '', $to));
				$cc = trim(str_replace(array("\r", "\t", "\n"), '', $cc));
				$subject = trim(str_replace(array("\r", "\t", "\n"), '', $_REQUEST['subject']));
				$replyTo = $from;
					
				// build the mail
				$mail = _new('BMMailBuilder');
				
				// mandatory headers
				$mail->AddHeaderField('X-Sender-IP',	$_SERVER['REMOTE_ADDR']);
				$mail->AddHeaderField('From', 			$from);
				$mail->AddHeaderField('Subject', 		$subject);
				$mail->AddHeaderField('Reply-To', 		$replyTo);
				
				// optional headers
				if($to != '')
					$mail->AddHeaderField('To',	 	$to);
				if($cc != '')
					$mail->AddHeaderField('Cc', 	$cc);
				
				//
				// add text
				//
				$mailText = $_REQUEST['text'] . GetsigStr('text');
				ModuleFunction('OnSendMail', array(&$mailText, false));
				$mail->AddText($mailText,
					'plain',
					$currentCharset);
				
				//
				// send!
				//
				$outboxFP = $mail->Send();
				
				//
				// ok?
				//
				if($outboxFP && is_resource($outboxFP))
				{					
					//
					// update stats
					//
					Add2Stat('send');
					$domains = explode(':', $bm_prefs['domains']);
					$local = false;
					foreach($domains as $domain)
						if(strpos(strtolower($to . $cc), '@'.strtolower($domain)) !== false)
							$local = true;
					Add2Stat('send_'.($local ? 'intern' : 'extern'));
					$thisUser->UpdateLastSend(count($recipients));
		
					//
					// add log entry
					//
					PutLog(sprintf('<%s> (%d, IP %s) sends mail from <%s> to <%s> using mobile compose form',
						$userRow['email'],
						$userRow['id'],
						$_SERVER['REMOTE_ADDR'],
						ExtractMailAddress($from),
						implode('>, <', $recipients)),
						PRIO_NOTE,
						__FILE__,
						__LINE__);
					
					//
					// save copy
					//
					$saveTo = FOLDER_OUTBOX;
					$mailObj = _new('BMMail', array(0, false, $outboxFP, false));
					$mailObj->Parse();
					$mailObj->ParseInfo();
					$mailbox->StoreMail($mailObj, $saveTo);
					
					//
					// clean up
					//
					$mail->CleanUp();
				
					//
					// done
					//
					$tpl->assign('msg', $lang_user['mailsent']);
					$tpl->assign('backLink', 'main.php?sid=' . session_id());
				}
				else 
				{
					$tpl->assign('msg', $lang_user['sendfailed']);
				}
			}
		}
		else 
		{
			$tpl->assign('msg', $lang_user['norecipients']);	
		}
	}
	
	// assign
	$tpl->assign('page', 'm/message.tpl');
	$tpl->assign('pageTitle', $lang_user['sendmail']);
	$tpl->display('m/index.tpl');
}

/**
 * logout
 */
else if($_REQUEST['action'] == 'logout')
{
	// delete cookies
	setcookie('bm_savedUser', 		'', 			time() - TIME_ONE_HOUR);
	setcookie('bm_savedPassword', 	'', 			time() - TIME_ONE_HOUR);
	setcookie('bm_savedLanguage', 	'', 			time() - TIME_ONE_HOUR);
	setcookie('bm_savedSSL', 		'', 			time() - TIME_ONE_HOUR);
	BMUser::Logout();
	header('Location: ./index.php');
	exit();
}
?>