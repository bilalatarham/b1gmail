<?php

include(B1GMAIL_DIR . 'api/config/config.php');
include(B1GMAIL_DIR . 'api/objects/validate_token.php');

if($_POST['action'] == 'create_token')
{
    include(B1GMAIL_DIR . 'api/objects/create_token.php');
    $user = new CreateToken($_POST);
    $user->init();
}
else if($_POST['action'] == 'directory_listing')
{
    //include B1 Gmail webdisk library
    include(B1GMAIL_DIR .'serverlib/webdisk.class.php');
    include(B1GMAIL_DIR .'serverlib/zip.class.php');
    include(B1GMAIL_DIR .'serverlib/unzip.class.php');

    //directory listing webservices
    include(B1GMAIL_DIR . 'api/objects/media_storage.php');
    $user = new MediaStorage($_POST);
    $user->listStorageDisk();
}
// get folder or file information 
else if($_POST['action'] == 'cloud_source_info')
{
    include(B1GMAIL_DIR . 'api/objects/media_storage.php');
    $user = new MediaStorage($_POST);
    $user->getSourceInfo();
}
//prepare directory listing 
else if($_POST['action'] == 'prepare_directory_listing')
{
    //include B1 Gmail webdisk library
    include(B1GMAIL_DIR .'serverlib/webdisk.class.php');
    include(B1GMAIL_DIR .'serverlib/zip.class.php');
    include(B1GMAIL_DIR .'serverlib/unzip.class.php');

    //directory listing webservices
    include(B1GMAIL_DIR . 'api/objects/media_storage.php');
    $user = new MediaStorage($_POST);
    $user->prepareStorageDisk();
}
//create file
else if($_POST['action'] == 'create_files')
{    //directory listing webservices
    include(B1GMAIL_DIR . 'api/objects/media_storage.php');
  
    $user = new MediaStorage($_POST , $_FILES);
    $user->uploadeFile();
}
//create folder
else if($_POST['action'] == 'create_folder')
{    //directory listing webservices
    include(B1GMAIL_DIR . 'api/objects/media_storage.php');
  
    $user = new MediaStorage($_POST);
    $user->CreateFolder();
}
else if($_POST['action'] == 'storage_access')
{
    include(B1GMAIL_DIR . 'api/objects/media_storage.php');
    $user = new MediaStorage($_POST);
    $user->getDiskStorage();
}

else if($_POST['action'] == 'get_contact_listing')
{
    include(B1GMAIL_DIR . 'api/objects/caldav.php');
    $user = new Caldav($_POST);
    $user->listContacts();
}