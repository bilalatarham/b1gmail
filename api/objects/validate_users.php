<?php

/**
 * Authenticate user and return user profile
*/

class AuthenticateUsers
{
    /**
     * post data object property
    */
    private $post; 
 
    /**
     * object properties
    */
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $total_space;
    public $used_space;
    public $available_space;
    
    /**
     * get post value from external source
     * $post array 
     */
    public function __construct($post)
    {
        $this->post = $post['data'];
    }

    // check if given email exist in the database
    function authuser()
    {
        global $db;
       
        /**
         * query result to check user in database
        */
        
        $query = $db->Query('SELECT id,email,passwort,vorname,nachname,diskspace_used FROM {pre}users WHERE email=?',
        $this->post['email']);
        $result = $query->FetchArray(MYSQLI_ASSOC);
        
        /**
         * check if query return results
        */

        if($result['email'])
        {
            /**
             * assign values to object properties
            */
            $this->id = $result['id'];
            $this->firstname = $result['vorname'];
            $this->lastname = $result['nachname'];
            $this->email = $result['email'];
            $this->password = $result['passwort'];
            $this->total_space = $result['diskspace_used'];
            $this->used_space = $result['diskspace_used'];
            $this->available_space = $result['diskspace_used'];
            
            /**
             * return true because email exists in the database
            */

            return true;
        }

        /**
         * return false if email does not exist in the database
        */

        return false;
    }
}