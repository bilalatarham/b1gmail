<?php
use \Firebase\JWT\JWT;
class MediaStorage
{
   /**
    * post data object properasdas
    * $_POST
    */
    private $post;
    private $user;
    private $files;

    /**
     * object properties
     */
    public $storageArray = [];

    function __construct($post ,$files ='')
    {
        $validateToken = new ValidateToken($post);
        $this->user    = $validateToken->init();
        $this->post    = $post['data'];
        $this->files   = $files;
    }


    /**
     * Directory listing
    */
    public function listStorageDisk()
    {
        $user_id = 3;
        $webdisk 		= _new('BMWebdisk', array($user_id));
        $folderID 		= !isset($this->post['directory_id']) ? 0 : (int)$this->post['directory_id'];
        $folderPath 	= $this->getFolderPath($folderID);

        $sourcePath = '/';
        if(!empty($folderPath))
        {
            foreach($folderPath as $folderBit)
            {
                $sourcePath .= $folderBit['title'] . '/';
                //$folderInfo 	= $webdisk->GetFolderInfo($folderID);
                $folderContent 	= $this->GetFolderContent($folderID,false, false,$sourcePath);
                $this->storageArray = $folderContent;
            }
        }
        else
        {
            $folderContent 	= $this->GetFolderContent($folderID,false, false,$sourcePath);
            $this->storageArray = $folderContent;
        }
       
        if(isset($this->storageArray[0]) && !empty($this->storageArray[0]))
        {
            http_response_code(200);
            header('Content-type: application/json');
            echo json_encode(
                [
                    'status' => 1,
                    "message" => "Storage files!",
                    "data" => $this->storageArray,
                ]
            );
        }
        else
        {
            header('Content-type: application/json');
            echo json_encode(
                [
                    'status' => 1,
                    "message" => "No storage files!",
                    "data" => [],
                ]
            );
        }
        exit();
    }


    /**
     * prepate directory listing by source path selected 
    */
    public function prepareStorageDisk()
    {
        $user_id = 3;
        if($this->post['source']['type'] == 'dir')
        {
            $folderID 		= !isset($this->post['source']['id']) ? 0 : (int)$this->post['source']['id'];
            $folderPath 	= $this->getFolderPath($folderID);
    
            $sourcePath = '/';
            if(!empty($folderPath))
            {
                foreach($folderPath as $folderBit)
                {
                    $sourcePath .= $folderBit['title'] . '/';
                    $folderContent 	= $this->GetFolderContent($folderID,false, false,$sourcePath);
                    $this->storageArray = $folderContent;
                }
            }
        }
        else if($this->post['source']['type'] == 'file')
        {
            $fileID = $this->post['source']['id'];
            $this->storageArray = $this->getSourceInfo($fileID,$user_id,'file');
        }

        if(isset($this->storageArray[0]) && !empty($this->storageArray[0]))
        {
            http_response_code(200);
            header('Content-type: application/json');
            echo json_encode(
                [
                    'status' => 1,
                    "message" => "Storage files!",
                    "data" => $this->storageArray,
                ]
            );
        }
        else
        {
            header('Content-type: application/json');
            echo json_encode(
                [
                    'status' => 1,
                    "message" => "No storage files!",
                    "data" => [],
                ]
            );
        }
        exit();
    }

    /**
     * get user storage space
    */
    public function getDiskStorage()
    {
        /**
         * query result to check user in database
        */
        global $db;
        
        $query = $db->Query('SELECT id,email,passwort,vorname,nachname,diskspace_used FROM {pre}users WHERE id=?',
        $this->user->id);
        $result = $query->FetchArray(MYSQLI_ASSOC);
        
        /**
         * check if query return results
        */

        if(isset($result['email']) && $result['email'] <> '')
        {
            $storage = [
                'total_space' =>  $result['diskspace_used'],
                'used_space' =>  $result['diskspace_used'],
                'available_space' =>  $result['diskspace_used'],    
            ];

            /**
             * return storage space data
            */
            echo json_encode($storage);
            exit();
        }
    }

    /**
	 * get folder contents
	 *
	 * @param integer $folderID Folder ID
	 */
	public function GetFolderContent($folderID, $sort = 'dateiname', $order = 'ASC',$sourcePath)
	{
        global $db;
        
		$result = array();
		if(!in_array($sort, array('dateiname', 'size')))
			$sort = 'dateiname';
		
		// folders
		$res = $db->Query('SELECT id,titel,share,created,accessed,modified FROM {pre}diskfolders WHERE parent=? AND user=? ORDER BY titel ' . $order,
			$folderID,
            $this->user->id);
		while($row = $res->FetchArray(MYSQLI_ASSOC))
		{
			$result[] = array(
				'id'		=> $row['id'],
				'type'		=> 'dir',//WEBDISK_ITEM_FOLDER,
				'basename'		=> $row['titel'],
				'name'		=> $row['titel'],
				'share'		=> $row['share']=='yes',
				'size'		=> 0,
				'created'	=> $row['created'],
				'ext'		=> $row['share']=='yes' ? '.SHAREDFOLDER' : '.FOLDER',
                'viewable'	=> true,
                'path'      => $sourcePath,
                'dirname'   => $row['titel'],
			);
		}
		$res->Free();
		
		// file
		$res = $db->Query('SELECT id,dateiname,size,created,accessed,modified,contenttype,ordner FROM {pre}diskfiles WHERE ordner=? AND user=? ORDER BY ' . $sort . ' ' . $order,
			$folderID,
            $this->user->id);
		while($row = $res->FetchArray(MYSQLI_ASSOC))
		{
			$dotPos = strrchr($row['dateiname'], '.');
            if($dotPos !== false)
            {
				$ext = substr($dotPos, 1);
            }
            else 
            {
                $ext = '?';
            }
            $B1GMAIL_DIR = str_replace("\\",'/', B1GMAIL_DIR);
            $B1GMAIL_DATA_DIR = str_replace("\\",'/', B1GMAIL_DATA_DIR);
            $dataDir = str_replace($B1GMAIL_DIR,"",$B1GMAIL_DATA_DIR);
            $result[] = [
                'id'		 => $row['id'],
                'type'		 => 'file',//WEBDISK_ITEM_FILE,
                'basename'		=> $row['dateiname'],
                'name'		 => $row['dateiname'],
                'size'		 => (int)$row['size'],
                'created'	 => (int)$row['created'],
                'accessed'	 => (int)$row['accessed'],
                'modified'	 => (int)$row['modified'],
                'ctype'		 => $row['contenttype'],
                'ext'		 => $ext,
                'path'       => $sourcePath,
                'dirname'    => $this->getFolderNameById($row['ordner']),
                'file_path'  => B1GMAIL_DATA_DIR .$row['id'].'.dsk',
                'public_url' => BASE_URL.$dataDir.$row['id'].'.dsk'
            ];
		}
        $res->Free();

		return($result);
    }

    /**
	 * get path to folder
	 * 
	 * @param int $folderID Folder ID
	 */
	public function getFolderPath($folderID)
	{
		global $db;

		$path = array();
		$parentID = $folderID;
		
		while($parentID != 0)
		{
			$res = $db->Query('SELECT id,titel,parent,share,share_pw FROM {pre}diskfolders WHERE id=? AND user=?',
				$parentID,
                $this->user->id);

			if($res->RowCount() == 0)
				break;
			list($thisID, $thisTitle, $parentID, $share, $share_pw) = $res->FetchArray(MYSQLI_NUM);
			$res->Free();
			
			$path[] = array('id' => $thisID, 'title' => $thisTitle, 'share' => $share, 'share_pw' => $share_pw);
		}
		
		$path = array_reverse($path);
		return($path);
	}

    /**
	 * get folder or file info
	 *
	 * @param int $folderID Folder ID
	 * @return array
	 */
    public function getSourceInfo($sourceId ='' ,$userId ='',$type ='')
    {
        $response = '';
        if($sourceId == '' || $userId || $type)
        {
            $sourceId = $this->post['id'];
            $type = $this->post['type'];
            $userId = 3;
            $response = 'json';
        }

        if($type == 'dir')
        {
            global $db;
            $res = $db->Query("SELECT * FROM {pre}diskfolders WHERE id=? AND user=?",
                $sourceId,
                $userId);
            if($res->RowCount() == 0)
            {
                $info = [];
            }
            else
            {
                $info = $res->FetchArray(MYSQLI_ASSOC);
                $info['name'] = $info['titel'];
                $info['type'] = 'dir';
            }

            $res->Free();
        }
        else if($type == 'file')
        {
            global $db;
            $res = $db->Query('SELECT * FROM {pre}diskfiles WHERE id=? AND user=?',
                $sourceId,
                $userId);

            if($res->RowCount() == 0)
            {
                $info = [];
            }
            else
            {
                $info = $res->FetchArray(MYSQLI_ASSOC);
                $info['name'] = $info['dateiname'];
                $info['type'] = 'file';
                $info['dirname']   = $this->getFolderNameById($info['ordner']);
                $info['file_path'] = B1GMAIL_DATA_DIR .$info['id'].'.dsk';
                $B1GMAIL_DIR = str_replace("\\",'/', B1GMAIL_DIR);
                $B1GMAIL_DATA_DIR = str_replace("\\",'/', B1GMAIL_DATA_DIR);
                $dataDir = str_replace($B1GMAIL_DIR,"",$B1GMAIL_DATA_DIR);
                $info['public_url'] = BASE_URL.$dataDir.$info['id'].'.dsk';
            }
          

            $res->Free();
        }
        if($response == 'json')
        {
            if(isset($info['id']))
            {
                http_response_code(200);
                header('Content-type: application/json');
                echo json_encode(
                    [
                        'status' => 1,
                        "message" => "Storage files!",
                        "data" => $info,
                    ]
                );
            }
            else
            {
                header('Content-type: application/json');
                echo json_encode(
                    [
                        'status' => 1,
                        "message" => "No storage files!",
                        "data" => [],
                    ]
                );
            }
        }
        else
        {
            return [$info];
        }
    }

    public function getFolderNameById($folderId)
    {
        global $db;

        $res = $db->Query("SELECT * FROM {pre}diskfolders WHERE id=?",
        $folderId);

        return $res->FetchArray(MYSQLI_ASSOC)['titel'];
    }

    /**
     * upload files
     */
    public function uploadeFile()
    {

        if (in_array($this->post['cloud_source'] , array(GOOGLE_DRIVE,DROPBOX,ONE_DRIVE,IONOS_HI_DRIVE)))
        {
            $fileName = $this->post['meta_data']['name'];
            $fileSize = (int)$this->post['meta_data']['size'];
            $mimeType = $this->post['meta_data']['contenttype'];
            $content = $this->post['content'];
        }
        else
        {
            $fileName = isset($this->post['content']['dateiname']) ? $this->post['content']['dateiname'] : 'unknown';
            $fileSize = (int)$this->post['content']['size'];
            $mimeType = $this->post['content']['contenttype'];
            $content = $this->post['content']['file_path'];
            $content = file_get_contents($content);
        }
        
        $folderID = $this->post['destination_folder_id'] ? $this->post['destination_folder_id'] : 0;

        if($mimeType == '' || $mimeType == 'application/octet-stream')
            $mimeType = GuessMIMEType($fileName);

        if(($fileID = $this->CreateFile($folderID, $fileName, $mimeType, $fileSize,$this->user->id)) !== false)
        {
            file_put_contents(B1GMAIL_DATA_DIR.$fileID.'.dsk',$content);
            //print_r($this->post);
            echo json_encode(array("file uploaded"));
            die();
        }
        else
        {
            echo json_encode(array("file already exists!"));
        }
    }

    /**
	 * create a new file, returns path to datafile
	 *
	 * @param int $folderID Parent folder
	 * @param string $fileName File name
	 * @param string $mimeType Mime type
	 * @param int $fileSize File size
	 * @param int $user_id User id
	 * @return string
	 */
	function CreateFile($folderID, $fileName, $mimeType, $fileSize,$user_id)
	{
		global $db;
		
		if( $this->FileExists($folderID, $fileName,$user_id))
			return(false);
		
		$db->Query('BEGIN');
		$db->Query('INSERT INTO {pre}diskfiles(user,dateiname,ordner,size,contenttype,created,accessed,modified,blobstorage) VALUES(?,?,?,?,?,?,?,?,?)',
            $user_id,
			$fileName,
			$folderID,
			$fileSize,
			$mimeType,
			time(),
			time(),
			time(),
			BMBlobStorage::getDefaultWebdiskProvider());
		$id = $db->InsertId();
		$this->UpdateSpace($fileSize,$user_id);
		$db->Query('COMMIT');

		assert('$id > 0');
		
		return($id);
	}
	
	/**
	 * check if file exists, return id
	 *
	 * @param int $folderID Parent folder
	 * @param string $fileName File name
	 * @return int
	 */
	function FileExists($folderID, $fileName , $user_id)
	{
		global $db;
		
		$res = $db->Query('SELECT id FROM {pre}diskfiles WHERE dateiname=? AND ordner=? AND user=?',
			$fileName,
			$folderID,
			$user_id);
	 	if($res->RowCount() != 0)
	 	{
			list($id) = $res->FetchArray(MYSQLI_NUM);
			$res->Free();
			return($id);
	 	}
	 	
	 	return(0);
    }
    

    /**
	 * update space
	 *
	 * @param int $bytes Bytes (negative or positive)
	 * @return boolean
	 */
	function UpdateSpace($bytes,$user_id)
	{
		global $db;

		if($bytes == 0)
			return(true);
		
		if($bytes < 0)
		{
			$db->Query('UPDATE {pre}users SET diskspace_used=diskspace_used-LEAST(diskspace_used,'.abs($bytes).') WHERE id=?',
            $user_id);
		}
		else if($bytes > 0)
		{
			$db->Query('UPDATE {pre}users SET diskspace_used=diskspace_used+' . abs($bytes) . ' WHERE id=?',
            $user_id);
		}
		
		return(true);
	}

	/**
	 * check if folder exists
	 *
	 * @param int $folderID Parent folder
	 * @param string $folderName Folder name
     * @param int $user_id user id
	 * @return bool
	 */
	function FolderExists($folderID, $folderName ,$user_id)
	{
		global $db;
		
		$res = $db->Query('SELECT id FROM {pre}diskfolders WHERE titel=? AND parent=? AND user=?',
			$folderName,
			$folderID,
			$user_id);
	 	if($res->RowCount() != 0)
	 	{
			list($id) = $res->FetchArray(MYSQLI_NUM);
			$res->Free();
			return $id;
	 	}
	 	
		return false;
    }
    
    /**
	 * create a new folder
	 * @return bool
	 */
	function CreateFolder()
	{
        global $db;
        
        $folderID = $this->post['destination_folder_id'];
        $folderID = explode("/",$folderID );
        if(isset($folderID[1]))
        {
            $folderNameId = array_slice($folderID, -2, 2, true);
            $folderID = $folderNameId[0];
            $folderName = $folderNameId[1];
        }
        else
        {
            $folderID = $folderID[0];
            $folderName = $this->post['folder_name'];
        }
        $checkFolder = $this->FolderExists($folderID, $folderName,$this->user->id);

        if($checkFolder)
        {
            echo json_encode(['path' => $checkFolder ]);
            die();
        }
		
		$db->Query('INSERT INTO {pre}diskfolders(user,parent,titel,share,share_pw,created,accessed,modified) VALUES(?,?,?,?,?,?,?,?)',
            $this->user->id,
			$folderID,
			$folderName,
			'no',
			'',
			time(),
			time(),
            time());
            
            echo json_encode(['path' => $db->InsertId() ]);
            die();
	}
}