<?php

use \Firebase\JWT\JWT;
class Caldav
{
   /**
    * post data object properasdas
    * $_POST
    */
    private $post;
    private $user;

    /**
     * object properties
     */

    function __construct($post)
    {
        $validateToken = new ValidateToken($post);
        $this->user    = $validateToken->init();
        $this->post    = $post['data'];
    }

    /**
     * List contacts by user
    */
    public function listContacts()
    {
        global $db;

echo json_encode($this->user->id);exit;
        $query = $db->Query('SELECT * FROM {pre}adressen WHERE id=?',
        $this->user->id);
        $addressBook = [];
        while($row = $query->FetchArray(MYSQLI_ASSOC))
		{
			$addressBook[] = [
                'firstname'			=> $row['vorname'],
                'lastname'			=> $row['nachname'],
                'salutation'	    => $row['anrede'],
                'position'			=> $row['position'],
                'company'			=> $row['firma'],
                'private_street'	=> $row['strassenr'],
                'private_zip'		=> $row['plz'],
                'private_city'		=> $row['ort'],
                'private_country'	=> $row['land'],
                'private_email'		=> $row['email'],
                'private_phone'		=> $row['tel'],
                'private_mobile'	=> $row['handy'],
                'private_fax'		=> $row['fax'],
                'work_street'	    => $row['work_strassenr'] ,
                'work_zip'			=> $row['work_plz'],
                'work_city'			=> $row['work_ort'],
                'work_country'		=> $row['work_land'],
                'work_email'		=> $row['work_email'],
                'work_phone'		=> $row['work_tel'],
                'work_mobile'		=> $row['work_handy'],
                'work_fax'			=> $row['work_fax'],
                'web'				=> $row['web'],
                'comment'			=> $row['kommentar'],
                'birthday'		    => $row['geburtsdatum']
            ];
		}
		$query->Free();
        echo json_encode($addressBook);
        exit;

    }
}