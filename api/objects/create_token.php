<?php

/**
 * Authenticate user and return user profile
*/
use \Firebase\JWT\JWT;

class CreateToken
{
    /**
     *  post data object property
     * $_POST
    */
    private $post;

    function __construct($post)
    {
        $this->post = $post;
    }
    
    public function init()
    {
        include(B1GMAIL_DIR . 'api/objects/validate_users.php');
        $user = new AuthenticateUsers($this->post);
        
        if($user->authuser())
        {
            $token = [
                "iat" => config['jwt_issued_at'],
                "exp" => config['jwt_expiration_time'],
                "iss" => config['jwt_issuer'],
                "data" => [
                    "id" => $user->id,
                    "email" => $user->email
                ]
            ];
         
            // set response code
            http_response_code(200);
        
            // generate jwt
            $jwt = JWT::encode($token,config['jwt_key']);
            header('Content-type: application/json');
            echo json_encode(
                [
                    'status' => 1,
                    "message" => "login successfully!",
                    "jwt" => $jwt,
                    'user' => [
                        "id" => $user->id,
                        "username" => $user->email,
                        "firstname" => $user->firstname,
                        "lastname" => $user->lastname,
                        "email" => $user->email,
                        'password' => $user->password,
                        "token" => $jwt,
                        "refreshToken" => '',
                        "token_type" => "JWT",
                        "expiresIn" => config['jwt_expiration_time'],
                    ],
                    'storage_space' => [
                        'total_space' => $user->total_space,
                        'used_space' => $user->used_space,
                        'available_space' => $user->available_space,
                    ]
                ]
            );
            exit;
        }
        else
        {
            // set response code
            http_response_code(401);
            // tell the user login failed
            echo json_encode(
                [
                    'status' => 0,
                    "message" => "Login failed."
                ]
            );
        }
    }
}