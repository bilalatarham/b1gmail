<?php

/**
 * Validate token and user credentials before moving to action
*/
use \Firebase\JWT\JWT;

class ValidateToken
{
    /**
     *  post data object property
     * $_POST
    */
    private $post;

    function __construct($post)
    {
        $this->post = $post['data'];
    }

    public function init()
    {
        // get jwt
        $jwt = isset($this->post['jwt']) ? $this->post['jwt'] : "";

        //decode jwt here if jwt is not empty
        if($jwt)
        {
            // if decode succeed, show user details
            try {
                // decode jwt
                $decoded = JWT::decode($jwt, config['jwt_key'], array('HS256'));
            
                // set response code
                http_response_code(200);

                return $decoded->data;
                // show user details
                // echo json_encode(array(
                //     "message" => "Access granted.",
                //     "data" => $decoded->data
                // ));
            }
            // if decode fails, it means jwt is invalid
            catch (Exception $e)
            {
                // set response code
                http_response_code(401);

                // token expire return false
                return false;

                // tell the user access denied  & show error message
                // echo json_encode(array(
                //     "message" => "Access denied.",
                //     "error" => $e->getMessage()
                // ));
            }
        }
        // show error message if jwt is empty
        else
        {
            // token expire return false
            return false;

            // set response code
            //http_response_code(401);

            // tell the user access denied
            //echo json_encode(array("message" => "Access denied."));
        }
    }
}