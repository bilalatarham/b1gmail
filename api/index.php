<?php
/**
 * include vendor directory to use install package
*/

include_once('../vendor/autoload.php');

/**
 * required headers
*/
header("Access-Control-Allow-Origin: http://localhost/b1gmail/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include('../serverlib/init.inc.php');


// $query = $db->Query('SELECT * FROM bm60_users WHERE email=?',
//         'bilal@vm24.qdns1.com');
//         $result = $query->FetchArray(MYSQLI_ASSOC);
// print_r($query);
// exit;
/**
 * Initiate apis
*/
include_once('init_api.php');
