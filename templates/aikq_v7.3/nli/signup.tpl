<div class="aikq_reg_left">
	<img class="home_regbutton" src="{$tpldir}images/aikq/reg_pic_left.png">
</div>
<div class="aikq_reg_right">
	<form action="index.php?action=signup" method="post" onsubmit="submitSignupForm()">
	<input type="hidden" name="do" value="createAccount" />
	<input type="hidden" name="transPostVars" value="true" />
	<input type="hidden" name="codeID" value="{$codeID}" />

	<div id="signupSpinner" style="display:none;width:100%;">
	<center><img src="{$tpldir}images/load_32.gif" border="0" alt="" /><br /><br /></center>
	</div>
	<div id="signupForm" style="display:;">

	<!-- sign up -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="signup"}</h3></div>
	<div class="aikq_div">{lng p="signuptxt"} {if $code}{lng p="signuptxt_code"}{/if}</div>
	<div class="aikq_div">
		{if $errorStep}
		<br /><br /><div class="errorText">{$errorInfo}</div>
		{else}
		<br />
		{/if}
	</div>
	<!-- address -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="wishaddress"}</h3></div>
	<div class="aikq_regdiv">
		<input class="aikq_reginput" type="text" name="email_local" id="email_local" value="{if $_safePost.email_local!=''}{$_safePost.email_local}{else}{lng p="wishaddress"}{/if}" size="20" onblur="if (this.value=='') this.value='{lng p="wishaddress"}'; checkAddressAvailability()" onfocus="if (this.value=='{lng p="wishaddress"}') this.value='';">
		{if $templatePrefs.domainDisplay!='normal'}<strong> @ </strong>{/if}
		<select name="email_domain" id="email_domain" onblur="checkAddressAvailability()">
			{foreach from=$domainList item=domain}<option value="{$domain}"{if $_safePost.email_domain==$domain} selected="selected"{/if}>{if $templatePrefs.domainDisplay=='normal'}@{/if}{$domain}</option>{/foreach}
		</select>
	</div>
	<div class="formStatus" id="addressAvailabilityIndicator">&nbsp;</div>
	<!-- address -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="contactinfo"}</h3></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" type="text" name="firstname" id="firstname" value="{if $_safePost.firstname!=''}{$_safePost.firstname}{else}Max{/if}" onblur="if (this.value=='') this.value='{lng p="firstname"}'" onfocus="if (this.value=='{lng p="firstname"}') this.value='';"></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" type="text" name="surname" id="surname" value="{if $_safePost.surname!=''}{$_safePost.surname}{else}Mustermann{/if}" onblur="if (this.value=='') this.value='{lng p="surname"}'" onfocus="if (this.value=='{lng p="surname"}') this.value='';"></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" type="text" name="mail2sms_nummer" id="mail2sms_nummer" value="{if $_mail2sms_nummer!=''}{$_mail2sms_nummer}{else}{lng p="mobile"}{/if}" onblur="if (this.value=='') this.value='{lng p="mobile"}'" onfocus="if (this.value=='{lng p="mobile"}') this.value='';"></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" type="text" name="altmail" id="altmail" value="{if $_safePost.altmail!=''}{$_safePost.altmail}{else}{lng p="altmail"}{/if}" onblur="if (this.value=='') this.value='{lng p="altmail"}'" onfocus="if (this.value=='{lng p="altmail"}') this.value='';"></div>
	<!-- password -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="password"}</h3></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" autocomplete="off" type="password" name="pass1" id="pass1" value="**********" onblur="if (this.value=='') this.value='**********'" onfocus="if (this.value=='**********') this.value='';">{hook id="password.security:steps"}</div>
	<div class="aikq_regdiv"><input class="aikq_reginput" autocomplete="off" type="password" name="pass2" id="pass2" value="**********" onblur="if (this.value=='') this.value='**********'" onfocus="if (this.value=='**********') this.value='';"></div>
	
	<div class="aikq_regdiv">
<div id="password-security-holder">
<div id="password-security-bar"></div>
</div>
</div>
	
	{if $code}
	<!-- code -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="code"}</h3></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" type="text" name="code" id="code" value="{if $_safePost.code!=''}{$_safePost.code}{else}{lng p="code"}{/if}" onblur="if (this.value=='') this.value='{lng p="code"}'" onfocus="if (this.value=='{lng p="code"}') this.value='';"></div>
	<div class="formStatus" id="codeValidity">&nbsp;</div>
	{/if}
	{if $f_safecode!='n'}
	<!-- safecode -->
	<div class="aikq_regdiv"><img src="index.php?action=codegen&id={$codeID}" border="0" alt="" style="cursor:pointer;" onclick="this.src='index.php?action=codegen&id={$codeID}&rand='+parseInt(Math.random()*10000);" /></div>
	<div class="aikq_regdiv"><input class="aikq_reginput" maxlength="6" style="text-align:center;" type="text" name="safecode" id="safecode" value="{lng p="safecode"}" onblur="if (this.value=='') this.value='{lng p="safecode"}'" onfocus="if (this.value=='{lng p="safecode"}') this.value='';"></div>
	<div class="aikq_regdiv"><small>{lng p="notreadable"}</small></div>
	{/if}
	<!-- terms of service -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="tos"}</h3></div>
	<div class="aikq_regdiv"><textarea style="width: 75%; height: 120px;" readonly="readonly">{$tos}</textarea></div>
	<div>
			<br />
			<input type="radio"{if $_safePost.tos=='true'} checked="checked"{/if} name="tos" value="true" id="tos_acc" /> <label for="tos_acc">{lng p="tosaccept"}</label>
			<input type="radio"{if $_safePost.tos!='true'} checked="checked"{/if} name="tos" value="false" id="tos_nacc" /> <label for="tos_nacc">{lng p="tosnaccept"}</label>
	</div>
	<!-- submit -->
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="submit"}</h3></div>
	<div class="aikq_regdiv"><input type="image" src="{$tpldir}images/aikq/aikq_reg_button.png" alt="Verbindlich registrieren" /></div>
	<div><br><br><br></div>
	</form>
</div>

{if $errorStep}
<script language="javascript">
<!--
{foreach from=$invalidFields item=field}
	markFieldAsInvalid('{$field}');
{/foreach}
{if $f_safecode!='n'}
	markFieldAsInvalid('safecode');
{/if}
	markFieldAsInvalid('pass1');
	markFieldAsInvalid('pass2');
//-->
</script>
{/if}