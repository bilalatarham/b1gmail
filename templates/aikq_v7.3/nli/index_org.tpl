<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>{$service_title}{if $pageTitle} - {text value=$pageTitle}{/if}</title>
	
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	{if $robotsNoIndex}<meta name="robots" content="noindex" />{/if}

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{$tpldir}style/notloggedin.css?{fileDateSig file="style/notloggedin.css"}" />
{foreach from=$_cssFiles.nli item=_file}	<link rel="stylesheet" type="text/css" href="{$_file}" />
{/foreach}

	<!-- client scripts -->
	<script language="javascript" type="text/javascript">
	<!--
		var tplDir = '{$tpldir}', serverTZ = {$serverTZ};
	//-->
	</script>
	<script src="clientlang.php" language="javascript" type="text/javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" language="javascript" type="text/javascript"></script>
	<script src="{$tpldir}js/notloggedin.js?{fileDateSig file="js/notloggedin.js"}" language="javascript" type="text/javascript"></script>
	<script src="clientlib/md5.js" language="javascript" type="text/javascript"></script>
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="clientlib/pngfix.js"></script>
	<![endif]-->
{foreach from=$_jsFiles.nli item=_file}	<script type="text/javascript" src="{$_file}"></script>
{/foreach}
</head>

<!-- body -->
<body onload="documentLoader()">

	<center>{banner}</center>
	
	<div id="main">
		<div id="head">
			<img src="{$tpldir}images/main/logo.gif" border="0" alt="" width="48" height="48" align="absmiddle" />
			{$service_title}
		</div>
	
		<div id="nav">
			<ul>
				<li{if $smarty.request.action!='signup'&&$smarty.request.action!='faq'} class="active"{/if}><a href="index.php">
					<img src="{$tpldir}images/main/login.gif" width="20" height="20" align="absmiddle" border="0" />
					{lng p="welcome"}
				</a></li>
				{if $_regEnabled||(!$templatePrefs.hideSignup)}<li{if $smarty.request.action=='signup'} class="active"{/if}><a href="index.php?action=signup">
					<img src="{$tpldir}images/main/signup.gif" width="20" height="20" align="absmiddle" border="0" />
					{lng p="signup"}
				</a></li>{/if}
				<li{if $smarty.request.action=='faq'} class="active"{/if}><a href="index.php?action=faq">
					<img src="{$tpldir}images/main/faq.gif" width="20" height="20" align="absmiddle" border="0" />
					{lng p="faq"}
				</a></li>
			</ul>
		</div>
		
		<div id="content">
			{include file="$page"}
		</div>
		
		<div id="footNav">
			<div style="float:left">
				<select class="smallInput" onchange="document.location.href='index.php?action=switchLanguage&amp;lang='+this.value;">
				{foreach from=$languageList key=langKey item=langInfo}
				<option value="{$langKey}"{if $langInfo.active} selected="selected"{/if}>{$langInfo.title}</option>
				{/foreach}
				</select>
			</div>
			
			<div style="float:right">
				<a href="index.php?action=tos">{lng p="tos"}</a>
			{foreach from=$pluginUserPages item=item}
			|	<a href="{$item.link}">{$item.text}</a>
			{/foreach}
			|	<a href="{$mobileURL}">{lng p="mobilepda"}</a>
			|	<a href="index.php?action=imprint">{lng p="imprint"}</a>
			</div>
			
			<div id="copyright" style="clear:both;">
				<br />{literal}&nbsp;{/literal}
			</div>
		</div>
	</div>

</body>

</html>
