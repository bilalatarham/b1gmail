<form action="index.php?action=login" method="post">
<input type="hidden" name="do" value="login" />
<input type="hidden" name="email_full" value="{text value=$email}" />
<input type="hidden" name="passwordMD5" value="{text value=$password}" />
<input type="hidden" name="language" value="{text value=$language}" />
{if $savelogin}<input type="hidden" name="savelogin" value="true" />{/if}
			
<table class="nliTable">
	<!-- validation code input -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/login.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="smsvalidation"}</h3>
			
			<p>
				{lng p="smsvalidation_text"}
			</p>
			
			<table>
				<tr>
					<td class="formCaption"><label for="sms_validation_code">{lng p="validationcode"}:</label></td>
					<td class="formField">
						<input type="text" name="sms_validation_code" id="sms_validation_code" size="25" />
					</td>
				</tr>
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">
						<input type="submit" value=" &nbsp; {lng p="ok"} &nbsp; " />
					</td>
				</tr>
			</table>
		
			<br />
		</td>
	</tr>
	
	{if $enableResend}
	<!-- resend code -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/password.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="didnotgetcode"}</h3>
			
			<p>
				{$resendText}
			</p>
			
			{if $allowResend}
			<p>
				<input type="submit" value=" &nbsp; {lng p="resendcode"} &nbsp; " name="resendCode" onclick="EBID('sms_validation_code').value='';" />
			</p>
			{/if}
		</td>
	</tr>
	{/if}
</table>

{if $smarty.post.sms_validation_code}
<script language="javascript">
<!--
	markFieldAsInvalid('sms_validation_code');
//-->
</script>
{/if}

</form>
