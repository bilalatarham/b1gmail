<div class="aikq_reg_left">
	<img class="home_regbutton" src="{$tpldir}images/aikq/reg_pic_left.png">
</div>
<div class="aikq_reg_right">
	<div class="aikq_reg_h3"><h3 class="aikq_h3">Passwort Zur&uuml;cksetzen</h3></div>
	<div class="aikq_div">
			
			<form id="pw_lost" action="index.php?action=lostPassword" method="post">
			
			<table>
				<tr>
					<td colspan="2" class="formField" style="padding:0px 0px 0px 0px;">
						<div align="justify">
						Wenn Sie Probleme beim Zur&uuml;cksetzen haben, klicken Sie einfach links auf "Fragen Sie uns", oder verwenden das Kontaktformular"<br /><br />Passwort an die alternative E-Mail-Adresse senden...<br /><br />
						</div>
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="email_local_pw">Ihre aikQ Mailadresse:</label></td>
					<td class="formField">
						<input type="text" name="email_local" size="30" id="email_local" />
						<select name="email_domain">
							{foreach from=$domainList item=domain}<option value="{$domain}">{if $templatePrefs.domainDisplay=='normal'}@{/if}{$domain}</option>{/foreach}
						</select>

					</td>
				</tr>
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">

						<small><a href="javascript:document.getElementById('pw_lost').submit();"><b>{lng p="requestpw"}?</b></a><input type="submit" value="&nbsp;" style="width:1px; height:1px; border:0px; background:transparent;" /></small>
						<!--<input type="submit" value=" &nbsp; {lng p="requestpw"} &nbsp; " />-->
					</td>
				</tr>
			</table>
			</form>
	</div>
</div>
<br>
<div class="aikq_reg_right">
	<div class="aikq_reg_h3"><h3 class="aikq_h3"></h3></div>
	<div class="aikq_div">
	{if $smspw}

					
			<h3></h3>
			
			<form id="smspwlost" action="index.php?action=lostpw&do=sendsmspw" method="post">
			
			<table>
				<tr>
					<td colspan="2" class="formField" style="padding:0px 0px 0px 0px;">
						<div align="justify">
							{$smsmsgtxt}<br /><br />
						</div>
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="sms-nummer">{lng p="smspwnr"}:</label></td>
					<td class="formField">
						<input type="text" name="smspwnr" size="30" id="smspwnr" />
					</td>
				</tr>
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">
						<small><a href="javascript:document.getElementById('smspwlost').submit();"><b>{lng p="requestpw"}?</b></a><input type="submit" value="&nbsp;" style="width:1px; height:1px; border:0px; background:transparent;" /></small>
					</td>
				</tr>
			</table>
			
			</form>
		
			<br />


		{if $msgok}
			<div style="color:#0000FF; text-align:center; line-height:1.3em; width:95%; border: 1px solid #0000FF; padding:3px 5px 3px 5px;">{$msgok}</div>
		{elseif $msgerror}
			<div style="color:#FF0000; text-align:center; line-height:1.3em; width:95%; border: 1px solid #FF0000; padding:3px 3px 3px 3px;"">{$msgerror}</div>
		{/if}

	{/if}


</div>
</div>
<div class="cleaner">
	<br><br><br>
</div>


{if $invalidFields}
<script language="javascript">
<!--
{foreach from=$invalidFields item=field}
	markFieldAsInvalid('{$field}');
{/foreach}
//-->
</script>
{/if}


