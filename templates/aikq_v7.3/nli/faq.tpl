<div class="aikq_reg_left">
	<img class="home_regbutton" src="{$tpldir}images/aikq/faq_pic_left.png">
</div>
<div class="aikq_reg_right">
	<div class="aikq_reg_h3"><h3 class="aikq_h3">{lng p="faq"}</h3></div>
	<div class="aikq_div">
		{foreach from=$faq key=id item=item}
			<div class="faqQuestion" onclick="toggleFAQItem({$id})">
				&nbsp;
				<img id="faqAnswerImage_{$id}" src="{$tpldir}images/expand.gif" border="0" alt="" class="faqExpand" />
				{$item.question}
			</div>
			<div class="faqAnswer" id="faqAnswer_{$id}" style="display:none;">
				<div style="padding:5px;">
					{$item.answer}
				</div>
			</div>
			<br />
		{/foreach}	
	</div>
</div>
<div class="cleaner">
	<br><br><br>
</div>