<html>
<head>
{if IsMobileUserAgent()} 
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
{/if}
	<title>{$service_title}: {lng p="error"}</title>
	<style>
	<!--
		{literal}*			{ font-family: tahoma, arial, verdana; font-size: 12px; }
		H1			{ font-size: 16px; font-weight: bold; border-bottom: 1px solid #DDDDDD; }
		H2			{ font-size: 14px; font-weight: normal; }
		.addInfo	{ font-family: courier, courier new; font-size: 10px; height: 100px; overflow: auto;
						border: 1px solid #DDDDDD; padding: 5px; }{/literal}
		{if IsMobileUserAgent()}
			{literal}
				input {
					-webkit-appearance: none;
					font-weight: bold;
					font-size: 16px;
					border: 1px solid #AAAAAA;
					padding: 10px 20px !important;
					color: #333333;
					text-decoration: none;
					background: #DDDDDD;
					-moz-border-radius: 2px;
					-webkit-border-radius: 2px;
					border-radius: 2px;
					background: #ffffff;	
				}
			{/literal}
		{/if}
	//-->
	</style>
</head>
<body bgcolor="#F1F2F6">
	
	<br /><br />
	<center>
		<table {if !IsMobileUserAgent()}width="600"{/if} cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">

			<tr>
				<td width="14" height="14"><img src="./res/msg_edge_left_top.png" border="0" alt="" /></td>
				<td height="14" background="./res/msg_top.png"><img src="./res/msg_top.png" border="0" alt="" /></td>
				<td width="14" height="14"><img src="./res/msg_edge_right_top.png" border="0" alt="" /></td>
			</tr>
			<tr>
				<td width="14" background="./res/msg_left.png"><img src="./res/msg_left.png" border="0" alt="" /></td>
				<td>
					<div style="padding: 10px;">

					<table width="100%">
						<tr>
							<td align="center" width="80" valign="top"><img src="./res/error_icon.png" border="0" alt="" /></td>
							<td valign="top" align="left">
							
								<h1>{$title}</h1>
								<h2>{$description}</h2>
								
								<hr size="1" color="#DDDDDD" width="100%" noshade="noshade" />
								<input type="button" value="&nbsp; {lng p="start"} &nbsp;" onclick="document.location.href='./';" style="padding: 1px;" />
								
							</td>
						</tr>
					</table>

					</div>
				</td>
				<td width="14" background="./res/msg_right.png"><img src="./res/msg_right.png" border="0" alt="" /></td>
			</tr>
			<tr>
				<td width="14" height="14"><img src="./res/msg_edge_left_bottom.png" border="0" alt="" /></td>
				<td height="14" background="./res/msg_bottom.png"><img src="./res/msg_bottom.png" border="0" alt="" /></td>
				<td width="14" height="14"><img src="./res/msg_edge_right_bottom.png" border="0" alt="" /></td>
			</tr>

		</table>
	</center>

</body>
</html>	