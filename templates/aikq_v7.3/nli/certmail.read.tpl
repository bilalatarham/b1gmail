<table class="nliTable">
	<!-- contact completion -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/mail.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{text value=$subject cut=45}</h3>
			
			<table>
				<tr>
					<td class="formCaption">{lng p="from"}:</td>
					<td class="formField">
						{addressList list=$fromAddresses simple=true}
					</td>
				</tr>
				<tr>
					<td class="formCaption">{lng p="subject"}:</td>
					<td class="formField">
						{text value=$subject}
					</td>
				</tr>
				<tr>
					<td class="formCaption">{lng p="date"}:</td>
					<td class="formField">
						{date timestamp=$date elapsed=true}
					</td>
				</tr>
				<tr>
					<td class="formCaption">{lng p="to"}:</td>
					<td class="formField">
						{addressList list=$toAddresses simple=true}
					</td>
				</tr>
				{if $ccAddresses}<tr>
					<td class="formCaption">{lng p="cc"}:</td>
					<td class="formField">
						{addressList list=$ccAddresses simple=true}
					</td>
				</tr>{/if}
				{if $replyToAddresses}<tr>
					<td class="formCaption">{lng p="replyto"}:</td>
					<td class="formField">
						{addressList list=$replyToAddresses simple=true}
					</td>
				</tr>{/if}
				{if $priority!=0}<tr>
					<td class="formCaption">{lng p="priority"}:</td>
					<td class="formField">
						<img src="{$tpldir}images/li/mailico_{if $priority==-1}low{elseif $priority==1}high{/if}.gif" border="0" alt="" align="absmiddle" />
						{lng p="prio_$priority"}
					</td>
				</tr>{/if}
			</table>
			
			<br />
			<iframe width="100%" style="background-color: #FFFFFF;height:250px;border: 1px solid #DDDDDD;" src="index.php?action=readCertMail&id={$mailID}&key={$key}&showText=true" frameborder="no"></iframe>
		
			{if $attachments}
			<table>
				<tr>
					<td class="formCaption" valign="top">{lng p="attachments"}:</td>
					<td class="formField">
						<table cellpadding="0">
						{foreach from=$attachments item=attachment key=attID}
						<tr>
							<td><img src="{$tpldir}images/main/attachment.gif" border="0" alt="" align="absmiddle" /></td>
							<td><a href="index.php?action=readCertMail&id={$mailID}&key={$key}&downloadAttachment=true&attachment={$attID}">{text value=$attachment.filename cut=45}</a></td>
						</tr>
						{/foreach}
						</table>
					</td>
				</tr>
			</table>
			{/if}
		</td>
	</tr>
</table>
