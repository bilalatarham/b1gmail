<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>{$service_title}{if $pageTitle} - {text value=$pageTitle}{/if}</title>
	
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	{if $robotsNoIndex}<meta name="robots" content="noindex" />{/if}

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
	<!--link rel="stylesheet" type="text/css" href="{$tpldir}style/notloggedin.css?{fileDateSig file="style/notloggedin.css"}" />-->
	<link rel="stylesheet" type="text/css" href="{$tpldir}style/notloggedin_aikq.css?{fileDateSig file="style/notloggedin_aikq.css"}" />
{foreach from=$_cssFiles.nli item=_file}	<link rel="stylesheet" type="text/css" href="{$_file}" />
{/foreach}

	<!-- client scripts -->
	<script language="javascript" type="text/javascript">
	<!--
		var tplDir = '{$tpldir}', serverTZ = {$serverTZ};
	//-->
	</script>
	<script src="clientlang.php" language="javascript" type="text/javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" language="javascript" type="text/javascript"></script>
	<script src="{$tpldir}js/notloggedin.js?{fileDateSig file="js/notloggedin.js"}" language="javascript" type="text/javascript"></script>
	<script src="clientlib/md5.js" language="javascript" type="text/javascript"></script>
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="clientlib/pngfix.js"></script>
	<![endif]-->
{foreach from=$_jsFiles.nli item=_file}	<script type="text/javascript" src="{$_file}"></script>
{/foreach}
</head>

<!-- body -->
<body onload="documentLoader()">

	<div class="wrapper">
		<div class="header">
			<div class="header_txt"><a href="index.php">Home</a> | 
			{if $_regEnabled||(!$templatePrefs.hideSignup)}{if $smarty.request.action=='signup'}{/if}<a href="index.php?action=signup">{lng p="signup"}</a>{/if} | 
			{foreach from=$pluginUserPages item=item}
			<a href="{$item.link}">{$item.text}</a> |
			{/foreach}
			{if $smarty.request.action=='faq'}{/if}<a href="index.php?action=faq">{lng p="faq"}</a> |
			<a href="index.php?action=tos">{lng p="tos"}</a> |
			<a href="index.php?action=imprint">{lng p="imprint"}</a>
			</div>
			<div class="cleaner"></div>
		</div>
		<div class="content">
			{include file="$page"}
		</div>
	</div>
{literal}
<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
<style type="text/css" media="screen, projection">
  @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
</style>
<script type="text/javascript">
  if (typeof(Zenbox) !== "undefined") {
    Zenbox.init({
      dropboxID:   "20304601",
      url:         "https://aikq.zendesk.com",
      tabTooltip:  "Fragen Sie uns",
      tabImageURL: "https://p3.zdassets.com/external/zenbox/images/tab_de_ask_us.png",
      tabColor:    "#78A300",
      tabPosition: "Left"
    });
  }
</script>
{/literal}
</body>

</html>
