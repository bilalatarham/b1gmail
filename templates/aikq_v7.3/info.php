<?php 
/*
 * b1gMail7
 * (c) 2002-2013 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: info.php,v 1.6 2013/05/19 19:07:26 patrick Exp $
 *
 */
 /* File changed by Martin Buchalik */
 
 /* Add a few language variables - we don't want to override the lang file or add an extra and unnecessary plugin --> it is done here */
global $currentLanguage;
if ($currentLanguage == "deutsch") {
	$lang_admin['fa_url'] = "URL zu FontAwesome";
	$lang_admin['proflat_headertitle'] = "Text in der oberen Leiste (leer lassen, um Seiten-Titel anzuzeigen)";
	$lang_admin['proflat_topbg'] = "Hintergrund-Farbe der oberen Leiste (Hex-Wert)";
	$lang_admin['proflat_topcolor'] = "Schriftfarbe der oberen Leiste (Hex-Wert)";
	$lang_admin['proflat_leftbg'] = "Hintergrund-Farbe des linken Menüs (Hex-Wert)";
	$lang_admin['proflat_leftcolor'] = "Schriftfarbe des linken Menüs (Hex-Wert)";
	$lang_admin['proflat_mainbg'] = "Hintergrund-Farbe oder -Bild des Hauptinhalts (Hex-Wert oder Bild-Pfad ausgehend vom /images/ - Ordner im Template)";
	$lang_admin['proflat_widgetBG1'] = "Fallback-Farbe für alle Widgets, die nicht in der Liste enthalten sind";
	$lang_admin['proflat_widgetBG2'] = "Farbe für alle Widgets vom Typ 1 (siehe Dokumentation)";
	$lang_admin['proflat_widgetBG3'] = "Farbe für alle Widgets vom Typ 2 (siehe Dokumentation)";
	$lang_admin['proflat_widgetBG4'] = "Farbe für alle Widgets vom Typ 3 (siehe Dokumentation)";
} else {
	$lang_admin['fa_url'] = "URL for FontAwesome";
	$lang_admin['proflat_headertitle'] = "Text of the top bar (leave empty to display page title)";
	$lang_admin['proflat_topbg'] = "Background color of the top bar (Hex-value)";
	$lang_admin['proflat_topcolor'] = "Color of the top bar font (Hex-value)";
	$lang_admin['proflat_leftbg'] = "Background color of the left menu (Hex-value)";
	$lang_admin['proflat_leftcolor'] = "Color of the left menu font (Hex-value)";
	$lang_admin['proflat_mainbg'] = "Background color oder image of the main content (Hex-value or image path starting in the /images/ - folder of this template)";
	$lang_admin['proflat_widgetBG1'] = "Fallback color for all widgets not included in the list";
	$lang_admin['proflat_widgetBG2'] = "Color for all widgets of type 1 (view documentation for more information)";
	$lang_admin['proflat_widgetBG3'] = "Color for all widgets of type 2 (view documentation for more information)";
	$lang_admin['proflat_widgetBG4'] = "Color for all widgets of type 3 (view documentation for more information)";
}
$templateInfo = array(
	'title'			=> 'pro-flat - Template',
	'author'		=> 'Martin Buchalik',
	'website'		=> 'http://martin-buchalik.de',
	'for_b1gmail'	=> B1GMAIL_VERSION,
	
	'prefs'	=> array(
		'prefsLayout'	=> array(
			'title'		=> $lang_admin['prefslayout'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array('onecolumn'		=> $lang_admin['onecolumn'],
									'twocolumns'	=> $lang_admin['twocolumns']),
			'default'	=> 'onecolumn'
		),
		'domainDisplay'	=> array(
			'title'		=> $lang_admin['domaindisplay'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options' 	=> array('normal'			=> $lang_admin['ddisplay_normal'],
									'separateat'	=> $lang_admin['ddisplay_separate']),
			'default'	=> 'normal'
		),
		'hideSignup'	=> array(
			'title'		=> $lang_admin['hidesignup'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'showUserEmail'	=> array(
			'title'		=> $lang_admin['showuseremail'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'showCheckboxes'=> array(
			'title'		=> $lang_admin['showcheckboxes'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'faUrl'=> array(
			'title'		=> $lang_admin['fa_url'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'
		),
		'liHeaderTitle'=> array(
			'title'		=> $lang_admin['proflat_headertitle'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> ''
		),
		'topbarBG'=> array(
			'title'		=> $lang_admin['proflat_topbg'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#222222'
		),
		'topbarColor'=> array(
			'title'		=> $lang_admin['proflat_topcolor'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#e0e0e0'
		),
		'mainmenuBG'=> array(
			'title'		=> $lang_admin['proflat_leftbg'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#2b3a48'
		),
		'mainmenuColor'=> array(
			'title'		=> $lang_admin['proflat_leftcolor'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#c0cfdd'
		),
		'maincontentBG'=> array(
			'title'		=> $lang_admin['proflat_mainbg'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#ffffff'
		),
		'widgetBG1'=> array(
			'title'		=> $lang_admin['proflat_widgetBG1'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#4A8BC1'
		),
		'widgetBG2'=> array(
			'title'		=> $lang_admin['proflat_widgetBG2'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#DE567A'
		),
		'widgetBG3'=> array(
			'title'		=> $lang_admin['proflat_widgetBG3'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#00A489'
		),
		'widgetBG4'=> array(
			'title'		=> $lang_admin['proflat_widgetBG4'].":",
			'type'		=> FIELD_TEXT,
			'default'	=> '#9D4A9C'
		),
	)
);
?>