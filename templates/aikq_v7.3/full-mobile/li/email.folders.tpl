<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="folderadmin"}
	</div>
</div>

<form name="f1" method="post" action="email.folders.php?action=action&sid={$sid}">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<thead>
	<tr>
		<th class="listTableHead">
			<a href="email.folders.php?sid={$sid}&sort=titel&order={$sortOrderInv}">{lng p="title"}</a>
			{if $sortColumn=='titel'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>		
		<th width="90">&nbsp;</th>
	</tr>
	</thead>
	
	{if $sysFolderList}
	<tr>
		<td colspan="2" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('sys');">&nbsp;<img id="groupImage_sys" src="{$tpldir}images/contract.gif" border="0" align="absmiddle" alt="" />
			&nbsp;{lng p="sysfolders"}</a>
		</td>
	</tr>
	<tbody id="group_sys" style="display:;">
	{foreach from=$sysFolderList key=folderID item=folder}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}" nowrap="nowrap">&nbsp;<a href="email.php?sid={$sid}&folder={$folderID}"><img src="{$tpldir}images/li/menu_ico_{$folder.type}.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$folder.titel cut=25}</a></td>
		
		<td class="{$class}" nowrap="nowrap">
			<a href="email.folders.php?action=editFolder&id={$folderID}&sid={$sid}">{if IsMobileUserAgent()}<i class="fa fa-pencil fa-2x fa-fw"></i>{else}<img src="{$tpldir}images/li/ico_edit.png" width="16" height="16" border="0" alt="{lng p="edit"}" align="absmiddle" />{/if}</a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
	
	{if $theFolderList}
	<tr>
		<td colspan="2" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('own');">&nbsp;<img id="groupImage_own" src="{$tpldir}images/contract.gif" border="0" align="absmiddle" alt="" />
			&nbsp;{lng p="ownfolders"}</a>
		</td>
	</tr>
	<tbody id="group_own" style="display:;">
	{foreach from=$theFolderList key=folderID item=folder}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{if $sortColumn=='titel'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;<a href="email.php?sid={$sid}&folder={$folderID}"><img src="{$tpldir}images/li/menu_ico_{if $folder.intelligent==1}intelli{/if}folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$folder.titel cut=25}</a></td>
		
		<td class="{$class}" nowrap="nowrap">
			<a href="email.folders.php?action=editFolder&id={$folderID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="email.folders.php?action=deleteFolder&id={$folderID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
</table>

</div>

</form>
