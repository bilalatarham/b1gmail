<div id="hSep1">
	<div>
		
		<div id="contentHeader">
			<div class="left">
				<img src="{$tpldir}images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				{lng p="notes"}
			</div>
		</div>
	
		<form name="f1" method="post" action="organizer.notes.php?action=action&sid={$sid}">
		<div class="scrollContainer withBottomBar">
			<table class="bigTable">
				<tr>					
					<th width="80">
						<a href="organizer.notes.php?sid={$sid}&sort=priority&order={$sortOrderInv}">{lng p="priority"}</a>
						{if $sortColumn=='priority'}<i class="fa fa-angle-double-{if $sortOrder == 'asc'}up{else}down{/if}"></i>{/if}
					</th>
					<th width="150">
						<a href="organizer.notes.php?sid={$sid}&sort=date&order={$sortOrderInv}">{lng p="date"}</a>
						{if $sortColumn=='date'}<i class="fa fa-angle-double-{if $sortOrder == 'asc'}up{else}down{/if}"></i>{/if}
					</th>
					<th>
						<a href="organizer.notes.php?sid={$sid}&sort=text&order={$sortOrderInv}">{lng p="text"}</a>
						{if $sortColumn=='text'}<i class="fa fa-angle-double-{if $sortOrder == 'asc'}up{else}down{/if}"></i>yy{/if}					
					</th>					
				</tr>
				
				{if $noteList}
				<tbody class="listTBody">
				{foreach from=$noteList key=noteID item=note}
				{cycle values="listTableTD,listTableTD2" assign="class"}
				{assign value=$note.priority var=prio}
				<tr onclick="noteOverview(1, '{$tpldir}', '{$noteID}')">					
					<td class="{if $sortColumn=='priority'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap"><img src="{$tpldir}images/li/prio_{if $note.priority==-1}low{elseif $note.priority==0}normal{else}high{/if}.gif" border="0" alt="" align="absmiddle" /> {lng p="prio_$prio"}</td>
					<td class="{if $sortColumn=='date'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;{date timestamp=$note.date nice=true}&nbsp;</td>
					<td class="{if $sortColumn=='text'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;<a href="javascript:previewNote('{$sid}', '{$noteID}');">{text value=$note.text}</a></td>
					
				</tr>
				{/foreach}
				</tbody>
				{/if}
			</table>
		</div>
		
		</form>
	</div>
</div>
<div id="hSepSep" style="display: none;"></div>
<div id="hSep2" style="width: 0;">
	<div class="notePreview"> 
    <div class="contentHeader">
			<div class="left">
				<img src="{$tpldir}images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				{lng p="notes"}
			</div>
	</div>  	 
		<div id="notePreview">
        	<div id="multiSelPreview_vCenter"><i class="fa fa-refresh fa-spin fa-2x"></i></div>
        </div>        
    </div>
</div>
<input type="hidden" id="currentSelected" name="currentSelected" value="1" />
<script language="javascript">
<!--	
{if $showID}
	registerLoadAction('previewNote(\'{$sid}\', \'{$showID}\')');
{/if}
//-->
</script>
