<div id="contentHeader">
	<div class="left">
		{lng p="signatures"}
	</div>
</div>

<form name="f1" method="post" action="prefs.php?action=signatures&do=action&sid={$sid}">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<tr>
		<th>
			{lng p="title"}
			<img src="{$tpldir}images/li/asc.gif" border="0" alt="" align="absmiddle" />
		</th>
		<th width="90">&nbsp;</th>
	</tr>
	
	{if $signatureList}
	<tbody class="listTBody">
	{foreach from=$signatureList key=signatureID item=signature}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="listTableTDActive" nowrap="nowrap">&nbsp;<a href="prefs.php?action=signatures&do=edit&id={$signatureID}&sid={$sid}"><img src="{$tpldir}images/li/ico_signatures.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$signature.titel}</a></td>
		<td class="{$class}" nowrap="nowrap">
			<a href="prefs.php?action=signatures&do=edit&id={$signatureID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="prefs.php?action=signatures&do=delete&id={$signatureID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
</table>
</div>

</form>