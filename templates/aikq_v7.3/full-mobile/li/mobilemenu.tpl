<button id="menu-button" onclick='javascript:slidemenu("all"), dropmenu("close");'> <i class="fa fa-navicon"> </i> </button> <input id="mainmenu-state" type="checkbox" />

<div id="mobilemenu-right">

{if $smarty.request.action=='folder' && $pageNo}
<button onclick="window.location.href='email.compose.php?sid={$sid}'"><i class="fa fa-plus fa-fw"></i></button>


{elseif $pageContent == 'li/email.folders.tpl'}
<button onclick="document.location.href='email.folders.php?action=addFolder&sid={$sid}';" type="button"><i class="fa fa-plus-square-o fa-fw"></i></button>


{elseif $pageContent == 'li/email.folders.edit.tpl' || $pageContent == 'li/email.folders.editsys.tpl'}
<button onclick="document.location.href='email.folders.php?sid={$sid}';" type="button"><i class="fa fa-angle-left fa-fw"></i></button>

{elseif $smarty.request.action=='read'}
<button type="button" onclick="document.location.href='email.php?folder={$folderID}&sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>
<button type="button" onclick="{if $folderID==-5}if(confirm('{lng p="realdel"}')) {/if} document.location.href='email.php?sid={$sid}&do=deleteMail&id={$mailID}&folder={$folderID}';"><i class="fa fa-trash-o fa-fw"></i></button>
<button type="button" onclick="mailReply({$mailID},false);"><i class="fa fa-reply fa-fw"></i></button>


{elseif $smarty.request.action=='compose'}
<button type="button" id="sendButton" onclick="if(!checkComposeForm(document.forms.f1, {if $attCheck}true{else}false{/if}, '{lng p="att_keywords"}')) return(false); EBID('do').value='sendMail';editor.submit();checkSMIME('{if $codeID}checkSafeCode({$codeID});{else}document.forms.f1.submit();{/if}');"><i class="fa fa-check fa-fw"></i></button>

{elseif $viewMode && $smarty.request.action=='start'}<button type="button" onclick="document.location.href='organizer.calendar.php?action=addDate&date={$theDate}&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>

{elseif $addressList}
<button id="addressBack" type="button" onclick="addressOverview(2, '{$tpldir}')" style="display: none"><i class="fa fa-angle-left fa-fw"></i></button>
<button id="addressAdd" type="button" onclick="document.location.href='organizer.addressbook.php?action=addContact&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>
<button id="addressEdit" type="button" onclick="addressEdit('{$sid}')" style="display: none"><i class="fa fa-pencil fa-fw"></i></button>

{elseif $pageContent == 'li/organizer.notes.tpl'}
<button id="noteBack" type="button" onclick="noteOverview(2, '{$tpldir}')" style="display: none"><i class="fa fa-angle-left fa-fw"></i></button>
<button id="noteAdd" type="button" onclick="document.location.href='organizer.notes.php?action=addNote&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>
<button id="noteEdit" type="button" onclick="noteEdit('{$sid}')" style="display: none"><i class="fa fa-pencil fa-fw"></i></button>

{elseif $pageContent == 'li/organizer.todo.tpl'}
<button id="taskBack" type="button" onclick="taskOverview(2, '{$tpldir}')" style="display: none"><i class="fa fa-angle-left fa-fw"></i></button>
<button id="taskAdd" type="button" onclick="taskAdd('{$sid}')" style="display: none"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/organizer.addressbook.tpl'}
<button id="noteAdd" type="button" onclick="document.location.href='organizer.addressbook.php?action=addContact&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/organizer.addressbook.edit.tpl'}
<button id="taskBack" type="button" onclick="document.location.href='organizer.addressbook.php?sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>

{elseif $pageContent == 'li/organizer.calendar.groups.tpl'}
<button onclick="document.location.href='organizer.calendar.php?action=groups&do=addForm&sid={$sid}';" type="button"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/organizer.calendar.groups.edit.tpl'}
<button id="taskBack" type="button" onclick="document.location.href='organizer.calendar.php?action=groups&do=add&sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>

{elseif $pageMenuFile == 'li/webdisk.folderbar.tpl' && $smarty.request.action == 'folder'}
<button id="wdBack" type="button" onclick="fmWdBack()" style="display: none"><i class="fa fa-angle-left fa-fw"></i></button>
<button id="wdAdd" type="button" onclick="fmWdAdd()"><i class="fa fa-plus fa-fw"></i></button>

{elseif $allowAdd && $pageContent == 'li/prefs.aliases.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=aliases&do=add&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.aliases.add.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=aliases&sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.filters.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=filters&do=add&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.filters.add.tpl' || $pageContent == 'li/prefs.filters.edit.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=filters&sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.extpop3.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=extpop3&do=add&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.extpop3.edit.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=extpop3&sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.signatures.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=signatures&do=add&sid={$sid}';"><i class="fa fa-plus fa-fw"></i></button>

{elseif $pageContent == 'li/prefs.signatures.edit.tpl'}
<button type="button" onclick="document.location.href='prefs.php?action=signatures&sid={$sid}';"><i class="fa fa-angle-left fa-fw"></i></button>
{/if}

<button onclick='javascript:dropmenu("all"), slidemenu("close");'><i class="fa fa-ellipsis-v fa-fw"></i></button> <input id="dropmenu-state" type="checkbox" checked />



<div id="dropmenu">
	<div id="dropmenu-content">
    	<ul>
        	<li><a href="search.php?sid={$sid}"><i class="fa fa-search fa-fw"></i> {lng p="search2"}</a></li>
            <li class="lastli">
            <a href="javascript:void(0)" onclick="if(confirm('{lng p="logoutquestion"}')) document.location.href='start.php?sid={$sid}&action=logout';">
					 <i class="fa fa-sign-out fa-fw"></i> {lng p="logout"}
			</a>
            </li>
            
            {if $smarty.request.action=='folder' && $perPage} 
                 <li> <a href="javascript:void(0)" onclick="javascript:document.location.href='email.php?do=markAllAsRead&folder='+currentFolderID+'&sid={$sid}';"><i class="fa fa-eye fa-fw"></i> {lng p="markallasread"}</a> </li>
                 
                <li><a href="javascript:void(0)" onclick="javascript:document.location.href='email.php?do=markAllAsRead&unread=true&folder='+currentFolderID+'&sid={$sid}';"><i class="fa fa-envelope fa-fw"></i> {lng p="markallasunread"}</a> </li>
                
                                
                <li><a href="javascript:void(0)" onclick="javascript:document.location.href='email.php?do=downloadAll&folder='+currentFolderID+'&sid={$sid}';"><i class="fa fa-download fa-fw"></i> {lng p="downloadall"}</a></li>
                
                {if $folderInfo.type!='intellifolder'}
                <li><a href="javascript:void(0);" onclick="if(confirm('{lng p="realempty"}')) document.location.href='email.php?do=emptyFolder&folder='+currentFolderID+'&sid={$sid}';"><i class="fa fa-trash-o fa-fw"></i> {lng p="emptyfolder"}</a></li>
                {/if}
             
            
            {elseif $smarty.request.action=='read'}
            <li><a href="javascript:void(0)" onclick="mailReply({$mailID},true);"><i class="fa fa-reply-all fa-fw"></i> {lng p="replyall"}</a></li>
            <li><a href="javascript:void(0)" onclick="document.location.href='email.compose.php?sid={$sid}&forward={$mailID}';"><i class="fa fa-share fa-fw"></i> {lng p="forward"}</a></li>
            <li><a href="javascript:void(0)" onclick="document.location.href='email.compose.php?sid={$sid}&redirect={$mailID}';"><i class="fa fa-random fa-fw"></i> {lng p="redirect"}</a></li>
            <li><a href="javascript:void(0)" onclick="document.location.href='email.read.php?sid={$sid}&action=download&id={$mailID}';"><i class="fa fa-download fa-fw"></i> {lng p="download"}</a></li>
            <li><a href="javascript:void(0)" onclick="javascript:dropmenu('close'), moveMail('{$mailID}');"><i class="fa fa-toggle-right fa-fw"></i> {lng p="move"}</a></li>
              
            
            {elseif $smarty.request.action=='compose'}
            <li><a href="javascript:void(0)" onclick="javascript:dropmenu('close'), fmOpenAddressbook('email')"><i class="fa fa-bookmark-o fa-fw"></i> {lng p="fromaddr"}</a></li>
            <li><a href="javascript:void(0)" onclick="EBID('do').value='saveDraft';editor.submit();document.forms.f1.submit();"><i class="fa fa-clipboard fa-fw"></i> {lng p="savedraft"}</a></li>
            <li><a href="javascript:void(0)" onclick="javascript:dropmenu('close'), fmAddAttachment()"><i class="fa fa-cloud fa-fw"></i> {lng p="attachments"}: {lng p="add"}</a></li>
            <li><input type="checkbox" onchange="dropcheckbox('attachVCard-mobile')" id="attachVCard" {if $mail.attachVCard} checked="checked"{/if} /><label for="attachVCard">{lng p="attachvc"}</label></li>
            <li><input type="checkbox" name="certMail" onchange="dropcheckbox('certMail-mobile')" id="certMail"{if $mail.certMail} checked="checked"{/if} /><label for="certMail">{lng p="certmail"}</label></li>
            <li><input type="checkbox" onchange="dropcheckbox('mailConfirmation-mobile')" id="mailConfirmation"{if $mail.mailConfirmation} checked="checked"{/if} /><label for="mailConfirmation">{lng p="mailconfirmation"}</label></li>
            {if $smime}
            <li><input type="checkbox" onchange="dropcheckbox('smimeSign-mobile')" id="smimeSign"{if $mail.smimeSign} checked="checked"{/if} /><label for="smimeSign">{lng p="sign"}</label></li>
            <li><input type="checkbox" onchange="dropcheckbox('smimeEncrypt-mobile')" id="smimeEncrypt"{if $mail.smimeEncrypt} checked="checked"{/if} /><label for="smimeEncrypt">{lng p="encrypt"}</label></li>
            {/if}
            <li><div class="drop-select">{lng p="priority"}:</div>
            	<select name="priority" id="priority" onchange="dropselect('priority-mobile','priority')">
                <option value="1"{if $mail.priority==1} selected="selected"{/if}>{lng p="prio_1"}</option>
                <option value="0"{if !$mail || $mail.priority==0} selected="selected"{/if}>{lng p="prio_0"}</option>
                <option value="-1"{if $mail.priority==-1} selected="selected"{/if}>{lng p="prio_-1"}</option>
                </select>
            </li>
        
        
           
            
            {elseif $viewMode && $smarty.request.action=='start'}
            <li><a href="organizer.calendar.php?action=groups&sid={$sid}"><i class="fa fa-group fa-fw"></i> {lng p="editgroups"}</a></li>
               <form action="organizer.calendar.php?sid={$sid}" method="post">
                        <li>                       
                        <small>&nbsp; {lng p="viewmode"}: &nbsp;</small>
                        <select class="smallInput" onchange="updateCalendarViewMode(this, '{$theDate}', '{$sid}')">
                            <option value="day"{if $viewMode=="day"} selected="selected"{/if}>{lng p="day"}</option>
                            <option value="week"{if $viewMode=="week"} selected="selected"{/if}>{lng p="week"}</option>
                            <option value="month"{if $viewMode=="month"} selected="selected"{/if}>{lng p="month"}</option>
                        </select>
                        </li>
                        
                        <li>                 
                        <small>&nbsp; {lng p="group"}: &nbsp;</small>
                        <select class="smallInput" onchange="updateCalendarGroup(this, '{$theDate}', '{$sid}')">
                            <option value="-2"{if $theGroup==-2} selected="selected"{/if}>------------</option>
                            <option value="-1"{if $theGroup==-1} selected="selected"{/if}>{lng p="nocalcat"}</option>
                            <optgroup label="{lng p="groups"}">
                            {foreach from=$groups item=group}
                            {if $group.id>0}
                                <option value="{$group.id}"{if $theGroup==$group.id} selected="selected"{/if}>{text value=$group.title}</option>
                            {/if}
                            {/foreach}
                            </optgroup>
                        </select>
                        </li>
                        
                        <li><small>&nbsp; {lng p="date"}: &nbsp;</small>
                        
                        {if $viewMode=='day'}
                        <br />
                        <a href="organizer.calendar.php?sid={$sid}&date={$date-86400}"><img src="{$tpldir}images/li/btn_prev.png" width="16" height="16" border="0" alt="" align="absmiddle" /></a>
                        {html_select_date prefix="date_" time=$date start_year="-5" end_year="+5" field_order="DMY"}
                        
                        <a href="organizer.calendar.php?sid={$sid}&date={$date+86400}"><img src="{$tpldir}images/li/btn_next.png" width="16" height="16" border="0" alt="" align="absmiddle" /></a>
                        
                        {elseif $viewMode=='week'}
                        <a href="organizer.calendar.php?sid={$sid}&date={$prevWeek}"><img src="{$tpldir}images/li/btn_prev.png" width="16" height="16" border="0" alt="" align="absmiddle" /></a>
                        
                            <select name="date_Week">
                                {section name=w start=1 loop=53 step=1}
                                <option value="{$smarty.section.w.index}"{if $smarty.section.w.index==$calWeekNo} selected="selected"{/if}>{lng p="cw"} {$smarty.section.w.index}</option>
                                {/section}
                            </select>
                            {html_select_date prefix="date_" time=$date start_year="-5" end_year="+5" field_order="Y"}
                        
                        
                        <a href="organizer.calendar.php?sid={$sid}&date={$nextWeek}"><img src="{$tpldir}images/li/btn_next.png" width="16" height="16" border="0" alt="" align="absmiddle" /></a>
                        
                        {elseif $viewMode=='month'}
                        <a href="organizer.calendar.php?sid={$sid}&date={$prevMonth}"><img src="{$tpldir}images/li/btn_prev.png" width="16" height="16" border="0" alt="" align="absmiddle" /></a>
                        {html_select_date prefix="date_" time=$date display_days=false start_year="-5" end_year="+5" field_order="MY"}
                        
                        <a href="organizer.calendar.php?sid={$sid}&date={$nextMonth}"><img src="{$tpldir}images/li/btn_next.png" width="16" height="16" border="0" alt="" align="absmiddle" /></a>
                        
                        {/if}
                        <br />
                        <div style="padding-left: 75px">
                            <input type="submit"  value=" {lng p="today"} " name="jumpToday" />
                            <input type="submit" value=" {lng p="ok"} " />
                		</div>
                        </li>                  
                </form>
            
            {elseif $aliasUsage}<li>{$aliasUsage}</li>
			{elseif $pageContent == 'li/prefs.extpop3.tpl'}<li>{$accountUsage}</li>{/if}
			
        </ul>
    </div>
</div>
</div>