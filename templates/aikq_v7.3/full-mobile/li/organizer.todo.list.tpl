<div class="taskContainer withBottomBar taskList" style="overflow-y:scroll;overflow-x:auto;">
		
	<table class="bigTable" id="tasksTable">
	<tr style="height: auto;">		
		<th width="16">&nbsp;</th>
		<th>
			{lng p="title"}				
		</th>
		<th width="120">
			{lng p="due"}
		</th>
		<th class="listTableHead" width="90">&nbsp;</th>
	</tr>
	
	<tr style="height:auto;">
		<td colspan="4" class="folderGroup">
			<a style="display:block;cursor:pointer;" onclick="toggleGroup(0,'todo0');">&nbsp;<img id="groupImage_0" src="{$tpldir}images/{if $smarty.cookies.toggleGroup.todo0=='closed'}expand{else}contract{/if}.gif" border="0" align="absmiddle" alt="" />
			&nbsp;{lng p="undonetasks"}</a>
		</td>
	</tr>
	
	<tbody id="group_0" style="display:{if $smarty.cookies.toggleGroup.todo0=='closed'}none{/if};">

	{foreach from=$todoList key=taskID item=task}{if $task.akt_status!=64}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr id="task_{$taskID}">		
		<td class="{$class}" nowrap="nowrap">
			<img src="{$tpldir}images/li/mailico_{if $task.priority==-1}low{elseif $task.priority==0}empty{else}high{/if}.gif" border="0" alt="" align="absmiddle" />
		</td>
		<td class="{$class}" nowrap="nowrap">
			{text value=$task.titel}
		</td>
		<td class="{$class}" nowrap="nowrap">
			{date timestamp=$task.faellig nice=true}
		</td>
		<td class="{$class}" nowrap="nowrap">
			<a href="organizer.todo.php?action=editTask&id={$taskID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="organizer.todo.php?action=deleteTask&taskListID={$taskListID}&id={$taskID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{else}{assign value=true var=haveDoneTasks}{/if}{/foreach}
	
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr id="newTask">		
		<td class="{$class}">&nbsp;</td>
		<td class="{$class}">
			<input type="text" id="newTaskText" onkeypress="return newTaskKeyPress(event);" onfocus="_tasksSel.unselectAll();" />
		</td>
		<td class="{$class}">&nbsp;</td>
		<td class="{$class}">
			<input type="button" class="smallInput" value=" {lng p="ok"} " onclick="addTask()" />
		</td>
	</tr>
	
	</tbody>
	
	{if $haveDoneTasks}
	<tr style="height:auto;">
		<td colspan="4" class="folderGroup">
			<a style="display:block;cursor:pointer;" onclick="toggleGroup(1,'todo1');">&nbsp;<img id="groupImage_1" src="{$tpldir}images/{if $smarty.cookies.toggleGroup.todo1=='closed'}expand{else}contract{/if}.gif" border="0" align="absmiddle" alt="" />
			&nbsp;{lng p="donetasks"}</a>
		</td>
	</tr>
	
	<tbody id="group_1" style="display:{if $smarty.cookies.toggleGroup.todo1=='closed'}none{/if};">
		
	{foreach from=$todoList key=taskID item=task}
	{if $task.akt_status==64}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr id="task_{$taskID}" class="done">		
		<td class="{$class}" nowrap="nowrap">
			<img src="{$tpldir}images/li/mailico_{if $task.priority==-1}low{elseif $task.priority==0}empty{else}high{/if}.gif" border="0" alt="" align="absmiddle" />
		</td>
		<td class="{$class}" nowrap="nowrap">
			{text value=$task.titel}
		</td>
		<td class="{$class}" nowrap="nowrap">
			{date timestamp=$task.faellig nice=true}
		</td>
		<td class="{$class}" nowrap="nowrap">
			<a href="organizer.todo.php?action=editTask&id={$taskID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="organizer.todo.php?action=deleteTask&taskListID={$taskListID}&id={$taskID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{/if}
	{/foreach}
	</tbody>
	
	{/if}

	</table>
	
</div>

<div class="contentFooter">
<div class="left">

</div>
<div class="right">
	
</div>
</div>

<script language="javascript">
<!--
	currentTaskListID = {$taskListID};
	initTasksSel();	
//-->
</script>
