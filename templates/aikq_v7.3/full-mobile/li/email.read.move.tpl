<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{lng p="move"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/{if IsMobileUserAgent()}full-mobile{else}dialog{/if}.css" rel="stylesheet" type="text/css" />
	<link href="{$tpldir}style/dtree.css" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script language="javascript">
	<!--
		var tplDir = '{$tpldir}';
	//-->
	</script>
	<script src="clientlang.php" type="text/javascript" language="javascript"></script>
	<script src="clientlib/dtree.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js" type="text/javascript" language="javascript"></script>
</head>

<body onload="documentLoader()">

	<table width="100%">
		<tr>
			<td{if IsMobileUserAgent()} class="mobile-overlay-heading" {/if}>{lng p="movemailto"}:</td>
		</tr>
		<tr>
			<td align="center">
				<div class="foldersDiv"><div style="padding:5px;">
					<script language="javascript">
					<!--
						var d = new dTree('d');
					{foreach from=$folderList item=folder}
						d.add({$folder.i}, {$folder.parent}, '{text value=$folder.text escape=true noentities=true}', 'email.read.php?action=move&id={$mailID}&dest={$folder.id}&sid={$sid}', '{text value=$folder.text escape=true noentities=true}', '', '{$tpldir}images/li/menu_ico_{$folder.icon}.png', '{$tpldir}images/li/menu_ico_{$folder.icon}.png'); 
					{/foreach}
						document.write(d);
					//-->
					</script>
				</div></div>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input type="button" onclick="parent.hideOverlay()" value="{lng p="cancel"}" />
			</td>
		</tr>
	</table>
	
</body>

</html>
