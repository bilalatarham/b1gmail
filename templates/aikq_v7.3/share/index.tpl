<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>{$service_title} - {lng p="sharing"}{if $userMail} - {$userMail}{/if}</title>
	
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="https://www.aikq.de/templates/aikq_v7.3/style/share.css" />

	<!-- client scripts -->
	<script type="text/javascript" src="{$selfurl}clientlang.php" language="javascript"></script>
	<script type="text/javascript" src="{$selfurl}{$_tpldir}js/common.js" language="javascript"></script>
	<script type="text/javascript" src="{$selfurl}clientlib/overlay.js" language="javascript"></script>
	<script type="text/javascript" src="{$selfurl}clientlib/share.js" language="javascript"></script>
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="{$selfurl}clientlib/pngfix.js"></script>
	<![endif]-->
</head>

<!-- body -->
<body{if !$error} onload="shareInit('{$user}','{$selfurl}{$_tpldir}')"{/if}>

	<br />
	<center>

		<p>
			{banner}
		</p>
		
	{if $error}
		<div class="errorMessage">
			<p><b>{$title}</b></p>
			<p>{$msg}</p>
		</div>
	{else}
		<div id="mainLayer">
			<div id="toolBar">
                <div id="logo">
                <a href="../index.php" title="{$service_title}">
                <img src="https://www.aikq.de/templates/aikq_v7.3/images/main/logo.gif" border="0" alt="" width="30px" height="30px" align="absmiddle" />
                </a>
                </div>
				<div id="titleBar">
					<img id="titleIcon" src="{$selfurl}{$_tpldir}images/li/webdisk_folder.png" border="0" alt="" width="16" height="16" align="absmiddle" />
					<span id="titleLayer">...</span>
				</div>
			</div>
			<table id="headingTable" cellspacing="0" cellpadding="0">
				<tr>
					<th id="thTitle">{lng p="title"}</th>
					<th id="thModified">{lng p="modified"}</th>
					<th id="thSize">{lng p="size"}</th>
					<th id="thActions">&nbsp;</th>
				</tr>
			</table>
			<div id="contentLayer">
				<table cellspacing="0" cellpadding="0" id="contentTable"></table>
			</div>
			<div id="locationBar"></div>
		</div>
	{/if}
	
	</center>

</body>

</html>
