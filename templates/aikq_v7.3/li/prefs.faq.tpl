<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_faq.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="faq"}
	</div>
</div>

<div class="scrollContainer">
<table class="bigTable">
	<tr>
		<th>
			{lng p="question"}
		</th>
	</tr>
	
	{foreach from=$faq item=item}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}">&nbsp;<a href="javascript:toggleGroup({$item.id});"><img id="groupImage_{$item.id}" src="{$tpldir}images/expand.gif" border="0" alt="" align="absmiddle" /> <img src="{$tpldir}images/li/ico_faq.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {$item.frage}</a></td>
	</tr>
	<tbody id="group_{$item.id}" style="display:none;">
		<tr>
			<td class="listTableTDText">{$item.antwort}</td>
		</tr>
	</tbody>
	{/foreach}
</table>

</div>
