var webdisk_d = new dTree('webdisk_d');
{foreach from=$folderList item=folder}
var icon = 'fa-folder-open';
{if $folder.icon=="folder_shared"}
icon = 'fa-users';
{/if}
webdisk_d.add({$folder.i}, {$folder.parent}, '<i class="fa '+icon+' fa-fw"></i> {text value=$folder.text escape=true noentities=true}', 'javascript:switchWebdiskFolder({$folder.id});', '{text value=$folder.text escape=true noentities=true}', '', tplDir+'images/pixel.gif', tplDir+'images/pixel.gif'); 
{/foreach}
