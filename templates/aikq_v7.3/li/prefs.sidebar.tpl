<div class="sidebarHeading">{lng p="prefs"}</div>
<div class="contentMenuIcons">
	<a href="prefs.php?sid={$sid}"><i class="fa fa-home fa-fw"></i> {lng p="overview"}</a><br />
	{foreach from=$prefsItems item=null key=item}
	<a href="prefs.php?action={$item}&sid={$sid}">
	<i class="fa
	{if $item == "autoresponder"}
	fa-mail-reply
		{elseif $item == "userprefs_proflat"} 
	 	fa-eye 
{elseif $item == "userprefs_security"} fa-shield
	
	{elseif $item == "contact"}
	fa-newspaper-o
	{elseif $item == "aliases"}
	fa-group
	{elseif $item == "antispam"}
	fa-trash
	{elseif $item == "antivirus"}
	fa-trash-o
	{elseif $item == "common"}
	fa-cog
	{elseif $item == "coupons"}
	fa-qrcode
	{elseif $item == "extpop3"}
	fa-globe
	{elseif $item == "faq"}
	fa-question-circle
	{elseif $item == "filters"}
	fa-filter
	{elseif $item == "keyring"}
	fa-key
	{elseif $item == "membership"}
	fa-user
	{elseif $item == "orders"}
	fa-money
	{elseif $item == "pay"}
	fa-credit-card
	{elseif $item == "signatures"}
	fa-certificate
	{elseif $item == "smsoutbox"}
	fa-mobile
	{elseif $item == "software"}
	fa-desktop
	{elseif $item == "facebook"}
	fa-facebook
	{elseif $item == "twitter"}
	fa-twitter
	{elseif $item == "refcenter"}
	fa-users
	{elseif $item == "openid"}
	fa-openid
	{else}
	fa-circle-o
	{/if}
	fa-fw"></i>
	{lng p="$item"}</a><br />
	{/foreach}
	<a href="start.php?usertour_start&sid={$sid}"><i class="fa fa-puzzle-piece fa-fw"></i> {lng p="aikqassistant"}</a>
</div>
