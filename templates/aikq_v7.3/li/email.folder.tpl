{if $enablePreview}
	{if $narrow}
		<div id="hSep1">
			<div id="folderMailArea">
				{include file="li/email.folder.contents.narrow.tpl"}
			</div>
		</div>
		<div id="hSepSep"></div>
		<div id="hSep2">
			<div class="scrollContainer withoutContentHeader" style="display:none;" id="previewArea"></div>
	
			<div class="scrollContainer withoutContentHeader" id="multiSelPreview">
				<div id="multiSelPreview_vCenter">
					<div id="multiSelPreview_inner">
						<div id="multiSelPreview_count">{lng p="nomailsselected"}</div>
					</div>
				</div>
			</div>
		</div>
		
		<script language="javascript">
		<!--
			var folderNarrowView = true;
			registerLoadAction('initHSep()');
		//-->
		</script>
	{else}
		<div id="vSep1">
			<div id="folderMailArea">
				{include file="li/email.folder.contents.tpl"}
			</div>
		</div>
		<div id="vSepSep"></div>
		<div id="vSep2">
			<div class="scrollContainer withoutContentHeader" style="display:none;" id="previewArea"></div>
	
			<div class="scrollContainer withoutContentHeader" id="multiSelPreview">
				<div id="multiSelPreview_vCenter">
					<div id="multiSelPreview_inner">
						<div id="multiSelPreview_count">{lng p="nomailsselected"}</div>
					</div>
				</div>
			</div>
		</div>
		
		<script language="javascript">
		<!--
			var folderNarrowView = false;
			registerLoadAction('initVSep()');
		//-->
		</script>
	{/if}
{else}
<div id="folderMailArea">
	{include file="li/email.folder.contents.tpl"}
</div>
{/if}

<img src="{$tpldir}images/li/drag_email.png" style="display:none;" /><img src="{$tpldir}images/li/drag_emails.png" style="display:none;" />

<!-- mail menu -->
<div id="mailMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;" oncontextmenu="return(false);" onmousedown="if(event.button==2) return(false);">
	<a class="mailMenuItem" href="javascript:document.location.href='email.read.php?id='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="{$tpldir}images/li/mail_read.png" width="16" height="16" border="0" alt="" /> {lng p="mail_read"}</a>
	<a class="mailMenuItem" href="javascript:printMail(currentID, currentSID);"><img align="absmiddle" src="{$tpldir}images/li/mail_print.png" width="16" height="16" border="0" alt="" /> {lng p="print"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:document.location.href='email.compose.php?reply='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="{$tpldir}images/li/mail_reply.png" width="16" height="16" border="0" alt="" /> {lng p="reply"}</a>
	<a class="mailMenuItem" href="javascript:document.location.href='email.compose.php?forward='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="{$tpldir}images/li/mail_forward.png" width="16" height="16" border="0" alt="" /> {lng p="forward"}</a>
	<a class="mailMenuItem" href="javascript:document.location.href='email.compose.php?redirect='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="{$tpldir}images/li/mail_redirect.png" width="16" height="16" border="0" alt="" /> {lng p="redirect"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 1, false);"><img align="absmiddle" src="{$tpldir}images/li/mail_markread.png" width="16" height="16" border="0" alt="" /> {lng p="markread"}</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 1, true);"><img align="absmiddle" src="{$tpldir}images/li/mail_markunread.png" width="16" height="16" border="0" alt="" /> {lng p="markunread"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 16, true);"><img align="absmiddle" src="{$tpldir}images/li/mailico_flagged.gif" width="16" height="16" border="0" alt="" /> {lng p="mark"}</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 16, false);"><img align="absmiddle" src="{$tpldir}images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> {lng p="unmark"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 4096, true);"><img align="absmiddle" src="{$tpldir}images/li/mailico_done.gif" width="16" height="16" border="0" alt="" /> {lng p="markdone"}</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 4096, false);"><img align="absmiddle" src="{$tpldir}images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> {lng p="unmarkdone"}</a>
	<div class="mailMenuSep"></div>
	<div class="mailColorButtons">
		<span id="mailColorButton_0" onclick="javascript:folderColorMail(currentID, 0);"><img src="{$tpldir}images/pixel.gif" /></span>
		<span id="mailColorButton_1" onclick="javascript:folderColorMail(currentID, 1);"><img src="{$tpldir}images/pixel.gif" /></span>
		<span id="mailColorButton_2" onclick="javascript:folderColorMail(currentID, 2);"><img src="{$tpldir}images/pixel.gif" /></span>
		<span id="mailColorButton_3" onclick="javascript:folderColorMail(currentID, 3);"><img src="{$tpldir}images/pixel.gif" /></span>
		<span id="mailColorButton_4" onclick="javascript:folderColorMail(currentID, 4);"><img src="{$tpldir}images/pixel.gif" /></span>
		<span id="mailColorButton_5" onclick="javascript:folderColorMail(currentID, 5);"><img src="{$tpldir}images/pixel.gif" /></span>
		<span id="mailColorButton_6" onclick="javascript:folderColorMail(currentID, 6);"><img src="{$tpldir}images/pixel.gif" /></span>
	</div>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:showMailSource(currentID);"><img align="absmiddle" src="{$tpldir}images/li/mail_source.png" width="16" height="16" border="0" alt="" /> {lng p="showsource"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:if(confirm('{lng p="realdel"}')) deleteMail(currentID);"><img align="absmiddle" src="{$tpldir}images/li/mail_delete.png" width="16" height="16" border="0" alt="" /> {lng p="mail_del"}</a>
</div>

<!-- folder menu -->
<div id="folderMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;">
	<a class="mailMenuItem" href="javascript:document.location.href='email.php?do=markAllAsRead&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/mail_markread.png" width="16" height="16" border="0" alt="" /> {lng p="markallasread"}</a>
	<a class="mailMenuItem" href="javascript:document.location.href='email.php?do=markAllAsRead&unread=true&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/mail_markunread.png" width="16" height="16" border="0" alt="" /> {lng p="markallasunread"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:document.location.href='email.php?do=downloadAll&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/ico_download.png" width="16" height="16" border="0" alt="" /> {lng p="downloadall"}</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:void(0);" onclick="if(confirm('{lng p="realempty"}')) document.location.href='email.php?do=emptyFolder&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/folder_empty.png" width="16" height="16" border="0" alt="" /> {lng p="emptyfolder"}</a>
</div>

{hook id="email.folder.tpl:foot"}

<script language="javascript">
<!--
	var currentFolderID = {$folderID};
{if $refreshInterval>0}
	initFolderRefresh({$folderID}, {$refreshInterval});
{/if}
{if $hotkeys}
	registerLoadAction('registerFolderHotkeyHandler()');
{/if}
//-->
</script>

{include file="li/email.addressmenu.tpl"}
