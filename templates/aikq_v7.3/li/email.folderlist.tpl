var d = new dTree('d');
{foreach from=$folderList item=folder}
var icon = 'fa-folder-open';
{if $folder.icon=="intellifolder"}
icon = 'fa-puzzle-piece';

{elseif $folder.icon=="drafts"}
icon = 'fa-paper-plane';

{elseif $folder.icon=="inbox"}
icon = 'fa-inbox';

{elseif $folder.icon=="outbox"}
icon = 'fa-mail-forward';

{elseif $folder.icon=="spam"}
icon = 'fa-frown-o';

{elseif $folder.icon=="trash"}
icon = 'fa-trash';
{/if}
d.add({$folder.i}, {$folder.parent}, '<i class="fa '+icon+' fa-fw"></i> {text value=$folder.text escape=true noentities=true}{if $folder.unread>0} <b>({$folder.unread})</b>{/if}', 'javascript:switchFolder({$folder.id});', '{text value=$folder.text escape=true noentities=true}', '', tplDir+'images/pixel.gif', tplDir+'images/li/menu_ico_{$folder.icon}.png'); 
{/foreach}
