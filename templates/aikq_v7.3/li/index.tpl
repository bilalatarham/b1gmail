<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{$service_title}{if $pageTitle} - {text value=$pageTitle}{/if}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <!-- integrate open sans and fontawesome -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="{$templatePrefs.faUrl}" rel="stylesheet">
	{if IsMobileUserAgent()} 
	<link href="{$tpldir}style/full-mobile.css?{fileDateSig file="style/full-mobile.css"}" rel="stylesheet" type="text/css" />
	{else}
	<link href="{$tpldir}style/loggedin.css?{fileDateSig file="style/loggedin.css"}" rel="stylesheet" type="text/css" />
	{/if}
	<link href="{$tpldir}style/dtree.css?{fileDateSig file="style/dtree.css"}" rel="stylesheet" type="text/css" />
{foreach from=$_cssFiles.li item=_file}	<link rel="stylesheet" type="text/css" href="{$_file}" />
{/foreach}
	{if !IsMobileUserAgent()}
	<style>
	{literal}
	.dragItem {
		background: {/literal}{$templatePrefs.widgetBG1}{literal};	
	}
	#BMPlugin_Widget_Tasks, #dc_startBoxesdragItem_BMPlugin_Widget_Tasks .dragBar, #BMPlugin_Widget_WebdiskDND, #BMPlugin_Widget_Mailspace {
		background: {/literal}{$templatePrefs.widgetBG2}{literal};
	}
		
	#BMPlugin_Widget_EMail, #BMPlugin_Widget_Quicklinks {
		background: {/literal}{$templatePrefs.widgetBG3}{literal};
	}
		
	#BMPlugin_Widget_Calendar {
		background: {/literal}{$templatePrefs.widgetBG4}{literal};
	}

	{/literal}
	
	{if $templatePrefs.maincontentBG|strstr:"."}
	{literal}
	#contentHeader, .contentHeader {
		background: {/literal}{$templatePrefs.mainmenuBG}{literal};
		color: {/literal}{$templatePrefs.mainmenuColor} !important{literal};
	}
	#contentHeader .left, .contentHeader .left, #contentHeader .left a, #contentHeader .right, .contentHeader .right,  #contentHeader .right button, .contentHeader .right button {
		color: inherit !important;	
	}
	{/literal}
	{/if}
	</style>
	{/if}
	<!-- client scripts -->
	<script language="javascript" type="text/javascript">
	<!--
		var currentSID = '{$sid}', tplDir = '{$tpldir}', serverTZ = {$serverTZ};
		var topbarBG = '{if $usertopbarBG}{$usertopbarBG}{else}{$templatePrefs.topbarBG}{/if}'; var topbarColor = '{if $usertopbarColor}{$usertopbarColor}{else}{$templatePrefs.topbarColor}{/if}'; var mainmenuBG = '{if 
$usermainmenuBG}{$usermainmenuBG}{else}{$templatePrefs.mainmenuBG}{/if}'; var mainmenuColor = '{if 
$usermainmenuColor}{$usermainmenuColor}{else}{$templatePrefs.mainmenuColor}{/if}'; var maincontentBG = '"{if 
$usermaincontentBG}{$usermaincontentBG}{else}{$templatePrefs.maincontentBG}"{/if}'; var widgetBG1 = '{if 
$userwidgetBG1}{$userwidgetBG1}{else}{$templatePrefs.widgetBG1}{/if}'; var widgetBG2 = '{if $userwidgetBG2}{$userwidgetBG2}{else}{$templatePrefs.widgetBG2}{/if}'; var widgetBG3 = '{if $userwidgetBG3}{$userwidgetBG3}{else}{$templatePrefs.widgetBG3}{/if}'; var widgetBG4 = '{if 
$userwidgetBG4}{$userwidgetBG4}{else}{$templatePrefs.widgetBG4}{/if}'; 

	{if $security_startdisplay}var security_startdisplay = {$security_startdisplay};{/if} 
	{if $security_autologout}var security_autologout = {$security_autologout};{/if}
		
	//-->
	</script>
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
    <!-- jQuery -->
    <script src="clientlib/jquery/jquery-1.8.2.min.js?{fileDateSig file="../../clientlib/jquery/jquery-1.8.2.min.js"}" type="text/javascript" language="javascript"></script>
	{if !IsMobileUserAgent()}
	<!-- jQuery slimscroll -->
    <script type="text/javascript" src="{$tpldir}js/jQuery-slimScroll/jquery.slimscroll.min.js"></script>
	{/if}
	
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js?{fileDateSig file="js/loggedin.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/dtree.js?{fileDateSig file="../../clientlib/dtree.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js?{fileDateSig file="../../clientlib/overlay.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/autocomplete.js?{fileDateSig file="../../clientlib/autocomplete.js"}" type="text/javascript" language="javascript"></script>
	<!--[if lt IE 9]>
	<script defer type="text/javascript" src="clientlib/IE9.js"></script>
	<![endif]-->
	<!--[if IE]>
	<meta http-equiv="Page-Enter" content="blendTrans(duration=0)" />
	<meta http-equiv="Page-Exit" content="blendTrans(duration=0)" />
	<![endif]-->
{foreach from=$_jsFiles.li item=_file}	<script type="text/javascript" src="{$_file}"></script>
{/foreach}
{if IsMobileUserAgent()}<script src="{$tpldir}js/full-mobile.js?{fileDateSig file="js/full-mobile.js"}" type="text/javascript" language="javascript"></script>{/if}
</head>

<body onload="documentLoader()">

{include file="../../plugins/templates/ui.security.tpl}
	
	<div id="main">
		{if IsMobileUserAgent()}
        <div id="mobile-topbar">
			{include file="./full-mobile/li/mobilemenu.tpl"}
        </div>
        <div id="mobile-overlay-mainTabs" onclick='javascript:slidemenu("mainTabs", "close");'></div> 
        {/if}
		
		{if IsMobileUserAgent() }
		<div id="mainTabs">        	
			<ul>
         <!--   {if IsMobileUserAgent()} 
            <li{if $smarty.request.action=='search'} class="active"{/if}><a href="search.php?sid={$sid}"><br /><i class="fa fa-search"></i> {lng p="search2"}</a></li>
            <li class="lastli">
            <a href="javascript:void(0)" onclick="if(confirm('{lng p="logoutquestion"}')) document.location.href='start.php?sid={$sid}&action=logout';">
					<br /> <i class="fa fa-sign-out"></i> {lng p="logout"}
			</a>
            </li>
            {/if}-->
	            {foreach from=$pageTabs key=tabID item=tab}
	            {comment text="tab $tabID"}
	            <li{if $activeTab==$tabID} class="active"{/if}>
	            	<a href="{$tab.link}{$sid}" title="{$tab.text}">
	                    <img width="24" height="24" src="{if !$tab.iconDir}{$tpldir}images/li/v2_tab_ico_{if $activeTab==$tabID}active_{/if}{else}{$tab.iconDir}{/if}{if $tab.modernIcon&&$activeTab!=$tabID}{$tab.modernIcon}{elseif $tab.modernIconActive&&$activeTab==$tabID}{$tab.modernIconActive}{else}{$tab.icon}{/if}.png" border="0" alt="" align="absmiddle" />
	                    <br />
	                    {if $templatePrefs.tabMode=='icons'&&$activeTab!=$tabID}<span class="hoverVis">{$tab.text}</span>{else}{$tab.text}{/if}
	                </a>
	            </li>
	      	    {/foreach}
	    	</ul>
            {if IsMobileUserAgent()} 
            <div id="mainMobileMenu">
			    {if $pageMenuFile}
	            {comment text="including $pageMenuFile"}
	            {if file_exists($tpldir|cat:"full-mobile/"|cat:$pageMenuFile)} 
                    {include file="full-mobile/$pageMenuFile"}
                {else}
                    {include file="$pageMenuFile"}
                {/if}	           
	            {else}
	            {foreach from=$pageMenu key=menuID item=menu}
	            {comment text="menuitem $menuID"}
	           	<a href="{$menu.link}">
		            <img src="{$tpldir}images/li/menu_ico_{$menu.icon}.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		            {$menu.text}
	            </a>
	            {if $menu.addText}
	            <span class="menuAddText">{$menu.addText}</span>
	            {/if}
	            <br />
	        	{/foreach}
	            {/if}                  
			
			</div>
            {/if}
		</div>
		{/if}
		
		<div id="mainToolbar" {if !IsMobileUserAgent()}style="color: {$templatePrefs.topbarColor} ; background: {$templatePrefs.topbarBG}"{/if} >
			<div class="left">
				 {if !IsMobileUserAgent()}
					 <div id="mainLogo">
						{if $templatePrefs.liHeaderTitle == ""}
							{$service_title}
						{else}
							{$templatePrefs.liHeaderTitle}
						{/if}
					</div>
				{/if}
		    	{if $pageToolbarFile}
		    	{comment text="including $pageToolbarFile"}
                {if file_exists($tpldir|cat:"full-mobile/"|cat:$pageToolbarFile) && IsMobileUserAgent()} 
                    {include file="full-mobile/$pageToolbarFile"}
                {else}
                    {include file="$pageToolbarFile"}
                {/if}	            
		    	{elseif $pageToolbar}
		        {$pageToolbar}
		        {else}
		        &nbsp;
		        {/if}
	        </div>
	        <div class="right">
	        	{if $templatePrefs.showUserEmail}
        		<a href="prefs.php?action=membership&sid={$sid}" style="margin-right:5px;">
        			<small>{text value=$_userEmail}</small>
        		</a>
	        	{/if}

       			<button onclick="showNewMenu(this)">
					<i class="fa fa-plus-circle"></i>
					{lng p="new"}
				</button>
				
				<button onmouseup="showSearchPopup()">
					<i class="fa fa-search"></i>
                	<span class="tooltip">{lng p="search"}</span>
				</button>
				
				<button onclick="document.location.href='prefs.php?action=faq&sid={$sid}';">
					<i class="fa fa-question-circle"></i>
                    <span class="tooltip">{lng p="help"}</span>
				</button>
				
				<button onclick="if(confirm('{lng p="logoutquestion"}')) document.location.href='start.php?sid={$sid}&action=logout';">
					<i class="fa fa-sign-out"></i>
                    <span class="tooltip rcor">{lng p="logout"}</span>
				</button>
	        </div>
		</div>
		
		<div id="mainMenu" {if !IsMobileUserAgent()}style="color: {$templatePrefs.mainmenuColor}; background: {$templatePrefs.mainmenuBG}"{/if}>
			<div id="mainMenuContainer" style="bottom:{math equation="x*37+4" x=$pageTabsCount}px;">
			<div id="mainMenuWrap">
	            {if $pageMenuFile}
	            {comment text="including $pageMenuFile"}
                {if file_exists($tpldir|cat:"full-mobile/"|cat:$pageMenuFile) && IsMobileUserAgent()} 
                    {include file="full-mobile/$pageMenuFile"}
                {else}
                    {include file="$pageMenuFile"}
                {/if}	            
	            {else}
	            {foreach from=$pageMenu key=menuID item=menu}
	            {comment text="menuitem $menuID"}
	           	<a href="{$menu.link}">
		            <img src="{$tpldir}images/li/menu_ico_{$menu.icon}.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		            {$menu.text}
	            </a>
	            {if $menu.addText}
	            <span class="menuAddText">{$menu.addText}</span>
	            {/if}
	            <br />
	        	{/foreach}
	            {/if}
            </div>
            </div>
			{if !IsMobileUserAgent()}
			<ul id="menuTabItems">
	            {foreach from=$pageTabs key=tabID item=tab}
	            {comment text="tab $tabID"}
	            <li{if $activeTab==$tabID} class="active"{/if}>
	            	<a href="{$tab.link}{$sid}">
	                    <!--<img height="16" width="16" src="{if !$tab.iconDir}{$tpldir}images/li/tab_ico_{else}{$tab.iconDir}{/if}{$tab.icon}.png" border="0" alt="" align="absmiddle" />-->
                        <i class="fa
                        {if $tab.icon == "start"}
                        fa-th-large
                        {elseif $tab.icon == "email" }
                        fa-envelope-o
                        {elseif $tab.icon == "organizer" }
                        fa-calendar
                        {elseif $tab.icon == "webdisk" }
                        fa-cloud
                        {elseif $tab.icon == "prefs" }
                        fa-cog
                        {elseif $tab.icon == "faxPlugin" }
                        fa-fax
						{elseif $tab.icon == "sms" }
						fa-mobile
						{elseif $tab.icon == "twitter_logo_16_modern" }
						fa-twitter
						{elseif $tab.icon == "modbriefkarte" }
						fa-inbox
                        {else}
                        fa-circle-o
                        {/if}
                        fa-fw"></i>
	                    {if $tab.text}
{if $proflat_smallmenu}<div class="tooltip">&nbsp;{$tab.text}</div>{else}&nbsp;{$tab.text}{/if}
{/if}
	                </a>
	            </li>
	            {/foreach}
			</ul>
			{/if}
		</div>
		
		{if !IsMobileUserAgent()}
		<div id="mainBanner" style="display:none;">
			{banner}
		</div>
		{/if}
		
		<div id="mainContent" style="{if !IsMobileUserAgent() && $templatePrefs.maincontentBG|strstr:"."}background-image: url('{$tpldir}images/{$templatePrefs.maincontentBG}') {elseif !IsMobileUserAgent()} background: {$templatePrefs.maincontentBG}{/if} ">
			{if file_exists($tpldir|cat:"full-mobile/"|cat:$pageContent) && IsMobileUserAgent()} 
				{include file="full-mobile/$pageContent"}
			{else}
				{include file="$pageContent"}
			{/if}
		</div>
		
		<div id="mainStatusBar">
			
		</div>
		
	    {comment text="search popup"}
		<table width="100%" cellspacing="0" cellpadding="0" id="searchPopup" style="display:none;" onmouseover="disableHide=true;" onmouseout="disableHide=false;">
			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="22" height="26" align="right"><img id="searchSpinner" style="display:none;" src="{$tpldir}images/load_16.gif" border="0" alt="" width="16" height="16" align="absmiddle" /></td>
							<td align="right" width="70">{lng p="search"}: &nbsp;</td>
							<td align="center">
								<input id="searchField" name="searchField" style="width:90%" onkeypress="searchFieldKeyPress(event)" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tbody id="searchResultBody" style="display:none">
			<tr>
				<td id="searchResults"></td>
			</tr>
			</tbody>
		</table>
		
	    {comment text="new menu"}
		<div id="newMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;">
		{foreach from=$newMenu item=item}
			{if $item.sep}
			<div class="mailMenuSep"></div>
			{else}
			<a class="mailMenuItem" href="{$item.link}{$sid}">
			<i class="fa
			{if $item.icon == "ico_calendar"}
			fa-calendar
			{elseif $item.icon == "ico_todo"}
			fa-check-circle-o
			{elseif $item.icon == "ico_addressbook"}
			fa-book
			{elseif $item.icon == "ico_notes"}
			fa-align-left
			{elseif $item.icon == "send_mail"}
			fa-paper-plane
			{elseif $item.icon == "webdisk_file"}
			fa-file
			{else}
			fa-circle-o
			{/if}
			fa-fw"></i>
			{$item.text}...</a>
			{/if}
		{/foreach}
		</div>
		
	</div>
	
</body>

</html>
