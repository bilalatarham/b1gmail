/*
 * b1gMail7 client library
 * (c) 2002-2008 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: notloggedin.js,v 1.2 2013/01/06 21:01:45 patrick Exp $
 *
 */

function submitLoginForm()
{
	return(true);
}

function submitSignupForm()
{
	EBID('signupForm').style.display = 'none';
	EBID('signupSpinner').style.display = '';
}

function toggleFAQItem(id)
{
	var faqItem = EBID('faqAnswer_' + id);
	var faqItemImg = EBID('faqAnswerImage_' + id);
	
	if(faqItem.style.display == '')
	{
		faqItem.style.display = 'none';
		faqItemImg.src = faqItemImg.src.replace(/contract/, 'expand');
	}
	else
	{
		faqItem.style.display = '';
		faqItemImg.src = faqItemImg.src.replace(/expand/, 'contract');
	}
}

function updateLoginSSL(sslURL, enable)
{
	var action = 'index.php?action=login';
	
	if(enable)
		action = sslURL + action;
	
	EBID('loginForm').action = action;
}
