<?php 
/*
 * b1gMail7
 * (c) 2002-2013 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: info.php,v 1.6 2013/05/19 19:07:26 patrick Exp $
 *
 */

$templateInfo = array(
	'title'			=> 'b1gMail ' . $lang_admin['default'],
	'author'		=> 'B1G Software',
	'website'		=> 'http://www.b1g.de/',
	'for_b1gmail'	=> B1GMAIL_VERSION,
	
	'prefs'	=> array(
		'navPos'		=> array(
			'title'		=> $lang_admin['navpos'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array('top'		=> $lang_admin['top'],
									'left'	=> $lang_admin['left']),
			'default'	=> 'top'
		),
		'tabMode'		=> array(
			'title'		=> $lang_admin['tabmode'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array('complete' => $lang_admin['complete'],
									'icons'	=> $lang_admin['icons']),
			'default'	=> 'complete'
		),
		'prefsLayout'	=> array(
			'title'		=> $lang_admin['prefslayout'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array('onecolumn'		=> $lang_admin['onecolumn'],
									'twocolumns'	=> $lang_admin['twocolumns']),
			'default'	=> 'onecolumn'
		),
		'domainDisplay'	=> array(
			'title'		=> $lang_admin['domaindisplay'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options' 	=> array('normal'			=> $lang_admin['ddisplay_normal'],
									'separateat'	=> $lang_admin['ddisplay_separate']),
			'default'	=> 'normal'
		),
		'hideSignup'	=> array(
			'title'		=> $lang_admin['hidesignup'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'showUserEmail'	=> array(
			'title'		=> $lang_admin['showuseremail'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'showCheckboxes'=> array(
			'title'		=> $lang_admin['showcheckboxes'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		)
	)
);
?>