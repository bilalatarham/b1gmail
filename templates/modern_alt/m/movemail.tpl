<div id="leftTopButtons">
	<a class="hButton" href="main.php?action=read&id={$mailID}&sid={$sid}">
		<div class="hButtonLeftArrow"></div>&nbsp;&laquo;&nbsp;<div class="hButtonRight"></div>
	</a>
</div>

<div id="content" class="white noFooter">
	<ul>
	{foreach from=$folders item=folder key=folderID}
	{if !$folder.intelligent}
		<li>
			<a href="{if $folderID!=$currentFolder}main.php?action=moveMail&id={$mailID}&to={$folderID}&sid={$sid}{else}main.php?folder={$folderID}&sid={$sid}{/if}" class="high">
				<img src="{$selfurl}{$_tpldir}images/li/menu_ico_{$folder.type}.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				{if $folderID!=$currentFolder}{$folder.title}{else}<span style="color:grey;">{$folder.title}</span>{/if}
			</a>
		</li>
	{/if}
	{/foreach}
	</ul>
</div>
