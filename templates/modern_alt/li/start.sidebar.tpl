{hook id="start.sidebar.tpl:head"}

<div class="sidebarHeading">{lng p="start"}</div>
<div class="contentMenuIcons">
	<a href="start.php?sid={$sid}"><img src="{$tpldir}images/li/menu_ico_start.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="start"}</a><br />
	<a href="start.php?action=customize&sid={$sid}"><img src="{$tpldir}images/li/menu_ico_customize.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="customize"}</a><br />
	{hook id="start.sidebar.tpl:start"}
</div>

<div class="sidebarHeading">{lng p="misc"}</div>
<div class="contentMenuIcons">
	<a href="start.php?sid={$sid}&action=logout" onclick="return(confirm('{lng p="logoutquestion"}'));"><img src="{$tpldir}images/li/ico_logout.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="logout"}</a><br />
	{hook id="start.sidebar.tpl:misc"}
</div>

{hook id="start.sidebar.tpl:foot"}
