<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="cancelmembership"}
	</div>
</div>

<div class="scrollContainer"><div class="pad">

<table>
	<tr>
		<td valign="top" width="64" align="center"><img src="{$tpldir}images/li/msg.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			{lng p="canceltext"}
			<br /><br />
			<input type="button" value="&laquo; {lng p="back"}" onclick="history.back();" />
			<input type="button" value=" {lng p="cancelmembership"} (30) " onclick="document.location.href='prefs.php?action=membership&do=reallyCancelAccount&sid={$sid}';" disabled="disabled" id="cancelButton" />
		</td>
	</tr>
</table>

<script language="javascript">
<!--
	{literal}var i = 30;
	
	function cancelTimer()
	{
		i--;
	
		if(i==0)
		{
			EBID('cancelButton').value = '{/literal}{lng p="cancelmembership"}{literal}';
			EBID('cancelButton').disabled = false;
		}
		else
		{
			EBID('cancelButton').value = '{/literal}{lng p="cancelmembership"}{literal} (' + i + ')';
			window.setTimeout('cancelTimer()', 1000);
		}
	}
	
	window.setTimeout('cancelTimer()', 1000);{/literal}
//-->
</script>

</div></div>
