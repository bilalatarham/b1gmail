<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{$service_title}{if $pageTitle} - {text value=$pageTitle}{/if}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/loggedin.css?{fileDateSig file="style/loggedin.css"}" rel="stylesheet" type="text/css" />
	<link href="{$tpldir}style/dtree.css?{fileDateSig file="style/dtree.css"}" rel="stylesheet" type="text/css" />
{foreach from=$_cssFiles.li item=_file}	<link rel="stylesheet" type="text/css" href="{$_file}" />
{/foreach}

	<!-- client scripts -->
	<script language="javascript" type="text/javascript">
	<!--
		var currentSID = '{$sid}', tplDir = '{$tpldir}', serverTZ = {$serverTZ};
	//-->
	</script>
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js?{fileDateSig file="js/loggedin.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/dtree.js?{fileDateSig file="../../clientlib/dtree.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js?{fileDateSig file="../../clientlib/overlay.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/autocomplete.js?{fileDateSig file="../../clientlib/autocomplete.js"}" type="text/javascript" language="javascript"></script>
	<!--[if lt IE 9]>
	<script defer type="text/javascript" src="clientlib/IE9.js"></script>
	<![endif]-->
	<!--[if IE]>
	<meta http-equiv="Page-Enter" content="blendTrans(duration=0)" />
	<meta http-equiv="Page-Exit" content="blendTrans(duration=0)" />
	<![endif]-->
{foreach from=$_jsFiles.li item=_file}	<script type="text/javascript" src="{$_file}"></script>
{/foreach}
</head>

<body onload="documentLoader()">
	
	<div id="main">
		
		<div id="mainLogo">
			{$service_title}
		</div>
		
		{if $templatePrefs.navPos=='top'}
		<div id="mainTabs">
			<ul>
	            {foreach from=$pageTabs key=tabID item=tab}
	            {comment text="tab $tabID"}
	            <li{if $activeTab==$tabID} class="active"{/if}>
	            	<a href="{$tab.link}{$sid}" title="{$tab.text}">
	                    <img width="24" height="24" src="{if !$tab.iconDir}{$tpldir}images/li/v2_tab_ico_{if $activeTab==$tabID}active_{/if}{else}{$tab.iconDir}{/if}{if $tab.modernIcon&&$activeTab!=$tabID}{$tab.modernIcon}{elseif $tab.modernIconActive&&$activeTab==$tabID}{$tab.modernIconActive}{else}{$tab.icon}{/if}.png" border="0" alt="" align="absmiddle" />
	                    <br />
	                    {if $templatePrefs.tabMode=='icons'&&$activeTab!=$tabID}<span class="hoverVis">{$tab.text}</span>{else}{$tab.text}{/if}
	                </a>
	            </li>
	      	    {/foreach}
	    	</ul>
		</div>
		{/if}

		<div id="mainToolbar">
			<div class="left">
		    	{if $pageToolbarFile}
		    	{comment text="including $pageToolbarFile"}
		    	{include file="$pageToolbarFile"}
		    	{elseif $pageToolbar}
		        {$pageToolbar}
		        {else}
		        &nbsp;
		        {/if}
	        </div>
	        <div class="right">
	        	{if $templatePrefs.showUserEmail}
        		<a href="prefs.php?action=membership&sid={$sid}" style="margin-right:5px;">
        			<img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" />
        			<small>{text value=$_userEmail}</small>
        		</a>
	        	{/if}

       			<button onclick="showNewMenu(this)">
					<img src="{$tpldir}images/li/ico_add.png" width="16" height="16" border="0" alt="" align="absmiddle" />
					{lng p="new"}
					<img src="{$tpldir}images/li/ico_btn_dropdown.png" border="0" alt="" align="absmiddle" />
				</button>
				
				<button onmouseup="showSearchPopup()">
					<img src="{$tpldir}images/li/ico_search.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				</button>
				
				<button onclick="document.location.href='prefs.php?action=faq&sid={$sid}';">
					<img src="{$tpldir}images/li/ico_faq.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				</button>
				
				<button onclick="if(confirm('{lng p="logoutquestion"}')) document.location.href='start.php?sid={$sid}&action=logout';">
					<img src="{$tpldir}images/li/ico_logout.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				</button>
	        </div>
		</div>
		
		<div id="mainMenu">
			<div id="mainMenuContainer"{if $templatePrefs.navPos=='left'} style="bottom:{math equation="x*29" x=$pageTabsCount}px;"{/if}>
	            {if $pageMenuFile}
	            {comment text="including $pageMenuFile"}
	            {include file="$pageMenuFile"}
	            {else}
	            {foreach from=$pageMenu key=menuID item=menu}
	            {comment text="menuitem $menuID"}
	           	<a href="{$menu.link}">
		            <img src="{$tpldir}images/li/menu_ico_{$menu.icon}.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		            {$menu.text}
	            </a>
	            {if $menu.addText}
	            <span class="menuAddText">{$menu.addText}</span>
	            {/if}
	            <br />
	        	{/foreach}
	            {/if}
            </div>
            
			{if $templatePrefs.navPos=='left'}
			<ul id="menuTabItems">
	            {foreach from=$pageTabs key=tabID item=tab}
	            {comment text="tab $tabID"}
	            <li{if $activeTab==$tabID} class="active"{/if}>
	            	<a href="{$tab.link}{$sid}">
	                    <img height="16" width="16" src="{if !$tab.iconDir}{$tpldir}images/li/tab_ico_{else}{$tab.iconDir}{/if}{$tab.icon}.png" border="0" alt="" align="absmiddle" />
	                    {if $tab.text}&nbsp;{$tab.text}{/if}
	                </a>
	            </li>
	            {/foreach}
			</ul>
			{/if}
		</div>

		<div id="mainBanner" style="display:none;">
			{banner}
		</div>

		<div id="mainContent">
			{include file="$pageContent"}
		</div>
		
		<div id="mainStatusBar">
			{literal}&nbsp;{/literal} 
		</div>
		
	    {comment text="search popup"}
		<table width="100%" cellspacing="0" cellpadding="0" id="searchPopup" style="display:none;" onmouseover="disableHide=true;" onmouseout="disableHide=false;">
			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="22" height="26" align="right"><img id="searchSpinner" style="display:none;" src="{$tpldir}images/load_16.gif" border="0" alt="" width="16" height="16" align="absmiddle" /></td>
							<td align="right" width="70">{lng p="search"}: &nbsp;</td>
							<td align="center">
								<input id="searchField" name="searchField" style="width:90%" onkeypress="searchFieldKeyPress(event)" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tbody id="searchResultBody" style="display:none">
			<tr>
				<td id="searchResults"></td>
			</tr>
			</tbody>
		</table>
		
	    {comment text="new menu"}
		<div id="newMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;">
		{foreach from=$newMenu item=item}
			{if $item.sep}
			<div class="mailMenuSep"></div>
			{else}
			<a class="mailMenuItem" href="{$item.link}{$sid}"><img align="absmiddle" src="{if !$item.iconDir}{$tpldir}images/li/{else}{$item.iconDir}{/if}{$item.icon}.png" width="16" height="16" border="0" alt="" /> {$item.text}...</a>
			{/if}
		{/foreach}
		</div>
		
	</div>
	
</body>

</html>
