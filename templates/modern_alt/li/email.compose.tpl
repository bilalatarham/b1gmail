<form name="f1" method="post" action="email.compose.php?action=sendMail&sid={$sid}" autocomplete="off" onreset="if(!askReset()) return(false);editor.reset();">

<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="sendmail"}
	</div>
	
	<div class="right">
		<select name="newTextMode" id="textMode" onchange="return editor.switchMode(this.value)">
			<option value="text"{if !$mail || $mail.textMode=='text'} selected="selected"{/if}>{lng p="plaintext"}</option>
			<option value="html"{if $mail.textMode=='html'} selected="selected"{/if}>{lng p="htmltext"}</option>
		</select>
		
		&nbsp;
		
		<img src="{$tpldir}images/li/mailico_flagged.gif" border="0" alt="" align="absmiddle" /> <select name="priority" id="priority">
			<option value="1"{if $mail.priority==1} selected="selected"{/if}>{lng p="prio_1"}</option>
			<option value="0"{if !$mail || $mail.priority==0} selected="selected"{/if}>{lng p="prio_0"}</option>
			<option value="-1"{if $mail.priority==-1} selected="selected"{/if}>{lng p="prio_-1"}</option>
		</select>
	</div>
</div>

<div class="bigForm withBottomBar">

	<input type="hidden" name="do" id="do" value="" />
	<input type="hidden" name="reference" value="{$reference}" />
	
	<div class="previewMailHeader" id="composeHeader">
		<table class="lightTable">
			<tr>
				<th width="120">* <label for="from">{lng p="from"}:</label></th>
				<td><select name="from" id="from" style="width:100%;">
					{foreach from=$possibleSenders key=senderID item=sender}
						<option value="{$senderID}"{if $senderID==$mail.from} selected="selected"{/if}>{text value=$sender}</option>
					{/foreach}
					</select></td>
				<td width="140">&nbsp;</td>
			</tr>
			<tr>
				<th>* <label for="to">{lng p="to"}:</label></th>
				<td><input type="text" name="to" id="to" value="{text allowEmpty=true value=$mail.to}" style="width:100%;" /></td>
				<td>
					&nbsp;
					<span id="addrDiv_to">
						<button onclick="javascript:openAddressbook('{$sid}','email')" type="button" class="smallInput">
							<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
							{lng p="fromaddr"}
						</button>
					</span>
				</td>
			</tr>
			<tr>
				<th>
					<a href="javascript:advancedOptions('fields', 'right', 'bottom', '{$tpldir}');composeSizer(true);"><img src="{$tpldir}images/li/mini_arrow_{if !$mail.replyto && !$mail.bcc}right{else}bottom{/if}.gif" align="absmiddle" border="0" alt="" id="advanced_fields_arrow" /></a> &nbsp;
					<label for="cc">{lng p="cc"}:</label></th>
				<td><input type="text" name="cc" id="cc" value="{text allowEmpty=true value=$mail.cc}" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			
			<tbody id="advanced_fields_body" style="display:{if !$mail.replyto && !$mail.bcc}none{/if};">
			<tr>
				<th><label for="bcc">{lng p="bcc"}:</label></th>
				<td><input type="text" name="bcc" id="bcc" value="{text allowEmpty=true value=$mail.bcc}" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<th><label for="replyto">{lng p="replyto"}:</label></th>
				<td><input type="text" name="replyto" id="replyto" value="{text allowEmpty=true value=$mail.replyto}" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			</tbody>
			
			<tr>
				<th>* <label for="subject">{lng p="subject"}:</label></th>
				<td><input type="text" name="subject" id="subject" value="{text allowEmpty=true value=$mail.subject}" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<th>{lng p="attachments"}:</th>
				<td>
					<input type="hidden" name="attachments" value="{text value=$mail.attachments allowEmpty=true}" id="attachments" />
					<div id="attachmentList"></div>
				</td>
				<td valign="top">
					&nbsp;
					<button onclick="javascript:addAttachment('{$sid}')" type="button" class="smallInput">
						<img src="{$tpldir}images/li/ico_add.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						{lng p="add"}
					</button>
				</td>
			</tr>
			
			<tr>
				<th>&nbsp;</th>
				<td class="mailSendOptions" colspan="2">
					<div><img src="{$tpldir}images/li/export_vcf.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="attachVCard" id="attachVCard"{if $mail.attachVCard} checked="checked"{/if} /><label for="attachVCard">{lng p="attachvc"}</label></div>
					
					<div><img src="{$tpldir}images/li/mail_cert.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="certMail" id="certMail"{if $mail.certMail} checked="checked"{/if} /><label for="certMail">{lng p="certmail"}</label></div>
					
					<div><img src="{$tpldir}images/li/mail_confirm.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="mailConfirmation" id="mailConfirmation"{if $mail.mailConfirmation} checked="checked"{/if} /><label for="mailConfirmation">{lng p="mailconfirmation"}</label></div>
					
					{if $smime}
					<div><img src="{$tpldir}images/li/mailico_signed_ok.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="smimeSign" id="smimeSign"{if $mail.smimeSign} checked="checked"{/if} /><label for="smimeSign">{lng p="sign"}</label></div>
					
					<div><img src="{$tpldir}images/li/mailico_encrypted.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="smimeEncrypt" id="smimeEncrypt"{if $mail.smimeEncrypt} checked="checked"{/if} /><label for="smimeEncrypt">{lng p="encrypt"}</label></div>
					{/if}
				</td>
			</tr>
		</table>
	</div>

	<div id="composeText" style="width:100%;position:absolute;">
		<textarea class="composeTextarea{if $lineSep} lineSep{/if}" name="emailText" id="emailText" style="width:100%;height:100%;{if $useCourier}font-family:courier;{/if}">{text allowEmpty=true value=$mail.text}</textarea>
		{if !$mail || $mail.textMode=='text'}
		<input type="hidden" name="textMode" value="text" />
		{else}
		<input type="hidden" name="textMode" value="html" />
		{/if}
		<script language="javascript" src="./clientlib/wysiwyg.js?{fileDateSig file="../../clientlib/wysiwyg.js"}"></script>
		<script language="javascript">
		<!--
			var editor = new htmlEditor('emailText', '{$tpldir}/images/editor/');
			editor.init();
			editor.modeField = 'textMode';
			registerLoadAction('editor.start()');
			registerLoadAction('editor.switchMode("{if !$mail||$mail.textMode=='text'}text{else}html{/if}", true);');
		//-->
		</script>
	</div>
</div>

{hook id="email.compose.tpl:foot"}

{if $codeID}
<div id="safecodeFooter">
	<table cellpadding="0" style="margin-left:auto;margin-right:auto;">
		<tr>
			<td>
				<label for="safecode">{lng p="safecode"}:</label>
			</td>
			<td style="padding-left:2em;"><img src="index.php?action=codegen&id={$codeID}" border="0" alt="" style="cursor:pointer;" onclick="this.src='index.php?action=codegen&id={$codeID}&rand='+parseInt(Math.random()*10000);" /></td>
			<td style="padding-left:2em;">
				<input type="hidden" name="codeID" value="{$codeID}" />
				<input type="text" maxlength="6" size="20" style="text-align:center;width:212px;" name="safecode" id="safecode" />
			</td>
			<td style="padding-left:2em;"><small>{lng p="notreadable"}</small></td>
		</tr>
	</table>
</div>
{/if}

<div id="contentFooter">
	<div class="left">
		<img src="{$tpldir}images/li/menu_ico_outbox.png" width="16" height="16" border="0" alt="" align="absmiddle" /><label for="savecopy">{lng p="savecopy"}</label> <select name="savecopy" id="savecopy">
				<option value="-128">-</option>
			{foreach from=$dropdownFolderList key=dFolderID item=dFolderTitle}
				<option value="{$dFolderID}" style="font-family:courier;"{if (!$composeDefaults.savecopy&&$composeDefaults.savecopy!=='0'&&$dFolderID==-2)||$composeDefaults.savecopy==$dFolderID} selected="selected"{/if}>{$dFolderTitle}</option>
			{/foreach}
		</select>
		
		{if $signatures}
		&nbsp;
		
		<img src="{$tpldir}images/li/ico_signatures.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <select name="signature" id="signature">
		{foreach from=$signatures item=signature}
			<option value="{$signature.id}">{text value=$signature.titel cut=15}</option>
		{/foreach}
		</select> <button type="button" onclick="placeSignature(EBID('signature').value)">&raquo;</button>
		{/if}
	</div>
	<div class="center" id="waitNotice" style="line-height:2em;">
	{if $waitTime}
		{lng p="waituntil1"}
		<span id="waitSeconds">{$waitTime}</span>
		{lng p="waituntil2"}
	{/if}
	</div>
	<div class="right">
		<button type="button" onclick="EBID('do').value='saveDraft';editor.submit();document.forms.f1.submit();" />
			<img src="{$tpldir}images/li/menu_ico_drafts.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="savedraft"}
		</button>
		<button type="button" id="sendButton"{if $waitTime} disabled="disabled"{/if} onclick="if(!checkComposeForm(document.forms.f1, {if $attCheck}true{else}false{/if}, '{lng p="att_keywords"}')) return(false); EBID('do').value='sendMail';editor.submit();checkSMIME('{if $codeID}checkSafeCode({$codeID});{else}document.forms.f1.submit();{/if}');">
			<img src="{$tpldir}images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="sendmail2"}
		</button>
	</div>
</div>

</form>

<script src="./clientlib/dndupload.js?{fileDateSig file="../../clientlib/dndupload.js"}" language="javascript" type="text/javascript"></script>

<script language="javascript">
<!--
	registerLoadAction(initComposeAutoComplete);
	registerLoadAction(generateAttachmentList);
	{if $waitTime}registerLoadAction(composeWaitCountdown);{/if}
	registerLoadAction(composeSizer);
	initDnDUpload(EBID('mainContent'), 'email.compose.php?action=uploadDnDAttachment&sid=' + currentSID, false, dndAttachmentUploaded, dndAttachmentURLAddition);
//-->
</script>
