<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="folderadmin"}
	</div>
</div>

<form name="f1" method="post" action="email.folders.php?action=action&sid={$sid}">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<thead>
	<tr>
		<th width="20"><input type="checkbox" id="allChecker" onclick="checkAll(this.checked, document.forms.f1, 'folder');" /></th>
		<th class="listTableHead">
			<a href="email.folders.php?sid={$sid}&sort=titel&order={$sortOrderInv}">{lng p="title"}</a>
			{if $sortColumn=='titel'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th width="160">
			<a href="email.folders.php?sid={$sid}&sort=parent&order={$sortOrderInv}">{lng p="parentfolder"}</a>
			{if $sortColumn=='parent'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th width="70">
			{lng p="size"}
		</th>
		<th width="140">
			{lng p="status"}
		</th>
		<th width="70">
			<a href="email.folders.php?sid={$sid}&sort=subscribed&order={$sortOrderInv}">{lng p="subscribed"}</a>
			{if $sortColumn=='subscribed'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th width="55">&nbsp;</th>
	</tr>
	</thead>
	
	{if $sysFolderList}
	<tr>
		<td colspan="7" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('sys');">&nbsp;<img id="groupImage_sys" src="{$tpldir}images/contract.gif" border="0" align="absmiddle" alt="" />
			&nbsp;{lng p="sysfolders"}</a>
		</td>
	</tr>
	<tbody id="group_sys" style="display:;">
	{foreach from=$sysFolderList key=folderID item=folder}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}" nowrap="nowrap"><input type="checkbox" disabled="disabled" /></td>
		<td class="{$class}" nowrap="nowrap">&nbsp;<a href="email.php?sid={$sid}&folder={$folderID}"><img src="{$tpldir}images/li/menu_ico_{$folder.type}.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$folder.titel cut=25}</a></td>
		<td class="{$class}" nowrap="nowrap">&nbsp;{text value=$folder.parent cut=15}</td>
		<td class="{$class}" nowrap="nowrap" style="text-align:center;">
			{size bytes=$folder.size}
		</td>
		<td class="{$class}" nowrap="nowrap" style="text-align:center;">
			<table>
				<tr>
					<td width="45" align="left"><img src="{$tpldir}images/li/menu_ico_folder.png" border="0" alt="" width="16" height="16" align="absmiddle" />
						{$folder.allMails}</td>
					<td width="45" align="left"><img src="{$tpldir}images/li/mail_markunread.png" border="0" alt="" width="16" height="16" align="absmiddle" />
						{$folder.unreadMails}</td>
					<td width="45" align="left"><img src="{$tpldir}images/li/mailico_flagged.gif" border="0" alt="" width="16" height="16" align="absmiddle" />
						{$folder.flaggedMails}</td>
				</tr>
			</table>
		</td>
		<td class="{$class}" nowrap="nowrap"><center><input type="checkbox" checked="checked" disabled="disabled" /></center></td>
		<td class="{$class}" nowrap="nowrap">
			<a href="email.folders.php?action=editFolder&id={$folderID}&sid={$sid}"><img src="{$tpldir}images/li/ico_edit.png" width="16" height="16" border="0" alt="{lng p="edit"}" align="absmiddle" /></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
	
	{if $theFolderList}
	<tr>
		<td colspan="7" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('own');">&nbsp;<img id="groupImage_own" src="{$tpldir}images/contract.gif" border="0" align="absmiddle" alt="" />
			&nbsp;{lng p="ownfolders"}</a>
		</td>
	</tr>
	<tbody id="group_own" style="display:;">
	{foreach from=$theFolderList key=folderID item=folder}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}" nowrap="nowrap"><input type="checkbox" id="folder_{$folderID}" name="folder_{$folderID}" /></td>
		<td class="{if $sortColumn=='titel'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;<a href="email.php?sid={$sid}&folder={$folderID}"><img src="{$tpldir}images/li/menu_ico_{if $folder.intelligent==1}intelli{/if}folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$folder.titel cut=25}</a></td>
		<td class="{if $sortColumn=='parent'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;{text value=$folder.parent cut=15}</td>
		<td class="{$class}" nowrap="nowrap" style="text-align:center;">
			{if $folder.intelligent}-{else}{size bytes=$folder.size}{/if}
		</td>
		<td class="{$class}" nowrap="nowrap" style="text-align:center;">
			<table>
				<tr>
					<td width="45" align="left"><img src="{$tpldir}images/li/menu_ico_folder.png" border="0" alt="" width="16" height="16" align="absmiddle" />
						{$folder.allMails}</td>
					<td width="45" align="left"><img src="{$tpldir}images/li/mail_markunread.png" border="0" alt="" width="16" height="16" align="absmiddle" />
						{$folder.unreadMails}</td>
					<td width="45" align="left"><img src="{$tpldir}images/li/mailico_flagged.gif" border="0" alt="" width="16" height="16" align="absmiddle" />
						{$folder.flaggedMails}</td>
				</tr>
			</table>
		</td>
		<td class="{if $sortColumn=='subscribed'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap"><center><input type="checkbox" {if $folder.subscribed==1}checked="checked" {/if} onchange="updateFolderSubscription('{$folderID}', this, '{$sid}')" /></center></td>
		<td class="{$class}" nowrap="nowrap">
			<a href="email.folders.php?action=editFolder&id={$folderID}&sid={$sid}"><img src="{$tpldir}images/li/ico_edit.png" width="16" height="16" border="0" alt="{lng p="edit"}" align="absmiddle" /></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="email.folders.php?action=deleteFolder&id={$folderID}&sid={$sid}"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="delete"}" align="absmiddle" /></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
</table>

</div>

<div id="contentFooter">
	<div class="left">
		<select class="smallInput" name="do">
			<option value="-">------ {lng p="selaction"} ------</option>
			<option value="delete">{lng p="delete"}</option>
		</select>
		<input class="smallInput" type="submit" value="{lng p="ok"}" />
	</div>
	
	<div class="right">
		<button onclick="document.location.href='email.folders.php?action=addFolder&sid={$sid}';" type="button">
			<img src="{$tpldir}images/li/ico_add.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="addfolder"}
		</button>
	</div>
</div>

</form>
