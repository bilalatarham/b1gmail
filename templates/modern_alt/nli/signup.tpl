<form action="index.php?action=signup" method="post" onsubmit="submitSignupForm()">
<input type="hidden" name="do" value="createAccount" />
<input type="hidden" name="transPostVars" value="true" />
<input type="hidden" name="codeID" value="{$codeID}" />

<div id="signupSpinner" style="display:none;width:100%;">
<center><img src="{$tpldir}images/load_32.gif" border="0" alt="" /><br /><br /></center>
</div>
<div id="signupForm" style="display:;">
<table class="nliTable">
	<!-- sign up -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/signup.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="signup"}</h3>
			
			{lng p="signuptxt"} {if $code}{lng p="signuptxt_code"}{/if}
			
			{if $errorStep}
			<br /><br /><div class="errorText">{$errorInfo}</div>
			{else}
			<br />
			{/if}
		
			<br />
		</td>
	</tr>
	
	<!-- address -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/address.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="wishaddress"}</h3>
			
			<table>
				<tr>
					<td class="formCaption">* <label for="email_local">{lng p="wishaddress"}:</label></td>
					<td class="formField">
						<input type="text" name="email_local" id="email_local" value="{$_safePost.email_local}" size="20" onblur="checkAddressAvailability()" />
						{if $templatePrefs.domainDisplay!='normal'}<strong> @ </strong>{/if}
						<select name="email_domain" id="email_domain" onblur="checkAddressAvailability()">
							{foreach from=$domainList item=domain}<option value="{$domain}"{if $_safePost.email_domain==$domain} selected="selected"{/if}>{if $templatePrefs.domainDisplay=='normal'}@{/if}{$domain}</option>{/foreach}
						</select>
					</td>
					<td class="formStatus" id="addressAvailabilityIndicator">&nbsp;</td>
				</tr>
			</table>
		
			<br />
		</td>
	</tr>
	
	<!-- address -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="contactinfo"}</h3>
			
			<table>
				{if $f_anrede!="n"}
				<tr>
					<td class="formCaption">{if $f_anrede=="p"}*{/if} <label for="salutation">{lng p="salutation"}:</label></td>
					<td class="formField">
						<select name="salutation" id="salutation">
							<option value="">&nbsp;</option>
							<option value="herr"{if $_safePost.salutation=='herr'} selected="selected"{/if}>{lng p="mr"}</option>
							<option value="frau"{if $_safePost.salutation=='frau'} selected="selected"{/if}>{lng p="mrs"}</option>
						</select>
					</td>
					<td class="formStatus" id="salutationValidity">&nbsp;</td>
				</tr>
				{/if}
				
				<tr>
					<td class="formCaption">* <label for="firstname">{lng p="firstname"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.firstname}" name="firstname" id="firstname" size="35" />
					</td>
					<td class="formStatus" id="firstnameValidity">&nbsp;</td>
				</tr>
				<tr>
					<td class="formCaption">* <label for="surname">{lng p="surname"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.surname}" name="surname" id="surname" size="35" />
					</td>
					<td class="formStatus" id="surnameValidity">&nbsp;</td>
				</tr>
				
				{if $f_strasse!="n"}
				<tr>
					<td class="formCaption">{if $f_strasse=="p"}*{/if} <label for="street">{lng p="streetnr"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.street}" name="street" id="street" size="35" />
						<input type="text" value="{$_safePost.no}" name="no" id="no" size="8" />
					</td>
					<td class="formStatus" id="streetValidity">&nbsp;</td>
				</tr>
				<tr>
					<td class="formCaption">{if $f_strasse=="p"}*{/if} <label for="zip">{lng p="zipcity"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.zip}" name="zip" id="zip" size="8" />
						<input type="text" value="{$_safePost.city}" name="city" id="city" size="35" />
					</td>
					<td class="formStatus" id="zipValidity">&nbsp;</td>
				</tr>
				<tr>
					<td class="formCaption">{if $f_strasse=="p"}*{/if} <label for="country">{lng p="country"}:</label></td>
					<td class="formField">
						<select name="country" id="country">
							{foreach from=$countryList item=country key=id}
							<option value="{$id}"{if (!$_safePost.country && $id==$defaultCountry) || ($_safePost.country==$id)} selected="selected"{/if}>{$country}</option>
							{/foreach}
						</select>
					</td>
					<td class="formStatus" id="countryValidity">&nbsp;</td>
				</tr>
				{/if}
				
				{if $f_telefon!="n"}
				<tr>
					<td class="formCaption">{if $f_telefon=="p"}*{/if} <label for="phone">{lng p="phone"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.phone}" name="phone" id="phone" size="35" />
					</td>
					<td class="formStatus" id="phoneValidity">&nbsp;</td>
				</tr>
				{/if}
				
				{if $f_fax!="n"}
				<tr>
					<td class="formCaption">{if $f_fax=="p"}*{/if} <label for="fax">{lng p="fax"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.fax}" name="fax" id="fax" size="35" />
					</td>
					<td class="formStatus" id="faxValidity">&nbsp;</td>
				</tr>
				{/if}
				
				{if $f_mail2sms_nummer!="n"}
				<tr>
					<td class="formCaption">{if $f_mail2sms_nummer=="p"}*{/if} <label for="mail2sms_nummer">{lng p="mobile"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.mail2sms_nummer}" name="mail2sms_nummer" id="mail2sms_nummer" size="35" />
					</td>
					<td class="formStatus" id="mail2sms_nummerValidity">&nbsp;</td>
				</tr>
				{/if}
				
				{if $f_alternativ!="n"}
				<tr>
					<td class="formCaption">{if $f_alternativ=="p"}*{/if} <label for="altmail">{lng p="altmail"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.altmail}" name="altmail" id="altmail" size="35" />
					</td>
					<td class="formStatus" id="altmailValidity">&nbsp;</td>
				</tr>
				{/if}
			</table>
		
			<br />
		</td>
	</tr>
	
	<!-- password -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/password.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="password"}</h3>
			
			<table>
				<tr>
					<td class="formCaption">* <label for="pass1">{lng p="password"}:</label></td>
					<td class="formField">
						<input autocomplete="off" type="password" name="pass1" id="pass1" onkeyup="passwordSecurity(this.value, 'secureBar')" size="35" />
					</td>
					<td class="formStatus" id="pass1Validity">&nbsp;</td>
				</tr>
				<tr>
					<td class="formCaption">* <label for="pass2">{lng p="repeat"}:</label></td>
					<td class="formField">
						<input autocomplete="off" type="password" name="pass2" id="pass2" size="35" />
					</td>
					<td class="formStatus" id="pass2Validity">&nbsp;</td>
				</tr>
				<tr>
					<td class="formCaption">{lng p="security"}:</td>
					<td class="formField">
						<div class="passwordSecurity">
							<div class="secureBar" id="secureBar" style="background: url({$tpldir}images/main/securebar.jpg); width:0%;">&nbsp;</div>
						</div>
					</td>
					<td class="formStatus">&nbsp;</td>
				</tr>
			</table>
			
			<br />
		</td>
	</tr>
	
	{if $code}
	<!-- code -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/code.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="code"}</h3>
			
			<table>
				<tr>
					<td class="formCaption"><label for="code">{lng p="code"}:</label></td>
					<td class="formField">
						<input type="text" value="{$_safePost.code}" name="code" id="code" size="35" />
					</td>
					<td class="formStatus" id="codeValidity">&nbsp;</td>
				</tr>
			</table>
			
			<br />
		</td>
	</tr>
	{/if}
	
	{if $profilfelder}
	<!-- misc  -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/misc.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="misc"}</h3>
			
			<table>
				{foreach from=$profilfelder item=feld}
				{assign var=fieldID value=$feld.id}
				{assign var=fieldName value="field_$fieldID"}
				<tr>
					<td class="formCaption" nowrap="nowrap">{if $feld.pflicht}*{/if} <label for="field_{$feld.id}">{$feld.feld}:</label></td>
					<td class="formField">
						{if $feld.typ==1}
							<input size="35" name="field_{$feld.id}" id="field_{$feld.id}" value="{$_safePost[$fieldName]}" type="text" />
						{elseif $feld.typ==2}
							<input type="checkbox" name="field_{$feld.id}" id="field_{$feld.id}"{if $_safePost[$fieldName]} checked="checked"{/if} />
						{elseif $feld.typ==4}
							<select name="field_{$feld.id}" id="field_{$feld.id}">
								{foreach from=$feld.extra item=item}
									<option value="{$item}"{if $_safePost[$fieldName]==$item} selected="selected"{/if}>{$item}</option>
								{/foreach}
							</select>
						{elseif $feld.typ==8}
							{foreach from=$feld.extra item=item}
								<input type="radio" id="field_{$feld.id}_{$item}" name="field_{$feld.id}" value="{$item}"{if $_safePost[$fieldName]==$item} checked="checked"{/if} />
								<label for="field_{$feld.id}_{$item}">{$item}</label> &nbsp; 
							{/foreach}
						{else if $feld.typ==32}
							{if $dateFields[$fieldName]}
							{html_select_date time=$dateFields[$fieldName] year_empty="---" day_empty="---" month_empty="---" start_year="-120" end_year="+0" prefix="field_$fieldID" field_order="DMY"}
							{else}
							{html_select_date time="---" year_empty="---" day_empty="---" month_empty="---" start_year="-120" end_year="+0" prefix="field_$fieldID" field_order="DMY"}
							{/if}
						{/if}
					</td>
					<td class="formStatus" id="field{$feld.id}Validity">&nbsp;</td>
				</tr>
				{/foreach}
			</table>
			
			<br />
		</td>
	</tr>	
	{/if}
	
	{if $f_safecode!='n'}
	<!-- safecode -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/safecode.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="safecode"}</h3>
			
			<table>
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">
						<img src="index.php?action=codegen&id={$codeID}" border="0" alt="" style="cursor:pointer;" onclick="this.src='index.php?action=codegen&id={$codeID}&rand='+parseInt(Math.random()*10000);" />
					</td>
					<td class="formStatus" id="safecodeValidity">
						<small>{lng p="notreadable"}</small>
					</td>
				</tr>
				<tr>
					<td class="formCaption" nowrap="nowrap">* <label for="safecode">{lng p="safecode"}:</label></td>
					<td class="formField">
						<input type="text" maxlength="6" style="text-align:center;" name="safecode" id="safecode" size="35" />
					</td>
					<td class="formStatus" id="safecodeValidity">&nbsp;</td>
				</tr>
			</table>
			
			<br />
		</td>
	</tr>
	{/if}
	
	<!-- terms of service -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/tos.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="tos"}</h3>
			
			<textarea style="width: 98%; height: 120px;" readonly="readonly">{$tos}</textarea>
			
			<center>
				<br />
				<input type="radio"{if $_safePost.tos=='true'} checked="checked"{/if} name="tos" value="true" id="tos_acc" /> <label for="tos_acc">{lng p="tosaccept"}</label>
				<input type="radio"{if $_safePost.tos!='true'} checked="checked"{/if} name="tos" value="false" id="tos_nacc" /> <label for="tos_nacc">{lng p="tosnaccept"}</label>
			</center>
			
			<br />
		</td>
	</tr>
	
	<!-- submit -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/signup.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="submit"}</h3>
			
			<img src="{$tpldir}images/main/ip.gif" style="vertical-align: middle;" border="0" alt="" /> {lng p="iprecord"}
			
			<br /><br />
			<center>
				<input type="submit" value=" &nbsp; {lng p="submit"} &nbsp; " />
			</center><br />
		</td>
	</tr>
</table>
</div>
</form>

{if $errorStep}
<script language="javascript">
<!--
{foreach from=$invalidFields item=field}
	markFieldAsInvalid('{$field}');
{/foreach}
{if $f_safecode!='n'}
	markFieldAsInvalid('safecode');
{/if}
	markFieldAsInvalid('pass1');
	markFieldAsInvalid('pass2');
//-->
</script>
{else}
<script language="javascript">
<!--
	EBID('email_local').focus();
//-->
</script>
{/if}