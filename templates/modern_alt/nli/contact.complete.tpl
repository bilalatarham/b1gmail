<form action="index.php?action=completeAddressBookEntry&contact={$contact.id}&key={$contact.invitationCode}" method="post">
<input type="hidden" name="do" value="save" />

<table class="nliTable">
	<!-- contact completion -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="addrselfcomplete"}</h3>
			
			{lng p="completeintro"}
		
			<br /><br />
		</td>
	</tr>
		
	<!-- common -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact_common.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="common"}</h3>
			
			<table>
				<tr>
					<td class="formCaption"><label for="anrede">{lng p="salutation"}:</label></td>
					<td class="formField">
						<select name="anrede" id="anrede">
							<option value=""{if $contact.anrede==''} selected="selected"{/if}>&nbsp;</option>
							<option value="frau"{if $contact.anrede=='frau'} selected="selected"{/if}>{lng p="mrs"}</option>
							<option value="herr"{if $contact.anrede=='herr'} selected="selected"{/if}>{lng p="mr"}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="formCaption">{lng p="firstname"} / {lng p="surname"}:</td>
					<td class="formField">
						<input type="text" value="{text value=$contact.vorname}" disabled="disabled" size="20" />
						<input type="text" value="{text value=$contact.nachname}" disabled="disabled" size="20" />
					</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
		
	<!-- private -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact_priv.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="priv"}</h3>
			
			<table>
				<tr>
					<td class="formCaption"><label for="strassenr">{lng p="streetnr"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.strassenr allowEmpty=true}" name="strassenr" id="strassenr" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="plz">{lng p="zipcity"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.plz allowEmpty=true}" name="plz" id="plz" size="8" />
						<input type="text" value="{text value=$contact.ort allowEmpty=true}" name="ort" id="ort" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="land">{lng p="country"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.land allowEmpty=true}" name="land" id="land" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="email">{lng p="email"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.email allowEmpty=true}" name="email" id="email" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="tel">{lng p="phone"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.tel allowEmpty=true}" name="tel" id="tel" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="fax">{lng p="fax"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.fax allowEmpty=true}" name="fax" id="fax" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="handy">{lng p="mobile"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.handy allowEmpty=true}" name="handy" id="handy" size="35" />
					</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
		
	<!-- work -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact_work.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="work"}</h3>
			
			<table>
				<tr>
					<td class="formCaption"><label for="work_strassenr">{lng p="streetnr"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_strassenr allowEmpty=true}" name="work_strassenr" id="work_strassenr" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="work_plz">{lng p="zipcity"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_plz allowEmpty=true}" name="work_plz" id="work_plz" size="8" />
						<input type="text" value="{text value=$contact.work_ort allowEmpty=true}" name="work_ort" id="work_ort" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="work_land">{lng p="country"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_land allowEmpty=true}" name="work_land" id="work_land" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="work_email">{lng p="email"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_email allowEmpty=true}" name="work_email" id="work_email" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="work_tel">{lng p="phone"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_tel allowEmpty=true}" name="work_tel" id="work_tel" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="work_fax">{lng p="fax"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_fax allowEmpty=true}" name="work_fax" id="work_fax" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="work_handy">{lng p="mobile"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.work_handy allowEmpty=true}" name="work_handy" id="work_handy" size="35" />
					</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	
	<!-- misc -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact_misc.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="misc"}</h3>
			
			<table>
				<tr>
					<td class="formCaption"><label for="firma">{lng p="company"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.firma allowEmpty=true}" name="firma" id="firma" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="position">{lng p="position"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.position allowEmpty=true}" name="position" id="position" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="web">{lng p="web"}:</label></td>
					<td class="formField">
						<input type="text" value="{text value=$contact.web allowEmpty=true}" name="web" id="web" size="35" />
					</td>
				</tr>
				<tr>
					<td class="formCaption">{lng p="birthday"}:</td>
					<td class="formField">
						{html_select_date time=$contact.geburtsdatum start_year="-120" end_year="+0" prefix="geburtsdatum_" field_order="DMY"}
					</td>
				</tr>
			</table>
			<br />
		</td>
	</tr>
	
	<!-- submit -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/contact.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="submit"}</h3>
			
			<img src="{$tpldir}images/main/ip.gif" style="vertical-align: middle;" border="0" alt="" /> {lng p="iprecord"}
			
			<br /><br />
			<center>
				<input type="submit" value=" &nbsp; {lng p="save"} &nbsp; " />
			</center><br />
		</td>
	</tr>
</table>

</form>