<table class="nliTable">
	<!-- terms of service -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/faq.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="faq"}</h3>
			
			{lng p="faqtxt"}
			<br /><br />
			
			{foreach from=$faq key=id item=item}
			<div class="faqQuestion" onclick="toggleFAQItem({$id})">
				&nbsp;
				<img id="faqAnswerImage_{$id}" src="{$tpldir}images/expand.gif" border="0" alt="" class="faqExpand" />
				{$item.question}
			</div>
			<div class="faqAnswer" id="faqAnswer_{$id}" style="display:none;">
				<div style="padding:5px;">
					{$item.answer}
				</div>
			</div>
			<br />
			{/foreach}
		
			<br /><br />
		</td>
	</tr>
</table>