<?php
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 *
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

global $currentLanguage;
if($currentLanguage == 'deutsch') {
	$lang_admin['mb_ps_showabout'] = '(nicht eingeloggt) Beschreibung des Dienstes (about.tpl) anzeigen';
	$lang_admin['mb_ps_maincolor'] = '(nicht eingeloggt) Haupt-Farbe';
	$lang_admin['mb_ps_maincolor_shade'] = '(nicht eingeloggt) Haupt-Farbe, verdunkelt';
} else {
	$lang_admin['mb_ps_showabout'] = '(not logged in) Show description (about.tpl)';
	$lang_admin['mb_ps_maincolor'] = '(not logged in) Main color';
	$lang_admin['mb_ps_maincolor_shade'] = '(not logged in) Main color, darkened';
}

$templateInfo = array(
	'title'			=> 'Pro-slide',
	'author'		=> 'Martin Buchalik',
	'website'		=> 'http://martin-buchalik.de/',
	'for_b1gmail'	=> '7.4.0',

	'prefs'	=> array(
		'splashImage'	=> array(
			'title'		=> $lang_admin['splashimage'] . ':',
			'type'		=> FIELD_TEXT,
			'default'	=> 'mountains-1985027_1920.jpg'
		),
		'psShowAbout'=> array(
			'title'		=> $lang_admin['mb_ps_showabout'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> true
		),
		'psMaincolor'=> array(
			'title'		=> $lang_admin['mb_ps_maincolor'] . ':',
			'type'		=> FIELD_TEXT,
			'default'	=> '#1976d2'
		),
		'psMaincolorShade'=> array(
			'title'		=> $lang_admin['mb_ps_maincolor_shade'] . ':',
			'type'		=> FIELD_TEXT,
			'default'	=> '#1565c0'
		),
		'hideSignup'	=> array(
			'title'		=> $lang_admin['hidesignup'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'navPos'		=> array(
			'title'		=> $lang_admin['navpos'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array(	'top'			=> $lang_admin['top'],
									'left'			=> $lang_admin['left']),
			'default'	=> 'top'
		),
		'prefsLayout'	=> array(
			'title'		=> $lang_admin['prefslayout'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array('onecolumn'		=> $lang_admin['onecolumn'],
									'twocolumns'	=> $lang_admin['twocolumns']),
			'default'	=> 'onecolumn'
		),
		'showUserEmail'	=> array(
			'title'		=> $lang_admin['showuseremail'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'showCheckboxes'=> array(
			'title'		=> $lang_admin['showcheckboxes'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		)
	)
);
