<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_filters.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="filters"}
	</div>
</div>

<form name="f1" method="post" action="prefs.php?action=filters&do=action&sid={$sid}">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<tr>		
		<th>
			<a href="prefs.php?sid={$sid}&action=filters&sort=title&order={$sortOrderInv}">{lng p="title"}</a>
			{if $sortColumn=='title'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>			
		<th width="50">
			<a href="prefs.php?sid={$sid}&action=filters&sort=active&order={$sortOrderInv}">{lng p="active"}?</a>
			{if $sortColumn=='active'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th width="90">&nbsp;</th>
	</tr>
	
	{if $filterList}
	<tbody class="listTBody">
	{foreach from=$filterList key=filterID item=filter}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{if $sortColumn=='title'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;<a href="prefs.php?sid={$sid}&action=filters&do=edit&id={$filterID}"><i class="fa fa-filter"></i> {text value=$filter.title}</a></td>		
		<td class="{if $sortColumn=='active'}listTableTDActive{else}{$class}{/if}" nowrap="nowrap">&nbsp;<input type="checkbox" disabled="disabled"{if $filter.active} checked="checked"{/if} /></td>
		<td class="{$class}" nowrap="nowrap">
			<a href="prefs.php?action=filters&do=edit&id={$filterID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="prefs.php?action=filters&do=delete&id={$filterID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
</table>
</div>

</form>
