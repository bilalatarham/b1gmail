<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_calendar_groups.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="groups"}
	</div>
</div>

<form name="f1" method="post" action="organizer.calendar.php?action=groups&do=action&sid={$sid}">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<tr>
		<th class="listTableHead">
			<a href="organizer.calendar.php?action=groups&sid={$sid}&sort=title&order={$sortOrderInv}">{lng p="title"}</a>
			{if $sortColumn=='title'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th class="listTableHead" width="70">
			<a href="organizer.calendar.php?action=groups&sid={$sid}&sort=color&order={$sortOrderInv}">{lng p="color"}</a>
			{if $sortColumn=='color'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th class="listTableHead" width="90">&nbsp;</th>
	</tr>
	
	{if $haveGroups}
	<tbody class="listTBody">
	{foreach from=$groups key=groupID item=group}
	{if $groupID!=-1}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td nowrap="nowrap" class="{if $sortColumn=='title'}listTableTDActive{else}{$class}{/if}">&nbsp;<a href="organizer.calendar.php?switchGroup={$groupID}&sid={$sid}"><img src="{$tpldir}images/li/ico_calendar_groups.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$group.title}</a></td>
		<td class="{if $sortColumn=='color'}listTableTDActive{else}{$class}{/if}"><div class="calendarDate_{$group.color}" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
		<td class="{$class}" nowrap="nowrap">
			<a href="organizer.calendar.php?action=groups&do=edit&id={$groupID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="organizer.calendar.php?action=groups&do=delete&id={$groupID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{/if}
	{/foreach}
	</tbody>
	{/if}
</table>
</div>

</form>
