<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_calendar.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="calendar"}: {$weekDay}, {date timestamp=$date dayonly=true} ({lng p="cw"} {$calWeek})
	</div>	
</div>

<div class="scrollContainer" style="overflow-y:hidden;" id="calendarContainer">
	<table class="bigTable" id="calendarWholeDayBody">
		<tr>
			<td class="calendarWholeDayTD">
				<table class="calendarWholeDayBody">
				<tr>
					<td class="calendarDayTimeCell">&nbsp;</td>
					<td class="calendarDaySepCell"></td>
					<td class="calendarDaySepCell2"></td>
					<td class="calendarWholeDayCell">
						{foreach from=$dates item=date}
						{if $date.flags&1}
							<div class="calendarDate_{$groups[$date.group].color}" onclick="showCalendarDate({$date.id}, {$date.startdate}, {$date.enddate})">
								{text value=$date.title}
							</div>
						{/if}
						{/foreach}
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	<iframe class="calendarDayBody" id="calendarDayBody" src="organizer.calendar.php?action=dayView&date={$theDate}&sid={$sid}" frameborder="0" border="0"></iframe>
</div>

<script language="javascript">
<!--
	registerLoadAction('calendarDaySizer()');
//-->
</script>
{if !IsMobileUserAgent()} 
<div id="contentFooter">
	<div class="right">
		<button type="button" onclick="document.location.href='organizer.calendar.php?action=addDate&date={$theDate}&sid={$sid}';">
			<img src="{$tpldir}images/li/ico_add.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="adddate"}
		</button>
	</div>
</div>
{/if}