<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_common.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="prefs"}
	</div>
</div>

<div class="scrollContainer"><div class="pad">

{if $templatePrefs.prefsLayout=='onecolumn'}
	<table width="100%">
	{foreach from=$prefsItems item=null key=item}
	<tr>
		<td width="{if IsMobileUserAgent()}10{else}55{/if}" height="62" valign="top">
			{if !IsMobileUserAgent()}<a href="prefs.php?action={$item}&sid={$sid}">
				<img src="{if $prefsImages[$item]}{$prefsImages[$item]}{else}{$tpldir}images/li/prefs_{$item}.png{/if}" width="48" height="48" border="0" alt="" />
			</a>{/if}
		</td>
		<td class="prefsBox">
       		{if IsMobileUserAgent()}
            <a href="prefs.php?action={$item}&sid={$sid}">
                <h2>{lng p="$item"}</h2>
                {lng p="prefs_d_$item"}
                </a>
            {else}
			<h2><a href="prefs.php?action={$item}&sid={$sid}">{lng p="$item"}</a></h2>
			{lng p="prefs_d_$item"}
            {/if}
		</td>
	</tr>
	{/foreach}
	</table>
{else}
	<table width="100%" cellpadding="5">
		<tr>
		{assign var="i" value=0}
		{foreach from=$prefsItems item=null key=item}
		{assign var="i" value=$i+1}
		{if $i==3}
		{assign var="i" value=1}
		</tr>
		<tr>
		{/if}
		
		<td width="50%">
			<table width="100%" class="listTable">
				<tr>
					<th colspan="3" class="listTableHead"><a href="prefs.php?action={$item}&sid={$sid}"><b>{lng p="$item"}</b></a></th>
				</tr>
				<tr>
					<td class="listTableIconSide"><img src="{if $prefsImages[$item]}{$prefsImages[$item]}{else}{$tpldir}images/li/prefs_{$item}.png{/if}" width="48" height="48" border="0" alt="" /></td>
					<td valign="top" style="padding:6px;">{lng p="prefs_d_$item"}</td>
					<td class="listTableSide"><a href="prefs.php?action={$item}&sid={$sid}">&raquo;</a></td>
				</tr>
			</table>
		</td>
		
		{/foreach}
		{if $i<3}
		{math assign="i" equation="x - y" x=2 y=$i}
		{section loop=$i name=rest}
			<td>&nbsp;</td> 
		{/section}
		{/if}
		</tr>
	</table>
{/if}

</div></div>
