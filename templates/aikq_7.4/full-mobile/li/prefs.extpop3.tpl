<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_extpop3.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="extpop3"}
	</div>
</div>

<form name="f1" method="post" action="prefs.php?action=extpop3&do=action&sid={$sid}">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<tr>
		<th>
			<a href="prefs.php?sid={$sid}&action=extpop3&sort=p_user&order={$sortOrderInv}">{lng p="username"}</a>
			{if $sortColumn=='p_user'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th>
			<a href="prefs.php?sid={$sid}&action=extpop3&sort=p_host&order={$sortOrderInv}">{lng p="host"}</a>
			{if $sortColumn=='p_host'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th width="90">&nbsp;</th>
	</tr>
	
	{if $accountList}
	<tbody class="listTBody">
	{foreach from=$accountList key=accountID item=account}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td nowrap="nowrap" class="{if $sortColumn=='p_user'}listTableTDActive{else}{$class}{/if}">&nbsp;<img src="{$tpldir}images/li/ico_extpop3.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$account.p_user}</td>
		<td nowrap="nowrap" class="{if $sortColumn=='p_host'}listTableTDActive{else}{$class}{/if}">&nbsp;{text value=$account.p_host}:{$account.p_port}</td>
		<td class="{$class}" nowrap="nowrap">
			<a href="prefs.php?action=extpop3&do=edit&id={$accountID}&sid={$sid}"><i class="fa fa-pencil fa-2x fa-fw"></i></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="prefs.php?action=extpop3&do=delete&id={$accountID}&sid={$sid}"><i class="fa fa-times fa-2x fa-fw fm-delete"></i></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
</table>
</div>

</form>
