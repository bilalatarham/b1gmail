<div id="fmMainCont">
	<div id="contentHeader">
		<div class="left">
			<img src="{$tpldir}images/li/tab_ico_webdisk.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			<a href="#" onclick="fmWdItem('{$sid}',1, 0)">{lng p="webdisk"}</a> {foreach from=$currentPath item=folder} &raquo; <a href="#" onclick="fmWdItem('{$sid}',1, {$folder.id})">{text value=$folder.title}</a> {/foreach}
		</div>
	</div>
	
	{hook id="webdisk.folder.tpl:head"}
	
	{if $isShared}
	<form action="email.compose.php?sid={$sid}" method="post" name="mailForm">
		<input type="hidden" name="subject" value="{text value=$shareMailSubject allowEmpty=true}" />
		<textarea name="text" style="display:none">{text value=$shareMail allowEmpty=true}</textarea>
	</form>
	{/if}
	
	<form enctype="multipart/form-data" action="webdisk.php?folder={$folderID}&sid={$sid}" method="post" name="f1" onsubmit="transferSelectedWebdiskItems();">
	<input type="hidden" name="" value="" id="wdAction" />
	<input type="hidden" name="massAction" value="" id="wdMassAction" />
	<input type="hidden" name="selectedWebdiskItems" id="selectedWebdiskItems" value="" />
	
	<div class="scrollContainer withBottomBar noSelect" id="wdDnDArea">
	
		<div id="fmUpload" style="display: none;">
		<fieldset style="margin-top:1em;">
			<legend>{lng p="uploadfiles"}</legend>
			<table width="100%">
				{assign var="i" value=0}
				{section name=file loop=5}
				<tr>
					<td width="16"><img src="{$tpldir}images/li/webdisk_file.png" width="16" height="16" border="0" alt="" /></td>
					<td><input type="file" name="file{$i}" style="width: 100%;" size="30" /></td>
				</tr>
				{assign var="i" value=$i+1}
				{/section}
				<tr>
					<td>&nbsp;</td>
					<td><img src="{$tpldir}images/li/progressbar.gif" border="0" alt="" style="display:none;" id="progressBar" />&nbsp;<input id="sbButton" type="button" value="{lng p="ok"}" onclick="EBID('wdAction').name='action';EBID('wdAction').value='uploadFiles';EBID('progressBar').style.display='';this.disabled=true;document.forms.f1.submit();" /></td>
				</tr>
			</table>
		</fieldset>
 
		<h3>{lng p="createfolder"} </h3>     
  
			<table>
				<tr>
					<td width="16"><img src="{$tpldir}images/li/webdisk_folder.png" width="16" height="16" border="0" alt="" /></td>
					<td><input type="text" name="fmFolderName" id="fmFolderName" style="width: 100px;" /></td>
					<td><input type="submit" value="{lng p="ok"}" id="fmFolderNameSubmit" onclick="fmWdAddFolder(currentWebdiskFolderID, '{$sid}');" /><div id="fmFolderNameSubmitSpinner"</div></td>
				</tr>
			</table>
	
	</div>	
	
	{if $isShared}
		<div class="note" style="margin-bottom:1em;margin-top:1em;">
			<small>{lng p="sharednote"}</small><br />
			<img src="{$tpldir}images/li/ico_share.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <a target="_blank" href="{$shareURL}" style="color:blue;">{$shareURL}</a>
			<button onclick="document.forms.mailForm.submit();return(false);">
				<img src="{$tpldir}images/li/send_mail.png" border="0" alt="" align="absmiddle" width="12" />
				{lng p="sendmail2"}
			</button>
		</div>
	{/if}
	
	<table class="bigTable" id="wdContentTable">
		<tr>
			<th width="35">&nbsp;</th>
			<th>{lng p="filename"}</th>	
            <th width="40">&nbsp;</th>		
		</tr>
		{foreach from=$folderContent item=item}	
		{cycle values="listTableTR,listTableTR2" assign="class"}
		<tr class="{$class}" id="wli_{$item.type}_{$item.id}">
			<td class="fm-wd" id="fmIcon_{$item.type}_{$item.id}" style="text-align:center;" onclick="fmWdItem('{$sid}',{$item.type}, {$item.id})">				
            <i class="fa 
            {if $item.ext==".SHAREDFOLDER"}
            fa-group
            {elseif $item.ext==".FOLDER"}
            fa-folder-open
            {elseif $item.ext=="bmp" || $item.ext=="jpg" || $item.ext=="jpeg" || $item.ext=="gif" || $item.ext=="png" || $item.ext=="tif" || $item.ext=="tiff" || $item.ext=="jpeg" || $item.ext=="psd" || $item.ext=="ai" || $item.ext=="indd"}
            fa-file-image-o
            {elseif $item.ext=="crt" || $item.ext=="csr" || $item.ext=="key"}
            fa-key
            {elseif $item.ext=="doc" || $item.ext=="docx" || $item.ext=="rtf" || $item.ext=="sdw"}
            fa-file-word-o
            {elseif $item.ext=="ppt" || $item.ext=="pptx" || $item.ext=="pptm" || $item.ext=="potx" || $item.ext=="pot"}
            fa-file-powerpoint-o
            {elseif $item.ext=="xlsx" || $item.ext=="xlsm" || $item.ext=="xlsb" || $item.ext=="xltx" || $item.ext=="xltm" || $item.ext=="xls" || $item.ext=="xlt"}
            fa-file-excel-o
            {elseif $item.ext=="eml" || $item.ext=="msg"}
            fa-envelope-o
            {elseif $item.ext=="exe" || $item.ext=="com" || $item.ext=="bat"}
            fa-windows
            {elseif $item.ext=="htm" || $item.ext=="html" || $item.ext=="shtm" || $item.ext=="shtml" || $item.ext=="xml" || $item.ext=="php" || $item.ext=="js" || $item.ext=="css" || $item.ext=="asp" || $item.ext=="aspx"}
            fa-file-code-o
            {elseif $item.ext=="mp3" || $item.ext=="mid" || $item.ext=="wav"}
            fa-file-audio-o
            {elseif $item.ext=="mpg" || $item.ext=="mpeg" || $item.ext=="divx" || $item.ext=="avi" || $item.ext=="qt" || $item.ext=="mov"}
            fa-file-video-o
            {elseif $item.ext=="pdf" || $item.ext=="ps"}
            fa-file-pdf-o
            {elseif $item.ext=="txt" || $item.ext=="1st"}
            fa-file-text-o
            {elseif $item.ext=="vcf" || $item.ext=="vcard"}
            fa-qrcode
            {elseif $item.ext=="zip" || $item.ext=="rar" || $item.ext=="ace" || $item.ext=="gz" || $item.ext=="bz2" || $item.ext=="pak" || $item.ext=="pk3" || $item.ext=="gcf"}
            fa-file-zip-o
            {else}
            fa-file
            {/if} 
            
            fa-lg fa-fw"></i>
			</td>
			<td nowrap="nowrap" onclick="fmWdItem('{$sid}',{$item.type}, {$item.id})">
				{text value=$item.title}
			</td>	
            <td onclick="fmWdItemFunctions('{$item.ext}',{$item.type},{$item.id},'{$item.title}')" style="border-left: 1px solid #e0e0e0">
            <i class="fa fa-cog fa-2x fa-fw"></i>
            </td>		
		</tr>
		{/foreach}
	</table>
		
	
	</div>
	</form>
</div>							
	
	
	{hook id="webdisk.folder.tpl:foot"}
	
	{if !$smarty.post.inline}	
	
	<script language="javascript">
	<!--			
		currentWebdiskFolderID = {$folderID};
		var treeID = webdiskGetTreeIDbyFolderID({$folderID});
		if(treeID > 0)
			webdisk_d.openTo(treeID);
		initWDSel();
	//-->
	</script>
	{/if}



<div id="rightSidebar" style="display: none;">
	{include file="full-mobile/li/webdisk.sidebar.tpl"}
</div>



<div id="fmWdItemFunctions" style="display: none">
	<div id="fmFileInfo"></div>
	{if $allowShare}<div id="fmShare"><a href="javascript:void(0);" onclick="fmWdShare(currentWebdiskFolderID, '{$sid}')"> {lng p="sharing"}</a></div>{/if}
    <div id="fmRename"><a href="javascript:void(0);" onclick="fmWdRename(1, '{$sid}')">{lng p="rename"}</a></div>
    <div id="fmRenameSave" style="display: none;"><a href="javascript:void(0);" onclick="fmWdRename(2, '{$sid}')" id="fmRenameSaveLink">{lng p="save"}</a></div>
    <div id="fmDelete"><a href="javascript:void(0);" onclick="if(confirm('{lng p="realdel"}')) fmWdDelete(currentWebdiskFolderID, '{$sid}');">{lng p="delete"}</a></div>
</div>

<input type="hidden" id="currentSelected" name="currentSelected" value="1" />
<input type="hidden" id="currentSelectedType" name="currentSelectedType" value="1" />