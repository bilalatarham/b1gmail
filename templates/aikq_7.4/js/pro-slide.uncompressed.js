'use strict';

$(document).ready(function() {
	pro_slide.init();
});

var pro_slide = {
	/* ===== init ===== */
	init: function() {
		var $window = $(window);
		var $about = $('#about');
		var $nav = $('#main-nav');

		if(!$about.length)
			return;

		$window.on('scroll', function() {
			if($window.scrollTop() >= $about.offset().top) {
				$nav.addClass('navbar-inverse');
			} else {
				$nav.removeClass('navbar-inverse');
			}
		});

		$('#ps-scrolldown').on('click', function() {
			$('html, body').animate({
				scrollTop: $about.offset().top + 1
			}, 1000);
		});
	}
};
