<!-- Remove this demo content and replace it with your own. Describe your services, write something about you, ... -->
<!-- Many of the listings here were inspired by https://getbootstrap.com/docs/3.3 -->
<div class="page-header">
	<h1>About.tpl</h1>
</div>

<h1 class="text-center">Headings</h1>
<h1>I am a h1 heading</h1>
<h2>I am a h2 heading</h2>
<h3>I am a h3 heading</h3>
<h4>I am a h4 heading</h4>
<h5>I am a h5 heading</h5>
<h6>I am a h6 heading</h6>
<br>

<h1 class="text-center">Buttons</h1>
<blockquote>
	Please note that btn-primary and btn-success are designed in the same way here,	since btn-success is often used in the same way as a primary button.
</blockquote>

<button type="button" class="btn">Basic</button>
<button type="button" class="btn btn-default">Default</button>
<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-link">Link</button>
<br>

<h1 class="text-center">Button sizes</h1>
<button type="button" class="btn btn-primary btn-lg">Large button</button>
<button type="button" class="btn btn-primary">Default button</button>
<button type="button" class="btn btn-primary btn-sm">Small button</button>
<button type="button" class="btn btn-primary btn-xs">Extra small button</button>

<h1 class="text-center">Alerts</h1>
<div class="alert alert-success" role="alert">Success</div>
<div class="alert alert-info" role="alert">Info</div>
<div class="alert alert-warning" role="alert">Warning</div>
<div class="alert alert-danger" role="alert">Danger</div>
