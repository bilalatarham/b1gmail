<br />
<table>
	<tr>
		<td valign="top" width="64" align="center"><img src="{$tpldir}images/li/msg.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			<b>{lng p="uploadfiles"}</b>
			<br /><br />
			
			<table>
				{foreach from=$success key=file item=msg}
				<tr>
					<td width="16"><img src="{$tpldir}images/li/ico_ok.png" width="16" height="16" border="0" alt="" /></td>
					<td><b>{text value=$file cut=30}</b>&nbsp;&nbsp;</td>
					<td>{$msg}</td>
				</tr>
				{/foreach}
				{foreach from=$error key=file item=msg}
				<tr>
					<td width="16"><img src="{$tpldir}images/li/ico_error.png" width="16" height="16" border="0" alt="" /></td>
					<td><b>{text value=$file cut=30}</b>&nbsp;&nbsp;</td>
					<td>{$msg}</td>
				</tr>
				{/foreach}
			</table>
			
			<br /><input type="button" value="&laquo; {lng p="back"}" onclick="document.location.href='webdisk.php?folder={$folderID}&sid={$sid}';" />
		</td>
	</tr>
</table>