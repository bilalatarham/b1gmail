<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="cancelmembership"}
	</div>
</div>

<div class="scrollContainer"><div class="pad">

<form action="prefs.php?sid={$sid}" method="post">
<input type="hidden" name="action" value="membership" />
<input type="hidden" name="do" value="reallyCancelAccount" />
<input type="hidden" name="really" id="really" value="false" />

<table>
	<tr>
		<td valign="top" width="64" align="center"><img src="{$tpldir}images/li/msg.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			{lng p="canceltext"}
			<br /><br />
			<input type="button" value="&laquo; {lng p="back"}" onclick="history.back();" />
			<input type="submit" value=" {lng p="cancelmembership"} (30) " disabled="disabled" id="cancelButton" />
		</td>
	</tr>
</table>
</form>

<script language="javascript">
<!--
	{literal}var i = 30;
	
	function cancelTimer()
	{
		i--;
	
		if(i==0)
		{
			EBID('cancelButton').value = '{/literal}{lng p="cancelmembership"}{literal}';
			EBID('cancelButton').disabled = false;
			EBID('cancelButton').className = 'primary';
			EBID('really').value = 'true';
		}
		else
		{
			EBID('cancelButton').value = '{/literal}{lng p="cancelmembership"}{literal} (' + i + ')';
			window.setTimeout('cancelTimer()', 1000);
		}
	}
	
	window.setTimeout('cancelTimer()', 1000);{/literal}
//-->
</script>

</div></div>
