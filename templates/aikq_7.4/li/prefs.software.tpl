<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_software.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="software"}
	</div>
</div>

<div class="scrollContainer"><div class="pad">
	<table class="listTable">
		<tr>
			<th class="listTableHead"> {lng p="software"}</th>
		</tr>
		<tr>
			<td class="pad">
				<p>
					{$introText}
				</p>
				
				<table>
					{if $releaseFiles.win}
					<tr>
						<td valign="top" width="64">
							<img src="{$tpldir}images/li/os_win.png" border="0" alt="" />
						</td>
						<td>
							<strong>Windows</strong> <small>(Version {$verNo})</small>
							<p>
								{lng p="software_win"}
							</p>
							<button onclick="document.location.href='prefs.php?action=software&do=download&os=win&sid={$sid}';">
								<img src="{$tpldir}images/li/ico_software.png" border="0" alt="" align="absmiddle" />
								{lng p="download"}
								<small>&nbsp;({size bytes=$fileSizes.win})</small>
							</button>
							<br /><br /><br />
						</td>
					</tr>
					{/if}
					
					{if $releaseFiles.mac}
					<tr>
						<td valign="top" width="64">
							<img src="{$tpldir}images/li/os_mac.png" border="0" alt="" />
						</td>
						<td>
							<strong>Mac</strong> <small>(Version {$verNo})</small>
							<p>
								{lng p="software_mac"}
							</p>
							<button onclick="document.location.href='prefs.php?action=software&do=download&os=mac&sid={$sid}';">
								<img src="{$tpldir}images/li/ico_software.png" border="0" alt="" align="absmiddle" />
								{lng p="download"}
								<small>&nbsp;({size bytes=$fileSizes.mac})</small>
							</button>
							<br /><br />
						</td>
					</tr>
					{/if}
				</table>
			</td>
		</tr>
	</table>
</div></div>
