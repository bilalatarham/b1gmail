{hook id="email.sidebar.tpl:head"}

<div class="sidebarHeading">{lng p="email"}</div>
<div class="contentMenuIcons">
	<a href="email.compose.php?sid={$sid}"><img src="{$tpldir}images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="sendmail"}</a><br />
	<a href="email.folders.php?sid={$sid}"><img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="folderadmin"}</a><br />
	{hook id="email.sidebar.tpl:email"}
</div>

<div class="sidebarHeading">{lng p="folders"}</div>
<div class="contentMenuIcons" id="folderList">
</div>
<script language="javascript">
<!--
	{include file="li/email.folderlist.tpl"}
	EBID('folderList').innerHTML = d;
	enableFolderDragTargets();
//-->
</script>

{hook id="email.sidebar.tpl:foot"}
