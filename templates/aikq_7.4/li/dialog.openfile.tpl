<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{$title}</title>

	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />

	<!-- client scripts -->
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js" type="text/javascript" language="javascript"></script>
</head>

<body>

		{$text}

		<form action="{$formAction}" enctype="multipart/form-data" method="post">
			<br /><br />
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20"><img src="{$tpldir}images/li/webdisk_file.png" width="16" height="16" border="0" alt="" /></td>
					<td>{fileSelector name="$fieldName" multiple=$multiple}</td>
				</tr>
			</table>

			<p>
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left">
							{if $bar}
								{progressBar value=$bar.value max=$bar.max width=100}
							{/if}
						</td>
						<td align="right">
							<input type="button" onclick="parent.hideOverlay()" value="{lng p="cancel"}" />
							<input type="submit" value="{lng p="ok"}" />
						</td>
					</tr>
				</table>
			</p>
		</form>

</body>

</html>
