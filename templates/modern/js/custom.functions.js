/*
 * b1gMail Custom Functions
 *
 */

function sortWidgets() {
    if (typeof (order) == 'undefined' || order == null)
        return;

    var orderArr = order.split(';');
    var currentElement = '';

    for (i = 0; i < orderArr.length; i++) {
        if (orderArr[i] != '') {
            currentElement = $("#sortable #" + orderArr[i]).clone();
            $("#sortable #" + orderArr[i]).remove();
            $("#sortable").append(currentElement);
        }
    }
}

function initWidgetSortable() {
    var elem = $('#dropzone');

    elem.on('drop', function (e) {
        dropFile(e.originalEvent);
    });

    $("#sortable").sortable({
        handle: '.handle',
        delay: 0,
        animation: 150,
        // use stop: event for jquery ui
        onEnd: function (event, ui) {
            var order = '';
            $('#sortable').children('.sortable-element').each(function () {
                order += $(this).attr('key') + ';';
            });

            $.ajax({
                url: 'start.php?action=saveWidgetOrder&order=' + order + '&sid=' + currentSID,
                type: 'get',
                success: function (response) {
                    console.log(response);
                }
            });
        }
    });
    //$("#sortable").disableSelection(); // for jquery ui
}

function sortOrganizerWidgets() {
    if (typeof (order) == 'undefined' || order == null)
        return;

    var orderArr = order.split(';');
    var currentElement = '';

    for (i = 0; i < orderArr.length; i++) {
        if (orderArr[i] != '') {
            currentElement = $("#organizer-sortable #" + orderArr[i]).clone();
            $("#organizer-sortable #" + orderArr[i]).remove();
            $("#organizer-sortable").append(currentElement);
        }
    }
}

function initOrganizerWidgetSortable() {
    var elem = $('#dropzone');

    elem.on('drop', function (e) {
        dropFile(e.originalEvent);
    });

    $("#organizer-sortable").sortable({
        handle: '.handle',
        delay: 0,
        animation: 150,
        onEnd: function (event, ui) {
            var order = '';
            $('#organizer-sortable').children('.organizer-sortable-element').each(function () {
                order += $(this).attr('key') + ';';
            });

            $.ajax({
                url: 'organizer.php?action=saveWidgetOrder&order=' + order + '&sid=' + currentSID,
                type: 'get',
                success: function (response) {
                    console.log(response);
                }
            });
        }
    });
}

function initMasonry() {
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 580
    });
}

var pbText, pbValue, currentXH, urlAddFunc, fileDoneAction;

function dropFile(event) {
    var files;

    event.stopPropagation();
    event.preventDefault();

    files = event.dataTransfer.files;

    if (typeof (files) == 'undefined' || files == null || files.length <= 0)
        return;

    var done = function () {
        ol.hide();
        document.location.href = 'webdisk.php?sid=' + currentSID + '&folder=0';
    };

    var spinImg = document.createElement('img');
    spinImg.setAttribute('border', 0);
    spinImg.setAttribute('src', tplDir + 'images/load_32.gif');

    pbValue = document.createElement('div');
    pbValue.setAttribute('class', 'progressBarValue');
    pbValue.style.width = '0px';

    var pbDiv = document.createElement('div');
    pbDiv.setAttribute('class', 'progressBar');
    pbDiv.style.marginLeft = 'auto';
    pbDiv.style.marginRight = 'auto';
    pbDiv.style.width = '200px';
    pbDiv.appendChild(pbValue);

    var cancelButton = document.createElement('button');
    cancelButton.appendChild(document.createTextNode(lang['cancel']));
    addEvent(cancelButton, 'click', function () {
        if (currentXH)
            currentXH.abort();
        done();
    });

    var olDiv = document.createElement('div');
    olDiv.style.paddingTop = '1.5em';
    olDiv.style.textAlign = 'center';
    olDiv.appendChild(spinImg);
    olDiv.appendChild(document.createElement('br'));
    olDiv.appendChild(document.createElement('br'));
    olDiv.appendChild(pbText = document.createTextNode(lang['uploading'] + '...'));
    olDiv.appendChild(document.createElement('br'));
    olDiv.appendChild(document.createElement('br'));
    olDiv.appendChild(pbDiv);
    olDiv.appendChild(document.createElement('br'));
    olDiv.appendChild(cancelButton);

    var ol = new Overlay(true);
    ol.setSize(420, 165);
    ol.setCaption(lang['uploading']);
    ol.olContent.appendChild(olDiv);
    ol.show();

    uploadFile(done, files);
}

function uploadFile(done, files) {
    var i = 0;
    var reader = new FileReader(), file = files[i];
    var url = 'webdisk.php?sid=' + currentSID + '&folder=0&action=dndUpload';

    reader.onerror = function (event) {

    };
    reader.onloadend = function (event) {
        var data = event.target.result, commaPos;
        if (data == 'data:') {
            data = '';
        }
        else {
            if (data == null || (commaPos = data.indexOf(',')) > data.length) {
                i++;

                if (i >= files.length - 1)
                    done();
                else
                    uploadFile(done);

                return;
            }
            data = data.substring(commaPos + 1);
        }

        var xh = GetXMLHTTP();
        if (xh) {
            currentXH = xh;
            var _this = this;
            addEvent(xh.upload, 'progress', function (event) {
                if (event.lengthComputable) {
                    var progress = event.loaded / event.total;
                    pbValue.style.width = Math.ceil(progress * 198) + 'px';
                }
            });
            xh.open('POST', url + '&filename=' + encodeURIComponent(file.name)
                + '&size=' + file.size
                + '&type=' + encodeURIComponent(file.type)
                + (urlAddFunc ? urlAddFunc() : ''), true);
            xh.setRequestHeader('Content-Type', 'application/octet-stream');

            xh.onreadystatechange = function () {
                if (xh.readyState == 4) {
                    if (fileDoneAction)
                        fileDoneAction(xh.responseText);

                    i++;

                    if (i >= files.length)
                        done();
                    else
                        uploadFile(done);
                }
            };

            xh.send(data);
        }
    };

    var fileNameShort = file.name;
    if (fileNameShort.length > 25)
        fileNameShort = fileNameShort.substring(0, 22) + '...';
    pbText.nodeValue = lang['uploading'] + ': "' + fileNameShort + '" (' + (i + 1) + ' / ' + files.length + ')';
    pbValue.style.width = '1px';
    reader.readAsDataURL(file);
}

sortWidgets();
initWidgetSortable();
sortOrganizerWidgets();
initOrganizerWidgetSortable();
initMasonry();