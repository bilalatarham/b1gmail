$.getDocHeight = function(){
    return Math.max(
        $(document).height(),
        $(window).height(),
        /* For opera: */
        document.documentElement.clientHeight
    );
};
$.getDocWidth = function(){
    return Math.max(
        $(document).width(),
        $(window).width(),
        /* For opera: */
        document.documentElement.clientWidth
    );
};
function slidemenu(state) {
	var button = document.getElementById("menu-button");
	var navigation = document.getElementById("mainTabs");
	var content = document.getElementById("mainContent");
	var overlay = document.getElementById("mobile-overlay-mainTabs");
	var menuright = document.getElementById("mobilemenu-right");
	if(document.getElementById("mainmenu-state").checked && state != "open") {
            navigation.style.cssText = "transform: translatex(0px) scale(0.8, 0.8);-moz-transform: translatex(0px) scale(0.8, 0.8);-o-transform: translatex(0px) scale(0.8, 0.8);-webkit-transform: translatex(0px) scale(0.8, 0.8);-ms-transform: translatex(0px) scale(0.8, 0.8);";
			button.style.cssText = "transform: translatex(0px);-moz-transform: translatex(0px);-o-transform: translatex(0px);-webkit-transform: translatex(0px);-ms-transform: translatex(0px);";
			content.style.cssText = "transform: none;-moz-transform: none;-o-transform: none;-webkit-transform: none;-ms-transform: none; left: 0; richt: 0;";
			overlay.style.cssText = "display: none;";
			menuright.style.cssText = "display: block";
			document.getElementById("mainmenu-state").checked = false;
         }
         else if(state != "close") {
           navigation.style.cssText = "transform: translatex(200px) scale(1, 1);-moz-transform: translatex(208px) scale(1, 1);-o-transform: translatex(208px) scale(1, 1);-webkit-transform: translatex(208px) scale(1, 1);-ms-transform: translatex(208px) scale(1, 1);";
		   button.style.cssText = "transform: translatex(200px);-moz-transform: translatex(200px);-o-transform: translatex(200px);-webkit-transform: translatex(200px);-ms-transform: translatex(200px);";
		   content.style.cssText = "transform: translatex(200px);-moz-transform: translatex(200px);-o-transform: translatex(200px);-webkit-transform: translatex(200px);-ms-transform: translatex(200px); left: 1px; right: -1px;";
		   overlay.style.cssText = "display: block;";
		   menuright.style.cssText = "display: none";
		   document.getElementById("mainmenu-state").checked = true;
        }
}
function dropmenu(state) {
		if(document.getElementById("dropmenu-state").checked && state != "close") {
			$( "#dropmenu" ).css({height: $.getDocHeight() - 50, width: $.getDocWidth()});
			$( "#dropmenu" ).show();
			$( "#dropmenu-content" ).show();
			document.getElementById("dropmenu-state").checked = false;
		}
		  else if(state != "open") {
			   $( "#dropmenu" ).hide();
			   $( "#dropmenu-content" ).hide();
			  document.getElementById("dropmenu-state").checked = true;
        }
}
function dropcheckbox(id) {
	if(document.getElementById(id).checked) {
		document.getElementById(id).checked = false;
	}
	else {
		document.getElementById(id).checked = true;
	};						   
							   
}

function dropselect(id,current) {
	document.getElementById(id).selectedIndex = document.getElementById(current).selectedIndex;	
}

$(document).ready(function() {
		$( "#dropmenu-content" ).hide();
		$( "#dropmenu" ).hide();
		$( "#dropmenu" ).css({height: $.getDocHeight() - 50, width: $.getDocWidth()});	 
		$( "#hSep1" ).css({height: $.getDocHeight() - 120, width: $.getDocWidth()});	 
		$( "#composeText" ).css({height: $( "#emailText" ).height(),});	 
		$( "tr" ).has( "label[for='widget_BMPlugin_Widget_WebdiskDND']" ).css( "display", "none" );
		$('textarea').on('keyup',function(){
			$(this).css('height','auto');
			$(this).height(this.scrollHeight + 30);
			$('#composeText').height(this.scrollHeight + 40);
   		 });
		
		$('body').on('click','#folderMailArea #mailTable tr td input:checkbox',function(){			
			var count_checked = $("#folderMailArea #mailTable tr td input:checkbox:checked").length;
			if($(this).prop('checked') == false) {
				count_checked++;
			}
			else {
				count_checked--;	
			}
			if(count_checked == 0) {
				$('#massActionHolder').hide();
				$('#folderTitle').show();
			}
			else {
				$('#massActionHolder').show();
				$('#folderTitle').hide();
			}
			
		});
		$(window).load(function(){
			if($('#emailText').length != 0) {
				$('#emailText').height($('#emailText')[0].scrollHeight + 30);
			}
		});
});
function addressOverview(state, tpldir, addressID) {
	if(state == 1) {	
		$( "#hSep1" ).css({left: 0, width: 0});
		$( "#hSep2" ).css({left: 0, width: '100%'});
		$( "#addressBack" ).show();
		$( "#addressAdd" ).hide();
		$( "#addressEdit" ).show();
		$( "#currentSelected" ).val(addressID);
		
	}
	else {
		$( "#hSep1" ).css({left: 0, width: '100%'});
		$( "#hSep2" ).css({left: 0, width: 0});
		$( "#previewArea" ).hide();
		$( "#addressBack" ).hide();
		$( "#addressEdit" ).hide();
		$( "#multiSelPreview" ).show();		
		$( "#addressAdd" ).show();
	}
}
function addressEdit(sid) {
	document.location.href='organizer.addressbook.php?action=editContact&id='+$( "#currentSelected" ).val()+'&sid='+sid;
}

function noteOverview(state, tpldir, noteID) {
	if(state == 1) {
		$( "#hSep1" ).css({left: 0, width: 0});
		$( "#hSep2" ).css({left: 0, width: '100%'});
		$( "#noteBack" ).show();
		$( "#noteAdd" ).hide();
		$( "#noteEdit" ).show();
		$( "#currentSelected" ).val(noteID);
		$( "#notePreview" ).html('<div id="multiSelPreview_vCenter"><i class="fa fa-refresh fa-spin fa-2x"></i></div>');
	}
	else {		
		$( "#hSep1" ).css({left: 0, width: '100%'});
		$( "#hSep2" ).css({left: 0, width: 0});
		$( "#noteBack" ).hide();
		$( "#noteEdit" ).hide();		
		$( "#noteAdd" ).show();
		
	}
}

function noteEdit(sid) {
	document.location.href='organizer.notes.php?action=editNote&id='+$( "#currentSelected" ).val()+'&sid='+sid;
}

function taskOverview(state, tpldir, taskListID) {
	if(state == 1) {
		$( "#taskList" ).css({left: 0, width: 0, height: 0});
		$( "#taskListContainer" ).css({left: 0, width: '100%'});
		$( "#taskBack" ).show();
		$( "#taskAdd" ).show();		
		$( "#currentSelected" ).val(taskListID);
	}
	else {	
		$( "#taskList" ).css({left: 0, width: '100%', height: 'auto'});
		$( "#taskListContainer" ).css({left: 0, width: 0});
		$( "#taskBack" ).hide();		
		$( "#taskAdd" ).hide();
		
	}
}
function taskAdd(sid) {
	document.location.href='organizer.todo.php?action=addTask&taskListID='+$( "#currentSelected" ).val()+'&sid='+sid;
}

function fmAddTodoList(sid) {
	var title = $( "#addListTitle" ).val();
	$( "#multiSelPreview_vCenter" ).show();	
	$.ajax({
	url: "organizer.todo.php?action=addList&title="+title+"&sid="+sid,
	context: document.body
	}).done(function() {
		location.reload();
	});
}

function fmWdItem(sid, type, id) {
	if(type == 1) {
		document.location.href = 'webdisk.php?folder='+id+'&sid='+sid;
	}
	else {
		document.location.href = 'webdisk.php?action=downloadFile&id='+id+'&sid='+sid;
	}
}
function fmWdItemFunctions(ext, type, id, name) {	
	$( "#currentSelected" ).val(id);	
	$( "#currentSelectedType" ).val(type);
	$( "#fmFileInfo" ).html('<div class="fm-wd">'+$( "#fmIcon_"+type+"_"+id ).html()+'</div> <div id="fmWdItemName">'+name+"</div>");
	if(ext != ".SHAREDFOLDER" && ext != ".FOLDER")
		$( "#fmShare" ).hide();
	else
		$( "#fmShare" ).show();
	$( "#fmWdItemFunctions" ).show();
	$( "#wdBack" ).show();
	$( "#wdAdd" ).hide();
	$( "#fmMainCont" ).hide();
}

function fmWdDelete(folderID, sid) {
	document.location.href='webdisk.php?action=deleteItem&type='+$( "#currentSelectedType" ).val()+'&folder='+folderID+'&id='+$( "#currentSelected" ).val()+'&sid='+sid;
}
function fmWdRename(step, sid) {
	var name = $( "#fmWdItemName" ).html();
	if(step == 1) {
	$( "#fmWdItemName" ).html('<input type="text" value="'+name+'" name="fmWdItemRename" id="fmWdItemRename" />');
	$( "#fmRename" ).hide();
	$( "#fmRenameSave" ).show(); 
	}
	else if(step == 2) {
		$( "#fmRenameSaveLink" ).html('<i class="fa fa-refresh fa-spin fa-lg"></i>'); 
	$.ajax({
	url: 'webdisk.php?action=renameItem&type='+$( "#currentSelectedType" ).val()+'&id='+$( "#currentSelected" ).val()+'&name='+$( "#fmWdItemRename" ).val()+'&sid='+sid,
	context: document.body
	}).done(function() {
		location.reload();
	});		
	}
	else {
		$( "#fmRename" ).show();
		$( "#fmRenameSave" ).hide(); 
		$( "#fmWdItemName" ).html($( "#fmWdItemRename" ).val());
	}
}
function fmWdShare(folderID, sid) {
	document.location.href='webdisk.php?action=shareFolder&folder=' + folderID + '&id=' + $( "#currentSelected" ).val() + '&sid='+sid;
}
function fmWdAdd() {
	$( "#fmUpload" ).show();
	$( "#wdBack" ).show();
	$( "#wdAdd" ).hide();
	$( "#wdContentTable" ).hide();
}
function fmWdBack() {
	if($('#fmWdItemFunctions').is(':visible')) {
		fmWdRename(0);
	}
	$( "#fmUpload" ).hide();
	$( "#fmWdItemFunctions" ).hide();
	$( "#wdBack" ).hide();
	$( "#wdAdd" ).show();
	$( "#wdContentTable" ).show();
	$( "#fmMainCont" ).show();
}

function fmWdAddFolder(folderID, sid) {
	$( "#fmFolderNameSubmit" ).hide();
	$( "#fmFolderNameSubmitSpinner" ).html('<i class="fa fa-refresh fa-spin fa-2x"></i>');
	$.ajax({
	url: 'webdisk.php?action=createFolder&rpc=true&folder='+currentWebdiskFolderID+'&folderName='+$( "#fmFolderName" ).val()+'&sid=' + sid,
	context: document.body
	}).done(function() {
		location.reload();
	});		
}

function fmIframeStyle(additional) {
	$('iframe').load(function() {
		$('iframe').contents().find("head").append($('<link href="'+tplDir+'style/full-mobile.css" rel="stylesheet" type="text/css" />'));
		if(additional) {
			$('iframe').contents().find("head").append($('<style type="text/css">'+additional+'</style>'));	
		}
	});
};

function fmAddAttachment() {
	addAttachment(currentSID);
	fmIframeStyle('table tr table tr td:first-child { display: none;}');
}
function fmOpenAddressbook(email) {
	openAddressbook(currentSID,email);
	fmIframeStyle();
}