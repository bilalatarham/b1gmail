<div data-role="header" data-position="fixed">
	<h1>{$pageTitle}</h1>
</div>

<div data-role="content">
	<h2>E-Mail Client Konfigurator</h2>
	{if $browser}
		<p>Mit unserem E-Mail Konfigurator wir das Einrichten auf Ihrem Apple iOS Gerät zum Kinderspiel.</p>

		<hr />

		{if !$step || $step == ''}
			<form action="clientconf.php?do=step1" method="post" data-ajax="false">
				<input type="hidden" name="do" value="login" />

				<div data-role="fieldcontain">
					<label for="email">Welche E-Mailadresse möchten Sie einrichten?</label>
					<input type="email" name="email" id="email" value="{text value=$smarty.cookies.bm_msavedUser allowEmpty=true}"  />
				</div>

				<button type="submit">Weiter</button>
			</form>


		{else if $step == 'Step1'}
			<h3>E-Mailprogramm einrichten</h3>

			<div style="padding: 10px; margin-bottom: 10px; border: 1px solid #DDDDDD; background-color: #FFFFCC;">
				Sie ben&ouml;tigen folgende Angaben:
				<ol>
					<li>{$email}</li>
					<li>Ihr Passwort</li>
				</ol>
			</div>

			<strong>Anleitung</strong>
			<ol>
				<li>&Ouml;ffnen Sie Ihr pers&ouml;nliches Profil:<br /><a href="{$profile}" target="_blank">Profil f&uuml;r <strong>{$email}</strong> &ouml;ffnen</a></li>
				<li>W&auml;hlen Sie "Installieren".</li>
				<li>(Optional) Falls Sie Ihr Telefon mit einem Pin gesch&uuml;tzt haben, geben Sie nun Ihren Code ein. Ansonsten bitte bei Schritt 4 weiterfahren.</li>
				<li>W&auml;hlen Sie "Installieren".</li>
				<li>Geben Sie Ihren Vor- und Nachnamen oder auch Ihren Firmennamen ein. Diese Eingabe wird dem Empf&auml;nger beim Erhalt eines E-Mails als Absendername angezeigt.</li>
				<li>Geben Sie das Passwort der zu installierenden E-Mailadresse an.</li>
				<li>Sie sehen nun nochmals eine &uuml;bersicht des zu installierenden E-Mailkontos. Mit dem Antippen von "Fertig" schliessen Sie die Installation ab.</li>
			</ol>

			<div style="padding: 10px; border: 1px solid #DDDDDD; background-color: #FFFFCC;">
				<strong>Automatische Konfiguration mit iCloud</strong><br />
				Wenn Sie iCloud nutzen, wird Ihr Profil auf alle Ihre Ger&auml;te &uuml;bertragen. Richten Sie Ihre Adresse z.B. auf einem iPhone ein, k&ouml;nnen Sie Ihre E-Mailadresse auch auf Ihrem Mac oder iPad nutzen.
			</div>



		{/if}


		<div class="bottomLink">
			<a href="{$selfurl}?noMobileRedirect=true&action=ClientConf" rel="external">{lng p="desktopversion"}</a>
		</div>

	{else}
		Das einrichten auf iOS Geräten funktioniert nur mit der Mobilen Safari Version.<br /><br />
		Auf der <a href="{$selfurl}?noMobileRedirect=true&action=ClientConf" rel="external">{lng p="desktopversion"}</a> gibt es noch weitere Anleitungen zum einrichten von anderen Geräten oder Programmen.
	{/if}
</div>
