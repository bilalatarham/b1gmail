<!-- Banner Section -->
<section class="site-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="banner-content">
                    <h1>Take Control of Your Inbox Today.</h1>
                    <p>One of the most cost effective digital marketing channels</p>
                    <div class="banner-btn-wrap">
                        <a href="#" class="btn btn-white btn-started"><span>Get started</span></a>
                        <a href="#" class="btn btn-white btn-quote"><span>Get a quote</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="img-wrapper">
                    <img src="{$tpldir}pages_assets/images/banner-left-img.svg" class="img-fluid" alt="banner-imag">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->
<!-- Platform Section -->
<section class="platform">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="platform-content">
                    <h1>All-in-One Platform</h1>
                    <p>Nam posuere sagittis jubs risus porta nonebe</p>
                    <div class="sub-content">
                        <p>Integer tincidunt pellentesque massa, non porta eros efficitur eget. Donec sed nibh pellentesque, placerat neque vitae, rutrum magna. Vivamus dignissim, dui vehicula ve lacinia, velit nulla consequat nibh, non fermentum nibh tortor vel metus. Nam elementum lorem at ligula sewn dignissim efficitur. Curabitur faucib us convallis blandit. Nullam nisl eros, blandit sit amet tincidunt at, tempus eus in libero nec purus molestie facilisi gsed hendrerit nequat arcu.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->
<!-- Features Section -->
<section class="features">
    <div class="container">
        <h1>Most Amazing Features</h1>
        <p>Neque porro quisquam esvetura dolor</p>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img1.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Aliases</h2>
                        <p>Duis imperdiet, ante vel molestie condimen ditum, erat velit ornare nisl.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img2.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Vouchers</h2>
                        <p>Cras bibendum, ant diultrices iaculis, tortor nibh molestie maegna.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img3.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>filters</h2>
                        <p>Btibulum erat risus, facilisis a hendrerit non, sagittis evpt libero auris venenatis.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img4.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Address Book</h2>
                        <p>Duis imperdiet, ante vel molestie condimen ditum, erat velit ornare nisl.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img5.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Bulk Download</h2>
                        <p>Cras bibendum, ant diultrices iaculis, tortor nibh molestie maegna.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img6.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Custom Signatures</h2>
                        <p>Btibulum erat risus, facilisis a hendrerit non, sagittis evpt libero auris venenatis.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img7.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Auto Responder</h2>
                        <p>Duis imperdiet, ante vel molestie condimen ditum, erat velit ornare nisl.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img8.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Anti-spam</h2>
                        <p>Cras bibendum, ant diultrices iaculis, tortor nibh molestie maegna.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="feature-content">
                    <div class="img-wrap">
                        <img src="{$tpldir}pages_assets/images/feature-img9.svg" class="img-fluid" alt="Feature img">
                    </div>
                    <div class="content-wrap">
                        <h2>Calendar</h2>
                        <p>Btibulum erat risus, facilisis a hendrerit non, sagittis evpt libero auris venenatis.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End -->
<!-- Goals Section -->
<section class="goals">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="goals-content">
                    <h1>Achieve Your Goals with <span class="text-clr">email</span><span class="text-reg">immunity</span></h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                    <ul class="list-unstled">
                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                        <li>Biusmod tempor incididunt ut labore et dolore magna</li>
                        <li>Quis ipsum suspendisse ultrices gravida</li>
                        <li>Risus commodo viverra maecenas accumsan lacus velde </li>
                    </ul>
                    <a href="#" class="btn btn-white btn-green"><span>Get started</span></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="img-wrap">
                    <img src="{$tpldir}pages_assets/images/goals-img.svg" class="img-fluid" alt="goals">
                </div>
            </div>
        </div>
        <div class="company-content">
            <h1>3000+ companies are delivering</h1>
            <p>conversational experiences with <span class="text-clr">email</span><span class="text-reg">immunity</span></p>	
            <div class="img-wrap">
                <img src="{$tpldir}pages_assets/images/companies-img.svg" class="img-fluid" alt="Companies">
            </div>
        </div>
    </div>
</section>
<!-- End -->