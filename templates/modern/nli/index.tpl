<!DOCTYPE html>
<html lang="{lng p="langCode"}">

<head>
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	{if $robotsNoIndex}<meta name="robots" content="noindex" />{/if}

	<title>{$service_title}{if $pageTitle} - {text value=$pageTitle}{/if}</title>
	<link rel="icon" href="{$tpldir}pages_assets/images/favicon.png" type="image/x-icon">
	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{$tpldir}pages_assets/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="{$tpldir}pages_assets/css/font-awesome.min.css" />
	<link type="text/css" rel="stylesheet" href="{$tpldir}pages_assets/css/style.css" />

	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script language="javascript" type="text/javascript">
	<!--
		var tplDir = '{$tpldir}', sslURL = '{$ssl_url}', serverTZ = {$serverTZ};
	//-->
	</script>

	<script src="clientlang.php" language="javascript" type="text/javascript"></script>
	<script type="application/javascript" src="{$tpldir}pages_assets/js/jquery.js"></script>
	<script type="application/javascript" src="{$tpldir}pages_assets/js/popper.js"></script>
	<script type="application/javascript" src="{$tpldir}pages_assets/js/bootstrap.min.js"></script>
	<script src="{$tpldir}js/nli.main.js?{fileDateSig file="js/nli.main.js"}"></script>
	{hook id="nli:index.tpl:head"}

</head>
<body>
	{hook id="nli:index.tpl:beforeContent"}
	<div class="wrapper">
		<!--Header Section-->
		<header class="site-header">
			<div class="container">
				<div class="main-menu">
					<div class="masthead-des">
						<!-- navbar-->
						<nav class="navbar navbar-expand-lg navbar-light main-nav">
							<a class="navbar-brand js-scroll-trigger" href="index.php">
								<img class="logo-img" src="{$tpldir}pages_assets/images/logo.svg" alt="logo"></a>
								<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
								<div class="collapse navbar-collapse" id="navbarResponsive">
								<ul class="navbar-nav list-unstyled">
									
									<li {if $smarty.request.action=='home'} class="nav-item active"{/if}>
										<a href="index.php">{lng p="home"}</a>
									</li>
									
									{foreach from=$pluginUserPages item=item}{if $item.top&&$item.after=='login'}
										<li {if $item.active} class="nav-item active"{/if}>
											<a href="{$item.link}">{$item.text}</a>
										</li>

									{/if}{/foreach}
										{if $_regEnabled||(!$templatePrefs.hideSignup)}
											<li{if $smarty.request.action=='signup'} class="nav-item active"{/if}>
												<a href="{if $ssl_signup_enable}{$ssl_url}{/if}index.php?action=signup">{lng p="signup"}</a>
											</li>
										{/if}

									{foreach from=$pluginUserPages item=item}{if $item.top&&$item.after=='signup'}
										<li{if $item.active} class="nav-item active"{/if}>
											<a href="{$item.link}">{$item.text}</a>
										</li>

									{/if}{/foreach}
										<li{if $smarty.request.action=='faq'} class="active"{/if}>
											<a href="index.php?action=faq">{lng p="faq"}</a>
										</li>

									{foreach from=$pluginUserPages item=item}{if $item.top&&$item.after=='faq'}
										<li{if $item.active} class="nav-item active"{/if}>
											<a href="{$item.link}">{$item.text}</a>
										</li>

									{/if}{/foreach}
										<li{if $smarty.request.action=='tos'} class=" nav-itemactive"{/if}>
											<a href="index.php?action=tos">{lng p="tos"}</a>
										</li>

									{foreach from=$pluginUserPages item=item}{if $item.top&&(!$item.after||$item.after=='tos')}
										<li{if $item.active} class="nav-item active"{/if}>
											<a href="{$item.link}">{$item.text}</a>
										</li>

									{/if}{/foreach}
										<li{if $smarty.request.action=='imprint'} class="nav-item active"{/if}>
											<a href="index.php?action=imprint">{lng p="contact"}</a>
										</li>

									<li class="nav-item button">
										<img src="{$tpldir}pages_assets/images/button-effect-img.svg" class="img-fluid" alt="">
										<div class="button-wrap">
											<a class="nav-link text-white" href="index.php?action=login">Login</a>
											<a class="nav-link text-green" href="index.php?action=signup">Signup</a>
										</div>
									</li>

									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">{foreach from=$languageList key=langKey item=langInfo}{if $langInfo.active}{$langInfo.title}{/if}{/foreach} <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											{foreach from=$languageList key=langKey item=langInfo}
											<li{if $langInfo.active} class="active"{/if}><a href="index.php?action=switchLanguage&amp;lang={$langKey}{if $smarty.get.action}&amp;target={text value=$smarty.get.action}{/if}">{$langInfo.title}</a></li>
											{/foreach}
										</ul>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<!--End-->

		<div class="modal fade" id="lostPW" tabindex="-1" role="dialog" aria-labelledby="lostPWLabel" aria-hidden="true">
			<div class="modal-dialog">
				<form action="index.php?action=lostPassword" method="post">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{lng p="cancel"}</span></button>
						<h4 class="modal-title" id="lostPWLabel">{lng p="lostpw"}</h4>
					</div>
					<div class="modal-body">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						{if $domain_combobox}
							<label class="sr-only" for="email_local_lpw">{lng p="email"}</label>
							<input type="text" name="email_local" id="email_local_lpw" class="form-control" placeholder="{lng p="email"}" required="true" />
							<div class="input-group-btn">
								<input type="hidden" name="email_domain" data-bind="email-domain" value="{domain value=$domainList[0]}" />
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span data-bind="label">@{domain value=$domainList[0]}</span> <span class="caret"></span></button>
								<ul class="dropdown-menu dropdown-menu-right domainMenu" role="menu">
									{foreach from=$domainList item=domain key=_key}<li{if $_key==0} class="active"{/if}><a href="#">@{domain value=$domain}</a></li>{/foreach}
								</ul>
							</div>
						{else}
							<label class="sr-only" for="email_full_p">{lng p="email"}</label>
							<input type="email" name="email_full" id="email_full_lpw" class="form-control" placeholder="{lng p="email"}" required="true" />
						{/if}
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">{lng p="cancel"}</button>
						<button type="submit" class="btn btn-success">{lng p="requestpw"}</button>
					</div>
				</div>
				</form>
			</div>
		</div>

		{include file="$page"}

		{* {if $page!='nli/login.tpl'}
			<div class="container">
				<hr />
				<footer class="row">
					<div class="col-xs-4">
						&copy; {$year} Email Immunity
					</div>
					<div class="col-xs-4" style="text-align:center;">
						<a href="{$mobileURL}">{lng p="mobilepda"}</a>
						{foreach from=$pluginUserPages item=item}{if !$item.top}
						|	<a href="{$item.link}">{$item.text}</a>
						{/if}{/foreach}
					</div>
					<div class="col-xs-4" style="text-align:right;">
						{literal}&nbsp;{/literal}
					</div>
				</footer>

				<br />
			</div>
		{/if} *}

		<!--footer-->
		<footer class="footer">
			<div class="container">
				<div class="top-border"></div>
				<div class="row">
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="footer-col-1">
							<div class="footer-logo">
								<a href="index.html"><img src="{$tpldir}pages_assets/images/footer-logo.svg" class="img-fluid" alt="Logo"></a>
							</div>
							<p>Nedabitur faucibus convlis blanllam nism eros, blandit sit amet tincidunt at, tempus eu odioe vamus in libero nec purxio.</p>
							<ul class="list-unstyled">
								<li class="icon-circle"><a href="#">
									<img src="{$tpldir}pages_assets/images/fb-icon.svg" class="img-fluid" alt="Facebook"></a>
								</li>
								<li class="icon-circle"><a href="#">
									<img src="{$tpldir}pages_assets/images/twitter-icon.svg" class="img-fluid" alt="Twitter"></a>
								</li>
								<li class="icon-circle"><a href="#">
									<img src="{$tpldir}pages_assets/images/linkedin-icon.svg" class="img-fluid" alt="Linkedin"></a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12">
						<div class="footer-col-2">
							<h1>legal</h1>
							<ul class="list-unstyled">
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Terms Of Services</a></li>
								<li><a href="#">Support</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-2 col-md-6 col-sm-12 col-xs-12">
						<div class="footer-col-3">
							<h1>Why aikQ</h1>
							<ul class="list-unstyled">
								<li><a href="#">Solutions</a></li>
								<li><a href="#">Features</a></li>
								<li><a href="#">Pricing</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="footer-col-4">
							<h1>contact</h1>
							<ul class="list-unstyled">
								<li>
									<a href="tel:+00 - 12345 678 90">+00 - 12345 678 90</a>
								</li>
								<li>
									<a href="mailto:support@aikq.de">support@aikq.de</a>
								</li>
								<li>
									<p>Neque porro quisquam esest qudkt dolorem ipsum quia dolor sit</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<p>
						Copyright &copy; {$year} Email Immunity. All Rights Reserved
						<a href="{$mobileURL}">{lng p="mobilepda"}</a>
						{foreach from=$pluginUserPages item=item}{if !$item.top}
						|	<a href="{$item.link}">{$item.text}</a>
						{/if}{/foreach}
					</p>
				</div>
			</div>
			<div class="right-img">
				<img src="{$tpldir}pages_assets/images/ar-logo.svg" class="img-fluid" alt="">
			</div>
		</footer>

		<!--End footer-->
	</div>
	{hook id="nli:index.tpl:afterContent"}
</body>
</html>