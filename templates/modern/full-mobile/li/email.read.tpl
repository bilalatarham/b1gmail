<div id="contentHeader">
	<div class="left">
		{if $attachments}
			<button class="fmBigButton" type="button" onclick="readMailShowBottomLayer('attachments')">
				<i class="fa fa-paperclip fa-lg fa-flip-vertical"></i>
				{lng p="attachments"} <strong>({$attachments|@count})</strong>
			</button>
		{/if}
		{lng p="mail_read"}: {text value=$subject cut=45}
	</div>
    
</div>

<div class="scrollContainer withBottomBar" id="mailReadScrollContainer">
	{hook id="email.read.tpl:head"}

	<div class="previewMailHeader" id="mailHeader">
		<table class="lightTable" width="100%">
			<tr>
				<th width="120">{lng p="from"}:</th>
				<td>{addressList list=$fromAddresses}</td>
			</tr>
			<tr>
				<th>{lng p="subject"}:</th>
				<td><strong>{text value=$subject}</strong></td>
			</tr>
			<tr>
				<th>{lng p="date"}:</th>
				<td>{date timestamp=$date elapsed=true}</td>
			</tr>
			<tr>
				<th>{lng p="to"}:</th>
				<td>{addressList list=$toAddresses}</td>
			</tr>
			{if $ccAddresses}<tr>
				<th>{lng p="cc"}:</th>
				<td>{addressList list=$ccAddresses}</td>
			</tr>{/if}
			{if $replyToAddresses}<tr>
				<th>{lng p="replyto"}:</th>
				<td>{addressList list=$replyToAddresses}</td>
			</tr>{/if}
			{if $priority!=0}<tr>
				<th>{lng p="priority"}:</th>
				<td>
					<img src="{$tpldir}images/li/mailico_{if $priority==-1}low{elseif $priority==1}high{/if}.gif" border="0" alt="" align="absmiddle" />
					{lng p="prio_$priority"}
				</td>
			</tr>{/if}
			
			{if $smimeStatus!=0&&!($smimeStatus&1)}
			<tr>
				<th>{lng p="security"}:</th>
				<td>
					{if $smimeStatus&2}
					<font color="#FF0000">
						<img src="{$tpldir}images/li/mailico_signed_bad.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						{lng p="badsigned"}
					</font>
					&nbsp;&nbsp;
					{/if}
					{if $smimeStatus&4}
					<img src="{$tpldir}images/li/mailico_signed_ok.png" width="16" height="16" border="0" alt="" align="absmiddle" />
					<a href="javascript:void(0);" onclick="showCertificate('{$smimeCertificateHash}');">{lng p="signed"}</a>
					&nbsp;&nbsp;
					{/if}
					{if $smimeStatus&8}
					<img src="{$tpldir}images/li/mailico_signed_noverify.png" width="16" height="16" border="0" alt="" align="absmiddle" />
					<a href="javascript:void(0);" onclick="showCertificate('{$smimeCertificateHash}');" style="color:#FF8C00;">{lng p="noverifysigned"}</a>
					&nbsp;&nbsp;
					{/if}
					{if $smimeStatus&64}
					<img src="{$tpldir}images/li/mailico_encrypted_error.png" width="16" height="16" border="0" alt="" align="absmiddle" />
							
					<font color="#FF0000">
						{lng p="decryptionfailed"}
					</font>
					&nbsp;&nbsp;
					{/if}
					{if $smimeStatus&128}
					<img src="{$tpldir}images/li/mailico_encrypted.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="encrypted"}
					&nbsp;&nbsp;
					{/if}
				</td>
			</tr>
			{/if}
			{hook id="email.read.tpl:metaTable"}
		</table>
	</div>
	{if !IsMobileUserAgent()}
	<div id="bigFormToolbar">
		
		{if $prevID}<button type="button" onclick="document.location.href='email.read.php?id={$prevID}&sid={$sid}';">
			<img src="res/dummy.gif" width="1" height="16" border="0" alt="" align="absmiddle" />
			&laquo;
		</button>{/if}
		
		<button type="button" onclick="mailReply({$mailID},false);">
		<img src="{$tpldir}images/li/mail_reply.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="reply"}
		</button>
		
		<button type="button" onclick="mailReply({$mailID},true);">
			<img src="{$tpldir}images/li/mail_replyall.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="replyall"}
		</button>
		
		<button type="button" onclick="document.location.href='email.compose.php?sid={$sid}&forward={$mailID}';">
			<img src="{$tpldir}images/li/mail_forward.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="forward"}
		</button>
	
		<button type="button" onclick="document.location.href='email.compose.php?sid={$sid}&redirect={$mailID}';">
			<img src="{$tpldir}images/li/mail_redirect.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="redirect"}
		</button>
		
		<button type="button" onclick="document.location.href='email.read.php?sid={$sid}&action=download&id={$mailID}';">
			<img src="{$tpldir}images/li/ico_download.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="download"}
		</button>
		<button type="button" onclick="printMail({$mailID},'{$sid}');">
			<img src="{$tpldir}images/li/mail_print.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="print"}
		</button>
        
		<button type="button" onclick="{if $folderID==-5}if(confirm('{lng p="realdel"}')) {/if} document.location.href='email.php?sid={$sid}&do=deleteMail&id={$mailID}&folder={$folderID}';">
			<img src="{$tpldir}images/li/mail_delete.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="delete"}
		</button>
        	
		{if $nextID}<button type="button" onclick="document.location.href='email.read.php?id={$nextID}&sid={$sid}';">
			
			&raquo; <img src="res/dummy.gif" width="1" height="16" border="0" alt="" align="absmiddle" />
		</button>{/if}
		
	</div>
	{/if}

<div class="pad">

{hook id="email.read.tpl:beforeText"}
<div>
		{if $flags&128}
		<div class="mailWarning">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/infected.png" width="16" height="16" />
			{lng p="infectedtext"}: {$infection}
		</div>
		{/if}
		{if $flags&256}
		<div class="mailNote" id="spamQuestionDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/spam.png" width="16" height="16" />
			{lng p="spamtext"}
			{if !$trained}<a href="javascript:setMailSpamStatus({$mailID}, false)">{lng p="isnotspam"}</a>{/if}
		</div>
		{elseif !$trained}
		<div class="mailNote" id="spamQuestionDiv" style="display:none;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/spam_question.png" width="16" height="16" />
			{lng p="spamquestion"}
			&nbsp;&nbsp;
			<a href="javascript:setMailSpamStatus({$mailID}, true)">
				<img src="{$tpldir}images/li/yes.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="yes"}
			</a>
			&nbsp;&nbsp;
			<a href="javascript:setMailSpamStatus({$mailID}, false)">
				<img src="{$tpldir}images/li/no.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="no"}
			</a>
		</div>
		{/if}
		{if $flags&512}
		<div class="mailNote">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/msg_small.png" width="16" height="16" />
			{lng p="certmailinfo"}
		</div>
		{/if}
		{if $htmlAvailable}
		<div class="mailNote">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/msg_small.png" width="16" height="16" />
			{lng p="htmlavailable"}
			<a href="email.read.php?sid={$sid}&id={$mailID}&htmlView=true">{lng p="view"} &raquo;</a>
		</div>
		{/if}
		{if $noExternal}
		<div class="mailNote" id="noExternalDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/msg_small.png" width="16" height="16" />
			{lng p="noexternal"}
			<a href="email.read.php?action=inlineHTML&mode={$textMode}&id={$mailID}&sid={$sid}&enableExternal=true" target="mailFrame" onclick="document.getElementById('noExternalDiv').style.display='none';">{lng p="showexternal"} &raquo;</a>
		</div>
		{/if}
		{if $confirmationTo}
		<div class="mailNote" id="confirmationDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/mail_confirm.png" width="16" height="16" />
			{lng p="senderconfirmto"}
			<b>{text value=$confirmationTo}</b>.
			<a href="javascript:sendMailConfirmation({$mailID});">{lng p="sendconfirmation"} &raquo;</a>
		</div>
		{elseif $flags&16384}
		<div class="mailNote preview" id="confirmationDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="{$tpldir}images/li/mail_confirm.png" width="16" height="16" />
			{lng p="confirmationsent"}
		</div>
		{/if}
		{hook id="email.read.tpl:mailNotes"}
	</div>
	
	<iframe name="mailFrame" width="100%" style="height:200px;" id="textArea" src="about:blank" class="mailHTMLText" frameborder="no"></iframe>
	<textarea id="textArea_raw" style="display:none;">{text allowEmpty=true value=$text allowDoubleEnc=true}</textarea>
	
	<script language="javascript">
	<!--
		initEMailTextArea(EBID('textArea_raw').value);
	//-->
	</script>
</p>
{hook id="email.read.tpl:afterText"}

<div id="afterText">
{if $vcards}
<p>
{foreach from=$vcards item=card key=key}
	<div class="mailBox"><table width="100%">
			<tr>
				<td rowspan="5" width="66" align="center"><img src="{$tpldir}images/li/vcf.png" width="48" height="48" border="0" alt="" /></td>
				<td width="100"><b>{lng p="firstname"}:</td>
				<td>{text value=$card.vorname}</td>
			</tr>
			<tr>
				<td><b>{lng p="surname"}:</td>
				<td>{text value=$card.nachname}</td>
			</tr>
			<tr>
				<td><b>{lng p="company"}:</td>
				<td>{text value=$card.firma}</td>
			</tr>
			<tr>
				<td><b>{lng p="email"}:</td>
				<td>{text value=$card.email}</td>
			</tr>
			<tr>
				<td><a href="email.read.php?id={$mailID}&action=importVCF&attachment={$key}&sid={$sid}"><img src="{$tpldir}images/li/import_vcf.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="importvcf"}</a></td>
				<td><a href="email.read.php?id={$mailID}&action=downloadAttachment&attachment={$key}&sid={$sid}"><img src="{$tpldir}images/li/ico_download.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="download"}</a></td>
			</tr>
		</table></div>
{/foreach}
</p>
{/if}

{hook id="email.read.tpl:foot"}

<form id="quoteForm" action="email.compose.php?sid={$sid}&reply={$mailID}" method="post">
	<input type="hidden" name="text" id="quoteText" value="" />
</form>

</div>

</div></div>

<div class="contentBottomLayer" id="bottomLayer_attachments" style="display:{if $smarty.get.openConversationView||$notes||!$attachments||IsMobileUserAgent()}none{/if};">
	<div class="contentHeader" style="border: none">
		<div class="left">
			<img src="{$tpldir}images/li/mailico_attachment.gif" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="attachments"} ({$attachments|@count})
		</div>
		<div class="right">
			<button onclick="readMailHideBottomLayers()" style="border: none">
				<i class="fa fa-times fa-3x" style="color: #aa1b1b"></i>
			</button>
		</div>
	</div>
	
	<form name="attachmentsForm" method="get" action="email.read.php">
	<input type="hidden" name="id" value="{$mailID}" />
	<input type="hidden" name="sid" value="{$sid}" />
		
	<div class="scrollContainer withBottomBar" style="padding-top: 10px">
		<table class="listTable">
			{foreach from=$attachments item=attachment key=attID}
			{cycle values="listTableTD,listTableTD2" assign="class"}
			<tr>
				<td class="{$class}" style="padding-left: 5px">{text value=$attachment.filename cut=45}</td>
				<td class="{$class}" width="130" style="text-align:right;">{lng p="approx"} {size bytes=$attachment.size}&nbsp;&nbsp;
											<a href="email.read.php?id={$mailID}&action=downloadAttachment&attachment={$attID}&sid={$sid}"><i class="fa fa-download fa-2x"></i></a>
											&nbsp;</td>
			</tr>
			{/foreach}
		</table>
	</div>
	
	</form>
</div>

<div class="contentBottomLayer" id="bottomLayer_props" style="display:{if !$notes||$smarty.get.openConversationView}none{/if};">
	<div class="contentHeader">
		<div class="left">
			<img src="{$tpldir}images/li/mailico_flagged.gif" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="props"}
		</div>
		<div class="right">
			<button onclick="readMailHideBottomLayers()">
				<img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			</button>
		</div>
	</div>
	
	<form method="post" action="email.read.php?id={$mailID}&sid={$sid}">
	<input type="hidden" name="do" value="saveMeta" />
	
	<div class="bigForm">
		<table class="listTable" style="border-bottom:none;">
			<tr>
				<th class="listTableHead">{lng p="color"}</th>
				<th class="listTableHead">{lng p="flags"}</th>
				<th class="listTableHead">{lng p="notes"}</th>
			</tr>
			<tr>
				<td width="160" style="padding:5px;">
					<table>
						<tr>
							<td><input type="radio"{if $color==1} checked="checked"{/if} name="color" value="1" /></td>
							<td><div class="calendarDate_0" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						
							<td><input type="radio"{if $color==2} checked="checked"{/if} name="color" value="2" /></td>
							<td><div class="calendarDate_1" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						
							<td><input type="radio"{if $color==3} checked="checked"{/if} name="color" value="3" /></td>
							<td><div class="calendarDate_2" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						</tr>
						<tr>
							<td><input type="radio"{if $color==4} checked="checked"{/if} name="color" value="4" /></td>
							<td><div class="calendarDate_3" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						
							<td><input type="radio"{if $color==5} checked="checked"{/if} name="color" value="5" /></td>
							<td><div class="calendarDate_4" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						
							<td><input type="radio"{if $color==6} checked="checked"{/if} name="color" value="6" /></td>
							<td><div class="calendarDate_5" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						</tr>
						<tr>
							<td><input type="radio"{if $color==0} checked="checked"{/if} name="color" value="0" /></td>
							<td colspan="5">&nbsp;{lng p="none"}</td>
						</tr>
					</table>
				</td>
				<td width="150" class="listTableTDActive" style="padding:5px;padding-left:10px;">
					<img src="{$tpldir}images/li/mail_markunread.png" border="0" alt="" align="absmiddle" />
					<input type="checkbox" name="flags[1]" id="flags1"{if $smarty.post.do=='saveMeta'&&($flags&1)} checked="checked"{/if} />
					<label for="flags1">{lng p="unread"}</label><br />
					
					<img src="{$tpldir}images/li/mailico_flagged.gif" border="0" alt="" align="absmiddle" />
					<input type="checkbox" name="flags[16]" id="flags16"{if $flags&16} checked="checked"{/if} />
					<label for="flags16">{lng p="marked"}</label><br />
					
					<img src="{$tpldir}images/li/mailico_done.gif" border="0" alt="" align="absmiddle" />
					<input type="checkbox" name="flags[4096]" id="flags4096"{if $flags&4096} checked="checked"{/if} />
					<label for="flags4096">{lng p="done"}</label><br />
				</td>
				<td style="padding:5px;">
					<textarea style="width:100%;height:65px;" name="notes">{text value=$notes allowEmpty=true}</textarea>
				</td>
			</tr>
		</table>
	</div>
	
	<div class="contentFooter">
	 	<div class="right">
			<button type="submit">
				<img src="{$tpldir}images/li/yes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				{lng p="save"}
			</button>
		</div>
	</div>
	
	</form>
</div>


{include file="li/email.addressmenu.tpl"}
