{if !$smarty.get.tableOnly}<form name="f1" action="email.php?do=action&{$folderString}&sid={$sid}" onsubmit="transferSelectedMailIDs()" method="post">
<input type="hidden" name="selectedMailIDs" id="selectedMailIDs" value="" />
	
<div id="contentHeader">
	<div class="left">
		<div id="folderTitle">
			<img src="{$tpldir}images/li/menu_ico_{$folderInfo.type}.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {$folderInfo.title}
		</div>
		<div id="massActionHolder" style="display: none;">
			<select class="smallInput" name="massAction" id="massAction">
				<option value="-">------ {lng p="selaction"} ------</option>
				
				<optgroup label="{lng p="actions"}">
					<option value="delete">{lng p="delete"}</option>
					<option value="forward">{lng p="forward"}</option>
					<option value="download">{lng p="download"}</option>
					{hook id="email.folder.tpl:mailSelect.actions"}
				</optgroup>
				
				<optgroup label="{lng p="flags"}">
					<option value="markread">{lng p="markread"}</option>
					<option value="markunread">{lng p="markunread"}</option>
					<option value="mark">{lng p="mark"}</option>
					<option value="unmark">{lng p="unmark"}</option>
					<option value="done">{lng p="markdone"}</option>
					<option value="undone">{lng p="unmarkdone"}</option>
					<option value="markspam">{lng p="markspam"}</option>
					<option value="marknonspam">{lng p="marknonspam"}</option>
					{hook id="email.folder.tpl:mailSelect.flags"}
				</optgroup>
				
				<optgroup label="{lng p="setmailcolor"}">
					<option value="color_0" class="mailColor_0">{lng p="color_0"}</option>
					<option value="color_1" class="mailColor_1">{lng p="color_1"}</option>
					<option value="color_2" class="mailColor_2">{lng p="color_2"}</option>
					<option value="color_3" class="mailColor_3">{lng p="color_3"}</option>
					<option value="color_4" class="mailColor_4">{lng p="color_4"}</option>
					<option value="color_5" class="mailColor_5">{lng p="color_5"}</option>
					<option value="color_6" class="mailColor_6">{lng p="color_6"}</option>
				</optgroup>
				
				<optgroup label="{lng p="move"} {lng p="moveto"}">
				{foreach from=$dropdownFolderList key=dFolderID item=dFolderTitle}
				<option value="moveto_{$dFolderID}" style="font-family:courier;">{$dFolderTitle}</option>
				{/foreach}
				</optgroup>
				
				{hook id="email.folder.tpl:mailSelect"}
			</select>
			<input class="smallInput" type="submit" value="{lng p="ok"}" />		
		</div>
	</div>
</div>

<div class="scrollContainer withBottomBar">
{/if}

<table class="bigTable" id="mailTable">
	<colgroup>
		<col style="width:40px;" />
		<col style="width:24px;" />
		<col />
	</colgroup>
	
	{if $mailList}
	{assign var=first value=true}
	{foreach from=$mailList key=mailID item=mail}
	{assign var=mailGroupID value=$mail.groupID}
	{cycle values="listTableTR,listTableTR2" assign="class"}
	
	{if $mailID<0}
	{cycle values="listTableTR,listTableTR2" assign="class"}
	{if !$first}
	</tbody>
	{/if}

	<tbody id="group_{$mailID}">
	{assign var=first value=false}
	{else}
	<tr id="mail_{$mailID}_ntr" class="{$class}">
		<td class="narrowRow" style="text-align:center;width:24px;">
			<input type="checkbox" id="selecTable_{$mailID}" />
		</td>
		<td id="mail_{$mailID}_ncol1" style="width:24px;" align="center" class="narrowRow{if $mail.color>0} mailColor_{$mail.color}{/if}">
            {if $mail.flags&1}<i class="fa fa-envelope fa-lg"></i>{/if}
		</td>
		<td draggable="false" id="mail_{$mailID}_ncol2" class="narrowRow" nowrap="nowrap">
			<a draggable="false" href="{if $folderID == -3}email.compose.php?redirect={else}email.read.php?id={/if}{$mailID}&sid={$sid}" onclick="return(true)"{if $mail.flags&8} style="text-decoration:line-through;"{/if}>
				<div id="mail_{$mailID}_nspan1" class="date{if $mail.flags&1} unread{/if}"{if $mail.flags&8} style="text-decoration:line-through;"{/if}>{date timestamp=$mail.timestamp nice=true}</div>
				<div id="mail_{$mailID}_nspan2" class="sender{if $mail.flags&1} unread{/if}">{if $folderID!=-2}{if $mail.from_name}{text value=$mail.from_name}{else}{text value=$mail.from_mail}{/if}{else}{if $mail.to_name}{text value=$mail.to_name}{else}{text value=$mail.to_mail}{/if}{/if}</div>
				<div class="subject">
					<img src="{$tpldir}images/li/mailico_done.gif" width="16" height="16" border="0" alt="" align="absmiddle" style="display:{if $mail.flags&4096}{else}none{/if};" id="maildone_{$mailID}" />
					<img id="mail_{$mailID}_flagimg" src="{$tpldir}images/li/mailico_{if $mail.flags&16}flagged{elseif $mail.priority==1}high{elseif $mail.priority==-1}low{else}empty{/if}.gif" border="0" alt="" align="absmiddle" style="display:{if $mail.flags&16||$mail.priority==1||$mail.priority==-1}{else}none{/if};" />
					{if $mail.flags&4||$mail.flags&2}<img src="{$tpldir}images/li/mailico_{if $mail.flags&4}forwarded{elseif $mail.flags&2}answered{/if}.gif" border="0" alt="" align="absmiddle" />{/if}
					{if $mail.flags&64}<img src="{$tpldir}images/li/mailico_attachment.gif" border="0" alt="" align="absmiddle" />{/if}
					{if $mail.flags&128}<img src="{$tpldir}images/li/infected.png" width="16" height="16" border="0" alt="" align="absmiddle" />{/if}
					{if $mail.flags&256}<img src="{$tpldir}images/li/spam.png" width="16" height="16" border="0" alt="" align="absmiddle" />{/if}
					
					{text value=$mail.subject}
				</div>
			</a>
		</td>
	</tr>
	{/if}
	{/foreach}
	{if !$first}
	</tbody>
	{/if}
	{/if}
    
</table>

    <div id="mobile-folderswitch">
        {if $pageNo > 1}
       		<a href="email.php?folder={$folderID}&page={$pageNo-1}&sid={$sid}"><i class="fa fa-angle-double-left"></i></a>
        {/if}
        {if $pageNo < $pageCount}
       		<a href="email.php?folder={$folderID}&page={$pageNo+1}&sid={$sid}"><i class="fa fa-angle-double-right"></i></a>
        {/if}
    </div>

{if !$smarty.get.tableOnly}

</div>

</div>

</form> 

<script language="javascript">
<!--
	currentSortColumn = '{$sortColumn}';
	currentSortOrder = '{$sortOrder}';
	currentPageNo = {$pageNo};
	currentPageCount = {$pageCount};
	narrowMode = true;
	initMailSel();
//-->
</script>
{/if}
