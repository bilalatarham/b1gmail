<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="addressbook"}
	</div>	
</div>

<form name="f1" method="post" action="organizer.addressbook.php?action=action&sid={$sid}" onsubmit="transferSelectedAddresses();">
<input name="addrIDs" id="addrIDs" value="" />

<div class="scrollContainer" style="overflow:hidden;">

		<div class="addressContents" id="hSep1">
			<div class="addressContainer withBottomBar">
				<table class="bigTable" id="addressTable">
					<tr style="height:auto;">						
						<th>{lng p="name"}</th>
					</tr>

					{if $addressList}
					{foreach from=$addressList key=letter item=addresses}
					{assign var=groupID value="addr$letter"}
			
					<tr style="height:auto;">
						<td colspan="1" class="folderGroup">							
							{$letter}
						</td>
					</tr>

					<tbody id="group_{$letter}" style="display:{if $smarty.cookies.toggleGroup.$groupID=='closed'}none{/if};">
			
					{foreach from=$addresses key=addressID item=address}
					{cycle values="listTableTD,listTableTD2" assign="class"}
					<tr id="addr_{$addressID}" onclick="addressOverview(1, '{$tpldir}', '{$addressID}')">
						

						
						<td class="{$class}">
							{if !$address.vorname&&!$address.nachname&&$address.firma}
							<strong>{text value=$address.firma}
							{else}
							{text value=$address.vorname}
							<strong>{text value=$address.nachname}</strong>
							{/if}
						</td>
					</tr>
					{/foreach}
			
					</tbody>
			
					{/foreach}
					{/if}
				</table>
			</div>
			
			<div class="contentFooter">
				<div class="left">
					
				</div>

				<div class="right">
					
				</div>
			</div>
		</div>
		
		<div id="hSepSep" style="display: none;"></div>
		
		<div class="addressPreview" id="hSep2" style="width: 0;">
			<div id="previewArea" style="display:none;"></div>
			<div id="multiSelPreview">
				<div id="multiSelPreview_vCenter">
					<div id="multiSelPreview_inner">
						<div id="multiSelPreview_count"><i class="fa fa-refresh fa-spin fa-2x"></i></div>
					</div>
				</div>
			</div>
		</div>
	
</div>
<input type="hidden" id="currentSelected" name="currentSelected" value="1" />
<script language="javascript">
<!--
		initAddrSel();
//-->
</script>

</form>