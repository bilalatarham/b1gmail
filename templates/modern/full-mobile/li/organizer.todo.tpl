<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_todo.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="todolist"}
	</div>
</div>
	
<div class="scrollContainer" style="overflow:hidden;">
	<div class="taskLists" id="taskList">
		<div class="taskContainer withBottomBar" style="overflow:auto;" id="taskListsScrollContainer">
			<table class="bigTable">
				<tr>
					<th>{lng p="tasklists"}</th>
				</tr>
			</table>
		
			<div id="taskListsContainer">
				{foreach from=$taskLists item=taskList}
				<a href="#" class="taskList" onclick="taskOverview(1,'{$tpldir}',{$taskList.tasklistid});selectTaskList({$taskList.tasklistid})" id="taskList_{$taskList.tasklistid}">
					{text value=$taskList.title}
					{if $taskList.tasklistid!=0}<img src="{$tpldir}images/li/delcross.png" onclick="deleteTaskList({$taskList.tasklistid})" />{/if}
				</a>
				{/foreach}
			</div>
		</div>
		
		<div class="contentFooter">
			<div class="left">
				<i class="fa fa-plus-circle fa-lg" style="color: #3DA83D"></i>
				<input type="text" id="addListTitle" name="addListTitle" class="smallInput" style="width:150px;" />
                <input type="button" value=" {lng p="ok"} " onclick="fmAddTodoList('{$sid}');" />
			</div>
			<div class="right">
				
			</div>
		</div>
	</div>
	<div id="multiSelPreview_vCenter" style="display: none;"><i class="fa fa-refresh fa-spin fa-2x"></i></div>
	<div class="taskContents" id="taskListContainer">
		{include file="full-mobile/li/organizer.todo.list.tpl"}
	</div>
</div>

<img src="{$tpldir}images/li/drag_task.png" style="display:none;" /><img src="{$tpldir}images/li/drag_tasks.png" style="display:none;" />
<input type="hidden" id="currentSelected" name="currentSelected" value="0" />
