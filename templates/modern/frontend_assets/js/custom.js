$( document ).ready(function() {
  
    $(window).scroll(function() {
        var nav = $('.site-nav');
        var top = 95;
        if ($(window).scrollTop() >= top) {
    
            nav.addClass('fixed-nav');
    
        } else {
            nav.removeClass('fixed-nav');
        }
    });

    /*Mobile Left Menu Toggle*/
    $(".navbar-toggler").click(function(){
        $(".left-menu").toggleClass("open");
        $(".right-canvas").toggleClass("expanded");
    }); 
   
});

