<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_faq.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="faq"}</span>
	</div>
</div>

<div class="scrollContainer">
	{* <table class="bigTable">
		<tr>
			<th>
				{lng p="question"}
			</th>
		</tr>
		
		{foreach from=$faq item=item}
		{cycle values="listTableTD,listTableTD2" assign="class"}
		<tr>
			<td class="{$class}">&nbsp;<a href="javascript:toggleGroup({$item.id});"><img id="groupImage_{$item.id}" src="{$tpldir}images/expand.png" width="11" height="11" border="0" alt="" align="absmiddle" /> <img src="{$tpldir}images/li/ico_faq.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {$item.frage}</a></td>
		</tr>
		<tbody id="group_{$item.id}" style="display:none;">
			<tr>
				<td class="listTableTDText">{$item.antwort}</td>
			</tr>
		</tbody>
		{/foreach}
	</table> *}

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="faqs-wrap">
					<p class="panel-t">{lng p="question"}</p>
					<div id="accordion">
						{foreach from=$faq item=item}
							<div class="card">
								<div class="card-header" id="heading_{$item.id}">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_{$item.id}" aria-expanded="false" aria-controls="collapse_{$item.id}">
											{* <img src="{$tpldir}images/li/ico_faq.png" width="16" height="16" border="0" alt="" align="absmiddle" />  *}
											{$item.frage}
										</button>
									</h5>
								</div>

								<div id="collapse_{$item.id}" class="collapse" aria-labelledby="heading_{$item.id}" data-parent="#accordion">
									<div class="card-body">
										{$item.antwort}
									</div>
								</div>
							</div>
						{/foreach}
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
