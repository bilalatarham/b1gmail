<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{lng p="viewoptions"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />

	<!-- New Links -->
	<link rel="stylesheet" href="{$tpldir}frontend_assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="{$tpldir}frontend_assets/css/theme.css">

	<!-- client scripts -->
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js" type="text/javascript" language="javascript"></script>
</head>

<body>

	<section class="modal-form">
		<div class="col-md-12">
			<form action="email.php?folder={$folderID}&do=setViewOptions&overlay=true&sid={$sid}" method="post">
				<h4 class="form-title">{lng p="viewoptions"}</h4>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							<label for="group_mode" class="input-label">{lng p="group_mode"}</label>
							<select name="group_mode" id="group_mode" class="form-control w-100">
								<option value="-"{if $groupMode=='-'} selected="selected"{/if}>------------</option>
								
								<optgroup label="{lng p="props"}">
									<option value="fetched"{if $groupMode=='fetched'} selected="selected"{/if}>{lng p="date"}</option>
									<option value="von"{if $groupMode=='von'} selected="selected"{/if}>{lng p="from"}</option>
								</optgroup>
								
								<optgroup label="{lng p="flags"}">
									<option value="gelesen"{if $groupMode=='gelesen'} selected="selected"{/if}>{lng p="read"}</option>
									<option value="beantwortet"{if $groupMode=='beantwortet'} selected="selected"{/if}>{lng p="answered"}</option>
									<option value="weitergeleitet"{if $groupMode=='weitergeleitet'} selected="selected"{/if}>{lng p="forwarded"}</option>
									<option value="flagged"{if $groupMode=='flagged'} selected="selected"{/if}>{lng p="flagged"}</option>
									<option value="done"{if $groupMode=='done'} selected="selected"{/if}>{lng p="done"}</option>
									<option value="attach"{if $groupMode=='attach'} selected="selected"{/if}>{lng p="attachment"}</option>
									<option value="color"{if $groupMode=='color'} selected="selected"{/if}>{lng p="color"}</option>
								</optgroup>
							</select>
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							<label for="perpage" class="input-label">{lng p="mails_per_page"}</label>
							<select name="perpage" id="perpage" class="form-control w-100">
								{section start=5 step=5 loop=55 name=num}
								<option value="{$smarty.section.num.index}"{if $perPage==$smarty.section.num.index} selected="selected"{/if}>{$smarty.section.num.index}</option>
								{/section}
								{section start=75 step=25 loop=175 name=num}
								<option value="{$smarty.section.num.index}"{if $perPage==$smarty.section.num.index} selected="selected"{/if}>{$smarty.section.num.index}</option>
								{/section}
							</select>
						</div>
					</div>

					<div class="col-md-12">
						<div class="act-btn text-right mt-2">
							<button type="button" class="btn btn-default" onclick="parent.hideOverlay()" >{lng p="cancel"}</button>
							<button type="submit" class="btn btn-success ml-2">{lng p="ok"}</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</body>

</html>
