<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/mail_reply.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>	
		<span class="page_title">{lng p="autoresponder"}</span>
	</div>
</div>

<div class="scrollContainer">
	<div class="container-fluid">
		<div class="pad">
		<form name="f1" method="post" action="prefs.php?action=autoresponder&do=save&sid={$sid}">
			<table class="listTable table custom_tbl_style table-striped">
				<tr>
					<th class="listTableHead" colspan="2"> {lng p="autoresponder"}</th>
				</tr>
				<tr>
					<td class="listTableLeft"><label for="active">{lng p="autoresponder"}:</label></td>
					<td class="listTableRight custom_check">
						<input type="checkbox" name="active" id="active"{if $active} checked="checked"{/if} />
						<label for="active"><b>{lng p="enable"}</b></label>
					</td>
				</tr>
				<tr>
					<td class="listTableLeft"><label for="betreff">{lng p="subject"}:</label></td>
					<td class="listTableRight">
						<input type="text" class="form-control round_field" name="betreff" id="betreff" value="{text allowEmpty=true value=$betreff}" style="width:400px;">
					</td>
				</tr>
				<tr>
					<td class="listTableLeft"><label for="mitteilung">{lng p="text"}:</label></td>
					<td class="listTableRight">
						<textarea name="mitteilung" class="form-control" id="mitteilung" style="width:400px;height:200px;">{text allowEmpty=true value=$mitteilung}</textarea>
					</td>
				</tr>
				<tr>
					<td class="listTableLeft">&nbsp;</td>
					<td class="listTableRight">
						<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
						<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
					</td>
				</tr>
			</table>
		</form>
		</div>	
	</div>	
</div>
