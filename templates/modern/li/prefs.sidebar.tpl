<div class="customize-widget-area sidebar-settings mt-0">
	<h4 class="menu-title fw-bold">{lng p="prefs"}</h4>

	<div class="contentMenuIcons">
		<div class="dtree">
			<div class="dTreeNode">
				<img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="prefs.php?sid={$sid}">{lng p="overview"}</a>
			</div>
		</div>

		<div class="dtree">
			{foreach from=$prefsItems item=null key=item}
			<div class="dTreeNode">
				<img src="{if $prefsIcons[$item]}{$prefsIcons[$item]}{else}{$tpldir}images/li/{if $item=='autoresponder'}mail_reply{elseif $item=='contact'}addr_common{else}ico_{$item}{/if}.png{/if}" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="prefs.php?action={$item}&sid={$sid}"> {lng p="$item"}</a>
			</div>
			{/foreach}
		</div>
	</div>
</div>

{* <div class="sidebarHeading">{lng p="prefs"}</div>
<div class="contentMenuIcons">
	<a href="prefs.php?sid={$sid}"><img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="overview"}</a><br />
	{foreach from=$prefsItems item=null key=item}
	<a href="prefs.php?action={$item}&sid={$sid}"><img src="{if $prefsIcons[$item]}{$prefsIcons[$item]}{else}{$tpldir}images/li/{if $item=='autoresponder'}mail_reply{elseif $item=='contact'}addr_common{else}ico_{$item}{/if}.png{/if}" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="$item"}</a><br />
	{/foreach}
</div> *}
