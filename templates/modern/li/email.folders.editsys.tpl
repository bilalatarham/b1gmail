<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div> 
		<span class="page_title">{lng p="editfolder"}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">
<form name="f1" method="post" action="email.folders.php?action=saveFolder&id={$folderID}&sid={$sid}">
	<table class="listTable table custom_tbl_style tbl_border_none">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="editfolder"}</th>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="titel">{lng p="title"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control" value="{text value=$folderTitle allowEmpty=true}" style="width:100%;" disabled="disabled" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="storetime">{lng p="storetime"}:</label></td>
			<td class="listTableRight">
				<select name="storetime" id="storetime" class="form-control field-min-w">
					<option value="-1">------------</option>
					<option value="86400"{if $storeTime==86400} selected="selected"{/if}>1 {lng p="days"}</option>
					<option value="172800"{if $storeTime==172800} selected="selected"{/if}>2 {lng p="days"}</option>
					<option value="432000"{if $storeTime==432000} selected="selected"{/if}>5 {lng p="days"}</option>
					<option value="604800"{if $storeTime==604800} selected="selected"{/if}>7 {lng p="days"}</option>
					<option value="1209600"{if $storeTime==1209600} selected="selected"{/if}>2 {lng p="weeks"}</option>
					<option value="2419200"{if $storeTime==2419200} selected="selected"{/if}>4 {lng p="weeks"}</option>
					<option value="4838400"{if $storeTime==4838400} selected="selected"{/if}>2 {lng p="months"}</option>
				</select>
			</td>
		</tr>
	
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>
</div></div>
</div>
