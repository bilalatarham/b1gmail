<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_coupons.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="coupons"}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<form name="f1" method="post" action="prefs.php?action=coupons&do=redeem&sid={$sid}">
	<table class="listTable table custom_tbl_style tbl_border_none">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="redeemcoupon"}</th>
		</tr>
		
		<tr>
			<td class="listTableRight" colspan="2">
				{lng p="prefs_d_coupons"}
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="code">{lng p="code"}:</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control field-min-w" name="code" id="code" value="" />
			</td>
		</tr>
		
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>

</div></div>
</div>
