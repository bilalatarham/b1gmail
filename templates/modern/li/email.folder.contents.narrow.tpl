{if !$smarty.get.tableOnly}<form name="f1" action="email.php?do=action&{$folderString}&sid={$sid}" onsubmit="transferSelectedMailIDs()" method="post">
<input type="hidden" name="selectedMailIDs" id="selectedMailIDs" value="" />

<div id="contentHeader">
	<div class="right float-right">
		
		<div class="btn-group action-btn" role="group">
			<button type="button" onclick="switchPage({$pageNo})" class="btn btn-secondary">
				<i class="fa fa-refresh fa-lg"></i>
			</button>
			<div class="btn-group" role="group">
			{if $folderInfo.type!='intellifolder'&&!$folderInfo.readonly}
				<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-gears fa-lg"></i>
				</button>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
				<a class="mailMenuItem dropdown-item" href="javascript:document.location.href='email.php?do=markAllAsRead&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/mail_markread.png" width="16" height="16" border="0" alt="" /> {lng p="markallasread"}</a>
				<a class="mailMenuItem dropdown-item" href="javascript:document.location.href='email.php?do=markAllAsRead&unread=true&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/mail_markunread.png" width="16" height="16" border="0" alt="" /> {lng p="markallasunread"}</a>
				<div class="mailMenuSep"></div>
				<a class="mailMenuItem dropdown-item" href="javascript:document.location.href='email.php?do=downloadAll&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/ico_download.png" width="16" height="16" border="0" alt="" /> {lng p="downloadall"}</a>
				<div class="mailMenuSep"></div>
				<a class="mailMenuItem dropdown-item" href="javascript:void(0);" onclick="if(confirm('{lng p="realempty"}')) document.location.href='email.php?do=emptyFolder&folder='+currentFolderID+'&sid={$sid}';"><img align="absmiddle" src="{$tpldir}images/li/folder_empty.png" width="16" height="16" border="0" alt="" /> {lng p="emptyfolder"}</a>
				</div>
			{/if}
				{if !$folderInfo.readonly}
					<button type="button" class="btn btn-secondary" onclick="folderViewOptions({$folderID});">
						<i class="fa fa-desktop fa-lg"></i>
					</button>
				{/if}
			</div>
		</div>
		<!--<button onclick="showFolderMenu(event);" type="button">
			<i class="fa fa-gears fa-lg"></i>
		</button>-->
<!--<button onclick="switchPage({$pageNo})" type="button">
			<i class="fa fa-refresh fa-lg"></i>
		</button>-->	
	</div>
	<div class="left"{if $templatePrefs.showCheckboxes} style="padding-left:2px;"{/if}>
		{if $templatePrefs.showCheckboxes}<input type="checkbox" style="vertical-align:middle;" id="checkAllMails" onclick="if(this.checked) _mailSel.selectAll(); else _mailSel.unselectAll()||showMultiSelPreview(0);" />{/if}
		<!--<img src="{$tpldir}images/li/menu_ico_{$folderInfo.type}.png" width="16" height="16" border="0" alt="" align="absmiddle" />--> 
		
		<div class="box-title">{$folderInfo.title}</div>
	</div>
</div>

<div class="scrollContainer withBottomBar">
{/if}

<table class="bigTable" id="mailTable">
	<colgroup>
		{if $templatePrefs.showCheckboxes}
		<col style="width:24px;" />
		{/if}
		<col style="width:24px;" />
		<col />
	</colgroup>

	{if $mailList}
	{assign var=first value=true}
	{foreach from=$mailList key=mailID item=mail}
	{assign var=mailGroupID value=$mail.groupID}
	{cycle values="listTableTR,listTableTR2" assign="class"}

	{if $mailID<0}
	{cycle values="listTableTR,listTableTR2" assign="class"}
	{if !$first}
	</tbody>
	{/if}
	<tr>
		<td colspan="{if $templatePrefs.showCheckboxes}3{else}2{/if}" class="folderGroup">
			<a style="display:block;cursor:pointer;" onclick="toggleGroup({$mailID},'{$mail.groupID}');">&nbsp;<img id="groupImage_{$mailID}" src="{$tpldir}images/{if $smarty.cookies.toggleGroup.$mailGroupID=='closed'}expand{else}contract{/if}.png" width="11" height="11" border="0" align="absmiddle" alt="" />
			&nbsp;{$mail.text} {if $mail.date && $mail.date!=-1}({date timestamp=$mail.date dayonly=true}){/if}</a>
		</td>
	</tr>
	<tbody id="group_{$mailID}" style="display:{if $smarty.cookies.toggleGroup.$mailGroupID=='closed'}none{/if};">
	{assign var=first value=false}
	{else}
	<tr id="mail_{$mailID}_ntr" class="{$class}">
		{if $templatePrefs.showCheckboxes}
		<td class="narrowRow" style="text-align:center;width:24px;">
			<input type="checkbox" id="selecTable_{$mailID}" />
		</td>
		{/if}
		<td id="mail_{$mailID}_ncol1" style="width:24px;" align="center" class="narrowRow{if $mail.color>0} mailColor_{$mail.color}{/if}">
			<i id="mail_{$mailID}_nicon" class="fa fa-envelope{if !($mail.flags&1)}-o{/if}"></i>
		</td>
		<td draggable="false" id="mail_{$mailID}_ncol2" class="narrowRow" nowrap="nowrap">
			<a draggable="false" href="email.read.php?id={$mailID}&sid={$sid}" onclick="return(false)"{if $mail.flags&8} style="text-decoration:line-through;"{/if}>
				<div id="mail_{$mailID}_nspan1" class="date{if $mail.flags&1} unread{/if}"{if $mail.flags&8} style="text-decoration:line-through;"{/if}>{date timestamp=$mail.timestamp nice=true}</div>
				<div id="mail_{$mailID}_nspan2" class="sender{if $mail.flags&1} unread{/if}">{if $folderID!=-2}{if $mail.from_name}{text value=$mail.from_name}{else}{if $mail.from_mail}{email value=$mail.from_mail}{else}-{/if}{/if}{else}{if $mail.to_name}{text value=$mail.to_name}{else}{if $mail.to_mail}{email value=$mail.to_mail}{else}-{/if}{/if}{/if}</div>
				<div class="subject">
					<i id="maildone_{$mailID}" class="{if $mail.flags&4096}fa fa-check{/if}"></i>
					<i id="mail_{$mailID}_flagimg" class="{if $mail.flags&16}fa fa-flag-o{elseif $mail.priority==1}fa fa-exclamation{elseif $mail.priority==-1}fa fa-long-arrow-down{else}{/if}"></i>
					{if $mail.flags&4||$mail.flags&2}<i class="fa {if $mail.flags&4}fa-mail-forward{elseif $mail.flags&2}fa-mail-reply{/if}"></i>{/if}
					{if $mail.flags&64}<i class="fa fa-paperclip"></i>{/if}
					{if $mail.flags&128}<img src="{$tpldir}images/li/infected.png" width="16" height="16" border="0" alt="" align="absmiddle" />{/if}
					{if $mail.flags&256}<img src="{$tpldir}images/li/spam.png" width="16" height="16" border="0" alt="" align="absmiddle" />{/if}

					{text value=$mail.subject}
				</div>
			</a>
		</td>
	</tr>
	{/if}
	{/foreach}
	{if !$first}
	</tbody>
	{/if}
	{/if}

</table>
{if !$smarty.get.tableOnly}

</div>

<div id="contentFooter">
	<div class="right float-right">
		{lng p="pages"}:
		&nbsp;
		<select class="smallInput form-control" onchange="switchPage(this.value)">
			{section name=page start=0 loop=$pageCount step=1}
				<option value="{$smarty.section.page.index+1}"{if $pageNo==$smarty.section.page.index+1} selected="selected"{/if}>{$smarty.section.page.index+1}</option>
			{/section}
		</select>
	</div>
	<div class="left action-drop">
		<select class="smallInput form-control" name="massAction" id="massAction">
			<option value="-">------ {lng p="selaction"} ------</option>

			<optgroup label="{lng p="actions"}">
			{if !$folderInfo.readonly}<option value="delete">{lng p="delete"}</option>{/if}
				<option value="forward">{lng p="forward"}</option>
				<option value="download">{lng p="download"}</option>
				{hook id="email.folder.tpl:mailSelect.actions"}
			</optgroup>

			{if !$folderInfo.readonly}<optgroup label="{lng p="flags"}">
				<option value="markread">{lng p="markread"}</option>
				<option value="markunread">{lng p="markunread"}</option>
				<option value="mark">{lng p="mark"}</option>
				<option value="unmark">{lng p="unmark"}</option>
				<option value="done">{lng p="markdone"}</option>
				<option value="undone">{lng p="unmarkdone"}</option>
				<option value="markspam">{lng p="markspam"}</option>
				<option value="marknonspam">{lng p="marknonspam"}</option>
				{hook id="email.folder.tpl:mailSelect.flags"}
			</optgroup>

			<optgroup label="{lng p="setmailcolor"}">
				<option value="color_0" class="mailColor_0">{lng p="color_0"}</option>
				<option value="color_1" class="mailColor_1">{lng p="color_1"}</option>
				<option value="color_2" class="mailColor_2">{lng p="color_2"}</option>
				<option value="color_3" class="mailColor_3">{lng p="color_3"}</option>
				<option value="color_4" class="mailColor_4">{lng p="color_4"}</option>
				<option value="color_5" class="mailColor_5">{lng p="color_5"}</option>
				<option value="color_6" class="mailColor_6">{lng p="color_6"}</option>
			</optgroup>

			<optgroup label="{lng p="move"} {lng p="moveto"}">
			{foreach from=$dropdownFolderList key=dFolderID item=dFolderTitle}
			<option value="moveto_{$dFolderID}" style="font-family:courier;">{$dFolderTitle}</option>
			{/foreach}
			</optgroup>{/if}

			{hook id="email.folder.tpl:mailSelect"}
		</select>
		<input class="smallInput btn btn-primary" type="submit" value="{lng p="ok"}" />
	</div>

	
</div>

</form>

<script language="javascript">
<!--
	currentSortColumn = '{$sortColumn}';
	currentSortOrder = '{$sortOrder}';
	currentPageNo = {$pageNo};
	currentPageCount = {$pageCount};
	narrowMode = true;
	initMailSel();
//-->
</script>
{/if}
