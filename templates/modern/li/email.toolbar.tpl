<div class="email-toolbar">
	{hook id="email.toolbar.tpl:firstColumn"}
	{comment text="space"}
	<div class="progress-area">
		<div class="item-label mr-1">
			<p>{lng p="space"}:</p>
		</div>
		<div class="progress-wrap">
			{progressBar value=$spaceUsed max=$spaceLimit width=100}
			<div class="used-space ml-1">
				<p>{size bytes=$spaceUsed} / {size bytes=$spaceLimit} {lng p="used"}</p>
			</div>
		</div>
	</div>
	{if $enablePreview}
		<div class="email-preview">
			<div class="item-label">
				<p>{lng p="preview"}:</p>
			</div>
			<div class="preview-select">
				<select class="smallInput form-control preview-option-sty" onchange="updatePreviewPosition(this)">
					<option value="bottom"{if !$narrow} selected="selected"{/if}>{lng p="bottom"}</option>
					<option value="right"{if $narrow} selected="selected"{/if}>{lng p="right"}</option>
				</select>
			</div>
		</div>
	{/if}
	{hook id="email.toolbar.tpl:lastColumn"}
</div>
