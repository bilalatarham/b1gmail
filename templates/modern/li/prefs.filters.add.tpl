<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_filters.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="addfilter"}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<form name="f1" method="post" action="prefs.php?action=filters&do=createFilter&sid={$sid}" onsubmit="return(checkFilterForm(this));">
	<table class="listTable table custom_tbl_style tbl_border_none">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="addfilter"}</th>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="titel">{lng p="title"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control" name="title" id="titel" value="" style="width:100%;" />
			</td>
		</tr>
		<tr class="custom_check">
			<td class="listTableLeft">
			<label for="active">{lng p="active"}?</label></td>
			<td class="listTableRight">
				<input type="checkbox" id="active" name="active" checked="checked" />
				<label for="active">&nbsp;</label>
			</td>
		</tr>
	
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>

</div></div>
</div>
