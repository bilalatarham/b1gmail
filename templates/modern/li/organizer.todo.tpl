<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_todo.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="todolist"}</span>
	</div>
</div>

<div class="mail-area-content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="taskLists mailbox-left">
					<div class="taskContainer withBottomBar custom_tbl_style_wrap" style="overflow:auto;" id="taskListsScrollContainer">
						<table class="bigTable table custom_tbl_style mb-0">
							<tr>
								<th>{lng p="tasklists"}</th>
							</tr>
						</table>
					
						<div id="taskListsContainer">
							{foreach from=$taskLists item=taskList}
							<a href="#" class="taskList{if $taskList.tasklistid==$taskListID} selected{/if}" onclick="selectTaskList({$taskList.tasklistid})" id="taskList_{$taskList.tasklistid}">
								{text value=$taskList.title}
								{if $taskList.tasklistid!=0}<i class="fa fa-close" onclick="deleteTaskList({$taskList.tasklistid})" ></i>{/if}
							</a>
							{/foreach}
						</div>
					</div>
					
					<div id="contentFooter" class="d-flex justify-content-between">
						<div class="left d-flex align-items-center">
							<i class="fa fa-plus-circle mr-2"></i>
							<input type="text" id="addListTitle" class="smallInput form-control" style="width:120px; height:30px;" onkeypress="return todoListInputKeyPress(event);" />
						</div>
						<div class="right">
							<input type="button" class="smallInput btn btn-primary" value=" {lng p="ok"} " onclick="addTodoList();" />
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-9">
				<div class="taskContents mailbox-right" id="taskListContainer">
					{include file="li/organizer.todo.list.tpl"}
				</div>
			</div>
			
		<img src="{$tpldir}images/li/drag_task.png" style="display:none;" /><img src="{$tpldir}images/li/drag_tasks.png" style="display:none;" />
		</div>
	</div>
</div>




	
	
	

