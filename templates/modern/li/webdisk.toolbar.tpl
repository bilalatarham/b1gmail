<div class="email-toolbar">
	{comment text="space"}
	<div class="progress-area">
		<div class="item-label mr-1">
			<p>{lng p="space"}:</p>
		</div>
		<div class="progress-wrap">
			{progressBar value=$spaceUsed max=$spaceLimit width=100}
			<div class="used-space ml-1">
				<p>{size bytes=$spaceUsed} / {size bytes=$spaceLimit}</p>
			</div>
		</div>
	</div>
	{if $trafficLimit>0}
		{comment text="traffic"}
		<div class="progress-area prg-traffic">
			<div class="item-label mr-1">
				<p>{lng p="traffic"}:</p>
			</div>
			<div class="progress-wrap">
				{progressBar value=$trafficUsed max=$trafficLimit width=100}
				<div class="used-space ml-1">
					<p>{size bytes=$trafficUsed} / {size bytes=$trafficLimit}</p>
				</div>
			</div>
		</div>	
	{/if}

	<div class="email-preview">
		<div class="item-label">
			<p>{lng p="viewmode"}:</p>
		</div>
		<div class="preview-select">
			<select class="smallInput form-control" onchange="updateWebdiskViewMode(this, '{$folderID}', '{$sid}')">
				<option value="icons"{if $viewMode=="icons"} selected="selected"{/if}>{lng p="icons"}</option>
				<option value="list"{if $viewMode=="list"} selected="selected"{/if}>{lng p="list"}</option>
			</select>
		</div>
	</div>
	{hook id="webdisk.toolbar.tpl:lastColumn"}
</div>





{* <table cellspacing="0" cellpadding="0">
	<tr>
		{comment text="space"}
		<td><img align="absmiddle" src="{$tpldir}images/li/tb_sep.gif" border="0" alt="" /></td>
		<td><small>&nbsp; {lng p="space"}: &nbsp;</small></td>
		<td>{progressBar value=$spaceUsed max=$spaceLimit width=100}</td>
		<td><small>&nbsp; {size bytes=$spaceUsed} / {size bytes=$spaceLimit}</small></td>
		
		{if $trafficLimit>0}
		{comment text="traffic"}
		<td width="15">&nbsp;</td>
		<td><img align="absmiddle" src="{$tpldir}images/li/tb_sep.gif" border="0" alt="" /></td>
		<td><small>&nbsp; {lng p="traffic"}: &nbsp;</small></td>
		<td>{progressBar value=$trafficUsed max=$trafficLimit width=100}</td>
		<td><small>&nbsp; {size bytes=$trafficUsed} / {size bytes=$trafficLimit}</small></td>
		{/if}
		
		{comment text="viewmode"}
		<td width="15">&nbsp;</td>
		<td><img align="absmiddle" src="{$tpldir}images/li/tb_sep.gif" border="0" alt="" /></td>
		<td><small>&nbsp; {lng p="viewmode"}: &nbsp;</small></td>
		<td><select class="smallInput" onchange="updateWebdiskViewMode(this, '{$folderID}', '{$sid}')">
			<option value="icons"{if $viewMode=="icons"} selected="selected"{/if}>{lng p="icons"}</option>
			<option value="list"{if $viewMode=="list"} selected="selected"{/if}>{lng p="list"}</option>
		</select></td>
		{hook id="webdisk.toolbar.tpl:lastColumn"}
	</tr>
</table> *}
