<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Calendar Week View</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/loggedin.css" rel="stylesheet" type="text/css" />
	<link href="{$tpldir}style/dtree.css" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script language="javascript">
	<!--
		var currentSID = '{$sid}', tplDir = '{$tpldir}', serverTZ = {$serverTZ};
	//-->
	</script>
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/organizer.js" type="text/javascript" language="javascript"></script>
	<script src="clientlib/dtree.js" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js" type="text/javascript" language="javascript"></script>
	<script src="clientlib/autocomplete.js" type="text/javascript" language="javascript"></script>
</head>

<body onload="initCalendar()" style="background-color:#FFF;background-image:none;">
	<div id="calendarDayBody" class="calendarWeekBody">
	<table class="calendarDayBody">
	{section name=halfHours start=0 loop=48}
	<tr>
	{if $smarty.section.halfHours.index%2==0}
		<td class="calendarDayTimeCell" rowspan="2">
			<div class="calendarDayTimeCellText">{halfHourToTime value=$smarty.section.halfHours.index}</div>
		</td>
	{/if}
	{if $smarty.section.halfHours.index==0}
		<td class="calendarDaySepCell" rowspan="48"></td>
		<td class="calendarDaySepCell2" rowspan="48"></td>
	{/if}	
	{assign var=d value=0}
	{foreach from=$dates key=dayName item=dontCare}
		<td class="calendarDayCell{if $smarty.section.halfHours.index%2}2{/if}{if $smarty.section.halfHours.index>=$dayStart && $smarty.section.halfHours.index<$dayEnd}_day{/if} calendarWeekCell" id="timeRow_{$d}_{$smarty.section.halfHours.index}" style="{if $smarty.section.halfHours.index==0}border-top:0;{/if}">
			&nbsp;
		</td>
	{assign var=d value=$d+1}
	{/foreach}
	</tr>
	{/section}
	</table>
	</div>
	
	<script language="javascript">
	<!--
		var calendarDayStart = {$dayStart},
			calendarDayEnd = {$dayEnd},
			calendarDates = [];

		{assign var=d value=0}
		{foreach from=$dates item=dayDates}
		{foreach from=$dayDates item=date}
		{if ($date.flags&1)==0}
		calendarDates.push([
			{$date.id},
			{$date.startdate},
			{$date.enddate},
			"{text escape=true noentities=true value=$date.title}",
			{$groups[$date.group].color},
			{$d}
		]);
		{/if}
		{/foreach}
		{assign var=d value=$d+1}
		{/foreach}
		
		registerLoadAction('calendarDaySizer()');
		registerLoadAction('initCalendar()');
	//-->
	</script>
</body>

</html>
