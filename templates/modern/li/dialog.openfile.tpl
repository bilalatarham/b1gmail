<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{$title}</title>

	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />

	<!-- New links -->
	<link href="{$tpldir}frontend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700&display=swap" rel="stylesheet"> 
	<link href="{$tpldir}frontend_assets/css/theme.css" rel="stylesheet" type="text/css" />

	<!-- client scripts -->
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js" type="text/javascript" language="javascript"></script>
</head>

<body>
<section class="modal-form">
	<div class="col-md-12">
		<p>{$text}</p>
		<div class="form-wrap">
			<form action="{$formAction}" enctype="multipart/form-data" method="post">
				<div class="file-select-area">
					<span class="icon-area">
						<img src="{$tpldir}images/li/webdisk_file.png" width="16" height="16" border="0" alt="" />
					</span>
					<span class="input-area">
						{fileSelector name="$fieldName" multiple=$multiple}
					</span>
				</div>
				<div class="progress-area mt-2">
					{if $bar}
						{progressBar value=$bar.value max=$bar.max width=100}
					{/if}
				</div>
				<div class="act-btn text-right mt-2">
					<button type="button" class="btn btn-default"onclick="parent.hideOverlay()" >{lng p="cancel"}</button>
					<button type="submit" class="btn btn-success ml-2">{lng p="ok"}</button>
				</div>
			</form>
		</div>
	</div>
</section>

</body>

</html>
