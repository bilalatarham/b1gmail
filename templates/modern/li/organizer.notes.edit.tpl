<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{if $note}{lng p="editnote"}{else}{lng p="addnote"}{/if}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">
	<form name="f1" method="post" action="organizer.notes.php?action={if $note}saveNote&id={$note.id}{else}createNote{/if}&sid={$sid}" onsubmit="return(checkNoteForm(this));">
		<table class="listTable table custom_tbl_style tbl_border_none">
			<tr>
				<th class="listTableHead" colspan="2"> {if $note}{lng p="editnote"}{else}{lng p="addnote"}{/if}</th>
			</tr>
			<tr>
				<td class="listTableLeft"><label for="priority">{lng p="priority"}:</label></td>
				<td class="listTableRight"><select class="form-control field-min-w" name="priority" id="priority">
					<option value="1"{if $note.priority==-1} selected="selected"{/if}>{lng p="prio_1"}</option>
					<option value="0"{if !$note.id || $note.priority==0} selected="selected"{/if}>{lng p="prio_0"}</option>
					<option value="-1"{if $note.priority==-1} selected="selected"{/if}>{lng p="prio_-1"}</option>
				</select></td>
			</tr>
			<tr>
				<td class="listTableLeft"><label for="text">{lng p="text"}<sup>*</sup> :</label></td>
				<td class="listTableRight">
					<textarea class="textInput form-control field-min-w" name="text" id="text" style="height:250px;">{text value=$note.text allowEmpty=true}</textarea>
				</td>
			</tr>
			<tr>
				<td class="listTableLeft">&nbsp;</td>
				<td class="listTableRight">
					<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
					<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
				</td>
			</tr>
		</table>
	</form>
</div></div>
</div>
