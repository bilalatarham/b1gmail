<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">	
			<img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="cancelmembership"}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<div class="info-message-area text-center">

<form action="prefs.php?sid={$sid}" method="post">
<input type="hidden" name="action" value="membership" />
<input type="hidden" name="do" value="reallyCancelAccount" />
<input type="hidden" name="really" id="really" value="false" />

<div class="row">
	<div class="col-md-8 offset-md-2">
		<div class="info-area-content bg-white p-5">
			<div class="image-area mb-3">
				<img src="{$tpldir}images/li/msg.png" width="48" height="48" border="0" alt="" />
			</div>
			<p>{lng p="canceltext"}</p>
			<div class="cta-btns mt-4">
				<input type="button" class="btn btn-primary" value="&laquo; {lng p="back"}" onclick="history.back();" />
				<input type="submit" class="btn btn-danger" value=" {lng p="cancelmembership"} (30) " disabled="disabled" id="cancelButton" />
			</div>
		</div>
	</div>
</div>



</form>
</div>

<script language="javascript">
<!--
	{literal}var i = 30;
	
	function cancelTimer()
	{
		i--;
	
		if(i==0)
		{
			EBID('cancelButton').value = '{/literal}{lng p="cancelmembership"}{literal}';
			EBID('cancelButton').disabled = false;
			EBID('cancelButton').className = 'primary btn btn-danger';
			EBID('really').value = 'true';
		}
		else
		{
			EBID('cancelButton').value = '{/literal}{lng p="cancelmembership"}{literal} (' + i + ')';
			window.setTimeout('cancelTimer()', 1000);
		}
	}
	
	window.setTimeout('cancelTimer()', 1000);{/literal}
//-->
</script>

</div></div>
</div>
