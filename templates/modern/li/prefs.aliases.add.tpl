<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_aliases.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="addalias"}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<form name="f1" method="post" action="prefs.php?action=aliases&do=create&sid={$sid}" onsubmit="return(checkAliasForm(this));">
	<table class="listTable table custom_tbl_style tbl_border_none">
		<tbody class="no-radius">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="addalias"}</th>
		</tr>
		<tr class="custom_check">
			<td class="listTableLeft">{lng p="type"}<sup>*</sup> :</td>
			<td class="listTableRight">
				{if $senderAliases}
				<input type="radio" name="typ" value="1" id="typ_1" checked="checked" onclick="updateAliasForm()" /> 
				<label for="typ_1" class="mr-4">{lng p="aliastype_1"}</label>
				{/if}
				<input type="radio" name="typ" value="3" id="typ_3" onclick="updateAliasForm()"{if !$senderAliases} checked="checked"{/if} /> 
				<label for="typ_3">{lng p="aliastype_1"} + {lng p="aliastype_2"}</label>
			</td>
		</tr>
		</tbody>
		
		<tbody id="tbody_1" class="no-radius" style="display:{if !$senderAliases}none{/if};">
		<tr>
			<td class="listTableLeft"><label for="typ_1_email">{lng p="email"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control field-min-w" name="typ_1_email" id="typ_1_email" value="" size="34" />
				<small>{lng p="typ_1_desc"}</small>
			</td>
		</tr>
		</tbody>
		
		<tbody id="tbody_3" style="display:{if $senderAliases}none{/if};">
		<tr>
			<td class="listTableLeft"><label for="email_local">{lng p="email"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control auto_field" name="email_local" id="email_local" value="" size="20" onblur="checkAddressAvailability()" />
				<select name="email_domain" class="form-control auto_field" id="email_domain" onblur="checkAddressAvailability()">
				{foreach from=$domainList item=domain}
					<option value="{$domain}">@{domain value=$domain}</option>
				{/foreach}
				</select>
				<span id="addressAvailabilityIndicator"></span>
			</td>
		</tr>
		</tbody>
		
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>

</div></div>
</div>
