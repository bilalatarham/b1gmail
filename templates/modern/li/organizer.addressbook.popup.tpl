<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{lng p="addressbook"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />

	<!-- New links -->
	<link href="{$tpldir}frontend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700&display=swap" rel="stylesheet"> 
	<link href="{$tpldir}frontend_assets/css/theme.css" rel="stylesheet" type="text/css" />

	<!-- client scripts -->
	<script language="javascript">
	<!--
		var tplDir = '{$tpldir}';
	//-->
	</script>
	<script src="clientlang.php" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js?{fileDateSig file="js/loggedin.js"}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js?{fileDateSig file="js/dialog.js"}" type="text/javascript" language="javascript"></script>
</head>

<body onload="documentLoader()">
<section class="modal-form">
	<div class="col-md-12">
		<div class="address-box-wrap mb-2">
			<div class="addressDiv form-control" style="height: 120px;" id="addresses"></div>
		</div>
		<div class="address-box-wrap">
			<div class="row">
				<div class="col-12">
					<div class="form-group mb-2">
						<button type="button" class="addrButton btn btn-primary w-auto mr-2" onclick="addAddr('to');">{lng p="to"}</button>
						<div class="addressDiv form-control" style="height: 63px;" id="to"></div>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group mb-2">
						<button type="button" class="addrButton btn btn-primary w-auto mr-2" onclick="addAddr('cc');">CC</button>
						<div class="addressDiv form-control" style="height: 63px;" id="cc"></div>
					</div>
				</div>
				<div class="col-12">
					<div class="form-group mb-2">
						<button type="button" class="addrButton btn btn-primary w-auto mr-2" onclick="addAddr('bcc');">BCC</button>
						<div class="addressDiv form-control" style="height: 63px;" id="bcc"></div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="act-btn text-right mt-2">
			<button type="button" class="btn btn-default" onclick="parent.hideOverlay()" >{lng p="cancel"}</button>
			<button type="submit" class="btn btn-success ml-2" onclick="submitAddressDialog('{$mode}')">{lng p="ok"}</button>
		</div>
	</div>
</section>

	<script language="javascript">
	<!--
		registerLoadAction(initAddressDialog);
	
		var toAddr = [],
			ccAddr = [],
			bccAddr = [],
			Addr = [];
		
		{literal}function initAddressDialog()
		{
			{/literal}{foreach from=$addresses item=address}
			{if ($mode=='handy'&&$address.handy) || ($mode!='handy'&&($address.email1||$address.email2))}
			{$address.type}Addr.push(["{text noentities=true escape=true value=$address.name}",
										"{text noentities=true escape=true value=$address.email1}",
									  	"{text noentities=true escape=true value=$address.email2}",
									  	"{text noentities=true escape=true value=$address.handy}"]);
			{/if}
			{/foreach}
			
			{if $mode!='handy'}
			initEMailAddresses(Addr, toAddr, ccAddr, bccAddr);
			{else}
			initMobileAddresses(Addr, toAddr);
			{/if}
			{literal}
		}{/literal}
	//-->
	</script>
	
</body>

</html>
