<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_extpop3.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{if $account}{lng p="editpop3"}{else}{lng p="addpop3"}{/if}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<form name="f1" method="post" action="prefs.php?action=extpop3&do={if $account}saveAccount&id={$account.id}{else}createAccount{/if}&sid={$sid}" onsubmit="return checkPOP3AccountForm(this);">
	<table class="listTable table custom_tbl_style tbl_border_none">
		<tr>
			<th class="listTableHead" colspan="2"> {if $account}{lng p="editpop3"}{else}{lng p="addpop3"}{/if}</th>
		</tr>
		<tr class="custom_check">
			<td class="listTableLeft"><label for="paused">{lng p="paused"}?</label></td>
			<td class="listTableRight">
				<input type="checkbox" name="paused" id="paused"{if $account && $account.paused} checked="checked"{/if} />
				<label for="paused">&nbsp;</label>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="p_host">{lng p="pop3server"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control field-min-w" name="p_host" id="p_host" value="{text value=$account.p_host allowEmpty=true}" size="48" />
			</td>
		</tr>
		<tr class="custom_check">
			<td class="listTableLeft"><label for="p_port">{lng p="port"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control auto_field mr-4" name="p_port" id="p_port" value="{if $account}{text value=$account.p_port allowEmpty=true}{else}110{/if}" size="6" />
				<input type="checkbox" name="p_ssl" id="p_ssl"{if $account&&$account.p_ssl} checked="checked"{/if} onclick="if(this.checked&&EBID('p_port').value==110) EBID('p_port').value=995; else if(!this.checked&&EBID('p_port').value==995) EBID('p_port').value=110;" />
				<label for="p_ssl">SSL</label>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="p_user">{lng p="username"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control field-min-w" name="p_user" id="p_user" value="{text value=$account.p_user allowEmpty=true}" size="48" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="p_pass">{lng p="password"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="password" class="form-control field-min-w" name="p_pass" id="p_pass" value="{text value=$account.p_pass allowEmpty=true}" size="24" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="p_target">{lng p="pop3target"}:</label></td>
			<td class="listTableRight">
				<select name="p_target" class="form-control field-min-w" id="p_target">
					<option value="-1">({lng p="default"})</option>
					
					<optgroup label="{lng p="folders"}">
					{foreach from=$dropdownFolderList key=dFolderID item=dFolderTitle}
					{if $dFolderID>0}<option value="{$dFolderID}" style="font-family:courier;"{if $account && $account.p_target==$dFolderID} selected="selected"{/if}>{$dFolderTitle}</option>{/if}
					{/foreach}
					</optgroup>
				</select>
			</td>
		</tr>
		<tr class="custom_check">
			<td class="listTableLeft"><label for="p_keep">{lng p="keepmails"}?</label></td>
			<td class="listTableRight">
				<input type="checkbox" name="p_keep" id="p_keep"{if $account && $account.p_keep} checked="checked"{/if} />
				<label for="p_keep">&nbsp;</label>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>

</div></div>
</div>