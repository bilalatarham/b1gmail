<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{if $contact}{lng p="editcontact"}{else}{lng p="addcontact"}{/if}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<form name="f1" method="post" action="organizer.addressbook.php?action={if $contact}saveContact&id={$contact.id}{else}createContact{/if}&sid={$sid}" onsubmit="return(checkContactForm(this));">
	<input type="hidden" id="submitAction" name="submitAction" value="" />
	<table class="listTable table custom_tbl_style tbl_border_none no-radius">
		<tr>
			<th class="listTableHead" colspan="3"> {if $contact}{lng p="editcontact"}{else}{lng p="addcontact"}{/if}</th>
		</tr>
		<tr>
			<td class="listTableLeftDesc" colspan="2">
				<div class="title_head sub_title_style">
					<div class="img_wrap">
						<img src="{$tpldir}images/li/addr_common.png" width="16" height="16" border="0" alt="" />
					</div>
					<span>{lng p="common"}</span>
				</div>
			</td>
			
			<td class="listTableRightest" rowspan="26" width="250">				
				<fieldset>
					<legend>{lng p="userpicture"}</legend>
					<input type="hidden" name="pictureFile" id="pictureFile" value="" />
					<input type="hidden" name="pictureMime" id="pictureMime" value="" />
					<center><div id="pictureDiv" style="background-size: cover; background-position: center center; background-repeat: no-repeat; background-image: url({if !$contact || $contact.picture==''}{$tpldir}images/li/no_picture.png{else}organizer.addressbook.php?action=addressbookPicture&id={$contact.id}&sid={$sid}{/if}); width: 80px; height: 80px;"><a href="javascript:addrUserPicture({if $contact}{$contact.id}{else}-1{/if});"><img src="{$tpldir}images/li/pic_frame.gif" width="80" height="80" border="0" alt="" /></a></div></center>
					<br /><small>{lng p="changepicbyclick"}</small>
				</fieldset>
				<small><br /></small>				
				<fieldset>
					<legend>{lng p="groupmember"}</legend>
					<div align="left" class="custom_check">
						{if !$groups}<label>{lng p="nogroups"}</label>{else}
						{foreach from=$groups item=group key=groupID}
							<input type="checkbox" id="group_{$groupID}" name="group_{$groupID}"{if $group.member} checked="checked"{/if} />
							<label for="group_{$groupID}">{text value=$group.title cut=18}</label><br />
						{/foreach}
						{/if}

						<input type="checkbox" id="group_new" name="group_new" />
						<label for="group_new">&nbsp;</label>
						<input type="text" name="group_new_name" placeholder="{lng p="newgroup"}" value="" class="smallInput form-control w-100" style="width:120px;" onchange="this.onkeypress();" onkeypress="EBID('group_new').checked = this.value.length > 0;" /><br />
					</div>
				</fieldset>
				<small><br /></small>				
				<fieldset>
					<legend>{lng p="features"}</legend>
					<div align="left">
						{if $contact}
							<a href="javascript:addrFunction('exportVCF');"><img src="{$tpldir}images/li/export_vcf.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="exportvcf"}</a><br />
							<a href="javascript:addrFunction('selfComplete');"><img src="{$tpldir}images/li/complete_contact.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="complete"}</a><br />
							<a href="javascript:addrFunction('intelliFolder');"><img src="{$tpldir}images/li/menu_ico_intellifolder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="convfolder"}</a><br />
							<a href="javascript:addrFunction('sendMail');"><img src="{$tpldir}images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="sendmail"}</a><br />
						{else}
							<a href="javascript:addrImportVCF();"><img src="{$tpldir}images/li/import_vcf.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="importvcf"}</a><br />
							<a href="javascript:addrFunction('selfComplete');"><img src="{$tpldir}images/li/complete_contact.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="complete"}</a><br />
						{/if}
					</div>
				</fieldset>
			</td>	
		</tr>
		<tr>
			<td class="listTableLeft"><label for="anrede">{lng p="salutation"}:</label></td>
			<td class="listTableRight">
				<select class="form-control" name="anrede" id="anrede" style="width:100px;">
					<option value=""{if $contact.anrede==''} selected="selected"{/if}>&nbsp;</option>
					<option value="frau"{if $contact.anrede=='frau'} selected="selected"{/if}>{lng p="mrs"}</option>
					<option value="herr"{if $contact.anrede=='herr'} selected="selected"{/if}>{lng p="mr"}</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"> * <label for="vorname">{lng p="firstname"}</label> / * <label for="nachname">{lng p="surname"}:</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control auto_field" name="vorname" id="vorname" value="{text value=$contact.vorname allowEmpty=true}" size="20" />
				<input type="text" class="form-control auto_field" name="nachname" id="nachname" value="{text value=$contact.nachname allowEmpty=true}" size="20" />
			</td>
		</tr>
		
		<tr>
			<td class="listTableLeftDesc" colspan="2">
				<div class="title_head sub_title_style justify-content-between">
					<div class="title-w-check d-flex align-items-center">
						<div class="img_wrap">
							<img src="{$tpldir}images/li/addr_priv.png" width="16" height="16" border="0" alt="" />
						</div>
						<span>{lng p="priv"}</span>
					</div>
					<div class="custom_check">
						<input type="radio" name="default" id="default_priv" value="priv"{if $contact.default_address!=2} checked="checked"{/if} />
						<label for="default_priv">{lng p="default"}</label>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="strassenr">{lng p="streetnr"}</label>:</td>
			<td class="listTableRight">
				<input class="form-control field-min-w" type="text" name="strassenr" id="strassenr" value="{text value=$contact.strassenr allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="plz">{lng p="zipcity"}:</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control auto_field" name="plz" id="plz" value="{text value=$contact.plz allowEmpty=true}" size="6" />
				<input type="text" class="form-control auto_field" name="ort" id="ort" value="{text value=$contact.ort allowEmpty=true}" size="20" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="land">{lng p="country"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="land" id="land" value="{text value=$contact.land allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="email">{lng p="email"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="email" id="email" value="{if $smarty.request.email}{text value=$smarty.request.email}{else}{text value=$contact.email allowEmpty=true}{/if}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="tel">{lng p="phone"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="tel" id="tel" value="{text value=$contact.tel allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="fax">{lng p="fax"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="fax" id="fax" value="{text value=$contact.fax allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="handy">{lng p="mobile"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="handy" id="handy" value="{text value=$contact.handy allowEmpty=true}" size="30" />
			</td>
		</tr>
		
		<tr>
			<td class="listTableLeftDesc" colspan="2">
				<div class="title_head sub_title_style justify-content-between">
					<div class="title-w-check d-flex align-items-center">
						<div class="img_wrap">
							<img src="{$tpldir}images/li/addr_work.png" width="16" height="16" border="0" alt="" />
						</div>
						<span>{lng p="work"}</span>
					</div>
					<div class="custom_check">
						<input type="radio" name="default" id="default_work" value="work"{if $contact.default_address==2} checked="checked"{/if} />
						<label for="default_work">{lng p="default"}</label>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="work_strassenr">{lng p="streetnr"}</label>:</td>
			<td class="listTableRight">
				<input type="text" class="form-control field-min-w" name="work_strassenr" id="work_strassenr" value="{text value=$contact.work_strassenr allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="work_plz">{lng p="zipcity"}:</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control auto_field" name="work_plz" id="work_plz" value="{text value=$contact.work_plz allowEmpty=true}" size="6" />
				<input type="text" class="form-control auto_field" name="work_ort" id="work_ort" value="{text value=$contact.work_ort allowEmpty=true}" size="20" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="work_land">{lng p="country"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="work_land" id="work_land" value="{text value=$contact.work_land allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="work_email">{lng p="email"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="work_email" id="work_email" value="{text value=$contact.work_email allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="tel">{lng p="phone"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="work_tel" id="work_tel" value="{text value=$contact.work_tel allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="fax">{lng p="fax"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="work_fax" id="work_fax" value="{text value=$contact.work_fax allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="work_handy">{lng p="mobile"}:</label></td>
			<td class="listTableRight">	
				<input class="form-control field-min-w" type="text" name="work_handy" id="work_handy" value="{text value=$contact.work_handy allowEmpty=true}" size="30" />
			</td>
		</tr>
		
		<tr>
			<td class="listTableLeftDesc" colspan="2">
				<div class="title_head sub_title_style">
					<div class="img_wrap">
						<img src="{$tpldir}images/li/addr_misc.png" width="16" height="16" border="0" alt="" />
					</div>
					<span>{lng p="misc"}</span>
				</div>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="firma">{lng p="company"}:</label></td>
			<td class="listTableRight"><input class="form-control field-min-w" type="text" name="firma" id="firma" value="{text value=$contact.firma allowEmpty=true}" size="30" /></td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="position">{lng p="position"}</label>:</td>
			<td class="listTableRight">
				<input type="text" class="form-control field-min-w" name="position" id="position" value="{text value=$contact.position allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="web">{lng p="web"}:</label></td>
			<td class="listTableRight">	
				<input type="text" class="form-control field-min-w" name="web" id="web" value="{text value=$contact.web allowEmpty=true}" size="30" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">{lng p="birthday"}:</td>
			<td class="listTableRight">	
				{if $contact.geburtsdatum}
				{html_select_date class="form-control birth-btn-des" time=$contact.geburtsdatum year_empty="---" day_empty="---" month_empty="---" start_year="-120" end_year="+0" prefix="geburtsdatum_" field_order="DMY"}
				{else}
				{html_select_date class="form-control birth-btn-des" time="---" year_empty="---" day_empty="---" month_empty="---" start_year="-120" end_year="+0" prefix="geburtsdatum_" field_order="DMY"}
				{/if}
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="kommentar">{lng p="comment"}:</label></td>
			<td class="listTableRight">	
				<textarea class="textInput form-control field-min-w" name="kommentar" id="kommentar">{text value=$contact.kommentar allowEmpty=true}</textarea>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr> 
	</table>
</form>

{$jsCode}

</div></div>
</div>
