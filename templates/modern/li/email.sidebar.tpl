{hook id="email.sidebar.tpl:head"}
	<div class="compose-btn text-center mb-4">
		<a href="email.compose.php?sid={$sid}" class="btn btn-default-w icon-btn btn-rounded">
			<span class="btn-icon">
				<img src="{$tpldir}frontend_assets/img/ic-plus.svg" class="img-fluid" />
			</span>
			<span class="btn-text fw-bold">Compose</span>
		</a>
	</div>

	<div class="customize-widget-area email-temp-menu mt-0">
		<!--<h4 class="menu-title fw-bold">{lng p="email"}</h4>-->
		<div class="contentMenuIcons" id="folderList"></div>
		<div class="lf-menu-seperator"></div>
		<h4 class="menu-title mt-0 mb-3 fw-med">Manage Folders</h4>
		<div class="create-new-label mt-3">
			<a href="email.folders.php?sid={$sid}" class="create-new-btn">
				<span class="icon-lbl">
					<img src="{$tpldir}frontend_assets/img/menu-ic-email-b.svg" class="img-fluid">
				</span>
				<span class="lbl-text fw-bold">{lng p="folderadmin"}</span>
			</a>
		</div>
		
	</div>
	{hook id="email.sidebar.tpl:email"}


<script language="javascript">
<!--
	{include file="li/email.folderlist.tpl"}
	EBID('folderList').innerHTML = d;
	enableFolderDragTargets();
//-->
</script>

{hook id="email.sidebar.tpl:foot"}
