<div id="contentHeader">
	<div class="pg-title">
		<img src="{$tpldir}frontend_assets/img/ic-setting-d.svg" width="16" height="16" border="0" alt="" align="absmiddle" />
		<p>{lng p="prefs"}</p>
	</div>
</div>

<div class="scrollContainer">
	<div class="container-fluid pl-4 pr-4">
		<div class="row">
			<div class="col-md-12">
				{if $templatePrefs.prefsLayout=='onecolumn'}
				{foreach from=$prefsItems item=null key=item}
				<div class="setting-item">
					<div class="item-icon">
						<a href="prefs.php?action={$item}&sid={$sid}">
							<img src="{if $prefsImages[$item]}{$prefsImages[$item]}{else}{$tpldir}images/li/prefs_{$item}.png{/if}" border="0" alt="" />
						</a>
					</div>
					<div class="item-content">
						<h2><a href="prefs.php?action={$item}&sid={$sid}">{lng p="$item"}</a></h2>
						<p>{lng p="prefs_d_$item"}</p>
					</div>
				</div>
				{/foreach}
			</div>
		</div>
	</div>
	{else}
	<table width="100%" cellpadding="5">
		<tr>
		{assign var="i" value=0}
		{foreach from=$prefsItems item=null key=item}
		{assign var="i" value=$i+1}
		{if $i==3}
		{assign var="i" value=1}
		</tr>
		<tr>
		{/if}
		
		<td width="50%">
			<table width="100%" class="listTable">
				<tr>
					<th colspan="3" class="listTableHead"><a href="prefs.php?action={$item}&sid={$sid}"><b>{lng p="$item"}</b></a></th>
				</tr>
				<tr>
					<td class="listTableIconSide"><img src="{if $prefsImages[$item]}{$prefsImages[$item]}{else}{$tpldir}images/li/prefs_{$item}.png{/if}" width="48" height="48" border="0" alt="" /></td>
					<td valign="top" style="padding:6px;">{lng p="prefs_d_$item"}</td>
					<td class="listTableSide"><a href="prefs.php?action={$item}&sid={$sid}">&raquo;</a></td>
				</tr>
			</table>
		</td>
		
		{/foreach}
		{if $i<3}
		{math assign="i" equation="x - y" x=2 y=$i}
		{section loop=$i name=rest}
			<td>&nbsp;</td> 
		{/section}
		{/if}
		</tr>
	</table>
{/if}

</div>
