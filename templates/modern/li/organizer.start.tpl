{* <div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="overview"}
	</div>
	
	<div class="right">
		<button onclick="document.location.href='organizer.php?action=customize&sid={$sid}';" type="button">
			<img src="{$tpldir}images/li/menu_ico_customize.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="customize"}
		</button>
	</div>
</div> *}

{* <div class="scrollContainer"><div class="pad">
	<div id="startBoxes">
	</div>
	<div id="startBoxes_elems" style="display:none">
	{foreach from=$widgets item=widget key=key}
		<div title="{text value=$widget.title}" rel="{if $widget.hasPrefs}1{else}0{/if},{$widget.prefsW},{$widget.prefsH},{if $widget.icon}{$widget.icon}{else}0{/if}" id="{$key}">{include file=$widget.template}</div>
	{/foreach}
	</div>
</div></div> *}

<div class="scrollContainer">
	<div class="pad">
		<div class="container-fluid pl-4 pr-4">
			<div class="row align-items-start" id="organizer-sortable">
				{foreach from=$widgets item=widget key=key}
					<div class="col-md-6 organizer-sortable-element {if $key!='BMPlugin_Widget_WebdiskDND'}handle{/if}"" title="{text value=$widget.title}" key="{$key}" id="{$key}">
						{include file=$widget.template}
					</div>
				{/foreach}
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	var order = '{$widgetOrder}';
</script>