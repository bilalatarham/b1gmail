<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_calendar.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="calendar"}: {$weekDay}, {date timestamp=$date dayonly=true} ({lng p="cw"} {$calWeek})</span>
	</div>
	<div class="right">
		<button type="button" class="btn btn-default" onclick="document.location.href='organizer.calendar.php?action=groups&sid={$sid}';">
			<img class="mr-2" src="{$tpldir}images/li/ico_calendar_groups.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="editgroups"}
		</button>
	</div>
</div>

<div class="withBottomBar" style="overflow-y:hidden;" id="calendarContainer">
	<div style="overflow-y:scroll;" id="calendarWholeDayBody">
		<table class="calendarWholeDayBody table" style="border-bottom:3px double #B3B8BD; margin-bottom:0;">
		<tr>
			<td class="calendarDayTimeCell">&nbsp;</td>
			<td class="calendarDaySepCell"></td>
			<td class="calendarDaySepCell2"></td>
			<td class="calendarWholeDayCell">
				{foreach from=$dates item=date}
				{if $date.flags&1}
					<div class="calendarDate_{$groups[$date.group].color}" onclick="showCalendarDate({$date.id}, {$date.startdate}, {$date.enddate})">
						{text value=$date.title}
					</div>
				{/if}
				{/foreach}
			</td>
		</tr>
		</table>
	</div>
	<iframe class="calendarDayBody" id="calendarDayBody" src="organizer.calendar.php?action=dayView&date={$theDate}&sid={$sid}" frameborder="0" border="0" style="height:calc(100vh - 190px); width:100%;"></iframe>
</div>

<script language="javascript">
<!--
	registerLoadAction('calendarDaySizer()');
//-->
</script>

<div id="contentFooter">
	<div class="right float-right">
		<button type="button" class="primary btn btn-success" onclick="document.location.href='organizer.calendar.php?action=addDate&date={$theDate}&sid={$sid}';">
			<i class="fa fa-plus-circle"></i>
			{lng p="adddate"}
		</button>
	</div>
</div>
