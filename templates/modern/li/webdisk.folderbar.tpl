{hook id="webdisk.folderbar.tpl:head"}
	<div class="customize-widget-area sidebar-settings mt-0">
		<h4 class="menu-title fw-bold">{lng p="createfolder"}</h4>
		<div class="create-folder">
			<form action="webdisk.php?action=createFolder&folder={$folderID}&sid={$sid}" method="post" onsubmit="return webdiskCreateFolder();">
			<table>
				<tr>
					{* <td width="16"><img src="{$tpldir}images/li/webdisk_folder.png" width="16" height="16" border="0" alt="" /></td> *}
					<td><input type="text" class="form-control" name="folderName" id="folderName" style="width: 150px;" /></td>
					<td><input type="submit" class="btn btn-primary" value="{lng p="ok"}" /></td>
				</tr>
			</table>
			</form>
			{hook id="webdisk.sidebar.tpl:createfolder"}
		</div>
		<div class="lf-menu-seperator"></div>
		<h4 class="menu-title fw-bold">{lng p="folders"}</h4>
		<div class="contentMenuIcons" id="folderList">
		</div>
	</div>

<script language="javascript">
<!--
	{include file="li/webdisk.folderlist.tpl"}
	EBID('folderList').innerHTML = webdisk_d;
	enableWebdiskDragTargets();
//-->
</script>

<img src="{$tpldir}images/li/drag_wdfile.png" style="display:none;" /><img src="{$tpldir}images/li/drag_wdfolder.png" style="display:none;" /><img src="{$tpldir}images/li/drag_wditems.png" style="display:none;" />

{hook id="webdisk.folderbar.tpl:foot"}
