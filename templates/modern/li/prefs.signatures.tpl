<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_signatures.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{lng p="signatures"}</span>
	</div>
</div>

<form name="f1" method="post" action="prefs.php?action=signatures&do=action&sid={$sid}">
<div class="container-fluid">
<div class="scrollContainer withBottomBar custom_tbl_style_wrap">
<table class="bigTable table custom_tbl_style">
	<tr>
		<th width="20"><input type="checkbox" id="allChecker" onclick="checkAll(this.checked, document.forms.f1, 'signature');" /></th>
		<th>
			{lng p="title"}
			<img src="{$tpldir}images/li/asc.gif" border="0" alt="" align="absmiddle" />
		</th>
		<th width="55">&nbsp;</th>
	</tr>
	
	{if $signatureList}
	<tbody class="listTBody">
	{foreach from=$signatureList key=signatureID item=signature}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}" nowrap="nowrap"><input type="checkbox" id="signature_{$signatureID}" name="signature_{$signatureID}" /></td>
		<td class="listTableTDActive" nowrap="nowrap">&nbsp;<a href="prefs.php?action=signatures&do=edit&id={$signatureID}&sid={$sid}"><img src="{$tpldir}images/li/ico_signatures.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {text value=$signature.titel}</a></td>
		<td class="{$class}" nowrap="nowrap">
			<a href="prefs.php?action=signatures&do=edit&id={$signatureID}&sid={$sid}"><img src="{$tpldir}images/li/ico_edit.png" width="16" height="16" border="0" alt="{lng p="edit"}" align="absmiddle" /></a>
			<a onclick="return confirm('{lng p="realdel"}');" href="prefs.php?action=signatures&do=delete&id={$signatureID}&sid={$sid}"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="delete"}" align="absmiddle" /></a>
		</td>
	</tr>
	{/foreach}
	</tbody>
	{/if}
</table>
</div>
</div>

<div id="contentFooter" class="pg-footer">
	<div class="right float-right">
		<button type="button" class="btn btn-success" onclick="document.location.href='prefs.php?action=signatures&do=add&sid={$sid}';">
			<i class="fa fa-plus-circle mr-1"></i>
			{lng p="addsignature"}
		</button>
	</div>
	<div class="left">
		<select class="smallInput form-control arrow-input" name="do2">
			<option value="-">------ {lng p="selaction"} ------</option>
			<option value="delete">{lng p="delete"}</option>
		</select>
		<input class="smallInput btn btn-primary" type="submit" value="{lng p="ok"}" />
	</div>
	
</div>

</form>
