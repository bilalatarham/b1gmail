<!doctype html>
<html lang="{lng p="langCode"}">

<head>
    <!-- title -->
	<title>{if $pageTitle}{text value=$pageTitle} - {/if}{$service_title}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="" />
    <meta property="og:description" content=""> 
    <meta property="og:type" content="website">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="820">

	<!-- links -->
    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" sizes="32x32" href="">
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <title>{if $pageTitle}{text value=$pageTitle} - {/if}{$service_title}</title>
    <link rel="stylesheet" href="{$tpldir}frontend_assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="{$tpldir}frontend_assets/css/theme.css">

    <!-- jquery-ui -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<!-- old links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	{* <link href="{$tpldir}style/loggedin.css?{fileDateSig file="style/loggedin.css"}" rel="stylesheet" type="text/css" />
	<link href="{$tpldir}style/dtree.css?{fileDateSig file="style/dtree.css"}" rel="stylesheet" type="text/css" /> *}
	<link href="clientlib/fontawesome/css/font-awesome.min.css?{fileDateSig file="../../clientlib/fontawesome/css/font-awesome.min.css"}" rel="stylesheet" type="text/css" />
	<link href="clientlib/fontawesome/css/font-awesome-animation.min.css?{fileDateSig file="../../clientlib/fontawesome/css/font-awesome-animation.min.css"}" rel="stylesheet" type="text/css" />
	{foreach from=$_cssFiles.li item=_file}	<link rel="stylesheet" type="text/css" href="{$_file}" />
	{/foreach}

	<!-- old client scripts -->
	<script language="javascript" type="text/javascript">

		var currentSID = '{$sid}', tplDir = '{$tpldir}', serverTZ = {$serverTZ}, ftsBGIndexing = {if $ftsBGIndexing}true{else}false{/if}{if $bmNotifyInterval},
			notifyInterval = {$bmNotifyInterval}, notifySound = {if $bmNotifySound}true{else}false{/if}{/if};

	</script>
	<script src="clientlang.php?sid={$sid}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js?{fileDateSig file="js/common.js"}" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js?{fileDateSig file="js/loggedin.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/dtree.js?{fileDateSig file="../../clientlib/dtree.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js?{fileDateSig file="../../clientlib/overlay.js"}" type="text/javascript" language="javascript"></script>
	<script src="clientlib/autocomplete.js?{fileDateSig file="../../clientlib/autocomplete.js"}" type="text/javascript" language="javascript"></script>
	<!--[if lt IE 9]>
	<script defer type="text/javascript" src="clientlib/IE9.js"></script>
	<![endif]-->
	<!--[if IE]>
	<meta http-equiv="Page-Enter" content="blendTrans(duration=0)" />
	<meta http-equiv="Page-Exit" content="blendTrans(duration=0)" />
	<![endif]-->
	{foreach from=$_jsFiles.li item=_file}	<script type="text/javascript" src="{$_file}"></script>
	{/foreach}
	{hook id="li:index.tpl:head"}
</head>

<body onload="documentLoader()">
	{hook id="li:index.tpl:beforeContent"}

    <div class="left-menu open">
        <div class="logo-area">
            <img src="{$tpldir}frontend_assets/img/logo-icon.png" class="img-fluid logo-icon d-inline-block" />
            <span class="logo-text d-inline-block">{$service_title}</span>
        </div>
        <div class="widget-menu-area">
            {if $possibleWidgets}
                <h5 class="menu-title-small fw-sbold">Start Page</h5>
                <div class="customize-widget-area">
                    <h4 class="menu-title fw-bold">Customize</h4>
                    <div class="widget-item">
                        <form name="f1" method="post" action="start.php?action=saveCustomize&sid={$sid}">
                            {foreach from=$possibleWidgets key=widget item=info}
                                <label class="custom-check">
                                    <div class="item-label">
                                        <span class="label-icon">
                                            <img src="{$info.new_icon}" class="img-fluid" />
                                        </span>
                                        <span class="label-text">{$info.title}</span>
                                    </div>
                                    <input type="checkbox" id="widget_{$widget}" name="widget_{$widget}"{if $info.active} checked="checked"{/if} />
                                    <span class="checkmark"></span>
                                </label>
                            {/foreach}
        
                            <input type="submit" class="btn btn-secondary" value="Save" />
                            <input type="reset" class="btn btn-info pull-right" value="{lng p="reset"}" />
                        </form>
                    </div>
                </div>
            {/if}

            {if !$possibleWidgets}
            <div id="mainMenu" class="up" file={$pageMenuFile}>
                <div id="mainMenuContainer"{if $templatePrefs.navPos=='left'} style="bottom:{math equation="x*29" x=$pageTabsCount}px;"{/if}>
                    {if $pageMenuFile}
                    {comment text="including $pageMenuFile"}
                    {include file="$pageMenuFile"}
                    {else}
                    {foreach from=$pageMenu key=menuID item=menu}
                    {comment text="menuitem $menuID"}
                    <a href="{$menu.link}">
                        <img src="{$tpldir}images/li/menu_ico_{$menu.icon}.png" width="16" height="16" border="0" alt="" align="absmiddle" />
                        {$menu.text}
                    </a>
                    {if $menu.addText}
                    <span class="menuAddText">{$menu.addText}</span>
                    {/if}
                    <br />
                    {/foreach}
                    {/if}
                </div>
                
                {if $templatePrefs.navPos=='left'}
                <ul id="menuTabItems">
                    {foreach from=$pageTabs key=tabID item=tab}
                    {comment text="tab $tabID"}
                    <li{if $activeTab==$tabID} class="active"{/if}>
                        <a href="{$tab.link}{$sid}">
                            <i class="fa {$tab.faIcon}"></i>
                            {if $tab.text}&nbsp;{$tab.text}{/if}
                        </a>
                    </li>
                    {/foreach}
                </ul>
                {/if}
            </div>
            {/if}

            <div class="mobile-menu-bottom d-block d-sm-block d-md-block d-lg-none">
                <div class="site-nav">
                    <div class="nav-top">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link link-notif" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="link-icon">
                                        <img src="{$tpldir}frontend_assets/img/header-ic-bell.svg" class="img-fluid" /> 
                                    </span>
                                    <span class="noti-count fw-bold">3</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right notif-drop notifications">
                                    <a href="#" class="notif-item">
                                        <span class="notif-icon">
                                            <img src="{$tpldir}frontend_assets/img/ic-message.svg" class="img-fluid" />
                                        </span>
                                        <span class="notif-dot"></span>
                                        <span class="notif-text">6 unread messages</span>
                                    </a>
                                    <a href="#" class="notif-item">
                                        <span class="notif-icon">
                                            <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                        </span>
                                        <span class="notif-dot"></span>
                                        <span class="notif-text">8 dates</span>
                                    </a>
                                    <a href="#" class="notif-item">
                                        <span class="notif-icon">
                                            <img src="{$tpldir}frontend_assets/img/ic-clipboard.svg" class="img-fluid" />
                                        </span>
                                        <span class="notif-dot"></span>
                                        <span class="notif-text">1 undone task</span>
                                    </a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link link-setting">
                                    <span class="link-icon">
                                        <img src="{$tpldir}frontend_assets/img/header-ic-settings.svg" class="img-fluid" /> 
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link link-more" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="link-icon">
                                        <img src="{$tpldir}frontend_assets/img/header-ic-more.svg" class="img-fluid" /> 
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right more-items-drop">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <a href="#" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/icon-home.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Start Page</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-email.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Email</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Organizer</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-webdisc.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Webdisc</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Organizer</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </li>
                            <li class="nav-item  dropdown">
                                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link link-user">
                                    <span class="link-icon bg-img" style="background-image: url({$tpldir}frontend_assets/img/header-user-img.png);"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right account-drop">
                                    <a href="#" class="dropdown-item">
                                        <span class="item-icon">
                                            <img src="{$tpldir}frontend_assets/img/ic-logout.svg" class="img-fluid" />
                                        </span>
                                        <span class="item-text fw-med">Logout</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="right-canvas expanded min_height">
        <!--Site Header Start-->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light site-nav fixed" role="navigation">
                <div class="container-fluid pad_0">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <button class="navbar-toggler" type="button">
                        <span class="navbar-toggler-icon">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </span>
                    </button>
                    <div class="d-block d-sm-none">
                        <a class="navbar-brand" href="#">
                            <img src="{$tpldir}frontend_assets/img/logo-icon.png" class="img-fluid logo-icon d-inline-block" />
                        </a>
                    </div>
                    <div class="toolbar header-toolbar" file={$pageToolbarFile}>
                        {if $pageToolbarFile}
                            {comment text="including $pageToolbarFile"}
                            {include file="$pageToolbarFile"}
                        {elseif $pageToolbar}
                            {$pageToolbar}
                        {else}
                            &nbsp;
                        {/if}
                    </div>
                    {*<div class="search-form head-search-wrap">
                         <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">
                                    <img src="{$tpldir}frontend_assets/img/ic-search.svg" class="img-fluid" />
                                </span>
                            </div>
                            <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon1" id="searchField" name="searchField" onkeypress="searchFieldKeyPress(event,{if $searchDetailsDefault}true{else}false{/if})" />
                        </div> *}
                        {* <a href="#" class="search-btn" onclick="showSearchPopup(this)" title="{lng p="search"}"><i class="fa fa-search"></i></a> 
                    </div>*}
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="nav-top collapse navbar-collapse" id="navbar-main">
                        <ul class="navbar-nav nav-mid ml-auto">
                            <li class="nav-item search-form head-search-wrap">
                                <a href="#" class="search-btn" onclick="showSearchPopup(this)" title="{lng p="search"}"><i class="fa fa-search"></i></a>
                            </li>
                            <li class="nav-item dropdown">
								{if $bmNotifyInterval>0}
									<a href="#" onclick="showNotifications(this)" title="{lng p="notifications"}" class="nav-link link-notif" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span id="notifyIcon" class="link-icon">
											<img src="{$tpldir}frontend_assets/img/header-ic-bell.svg" class="img-fluid" /> 
										</span>
										<span id="notifyCount" class="noti-count fw-bold" {if $bmUnreadNotifications==0} style="display:none;"{/if}>{number value=$bmUnreadNotifications min=0 max=99}</span>
									</a>
								{/if}
                                <div id="notifyBox" class="dropdown-menu dropdown-menu-right notif-drop notifications notifyBell-icon">
									<div class="inner" id="notifyInner">
                                    </div>
                                    {* <a href="#" class="notif-item">
                                        <span class="notif-icon">
                                            <img src="{$tpldir}frontend_assets/img/ic-message.svg" class="img-fluid" />
                                        </span>
                                        <span class="notif-dot"></span>
                                        <span class="notif-text">6 unread messages</span>
                                    </a>
                                    <a href="#" class="notif-item">
                                        <span class="notif-icon">
                                            <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                        </span>
                                        <span class="notif-dot"></span>
                                        <span class="notif-text">8 dates</span>
                                    </a>
                                    <a href="#" class="notif-item">
                                        <span class="notif-icon">
                                            <img src="{$tpldir}frontend_assets/img/ic-clipboard.svg" class="img-fluid" />
                                        </span>
                                        <span class="notif-dot"></span>
                                        <span class="notif-text">1 undone task</span>
                                    </a> *}
                                </div>
                            </li>
                            <li class="nav-item">
                                <a href="prefs.php?action=faq&sid={$sid}" title="{lng p="faq"}" class="nav-link link-setting">
                                    <span class="link-icon">
                                        <img src="{$tpldir}frontend_assets/img/header-ic-settings.svg" class="img-fluid" /> 
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link link-more" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="link-icon">
                                        <img src="{$tpldir}frontend_assets/img/header-ic-more.svg" class="img-fluid" /> 
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right more-items-drop">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <a href="start.php?sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/icon-home.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Start Page</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="email.php?sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-email.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Email</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="organizer.php?sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Organizer</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="webdisk.php?sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-webdisc.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Webdisc</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="prefs.php?sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Settings</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link link-more" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="link-icon">
                                        <img src="{$tpldir}frontend_assets/img/menu-ic-link.svg" class="img-fluid img-white" /> 
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right more-items-drop">
                                    <div class="row no-gutters">
                                        <div class="col-md-4">
                                            <a href="email.compose.php?sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-email.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Email</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="organizer.calendar.php?action=addDate&sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Date</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="organizer.todo.php?action=addTask&sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-task.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Task</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="organizer.addressbook.php?action=addContact&sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/ic-addressbook.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Contact</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="organizer.notes.php?action=addNote&sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/menu-ic-notes.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">Note</span>
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="webdisk.php?do=uploadFilesForm&sid={$sid}" class="more-menu-item text-center">
                                                <span class="d-block more-item-icon">
                                                    <img src="{$tpldir}frontend_assets/img/ic-folder.svg" class="img-fluid" />
                                                </span>
                                                <span class="more-item-text d-block">File</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item  dropdown">
                                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link link-user">
                                    <span class="link-icon bg-img" style="background-image: url({$tpldir}frontend_assets/img/header-user-img.png);"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right account-drop">
                                    <a href="start.php?sid={$sid}&action=logout" onclick="return confirm('{lng p="logoutquestion"}');" title="{lng p="logout"}" class="dropdown-item">
                                        <span class="item-icon">
                                            <img src="{$tpldir}frontend_assets/img/ic-logout.svg" class="img-fluid" />
                                        </span>
                                        <span class="item-text fw-med">Logout</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </header><!--Site Header End-->

        {comment text="search popup"}
	    <div class="headerBox" id="searchPopup" style="display:none">
			<div class="arrow"></div>
			<div class="inner">
				<table width="100%" cellspacing="0" cellpadding="0" class="up" onmouseover="disableHide=true;" onmouseout="disableHide=false;">
					<tr>
						<td class="p-0 border-none">
							{if $templatePrefs.navPos=='top'}<div class="arrow"></div>{/if}
							<table cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td width="22" height="26" align="right"><img id="searchSpinner" style="display:none;" src="{$tpldir}images/load_16.gif" border="0" alt="" width="16" height="16" align="absmiddle" /></td>
									<td align="right" width="70">{lng p="search"}: &nbsp;</td>
									<td align="center">
										<input id="searchField" name="searchField" style="width:90%" onkeypress="searchFieldKeyPress(event,{if $searchDetailsDefault}true{else}false{/if})" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tbody id="searchResultBody" style="display:none">
					<tr>
						<td id="searchResults" class="p-0 border-none"></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
        {comment text="new menu"}
		<div class="headerBox" id="newMenu" style="display:none;">
			<div class="arrow"></div>
			<div class="inner">
			{foreach from=$newMenu item=item}
				{if $item.sep}
				<div class="mailMenuSep"></div>
				{else}
				<a class="mailMenuItem" href="{$item.link}{$sid}"><img align="absmiddle" src="{if !$item.iconDir}{$tpldir}images/li/{else}{$item.iconDir}{/if}{$item.icon}.png" width="16" height="16" border="0" alt="" /> {$item.text}...</a>
				{/if}
			{/foreach}
			</div>
		</div>

		<div id="mainBanner" style="display:none;">
			{banner}
		</div>

		<div id="mainContent" class="up">
			{include file="$pageContent"}
		</div>
		
		<div id="mainStatusBar">
			{literal}&nbsp;{/literal} 
		</div>
		
    </section>

	{hook id="li:index.tpl:afterContent"}

    <!-- scripts -->
    <script src="{$tpldir}frontend_assets/js/jquery.min.js"></script>
    <script src="{$tpldir}frontend_assets/js/popper.min.js"></script> 
    <script src="{$tpldir}frontend_assets/js/bootstrap.min.js"></script>
    <script src="{$tpldir}frontend_assets/js/custom.js"></script>

    <!-- sortable-->
    <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>

    <!-- masonry -->
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

    <!-- custom function -->
    <script src="{$tpldir}js/custom.functions.js" type="text/javascript" language="javascript"></script>
</body>
</html>
