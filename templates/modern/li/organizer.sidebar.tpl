<div class="customize-widget-area sidebar-settings mt-0">
	<h4 class="menu-title fw-bold">{lng p="organizer"}</h4>
	<div class="contentMenuIcons">
		<div class="dtree">
			<div class="dTreeNode">
				<img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="organizer.php?sid={$sid}"> {lng p="overview"}</a>
			</div>
		</div>
		<div class="dtree">
			<div class="dTreeNode">
				<img src="{$tpldir}images/li/ico_calendar.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="organizer.calendar.php?sid={$sid}"> {lng p="calendar"}</a>
			</div>
		</div>
		<div class="dtree">
			<div class="dTreeNode">
				<img src="{$tpldir}images/li/ico_todo.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="organizer.todo.php?sid={$sid}"> {lng p="todolist"}</a>
			</div>
		</div>
		<div class="dtree">
			<div class="dTreeNode">
				<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="organizer.addressbook.php?sid={$sid}"> {lng p="addressbook"}</a>
			</div>
		</div>
		<div class="dtree">
			<div class="dTreeNode">
				<img src="{$tpldir}images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
				<a href="organizer.notes.php?sid={$sid}"> {lng p="notes"}</a>
			</div>
		</div>
	</div>
	<div class="lf-menu-seperator"></div>
	<div class="contentMenuIcons widget-item">
		<h4 class="menu-title fw-bold">{lng p="tasks"}</h4>
		{foreach from=$tasks key=taskID item=task}
		<label class="custom-check">
			<div class="item-label">
				<span class="label-text"><a href="organizer.todo.php?action=editTask&id={$taskID}&sid={$sid}">{text value=$task.titel cut=20}</a></span>
			</div>
			<input type="checkbox" id="sbTask_{$taskID}" onclick="setTaskDone('{$sid}', {$taskID}, this.checked);"{if $task.akt_status==64} checked="checked"{/if} />
			<span class="checkmark"></span>
		</label>
		{* <input type="checkbox" id="sbTask_{$taskID}" onclick="setTaskDone('{$sid}', {$taskID}, this.checked);"{if $task.akt_status==64} checked="checked"{/if} />
		<a href="organizer.todo.php?action=editTask&id={$taskID}&sid={$sid}">{text value=$task.titel cut=20}</a><br /> *}
		{/foreach}
		{if $tasks_haveMore}
			<small><a href="organizer.todo.php?sid={$sid}">{lng p="more"}...</a></small><br />
		{/if}
	</div>
	<div class="lf-menu-seperator"></div>
	<div class="sidebar-calendar">
		<h4 class="menu-title fw-bold">{lng p="calendar"}</h4>
		{miniCalendar}
	</div>
</div>
