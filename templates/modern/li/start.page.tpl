{* <div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/menu_ico_start.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="welcome"}!
	</div>
	
	<div class="right">
		{if $templatePrefs.showUserEmail}
		<em>{$_userEmail}</em>
		{/if}
		<button onclick="document.location.href='start.php?action=customize&sid={$sid}';" type="button">
			<img src="{$tpldir}images/li/menu_ico_customize.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			{lng p="customize"}
		</button>
	</div>
</div> *}

<div class="scrollContainer"><div class="pad">
	{hook id="start.page.tpl:head"}
	<div class="container-fluid pl-4 pr-4">
        <div class="row align-items-start" id="sortable">
			{foreach from=$widgets item=widget key=key}
				<div class="col-md-6 sortable-element {if $key!='BMPlugin_Widget_WebdiskDND'}handle{/if}" title="{text value=$widget.title}" key="{$key}" id="{$key}">
					{include file=$widget.template}
				</div>
			{/foreach}
			{* <div id="startBoxes">
			</div>
			<div id="startBoxes_elems" style="display:none">
			{foreach from=$widgets item=widget key=key}
				<div title="{text value=$widget.title}" rel="{if $widget.hasPrefs}1{else}0{/if},{$widget.prefsW},{$widget.prefsH},{if $widget.icon}{$widget.icon}{else}0{/if}" id="{$key}">{include file=$widget.template}</div>
			{/foreach}
			</div> *}
		</div>
	</div>
	
	{hook id="start.page.tpl:foot"}
</div></div>

<script language="javascript" type="text/javascript">
	var order = '{$widgetOrder}';
</script>
