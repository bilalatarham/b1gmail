<div id="contentHeader" class="d-flex justify-content-between">
	<div class="left titleWith_icon d-flex align-items-center">
		<div class="img_wrap">
			<img src="{$tpldir}images/li/ico_calendar_groups.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		</div>
		<span class="page_title">{if $group}{lng p="editgroup"}{else}{lng p="addgroup"}{/if}</span>
	</div>
</div>

<div class="scrollContainer">
<div class="container-fluid">
<div class="pad">

<form name="f2" method="post" action="organizer.calendar.php?action=groups&do={if $group}save&id={$group.id}{else}add{/if}&sid={$sid}" onsubmit="return(checkCalendarGroupForm(this));">
	<table class="listTable table custom_tbl_style tbl_border_none no-radius">
		<tr>
			<th class="listTableHead" colspan="2"> {if $group}{lng p="editgroup"}{else}{lng p="addgroup"}{/if}</th>
		</tr>
		
		<tr>
			<td class="listTableLeft"><label for="title">{lng p="title"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<input type="text" class="form-control" name="title" id="title" value="{text value=$group.title allowEmpty=true}" size="34" style="width:100%;" />
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="title">{lng p="color"}<sup>*</sup> :</label></td>
			<td class="listTableRight">
				<table>
					<tr class="custom_check">
						<td><input type="radio" id="color0" {if !$group||$group.color==0} checked="checked"{/if} name="color" value="0" />
							<label for="color0">
								<div class="calendarDate_0" style="padding:0px;margin:0px;margin-left:8px;width:30px;height:30px;"></div>
							</label>
						</td>
					</tr>
					<tr class="custom_check">
						<td><input type="radio" id="color1" {if $group.color==1} checked="checked"{/if} name="color" value="1" />
							<label for="color1">
								<div class="calendarDate_1" style="padding:0px;margin:0px;margin-left:8px;width:30px;height:30px;"></div>
							</label>
						</td>
					</tr>
					<tr class="custom_check">
						<td><input type="radio" id="color2" {if $group.color==2}  checked="checked"{/if} name="color" value="2" />
							<label for="color2">
								<div class="calendarDate_2" style="padding:0px;margin:0px;margin-left:8px;width:30px;height:30px;"></div>
							</label>
						</td>
					</tr>
					<tr class="custom_check">
						<td><input type="radio"id="color3"  {if $group.color==3} checked="checked"{/if} name="color" value="3" />
							<label for="color3">
								<div class="calendarDate_3" style="padding:0px;margin:0px;margin-left:8px;width:30px;height:30px;"></div>
							</label>
						</td>
					</tr>
					<tr class="custom_check">
						<td><input type="radio" id="color4" {if $group.color==4} checked="checked"{/if} name="color" value="4" />
							<label for="color4">
								<div class="calendarDate_4" style="padding:0px;margin:0px;margin-left:8px;width:30px;height:30px;"></div>
							</label>
						</td>
					</tr>
					<tr class="custom_check">
						<td><input type="radio" id="color5" {if $group.color==5} checked="checked"{/if} name="color" value="5" />
							<label for="color5">
								<div class="calendarDate_5" style="padding:0px;margin:0px;margin-left:8px;width:30px;height:30px;"></div>
							</label>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" class="primary btn btn-primary" value="{lng p="ok"}" />
				<input type="reset" class="btn btn-danger" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>

</div></div>
</div>
