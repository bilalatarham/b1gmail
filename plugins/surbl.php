<?php
/*
 * Plugin surbl
 */
class surbl extends BMPlugin 
{
	/*
	 * Eigenschaften des Plugins
	 */
	function surbl()
	{
		$this->name					= 'SURBL';
		$this->version				= '1.0.0';
		$this->designedfor			= '7.3.0';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'dotaachen';
		$this->mail					= 'b1g@dotaachen.net';
		$this->web 					= 'http://b1g.dotaachen.net';

		$this->update_url			= 'http://my.b1gmail.com/update_service/';
		$this->website				= 'http://my.b1gmail.com/details/161/';

		$this->admin_pages			= true;
		$this->admin_page_title		= 'SURBL';
		$this->admin_page_icon		= "surbl_icon.png";
	}

	/*
	 *  Link  und Tabs im Adminbereich 
	 */
	function AdminHandler()
	{
		global $tpl, $lang_admin;

		// Plugin aufruf ohne Action
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'page1';

		// Tabs im Adminbereich
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['surbl_name'],
				'link'		=> $this->_adminLink() . '&action=page1&',
				'active'	=> $_REQUEST['action'] == 'page1',
				'icon'		=> '../plugins/templates/images/surbl_logo.png'
			),
			1 => array(
				'title'		=> $lang_admin['surbl_list'],
				'link'		=> $this->_adminLink() . '&action=page2&',
				'active'	=> $_REQUEST['action'] == 'page2',
				'icon'		=> '../plugins/templates/images/surbl_logo.png'
			),
			2 => array(
				'title'		=> $lang_admin['surbl_blacklist'],
				'link'		=> $this->_adminLink() . '&action=page3&',
				'active'	=> $_REQUEST['action'] == 'page3',
				'icon'		=> '../plugins/templates/images/surbl_logo.png'
			),
			3 => array(
				'title'		=> $lang_admin['surbl_whitelist'],
				'link'		=> $this->_adminLink() . '&action=page4&',
				'active'	=> $_REQUEST['action'] == 'page4',
				'icon'		=> '../plugins/templates/images/surbl_logo.png'
			)
		);
		$tpl->assign('tabs', $tabs);

		// Plugin aufruf mit Action 
		if($_REQUEST['action'] == 'page1') {
			$tpl->assign('page', $this->_templatePath('surbl1.pref.tpl'));
			$this->_Page1();
		} else if($_REQUEST['action'] == 'page2') {
			$tpl->assign('page', $this->_templatePath('surbl2.pref.tpl'));
			$this->_Page2();
		} else if($_REQUEST['action'] == 'page3') {
			$tpl->assign('page', $this->_templatePath('surbl3.pref.tpl'));
			$this->_Page3();
		} else if($_REQUEST['action'] == 'page4') {
			$tpl->assign('page', $this->_templatePath('surbl4.pref.tpl'));
			$this->_Page4();
		}
	}

	/*
	 *  Sprach variablen
	 */
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		$lang_admin['surbl_name']					= "SURBL";
		$lang_admin['surbl_text']					= "SURBL's sind Listen von Webseiten, die in unerwünschten E-Mails auftauchen.";		

		$lang_admin['surbl_filter']					= "SURBL-Filter";
		$lang_admin['surbl_list']					= "SURBL-Liste";
		$lang_admin['surbl_blacklist']				= "Blacklist";
		$lang_admin['surbl_whitelist']				= "Whitelist";
		$lang_admin['surbl_server']					= "SURBL-Server";
		$lang_admin['surbl_required']				= "Erforderlicher SURBL Score";
	}

	/*
	 * installation routine
	 */	
	function Install()
	{
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	function _Page1()
	{
		global $tpl;

		// save
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$surblArray = explode("\n", $_REQUEST['surbl_bl']);
			foreach($surblArray as $key=>$val)
				if(($val = trim($val)) != '')
					$surblArray[$key] = $val;
				else 
					unset($surblArray[$key]);
			$surbl = implode(':', $surblArray);

			$this->_setPref("surbl_aktiv", 		isset($_REQUEST['surbl_aktiv']));
			$this->_setPref("surbl_surbl",		$surbl);
			$this->_setPref("surbl_required",	$_REQUEST['surbl_required']);
		}
		//reset
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'reset')
		{
			$this->_setPref("surbl_calls", 	0);
		}

		$surbl_bl 		= str_replace(':', "\n", $this->_getPref("surbl_surbl"));
		$tpl->assign('surbl_aktiv', 		$this->_getPref("surbl_aktiv"));
		$tpl->assign('surbl_bl',			$surbl_bl);
		$tpl->assign('surbl_required',		$this->_getPref("surbl_required"));
		$tpl->assign('surblCount',			$this->_getPref("surbl_calls"));
	}

	function _Page2()
	{
		global $tpl;

		// save
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$surblArray = explode("\n", $_REQUEST['surbl_list']);
			foreach($surblArray as $key=>$val)
				if(($val = trim($val)) != '')
					$surblArray[$key] = $val;
				else 
					unset($surblArray[$key]);
			$surbl = implode(':', $surblArray);

			$this->_setPref("surbl_list",		$surbl);
		}

		$surbl_list 		= str_replace(':', "\n", $this->_getPref("surbl_list"));
		$tpl->assign('surbl_list',			$surbl_list);

	}

	function _Page3()
	{
		global $tpl;

		// save
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$surblArray = explode("\n", $_REQUEST['surbl_bl']);
			foreach($surblArray as $key=>$val)
				if(($val = trim($val)) != '')
					$surblArray[$key] = $val;
				else 
					unset($surblArray[$key]);
			$surbl = implode(':', $surblArray);

			$this->_setPref("surbl_bl",		$surbl);
		}

		$surbl_bl 		= str_replace(':', "\n", $this->_getPref("surbl_bl"));
		$tpl->assign('surbl_bl',			$surbl_bl);

	}
	
	function _Page4()
	{
		global $tpl;

		// save
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$surblArray = explode("\n", $_REQUEST['surbl_wl']);
			foreach($surblArray as $key=>$val)
				if(($val = trim($val)) != '')
					$surblArray[$key] = $val;
				else 
					unset($surblArray[$key]);
			$surbl = implode(':', $surblArray);

			$this->_setPref("surbl_wl",		$surbl);
		}

		$surbl_wl 		= str_replace(':', "\n", $this->_getPref("surbl_wl"));
		$tpl->assign('surbl_wl',			$surbl_wl);

	}

	function AfterReceiveMail(&$mail, &$mailbox, &$user)
	{
		if($this->_getPref("surbl_aktiv"))
		{
			if(($mail->flags & FLAG_SPAM) != 0)
				return;

			$msg 			= $mail->GetTextParts();
			if($msg['html'] == null) {
				$msg 		= strip_tags($msg['text']);
			} else {
				$msg 		= strip_tags($msg['html']);
			}

			$ret_arr_full 	= array();
			if(preg_match_all('((ht|f)tp(s?)\://{1}\S+)', $msg, $ret_arr_full))
			{
				$surbl 			= $this->_getPref("surbl_surbl");
				$list 			= $this->_getPref("surbl_list");
				$wl 			= $this->_getPref("surbl_wl");
				$bl 			= $this->_getPref("surbl_bl");
				$surbl_required	= $this->_getPref("surbl_required");
				$surbl_calls	= $this->_getPref("surbl_calls");

				$surblServers 	= explode(':', $surbl);
				$grList			= explode(':', $list);
				$wlList			= explode(':', $wl);
				$blList			= explode(':', $bl);

				$posServers 	= 0;
				$domains 		= array();
				foreach($ret_arr_full[0] as $domain)
				{
					$uri		= $this->cutDomain($domain);
					
					if($uri !== false)
					{
						$domains[] 	= $uri;
					}
				}

				$checked 	= array();
				foreach($domains as $domain) 
				{
					$surblServers = explode(':', $surbl);
					// ip
					if($this->isIP($domain))
					{
						continue;
					}
					// whitelist -4
					if(in_array($domain, $wlList))
					{
						$posServers = $posServers-4;
						continue;
					}
					// blacklist +5
					if(in_array($domain, $blList))
					{
						$posServers = $posServers+5;
						continue;
					}
					// list +2
					if(in_array($domain, $grList))
					{
						$posServers = $posServers+2;
						continue;
					}
					// good domain and already surbl checked
					if(in_array($domain, $checked))
					{
						continue;
					}
					// surbl +2
					foreach($surblServers as $server)
					{
						$dns 				= @gethostbyname($domain . '.' . $server);
						$surbl_calls		= $surbl_calls+1;

						if($dns == "127.0.1.255")
						{
							PutLog(sprintf('SURBL - <%s> is not a valid Domain', $domain), PRIO_PLUGIN, __FILE__, __LINE__);
						} else if($dns == "127.0.1.2" OR $dns == "127.0.1.3") {							
							$posServers 	= $posServers+2;
							$list			= $list.":".$domain;
							$grList			= explode(':', $list);
						} else {
							$checked[] 		= $domain;
						}
					}
					$this->_setPref("surbl_calls",	$surbl_calls);
					$this->_setPref("surbl_list",	$list);
				}

				if($posServers >= $surbl_required)
				{
					PutLog(sprintf('Mail to %s identified as spam by SURBL Plugin with Score <%s>', $user->_id, $posServers), PRIO_PLUGIN, __FILE__, __LINE__);

					// SetSpamStatus
					$mailbox->SetSpamStatus($mail->id, true);
				}
			}
		}
	}

	function isIP($ipaddr)
	{
		if(preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $ipaddr))
		{
			return true;
		}
		return false;
	}

	function cutDomain($domain)
	{
		$domain 	= str_replace("https://", "", $domain);	
		$domain 	= str_replace("http://", "", $domain);
		$domain 	= str_replace("www.", "", $domain);

		$pos		= strpos($domain, "/");
		if($pos !== false)
		{
			$domain 	= substr($domain, 0, $pos);
		}

		$pos		= strpos($domain, "?");
		if($pos !== false)
		{
			$domain 	= substr($domain, 0, $pos);
		}

		if(strlen($domain) <= 4)
		{
			return false;
		}

		return strtolower($domain);
	}
}
/*
 * register plugin
 */
$plugins->registerPlugin('surbl');
?>