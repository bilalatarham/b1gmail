<?php

/**
 * Plugin example: Disable Users after registration
 *
 */

class DUR extends BMPlugin
{
   function DUR()
   {
      $this->name             = 'Disable Users after registration';
      $this->author           = 'Philipp Hertweck';
      $this->web              = 'http://www.pilleslife.de';
      $this->mail             = 'webmaster@pilleslife.de';
      $this->version          = '1.0';
      $this->designedfor      = '7.2.0';
      $this->type             = BMPLUGIN_DEFAULT;

      $this->update_url = 'http://my.b1gmail.com/update_service/';
  
	  // group option
     $this->RegisterGroupOption('DUR_enable',FIELD_CHECKBOX,'User dieser Gruppe nach eine gewissen Zeit nach der Registrierung deaktivieren?');
	  $this->RegisterGroupOption('DUR_number',FIELD_TEXT,'Deaktivieren nach');
	  $einheit = array(
	  	's' => 'Sekunden',
		'm' => 'Minuten',
		'h' => 'Stunden',
		'd' => 'Tage',
		'mon' => 'Monate',
		'a' => 'Jahre'
	  );
	  $this->RegisterGroupOption('DUR_unit',FIELD_DROPDOWN,'Einheit',$einheit,'d');
   }
      //Datenbanktabellen anlegen
   function Install(){
	   global $db;
	   
	   $db->query("CREATE TABLE `{pre}DUR_settings` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`time` INT NOT NULL) ENGINE = MYISAM ;");
	   $db->query("INSERT INTO `{pre}DUR_settings` (`id` , `time`) VALUES ('1', '0');");

	   return true;
   }
   
   //Datenbanktabellen deinstallieren
   function Uninstall(){
	   global $db;
	   
	   $db->query("DROP TABLE `{pre}DUR_settings`");
	   
	   return true;
   }
   
   //Benutzer nach gewisser Zeit deaktivieren
   function OnCron(){
	   global $db;
	   
	   //Letzte Ausführungszeit auslesen
	   $res=$db->query("SELECT * FROM `{pre}DUR_settings` WHERE `id` =1");
	   while($data = $res->FetchArray(MYSQL_ASSOC)){
	   		$lastaction = $data["time"];
	   }
	   $res->Free();
	   
	   //Wenn heute noch nicht ausgeführt
	   if(date("z",$lastaction) != date("z",time())){
	   
		   //Variablen um in Sekunden umzurechnen
		   $minute = 60;
		   $stunde = 60*$minute;
		   $tag = 24*$stunde;
		   $monat = 31*$tag;
		   $jahr = 365*$tag;
		   
		   //Überprüfen, welche Gruppe
		   $res = $db->query("SELECT `id` FROM `{pre}gruppen`");
		   while($data = $res->FetchArray(MYSQL_ASSOC)){
				if($this->GetGroupOptionValue('DUR_enable',$data["id"])){
					//Deadline berechnen
					$number = $this->GetGroupOptionValue('DUR_number',$data["id"]);
					switch($this->GetGroupOptionValue('DUR_unit',$data["id"])){
						case "s":
						$seconds = $number;
						break;
						
						case "m":
						$seconds = $number*$minute;
						break;
						
						case "h":
						$seconds = $number*$stunde;
						break;
						
						case "d":
						$seconds = $number*$tag;
						break;
						
						case "mon":
						$seconds = $number*$monat;
						break;
						
						case "a":
						$seconds = $number*$jahr;
						break;
					}
					
					$deadline = time() - $seconds;
					
					//Alle Benutzer der Gruppe auslesen
					$db->query("UPDATE `{pre}users` SET `gesperrt` = 'yes' WHERE `gruppe` =? AND `reg_date` < ?;",$data["id"],$deadline);
					PutLog(mysql_affected_rows()." Benutzer erfolgreich erfolgreich deaktiviert (Zeit nach Registrierung &uuml;berschritten." , PRIO_NOTE , __FILE__, __LINE__);
				}
		   }	   
		   $res->Free();
		   
		   $db->query("UPDATE `{pre}DUR_settings` SET `time` = ? WHERE `id` =1;",time());
   }
   }
	
}

/**
 * register plugin
 */
$plugins->registerPlugin('DUR');
?>