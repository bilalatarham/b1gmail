<?php 
/*
 * 
 * (c) IT- und Webl�sungen Kretschmar ::: www.iscgr.de
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: demouser.plugin.php,v 1.1 2012/10/17 18:30:00 Informant $
 *
 */
 
// Damit das Plugin funktioniert, muss eine Gruppe angelegt werden, welche im folgenden eingetragen werden muss. 
// Weiterhin muss ein Benutzer in dieser Gruppe erstellt werden. Anschlie�end arbeitet das Plugin voll automatisch und l�scht/erstellt alle 6 Stunden den Demo-Benutzer.

// Gruppenname
define("GROUPNAME", 'demo');
// Demo-User E-Mail-Adresse
define("DUMAIL", 'demo@aikq.de');
// Demo-User Passwort
define("DUPW", 'demo');
// Fehlermeldung bei Aufruf der gesperrten Seiten
define("DUEMSG", 'Diese Funktion steht Ihnen im Demo-Account nicht zur Verf&uuml;gung!');

//include('./serverlib/admin.inc.php');
 
class demouser extends BMPlugin 
{
	function demouser()
	{
		global $lang_user;
		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Demo-Benutzer';
		$this->author			= 'Informant';
		$this->version			= '1.1';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
	   global $currentCharset;
       
		if($lang == 'deutsch')
		{
			$lang_custom['du_betreff']				= 'Willkommen bei aikQ';
			$lang_custom['du_StatusTXT']			= 'Lieber Test-User,' . "\n\n"
													. 'Herzlich Willkommen im Demo-Account von aikQ! Hier k�nnen Sie sich in Ruhe umsehen einen �berblick �ber die Funktionen und M�glichkeiten Verschaffen, die im Standard-Account im vollem Umfang zur Verf�gung stehen.' . "\n\n"
													. 'Wenn wir sie von aikQ �berzeugen k�nnen, wollen wir Ihnen den Einstieg so bequem wie m�glich machen � ein kompletter Wechsel von Ihrem alten Anbieter zu aikQ ist darum nicht n�tig, denn Sie k�nnen all ihre E-Mails, egal bei welchem  E-Mail Provider, �bersichtlich und zeitsparend �ber aikQ abrufen und auch beantworten - eine Oberfl�che, ein Konto, volle Kontrolle und all ihre E-Mail Adressen auf einen Blick.' . "\n\n"
													. 'Hilfe zum Importieren Ihrer E-Mail Konten von externen Anbietern, sowie eine Liste mit h�ufig gestellten Fragen und Antworten erhalten Sie jederzeit durch Klick auf das Fragezeichen in der oberen rechten Ecke. Sollten Sie hier einmal nicht die passende Antwort finden, k�nnen Sie entweder �ber die Hilfedatenbank eine neue Anfrage stellen oder uns �ber support@aikq.de direkt und pers�nlich erreichen.' . "\n\n"
                                                    . 'Wir w�nschen Ihnen viel Spa� bei aikQ und ab sofort mehr Zeit f�r die wichtigen Dinge des Lebens!' . "\n\n"
													. 'Viele Gr��e,' . "\n\n"
													. 'Ihr aikQ-Team' . "\n\n\n"
													. '(Diese E-Mail wurde automatisch erstellt!)';
		}
		else
		{
			// englisch?
		}
		
		if(function_exists('CharsetDecode') && !in_array(strtolower($currentCharset), array('iso-8859-1', 'iso-8859-15')))
        {
            foreach($lang_custom as $key => $val)
                if(substr($key, 0, 3) == 'du_')
                    $lang_custom[$key] = CharsetDecode($val, 'iso-8859-15');
            foreach($lang_user as $key=>$val)
                if(substr($key, 0, 3) == 'du_')
                    $lang_user[$key] = CharsetDecode($val, 'iso-8859-15');
        }
	}
	
	function Install()
	{
		PutLog('Demo-Benutzer Plugin was successfull installed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		PutLog('Demo-Benutzer Plugin was successfull removed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function createDemoUser($grpID)
	{
		global $db;
		
		// demo-user erstellen
		$profileData = array();
		$userID = BMUser::CreateAccount(DUMAIL,
				'demo-vorname',
				'demo-nachname',
				'demo-strasse',
				'111',
				'12345',
				'demo-ort',
				'25',
				'',
				'',
				'',
				'',
				DUPW,
				$profileData,
				false,
				'',
				'herr');
				
		// update misc stuff
		$db->Query('UPDATE {pre}users SET mail2sms_nummer=?, gruppe=?, gesperrt=?, sms_kontigent=?, notes=? WHERE id=?',
			'',
			$grpID,
			'no',
			0,
			'DEMO-BENUTZER',
			$userID);
		PutLog("Test-Group create demo-user (#" . $userID . ") successfully.", PRIO_NOTE, __FILE__, __LINE__);
	}

/**
 * delete an user and associated data
 *
 * @param int $userID
 */
function DeleteUser($userID, $qAddAND = '')
{
	global $db;

	if($userID <= 0)
		return(false);

	// get mail address
	$res = $db->Query('SELECT email FROM {pre}users WHERE id=?' . $qAddAND,
		$userID);
	if($res->RowCount() == 0)
		return(false);
	list($userMail) = $res->FetchArray(MYSQLI_NUM);
	$res->Free();

	// module handler
	ModuleFunction('OnDeleteUser', array($userID));

	// delete blobs
	$blobStorageIDs = array();
	$res = $db->Query('SELECT DISTINCT(`blobstorage`) FROM {pre}mails WHERE userid=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$blobStorageIDs[] = $row['blobstorage'];
	$res->Free();
	$res = $db->Query('SELECT DISTINCT(`blobstorage`) FROM {pre}diskfiles WHERE `user`=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$blobStorageIDs[] = $row['blobstorage'];
	$res->Free();
	foreach(array_unique($blobStorageIDs) as $blobStorageID)
		BMBlobStorage::createProvider($blobStorageID, $userID)->deleteUser();

	// delivery status entries
	$db->Query('DELETE FROM {pre}maildeliverystatus WHERE userid=?',
		$userID);

	// abuse points
	$db->Query('DELETE FROM {pre}abuse_points WHERE userid=?',
		$userID);

	// delete group<->member associations + groups
	$groupIDs = array();
	$res = $db->Query('SELECT id FROM {pre}adressen_gruppen WHERE user=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$groupIDs[] = $row['id'];
	$res->Free();
	if(count($groupIDs) > 0)
	{
		$db->Query('DELETE FROM {pre}adressen_gruppen_member WHERE gruppe IN(' . implode(',', $groupIDs) . ')');
		$db->Query('DELETE FROM {pre}adressen_gruppen WHERE user=?',
			$userID);
	}

	// delete addresses
	$db->Query('DELETE FROM {pre}adressen WHERE user=?',
		$userID);

	// delete aliases
	$db->Query('DELETE FROM {pre}aliase WHERE user=?',
		$userID);

	// delete autoresponder
	$db->Query('DELETE FROM {pre}autoresponder WHERE userid=?',
		$userID);

	// delete calendar dates
	$dateIDs = array();
	$res = $db->Query('SELECT id FROM {pre}dates WHERE user=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$dateIDs[] = $row['id'];
	$res->Free();
	if(count($dateIDs) > 0)
	{
		$db->Query('DELETE FROM {pre}dates_attendees WHERE date IN(' . implode(',', $dateIDs) . ')');
		$db->Query('DELETE FROM {pre}dates WHERE user=?',
			$userID);
	}

	// delete calendar groups
	$db->Query('DELETE FROM {pre}dates_groups WHERE user=?',
		$userID);

	// delete disk props
	$db->Query('DELETE FROM {pre}diskprops WHERE user=?',
		$userID);

	// delete disk locks
	$db->Query('DELETE FROM {pre}disklocks WHERE user=?',
		$userID);

	// delete disk folders
	$db->Query('DELETE FROM {pre}diskfolders WHERE user=?',
		$userID);

	// delete disk files
	$db->Query('DELETE FROM {pre}diskfiles WHERE user=?',
		$userID);

	// delete cert mails
	$db->Query('DELETE FROM {pre}certmails WHERE user=?',
		$userID);

	// delete filters
	$filterIDs = array();
	$res = $db->Query('SELECT id FROM {pre}filter WHERE userid=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$filterIDs[] = $row['id'];
	$res->Free();
	if(count($filterIDs) > 0)
	{
		$db->Query('DELETE FROM {pre}filter_actions WHERE filter IN(' . implode(',', $filterIDs) . ')');
		$db->Query('DELETE FROM {pre}filter_conditions WHERE filter IN(' . implode(',', $filterIDs) . ')');
		$db->Query('DELETE FROM {pre}filter WHERE userid=?',
			$userID);
	}

	// delete folder conditions + folders
	$folderIDs = array();
	$res = $db->Query('SELECT id FROM {pre}folders WHERE userid=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$folderIDs[] = $row['id'];
	$res->Free();
	if(count($folderIDs) > 0)
	{
		$db->Query('DELETE FROM {pre}folder_conditions WHERE folder IN(' . implode(',', $folderIDs) . ')');
		$db->Query('DELETE FROM {pre}folders WHERE userid=?',
			$userID);
	}

	// delete mails
	$db->Query('DELETE FROM {pre}mailnotes WHERE `mailid` IN (SELECT `id` FROM {pre}mails WHERE `userid`=?)',
		$userID);
	$db->Query('DELETE FROM {pre}mails WHERE userid=?',
		$userID);
	$db->Query('DELETE FROM {pre}attachments WHERE userid=?',
		$userID);

	// delete notes
	$db->Query('DELETE FROM {pre}notes WHERE user=?',
		$userID);

	// uid index + ext. pop3s
	$pop3IDs = array();
	$res = $db->Query('SELECT id FROM {pre}pop3 WHERE user=?',
		$userID);
	while($row = $res->FetchArray(MYSQLI_ASSOC))
		$pop3IDs[] = $row['id'];
	$res->Free();
	if(count($pop3IDs) > 0)
	{
		$db->Query('DELETE FROM {pre}uidindex WHERE pop3 IN(' . implode(',', $pop3IDs) . ')');
		$db->Query('DELETE FROM {pre}pop3 WHERE user=?',
			$userID);
	}

	// sigs
	$db->Query('DELETE FROM {pre}signaturen WHERE user=?',
		$userID);

	// sent sms
	$db->Query('DELETE FROM {pre}smsend WHERE user=?',
		$userID);

	// spam index
	$db->Query('DELETE FROM {pre}spamindex WHERE userid=?',
		$userID);

	// tasks
	$db->Query('DELETE FROM {pre}tasks WHERE user=?',
		$userID);

	// workgroup memberships
	$db->Query('DELETE FROM {pre}workgroups_member WHERE user=?',
		$userID);

	// certificates
	$db->Query('DELETE FROM {pre}certificates WHERE userid=?',
		$userID);

	// user prefs
	$db->Query('DELETE FROM {pre}userprefs WHERE userid=?',
		$userID);

	// search index
	$indexFileName = DataFilename($userID, 'idx', true);
	if(file_exists($indexFileName))
		@unlink($indexFileName);

	// finally, the user record itself
	$db->Query('DELETE FROM {pre}users WHERE id=?',
		$userID);

	// log
	PutLog(sprintf('User <%s> (%d) deleted',
		$userMail,
		$userID),
		PRIO_NOTE,
		__FILE__,
		__LINE__);

	return(true);
}

	function OnCron()
	{
		global $db;
		
		$grpID = "";
		// gruppen-id holen
		$res = $db->Query("SELECT id FROM {pre}gruppen WHERE titel=?", GROUPNAME);
		$row = $res->FetchArray();
		$grpID = $row['id'];
		$res->Free();

		if($grpID)
		{
			$uID = "";
			// user-id holen, �lter als 6 stunden nach reg-date
			$res = $db->Query("SELECT id FROM {pre}users WHERE gruppe=? AND reg_date<=?", $grpID, time()-21600);
			$row = $res->FetchArray();
			$uID = $row['id'];
			$res->Free();
					
			if($uID)
			{
				// demo-user l�schen
				$this->DeleteUser((int)$uID);
				PutLog("Test-Group auto-delete user (#" . $uID . ") successfully.", PRIO_NOTE, __FILE__, __LINE__);
				
				// demo-user erstellen
				$this->createDemoUser($grpID);
				// welcome-email senden
				SystemMail("Kundenservice <info@aikq.de", DUMAIL, $lang_custom['du_betreff'], 'du_StatusTXT', $vars, $uID);
			}
		}
	}
	
	function FileHandler($file, $action)
	{
		global $db, $thisUser;
		
		$grpID = "";
		// gruppen-id holen
		$res = $db->Query("SELECT id FROM {pre}gruppen WHERE titel=?", GROUPNAME);
		$row = $res->FetchArray();
		$grpID = $row['id'];
		$res->Free();

		if($grpID)
		{
			$uID = "";
			// user-id holen
			$res = $db->Query("SELECT id FROM {pre}users WHERE gruppe=?", $grpID);
			$row = $res->FetchArray();
			$uID = $row['id'];
			$res->Free();
					
			if($uID == $thisUser->_id)
			{
				if($file == 'prefs.php')
				{
					if($action == 'membership' 
					OR $action == 'orders'
					OR $action == 'pacc_mod')
					{
						exit(DUEMSG);
					}
				}
				if($file == 'start.php')
				{
					if($action == 'support')
					{
						exit(DUEMSG);
					}
				}		
				if($file == 'email.php')
				{
					if(isset($_REQUEST['do']) 
					&& ($_REQUEST['do'] == 'emptyFolder' 
					OR $_REQUEST['do'] == 'deleteMail'))
					{
						exit(DUEMSG);
					}
				}
			}
		}
	}
}

$plugins->registerPlugin('demouser');

?>
