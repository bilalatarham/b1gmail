{* <div class="innerWidget notePreview" id="notePreview">
	{lng p="clicknote"}
</div>
<div class="innerWidget" style="max-height: 79px; overflow-y: auto; border-top: 1px solid #DDDDDD;">
{foreach from=$bmwidget_notes_items key=noteID item=note}
	<a href="javascript:previewNote('{$sid}', '{$noteID}');">
	<img width="16" height="16" src="{$tpldir}images/li/ico_notes.png" border="0" alt="" align="absmiddle" />
	{text value=$note.text cut=30}</a><br />
{/foreach}
</div> *}

<div class="dragable-box content-box">
	<div class="box-title">
		<span class="tl-icon">
			<img src="{$tpldir}frontend_assets/img/menu-ic-notes.svg" class="img-fluid" />
		</span>
		<h3 class="tl-title fw-med">Notes</h3>
	</div>
	<div class="widget-notes">
		<div class="preview-area">
			<div class="help-info text-center">
				<span class="note-count fw-med">{$bmwidget_notes_items|@count}</span>
				<p class="fw-med mb-0" id="notePreview">{lng p="clicknote"}</p>
			</div>
		</div>
		<div class="notes-items mt-4">
			{foreach from=$bmwidget_notes_items key=noteID item=note}
				<a href="javascript:previewNote('{$sid}', '{$noteID}');">
					<span class="nt-link-icon">
						<img src="{$tpldir}frontend_assets/img/ic-pin.svg" class="img-fluid icon-default">
						<img src="{$tpldir}frontend_assets/img/ic-pin-hover.svg" class="img-fluid icon-hover">
					</span>
					<span class="note-text">{text value=$note.text cut=30}</span>
				</a>
			{/foreach}
		</div>
	</div>
</div>