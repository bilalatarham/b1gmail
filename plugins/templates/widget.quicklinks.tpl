<div class="innerWidget">
	{* <fieldset>
		<legend>{lng p="email"}</legend>
		<a href="email.php?sid={$sid}"><img src="{$tpldir}images/li/menu_ico_inbox.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="inbox"}</a><br />
		<a href="email.compose.php?sid={$sid}"><img src="{$tpldir}images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="sendmail"}</a><br />
		<a href="email.folders.php?sid={$sid}"><img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="folderadmin"}</a><br />
	</fieldset>
	<fieldset>
		<legend>{lng p="organizer"}</legend>
		<a href="organizer.php?sid={$sid}"><img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="overview"}</a><br />
		<a href="organizer.calendar.php?sid={$sid}"><img src="{$tpldir}images/li/ico_calendar.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="calendar"}</a><br />
		<a href="organizer.todo.php?sid={$sid}"><img src="{$tpldir}images/li/ico_todo.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="tasks"}</a><br />
		<a href="organizer.addressbook.php?sid={$sid}"><img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="addressbook"}</a><br />
		<a href="organizer.notes.php?sid={$sid}"><img src="{$tpldir}images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="notes"}</a><br />
	</fieldset>
	{if $pageTabs.webdisk}<fieldset>
		<legend>{lng p="webdisk"}</legend>
		<a href="webdisk.php?sid={$sid}"><img src="{$tpldir}images/li/ico_share.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="webdisk"}</a><br />
		<a href="webdisk.php?sid={$sid}&do=uploadFilesForm"><img src="{$tpldir}images/li/webdisk_file.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="uploadfiles"}</a><br />
	</fieldset>{/if}
	<fieldset>
		<legend>{lng p="misc"}</legend>
		<a href="prefs.php?sid={$sid}"><img src="{$tpldir}images/li/ico_common.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="prefs"}</a><br />
		<a href="start.php?sid={$sid}&action=logout"><img src="{$tpldir}images/li/ico_logout.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="logout"}</a><br />
	</fieldset> *}
	
	<div class="dragable-box content-box">
		<div class="box-title">
			<span class="tl-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-link.svg" class="img-fluid" />
			</span>
			<h3 class="tl-title fw-med">Quick Links</h3>
		</div>
		<div class="widget-quicklinks">
			<div class="ql-item">
				<h4 class="ql-title fw-med">{lng p="email"}</h4>
				<div class="ql-item-links">
					<a href="email.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-arrow-down-b.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="inbox"}</span>
					</a>
					<a href="email.compose.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-arrow-up-b.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="sendmail"}</span>
					</a>
					<a href="email.folders.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-folder.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="folderadmin"}</span>
					</a>
				</div>
			</div>
			<div class="ql-item">
				<h4 class="ql-title fw-med">{lng p="organizer"}</h4>
				<div class="ql-item-links">
					<a href="organizer.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-overview.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="overview"}</span>
					</a>
					<a href="organizer.calendar.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="calendar"}</span>
					</a>
					<a href="organizer.todo.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-clipboard.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="tasks"}</span>
					</a>
					<a href="organizer.addressbook.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-addressbook.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="addressbook"}</span>
					</a>
					<a href="organizer.notes.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/menu-ic-notes.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="notes"}</span>
					</a>
				</div>
			</div>
			<div class="ql-item">
				<h4 class="ql-title fw-med">{lng p="webdisk"}</h4>
				<div class="ql-item-links">
					<a href="webdisk.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/menu-ic-webdisc.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="webdisk"}</span>
					</a>
					<a href="webdisk.php?sid={$sid}&do=uploadFilesForm">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-upload.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="uploadfiles"}</span>
					</a>
				</div>
			</div>
			<div class="ql-item">
				<h4 class="ql-title fw-med">{lng p="misc"}</h4>
				<div class="ql-item-links">
					<a href="prefs.php?sid={$sid}">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-setting-d.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="prefs"}</span>
					</a>
					<a href="start.php?sid={$sid}&action=logout">
						<span class="ql-icon">
							<img src="{$tpldir}frontend_assets/img/ic-logout.svg" class="img-fluid" />
						</span>
						<span class="ql-text">{lng p="logout"}</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>