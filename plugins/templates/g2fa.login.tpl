<div class="jumbotron splash" style="background-image: url({$tpldir}images/nli/{$templatePrefs.splashImage});">
	<div class="container">
		<div class="panel panel-primary login">
			<div class="panel-heading">
				{lng p="g2fa"}
			</div>
			<div class="panel-body">
				<form action="index.php?sid={$sid}" method="post">
					{if isset($g2fa_error)}
                        <div class="alert alert-danger" role="alert">
                            {$g2fa_error}
                        </div>
                    {/if}
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input type="text" name="g2fa_code" class="form-control" placeholder="{lng p="g2fa_code"}" required="true">
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">{lng p="login"}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
