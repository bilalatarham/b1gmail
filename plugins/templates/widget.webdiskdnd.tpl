<div class="innerWidget" style="text-align:center;">
    <div class="dragable-box content-box">
		<div class="box-title handle">
			<span class="tl-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-drag.svg" class="img-fluid" />
			</span>
			<h3 class="tl-title fw-med">Webdisc Drag & Drop</h3>
		</div>
		<div class="widget-drag">
			<div class="drag-area text-center d-flex" id="dropzone">
				<span class="icon-drag">
					<img src="{$tpldir}frontend_assets/img/ic-download.svg" class="img-flui" />
				</span>
				<p class="mb-0"><span class="fw-bold">Drop files here</span></p>
			</div>
		</div>
	</div>

	{* <div id="wdDnDArea">
		<img src="./plugins/templates/images/widget_dnd_inactive.gif" border="0" alt="" />
	</div> *}
	
	{* <script src="./clientlib/dndupload.js" language="javascript" type="text/javascript"></script>
	
	<script language="javascript">
		initDnDUpload(EBID('wdDnDArea'), 'webdisk.php?sid='+currentSID+'&folder=0&action=dndUpload', function() {literal}{{/literal} document.location.href='webdisk.php?sid='+currentSID+'&folder=0'; {literal}}{/literal});
	</script> *}
</div>
