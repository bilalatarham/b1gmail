
<h1><img src="./plugins/templates/images/modbriefkarte_bk_send.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="bk_brief"}</h1>

<form name="f1" method="post" action="start.php?action=briefkarte&bktyp=brief&do=BriefSenden&sid={$sid}" enctype="multipart/form-data">
<input type="hidden" name="b_land" id="b_land" value="{text value=$b_land allowEmpty=false}" />
<input type="hidden" name="b_einschreiben" id="b_einschreiben" value="{text value=$b_einschreiben allowEmpty=false}" />
<input type="hidden" name="b_duplex" id="b_duplex" value="{text value=$b_duplex allowEmpty=false}" />
<input type="hidden" name="b_farbe" id="b_farbe" value="{text value=$b_farbe allowEmpty=false}" />
<input type="hidden" name="bkey" id="bkey" value="{text value=$bkey allowEmpty=false}" />
<input type="hidden" name="bid" id="bid" value="{text value=$bid allowEmpty=false}" />
<table class="listTable">
		<tr>
			<th colspan="2" class="listTableHead">{lng p="bk_brief"}</th>
		</tr>
        <tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding-bottom:1em;">{lng p="b_previewtext"}</td>
		</tr>
        <tr>
			<td class="listTableLeft">{lng p="preview"}:</td>
			<td class="listTableRight">
				<img src="./plugins/templates/images/modbriefkarte_pdf.gif" border="0" alt="" align="absmiddle" />
				<a href="{$pdflink}" target="_blank">{lng p="b_preview"}</a>
			</td>
		</tr>
        <tr>
			<td class="listTableLeft">{lng p="b_pages"}:</td>
			<td class="listTableRight">
				<span>{if $b_seiten > 0}{$b_seiten}{else}0{/if}</span>
			</td>
		</tr>
        <tr>
			<td class="listTableLeft">{lng p="price"}:</td>
			<td class="listTableRight">
				<span>{$briefkosten}</span>
				&nbsp; <small><i>({lng p="accbalance"}: {$accBalance})</i></small>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding: 5px 5px 5px 5px;">
				<input type="button" value="{lng p="b_karte"}" id="sendButton" onclick="document.forms.f1.submit();" {if $accCredits==false} disabled="disabled"{/if}/>
				<input type="button" value="{lng p="reset"}" onclick="window.location.href = 'start.php?action=briefkarte&bktyp=brief&do=BriefRemove&bkey={$bkey}&bid={$bid}&sid={$sid}';"/>
				{if $accCredits==false}<div class="note" id="errorMsg">{lng p="b_pricewarning"}</div>{/if}
			</td>
		</tr>
		<tr>
			<td colspan="2" class="listTableRightDesc"><div><small><b>Hinweis:</b> {lng p="bk_prevhinw"}</small></div></td>
		</tr>
	</table>
</form>
