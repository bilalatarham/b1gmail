<fieldset>
    <legend>{lng p="msLogs"}</legend>

    <table class="list">
        <tr>
            <th style="width: 20px;">&nbsp;</th>
            <th style="width: 150px;">{lng p="msDatum"}</th>
            {if $JogLog}
                <th style="width: 200px;">{lng p="msUser"}</th>
                <th style="width: 70px;">{lng p="msJobID"}</th>
            {/if}
            <th>{lng p="msLogs"}</th>
        </tr>

        {foreach from=$LogList item=lo}
            {cycle name=class values="td1,td2" assign=class}
            <tr class="{$class}">
                <td>{$lo.Status}</td>
                <td>{text value=$lo.LogDate cut=100}</td>
                {if $JogLog}
                    <td><a href="users.php?do=edit&id={$lo.UserID}&sid={$sid}">{text value=$lo.EMail}</a></td>
                    <td>{$lo.JobID}</td>
                {/if}
                <td>{text value=$lo.Nachricht}</td>
            </tr>
        {/foreach}
    </table>
</fieldset>