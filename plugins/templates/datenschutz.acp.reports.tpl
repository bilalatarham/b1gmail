<a href="{$pageURL}&do=reports&show={$ShowContent}&sid={$sid}" style="float: right;">{$ShowContentText}</a>
<table class="list">
    <tr>
        <th width="20">&nbsp;</th>
        <th>{lng p="dsReportsUser"}</th>
        <th>{lng p="dsReportsDate"}</th>
        <th>{lng p="dsReportsUpdate"}</th>
        <th>{lng p="dsReportsAktion"}</th>
        <th width="30">&nbsp;</th>
    </tr>

    {foreach from=$Content item=cont}
        {cycle name=class values="td1,td2" assign=class}
        <tr class="{$class}">
            <td align="center"><img src="../plugins/templates/images/{text value=$cont.Status}" width="16" height="16" title="{text value=$cont.StatusText}" /></td>
            <td><a href="users.php?do=edit&id={$cont.UserID}&sid={$sid}">{text value=$cont.UserInfo}</a></td>
            <td>{text value=$cont.JobDate}</td>
            <td>{text value=$cont.JobUpdate}</td>
            <td>
                {if $cont.StatusID == 1}<a href='{$pageURL}&do=reports&do2=accept&id={$cont.ID}&userid={$cont.UserID}&sid={$sid}'">Report Akzeptieren</a>{/if}
                {if $cont.StatusID == 0 && $cont.Aktion}<a href='{$pageURL}&do=reports&do2=download&id={$cont.ID}&sid={$sid}'">Download Report ({$cont.DokumentSize})</a>{/if}
            </td>
            <td>
                <a href="{$pageURL}&do=reports&delete={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="../plugins/templates/images/datenschutz_delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
            </td>
        </tr>
    {/foreach}
</table>