<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
	Die Farben haben folgende Reihenfolge: BG obere Leiste, Schrift obere Leiste, Hintergrund linkes Menü, Schrift linkes Menü, Hintergrund Hauptinhalt, Widgets (nicht in Liste enthalten), Widgets Typ1, Widgets Typ2, Widgets Typ3
	<table id="usertour_admin" class="list">
		<tr>
			<th>Farben(mit Komma getrennt, HEX)</th>
			<th>Beschreibung</th>
			<th></th>
		</tr>
			{foreach from=$pfcolors item=name key=key}
			<tr>
			<input type="hidden" name="id[]" value="{$pfids[$key]}" />
				<td><input type="text" class="name" name="colors[]" value="{$pfcolors[$key]}" style="width: 80%" /></td>
				<td><input type="text" class="desc" name="desc[]" value="{$pfdesc[$key]}" /></td>
				
				<td><a href="{$pageURL}&sid={$sid}&do=delete&item={$pfids[$key]}">Löschen!</a></td>
			</tr>
			{/foreach}
			
	</table>
	<a onclick="addLine()" href="#" style="padding-right: 10px;">Zeile Hinzufügen</a>
	<input class="button" type="submit" value=" {lng p="save"} " />
</form>
{literal}
<script type="text/javascript">
function addLine() {
	var div = document.getElementById('usertour_admin');
	div.innerHTML = div.innerHTML + '<tr><input type="hidden" name="id[]" value="" /><td><input type="text" class="name" name="colors[]" value="" style="width: 80%" /></td><td><input type="text" class="desc" name="desc[]" value="" /></td><td></td></tr>';
	return(false);
}
</script>
{/literal}