<div id="contentHeader">
	<div class="left">
        <img src="plugins/templates/images/g2fa_ico.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="g2fa"}
	</div>
</div>

<div class="scrollContainer">
    <div class="pad">
        <div class="note">
            <h2>
                <img src="./templates/modern/images/li/ico_software.png" alt="" width="16" height="16" border="0" align="absmiddle">
                {lng p="g2fa_install_app"}
            </h2>
            <p>{lng p="g2fa_install_app_description"}</p>
        </div>

        {if isset($g2fa_secret)}
            <div class="note">
                <h2>
                    <img src="./templates/modern/images/li/ico_common.png" alt="" width="16" height="16" border="0" align="absmiddle">
                    {lng p="g2fa_configure_app"}
                </h2>
                <p>{lng p="g2fa_configure_app_description"}</p>
                <p>
                    <img src="{$g2fa_qrcode_url}"><br>
                    <b>{lng p="g2fa_secret_key"}:</b> {$g2fa_secret}
                </p>
            </div>
        {/if}

        <table class="listTable">
            <thead>
                <tr>
                    <th class="listTableHead" colspan="2">
                        {lng p="g2fa_settings"}
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="listTableLeft">{lng p="g2fa_state"}</td>
                    <td class="listTableRight">
                        {if $g2fa_enabled}
                            <div>{lng p="g2fa_state_enabled"}</div>
                            <button type="button" onclick="document.location.href='prefs.php?action=g2fa&do=disable&sid={$sid}';">
                                <i class="fa fa-remove"></i>
                                {lng p="g2fa_disable"}
                            </button>
                        {else}
                            <div>{lng p="g2fa_state_disabled"}</div>
                            <button type="button" class="primary" onclick="document.location.href='prefs.php?action=g2fa&do=create_secret&sid={$sid}';">
                    			<i class="fa fa-check"></i>
                    			{lng p="g2fa_enable"}
                    		</button>
                        {/if}
                    </td>
                </tr>
                {if $g2fa_enabled}
                    <tr>
                        <td class="listTableLeft">{lng p="g2fa_secret_key"}</td>
                        <td class="listTableRight">
                            <div>{lng p="g2fa_new_secret_description"}</div>
                            <button type="button" class="primary" onclick="document.location.href='prefs.php?action=g2fa&do=create_secret&sid={$sid}';">
                                <i class="fa fa-refresh"></i>
                                {lng p="g2fa_new_secret"}
                            </button>
                        </td>
                    </tr>
                {/if}
            </tbody>
        </table>
    </div>
</div>
