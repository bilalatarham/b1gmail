{if $MSG}
    {$MSG}
{else}
    <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #333; background-color: #f5f5f5; border-color: #ddd;">
        {lng p="dsCookieInfo"}
    </div>
{/if}

<form action="{$pageURL}&do=cookie&sid={$sid}"method="post">
    <table style="width: 100%; margin: 0px; padding: 0px;">
        <tr style="vertical-align: top;">
            <td style="width: 50%;">
                <fieldset>
                    <legend>{lng p="dsCookieAllgemein"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieAktivieren"}:</td>
                            <td class="td2">
                                <input id="cookienliaktivieren" name="cookienliaktivieren" type="checkbox" {if $CookieNLIAktivieren == 'yes'}checked="checked"{/if} />
                                <label for="cookienliaktivieren">{lng p="dsCookieNLIAktivierenBesch"}</label><br />
                                <input id="cookieliaktivieren" name="cookieliaktivieren" type="checkbox" {if $CookieLIAktivieren == 'yes'}checked="checked"{/if} />
                                <label for="cookieliaktivieren">{lng p="dsCookieLIAktivierenBesch"}</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">{lng p="dsCookieDenyAktivieren"}:</td>
                            <td class="td2">
                                <input id="javascriptdenycookie" name="javascriptdenycookie" type="checkbox" {if $CookieJavaScriptDenyCookie == 'yes'}checked="checked"{/if} />
                                <label for="javascriptdenycookie">{lng p="dsCookieDenyAktivierenBesch"}</label>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend>{lng p="dsCookieDesign"}</legend>
                    <script src="../plugins/js/datenschutz_jscolor.js"></script>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieDesignBG"}:</td>
                            <td class="td2">
                                <input type="text" id="designbg" name="designbg" class="jscolor" style="width:100%;" value="{$CookieDesignBG}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieDesignFG"}:</td>
                            <td class="td2">
                                <input type="text" id="designfg" name="designfg" class="jscolor" style="width:100%;" value="{$CookieDesignFG}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieDesignLinkBG"}:</td>
                            <td class="td2">
                                <input type="text" id="designlinkbg" name="designlinkbg" class="jscolor" style="width:100%;" value="{$CookieDesignLinkBG}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieDesignLinkFG"}:</td>
                            <td class="td2">
                                <input type="text" id="designlinkfg" name="designlinkfg" class="jscolor" style="width:100%;" value="{$CookieDesignLinkFG}" />
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend>{lng p="dsCookieKonfiguration"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieTheme"}:</td>
                            <td class="td2">
                                <select name="designtheme" style="width: 100%;">
                                    <option value="classic" {if $CookieDesignTheme == 'classic'}selected{/if}>Classic</option>
                                    <option value="edgeless" {if $CookieDesignTheme == 'edgeless'}selected{/if}>Edgeless</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">{lng p="dsCookiePosition"}:</td>
                            <td class="td2">
                                <select name="designposition" style="width: 100%;">
                                    <option value="" {if $CookieDesignPosition == ''}selected{/if}>Banner bottom</option>
                                    <option value="top" {if $CookieDesignPosition == 'top'}selected{/if}>Banner top</option>
                                    <option value="bottom-left" {if $CookieDesignPosition == 'bottom-left'}selected{/if}>Floating left</option>
                                    <option value="bottom-right" {if $CookieDesignPosition == 'bottom-right'}selected{/if}>Floating right</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">{lng p="dsCookieType"}:</td>
                            <td class="td2">
                                <select name="designtype" style="width: 100%;">
                                    <option value="1" {if $CookieDesignType == '1'}selected{/if}>{lng p="dsCookieTypeInfo"}</option>
                                    <option value="opt-out" {if $CookieDesignType == 'opt-out'}selected{/if}>{lng p="dsCookieTypeOptout"}</option>
                                    <option value="opt-in" {if $CookieDesignType == 'opt-in'}selected{/if}>{lng p="dsCookieTypeOptin"}</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td style="width: 50%;">
                <fieldset>
                    <legend>{lng p="dsCookieText"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieLinkText"}:</td>
                            <td class="td2">
                                {foreach from=$Languages key=langID item=lang}
                                    <div class="input-group prefix">
                                        <span class="input-group-addon"><img src="/plugins/templates/images/datenschutz_flag_{$langID}.png" width="14" height="14" alt="{$langID}" /></span>
                                        <input type="text" class="inputprefix" id="{$langID}_cookielinktext" name="{$langID}_cookielinktext" value="{$CookieLinkText.$langID}" />
                                    </div>
                                {/foreach}
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsCookieLink"}:</td>
                            <td class="td2">
                                <div class="input-group prefix">
                                    <span class="input-group-addon"><img src="/plugins/templates/images/datenschutz.png" width="14" height="14" alt="{$langID}" /></span>
                                    <input type="text" class="inputprefix" id="cookielink" name="cookielink" style="width:100%;" value="{$CookieLink}" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">{lng p="dsCookieText"}:</td>
                            <td class="td2">
                                <table width="100%">
                                {foreach from=$Languages key=langID item=lang}
                                    <tr style="vertical-align: top;">
                                        <td style="padding-top: 10px; text-align: center; width: 40px; background-color: #eee; border: 1px solid #ccc;"><img src="/plugins/templates/images/datenschutz_flag_{$langID}.png" width="14" height="14" alt="{$langID}" /></td>
                                        <td style="padding-lef: 0px;"><textarea id="{$langID}_cookietext" name="{$langID}_cookietext" style="width: 100%; height: 83px;">{$CookieText.$langID}</textarea></td>
                                    </tr>
                                {/foreach}
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">{lng p="dsCookieAllow"}:</td>
                            <td class="td2">
                                {foreach from=$Languages key=langID item=lang}
                                    <div class="input-group prefix">
                                        <span class="input-group-addon"><img src="/plugins/templates/images/datenschutz_flag_{$langID}.png" width="14" height="14" alt="{$langID}" /></span>
                                        <input type="text" class="inputprefix" id="{$langID}_cookieallow" name="{$langID}_cookieallow" value="{$CookieAllow.$langID}" />
                                    </div>
                                {/foreach}
                            </td>
                        </tr>
                        <tr>
                            <td class="td1">{lng p="dsCookieClose"}:</td>
                            <td class="td2">
                                {foreach from=$Languages key=langID item=lang}
                                    <div class="input-group prefix">
                                        <span class="input-group-addon"><img src="/plugins/templates/images/datenschutz_flag_{$langID}.png" width="14" height="14" alt="{$langID}" /></span>
                                        <input type="text" class="inputprefix" id="{$langID}_cookieclose" name="{$langID}_cookieclose" value="{$CookieClose.$langID}" />
                                    </div>
                                {/foreach}
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>

    <fieldset>
        <legend>{lng p="dsCookieSystem"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="dsCookieSystemInfo"}:<br /></td>
                <td class="td2" colspan="2"><br /><textarea id="systemcookies" name="systemcookies" style="width: 100%; height: 150px;">{$CookieSystem}</textarea><small style="font-weight: normal;">{lng p="dsCookieSystemBesch"}</small></td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>{lng p="dsCookieKonfigurationJS"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="dsCookieKonfigurationJSAllow"}:<br /><small style="font-weight: normal;">{lng p="dsCookieKonfigurationJSAllowBesch"}</small></td>
                <td class="td2" colspan="2">&lt;script type="text/javascript"&gt;<br /><textarea id="javascriptallow" name="javascriptallow" style="width: 100%; height: 150px;">{$CookieJavaScriptAllow}</textarea>&lt;/script&gt;</td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsCookieKonfigurationJSDeny"}:<br /><small style="font-weight: normal;">{lng p="dsCookieKonfigurationJSDenyBesch"}</small></td>
                <td class="td2" colspan="2">&lt;script type="text/javascript"&gt;<br /><textarea id="javascriptdeny" name="javascriptdeny" style="width: 100%; height: 150px;">{$CookieJavaScriptDeny}</textarea>&lt;/script&gt;</td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsCookieKonfigurationJSOpt"}:<br /><small style="font-weight: normal;">{lng p="dsCookieKonfigurationJSOptBesch"}</small></td>
                <td class="td2">{lng p="dsCookieKonfigurationJSAllow"}<br /><textarea style="width: 100%; height: 120px;" disabled>&#123;if $CookieStatus&#125;
&lt;script type="text/javascript"&gt;
   &#123;literal&#125;
      ...
   &#123;/literal&#125;
&lt;/script&gt;
&#123;/if&#125;</textarea></td>
                <td class="td2">{lng p="dsCookieKonfigurationJSDeny"}<br /><textarea style="width: 100%; height: 120px;" disabled>&#123;if !$CookieStatus&#125;
&lt;script type="text/javascript"&gt;
   &#123;literal&#125;
      ...
   &#123;/literal&#125;
&lt;/script&gt;
&#123;/if&#125;</textarea></td>
            </tr>
        </table>
    </fieldset>

    <p align="right" style="padding-right: 3px;">
        <input class="button" type="submit" name="cookiesave" value=" {lng p="dsCookieSpeichern"} " />
    </p>
</form>