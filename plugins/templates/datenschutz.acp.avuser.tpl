<a href="{$pageURL}&do=avuser&show={$ShowContent}&sid={$sid}" style="float: right;">{$ShowContentText}</a>
<table class="list">
    <tr>
        <th>{lng p="dsReportsUser"}</th>
        <th>{lng p="dsReportsDate"}</th>
        <th>{lng p="dsReportsAktion"}</th>
        <th width="30">&nbsp;</th>
    </tr>

    {foreach from=$Content item=cont}
        {cycle name=class values="td1,td2" assign=class}
        <tr class="{$class}">
            <td><a href="users.php?do=edit&id={$cont.UserID}&sid={$sid}">{text value=$cont.UserInfo}</a></td>
            <td>{text value=$cont.DocDate}</td>
            <td><a href='{$pageURL}&do=avuser&do2=download&id={$cont.ID}&sid={$sid}'">Download AV-Report ({$cont.DokumentSize})</a>
            </td>
            <td>
                <a href="{$pageURL}&do=avuser&delete={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="../plugins/templates/images/datenschutz_delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
            </td>
        </tr>
    {/foreach}
</table>