<div id="block-button-overlay" style="display: none">
	<div id="block-button-container" style="display: none">
		<div class="block-button-step visible">
			<div id="block-button-fullmail" class="block-button-action left">
				<div class="block-button-description">E-Mail-Adresse blocken</div>
				<i class="fa fa-user fa-5x"></i>
				<div class="block-button-data"></div>
			</div>
			
			<div id="block-button-ending" class="block-button-action right">
				<div class="block-button-description">Endung blocken</div>
				<i class="fa fa-globe fa-5x"></i>
				<div class="block-button-data"></div>
			</div>
		</div>
		<div class="block-button-step hidden center">
			<i class="fa fa-cog fa-spin fa-5x"></i>
		</div>
		<div class="block-button-step hidden center">
			<div id="block-button-status" class="center"></div>			
		</div>
		<button id="block-button-close" onclick="blockbutton.closeOverlay()">{lng p="close"}</button>
	</div>
</div>