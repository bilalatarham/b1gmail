<fieldset>
	<legend>L&ouml;schzeit</legend>
    <table class="list">
    	<tr><th>Gruppe</th><th>Sperren nach</th><th></th></tr> 
    	
        {foreach from=$poll_umfragen item=umfrage}
  		<tr class="td1"><td>{$umfrage.id}</td><td><a href="{$pageURL}{$link}&action=page2&pollid={$umfrage.id}&sid={$sid}">{$umfrage.titel}</a></td><td><a href="{$pageURL}{$link}&action=page1&pollid={$umfrage.id}&sid={$sid}&do=del" onclick="return confirm('Wirklich löschen?')"><img height="16" border="0" width="16" alt="L&ouml;schen" src="../plugins/templates/images/poll_delete.png" /></a></td></tr>
  		{/foreach}
    </table>
</fieldset>
<fieldset>
	<legend>Neu</legend>
    <a href="{$pageURL}{$link}&action=page2&pollid={$umfrage.id}&do=new&sid={$sid}">Neue Umfrage hinzuf&uuml;gen</a>
</fieldset>
<fieldset>
	<legend>Widgetname</legend>
    <form action="{$pageURL}{$link}&action=page1&pollid={$umfrage.id}&do=titel&sid={$sid}" method="post">
    <p>Widgetname: <input type="text" name="titel" value="{$poll_titel}" /><input type="submit" value="Speichern" /></p>
    </form>    
</fieldset>