<form action="{$pageURL}&do=general&sid={$sid}" method="post" enctype="multipart/form-data">
    {$MSG}

    <fieldset>
        <legend>{lng p="dsAllgemein"}</legend>
        <table width="100%">
            {if $Logo}
                <tr>
                    <td class="td1">{lng p="dsLogoBild"}:</td>
                    <td class="td2">
                        <img src="../plugins/templates/images/{$Logo}" style="max-width: 200px;" alt="Logo" />
                        <a onclick="return confirm('{lng p="dsLogoEntfernen"}');" href="{$pageURL}&do=conf&do2=dellogo&sid={$sid}"><img src="../plugins/templates/images/datenschutz_delete.png" width="16" height="16" border="0" alt="L&ouml;schen" align="absmiddle" /></a>
                    </td>
                </tr>
            {/if}
            <tr>
                <td class="td1" width="300">{lng p="dsLogo"}:</td>
                <td class="td2">
                    <input type="file" id="logo" name="logo" style="width:100%;" value="" />
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>{lng p="dsGeneralBasic"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="300">{lng p="dsGeneralName"}</td>
                <td class="td2">
                    <input type="text" id="dsname" name="dsname" style="width:100%;" value="{$Name}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsGeneralOptional"}</td>
                <td class="td2">
                    <input type="text" id="dsoptional" name="dsoptional" style="width:100%;" value="{$Optional}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsGeneralStreet"}</td>
                <td class="td2">
                    <input type="text" id="dsstreet" name="dsstreet" style="width:79%;" value="{$Street}" />
                    <input type="text" id="dsstreetnr" name="dsstreetnr" style="width:20%;" value="{$StreetNr}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsGeneralPLZPlace"}</td>
                <td class="td2">
                    <input type="text" id="dsplz" name="dsplz" style="width:29%;" value="{$PLZ}" />
                    <input type="text" id="dsplace" name="dsplace" style="width:70%;" value="{$Place}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsGeneralCountry"}</td>
                <td class="td2">
                    <input type="text" id="dscountry" name="dscountry" style="width:100%;" value="{$Country}" />
                </td>
            </tr>

        </table>
    </fieldset>

    <fieldset>
        <legend>{lng p="dsContact"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="300">{lng p="dsContactMail"}:</td>
                <td class="td2">
                    <input type="email" id="dscontactmail" name="dscontactmail" style="width:100%;" value="{$ContactEMail}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsPrivacyMail"}:</td>
                <td class="td2">
                    <input type="email" id="dsprivacymail" name="dsprivacymail" style="width:100%;" value="{$PrivacyEMail}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsPhone"}:</td>
                <td class="td2">
                    <input type="text" id="dsphone" name="dsphone" style="width:100%;" value="{$Phone}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsFax"}:</td>
                <td class="td2">
                    <input type="text" id="dsfax" name="dsfax" style="width:100%;" value="{$Fax}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsContactForm"}:</td>
                <td class="td2">
                    <input type="text" id="dscontactform" name="dscontactform" style="width:100%;" value="{$ContactForm}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsMore"}:</td>
                <td class="td2">
                    <input type="text" id="dsmore" name="dsmore" style="width:100%;" value="{$MoreContactOptions}" />
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>{lng p="dsCompanySettings"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="300">{lng p="dsAuthorizedToRepresent"}</td>
                <td class="td2">
                    <input type="text" id="dsauthorized" name="dsauthorized" style="width:100%;" value="{$AuthorizedToRepresent}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsUstID"}</td>
                <td class="td2">
                    <input type="text" id="dsustid" name="dsustid" style="width:100%;" value="{$UstID}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsWidNr"}</td>
                <td class="td2">
                    <input type="text" id="dswidnr" name="dswidnr" style="width:100%;" value="{$WidNr}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsBusinessArea"}</td>
                <td class="td2">
                    <input type="text" id="dsbusinessarea" name="dsbusinessarea" style="width:100%;" value="{$BusinessArea}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsAGBUrl"}</td>
                <td class="td2">
                    <input type="text" id="dsagb" name="dsagb" style="width:100%;" value="{$AGBUrl}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsRegistryCourt"}</td>
                <td class="td2">
                    <input type="text" id="dsregistrycurt" name="dsregistrycurt" style="width:100%;" value="{$RegistryCourt}" />
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsRegistryCourtNr"}</td>
                <td class="td2">
                    <input type="text" id="dsregistrycurtnr" name="dsregistrycurtnr" style="width:100%;" value="{$RegistryCourtNr}" />
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>{lng p="dsImpressum"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="300">{lng p="dsConsumerDisputeResolution"}</td>
                <td class="td2">
                    <input id="dsconsumerdis" name="dsconsumerdis" type="checkbox" {if $ConsumerDisputeResolution == 'yes'}checked="checked"{/if} />
                    <label for="dsconsumerdis"><b>{lng p="dsConsumerDisputeResolutionDesc"}</b></label>
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsSocialMedia"}</td>
                <td class="td2">
                    <input id="dssocialmedia" name="dssocialmedia" type="checkbox" {if $SocialMedia == 'yes'}checked="checked"{/if} />
                    <label for="dssocialmedia"><b>{lng p="dsSocialMediaDesc"}</b></label>
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsCopyrightNotices"}</td>
                <td class="td2">
                    <input id="dscopyrightnot" name="dscopyrightnot" type="checkbox" {if $CopyrightNotices == 'yes'}checked="checked"{/if} />
                    <label for="dscopyrightnot"><b>{lng p="dsCopyrightNoticesDesc"}</b></label>
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsPhotoCredits"}</td>
                <td class="td2">
                    <input id="dsphotocredits" name="dsphotocredits" type="checkbox" {if $PhotoCredits == 'yes'}checked="checked"{/if} />
                    <label for="dsphotocredits"><b>{lng p="dsPhotoCreditsDesc"}</b></label>
                </td>
            </tr>
        </table>
    </fieldset>

    <p align="right" style="padding-right: 3px;">
        <input class="button" type="submit" name="gensave" value=" {lng p="dsSave"} " />
    </p>
</form>