<form action="{$page_url}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
    <fieldset>
    	<legend>{lng p="g2fa_qrcode"}</legend>
        <table width="100%">
            <tr>
				<td class="td1" width="200">{lng p="g2fa_ga_app_title"}:</td>
				<td class="td2"><input type="text" name="ga_app_title" value="{$g2fa_prefs.ga_app_title}"></td>
			</tr>
            <tr>
				<td class="td1" width="200">{lng p="g2fa_qrcode_width"}:</td>
				<td class="td2"><input type="text" name="qrcode_width" value="{$g2fa_prefs.qrcode_width}"> px</td>
			</tr>
            <tr>
				<td class="td1" width="200">{lng p="g2fa_qrcode_height"}:</td>
				<td class="td2"><input type="text" name="qrcode_height" value="{$g2fa_prefs.qrcode_height}"> px</td>
			</tr>
        </table>
    </fieldset>
    <div style="float: right;">
        <input class="button" type="submit" value="{lng p="save"} ">
    </div>
</form>
