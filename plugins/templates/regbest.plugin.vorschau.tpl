<fieldset>
	<legend>{lng p="vorschau"}</legend>
	
	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/modregbest32.png" border="0" alt="" width="32" height="32" /></td>
			<td>
				<blockquote>
					<div style="width:475px;">
						<div style="padding:0px 0px 5px 0px;"><b>{lng p="ueb1"}:</b></div>
						<div style="padding:5px; border:1pt solid blue; width:100%;">{$mailtext}</div>
					</div>
				</blockquote>
			</td>
		</tr>
		<tr>
			<td align="left">
				<input class="button" onClick="location.href='{$pageURL}&sid={$sid}&action=prefs'" type="button" name="{lng p="back"}" value="{lng p="back"}">
			</td>
		</tr>
	</table>
</fieldset>