<div class="smallmenu_settings">
	<h3>{lng p="proflat_smallmenu"}</h3>
	<div class="smallmenu_setting smallmenu_no{if !$proflat_smallmenu} selected{/if}">
		<div class="smallmenu_setting_container">
			<div class="smallmenu_setting_itemcontainer">
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
			</div>
		</div>
		<div class="smallmenu_setting_desc">{lng p="proflat_smallmenu_no"}</div>
	</div>
	
	<div class="smallmenu_setting smallmenu_yes{if $proflat_smallmenu} selected{/if}">
		<div class="smallmenu_setting_container">
			<div class="smallmenu_setting_itemcontainer">
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
				<div class="smallmenu_setting_menuitem"></div>
			</div>
		</div>
		<div class="smallmenu_setting_desc">{lng p="proflat_smallmenu_yes"}</div>
	</div>
	
	<input type="hidden" id="smallmenu" name="smallmenu" value="yes" />
</div>

<script type="text/javascript">
{literal}
$(document).ready(function(){
	$(document).on("click", ".smallmenu_setting", function(e) {
		var smallmenu = "no";
		$(".smallmenu_setting").removeClass("selected");
		$(this).addClass("selected");
		
		if($(this).hasClass("smallmenu_yes"))
			smallmenu = "yes";
			
		$("#smallmenu").val(smallmenu);
	});
});
{/literal}
</script>