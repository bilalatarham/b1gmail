<fieldset>
	<legend>{lng p="bk_karte"} - {lng p="bk_statistik"}</legend>
		<table width="100%">
			<tr>
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_stamp.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="bk_stats_day_ok"}:</td>
    			<td class="td2" width="15%">{$bk_day_ok}</td>
    			
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_stamp_error.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="bk_stats_day_error"}:</td>
    			<td class="td2" width="15%">{$bk_day_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="bk_stats_month_ok"}:</td>
    			<td class="td2" width="15%">{$bk_month_ok}</td>
    			
    			<td class="td1">{lng p="bk_stats_month_error"}:</td>
    			<td class="td2" width="15%">{$bk_month_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="bk_stats_all_ok"}:</td>
    			<td class="td2" width="15%">{$bk_all_ok}</td>
    			
    			<td class="td1">{lng p="bk_stats_all_error"}:</td>
    			<td class="td2" width="15%">{$bk_all_error}</td>
    		</tr>
    		<tr>
    			<td colspan="6">&nbsp;</td>
    		</tr>
    		<tr>
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_money.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="bk_credits_day_ok"}:</td>
    			<td class="td2" width="15%">{$bk_credits_day_ok}</td>
    			
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_money_back.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="bk_credits_day_error"}:</td>
    			<td class="td2" width="15%">{$bk_credits_day_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="bk_credits_month_ok"}:</td>
    			<td class="td2">{$bk_credits_month_ok}</td>
    			
    			<td class="td1">{lng p="bk_credits_month_error"}:</td>
    			<td class="td2" width="15%">{$bk_credits_month_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="bk_credits_all_ok"}:</td>
    			<td class="td2">{$bk_credits_all_ok}</td>
    			
    			<td class="td1">{lng p="bk_credits_all_error"}:</td>
    			<td class="td2" width="15%">{$bk_credits_all_error}</td>
    		</tr>
		</table>
</fieldset>
<br />
<fieldset>
	<legend>{lng p="bk_brief"} - {lng p="bk_statistik"}</legend>
				<table width="100%">
			<tr>
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_letter.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="b_stats_day_ok"}:</td>
    			<td class="td2" width="15%">{$b_day_ok}</td>
    			
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_letter_error.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="b_stats_day_error"}:</td>
    			<td class="td2" width="15%">{$b_day_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="b_stats_month_ok"}:</td>
    			<td class="td2" width="15%">{$b_month_ok}</td>
    			
    			<td class="td1">{lng p="b_stats_month_error"}:</td>
    			<td class="td2" width="15%">{$b_month_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="b_stats_all_ok"}:</td>
    			<td class="td2" width="15%">{$b_all_ok}</td>
    			
    			<td class="td1">{lng p="b_stats_all_error"}:</td>
    			<td class="td2" width="15%">{$b_all_error}</td>
    		</tr>
    		<tr>
    			<td colspan="6">&nbsp;</td>
    		</tr>
    		<tr>
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_money.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="b_credits_day_ok"}:</td>
    			<td class="td2" width="15%">{$b_credits_day_ok}</td>
    			
    			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/modbriefkarte32_money_back.png" border="0" alt="" width="32" heigh="32" /></td>
    			<td class="td1">{lng p="b_credits_day_error"}:</td>
    			<td class="td2" width="15%">{$b_credits_day_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="b_credits_month_ok"}:</td>
    			<td class="td2">{$b_credits_month_ok}</td>
    			
    			<td class="td1">{lng p="b_credits_month_error"}:</td>
    			<td class="td2" width="15%">{$b_credits_month_error}</td>
    		</tr>
    		<tr>
    			<td class="td1">{lng p="b_credits_all_ok"}:</td>
    			<td class="td2">{$b_credits_all_ok}</td>
    			
    			<td class="td1">{lng p="b_credits_all_error"}:</td>
    			<td class="td2" width="15%">{$b_credits_all_error}</td>
    		</tr>
		</table>
</fieldset>
