<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
	<table id="usertour_admin" class="list">
		<tr>
			<th>Angezeigter Name</th>
			<th>pop3-Server</th>
			<th>Port</th>
			<th>SSL?</th>
			<th></th>
		</tr>
			{foreach from=$names item=name key=key}
			<tr>
				<td><input type="text" class="name" name="name[]" value="{$names[$key]}" readonly style="background: #cccccc" /></td>
				<td><input type="text" class="server" name="server[]" value="{$servers[$key]}" /></td>
				<td><input type="text" class="port" name="port[]" value="{$ports[$key]}" /></td>
				<td><select class="ssl" name="ssl[]">
					<option value="yes" {if $ssls[$key] == "1"}selected{/if}>Ja</option>
					<option value="no" {if $ssls[$key] == "0"}selected{/if}>Nein</option>
				</select></td>
				<td><a href="{$pageURL}&sid={$sid}&do=delete&item={$names[$key]}">Löschen!</a></td>
			</tr>
			{/foreach}
			
	</table>
	<a onclick="addLine()" href="#" style="padding-right: 10px;">Zeile Hinzufügen</a>
	<input class="button" type="submit" value=" {lng p="save"} " />
</form>
{literal}
<script type="text/javascript">
function addLine() {
	var div = document.getElementById('usertour_admin');
	div.innerHTML = div.innerHTML + '<tr><td><input type="text" class="name" name="name[]" value="" /></td><td><input type="text" class="server" name="server[]" value="" /></td><td><input type="text" class="port" name="port[]" value="995" /></td><td><select class="ssl" name="ssl[]"><option value="yes">Ja</option><option value="no">Nein</option></select></td><td></td></tr>';
	return(false);
}
</script>
{/literal}