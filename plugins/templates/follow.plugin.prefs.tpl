<script src="../plugins/templates/follow.plugin.overlibjs.tpl" type="text/javascript" language="javascript"></script>
{if $groupID == "0"}
<form action="{$pageURL}&action=prefs&save=ok&sid={$sid}" method="post" id="newsletterForm" onsubmit="editor.submit();spin(this);">
<fieldset>
        <legend>{lng p="follow_lang_setup"}</legend>
        <table width="100%">
                <tr>
                        <td width="40" valign="top" rowspan="5"><img src="../plugins/templates/images/follow_plugin_setup.png" border="0" alt="" width="32" height="32" /></td>
                        <td class="td1" width="120">{lng p="follow_lang_testmail"}</td>
                        <td class="td2"><input type="text" name="testmail" value="{$testmail}" size="60" /></td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_cron"}</td>
                        <td class="td2">
                                <input type="text" name="perCron" value="{$perCron}" size="5" />
                        </td>
                </tr>
               <tr>
                        <td class="td1" width="120">{lng p="follow_lang_maxResp"}</td>
                        <td class="td2">
                                <input type="text" name="maxResp" value="{$maxResp}" size="4" />&nbsp;<input type="checkbox" name="userReset" id="userReset" value="" />&nbsp;&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_maxResp_info"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a>
                        </td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_maxDatum"}</td>
                        <td class="td2"><input type="hidden" name="maxDatum" value="{$maxDatum}"  /><b>{$maxDatum}</b>&nbsp;<input type="checkbox" name="maxReset" id="maxReset" value="" />&nbsp;&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_maxDatum_info"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a></td>
                </tr>
        </table>
</fieldset>
<fieldset>
        <legend>{lng p="info"}</legend>

        <table width="100%">
                <tr>
                        <td width="40" valign="top"><img src="{$tpldir}images/info32.png" border="0" alt="" width="32" height="32" /></td>
                        <td valign="top">{lng p="follow_lang_einstellung_info"}</td>
                 </tr>
        </table>
</fieldset>
{else}
<form action="{$pageURL}&action=groups&do=aktiv&sid={$sid}" method="post" id="newsletterForm" onsubmit="editor.submit();spin(this);">
<input type="hidden" name="groupID" id="groupID" value="{$groupID}" />
<input type="hidden" name="saving" id="saving" value="{$groupID}" />
<fieldset>
        <legend>{lng p="follow_lang_setup"}</legend>
        <table width="100%">
                <tr>
                        <td width="40" valign="top" rowspan="5"><img src="../plugins/templates/images/follow_plugin_setup.png" border="0" alt="" width="32" height="32" /></td>
                        <td class="td1" width="120">{lng p="follow_lang_cron"}</td>
                        <td class="td2">
                                <input type="text" name="perCron" value="{$perCron}" size="5" />
                        </td>
                </tr>
        </table>
</fieldset>
{/if}

<fieldset>
        <legend>{lng p="recipients"}</legend>

        <table width="100%">
                <tr>
                        <td width="40" valign="top"><img src="{$tpldir}images/filter.png" border="0" alt="" width="32" height="32" /></td>
                        <td class="td1" width="120">{lng p="status"}:</td>
                        <td class="td2">
                                <input type="checkbox" name="statusActive" id="statusActive" {if $lockedValues.statusActive}checked="checked"{/if} />
                                        <label for="statusActive"><b>{lng p="active"}</b></label><br />
                                <input type="checkbox" name="statusLocked" id="statusLocked" {if $lockedValues.statusLocked}checked="checked"{/if}  />
                                        <label for="statusLocked"><b>{lng p="locked"}</b></label><br />
                                <input type="checkbox" name="statusNotActivated" id="statusNotActivated" {if $lockedValues.statusNotActivated}checked="checked"{/if}  />
                                        <label for="statusNotActivated"><b>{lng p="notactivated"}</b></label><br />
                        </td>
{if $groupID == "0"}
                        <td class="td1" width="120">{lng p="groups"}:</td>
                        <td class="td2">
                                {foreach from=$groups item=group key=groupID}
                                        <input type="checkbox" name="group_{$groupID}" id="group_{$groupID}"{if $group.check =="on" } checked="checked"{/if} />
                                                <label for="group_{$groupID}"><b>{text value=$group.title}</b></label><br />
                                {/foreach}
                        </td>
{/if}
                </tr>
        </table>
</fieldset>

<fieldset>
        <legend>{lng p="prefs"}</legend>

        <table width="100%">
                <tr>
                        <td width="40" valign="top" rowspan="5"><img src="{$tpldir}images/rules_import.png" border="0" alt="" width="32" height="32" /></td>
                        <td class="td1" width="120">{lng p="from"}<small>(Name)</small> :</td>
                        <td class="td2"><input type="text" name="from_name" value="{text value=$from_name}" size="60" /></td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="from"}<small>(eMail)</small> :</td>
                        <td class="td2"><input type="text" name="from_mail" value="{text value=$from_mail}" size="60" /></td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="sendto"}:</td>
                        <td class="td2">
                                <input type="radio" name="sendto" value="mailboxes" {if $sendto =="mailboxes"}checked="checked"{/if} />
                                <label for="sendto_mailboxes"><b>{lng p="mailboxes"}</b></label>

                                <input type="radio" name="sendto" value="altmails" {if $sendto =="altmails"}checked="checked"{/if}/>
                                <label for="sendto_altmails"><b>{lng p="altmails"}</b></label>
                        </td>
                </tr>

        </table>
</fieldset>

<p>
        <div style="float:right;">
                <input type="submit" value=" {lng p="save"} " id="submitButton" />&nbsp;
        </div>
</p>
</form>