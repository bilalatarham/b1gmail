<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this);">
	<fieldset>
		<legend>{lng p="prefs"}</legend>
		<table style="width: 100%;">
			<colgroup>
				<col style="width: 250px;" />
				<col />
			</colgroup>
			{foreach from=$fields key=key item=value}
	        	<tr>
					<td class="td1">{text value=$value.title}</td>
					<td class="td2">
						{if $value.type==128}
							<input type="password" style="width:85%;" name="{text value=$key}" value="{text value=$value.value allowEmpty=true}" />
						{elseif $value.type==16}
							<textarea style="width:100%;height:80px;" name="{text value=$key}">{text value=$value.value allowEmpty=true}</textarea>
						{elseif $value.type==8}
							{foreach from=$value.options item=optionValue key=optionKey}
								<input type="radio" name="{text value=$key}" id="{text value=$key}_{text value=$optionKey}" value="{text value=$optionKey}"{if $value.value==$optionKey} checked="checked"{/if} />
								<label for="{text value=$key}_{text value=$optionKey}">{text value=$optionValue}</label>
							{/foreach}
						{elseif $value.type==4}
							<select name="{text value=$key}">
							{foreach from=$value.options item=optionValue key=optionKey}
								<option value="{text value=$optionKey}"{if $value.value==$optionKey} selected="selected"{/if}>{text value=$optionValue}</option>
							{/foreach}
							</select>
						{elseif $value.type==2}
							<input type="checkbox" name="{text value=$key}" value="1"{if $value.value} checked="checked"{/if} />
						{elseif $value.type==1}
							<input type="text" style="width:85%;" name="{text value=$key}" value="{text value=$value.value allowEmpty=true}" />
						{/if}
					</td>
				</tr>
	        {/foreach}
		</table>

	</fieldset>
	<div class="buttons">
 		<input class="button" type="submit" value="{lng p="save"}" />
	</div>
</form>
