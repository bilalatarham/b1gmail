<fieldset>
	<legend>Gruppen</legend>	
	<form action="{$pageURL}sid={$sid}&do=updateGroups" method="post" onsubmit="spin(this);">
		<table class="list">
			<tbody>
				<tr>
					<th>Gruppen-ID</th>
					<th>Titel</th>
					<th>In die Abfrage aufnehmen?</th>
				</tr>
				{foreach from=$groups key=id item=value}
				{cycle name=class values="td1,td2" assign=class}
					<tr class="{$class}">
						<td>{$id}</td>
						<td>{$value.titel}</td>
						<td><input name="group_{$id}" type="checkbox" {if $value.enabled}checked{/if}  /> </td>
					</tr>
				{/foreach}		
			</tbody>
		</table>
		<input class="button" type="submit" value="{lng p='save'}" style="margin-top: 1em;"/>
	</form>
</fieldset>