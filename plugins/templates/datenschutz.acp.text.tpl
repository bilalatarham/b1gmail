{$MSG}

<fieldset>
    <legend>{lng p="language"}</legend>

    <form action="{$pageURL}&do=text&action=add&sid={$sid}" method="post">
        <center>
            <table>
                <tr>
                    <td>{lng p="language"}:</td>
                    <td><select name="lang">
                            {foreach from=$languages key=langID item=lang}
                                <option value="{$langID}"{if $langID==$selectedLang} selected="selected"{/if}>{text value=$lang.title}</option>
                            {/foreach}
                        </select></td>
                    <td><input class="button" type="submit" value=" {lng p="ok"} &raquo; " /></td>
                </tr>
            </table>
        </center>
    </form>
</fieldset>

<fieldset>
    <legend>{lng p="dsDatenschutz"}</legend>

    <table class="list">
        <tr>
            <th width="30">{lng p="dsSortierung"}</th>
            <th>{lng p="title"}</th>
            <th width="55">&nbsp<a href="{$pageURL}&do=dsadd&department={$DepActive}&sid={$sid}" style="float: right;"><img src="{$selfurl}plugins/templates/images/datenschutz_add.png" width="16" height="16" border="0" title="{lng p="SupportSystemTicketAdd"}" align="absmiddle" /></a></th>
        </tr>

        {foreach from=$DSContent item=cont}
            {cycle name=class values="td1,td2" assign=class}
            <tr class="{$class}">
                <td>{text value=$cont.Sortierung}</td>
                <td>{text value=$cont.Title cut=100}</td>
                <td>
                    <a href="{$pageURL}&do=dsedit&id={$cont.ID}&sid={$sid}" title="{lng p="edit"}"><img src="../plugins/templates/images/datenschutz_edit.png" border="0" alt="{lng p="edit"}" width="16" height="16" /></a>
                    <a href="{$pageURL}&do=text&dsdel={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="../plugins/templates/images/datenschutz_delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                </td>
            </tr>
        {/foreach}
    </table>
</fieldset>

<fieldset>
    <legend>{lng p="dsImpressum"}</legend>

    <form action="{$pageURL}&do=text&sid={$sid}" method="post">
        <script language="javascript" src="../clientlib/wysiwyg.js"></script>
        <script type="text/javascript" src="../clientlib/ckeditor/ckeditor.js"></script>
        <table width="100%">
            {foreach from=$Impressum item=imp}
                <tr>
                    <td class="td1" width="180">{$imp.Title}</td>
                    <td class="td2" colspan="2">
                        {if $imp.Text == 'ImpressumContent'}
                            <textarea name="{$imp.Text}" id="{$imp.Text}" class="plainTextArea" style="width:100%;height:300px;">{$imp.Content}</textarea>
                            <script language="javascript">
                                <!--
                                var editor = new htmlEditor('{$imp.Text}');
                                editor.height = 300;
                                editor.init();
                                registerLoadAction('editor.start()');
                                //-->
                            </script>
                        {else}
                            <textarea id="{$imp.Text}" name="{$imp.Text}" style="width: 100%; height: 150px;">{$imp.Content}</textarea>
                        {/if}
                        <small style="font-weight: normal;">{$imp.Desc}</small>
                    </td>
                </tr>
            {/foreach}
        </table>

        <p align="right" style="padding-right: 3px;">
            <input class="button" type="submit" name="imprsave" value=" {lng p="dsSave"} " />
        </p>
    </form>
</fieldset>