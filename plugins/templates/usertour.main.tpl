<div id="usertour">
   <div id="usertour_main">
      {if $usertour_step == 1}
      <div class="stage1">
         <div class="heading">
            {lng p="ut_welcomeheading"}!
         </div>
         <div class="infosec ut-left">
            <img src="plugins/templates/images/aikq_home_logo.png" />
         </div>
         <div class="infosec ut-right">
            {lng p="ut_welcomeexp"}
         </div>
         <div class="infosec ut-bottom">
            {lng p="ut_wanttostart"}
            <button class="step" id="starttour">{lng p="yes"}</button>
            <button class="endtour">{lng p="no"}</button>
         </div>
      </div>
      <div class="stage2 hidden">
         <div class="heading">
            {lng p="ut_pop3heading"}
         </div>
         <div class="pop3 explain">
            {lng p="ut_pop3exp"}
         </div>
         <div class="pop3">
            <label for="pop3serverlist">{lng p="ut_pop3whichprovider"}</label>
            <select id="pop3serverlist" name="pop3serverlist">
               <option></option>
               <option value="other">{lng p="ut_pop3other"}</option>
               {foreach from=$usertourproviderlist item=provider}
               <option value="{$provider}">{$provider}</option>
               {/foreach}
            </select>
         </div>
         <form id="pop3form" name="f1">
            <div class="pop3">
               <input type="checkbox" checked="checked" name="paused" style="display: none">
               <div id="pop3server" style="display: none"></div>
            </div>
            <div class="pop3 hidden" id="pop3account">
               <div id="pop3advanced">
                  <div class="options">
                     <label for="p_host">{lng p="pop3server"}:</label><input id="p_host" type="text" value="" name="p_host">
                  </div>
                  <div class="options">
                     <label for="p_port">{lng p="port"}:</label><input id="p_port" type="text" value="995" name="p_port">
                     <label for="p_ssl" style="width: auto; padding-left: 10px;">SSL?</label><input type="checkbox" name="p_ssl" id="p_ssl" checked="checked" onclick="if(this.checked&&EBID('p_port').value==110) EBID('p_port').value=995; else if(!this.checked&&EBID('p_port').value==995) EBID('p_port').value=110;">
                  </div>
               </div>
               <div class="options">
                  <label for="p_user">{lng p="ut_pop3username"}:</label>
                  <input id="p_user" type="text" value="" name="p_user" size="48" />
               </div>
               <div class="options">
                  <label for="p_user">{lng p="password"}:</label>
                  <input id="p_user" type="password" value="" name="p_pass" size="48" />
               </div>
               <div class="options" id="pop3target">
                  <div id="pop3targetholder">
                     <label for="p_target">{lng p="pop3target"}:</label>
                     <select name="p_target" id="p_target">
                        <option value="-1">({lng p="default"})</option>
                        <optgroup label="{lng p="folders"}">
                        {foreach from=$usertourfolderlist key=dFolderID item=dFolderTitle}
                        {if $dFolderID>0}
                        <option value="{$dFolderID}">{$dFolderTitle}</option>
                        {/if}
                        {/foreach}
                        </optgroup>
                     </select>
                     <button class="noborder" id="pop3addfolder"><i class="fa fa-plus-circle"></i></button>
                  </div>
               </div>
               <div class="options">
                  <label for="p_keep">{lng p="keepmails"}?</label>
                  <input type="checkbox" name="p_keep" id="p_keep" checked="checked" />
               </div>
               <button id="savepop3">{lng p="ut_next"}</button>
            </div>
         </form>
         <div id="pop3error" class="error" style="display: none"></div>
         <div class="pop3loading" style="display: none"><i class="fa fa-cog fa-spin fa-2x"></i></div>
         <button class="jumpstage endpop3">{lng p="ut_jumpstep"} <i class="fa fa-angle-double-right"></i></button>
      </div>
      <div class="stage3 hidden">
         <div class="heading animated">
            {lng p="ut_success"} <i class="fa fa-smile-o"></i>
         </div>
         <div class="infosec ut-bottom">
            {lng p="ut_pop3addanother"}
            <button id="pop3goback">{lng p="yes"}</button>
            <button class="endpop3">{lng p="no"}</button>
         </div>
      </div>
      <div class="extrastage1 hidden">
         <div class="heading">
            {lng p="addfolder"}
         </div>
         <div class="pop3 smallcontent">
            <form name="f2" id="pop3folderform">
               <div class="options">
                  <label for="titel">{lng p="title"}: </label><input id="pop3foldertitel" type="text" value="" name="titel">
               </div>
               <input type="hidden" name="parentfolder" value="-1" />
               <input type="hidden" name="storetime" value="-1" />
               <input type="checkbox" name="subscribed" id="pop3foldersubscribed" checked="checked" style="display: none">
               <input type="checkbox" name="intelligent" id="pop3folderintelligent" style="display: none">
               <div class="options">
                  <button id="pop3savefolder">{lng p="ut_ready"}</button>
                  <button id="pop3extra1end">{lng p="cancel"}</button>
               </div>
            </form>
         </div>
         <div class="error" id="pop3foldererror" style="display: none">{lng p="ut_pop3foldertooshort"}!</div>
         <div class="pop3loading" style="display: none"><i class="fa fa-cog fa-spin fa-2x"></i></div>
      </div>
      {elseif $usertour_step == 2}
      <div class="stage1">
         <div class="heading animated">
            {lng p="ut_aliasheading"}
         </div>
         <div class="alias explain">
            {lng p="ut_aliasexp"}!
         </div>
         <form name="f1" id="aliasform">
            <div class="alias">
               <div class="options">
                  <label for="typ_1">{lng p="aliastype_1"}</label><input type="radio" id="typ_1" name="typ" value="1" id="typ_1" />
               </div>
               <div class="options">
                  <label for="typ_3">{lng p="aliastype_1"} + {lng p="aliastype_2"}</label><input type="radio" id="typ_3" name="typ" value="3" id="typ_3" />
               </div>
            </div>
            <div class="aliasbox hidden options" id="alias1">
               <label for="typ_1_email">{lng p="email"}:</label><input type="text" name="typ_1_email" id="typ_1_email" value="" size="34" /><br />
               <div class="options"><small>{lng p="typ_1_desc"}</small></div>
            </div>
            <div class="aliasbox hidden options" id="alias2" style="display: none">
               <label for="email_local">{lng p="email"}:</label><input type="text" name="email_local" id="email_local" value="" size="20" onblur="checkAddressAvailability()" />
               {if $templatePrefs.domainDisplay!='normal'}<strong> @ </strong>{/if}
               <select name="email_domain" id="email_domain" onblur="checkAddressAvailability()">
                  {foreach from=$usertourdomains item=domain}
                  <option value="{$domain}">{if $templatePrefs.domainDisplay=='normal'}@{/if}{$domain}</option>
                  {/foreach}
               </select>
               <div class="options">
                  <span id="addressAvailabilityIndicator"></span>
               </div>
            </div>
            <button id="savealias" class="hidden">{lng p="ut_next"}</button>
         </form>
         <div id="aliaserror" class="error" style="display: none"></div>
         <div id="aliasloading" style="display: none"><i class="fa fa-cog fa-spin fa-2x"></i></div>
         <button class="jumpstage endalias">{lng p="ut_jumpstep"} <i class="fa fa-angle-double-right"></i></button>
      </div>
      <div class="stage2 hidden options">
         <div class="heading animated">
            {lng p="ut_success"} <i class="fa fa-smile-o"></i>
         </div>
         <div class="infosec ut-bottom">
            {lng p="ut_aliasaddanother"}
            <button id="aliasgoback">{lng p="yes"}</button>
            <button class="endalias">{lng p="no"}</button>
         </div>
      </div>
      {elseif $usertour_step == 3}
      <div class="stage1">
         <div class="heading animated">
            {lng p="ut_signatureheading"}
         </div>
         <div class="signature explain">
            {lng p="ut_signatureexp"}!
         </div>
         <form name="f1" id="signatureform">
            <div class="signature">
               <div class="options">
                  <label for="titel">{lng p="title"}: </label><input id="signaturetitel" type="text" value="" name="titel" />
               </div>
               <div class="options">
                  <textarea name="text" id="text" style="width:100%;height:150px;"></textarea>
               </div>
               <div class="options">
                  <button id="savesignature">{lng p="ut_next"}</button>
               </div>
               <div id="signatureloading" style="display: none"><i class="fa fa-cog fa-spin fa-2x"></i></div>
            </div>
         </form>
         <div id="signatureerror" class="error" style="display: none">{lng p="ut_titletooshort"}!</div>
         <button class="jumpstage endsignature">{lng p="ut_jumpstep"} <i class="fa fa-angle-double-right"></i></button>
      </div>
      <div class="stage2 hidden options">
         <div class="heading animated">
            {lng p="ut_success"} <i class="fa fa-smile-o"></i>
         </div>
         <div class="infosec ut-bottom">
            {lng p="ut_signatureaddanother"}
            <button id="signaturegoback">{lng p="yes"}</button>
            <button class="endsignature">{lng p="no"}</button>
         </div>
      </div>
      {elseif $usertour_step == 4}
      <div class="stage1">
         <div class="heading animated">
            {lng p="ut_finished"} <i class="fa fa-thumbs-o-up"></i>
         </div>
         <div class="addressbook explain">
            {lng p="ut_havefun"}!
         </div>
         <div class="infosec ut-bottom">
            <img src="plugins/templates/images/aikq_home_logo.png" />
            <div class="options">
               <button class="endtour">{lng p="close"}</button>
            </div>
         </div>
      </div>
      {/if}
   </div>
</div>
<div id="usertour_data" style="display: none">
   {if $usertour_pop3server}
   {$usertour_pop3server}
   {elseif $usertour_updatedfolderlist}
   {$usertour_updatedfolderlist}
   {/if}
</div>
<script type="text/javascript">
   var pleasechoose = "{lng p="ut_pleasechoose"}";
   var pop3loginerror = "{lng p="pop3loginerror"}";
   var pop3ownerror = "{lng p="pop3ownerror"}";
   var toomanyaccounts = "{lng p="toomanyaccounts"}";
   var addresstaken = "{lng p="addresstaken"}";
   var addressinvalid = "{lng p="addressinvalid"}";
   var toomanyaliases = "{lng p="toomanyaliases"}";
</script>