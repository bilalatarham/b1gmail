{* $Id: wior.widgetorder.tpl,v 1.1 2008/10/30 19:25:31 patrick Exp $ *}
<style type="text/css">
{literal}<!--
	.dragTable
	{
		width: 100%;
	}
	.dragTableColumn
	{
		vertical-align: top;
	}
	.dragItem
	{
		border-spacing: 0px;
	    border-collapse: collapse;
		border-left: 1px solid #DDDDDD;
		border-right: 1px solid #DDDDDD;
		border-bottom: 1px solid #DDDDDD;
		background-color: #FFFFFF;
	}
	.dragBar
	{
		background-image: url(../templates/default/images/li/list_bg.png);
		font-weight: normal;
		padding-top: 4px;
		padding-left: 5px;
		height: 18px;
		vertical-align: middle;
		border-bottom: 1px solid #DDDDDD;
		cursor: hand;
		cursor: pointer;
	}
	.dragTargetInactive
	{
	}
	.dragTargetActive
	{
		border: 2px dashed #999999;
	}
//-->{/literal}
</style>

<script language="javascript" type="text/javascript">
{literal}<!--

function dashboardOrderChanged()
{
	EBID('order').value = this.order;
}

//-->{/literal}
</script>

<fieldset>
	<legend>{lng p="wior_defaultlayout"}</legend>

	<form action="{$pageURL}&action={$action}&saveOrder=true&sid={$sid}" method="post" onsubmit="spin(this)">
		<input type="hidden" name="order" id="order" value="{$widgetOrder}" />
		
		<div id="dashboard" style="border:1px inset #CCCCCC;background-color:#FEFEFE;padding:10px;">
		</div>
		<div id="dashboard_elems" style="display:none">
		{foreach from=$widgets item=widget key=key}
			<div title="{text value=$widget.title}" id="{$key}"><div style="padding:5px;"><i>({text value=$widget.title})</i></div></div>
		{/foreach}
		</div>
		
		<script src="../clientlib/dragcontainer.js" language="javascript" type="text/javascript"></script>
		<script language="javascript">
		<!--
			var dc = new dragContainer('dashboard', 3, 'dc');
			dc.order = '{$widgetOrder}';
			dc.onOrderChanged = dashboardOrderChanged;
			dc.run();
		//-->
		</script>
		
		<p>
			<div style="float:left;">
				<img src="../plugins/templates/images/wior_add.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				<a href="{$pageURL}&action={$action}&do=addremove&sid={$sid}">{lng p="wior_addremove"}</a>
			</div>
			<div style="float:right;">
				<input type="submit" value=" {lng p="save"} " />
			</div>
		</p>
	</form>
</fieldset>

<fieldset>
	<legend>{lng p="reset"}</legend>
	
	<form action="{$pageURL}&action={$action}&resetOrder=true&sid={$sid}" method="post" onsubmit="spin(this)">
		<table>
			<tr>
				<td width="40" valign="top"><img src="../plugins/templates/images/wior_reset32.png" border="0" alt="" width="32" height="32" /></td>
				<td valign="top">
					<p>
						{lng p="wior_resetdesc"}
					</p>
					
					<blockquote>
						{foreach from=$groups item=group key=groupID}
							<input type="checkbox" name="groups[]" value="{$groupID}" id="group_{$groupID}"{if !$smarty.get.toGroup||$smarty.get.toGroup==$groupID} checked="checked"{/if} />
								<label for="group_{$groupID}"><b>{text value=$group.title}</b></label><br />
						{/foreach}
					</blockquote>
				</td>
			</tr>
		</table>
		
		<p>
			<div style="float:left;">
				<img src="{$tpldir}images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				{lng p="undowarn"}
			</div>
			<div style="float:right;">
				<input type="submit" value=" {lng p="execute"} " />
			</div>
		</p>
	</form>
</fieldset>
