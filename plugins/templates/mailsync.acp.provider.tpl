{$MSG}

<fieldset>
    <legend>{lng p="msProvider"}</legend>

    <table class="list">
        <tr>
            <th width="30">&nbsp;</th>
            <th>{lng p="msName"}</th>
            <th>{lng p="msServer"}</th>
            <th>{lng p="msType"}</th>
            <th>{lng p="msPort"}</th>
            <th>{lng p="msCert"}</th>
            <th width="55">&nbsp;</th>
        </tr>

        {foreach from=$msProvider item=cont}
            {cycle name=class values="td1,td2" assign=class}
            <tr class="{$class}">
                <td>{if $cont.Status == 'yes'}<img src="./templates/images/indicator_green.png">{else}<img src="./templates/images/indicator_grey.png">{/if}</td>
                <td>{text value=$cont.Name cut=100}</td>
                <td>{text value=$cont.Server cut=100}</td>
                <td>{text value=$cont.Type cut=100}</td>
                <td>{text value=$cont.Port cut=100}</td>
                <td>{text value=$cont.Cert cut=100}</td>
                <td>
                    <a href="{$pageURL}&do=provideredit&id={$cont.ID}&sid={$sid}" title="{lng p="edit"}"><img src="{$tpldir}images/edit.png" border="0" alt="{lng p="edit"}" width="16" height="16" /></a>
                    <a href="{$pageURL}&do=provider&delete={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="{$tpldir}images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                </td>
            </tr>
        {/foreach}
    </table>
</fieldset>

<fieldset>
    <legend>{lng p="msProvider"} {lng p="msTextAdd"}</legend>

    <form action="{$pageURL}&do=provider&id={$ID}&lang={$selectedLang}&sid={$sid}" method="post" onsubmit="EBID('title').focus();if(EBID('title').value.length<2) return(false);editor.submit();spin(this)">
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="msActivate"}:</td>
                <td class="td2"><input id="activate" name="activate" type="checkbox" {if $Status == 'yes'}checked="checked"{/if} /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="msName"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="name" id="name" value="{$Name}" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="msServer"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="server" id="server" value="{$Server}" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="msPort"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="port" id="port" value="{$Port}" /></td>
            </tr>
            <tr>
                <td class="td1">{lng p="msProtokoll"}:</td>
                <td class="td2"><select name="type" class="form-control formred" style="width: 277px;">
                        <option value="imap" {if $Protokoll == ''}selected{/if}>IMAP ({lng p="msNotEncrypted"})</option>
                        <option value="imaptls" {if $Protokoll == 'tls'}selected{/if}>IMAP + STARTTLS ({lng p="msEncrypted"})</option>
                        <option value="imapssl" {if $Protokoll == 'ssl'}selected{/if}>IMAP + SSL ({lng p="msEncrypted"})</option>
                    </select></td>
            </tr>
        </table>

        <p align="right">
            <input class="button" type="submit" name="provider-save" value=" {lng p="msSpeichern"} " />
        </p>
    </form>

</fieldset>