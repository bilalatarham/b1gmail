
<h1><img src="./plugins/templates/images/modbriefkarte_bk_send.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="bk_brief"}</h1>

<form name="f1" method="post" action="start.php?action=briefkarte&bktyp=brief&do=BriefPreview&sid={$sid}" enctype="multipart/form-data">
	<table class="listTable">
		<tr>
			<th class="listTableHead">{lng p="b_versandnach"}</th>
            <th class="listTableHead">&nbsp;</th>
            <th class="listTableHead">{lng p="b_farbe"}</th>
		</tr>		
		<tr>
			<td bgcolor="#f3f3f3" align="center">
				<br />
					<input type="radio" name="b_land" value="de" checked="checked" onclick="einschreiben(true);" /> {lng p="bk_de"}
    				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="b_land" value="eu" onclick="einschreiben(false);" /> {lng p="bk_eu"}
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="b_land" value="welt" onclick="einschreiben(false);" /> {lng p="bk_welt"}
				<br /><br />
			</td>
            <td bgcolor="#f3f3f3" align="center">&nbsp;</td>
            <td bgcolor="#f3f3f3" align="center">
            <br />
					<input type="radio" name="b_farbe" value="1" checked="checked" /> {lng p="b_farbe1"}
    				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="b_farbe" value="0" /> {lng p="b_farbe2"}
				<br /><br />
            </td>
		</tr>
	</table>

    <table id="b_einschreiben2" style="display: table;">
        <tr>
            <td height="10"></td>
        </tr>
    </table>
    <table class="listTable" id="b_einschreiben" style="display: table;">
        <tr>
			<th class="listTableHead">{lng p="b_einschreiben_info"}</th>
		</tr>
        <tr>
            <td bgcolor="#f3f3f3" align="center">
                <input type="checkbox" name="b_einschreiben" id="db_einschreiben" value="b_einschreiben" /> {lng p="b_einschreiben"}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="checkbox" name="b_einschreiben_online" id="db_einschreiben_online" value="b_einschreiben_online" /> {lng p="b_einschreiben_online"}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="checkbox" name="b_einschreiben_rs" id="db_einschreiben_rs" value="b_einschreiben_rs" /> {lng p="b_einschreiben_rs"}
            </td>
        </tr>
    </table>
    <br />
    <table class="listTable">
		<tr>
			<th class="listTableHead">{lng p="b_deckblatt"}</th>
		</tr>		
		<tr>
			<td bgcolor="#f3f3f3" align="center">
                <input type="checkbox" name="b_deckblatt" id="db_info" value="b_deckblatt" onclick="deckblatt('db_info', 'b_deckblatt');" /> {lng p="b_db_info"}
			</td>
		</tr>
	</table>
    <table class="listTable" id="b_deckblatt" style="display: none;">
        <tr>
			<td class="listTableLeft">* <label for="b_absender">{lng p="b_absender"}</label>:</td>
			<td class="listTableRight">
				<input type="text" name="b_absender" id="b_absender" value="{text value=$b_absender allowEmpty=true}" size="75" />
			</td>
		</tr>
        <tr>
			<th class="listTableHead" colspan="2">{lng p="b_empf_info"}</th>
		</tr>
        <tr>
			<td class="listTableLeft"><label for="b_firma">{lng p="b_firma"}</label>:</td>
			<td class="listTableRight">
				<input type="text" name="b_empf_firma" id="b_empf_firma" value="{text value=$b_empf_firma allowEmpty=true}" size="35" />
			</td>
		</tr>
        <tr>
			<td class="listTableLeft">* <label for="b_empf_vorname">{lng p="firstname"}</label>:</td>
			<td class="listTableRight">
				<input type="text" name="b_empf_vorname" id="b_empf_vorname" value="{text value=$b_empf_vorname allowEmpty=true}" size="35" />
			</td>
		</tr>
        <tr>
			<td class="listTableLeft">* <label for="b_empf_nachname">{lng p="surname"}</label>:</td>
			<td class="listTableRight">
				<input type="text" name="b_empf_nachname" id="b_empf_nachname" value="{text value=$b_empf_nachname allowEmpty=true}" size="35" />
			</td>
		</tr>
        <tr>
			<td class="listTableLeft">* <label for="b_empf_strhnr">{lng p="streetnr"}</label>:</td>
			<td class="listTableRight">
				<input type="text" name="b_empf_strhnr" id="b_empf_strhnr" value="{text value=$b_empf_strhnr allowEmpty=true}" size="35" />
			</td>
		</tr>
        <tr>
			<td class="listTableLeft">* <label for="b_empf_plz">{lng p="zipcity"}:</label></td>
			<td class="listTableRight">
				<input type="text" name="b_empf_plz" id="b_empf_plz" value="{text value=$b_empf_plz allowEmpty=true}" size="6" />
				<input type="text" name="b_empf_ort" id="b_empf_ort" value="{text value=$b_empf_ort allowEmpty=true}" size="30" />
			</td>
		</tr>
        <tr>
			<td class="listTableLeft"><label for="b_empf_land">{lng p="country"}</label>:</td>
			<td class="listTableRight">
				<input type="text" name="b_empf_land" id="b_empf_land" value="{text value=$b_empf_land allowEmpty=true}" size="35" />&nbsp;
                <span id="addrDiv_to">
					<a href="javascript:openPkAddressbook()">
						<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						{lng p="fromaddr"}
					</a>
				</span>
			</td>
		</tr>
	</table>
	<br />
	<table class="listTable">
		<tr>
			<th class="listTableHead">{lng p="b_dateiupload"}</th>
		</tr>		
		<tr>
			<td bgcolor="#f3f3f3" align="center">
				<br /><input type="file" name="b_datei" size="40"><br /><br />
			</td>
		</tr>
	</table>
    <br />
    <table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2">{lng p="b_textmsg"}</th>
		</tr>	
		<tr>
			<td colspan="2" class="listTableCompose" style="border-top:1px solid #dddddd;">
                <textarea class="composeTextarea" name="b_text" id="b_text" style="width:100%;height:180px;"></textarea>
			</td>
		</tr>
        <tr>
			<td class="listTableLeftDescTopLine" style="height:5px;"></td>
			<td class="listTableRightDesc" style="height:5px;"></td>
		</tr>
        <tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding: 5px 5px 5px 5px;">
				<input type="button" value="{lng p="b_weiter"}" id="sendButton" onclick="if(!checkBkComposeForm()) return(false); document.forms.f1.submit();" />
				<input type="reset" value="{lng p="reset"}" onclick="return askReset();"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="checkbox" name="b_duplex" id="b_duplex" value="b_duplex" /> {lng p="b_duplex"}
			</td>
		</tr>
	</table>
</form>

{literal}
<script language="javascript">
<!--
    function deckblatt(wert1, wert2)
	{
        if(document.getElementById(wert1).checked == true)
        {
                document.getElementById(wert2).style.display = "table";
        }
        else
        {
                document.getElementById(wert2).style.display = "none";
        }
    }
    
    function einschreiben(wert1)
    {
        if(wert1 == true)
        {
            document.getElementById('b_einschreiben').style.display = "table";
            document.getElementById('b_einschreiben2').style.display = "table";
        }
        else
        {
            document.getElementById('b_einschreiben').style.display = "none";
            document.getElementById('b_einschreiben2').style.display = "none";
        }
    }
    
   	function openPkAddressbook()
	{	
		openOverlay('start.php?action=briefkarte&do=addressBookBrief&sid=' + currentSID,
			lang['addressbook'],
			450,
			380,
			true);
	}
    
    function checkBkComposeForm()
	{
	    var einschreiben = false;
        
        if(document.getElementById('db_info').checked == true)
            einschreiben = true;
		
		if(((EBID('b_absender') && EBID('b_absender').value.length < 3)
			|| (EBID('b_empf_vorname') && EBID('b_empf_vorname').value.length < 3)
			|| (EBID('b_empf_nachname') && EBID('b_empf_nachname').value.length < 3)
			|| (EBID('b_empf_strhnr') && EBID('b_empf_strhnr').value.length < 3)
			|| (EBID('b_empf_ort') && EBID('b_empf_ort').value.length < 3))
            && einschreiben == true)
		{
			alert(lang['fillin']);
			return(false);
		}
		return(true);
	}
//-->
</script>
{/literal}