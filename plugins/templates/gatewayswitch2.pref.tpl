<fieldset>
	<legend>{lng p="gatewayswitch_name"}</legend>
	
	<table>
		<tr>
			<td width="48"><img src="../plugins/templates/images/gatewayswitch_logo.png" width="48" height="48" border="0" alt="" /></td>
			<td width="10">&nbsp;</td>
			<td><b>{lng p="gatewayswitch_name"}</b><br />{lng p="gatewayswitch_text"}</td>
		</tr>
	</table>
</fieldset>

{if $id==true}
<form action="plugin.page.php?plugin=gatewayswitch&action=page2&do=update&sid={$sid}&id={$gateway.id}" method="post" onsubmit="spin(this)">
{/if}
{if $id==false}
<form action="plugin.page.php?plugin=gatewayswitch&action=page2&do=save&sid={$sid}" method="post" onsubmit="spin(this)">
{/if}

	<fieldset>
		<legend>{lng p="priority"}</legend>

		<table>
			<tr>
				<td class="td1" width="220">* {lng p="priority"}:</td>
				<td class="td2"><input type="text" name="sort" value="{$gateway.sort}" size="6" />
				<a href="plugin.page.php?plugin=gatewayswitch&action=page4&sid={$sid}"><img src="./templates/images/help.png" border="0" alt="{lng p="help"}" width="16" height="16" /></a></td>
			</tr>
		</table>
	</fieldset>


	<fieldset>
		<legend>{lng p="recvmethod"}</legend>

		<table>
			<tr>
				<td class="td1" width="220">* {lng p="recvmethod"}:</td>
				<td class="td2"><select name="receive_method">
					<option value="pop3"{if $gateway.receive_method=='pop3'} selected="selected"{/if}>{lng p="pop3gateway"}</option>
					<option value="pipe"{if $gateway.receive_method=='pipe'} selected="selected"{/if}>{lng p="pipeetc"}</option>
				</select></td>
			</tr>
		</table>
	</fieldset>


	<fieldset>
		<legend>{lng p="pop3gateway"}</legend>

		<table>
			<tr>
				<td class="td1" width="220">* {lng p="pop3host"}:</td>
				<td class="td2"><input type="text" name="pop3_host" value="{$gateway.pop3_host}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1">* {lng p="pop3port"}:</td>
				<td class="td2"><input type="text" name="pop3_port" value="{$gateway.pop3_port}" size="6" /></td>
			</tr>
			<tr>
				<td class="td1" width="220">* {lng p="pop3user"}:</td>
				<td class="td2"><input type="text" name="pop3_user" value="{$gateway.pop3_user}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220">* {lng p="pop3pass"}:</td>
				<td class="td2"><input type="password" name="pop3_pass" value="{$gateway.pop3_pass}" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220">* {lng p="fetchcount"}:</td>
				<td class="td2"><input type="text" name="fetchcount" value="{$gateway.fetchcount}" size="6" /></td>
			</tr>
		</table>

		<p align="right">
			<input type="submit" value=" {lng p="save"} " class="button"/>
		</p>
	</fieldset>
</form>