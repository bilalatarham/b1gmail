<fieldset>
        <legend>{lng p="follow_lang_start_titel"}</legend>

        <table width="100%">
                <tr>
                        <td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/follow_plugin_cronjob.png" border="0" alt="" width="32" heigh="32" /></td>
                        <td class="td1">{lng p="follow_lang_start_cronjob"}</td>
                        <td class="td2" width="25%">
                        {if $FollowResponderCron == "no"}
                        <font color="#FF0000"><u>ist nicht</u> <b>AKTIV</b></font>&nbsp;&nbsp;[&nbsp;<a href="{$pageURL}&cronjob=yes&sid={$sid}">Cronjob Aktivieren</a>&nbsp;]
                        {else}
                        <font color="#008000"><b>AKTIV</b></font>&nbsp;&nbsp;[&nbsp;<a href="{$pageURL}&cronjob=no&sid={$sid}">Cronjob Deaktivieren</a>&nbsp;]
                        {/if}</td>

                        <td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/follow_plugin_gruppen.png" border="0" alt="" width="32" heigh="32" /></td>
                        <td class="td1">{lng p="follow_lang_start_vday"}</td>
                        <td class="td2" width="25%">{$dayResponderCount}</td>

                </tr>
                <tr>
                        <td class="td1">Aktueller durchlauf</td>
                        <td class="td2">{lng p="$FollowResponderCronText"}</td>

                        <td class="td1">{lng p="follow_lang_start_vges"}</td>
                        <td class="td2">{$gesResponderCount}</td>
                </tr>
                <tr>
                        <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                        <td width="40" align="center" valign="top">&nbsp;</td>
                        <td class="td2">&nbsp;</td
                        <td class="td2"><b>{lng p="follow_lang_start_ges"}</b></td>

                        <td width="40" align="center" valign="top">&nbsp;</td>
                        <td class="td2">&nbsp;</td
                        <td class="td2"><b>{lng p="follow_lang_start_ver"}</b></td>

                </tr>
                <tr>
                        <td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/follow_plugin_kampagnen.png" border="0" alt="" width="32" heigh="32" /></td>
                        <td class="td1">{lng p="follow_lang_start_resp"}:</td>
                        <td class="td2">(<b>{$followCount.respG}</b> / <font color="#FF0000">{$followCount.respD}</font> / <font color="#008000">{$followCount.respA}</font>)</td>

                        <td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/follow_plugin_stats.png" border="0" alt="" width="32" heigh="32" /></td>
                        <td class="td1">{lng p="follow_lang_start_resp"}:</td>
                        <td class="td2">{$followCount.respE}</td>

                </tr>
                <tr>
                        <td class="td1">{lng p="follow_lang_start_datum"}:</td>
                        <td class="td2">(<b>{$followCount.datumG}</b> / <font color="#FF0000">{$followCount.datumD}</font> / <font color="#008000">{$followCount.datumA}</font>)</td>

                        <td class="td1">{lng p="follow_lang_start_datum"}:</td>
                        <td class="td2">{$followCount.datumE}</td>
                </tr>
                <tr>
                        <td class="td1">{lng p="follow_lang_start_grupp"}:</td>
                        <td class="td2">(<b>{$followCount.groupG}</b> / <font color="#FF0000">{$followCount.groupD}</font> / <font color="#008000">{$followCount.groupA}</font>)</td>

                        <td class="td1">{lng p="follow_lang_start_grupp"}:</td>
                        <td class="td2">{$followCount.groupE}</td>
                </tr>
                <tr>
                        <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                        <td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/follow_plugin_info32.png" border="0" alt="" width="32" heigh="32" /></td>
                        <td class="td1">{lng p="follow_lang_maxResp"}:</td>
                        <td class="td2">{$maxResp}</td>

                        <td rowspan="3" width="40" align="center" valign="top">&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                        <td class="td1">{lng p="follow_lang_maxDatum"} :</td>
                        <td class="td2">{$maxDatum}</td>
                        <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                        <td class="td1">{lng p="follow_lang_testmail"}:</td>
                        <td class="td2">{$testmail}</td>
                        <td colspan="2">&nbsp;</td>
                </tr>


        </table>
</fieldset>