<!DOCTYPE html>
<html lang="{lng p="langCode"}">
<head>
    <title>{$service_title} - {lng p="dsDerefTitle"}</title>
    <meta http-equiv="content-type" content="text/html; charset={$charset}" />
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <style>
        {literal}
        body { text-align: center; padding: 20px; }
        @media (min-width: 768px){
            body{ padding-top: 150px; }
        }
        h1 { font-size: 50px; }
        body { font: 20px Helvetica, sans-serif; color: #333; }
        article { display: block; text-align: left; max-width: 650px; margin: 0 auto; }
        a { color: #dc8100; text-decoration: none; }
        a:hover { color: #333; text-decoration: none; }
        {/literal}
    </style>
</head>
<body>
<article>
    <h1>{lng p="dsDerefTitle"}</h1>
    <div>
        <p>{lng p="dsDerefRedirect"}<br /><a href="{$url}" rel="noreferrer nofollow">{$url}</a></p>
        <p>{lng p="dsDerefBack"}</p>
    </div>
</article>
</body>
</html>