{* $Id: taor.taborder.tpl,v 1.1 2009/04/15 09:52:54 patrick Exp $ *}
<form action="{$pageURL}&save=true&sid={$sid}" name="f1" method="post" onsubmit="spin(this)">
<fieldset>
	<legend>{lng p="taor_taborder"}</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th>{lng p="title"}</th>
			<th width="80">{lng p="taor_pos"}</th>
		</tr>
		
		{foreach from=$pageTabs item=tab key=tabKey}
		{cycle name=class values="td1,td2" assign=class}
		<tr class="{$class}">
			<td align="center"><img src="{if !$tab.iconDir}{$usertpldir}images/li/tab_ico_{else}../{$tab.iconDir}{/if}{$tab.icon}.png" border="0" alt="" width="16" height="16" /></td>
			<td>{text value=$tab.text}</td>
			<td><input type="text" name="order[{$tabKey}]" value="{$tab.order}" size="6" /></td>
		</tr>
		{/foreach}
	</table>
</fieldset>
<p>
	<div style="float:right;">
		<input type="submit" value=" {lng p="save"} " />
	</div>
</p>
</form>
