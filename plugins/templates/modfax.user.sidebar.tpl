{if $_tplname=='modern'}
<div class="sidebarHeading">{lng p="modfax_fax"}</div>
<div class="contentMenuIcons">
	<a href="start.php?action=faxPlugin&sid={$sid}"><img src="./plugins/templates/images/modfax_compose.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="modfax_send"}</a><br />
	<a href="start.php?action=faxPlugin&do=outbox&sid={$sid}"><img src="./plugins/templates/images/modfax_outbox.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="modfax_outbox"}</a><br />
	<a href="prefs.php?action=membership&sid={$sid}"><img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="accbalance"}</a><br />
</div>
{else}
<div class="sidebarHeading"> &nbsp; {lng p="modfax_fax"}</div>
<div class="contentMenuIcons">
	&nbsp;<a href="start.php?action=faxPlugin&sid={$sid}"><img src="./plugins/templates/images/modfax_compose.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="modfax_send"}</a><br />
	&nbsp;<a href="start.php?action=faxPlugin&do=outbox&sid={$sid}"><img src="./plugins/templates/images/modfax_outbox.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="modfax_outbox"}</a><br />
	&nbsp;<a href="prefs.php?action=membership&sid={$sid}"><img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="accbalance"}</a><br />
</div>
{/if}
