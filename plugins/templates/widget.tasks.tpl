{* <div class="innerWidget" style="max-height: 150px; overflow-y: auto;">
<table width="100%" cellspacing="0" cellpadding="0">
{foreach from=$bmwidget_tasks_items key=taskID item=task}
	<tr>
		<td><input type="checkbox" onclick="setTaskDone('{$sid}', {$taskID}, this.checked);"{if $task.akt_status==64} checked="checked"{/if} />
		<a href="organizer.todo.php?action=editTask&id={$taskID}&sid={$sid}">{text value=$task.titel cut=30}</a></td>
		<td align="right"><img src="{$tpldir}images/li/mailico_{if $task.priority==-1}low{elseif $task.priority==0}empty{else}high{/if}.gif" border="0" alt="" align="absmiddle" /></td>
	</tr>
{/foreach}
</table>
</div> *}

<div class="dragable-box content-box">
	<div class="box-title">
		<span class="tl-icon">
			<img src="{$tpldir}frontend_assets/img/menu-ic-task.svg" class="img-fluid" />
		</span>
		<h3 class="tl-title fw-med">Tasks</h3>
	</div>
	<div class="widget-notes widget-tasks">
		<div class="preview-area">
			<div class="help-info text-center">
				<span class="note-count fw-med">{$bmwidget_tasks_items|@count}</span>
				<p class="fw-med mb-0">Please click the text preview of a task in the list of tasks to view the complete task.</p>
			</div>
		</div>
		<div class="notes-items mt-4">
			{foreach from=$bmwidget_tasks_items key=taskID item=task}
				<a href="organizer.todo.php?action=editTask&id={$taskID}&sid={$sid}">
					<span class="nt-link-icon">
						<img src="{$tpldir}frontend_assets/img/menu-ic-task.svg" class="img-fluid icon-default">
						<img src="{$tpldir}frontend_assets/img/menu-ic-task-hover.svg" class="img-fluid icon-hover">
					</span>
					<span class="note-text">{text value=$task.titel cut=30}</span>
					<img src="{$tpldir}images/li/mailico_{if $task.priority==-1}low{elseif $task.priority==0}empty{else}high{/if}.gif" border="0" alt="" align="absmiddle" style="float: right;"/>
				</a>
			{/foreach}
		</div>
	</div>
</div>