{if $msShowLog == TRUE}
<fieldset>
    <legend>{lng p="logs"} ({date nice=true timestamp=$start} - {date nice=true timestamp=$end})</legend>

    <table class="list">
        <tr>
            <th width="20">&nbsp;</th>
            <th>{lng p="entry"}</th>
            <th width="150">{lng p="date"}</th>
        </tr>

        {foreach from=$entries item=entry}
            {cycle name=class values="td1,td2" assign=class}
            <tr class="{$class}">
                <td><img src="{$tpldir}images/{$entry.prioImg}.png" border="0" alt="" width="16" height="16" /></td>
                <td><code>{text value=$entry.eintrag}</code></td>
                <td>{date nice=true timestamp=$entry.zeitstempel}</td>
            </tr>
        {/foreach}
    </table>
</fieldset>
{/if}

<fieldset>
    <legend>{lng p="msList"}</legend>
    <table class="list">
        <tr>
            <th width="20">&nbsp;</th>
            <th width="40">ID</th>
            <th>{lng p="msUser"}</th>
            <th>{lng p="msDatum"}</th>
            <th>{lng p="msServer"}</th>
            <th>{lng p="msSMail"}</th>
            <th>{lng p="msTOrdner"}</th>
            <th width="55">{lng p="msAnzMails"}</th>
            <th width="55">{lng p="msAnzFolders"}</th>
            <th width="60">&nbsp;</th>
        </tr>

        {foreach from=$List item=cont}
            {cycle name=class values="td1,td2" assign=class}
            <tr class="{$class}">
                <td align="center"><img src="../plugins/templates/images/{text value=$cont.Status}" width="16" height="16" title="{text value=$cont.StatusText}" /></td>
                <td>{text value=$cont.ID}</td>
                <td><a href="users.php?do=edit&id={$cont.UserID}&sid={$sid}">{text value=$cont.EMail}</a></td>
                <td>{text value=$cont.JobDate cut=100}</td>
                <td>{text value=$cont.SourceServer cut=100} ({text value=$cont.SourceType cut=100}, {text value=$cont.SourcePort cut=100}, {text value=$cont.SourceCert cut=100})</td>
                <td>{text value=$cont.SourceMail cut=100}</td>
                <td>{text value=$cont.TargetFolder cut=100}</td>
                <td>{text value=$cont.AnzMails}</td>
                <td>{text value=$cont.AnzFolders}</td>
                <td>
                    <a href="{$pageURL}&do=logs&id={$cont.ID}&sid={$sid}"><img src="../plugins/templates/images/mailsync_log.png" width="16" height="16" border="0" alt="Log" align="absmiddle" /></a>
                    <a href="{$pageURL}&do=list&delete={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="{$tpldir}images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                </td>
            </tr>
        {/foreach}
    </table>
</fieldset>