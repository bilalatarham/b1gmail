<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_smsoutbox.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="smsoutbox"}
	</div>
</div>


<div class="scrollContainer"><div class="pad">
<form name="f1" method="post" action="sms.php?action=outbox&do=action&sid={$sid}">
<table class="listTable">
	<tr>
		<th class="listTableHead" width="20"><input type="checkbox" id="allChecker" onclick="checkAll(this.checked, document.forms.f1, 'sms');" /></th>
		<th class="listTableHead">
			<a href="sms.php?action=outbox&sid={$sid}&sort=from&order={$sortOrderInv}">{lng p="from"}</a>
			{if $sortColumn=='from'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th class="listTableHead">
			<a href="sms.php?action=outbox&sid={$sid}&sort=to&order={$sortOrderInv}">{lng p="to"}</a>
			{if $sortColumn=='to'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th class="listTableHead" width="160">
			<a href="sms.php?action=outbox&sid={$sid}&sort=date&order={$sortOrderInv}">{lng p="date"}</a>
			{if $sortColumn=='date'}<img src="{$tpldir}images/li/{$sortOrder}.gif" border="0" alt="" align="absmiddle" />{/if}
		</th>
		<th class="listTableHead" width="35">&nbsp;</th>
	</tr>
	
	{if $outbox}
	<tbody class="listTBody">
	{foreach from=$outbox key=smsID item=sms}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	{assign value=$task.priority var=prio}
	{assign value=$task.akt_status var=status}
	<tr>
		<td class="{$class}" nowrap="nowrap"><input type="checkbox" id="sms_{$sms.id}" name="sms_{$sms.id}" /></td>
		<td class="{if $sortColumn=='from'}listTableTDActive{else}{$class}{/if}">&nbsp;<a href="javascript:toggleGroup({$sms.id});"><img id="groupImage_{$sms.id}" src="{$tpldir}images/{if $smarty.request.show==$sms.id}contract{else}expand{/if}.gif" border="0" alt="" align="absmiddle" /></a>&nbsp;{text value=$sms.from}</td>
		<td class="{if $sortColumn=='to'}listTableTDActive{else}{$class}{/if}">&nbsp;{text value=$sms.to}</td>
		<td class="{if $sortColumn=='date'}listTableTDActive{else}{$class}{/if}">&nbsp;{date timestamp=$sms.date nice=true}</td>
		<td class="{$class}">
			<a onclick="return confirm('{lng p="realdel"}');" href="sms.php?action=outbox&do=delete&id={$sms.id}&sid={$sid}"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="delete"}" align="absmiddle" /></a>
		</td>
	</tr>
	<tbody id="group_{$sms.id}" style="display:{if $smarty.request.show!=$sms.id}none{/if}">
	<tr>
		<td colspan="6" class="listTableTDText">{text value=$sms.text}</td>
	</tr>
	</tbody>
	{/foreach}
	</tbody>
	{/if}
	
	<tr>
		<td colspan="6" class="listTableFoot">
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td align="left">
						<select class="smallInput" name="do2">
							<option value="-">------ {lng p="selaction"} ------</option>
							<option value="delete">{lng p="delete"}</option>
						</select>
						<input class="smallInput" type="submit" value="{lng p="ok"}" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<br />
<h1><img src="{$tpldir}images/li/ico_smsoutbox.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="sendlargesmsready"}</h1>

<table class="listTable">
	<tr>
		<th class="listTableHead">
			{lng p="from"}
		</th>
		<th class="listTableHead">
			{lng p="to"}
		</th>
		<th class="listTableHead" width="160">
			{lng p="date"} <img src="{$tpldir}images/li/desc.gif" border="0" alt="" align="absmiddle" />
		</th>
		<th class="listTableHead" width="35">&nbsp;</th>
	</tr>

	{if $largesmsoutbox}
	<tbody class="listTBody">
	{foreach from=$largesmsoutbox key=lsmsID item=lsms}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}">&nbsp;<a href="javascript:toggleGroup2({$lsms.id});"><img id="group2Image_{$lsms.id}" src="{$tpldir}images/{if $smarty.request.show==$lsms.id}contract{else}expand{/if}.gif" border="0" alt="" align="absmiddle" /></a>&nbsp;{text value=$lsms.from}</td>
		<td class="{$class}">&nbsp;{text value=$lsms.to}</td>
		<td class="{$class}">&nbsp;{date timestamp=$lsms.datum nice=true}</td>
		<td class="{$class}">
			<a onclick="return confirm('{lng p="realdel"}');" href="sms.php?action=outbox&do=deletelargesms&id={$lsms.id}&sid={$sid}"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="delete"}" align="absmiddle" /></a>
		</td>
	</tr>
	<tbody id="group2_{$lsms.id}" style="display:{if $smarty.request.show!=$lsms.id}none{/if}">
	<tr>
		<td colspan="6" class="listTableTDText">{text value=$lsms.text}</td>
	</tr>
	</tbody>
	{/foreach}
	</tbody>
	{/if}
</table>

{literal}
<script type="text/javascript">
	function toggleGroup2(id)
	{
		var groupItem = EBID('group2_' + id);
		var groupItemImg = EBID('group2Image_' + id);
		
		if(groupItem.style.display == '')
		{
			groupItem.style.display = 'none';
			groupItemImg.src = groupItemImg.src.replace(/contract/, 'expand');
		}
		else
		{
			groupItem.style.display = '';
			groupItemImg.src = groupItemImg.src.replace(/expand/, 'contract');
		}
	}
</script>
{/literal}
</div></div>