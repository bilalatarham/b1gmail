{$MSG}

<div style="text-align: center; margin: 20px 0px 20px 0px;">
    <img src="../plugins/templates/images/datenschutz_logo.png" width="450" height="48" alt="OneSystems" /><br /><br />
    <strong>OneSystems</strong><br />
    Plugin Version: {$Version}<br /><br />
</div>

<fieldset>
    <legend>{lng p="dsSupport"}</legend>
    Falls es Fragen oder Probleme gibt verwenden Sie bitte nachfolgendes Kontakformular:<br />
    <a href="https://www.onesystems.ch/kontakt/" target="_blank">https://www.onesystems.ch/kontakt/</a><br />
    <a href="https://faq.onesystems.ch/de-de/6-datenschutz" target="_blank">Knowledgebase</a>
</fieldset>

<table style="width: 100%; margin: 0px; padding: 0px;">
    <tr style="vertical-align: top;">
        <td style="margin: 0px; padding: 0px; width: 50%;">
            <fieldset>
                <legend>{lng p="dsLegende"}</legend>
                Synchronisationssatus

                <table width="100%">
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/datenschutz_grey.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="dsStatusOFF"}</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/datenschutz_yellow.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="dsStatusONG"}</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/datenschutz_yellow_accepted.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="dsStatusONA"}</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/datenschutz_green.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="dsStatusON"}</td>
                    </tr>
                </table>
            </fieldset>
        </td>
        <td style="margin: 0px; padding: 0px; width: 50%;">
            <fieldset style="margin-left: 10px;">
                <legend>{lng p="dsVorlageLaden"}</legend>
                {lng p="dsVorlageLadenBeschr"}<br />
                <a href="{$pageURL}&do=hilfe&import=ds&sid={$sid}" onclick="return confirm('{lng p="dsVorlageWarnung"}');">{lng p="dsDatenschutz"}</a><br />
                <a href="{$pageURL}&do=hilfe&import=av&sid={$sid}" onclick="return confirm('{lng p="dsVorlageWarnung"}');">{lng p="dsAV"}</a><br />
                <a href="{$pageURL}&do=hilfe&import=module&sid={$sid}" onclick="return confirm('{lng p="dsVorlageWarnung"}');">Module {lng p="dsModuleAdd"}</a><br />
                <a href="{$pageURL}&do=hilfe&import=impressum&sid={$sid}" onclick="return confirm('{lng p="dsVorlageWarnung"}');">{lng p="dsImpressum"}</a><br /><br /><br />
            </fieldset>
        </td>
    </tr>
    <tr style="vertical-align: top;">
        <td style="margin: 0px; padding: 0px; width: 50%;">
            <fieldset>
                <legend>PHP</legend>
                <table width="100%">
                    <tr>
                        <td class="td1">{$PHPMemory}</td>
                        <td class="td2">PHP Memory</td>
                    </tr>
                    <tr>
                        <td class="td1"><img src="../plugins/templates/images/{$PHPExtGD}" width="16" height="16" alt="Status" /></td>
                        <td class="td2">GD Library</td>
                    </tr>
                    <tr>
                        <td class="td1"><img src="../plugins/templates/images/{$PHPExtMBSTRING}" width="16" height="16" alt="Status" /></td>
                        <td class="td2">Mbstring</td>
                    </tr>
                </table>
            </fieldset>
        </td>
        <td style="margin: 0px; padding: 0px; width: 50%;">
            <fieldset style="margin-left: 10px;">
                <legend>PHP Class</legend>
                <table width="100%">
                    <tr>
                        <td class="td1" width="100">TCPDF Class</td>
                        <td class="td2">{$TCPDF}</td>
                    </tr>

                </table>
            </fieldset>
        </td>
    </tr>
</table>

<fieldset>
    <legend>{lng p="dsPlatzhalter"}</legend>

    <table class="list">
        <tr>
            <th width="180px;">{lng p="dsPlatzhalter"}</th>
            <th>{lng p="dsPlatzhalterBesch"}</th>
            <th>{lng p="dsPlatzhalterInhalt"}</th>
        </tr>
        <tr class="td1">
            <td>[%kundendaten%]</td>
            <td>Kontaktadresse des Kunden</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%aktdatum%]</td>
            <td>Aktuelles Datum</td>
            <td>{$Datum}</td>
        </tr>
        <tr class="td1">
            <td>[%anrede%]</td>
            <td>Anrede</td>
            <td>Sehr geehrte Frau / Sehr geehrter Herr</td>
        </tr>
        <tr class="td2">
            <td>[%name%]</td>
            <td>{lng p="dsGeneralName"}</td>
            <td></td>
        </tr>
        <tr class="td1">
            <td>[%optional%]</td>
            <td>{lng p="dsGeneralOptional"}</td>
            <td></td>
        </tr>
        <tr class="td2">
            <td>[%street%]</td>
            <td>{lng p="dsGeneralStreet"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%streetnr%]</td>
            <td>{lng p="dsGeneralStreet"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%plz%]</td>
            <td>{lng p="dsGeneralPLZPlace"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%place%]</td>
            <td>{lng p="dsGeneralPLZPlace"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%country%]</td>
            <td>{lng p="dsGeneralCountry"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%contactmail%]</td>
            <td>{lng p="dsContactMail"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%privacymail%]</td>
            <td>{lng p="dsPrivacyMail"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%phone%]</td>
            <td>{lng p="dsPhone"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%fax%]</td>
            <td>{lng p="dsFax"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%contactform%]</td>
            <td>{lng p="dsContactForm"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%contactmore%]</td>
            <td>{lng p="dsMore"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%authorized%]</td>
            <td>{lng p="dsAuthorizedToRepresent"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%ustid%]</td>
            <td>{lng p="dsUstID"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%widnr%]</td>
            <td>{lng p="dsWidNr"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%businessarea%]</td>
            <td>{lng p="dsBusinessArea"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%agburl%]</td>
            <td>{lng p="dsAGBUrl"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td2">
            <td>[%registrycourt%]</td>
            <td>{lng p="dsRegistryCourt"}</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="td1">
            <td>[%registrycourtnr%]</td>
            <td>{lng p="dsRegistryCourtNr"}</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>{lng p="dsCookieTemplate"}</legend>
    <table width="100%">
        <tr>
            <td class="td1" width="180">Variante 1:</td>
            <td class="td2">
                /templates/[TEMPLATENAME]/nli/index.tpl<br />
                CSS:<br />
                <textarea style="width: 100%; height: 30px;" disabled>&#123;foreach from=$_cssFiles.nli item=_file&#125; <link rel="stylesheet" type="text/css" href="&#123;$_file&#125;" />&#123;/foreach&#125;</textarea>

                JavaScript:<br />
                <textarea style="width: 100%; height: 30px;" disabled>&#123;foreach from=$_jsFiles.nli item=_file&#125;  <script type="text/javascript" src="&#123;$_file&#125;"></script>&#123;/foreach&#125;</textarea>

                <br />
                <br />

                /templates/[TEMPLATENAME]/li/index.tpl<br />
                CSS:<br />
                <textarea style="width: 100%; height: 30px;" disabled>&#123;foreach from=$_cssFiles.li item=_file&#125; <link rel="stylesheet" type="text/css" href="&#123;$_file&#125;" />&#123;/foreach&#125;</textarea>

                JavaScript:<br />
                <textarea style="width: 100%; height: 30px;" disabled>&#123;foreach from=$_jsFiles.li item=_file&#125;  <script type="text/javascript" src="&#123;$_file&#125;"></script>&#123;/foreach&#125;</textarea>
            </td>
        </tr>
        <tr>
            <td class="td1" width="180">Variante 2:</td>
            <td class="td2">
                /templates/[TEMPLATENAME]/nli/index.tpl<br />
                /templates/[TEMPLATENAME]/li/index.tpl<br />
                CSS:<br />
                <textarea style="width: 100%; height: 30px;" disabled><link rel="stylesheet" type="text/css" href="./plugins/css/datenschutz_cookieinfo.css" /></textarea>

                JavaScript:<br />
                <textarea style="width: 100%; height: 50px;" disabled><script type="text/javascript" src="./plugins/js/datenschutz_cookieinfo.js"></script>
<script type="text/javascript" src="./plugins/js/datenschutz_cookieinfo_design.js"></script></textarea>
            </td>
        </tr>
    </table>
</fieldset>
