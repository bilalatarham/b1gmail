<fieldset>
        <legend>Opinion</legend>
        <table>
                <tr>
                        <td width="48"><img src="../plugins/templates/images/opinion_big.png" width="48" height="48" border="0" alt="" /></td>
                        <td width="10">&nbsp;</td>
                        <td><b>Thats what your users telling</b><br />Shows a small overview of the user opinions.</td>
                </tr>
        </table>
</fieldset>

<fieldset>
        <legend>Opinion</legend>
        <table class="list" style="table-layout:fixed">
                <tr>
                        <th width="4%">{lng p="id"}</th>
                        <th width="15%">Username</th>
                        <th width="63%">Opinion</th>
                        <th width="4%">Rate</th>
                        <th width="8%">Date</th>
                        <th width="6%"></th>
                </tr>
        </table>
        {foreach from=$opinions_acp item=op_acp}
        <table class="list" style="table-layout:fixed">
                <tr class="td1">
                        <td width="4%"><center><a href="users.php?do=edit&id={$op_acp.id}&sid={$sid}">{$op_acp.id}</a></center></td>
                        <td width="15%"><center>{$op_acp.vorname} {$op_acp.nachname}</center></td>
                        <td width="63%"><center>{$op_acp.text}</center></td>
                        <td width="4%"><center>{$op_acp.rate}</center></td>
                        <td width="8%"><center>{$op_acp.datum}</center></td>
                        <td width="6%">
							<center>
							<a href="plugin.page.php?plugin=rating&action=overview&act=op&do={if $op_acp.public}de{/if}activate&id={$op_acp.id}&sid={$sid}"><img src="{$tpldir}images/{if $op_acp.public}ok{else}error{/if}.png" width="16" height="16" alt="{if $op_acp.public}{lng p="continue"}{else}{lng p="pause"}{/if}" border="0" /></a>
							<a href="plugin.page.php?plugin=rating&sid={$sid}&action=overview&act=op&do=delete&id={$op_acp.id}" onclick="return confirm('{lng p="realdel"}');"><img width="16" height="16" border="0" alt="{lng p="delete"}" src="./templates/images/delete.png"></a>
							</center>
						</td>
                </tr>
        </table>
        {/foreach}
</fieldset>