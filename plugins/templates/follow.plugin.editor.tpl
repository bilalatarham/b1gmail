<script src="../plugins/templates/follow.plugin.overlibjs.tpl" type="text/javascript" language="javascript"></script>
<form action="{$pageURL}&action=editor&do=save&sid={$sid}" method="post" id="newsletterForm" onsubmit="editor.submit();spin(this);">

{if $RespID =="0"}
<input type="hidden" name="db_query" id="db_query" value="insert" />
{else}
<input type="hidden" name="id" id="id" value="{$RespID}" />
<input type="hidden" name="db_query" id="db_query" value="update" />
<input type="hidden" name="erhalten" id="erhalten" value="{$erhalten}" />
{/if}
<input type="hidden" name="mod" id="mod" value="{$mod}" />
{if $groupID !="0"}
<input type="hidden" name="groupID" id="groupID" value="{$groupID}" />
{/if}

{if $error !="0"}
<fieldset>
        <legend>{lng p="follow_lang_fehler_responder"}</legend>
         <table>
                <tr>
                        <td width="36" valign="top"><img src="{$tpldir}images/info32.png" border="0" alt="" width="32" height="32" /></td>
                        <td valign="top"><font color="#FF0000"><b>{lng p="$error"}</b></font></td>
                </tr>
        </table>
</fieldset>
{/if}

{if $mod =="datum"}
<fieldset>
        <legend>{lng p="recipients"}</legend>

        <table width="100%">
                <tr>
                        <td width="40" valign="top"><img src="{$tpldir}images/filter.png" border="0" alt="" width="32" height="32" /></td>
                        <td class="td1" width="120">{lng p="status"}:</td>
                        <td class="td2">
                                <input type="checkbox" name="statusActive" id="statusActive" {if $lockedValues.statusActive}checked="checked"{/if} />
                                        <label for="statusActive"><b>{lng p="active"}</b></label><br />
                                <input type="checkbox" name="statusLocked" id="statusLocked" {if $lockedValues.statusLocked}checked="checked"{/if}  />
                                        <label for="statusLocked"><b>{lng p="locked"}</b></label><br />
                                <input type="checkbox" name="statusNotActivated" id="statusNotActivated" {if $lockedValues.statusNotActivated}checked="checked"{/if}  />
                                        <label for="statusNotActivated"><b>{lng p="notactivated"}</b></label><br />
                        </td>
                        <td class="td1" width="120">{lng p="groups"}:</td>
                        <td class="td2">
                                {foreach from=$groups item=group key=groupID}
                                        <input type="checkbox" name="group_{$groupID}" id="group_{$groupID}"{if $group.check =="on" } checked="checked"{/if} />
                                                <label for="group_{$groupID}"><b>{text value=$group.title}</b></label><br />
                                {/foreach}
                        </td>
                </tr>
        </table>
</fieldset>

<fieldset>
        <legend>{lng p="prefs"}</legend>

        <table width="100%">
                <tr>
                        <td width="40" valign="top" rowspan="5"><img src="{$tpldir}images/rules_import.png" border="0" alt="" width="32" height="32" /></td>
                        <td class="td1" width="120">{lng p="from"}<small>&nbsp;<i>{lng p="follow_lang_name"}</i>&nbsp;</small>:</td>
                        <td class="td2"><input type="text" name="from_name" value="{text value=$from_name}" size="60" /></td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="from"}<small>&nbsp;<i>{lng p="follow_lang_email"}</i>&nbsp;</small>:</td>
                        <td class="td2"><input type="text" name="from_mail" value="{text value=$from_mail}" size="60" /></td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="sendto"}:</td>
                        <td class="td2">
                                <input type="radio" name="sendto" value="mailboxes" {if $sendto =="mailboxes"}checked="checked"{/if} />
                                <label for="sendto_mailboxes"><b>{lng p="mailboxes"}</b></label>

                                <input type="radio" name="sendto" value="altmails" {if $sendto =="altmails"}checked="checked"{/if}/>
                                <label for="sendto_altmails"><b>{lng p="altmails"}</b></label>
                        </td>
                </tr>
                <tr>
                        <td class="td1" width="120">per Cronjob:</td>
                        <td class="td2">
                                <input type="text" name="perCron" value="{$perCron}" size="5" />
                        </td>
                </tr>
        </table>
</fieldset>
{/if}

<fieldset>
        <legend>{if $RespID =="0"}{lng p="follow_lang_new_$mod"}{else}{lng p="follow_lang_edit_$mod"}{/if}</legend>

        <table width="100%">
                <tr>
                        <td colspan="3" width="40" valign="top" rowspan="5"><img src="{$tpldir}images/newsletter.png" border="0" alt="" width="32" height="32" /></td>
                </tr>
                <tr>
                        <td class="td1">{lng p="subject"}:</td>
                        <td class="td2"><input type="text" name="subject" value="{$betreff}" size="60" /></td>
                </tr>
                <tr>
                        <td class="td1">{lng p="priority"}:</td>
                        <td class="td2"><select name="priority" id="priority">
                                                        <option value="1">{lng p="prio_1"}</option>
                                                        <option value="0" selected="selected">{lng p="prio_0"}</option>
                                                        <option value="-1">{lng p="prio_-1"}</option>
                                                </select></td>
                </tr>
                <tr>
                        <td colspan="2" style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
                                <textarea name="emailText" id="emailText" class="plainTextArea" style="width:100%;height:320px;">{$nachricht}</textarea>
                                <script language="javascript" src="../clientlib/wysiwyg.js"></script>

                                <script language="javascript">
                                <!--
                                        var editor = new htmlEditor('emailText', '{$usertpldir}/images/editor/');
                                        editor.init();
                                        registerLoadAction('editor.start()');
                                //-->
                                </script>


                        </td>
                </tr>
                <tr>
                        <td width="220">
                                <select class="smallInput" onchange="editor.insertText(this.value);">
                                        <option value="">-- {lng p="vars"} --</option>
                                        <option value="%%email%%">%%email%% ({lng p="email"})</option>
                                        <option value="%%firstname%%">%%firstname%% ({lng p="firstname"})</option>
                                        <option value="%%lastname%%">%%lastname%% ({lng p="lastname"})</option>
                                </select>&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_platzhalter"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a>
                        </td>
                        <td>
                                <select class="smallInput" onchange="editor.insertText(this.value);">
                                        <option value="">-- {lng p="follow_lang_datumPlus"} --</option>
                                        <option value="%%datum+0%%">%%datum+0%% (datum+0)</option>
                                        <option value="%%datum+1%%">%%datum+1%% (datum+1)</option>
                                        <option value="%%datum+2%%">%%datum+2%% (datum+2)</option>
                                        <option value="%%datum+3%%">%%datum+3%% (datum+3)</option>
                                        <option value="%%datum+4%%">%%datum+4%% (datum+4)</option>
                                        <option value="%%datum+5%%">%%datum+5%% (datum+5)</option>
                                        <option value="%%datum+6%%">%%datum+6%% (datum+6)</option>
                                        <option value="%%datum+7%%">%%datum+7%% (datum+7)</option>
                                        <option value="%%datum+8%%">%%datum+8%% (datum+8)</option>
                                        <option value="%%datum+9%%">%%datum+9%% (datum+9)</option>
                                        <option value="%%datum+10%%">%%datum+10%% (datum+10)</option>
                                        <option value="%%datum+11%%">%%datum+11%% (datum+11)</option>
                                        <option value="%%datum+12%%">%%datum+12%% (datum+12)</option>
                                        <option value="%%datum+13%%">%%datum+13%% (datum+13)</option>
                                        <option value="%%datum+14%%">%%datum+14%% (datum+14)</option>
                                </select>&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_datum_plus"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a>
                        </td>
                </tr>
        </table>
</fieldset>

<fieldset>
        <legend>{lng p="prefs"}</legend>

        <table width="100%">
                <tr>
                        <td width="40" valign="top" rowspan="4"><img src="{$tpldir}images/newsletter_prefs.png" border="0" alt="" width="32" height="32" /></td>


                        {if $RespID =="0"}

                        <td class="td1">{lng p="mode"}:</td>
                        <td class="td2">
                                <input type="radio" name="mode" value="html" id="mode_html" checked="checked" onclick="if(this.checked) return editor.switchMode('html');" />
                                <label for="mode_html"><b>{lng p="htmltext"}</b></label>

                                <input type="radio" name="mode" value="text" id="mode_text"  onclick="if(this.checked) return editor.switchMode('text');" />
                                <label for="mode_text"><b>{lng p="plaintext"}</b></label>
                        </td>
                        {else}
                        {if $textModus =="text"}
                        <script language="javascript">
                                         <!--
                                         editor.switchMode('text','noMSG=""');
                                         //-->
                                 </script>
                        {/if}
                        <td class="td1">{lng p="mode"}:</td>
                        <td class="td2">
                                <input type="radio" name="mode" value="html" id="mode_html" {if $textModus =="html"}checked="checked"{/if} onclick="if(this.checked) return editor.switchMode('html');" />
                                <label for="mode_html"><b>{lng p="htmltext"}</b></label>

                                <input type="radio" name="mode" value="text" id="mode_text" {if $textModus =="text"}checked="checked"{/if} onclick="if(this.checked) return editor.switchMode('text');" />
                                <label for="mode_text"><b>{lng p="plaintext"}</b></label>
                        </td>
                       {/if}
                </tr>
                {if $mod == "datum"}
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_regSeid"}:</td>
                        <td class="td2"><input type="text" name="tag" value="{$tage}" size="10" maxlength="10" /> Tage&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_versand_reg_dat"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a></td>
                </tr>
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_send_am"}:</td>
                        <td class="td2"><input type="text" name="datum" value="{$datum}" size="10" > &nbsp;&nbsp;(Tag-Monat-Jahr)&nbsp;(01-01-2008)&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_datum_versand"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a> </td>
                </tr>
                <tr>

                        <td class="td1" width="120">{lng p="follow_lang_send_ab"}:</td>
                        <td class="td2"><select name="stunde" id="stunde">
                                        <option {if $stunde == "0"} selected="selected"{/if} value="0">00.00 Uhr</option>
                                        <option {if $stunde == "1"} selected="selected"{/if} value="1">01.00 Uhr</option>
                                        <option {if $stunde == "2"} selected="selected"{/if} value="2">02.00 Uhr</option>
                                        <option {if $stunde == "3"} selected="selected"{/if} value="3">03.00 Uhr</option>
                                        <option {if $stunde == "4"} selected="selected"{/if} value="4">04.00 Uhr</option>
                                        <option {if $stunde == "5"} selected="selected"{/if} value="5">05.00 Uhr</option>
                                        <option {if $stunde == "6"} selected="selected"{/if} value="6">06.00 Uhr</option>
                                        <option {if $stunde == "7"} selected="selected"{/if} value="7">07.00 Uhr</option>
                                        <option {if $stunde == "8"} selected="selected"{/if} value="8">08.00 Uhr</option>
                                        <option {if $stunde == "9"} selected="selected"{/if} value="9">09.00 Uhr</option>
                                        <option {if $stunde == "10"} selected="selected"{/if} value="10">10.00 Uhr</option>
                                        <option {if $stunde == "11"} selected="selected"{/if} value="11">11.00 Uhr</option>
                                        <option {if $stunde == "12"} selected="selected"{/if} value="12">12.00 Uhr</option>
                                        <option {if $stunde == "13"} selected="selected"{/if} value="13">13.00 Uhr</option>
                                        <option {if $stunde == "14"} selected="selected"{/if} value="14">14.00 Uhr</option>
                                        <option {if $stunde == "15"} selected="selected"{/if} value="15">15.00 Uhr</option>
                                        <option {if $stunde == "16"} selected="selected"{/if} value="16">16.00 Uhr</option>
                                        <option {if $stunde == "17"} selected="selected"{/if} value="17">17.00 Uhr</option>
                                        <option {if $stunde == "18"} selected="selected"{/if} value="18">18.00 Uhr</option>
                                        <option {if $stunde == "19"} selected="selected"{/if} value="19">19.00 Uhr</option>
                                        <option {if $stunde == "20"} selected="selected"{/if} value="20">20.00 Uhr</option>
                                        <option {if $stunde == "21"} selected="selected"{/if} value="21">21.00 Uhr</option>
                                        <option {if $stunde == "22"} selected="selected"{/if} value="22">22.00 Uhr</option>
                                        <option {if $stunde == "23"} selected="selected"{/if} value="23">23.00 Uhr</option>
                                </select>&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_senden_ab"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a>

                </tr>
                {else}
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_send_tag"}:</td>
                        <td class="td2"><input type="text" name="tag" value="{$tage}" size="10" maxlength="10">
                        {if $RespID =="0"}
                        {lng p="follow_lang_send_eins"}&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_versand_tag"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a></td>
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_gueltig"}:</td>
                        <td class="td2"><input type="Checkbox" name="an_user" value="ja">{lng p="follow_lang_gueltig_user"}&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_gueltig_info"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a></td>
                </tr>
                        {else}
                        &nbsp;&nbsp;&nbsp;{lng p="follow_lang_send_stats"}: <input type="Checkbox" name="statistik" value="ja"></td>

                {if $aktiv == "nein"}
                <tr>
                        <td class="td1" width="120">{lng p="follow_lang_gueltig"}:</td>
                        <td class="td2"><input type="Checkbox" name="an_user" value="ja" {$an_user}>{lng p="follow_lang_gueltig_user"}&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_gueltig_info"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a></td>
                </tr>
                {/if}


                        {/if}

                </tr>
                {/if}
        </table>
</fieldset>

<p>
        <div style="float:right;">
                <input type="submit" {if $RespID =="0"}value=" {lng p="save"} "{else}value=" {lng p="edit"} "{/if} id="submitButton" />&nbsp;
        </div>
</p>
<br />
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>