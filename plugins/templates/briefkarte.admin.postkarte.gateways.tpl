<form action="plugin.page.php?plugin=briefkarte&action=bkgateways&do=save&sid={$sid}" method="post" onsubmit="spin(this)">
<fieldset>
	<legend>{lng p="bk_karte"} - {lng p="prefs"}</legend>
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="../plugins/templates/images/modbriefkarte32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200">{lng p="bk_gw"}:</td>
				<td class="td2"><textarea style="width:85%;" id="post_gw" name="post_gw" rows="1">{text value=$post_gw allowEmpty=true}</textarea></td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de"}:</td>
				<td class="td2"><input type="text" size="10" id="post_preis_de" name="post_preis_de" value="{text value=$post_preis_de allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_eu_welt"}:</td>
				<td class="td2"><input type="text" size="10" id="post_preis_eu" name="post_preis_eu" value="{text value=$post_preis_eu allowEmpty=true}" /> Credits</td>
			</tr>	
			<tr>
				<td class="td1">{lng p="user"}:</td>
				<td class="td2"><input type="text" size="36" id="post_user" name="post_user" value="{text value=$post_user allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="password"}:</td>
				<td class="td2"><input type="password" autocomplete="off" size="36" id="post_pw" name="post_pw" value="{text value=$post_pw allowEmpty=true}" /></td>
			</tr>
		</table>
		<p align="right">
			<input type="submit" value=" {lng p="save"} " />
		</p>
</fieldset>
</form>
