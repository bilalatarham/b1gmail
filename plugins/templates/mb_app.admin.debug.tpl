<fieldset>
	<legend>{lng p="mb_app_fcm"}</legend>
	{lng p="mb_app_debug_fcm_desc"}
	<table style="width: 100%;">
		<colgroup>
			<col style="width: 250px;" />
			<col />
		</colgroup>

		<tr>
			<td class="td1">{lng p="mb_app_debug_userinput"}:</td>
			<td class="td2">
				<form onsubmit='return window.debug_push("fcm")'>
					<input id="debug_fcm" type="text" style="width:85%;" value="" />
					<input class="button" type="submit" value="{lng p="test"}" />
				</form>
			</td>
		</tr>

	</table>
</fieldset>

<fieldset>
	<legend>{lng p="mb_app_apns"}</legend>
	{lng p="mb_app_debug_apns_desc"}
	<table style="width: 100%;">
		<colgroup>
			<col style="width: 250px;" />
			<col />
		</colgroup>

		<tr>
			<td class="td1">{lng p="mb_app_debug_userinput"}:</td>
			<td class="td2">
				<form onsubmit='return window.debug_push("apns")'>
					<input id="debug_apns" type="text" style="width:85%;" value="" />
					<input class="button" type="submit" value="{lng p="test"}" />
				</form>
			</td>
		</tr>

	</table>
</fieldset>

{literal}
<script>
function debug_push(service) {
	'use strict';
	var value = document.getElementById('debug_' + service).value;
	{/literal}
	var url = "{$pageURL}&sid={$sid}&debug=" + service + "&user=" + encodeURIComponent(value);
	{literal}
	window.open(url, "_blank");
	return false;
}
</script>
{/literal}
