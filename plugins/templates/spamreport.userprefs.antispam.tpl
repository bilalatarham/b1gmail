
<h1><img src="{$tpldir}images/li/ico_antispam.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="antispam"}</h1>

<form name="f1" method="post" action="prefs.php?action=antispam&do=save&sid={$sid}">
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="antispam"}</th>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="spamfilter">{lng p="spamfilter"}:</label></td>
			<td class="listTableRight">
				<input type="checkbox" name="spamfilter" id="spamfilter"{if $spamFilter} checked="checked"{/if} />
				<label for="spamfilter"><b>{lng p="enable"}</b></label>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="unspamme">{lng p="unspamme"}:</label></td>
			<td class="listTableRight">
				<input type="checkbox" name="unspamme" id="unspamme"{if $unspamMe} checked="checked"{/if} />
				<label for="unspamme"><b>{lng p="marknonspam"}</b></label>
			</td>
		</tr>
		{if $localMode}
		<tr>
			<td class="listTableLeft"><label for="bayes_border">{lng p="bayesborder"}:</label></td>
			<td class="listTableRight">
				<table class="bayesBorderTable">
					<tr>
						<td colspan="2">
							<div class="bayesBorderSlider">
								<table class="bayesBorderTable2">
									<td><input type="radio" name="bayes_border" value="98"{if $bayes_border==98} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="96"{if $bayes_border==96} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="94"{if $bayes_border==94} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="92"{if $bayes_border==92} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="90"{if $bayes_border==90} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="88"{if $bayes_border==88} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="84"{if $bayes_border==84} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="80"{if $bayes_border==80} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="75"{if $bayes_border==75} checked="checked"{/if} /></td>
									<td><input type="radio" name="bayes_border" value="70"{if $bayes_border==70} checked="checked"{/if} /></td>					
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td class="bayesBorderLeftTD">{lng p="defensive"}</td>
						<td class="bayesBorderRightTD">{lng p="aggressive"}</td>
					</tr>
				</table>
			</td>
		</tr>
		{/if}
		<tr>
			<td class="listTableLeft"><label for="spamaction">{lng p="spamaction"}:</label></td>
			<td class="listTableRight">
				<select name="spamaction" id="spamaction">
					<option value="-1"{if $spamAction==-1} selected="selected"{/if}>{lng p="block"}</option>
					
					<optgroup label="{lng p="move"} {lng p="moveto"}">
					{foreach from=$dropdownFolderList key=dFolderID item=dFolderTitle}
					<option value="{$dFolderID}" style="font-family:courier;"{if $spamAction==$dFolderID} selected="selected"{/if}>{$dFolderTitle}</option>
					{/foreach}
					</optgroup>
				</select>
			</td>
		</tr>
		<!-- SPAM-Report start -->
		{if $asp==1}
		<tr>
			<td class="listTableLeft"><label for="sBericht">{lng p="sBericht"}</label></td>
			<td class="listTableRight">
				<select name="wBericht">
					<option value="1"{if $sBericht==1} selected="selected"{/if}>&nbsp;{lng p="sAktT"}&nbsp;</option>
					<option value="2"{if $sBericht==2} selected="selected"{/if}>&nbsp;{lng p="sAktW"}&nbsp;</option>
					<option value="0"{if $sBericht==0} selected="selected"{/if}>&nbsp;{lng p="sDeakt"}&nbsp;</option>
				</select>
			</td>
		</tr>
		{/if}
		<!-- SPAM-Report ende -->
		<!-- Whitlist-Pro start -->
		{if $wlproGrp}
		<tr>
			<td class="listTableLeft"><label for="wlpro">Whitelist:</label></td>
			<td class="listTableRight">
				<input type="checkbox" name="wlproStatus" id="wlproStatus"{if $wlproStatus==1} checked="checked"{/if} /> <b>Aktivieren</b>
				<div style="padding:2px 0px 2px 0px;"><small><b>Hinweis:</b> Bei aktivierter Whitelist-Option k&ouml;nnen Sie nur noch E-Mails von den Absendern empfangen, welche sich im <a href="organizer.addressbook.php?sid={$sid}">Adressbuch</a> befinden!</small></div>
			</td>
		</tr>
		{/if}
		<!-- Whitlist-Pro ende -->
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" value="{lng p="ok"}" />
				<input type="reset" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>

{if $localMode}
<br />
<form name="f1" method="post" action="prefs.php?action=antispam&do=resetDB&sid={$sid}">
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="spamindex"}</th>
		</tr>
		<tr>
			<td class="listTableLeft">{lng p="entries"}:</td>
			<td class="listTableRight">
				{$dbEntries}
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" value="{lng p="resetindex"}"{if $dbEntries==0} disabled="disabled"{/if} /><br />
				<small>{lng p="resetindextext"}</small>
			</td>
		</tr>
	</table>
</form>
{/if}
