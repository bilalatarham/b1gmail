<div id="security-overlay">
	<div id="security-content">
		<h1>{lng p="security_autologout"}</h1>
	
		{lng p="security_desc"}
		<div id="security-countdown" class="running"></div>
		<div id="security-actions">
			<button id="security-keep">{lng p="security_extendsession"}</button>
			<button id="security-logout" onclick="document.location.href='start.php?sid={$sid}&action=logout'">{lng p="logout"}</button>
		</div>
	</div>
</div>