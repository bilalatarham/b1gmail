<fieldset>
	<legend>Potenzielle Spammer</legend>	
	{if !$spammers}
		Das System konnte keine Spammer ermitteln.
	{else}
		<table class="list">
			<tbody>
				<tr>
					<th>Nutzer-ID</th>
					<th>Haupt-E-Mail</th>
					<th>Versendete Mails</th>
					<th>Nutzer gesperrt?</th>
					<th>Zur Whitelist hinzufügen</th>
				</tr>
				{foreach from=$spammers key=id item=value}
				{cycle name=class values="td1,td2" assign=class}
					<tr class="{$class}">
						<td>{$id}</td>
						<td>{$value.email}</td>
						<td>{$value.sent_mails}</td>
						<td>{if $value.gesperrt == "no"}{lng p="active"}{elseif $value.gesperrt == "yes"}{lng p="locked"}{elseif $value.gesperrt == "locked"}{lng p="notactivated"}{elseif $value.gesperrt == "delete"}{lng p="deleted"}{/if} </td>
						<td><a href="{$pluginURL}action=add&whitelist_add={$id}&sid={$sid}"><img border="0" align="absmiddle" src="templates/images/cert_ok.png"></a></td>
					</tr>
				{/foreach}		
			</tbody>
		</table>
	{/if}
</fieldset>