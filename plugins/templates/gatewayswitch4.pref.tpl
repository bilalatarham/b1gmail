<fieldset>
	<legend>{lng p="gatewayswitch_name"}</legend>
	
	<table>
		<tr>
			<td width="48"><img src="../plugins/templates/images/gatewayswitch_logo.png" width="48" height="48" border="0" alt="" /></td>
			<td width="10">&nbsp;</td>
			<td><b>{lng p="gatewayswitch_name"}</b><br />{lng p="gatewayswitch_text"}</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend>Wof&uuml;r ist die Priorit&auml;t und was muss ich dort eintragen?</legend>

	<table>
	<tr>
		<td class="td2">Der Eintrag f&uuml;r Priorit&auml;t legt die Reihenfolge der Gateways fest.<br />
		Genau in dieser Reihenfolge werden die Gateways gewechselt.<br />
		<b>Es ist sehr wichtig, dass die Nummerierung durchgehend und ohne L&uuml;cken erfolgt, da es sonst zu fehlern kommt.</b></td>
	</tr>
</table>
</fieldset>

<fieldset>
	<legend>Wie rufe ich das Script auf?</legend>

	<table>
	<tr>
		<td class="td2">Das Script wird jedes mal aufgerufen, wenn cron.php im Hauptverzeichnis aufgerufen wird.<br />
		Au&szlig;erdem k&ouml;nnen Sie es ausf&uuml;hren indem Sie die "index.php?action=gatewayswitch" im Hauptverzeichnis von B1gMail aufrufen.<br />
		Sie k&ouml;nnen den Gateway auch manuell unter Gateways &auml;ndern</td>
	</tr>
	</table>
</fieldset>

<fieldset>
	<legend>Wechseln beim Cronjob zulassen?</legend>

	<table>
	<tr>
		<td class="td2">Der POP3-Gateway wird jedes mal ge&auml;ndert, wenn cron.php im Hauptverzeichnis aufgerufen wird.<br />
		Diese Funktion kann deaktiviert werden. Dadurch wird der POP3-Gateway nicht mehr regelm&auml;&szlig;ig ge&auml;ndert.</td>
	</tr>
	</table>
</fieldset>

<fieldset>
	<legend>Wechseln auf der Seite zulassen?</legend>

	<table>
	<tr>
		<td class="td2">Der POP3-Gateway wird jedes mal ge&auml;ndert, wenn Sie die "index.php?action=gatewayswitch" im Hauptverzeichnis von B1gMail aufrufen.<br />
		Diese Funktion kann deaktiviert werden. Dadurch kann der POP3-Gateway nicht mehr manuell (ohne Admin Rechte) oder von anderen Scripten ge&auml;ndert werden.</td>
	</tr>
	</table>
</fieldset>

	<fieldset>
	<legend>Gateway manuell &auml;ndern</legend>

	<table>
	<tr>
		<td class="td2">Wenn Sie einen bestimmten Gateway einstellen wollen, k&ouml;nnen Sie dies &uuml;ber den Punkt "Gateway Bearbeiten" unter "Gateways" machen.<br />
		Tragen Sie daf&uuml;r einfach die Nummer der Reihenfolge des Gateways ein und starten den Prozess.<br/>
		Unter "POP3-Gateway" rufen Sie danach die Mails mit dem aktuellem Gateway ab.</td>
	</tr>
	</table>
</fieldset>