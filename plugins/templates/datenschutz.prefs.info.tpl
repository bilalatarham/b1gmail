<div class="contentHeader">
    <div class="left">
        <img src="plugins/templates/images/datenschutz.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="dsTitel"}
    </div>
</div>

<form name="f1" method="post" action="prefs.php?action=ds&sid={$SID}">
    <div class="scrollContainer">
        <div class="pad">

            {$MSG}

            {if $AVArr|is_array}
                <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #333; background-color: #f5f5f5; border-color: #ddd;">
                    <strong>{lng p="dsAVModalTitel"}</strong><br />
                    {foreach from=$AVArr item=av}
                        {lng p="dsAVModalDatum"} {$av.Date}<br /><br />
                        {if $av.Dokument}<button type="button" onclick="document.location.href='prefs.php?action=ds&do=avdownload&id={$av.ID}&sid={$SID}';"><img src="{$tpldir}images/li/ico_download.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="dsAV"} ({$av.DokumentSize})</button>{/if}
                    {/foreach}
                </div>
            {/if}

            {if $AufgabenArr|is_array}
                <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #333; background-color: #f5f5f5; border-color: #ddd;">
                    <strong>{lng p="dsPrefsAuswahl"}</strong><br />
                    {foreach from=$AufgabenArr item=aufgabe}
                        {if $aufgabe.Status > 0}
                            {text value=$aufgabe.StatusText}
                        {else}
                            {$InfoMSG}
                            <br /><br />
                            {lng p="dsAVModalDatum"} {$aufgabe.JobUpdate}
                            <br /><br />
                            <button type="button" onclick="document.location.href='prefs.php?action=ds&do=download&id={$aufgabe.ID}&sid={$SID}';"><img src="{$tpldir}images/li/ico_download.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="dsReport"} ({$aufgabe.DokumentSize})</button>
                            &nbsp;<a onclick="return confirm('{lng p="dsLoeschenNachricht"}');" href="prefs.php?action=ds&do=renew&id={$aufgabe.ID}&sid={$SID}" style="margin: 2px; border: 1px solid rgba(0, 0, 0, 0.3); padding: 4px; color: #444; text-decoration: none; -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px; cursor: pointer; background: none;"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="msBTNLoeschen"}" align="absmiddle" /> {lng p="dsLoeschen"}</a>
                        {/if}
                    {/foreach}
                </div>
            {else}
                <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #31708f; background-color: #d9edf7; border-color: #bce8f1;">
                    <strong>Report</strong><br />
                    {$InfoMSG}
                </div>

                <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #333; background-color: #f5f5f5; border-color: #ddd;">
                    <strong>{lng p="dsPrefsAuswahl"}</strong><br />
                    <ul>
                        <li>{lng p="dsPrefsProfile"}: {lng p="dsPrefsProfileBeschr"}</li>
                        <li>{lng p="dsPrefsReg"}: {lng p="dsPrefsRegBeschr"}</li>
                        <li>{lng p="dsPrefsLWeb"}: {lng p="dsPrefsLWebBeschr"}</li>
                        <li>{lng p="dsPrefsLPOP"}: {lng p="dsPrefsLPOPBeschr"}</li>
                        <li>{lng p="dsPrefsLIMAP"}: {lng p="dsPrefsLIMAPBeschr"}</li>
                        <li>{lng p="dsPrefsLSMTP"}: {lng p="dsPrefsLSMTPBeschr"}</li>
                        <li>{lng p="dsDaten"}: {lng p="dsPrefsDatenBeschr"}</li>
                        <li>{lng p="dsModule"}: {lng p="dsModuleBeschr"}</li>
                        <li>{lng p="dsInfoGes"}: {lng p="dsInfoGesBeschr"}</li>
                    </ul>
                    <input type="submit" name="speichern" class="primary" value="{lng p="dsDokumentErstellen"}" />
                </div>
            {/if}
        </div>
    </div>

</form>