<div id="contentHeader">
    <div class="left">
        <img src="plugins/templates/images/mailsync.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="ms_add"}
    </div>
</div>

<div class="scrollContainer">
    <div class="pad">


        {if $ShowMSG}
            {$ShowMSG}
        {else}
            <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6;">
                {lng p="msInfo"}
            </div>
        {/if}

        <form name="f1" method="post" action="prefs.php?action=ms&page=add&provider={$SourceProvider}&sid={$SID}">
            <table class="listTable">
                <tr>
                    <th class="listTableHead" colspan="2"> {lng p="msAddMail"}</th>
                </tr>
                <tbody id="tbody_1">
                <tr>
                    <td class="listTableLeftDesc"><img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" /></td>
                    <td class="listTableRightDesc" style="border-top: 0px;">{lng p="msSourceTitle"}</td>
                </tr>
                <tr>
                    <td class="listTableLeft"><label for="email">{lng p="msProvider"}:</label></td>
                    <td class="listTableRight">
                        <select class="form-control" onchange="location = this.value;" style="width: 277px;">
                            <option value="prefs.php?action=ms&page=add&&sid={$SID}">--- {lng p="msSelectProvider"} ---</option>
                            <optgroup label="{lng p="msProvider"}">
                                {foreach from=$ProviderList item=pl}
                                <option value="prefs.php?action=ms&page=add&provider={$pl.ID}&sid={$SID}" {if $SourceProvider == $pl.ID}selected{/if}>{$pl.Name}</option>
                                {/foreach}
                            </optgroup>
                            <optgroup label="{lng p="msNoProvider"}">
                                <option value="prefs.php?action=ms&page=add&provider=noprov&sid={$SID}" {if $SourceProvider == 'noprov'}selected{/if}>{lng p="msSelectOwnProvider"}</option>
                            </optgroup>
                        </select>
                    </td>
                </tr>
                {if $SourceProvider}
                    <tr>
                        <td class="listTableLeft"><label for="sourcemail">{lng p="msMailAdresse"}:</label></td>
                        <td class="listTableRight">
                            <input type="email" name="sourcemail" id="sourcemail" value="{$SourceMail}" size="34" required /><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="listTableLeft"><label for="sourcepw">{lng p="msPasswort"}:</label></td>
                        <td class="listTableRight">
                            <input type="password" name="sourcepw" id="sourcepw" value="{$SourcePW}" size="34" required /><br />
                        </td>
                    </tr>
                {/if}
                {if $SourceProvider == 'noprov'}
                    <tr>
                        <td class="listTableLeft"><label for="sourceserver">{lng p="msMailServer"}:</label></td>
                        <td class="listTableRight">
                            <input type="text" name="sourceserver" id="sourceserver" value="{$SourceServer}" size="34" required /><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="listTableLeft"><label for="sourceport">{lng p="msPort"}:</label></td>
                        <td class="listTableRight">
                            <input type="text" name="sourceport" id="sourceport" value="{$SourcePort}" size="34" required /><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="listTableLeft"><label for="sourcecert">{lng p="msCert"}:</label></td>
                        <td class="listTableRight">
                            <select name="sourcecert" class="form-control formred" style="width: 277px;">
                                <option value="" {if $SourceCert == ''}selected{/if}>IMAP ({lng p="msNotEncrypted"})</option>
                                <option value="tls" {if $SourceCert == 'tls'}selected{/if}>IMAP + STARTTLS ({lng p="msEncrypted"})</option>
                                <option value="ssl" {if $SourceCert == 'ssl'}selected{/if}>IMAP + SSL ({lng p="msEncrypted"})</option>
                            </select>
                        </td>
                    </tr>
                {else}
                    <input type="hidden" name="sourceserver" id="sourceserver" value="{$SourceServer}" size="34" />
                    <input type="hidden" name="sourceport" id="sourceport" value="{$SourcePort}" size="34" />
                    <input type="hidden" name="sourcecert" id="sourcecert" value="{$SourceCert}" size="34" />
                {/if}
                <tr>
                    <td class="listTableLeftDesc"><img src="{$tpldir}images/li/menu_ico_inbox.png" width="16" height="16" border="0" alt="" /></td>
                    <td class="listTableRightDesc">{$TargetTitel}</td>
                </tr>
                <tr>
                    <td class="listTableLeft"><label for="sourcepw">{lng p="msPasswort"}:</label></td>
                    <td class="listTableRight">
                        <input type="password" name="targetpw" id="targetpw" value="{$TargetPW}" size="34" required /><br />
                    </td>
                </tr>
                <tr>
                    <td class="listTableLeft"><label for="targetfolder">{lng p="msOrdner"}:</label></td>
                    <td class="listTableRight">
                        <input type="text" name="targetfolder" id="targetfolder" value="{$TargetFolder}" size="34" required /><br />
                    </td>
                </tr>
                </tbody>
                <tr>
                    <td class="listTableLeft">&nbsp;</td>
                    <td class="listTableRight">
                        <input type="submit" name="add" value="{lng p="msSpeichern"}" />
                    </td>
                </tr>

            </table>
        </form>

    </div>
</div>

<div id="contentFooter">
    <div class="right">
        <button type="button" onclick="document.location.href='prefs.php?action=ms&page=list&sid={$SID}';">
            <img src="{$tpldir}images/li/ico_menu.png" width="16" height="16" border="0" alt="" align="absmiddle" />
            {lng p="msZuruck"}
        </button>
    </div>
</div>
