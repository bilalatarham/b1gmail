<style type="text/css">
{literal}
.listTBody td {
	text-align: center;
}
{/literal}
</style>

<div id="contentHeader">
	<div class="left">
		<img src="plugins/templates/images/mb_app_16.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="mb_app"}
	</div>
</div>

<div class="scrollContainer">
	<table class="bigTable">
		<tr>
			<th>
				{lng p="mb_app_prefspage_platform"}
			</th>
			<th>
				{lng p="mb_app_prefspage_added"}
			</th>
			<th>
				{lng p="mb_app_prefspage_lastaccessed"}
			</th>
			<th>
				{lng p="mb_app_prefspage_push"}
			</th>
			<th>{lng p="mb_app_delete"}</th>
		</tr>

		{if $appaccounts}
			<tbody class="listTBody">
				{foreach from=$appaccounts key=key item=item}
				{cycle values="listTableTD,listTableTD2" assign="class"}
				<tr>
					<td class="{$class}" nowrap="nowrap">
						{if $item.platform == "android"}
							{lng p="mb_app_prefspage_os_android"} <i class="fa fa-android"></i>
						{elseif $item.platform == "ios"}
							{lng p="mb_app_prefspage_os_ios"} <i class="fa fa-apple"></i>
						{else}
							{lng p="mb_app_prefspage_os_other"}
						{/if}
					</td>
					<td class="{$class}" nowrap="nowrap">
						{date timestamp=$item.added elapsed=true}
						<br/>
						<small>
							{lng p="mb_app_prefspage_ip"}: {text value=$item.ipAdded}
						</small>
					</td>
					<td class="{$class}" nowrap="nowrap">
						{date timestamp=$item.lastaccessed elapsed=true}
						<br/>
						<small>
							{lng p="mb_app_prefspage_ip"}: {text value=$item.ipLastaccessed}
						</small>
					</td>
					<td class="{$class}" nowrap="nowrap">
						<img src="{$tpldir}images/li/{if $item.pushactive == "yes"}yes{else}no{/if}.png" width="16" height="16" border="0" align="absmiddle" />
					</td>
					<td class="{$class}" nowrap="nowrap">
						<a onclick="return confirm('{lng p="realdel"}');" href="prefs.php?action=mb_app&do=delete&id={$item.id}&sid={$sid}" title="{lng p="delete"}">
							<img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="delete"}" align="absmiddle" />
						</a>
					</td>
				</tr>
				{/foreach}
			</tbody>
		{/if}
	</table>
</div>
