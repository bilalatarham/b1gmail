<table class="nliTable">
	<!-- login -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/login.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="login"}</h3>
			
			<form action="{if $ssl_login_enable||($welcomeBack&&$smarty.cookies.bm_savedSSL)}{$ssl_url}{/if}index.php?action=login" method="post" id="loginForm">
			<input type="hidden" name="do" value="login" />
			<input type="hidden" name="timezone" id="timezone" value="{$timezone}" />
			
			{if $welcomeBack}
			
			<input type="hidden" name="email_full" value="{$smarty.cookies.bm_savedUser}" />
			<input type="hidden" name="passwordMD5" value="{$smarty.cookies.bm_savedPassword}" />
			<input type="hidden" name="language" value="{$smarty.cookies.bm_savedLanguage}" />
			<input type="hidden" name="savelogin" value="true" />
			{if $smarty.cookies.bm_savedSSL}<input type="hidden" name="ssl" value="true" />{/if}
			
			{$welcomeBack}
			
			<center>
				<br />
				<input type="submit" value=" &nbsp; {lng p="login"} &nbsp; " />
				<input type="button" onclick="location.href='index.php?action=forgetCookie';" value=" &nbsp; {lng p="otheruser"} &nbsp; " />
			</center>
			
			{else}
			<table>
				<tr>
					<td class="formCaption"><label for="email_local">{lng p="email"}:</label></td>
					<td class="formField">
						{if $domain_combobox}
						<input type="text" name="email_local" size="30" id="email_local" />
						<select name="email_domain">
							{foreach from=$domainList item=domain}<option value="{$domain}">@{$domain}</option>{/foreach}
						</select>
						{else}
						<input type="text" name="email_full" size="45" id="email_local" />
						{/if}
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="password">{lng p="password"}:</label></td>
					<td class="formField">
						<input type="password" name="password" id="password" size="30" />
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="language">{lng p="language"}:</label></td>
					<td class="formField">
						<select name="language" id="language">
							{foreach from=$languageList item=langInfo key=langKey}<option value="{$langKey}"{if $langInfo.active} selected="selected"{/if}>{$langInfo.title}</option>{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="savelogin">{lng p="savelogin"}?</label></td>
					<td class="formField">
						<input type="checkbox" id="savelogin" name="savelogin"{if $saveLogin} checked="checked"{/if} />
					</td>
				</tr>
				{if $ssl_login_option}<tr>
					<td class="formCaption"><label for="ssl">{lng p="ssl"}?</label></td>
					<td class="formField">
						<input type="checkbox" id="ssl" name="ssl" {if $ssl_login_enable} checked="checked"{/if} onclick="updateLoginSSL('{$ssl_url}',this.checked);" />
					</td>
				</tr>{/if}
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">
						<input type="submit" value=" &nbsp; {lng p="login"} &nbsp; " />
					</td>
				</tr>
			</table>
			{/if}
			
			</form>
		
			<br />
		</td>
	</tr>
	
	<!-- lost password -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/lostpw.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="lostpw"}?</h3>
			
			<form action="index.php?action=lostPassword" method="post">
			
			<table>
				<tr>
					<td class="formCaption"><label for="email_local_pw">{lng p="email"}:</label></td>
					<td class="formField">
						{if $domain_combobox}
						<input type="text" name="email_local" size="30" id="email_local_pw" />
						<select name="email_domain">
							{foreach from=$domainList item=domain}<option value="{$domain}">@{$domain}</option>{/foreach}
						</select>
						{else}
						<input type="text" name="email_full" size="45" id="email_local_pw" />
						{/if}
					</td>
				</tr>
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">
						<input type="submit" value=" &nbsp; {lng p="requestpw"} &nbsp; " />
					</td>
				</tr>
			</table>
			
			</form>
		
			<br />
		</td>
	</tr>
    
    <!-- lost password via sms -->
	{if $smspw}
	<tr>
		<td>&nbsp;</td>
		<td class="nliTD">
					
			<h3></h3>
			
			<form id="smspwlost" action="index.php?action=lostPassword&do=sendsmspw" method="post">
			
			<table>
				<tr>
					<td colspan="2" class="formField" style="padding:0px 0px 0px 0px;">
						<div align="justify">
							{$smsmsgtxt}<br /><br />
						</div>
					</td>
				</tr>
				<tr>
					<td class="formCaption"><label for="sms-nummer">{lng p="smspwnr"}:</label></td>
					<td class="formField">
						<input type="text" name="smspwnr" size="30" id="smspwnr" />
					</td>
				</tr>
				<tr>
					<td class="formCaption">&nbsp;</td>
					<td class="formField">
						<input type="submit" value=" &nbsp; {lng p="requestpw"} &nbsp; " />
					</td>
				</tr>
			</table>
			
			</form>
		
			<br />
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
		{if $msgok}
			<div style="color:#0000FF; text-align:center; line-height:1.3em; width:95%; border: 1px solid #0000FF; padding:3px 5px 3px 5px;">{$msgok}</div>
		{elseif $msgerror}
			<div style="color:#FF0000; text-align:center; line-height:1.3em; width:95%; border: 1px solid #FF0000; padding:3px 3px 3px 3px;">{$msgerror}</div>
		{/if}
		</td>
	</tr>
	{/if}
	
	<!-- sign up -->
	<tr>
		<td class="nliIconTD"><img src="{$tpldir}images/main/signup.gif" border="0" alt="" /></td>
		<td class="nliTD">
					
			<h3>{lng p="notmember"}?</h3>
			
			{lng p="notmembertxt"} 
		
			<br /><br />
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
<!--
	EBID('timezone').value = clientTZ;
{if $invalidFields}
{foreach from=$invalidFields item=field}
	markFieldAsInvalid('{$field}');
{/foreach}
{/if}
//-->
</script>
