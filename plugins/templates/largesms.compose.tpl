<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_composesms.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="sendlargesms"}
	</div>
</div>

<div class="scrollContainer"><div class="pad">
<form name="f1" method="post" action="sms.php?action=largesms&do=sendLargeSMS&sid={$sid}">
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="sendlargesms"}</th>
		</tr>
		{if $eigSMSabs == 'yes'}
		<tr>
			<td class="listTableLeft">{if $ownFrom}* {/if}{lng p="from"}:</td>
			<td class="listTableRight">
				<input type="text" style="width:175px; background-color: #ebebe4; border:1px solid #7f9db9;" value="{if !$ownFrom}{text value=$smsFrom}{else}{mobileNr name="from" value=$smsFrom size="350px"}{/if}" readonly="readonly">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="prefs.php?action=contact&sid={$sid}">{lng p="changeAbs"}</a>
			</td>
		</tr>
		{/if}
		<tr>
			<td class="listTableLeftDescBottomLine">* {lng p="to"}:</td>
			<td class="listTableRightDesc">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="364">
							{mobileNr name="to" size="350px"}
						</td>
						<td>
							<span id="addrDiv_to">
								<a href="javascript:openCellphoneAddressbook('{$sid}')">
									<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
									{lng p="fromaddr"}
								</a>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="listTableCompose" colspan="2">
				<textarea class="composeTextarea" name="LargeSmsText" id="LargeSmsText" style="width:100%;height:180px;" onkeyup="updateMaxCharsLargeSMS(this,{$maxChars},{$SMSanz},{$SigZei})"></textarea>
			</td>
		</tr>
		
		<tr>
			<td class="listTableLeftDescTopLine">{lng p="chars"}:</td>
			<td class="listTableRightDesc">
				<div style="float:left; padding-top: 2px;">
					{progressBar value=0 max=$maxChars width=100 name="charCountBar"}
				</div>
				<div style="float:left">
					&nbsp;&nbsp;
				</div>
				<div style="float:left" id="charCountDiv">
					0 / {$maxChars}
				</div>
				<div style="float:left">
					&nbsp;&nbsp;&bull;&nbsp;&nbsp;
				</div>
				<div style="float:left">
					{lng p="largesmsanz"}
				</div>
				<div style="float:left">
					&nbsp;
				</div>
				<div style="float:left" id="anzCountDiv">
					{$SMSanz}
				</div>
				<div style="float:left">
					&nbsp;&bull; {lng p="powersmsakt_preis"}:&nbsp;
				</div>
				<div style="float:left" id="charPreisDiv">
					0 Credits
				</div>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding: 5px 5px 5px 5px;">
				<input type="button" value="{lng p="send2sms"}" id="sendButton" onclick="if(!checkLargeSmsComposeForm()) return(false); document.forms.f1.submit();" {if $accCredits==false} disabled="disabled"{/if}/>
				<input type="reset" value="{lng p="reset"}" onclick="return askReset();"/>
				{if $accCredits==false}<div class="note" id="errorMsg2">{lng p="largesmspricewarning"}</div>{/if}
				<div class="note" id="errorMsg" style="display:none;">{lng p="largesmspricewarning"}</div>
			</td>
		</tr>
		<tr>
			<td class="listTableRightDesc" colspan="2" style="padding: 3px 0px 3px 5px;"><small>{lng p="largesmsinfo"}</small></td>
		</tr>
	</table>
</form>

{literal}
<script language="javascript">
<!--
	function checkLargeSmsComposeForm()
	{
		if((EBID('to') && EBID('to').value.length < 3)
			|| (EBID('to_no') && EBID('to_no').value.length < 3)
			|| EBID('LargeSmsText').value.length < 3)
		{
			alert(lang['fillin']);
			return(false);
		}
		return(true);
	}
	
	// fehlermeldung anzeigen, wenn guthaben nicht ausreicht
	function dispHandle(obj, statusWert)
	{
		if(statusWert == true)
			obj.style.display = "";
		else
			obj.style.display = "none";
	}

	function updateMaxCharsLargeSMS(field, maxChars, SMSanz, SigZei)
	{
		var length = field.value.length;
		
		// crop, if needed
		if(length > maxChars)
			field.value = field.value.substring(0, maxChars);
		length = field.value.length;
	
		// SMS-Anzahl ermitteln
		var anz = 0;

		if(length == 0)
			anz = 0;
		else if(length > 0 && length <= 160-SigZei)
			anz = 1;
		else if(length > 160-SigZei && length <= 306-SigZei)
			anz = 2;
		else if(length > 306-SigZei && length <= 459-SigZei)
			anz = 3;
		else if(length > 459-SigZei && length <= 612-SigZei)
			anz = 4;
		else if(length > 612-SigZei && length <= 765-SigZei)
			anz = 5;
		else if(length > 765-SigZei && length <= 918-SigZei)
			anz = 6;
		else if(length > 918-SigZei && length <= 1071-SigZei)
			anz = 7;
		else if(length > 1071-SigZei && length <= 1224-SigZei)
			anz = 8;
		else if(length > 1224-SigZei && length <= 1377-SigZei)
			anz = 9;
		else if(length > 1377-SigZei && length <= 1530-SigZei)
			anz = 10;
	
		// js-variablen bekommen die werte von den tpl-variablen zugewiesen
		var creditwert = {/literal}{$creditwert}{literal};
		var accBalance = {/literal}{$accBalance}{literal};
		
		// aktuellen sms-preis berechnen
		smsPrice = anz*creditwert;
		
		// abfrage, ob credit ausreicht und sperrung/entsperrung des sende buttons
		if(accBalance < (creditwert*anz))
		{
			// fehlermeldung f�r zu wenig guthaben nicht anzeigen
			document.getElementById('sendButton').disabled=true;
			statusWert = true;
		}
		else
		{
			// fehlermeldung f�r zu wenig guthaben anzeigen
			document.getElementById('sendButton').disabled=false;
			statusWert = false;
		}
		
		// sende-button ausblenden, wenn anz=0 und zu wenig credits vorhanden sind
		if(anz == 0 && accBalance < creditwert)
			document.getElementById('sendButton').disabled=true;
			
		// doppelte fehlermeldung ein-/ausblenden
		if(accBalance < creditwert)
		{
			if(anz == 0)
				document.getElementById('errorMsg2').style.display = "";
			else
				document.getElementById('errorMsg2').style.display = "none";
		}
		
		// status�bergabe f�r fehlermeldung ein-/ausblenden
		dispHandle(document.getElementById('errorMsg'), statusWert);
			
		// update SMS-Anzahl
		EBID('anzCountDiv').innerHTML = anz;
		
		// update text
		EBID('charCountDiv').innerHTML = length + ' / ' + maxChars;
		
		// update aktuelle sms-kosten
		EBID('charPreisDiv').innerHTML = smsPrice + ' Credits';
		
		// progressbar width?
		var pbWidth = parseInt(EBID('pb_charCountBar').style.width);
		var newValueWidth = Math.ceil(pbWidth/maxChars * length);
		if(newValueWidth > pbWidth-2)
			newValueWidth = pbWidth-2;
		EBID('pb_charCountBar_value').style.width = ((newValueWidth == 0) ? 1 : newValueWidth) + 'px';
		
		//alte version
		//var pbWidth = 0;
		//if(isNaN(parseInt(EBID('pb_charCountBar_value').width)))
		//	pbWidth = parseInt(EBID('pb_charCountBar_free').width);
		//else
		//	pbWidth = parseInt(EBID('pb_charCountBar_value').width) + parseInt(EBID('pb_charCountBar_free').width);
		//var newValueWidth = Math.ceil(pbWidth/maxChars * length),
		//	newFreeWidth = pbWidth - newValueWidth;
		//EBID('pb_charCountBar_free').width = newFreeWidth;
		//EBID('pb_charCountBar_value').width = newValueWidth == 0 ? 1 : newValueWidth;
	}
	
	function openCellphoneAddressbook(sid)
	{	
		openOverlay('organizer.addressbook.php?sid=' + sid + '&action=numberPopup',
			lang['addressbook'],
			450,
			380,
			true);
	}
	
//-->
</script>
{/literal}
</div></div>