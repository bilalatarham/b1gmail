<div class="innerWidget">
	{* <table cellspacing="0" cellpadding="2" width="100%">
		<tr>
			<td align="center">{progressBar value=$bmwidget_mailspace_spaceUsed max=$bmwidget_mailspace_spaceLimit width=250}</td>
		</tr>
		<tr>
			<td>{size bytes=$bmwidget_mailspace_spaceUsed} / {size bytes=$bmwidget_mailspace_spaceLimit} {lng p="used"}</td>
		</tr>
	</table> *}
	{assign var="percentage" value=$bmwidget_mailspace_spaceUsed/$bmwidget_mailspace_spaceLimit}

	<div class="dragable-box content-box" percentage={math equation="x * y" x=$percentage y=100}>
		<div class="widget-sp-email">
			<div class="widget-icon">
				<img src="{$tpldir}frontend_assets/img/ic-space-email.svg" class="img-fluid" />
			</div>
			<p class="main-title fw-med">Space</p>
			<p class="sub-title fw-sbold">Email</p>
			<div class="progress-container text-center">
				<p class="progress-value">{size bytes=$bmwidget_mailspace_spaceUsed} / {size bytes=$bmwidget_mailspace_spaceLimit} {lng p="used"}</p>
				<div class="progress">
					<div class="progress-bar" role="progressbar" used="{$bmwidget_mailspace_spaceUsed}" total="{$bmwidget_mailspace_spaceLimit}" style="width: {math equation="x * y" x=$percentage y=100}%" aria-valuenow="{math equation="x * y" x=$percentage y=100}" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
		</div>
	</div>
</div>