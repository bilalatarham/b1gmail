<div style="background-color:#dedede; padding:1px 10px 7px 10px; -moz-border-radius:10px;">
<div style="border-bottom: 1px solid grey;"><h1>{$folderTitel}</h1></div>
<br />

<table width="100%" align="center" class="wdIconTable">
	<tr>
		{assign var="i" value=0}
		{foreach from=$images item=element}
		{assign var="i" value=$i+1}
		{if $i==8}
		{assign var="i" value=1}
	</tr>
	<tr>
		{/if}
			<td style="vertical-align:bottom; text-align:center;" class="webdiskItem">
				<a id="wli_{$element.type}_{$element.id}" href="index.php?action=getpicfile&picid={$element.id}&pw={$pw}" rel="lightbox[galerie]" title="{$element.title}">
					<img src="index.php?action=getthumbfile&picid={$element.id}&pw={$pw}" border="0" width="100px" heigth="100px"/><br />{text value=$element.title cut=13 escape=true}
				</a><br /><br />
			</td>
			{if $i==8}
			</tr>
			<tr>
			{/if}
		{/foreach}
		{if $i==0}
			<td><div>&bull; {lng p="webdiskgalerie_nopic"}</div></td>
		{/if}
		{if $i<8 && $i!=0}
		{math assign="i" equation="x - y" x=7 y=$i}
		{section loop=$i name=rest}
			<td>&nbsp;</td>
		{/section}
		{/if}
	</tr>
</table>
<br />

{if $picscount >= 2}
<div style="border-top: 1px solid grey; padding:5px 0px 3px 3px;"><img src="./plugins/templates/images/gal_play.png" border="0" align="top" /><a href="index.php?action=slideshow&gal={$gal}&pw2={$pw}" target="_blank" style="padding:5px 0px 3px 8px;"><b>{lng p="webdiskgalerie_sls"}</b></a></div>
{else}
<div style="border-bottom: 1px solid grey;"></div>
{/if}

{if $folders_count > 0}
<table width="100%" class="wdIconTable">
	{assign var="i" value=0}
	{foreach from=$folders item=item}
	{assign var="i" value=$i+1}
	<tr>
		<td class="webdiskItem" style="width:20px;">
			<img src="{$tpldir}images/li/webdisk_folder.png">
		</td>
		<td class="webdiskItem">
			<a id="wli_{$item.type}_{$item.id}" title="{text value=$item.title}" href="index.php?action=galerie&gal={$item.id}">{text value=$item.title cut=50}</a>
		</td>
	</tr>
	{/foreach}
</table>
	{if $parentGal}<div style="border-top: 1px solid grey;"></div>{/if}
{/if}
{if $parentGal}
<table width="100%" class="wdIconTable" style="padding:5px 0px 0px 0px;">
	<tr>
		<td align="left">
			<a href="index.php?action=galerie&gal={$parentID}"><b>&laquo; {lng p="back"} </b></a>
		</td>
	</tr>
</table>
{/if}
</div>
