<form action="plugin.page.php?plugin=briefkarte&action=bkbgateways&do=save&sid={$sid}" method="post" onsubmit="spin(this)">
<fieldset>
	<legend>{lng p="bk_brief"} - {lng p="prefs"}</legend>
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="28"><img src="../plugins/templates/images/modbriefkarte32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="250">{lng p="bk_gw"}:</td>
				<td class="td2"><textarea style="width:85%;" id="brief_gw" name="brief_gw" rows="1">{text value=$brief_gw allowEmpty=true}</textarea></td>
			</tr>
			<tr>
				<td class="td1">{lng p="user"}:</td>
				<td class="td2"><input type="text" size="36" id="brief_user" name="brief_user" value="{text value=$brief_user allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="password"}:</td>
				<td class="td2"><input type="password" autocomplete="off" size="36" id="brief_pw" name="brief_pw" value="{text value=$brief_pw allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
            <tr>
				<td class="td1">{lng p="bk_preis_de_s1b2"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_s1u2" name="bk_preis_s1u2" value="{text value=$brief_preis_s1u2 allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_jws"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_ab3s" name="bk_preis_ab3s" value="{text value=$brief_preis_ab3s allowEmpty=true}" /> Credits</td>
			</tr>
            <tr>
				<td class="td1">{lng p="bk_preis_brief_es"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_einschreiben" name="bk_preis_einschreiben" value="{text value=$bk_preis_einschreiben allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_brief_es_onl"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_einschreiben_online" name="bk_preis_einschreiben_online" value="{text value=$bk_preis_einschreiben_online allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_brief_es_rs"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_einschreiben_rs" name="bk_preis_einschreiben_rs" value="{text value=$bk_preis_einschreiben_rs allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_postID_gw"}:</td>
				<td class="td2"><textarea style="width:85%;" id="postID_gw" name="postID_gw" rows="1">{text value=$postID_gw allowEmpty=true}</textarea></td>
			</tr>
            <tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="td1"><b>{lng p="bk_p_de"}</b></td>
				<td class="td2">&nbsp;</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_farbe"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_de_farbe" name="bk_preis_de_farbe" value="{text value=$brief_preis_farbe_de allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps1b3"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_de_porto_1b3s" name="bk_preis_de_porto_1b3s" value="{text value=$brief_preis_porto_1b3s_de allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps4b15"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_de_porto_4b15s" name="bk_preis_de_porto_4b15s" value="{text value=$brief_preis_porto_4b15s_de allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps16b99"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_de_porto16b99s" name="bk_preis_de_porto16b99s" value="{text value=$brief_preis_porto16b99s_de allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="td1"><b>{lng p="bk_p_eu"}</b></td>
				<td class="td2">&nbsp;</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_farbe"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_eu_farbe" name="bk_preis_eu_farbe" value="{text value=$brief_preis_farbe_eu allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps1b3"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_eu_porto_1b3s" name="bk_preis_eu_porto_1b3s" value="{text value=$brief_preis_porto_1b3s_eu allowEmpty=true}" /> Credits</td>
			</tr>
            <tr>
				<td class="td1">{lng p="bk_preis_de_ps4b15"}:</td>
				<td class="td2"><input type="text" size="10" id="brief_preis_porto_4b15s_eu" name="brief_preis_porto_4b15s_eu" value="{text value=$brief_preis_porto_4b15s_eu allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps16b99"}:</td>
				<td class="td2"><input type="text" size="10" id="brief_preis_porto_16b99s_eu" name="brief_preis_porto_16b99s_eu" value="{text value=$brief_preis_porto_16b99s_eu allowEmpty=true}" /> Credits</td>
			</tr>
            <tr>
				<td colspan="2">&nbsp;</td>
			</tr>
            <tr>
				<td class="td1"><b>{lng p="bk_p_welt"}</b></td>
				<td class="td2">&nbsp;</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_farbe"}:</td>
				<td class="td2"><input type="text" size="10" id="bk_preis_welt_farbe" name="bk_preis_welt_farbe" value="{text value=$brief_preis_farbe_welt allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps1b3"}:</td>
				<td class="td2"><input type="text" size="10" id="brief_preis_porto_1b3s_welt" name="brief_preis_porto_1b3s_welt" value="{text value=$brief_preis_porto_1b3s_welt allowEmpty=true}" /> Credits</td>
			</tr>
            <tr>
				<td class="td1">{lng p="bk_preis_de_ps4b15"}:</td>
				<td class="td2"><input type="text" size="10" id="brief_preis_porto_4b15s_welt" name="brief_preis_porto_4b15s_welt" value="{text value=$brief_preis_porto_4b15s_welt allowEmpty=true}" /> Credits</td>
			</tr>
			<tr>
				<td class="td1">{lng p="bk_preis_de_ps16b99"}:</td>
				<td class="td2"><input type="text" size="10" id="brief_preis_porto_16b99s_welt" name="brief_preis_porto_16b99s_welt" value="{text value=$brief_preis_porto_16b99s_welt allowEmpty=true}" /> Credits</td>
			</tr>
		</table>
		<p align="right">
			<input type="submit" value=" {lng p="save"} " />
		</p>
</fieldset>
</form>
