{* $Id: pacc.user.invoice.tpl,v 1.2 2008/07/02 09:53:11 patrick Exp $ *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{lng p="pacc_invoice"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script language="javascript">
	<!--
		var tplDir = '{$tpldir}';
	//-->
	</script>
	<script src="clientlang.php" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js" type="text/javascript" language="javascript"></script>
</head>

<body onload="documentLoader(){if $print};window.print();window.close();{/if}" style="background-color:#FFFFFF;">

	{if !$print}
	<div style="position:fixed;top:0px;left:0px;width:100%;background-image:url(plugins/templates/images/pacc_bar.png);padding:6px;">
		<div style="float:left;">
			<a href="javascript:void(0);" onclick="openWindow('prefs.php?action=pacc_mod&do=showInvoice&id={$id}&print=true&sid={$sid}','printInvoice_{$id}',640,480);" style="color:#FFFFFF;text-decoration:none;">
				<img src="{$tpldir}images/li/mail_print.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				<span style="text-decoration:underline;">{lng p="pacc_printinvoice"}</span>
			</a>
		</div>
		<div style="float:right;margin-right:18px;">
			<a href="javascript:parent.hideOverlay();" style="color:#FFFFFF;text-decoration:none;">
				<img src="{$tpldir}images/li/ico_delete.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				<span style="text-decoration:underline;">{lng p="close"}</span>
			</a>
		</div>
	</div>

	<br /><br />
	{/if}
	
	{$invoice}
	
</body>

</html>
