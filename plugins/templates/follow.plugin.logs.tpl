<fieldset>
        <legend>{lng p="logs"} ({date nice=true timestamp=$start} - {date nice=true timestamp=$end})</legend>

        <table class="list">
                <tr>
                        <th width="20">&nbsp;</th>
                        <th>{lng p="follow_lang_user_id"}</th>
                        <th>{lng p="follow_lang_email"}</th>
                        <th>{lng p="follow_lang_betreff"}</th>
                        <th width="150">{lng p="date"}</th>
                </tr>

                {foreach from=$entries item=entry}
                {cycle name=class values="td1,td2" assign=class}
                <tr class="{$class}">
                        <td><img src="../plugins/templates/images/follow_plugin_{$entry.funktion}.png" border="0" alt="" width="16" height="16" /></td>
                        <td><code>{text value=$entry.user}</code></td>
                        <td><code>{text value=$entry.email}</code></td>
                        <td><code>{text value=$entry.vorgang}</code></td>
                        <td>{date nice=true timestamp=$entry.datum}</td>
                </tr>
                {/foreach}
        </table>

</fieldset>

<fieldset>
        <legend>{lng p="filter"}</legend>

        <form action="{$pageURL}&action=logs&sid={$sid}" method="post" onsubmit="spin(this)">
                <table>
                        <tr>
                                <td width="40" valign="top" rowspan="4"><img src="{$tpldir}images/filter.png" border="0" alt="" width="32" height="32" /></td>
                                <td class="td1" width="80">{lng p="from"}:</td>
                                <td class="td2">
                                                {html_select_date prefix="start" time=$start start_year="-5" field_order="DMY" field_separator="."},
                                                {html_select_time prefix="start" time=$start display_seconds=false}
                                </td>
                        </tr>
                        <tr>
                                <td class="td1">{lng p="to"}:</td>
                                <td class="td2">
                                                {html_select_date prefix="end" time=$end start_year="-5" field_order="DMY" field_separator="."},
                                                {html_select_time prefix="end" time=$end display_seconds=false}
                                </td>
                        </tr>
                       <tr>
                                <td class="td1">{lng p="email"}:</td>
                                <td class="td2">
                                                <input type="text" name="q" value="{text value=$q allowEmpty=true}" size="36" style="width:85%;" />
                                </td>
                        </tr>
                </table>

                <p align="right">
                        <input type="submit" value=" {lng p="apply"} " />
                </p>
        </form>
</fieldset>