{$MSG}

<div style="text-align: center; margin: 20px 0px 20px 0px;">
    <img src="../plugins/templates/images/mailsync_logo.png" width="427" height="200" alt="Mail Town E-Mail Service" /><br /><br />
    <strong>Mail Town E-Mail Service</strong><br />
    Plugin Version: {$Version}<br /><br />
</div>

<fieldset>
    <legend>{lng p="msSupport"}</legend>
    Falls es Fragen oder Probleme gibt verwenden Sie bitte nachfolgendes Kontakformular:<br />
    <a href="https://mail.town/kontakt" target="_blank">https://mail.town/kontakt</a>
</fieldset>

<table style="width: 100%; margin: 0px; padding: 0px;">
    <tr style="vertical-align: top;">
        <td style="width: 33%; margin: 0px; padding: 0px;">
            <fieldset style="margin: 0px 10px 0px 0px;">
                <legend>{lng p="msLegende"}</legend>
                {lng p="msLegendeStatus"}

                <table width="100%">
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/mailsync_grey.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="msStatusOFF"}</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/mailsync_yellow.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="msStatusONG"}</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/mailsync_blue.png" width="16" height="16" alt="Status" /></td>
                        <td class="td2">{lng p="msStatusON"}</td>
                    </tr>
                </table>
            </fieldset>
        </td>
        <td style="margin: 0px; padding: 0px;">
            <fieldset style="margin: 0px 0px 0px 10px;">
                <legend>{lng p="msHilfeErweiterungen"}</legend>
                {lng p="msHilfeErweiterungenBesch"}<br />

                <table width="100%">
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/{$PluginBMA}" width="16" height="16" alt="Status" /></td>
                        <td class="td2">b1gMailServer / b1gMailAdmin</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/{$PHPExtIMAP}" width="16" height="16" alt="Status" /></td>
                        <td class="td2">php-imap</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/{$PHPExtOSSL}" width="16" height="16" alt="Status" /></td>
                        <td class="td2">php-openssl</td>
                    </tr>
                    <tr>
                        <td class="td1" width="30"><img src="../plugins/templates/images/{$PHPExtMBSTRING}" width="16" height="16" alt="Status" /></td>
                        <td class="td2">php-mbstring</td>
                    </tr>
                </table>
            </fieldset>
        </td>
    </tr>
</table>