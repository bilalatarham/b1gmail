{* $Id: pacc.admin.prefs.invoicetemplate.tpl,v 1.2 2010/03/09 16:44:39 patrick Exp $ *}
<form action="{$pageURL}&sid={$sid}&action=prefs&do=invoiceTemplate&save=true" method="post" onsubmit="editor.submit();spin(this)">
	<fieldset>
		<legend>{lng p="pacc_rgtemplate"}</legend>
		
		<div style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
			<textarea name="rgtemplate" id="rgtemplate" class="plainTextArea" style="width:100%;height:500px;">{text value=$pacc_prefs.rgtemplate allowEmpty=true}</textarea>
			<script language="javascript" src="../clientlib/wysiwyg.js"></script>
			<script language="javascript">
			<!--
				var editor = new htmlEditor('rgtemplate', '{$usertpldir}/images/editor/');
				editor.init();
				registerLoadAction('editor.start()');
			//-->
			</script>
		</div>
	</fieldset>
	
	<p>
		<div style="float:left">
			<input class="button" type="button" value=" &laquo; {lng p="back"} " onclick="document.location.href='{$pageURL}&action=prefs&sid={$sid}';" />
		</div>
		<div style="float:right">
			<input class="button" type="submit" value=" {lng p="save"} " />
		</div>
	</p>
</form>
