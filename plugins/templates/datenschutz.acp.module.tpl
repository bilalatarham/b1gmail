{$MSG}

<fieldset>
    <legend>{lng p="dsModuleInfo"}</legend>
    {lng p="dsModuleInfoText"}
</fieldset>

{if $Step == 'step1'}
    <fieldset>
        <legend>{lng p="dsModuleAdd"}</legend>
            <form action="{$pageURL}&do=module&sid={$sid}"method="post">
                <table width="100%">
                    <tr>
                        <td class="td1" width="180">{lng p="dsModuleAddName"}:</td>
                        <td class="td2"><input type="text" style="width:85%;" name="addname" id="addname" value="{$AddName}" /></td>
                    </tr>
                    <tr>
                        <td class="td1">{lng p="dsModuleAddTabelle"}:</td>
                        <td class="td2">
                            <select style="width:85%;" name="addtabelle">
                                {foreach from=$DBTabellen item=tabarr}
                                    {foreach from=$tabarr item=tab}
                                        <option value="{$tab}"{if $AddTabelle==$tab} selected="selected"{/if}>Tabelle: {text value=$tab}</option>
                                    {/foreach}
                                {/foreach}
                            </select>
                        </td>
                    </tr>
                </table>

                <p align="right">
                    <input class="button" type="submit" name="addmodule" value=" {lng p="dsAbfr"} " />
                </p>
            </form>

    </fieldset>
{/if}
{if $Step == 'step2'}
    <form action="{$pageURL}&do=module&step=2&name={$AddName}&tabelle={$URLTabelle}&sid={$sid}"method="post">
        <fieldset>
            <legend>{lng p="dsModuleAdd"}</legend>


            <table width="100%">
                <tr>
                    <td class="td1" width="180">{lng p="dsModuleAddName"}:</td>
                    <td class="td2">{$AddName}</td>
                </tr>
                <tr>
                    <td class="td1">{lng p="dsModuleAddTabelle"}:</td>
                    <td class="td2">{$AddTabelle}</td>
                </tr>
                <tr>
                    <td class="td1">{lng p="dsModuleAddBenutzer"}:</td>
                    <td class="td2">
                        <select name="adduser">
                            {foreach from=$AllUsers item=au}
                                <option value="{$au.id}"{if $AddUser==$au.id} selected="selected"{/if}>{text value=$au.vorname} {text value=$au.nachname} ({text value=$au.email})</option>
                            {/foreach}
                        </select><br /><i>{lng p="dsModuleAddBenutzerInfo"}</i>
                    </td>
                </tr>
                <tr>
                    <td class="td1">{lng p="dsModuleAddBenZuordnung"}:</td>
                    <td class="td2">
                        <select name="addusertabelle">
                            {foreach from=$DBSpalten item=spa}
                                <option value="{$spa.Field}"{if $AddUserTabelle==$spa.Field} selected="selected"{/if}>Spalte: {text value=$spa.Field}</option>
                            {/foreach}
                        </select>

                        <select name="addusertype">
                            <option value="userid"{if $AddUserType=='userid'} selected="selected"{/if}>Type: UserID</option>
                            <option value="usermail"{if $AddUserType=='usermail'} selected="selected"{/if}>Type: User E-Mail</option>
                        </select><br /><i>{lng p="dsModuleAddBenZuordnungInfo"}</i>
                    </td>
                </tr>
                <tr>
                    <td class="td1">{lng p="dsAktivieren"}:</td>
                    <td>
                        {if is_array($DBSpalten)}
                        {foreach from=$DBSpalten key=key item=spa}
                            <input name="addspalten[]" value="{$spa.Field}" type="checkbox" {if $AddUserTabelleSpalten}{if $spa.Field|in_array:$AddUserTabelleSpalten}checked="checked"{/if}{/if} />
                            <label><b>{$spa.Field}</b></label><br />
                        {/foreach}
                        {/if}
                    </td>
                </tr>
            </table>
            <p align="right">
                <input class="button" type="submit" name="addmoduleshow" value=" {lng p="dsAbfr"} " />
            </p>
        </fieldset>

        {if $AddUserTabelleSpaltenOutput}
        <fieldset>
            <legend>{lng p="dsModuleAddShow"}</legend>
            <h3>{lng p="dsModule"}</h3>
            <table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">
                <tr style="background-color: #cccccc; padding:5px;">
                    <td colspan="{$AddUserTabelleSpalten|@count}">{$AddName}</td>
                </tr>
                {foreach from=$AddUserTabelleSpaltenOutput item=output}
                    <tr>
                        {foreach from=$output item=out}
                            <td style="font-size: 12px;">{$out}</td>
                        {/foreach}
                    </tr>
                {/foreach}
            </table>
            <p align="right">
                <input class="button" type="submit" name="addmoduelsave" value=" {lng p="dsSave"} " />
            </p>
        </fieldset>
        {/if}
    </form>
{/if}

{if $Step == 'step1' || $Step == 'step3'}
    <fieldset>
        <legend>{lng p="dsModuleAll"}</legend>
        <table class="list">
            <tr>
                <th>{lng p="dsModuleName"}</th>
                <th>{lng p="dsModuleTabelle"}</th>
                <th>{lng p="dsModuleSpalten"}</th>
                <th width="30">&nbsp;</th>
            </tr>
            {if is_array($Content)}
            {foreach from=$Content item=cont}
                {cycle name=class values="td1,td2" assign=class}
                <tr class="{$class}">
                    <td>{text value=$cont.Name}</td>
                    <td>{text value=$cont.DBTabelle}</td>
                    <td>
                        {foreach from=$cont.Spalten item=spal}
                            {$spal}&nbsp;
                        {/foreach}
                    </td>
                    <td>
                        <a href="{$pageURL}&do=module&delete={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="../plugins/templates/images/datenschutz_delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                    </td>
                </tr>
            {/foreach}
            {/if}
        </table>
    </fieldset>
{/if}

{if $Module}
    <fieldset>
        <legend>{lng p="dsModuleEx"}</legend>

        <div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px;color: #a94442; background-color: #f2dede; border-color: #ebccd1;">
        {lng p="dsModuleExInfo"}
        </div>

        <table class="list">
            <tr>
                <th>{lng p="dsModuleDatei"}</th>
            </tr>

            {foreach from=$Module item=cont}
                {cycle name=class values="td1,td2" assign=class}
                <tr class="{$class}">
                    <td>{text value=$cont}</td>
                </tr>
            {/foreach}
        </table>
    </fieldset>
{/if}