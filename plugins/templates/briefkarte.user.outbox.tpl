
<h1><img src="./plugins/templates/images/modbriefkarte_bk.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="bk_journal"}</h1>

<table class="listTable">
	<tr>
		<th class="listTableHead" width="30%">
			{lng p="bk_typ"}
		</th>
		<th class="listTableHead" width="30%">
			{lng p="bk_datum"} <img src="{$tpldir}images/li/desc.gif" border="0" alt="" align="absmiddle" />
		</th>
		<th class="listTableHead" width="30%" align="center">
			{lng p="bk_status"}
		</th>
        <th class="listTableHead">
			&nbsp;
		</th>
		<th class="listTableHead" width="15">&nbsp;</th>
	</tr>
	{if $bkoutbox}
	<tbody class="listTBody">
	{foreach from=$bkoutbox key=bkID item=bk}
	{cycle values="listTableTD,listTableTD2" assign="class"}
	<tr>
		<td class="{$class}">&nbsp;<a href="javascript:toggleGroup3({$bk.id});"><img id="group3Image_{$bk.id}" src="{$tpldir}images/{if $smarty.request.show==$bk.id}contract{else}expand{/if}.gif" border="0" alt="" align="absmiddle" /></a>&nbsp;{if $bk.bktyp==brief}{lng p="bk_typ1"}{else}{lng p="bk_typ2"}{/if}</td>
		<td class="{$class}">&nbsp;{date timestamp=$bk.erstellt nice=true}</td>
       	<td class="{$class}" style="text-align:center;">&nbsp;{if $bk.status==1}{lng p="bk_status1"}{elseif $bk.status==2}{lng p="bk_status2"}{else}{lng p="bk_status_err"}{/if}</td>
        <td class="{$class}" style="text-align:right;"><a href="start.php?action=getBKFile&bkkey={$bk.bkkey}&id={$bk.id}&journal=1&sid={$sid}"><img src="{if $bk.bktyp==postkarte}{$tpldir}images/li/ico_download.png{else}./plugins/templates/images/modbriefkarte_pdf.gif{/if}" border="0" align="absmiddle" /></a>&nbsp;&nbsp;<a onclick="return confirm('{lng p="realdel"}');" href="start.php?action=briefkarte&do=deletebk&id={$bk.id}&sid={$sid}"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="delete"}" align="absmiddle" /></a></td>
        <td class="{$class}">&nbsp;</td>
	</tr>
	<tbody id="group3_{$bk.id}" style="display:{if $smarty.request.show!=$bk.id}none{/if}">
	<tr>
		<td colspan="6" class="listTableTDText">
            <b>{lng p="bk_vn"}: </b>{if $bk.land=='de'}{lng p="bk_de"}{elseif $bk.land=='eu'}{lng p="bk_eu"}{else}{lng p="bk_welt"}{/if}<br />
            <b>{lng p="bk_status"}: </b>{if $bk.status==1}{lng p="bk_status1"}{elseif $bk.status==2}{lng p="bk_status2"}{else}{lng p="bk_status3"}{/if}<br />
			{if $bk.status==2 && $bk.einschreiben!=0}<b>{lng p="b_einschreibencheck"}: </b>&raquo; <a href="start.php?action=briefkarte&do=checkeinschreiben&aid={$bk.auftrag}&uid={$bk.userid}&sid={$sid}" target="_blank">{lng p="b_clickhere"}</a> &laquo;<br />{/if}
            <b>{lng p="bk_price"}: </b>{text value=$bk.kosten} Credits
        </td>
	</tr>
	</tbody>
	{/foreach}
	</tbody>
	{/if}
</table>

{literal}
<script type="text/javascript">
	function toggleGroup3(id)
	{
		var groupItem = EBID('group3_' + id);
		var groupItemImg = EBID('group3Image_' + id);
		
		if(groupItem.style.display == '')
		{
			groupItem.style.display = 'none';
			groupItemImg.src = groupItemImg.src.replace(/contract/, 'expand');
		}
		else
		{
			groupItem.style.display = '';
			groupItemImg.src = groupItemImg.src.replace(/expand/, 'contract');
		}
	}
</script>
{/literal}