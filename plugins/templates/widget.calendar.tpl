<div class="innerWidget">
	{* <center>
		{$bmwidget_calendar_html}
	</center>
	
	<div class="clndrWdgtDates">
	{if $bmwidget_calendar_nextDates}
		<ul>
			{foreach from=$bmwidget_calendar_nextDates item=_date}
			<li>
				<span class="date">{date timestamp=$_date.startdate format="%a., %d.%m."}</span>
				<a href="organizer.calendar.php?date={$_date.startdate}&sid={$sid}">{text value=$_date.title cut=35}</a>
			</li>
			{/foreach}
		</ul>
	{else}
		<div style="text-align:center;font-size:10px;font-style:italic;">{lng p="nodatesin31d"}</div>
	{/if}
	</div> *}

	<div class="dragable-box content-box">
		<div class="box-title">
			<span class="tl-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
			</span>
			<h3 class="tl-title fw-med">Calendar</h3>
		</div>
		<div class="cal-widget">
			<center>
				{$bmwidget_calendar_html}
			</center>
			
			<div class="clndr-widget-info">
				{if $bmwidget_calendar_nextDates}
					{foreach from=$bmwidget_calendar_nextDates item=_date}
					<p>
					    {date timestamp=$_date.startdate format="%a, %d.%m."}
						<span class="fw-bold">
							<a href="organizer.calendar.php?date={$_date.startdate}&sid={$sid}">{text value=$_date.title cut=35}</a>
						</span>
					</p>
					{/foreach}
				{else}
					<div style="text-align:center;font-size:10px;font-style:italic;">{lng p="nodatesin31d"}</div>
				{/if}
			</div>
		</div>
	</div>
</div>