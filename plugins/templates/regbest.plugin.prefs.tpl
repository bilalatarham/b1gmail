<fieldset>
	<legend>{lng p="prefs"}</legend>
	
	<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/modregbest32.png" border="0" alt="" width="32" height="32" /></td>
			<td>
				<div style="line-height:1.5em;">{lng p="ueb2"}</div>
				<blockquote>
					<div style="width:450px;">
						<div style="float:left; padding:4px 0px 5px 0px;"><b>{lng p="regakt"}</b></div>
						<div style="text-align:right; padding:2px 257px 5px 0px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="regbestaktiv" value="regbestaktiv" {if $regbest.regbestaktiviert}checked="checked"{/if}></div>
						<div style="float:left; padding:5px 0px 5px 0px;"><b>{lng p="betreff"}: </b></div>
						<div style="text-align:right; padding:2px 0px 5px 0px;"><input type="text" name="betreff" value="{text value=$betreff}" size="50" /></div>
						<div style="float:left; padding:5px 0px 5px 0px;"><b>{lng p="absender"}: </b></div>
						<div style="text-align:right; padding:2px 0px 5px 0px;"><input type="text" name="absender" value="{text value=$absender}" size="50" /></div>
						<div style="float:left; padding:5px 0px 5px 0px;"><b>{lng p="anbieter"}: </b></div>
						<div style="text-align:right; padding:2px 0px 5px 0px;"><input type="text" name="pt" value="{text value=$pt}" size="50" /></div><br />
						<div style="padding:0px 0px 5px 0px;"><b>{lng p="mailtext"}</b></div>
						<div>&raquo; <a href="prefs.languages.php?sid={$sid}">{lng p="clickhereVar"}</a> &laquo;</div>
						<div style=" padding:5px 0px 5px 0px;"><b>{lng p="variablen"}:</b></div>
						<div>%%pt%% = {lng p="anbieter"}, %%domain%% = Domainname, <br />%%usermail%% = Usermail</div>
					</div>
				</blockquote>
			</td>
		</tr>
	</table>
	<p>
		<div style="float:right;">
			<input type="submit" class="button" value=" {lng p="save"} " />
		</div>
	</p>
	</form>
</fieldset>