<fieldset>
        <legend>Settings</legend>
        <form action="plugin.page.php?plugin=rating&sid={$sid}&action=settings&do=change" method="post" onsubmit="return(checkNoteForm(this));">
		<table>
{if $pref_error}
			<p>&nbsp;&nbsp;<img src="../plugins/templates/images/opinion_info.png" width="16" height="16" /> <span style="background-color:red">Please enter a value into all fields.</span></p> <br />
{/if}	
					<tr>
						<td class="td1">Opinions count:</td> <td class="td2"><input name="count" value="{$opinion_acp_settings.pref1}" type="text" size="6" maxlength="2" value="{$opinion_acp_settings.pref1}"> How much entries should select from database?</td>
					</tr><tr>	
						<td class="td1">Link to Opinions:</td> <td class="td2"><b>index.php?action=</b><input name="datalnk" type="text" size="15" maxlength="25" value="{$opinion_acp_settings.pref2}">, <input name="datalnk2" type="text" size="15" maxlength="25" value="{$opinion_acp_settings.pref3}"> (maximum of two sites, separated by comma)
						|| <input type="checkbox" name="datalnk3" value="1" {$opinion_acp_settings.pref4}> Startpage?</td>
					</tr><tr>
        </table>
					<div style="float:right;">
						<input class="button" type="submit" value=" Ausf&uuml;hren " />
					</div>		
		</from>
</fieldset>

<fieldset>
        <legend>Code snippets</legend>
        <br />The following code allows you to represent the opinions on your homepage:
<pre>{literal}
{if !$opinions_tpl}														
&nbsp;&nbsp;Sorry, no opinion in the database!									
{else}																		
&nbsp;&nbsp;{foreach name=opinions_tpl from=$opinions_tpl key=id item=op_tpl}				
&nbsp;&nbsp;{$op_tpl.vorname}, {$op_tpl.nachname}, {$op_tpl.rate}, {$op_tpl.text}, {$op_tpl.datum}	
&nbsp;&nbsp;{/foreach}															
{/if}
{/literal}</pre>
</fieldset>

{if $op_v09_oldfile}
<fieldset>
        <legend>Warning</legend>
        <table>
                <tr>
                        <td width="48"><img src="../plugins/templates/images/opinion_warning.png" width="48" height="48" border="0" alt="" /></td>
                        <td width="10">&nbsp;</td>
                        <td><b>There are old files!</b><br />The following files are unnecessary and can be deleted: <br /><br />
						&nbsp;&nbsp;<span style="background-color:yellow">(path_to_b1gMail)/plugins/templates/opinions.pref.tpl</span><br />
						&nbsp;&nbsp;<span style="background-color:yellow">(path_to_b1gMail)/plugins/templates/images/send.png</span><br />
						&nbsp;&nbsp;<span style="background-color:yellow">(path_to_b1gMail)/plugins/templates/images/send_mouseover.png</span></td>
                </tr>
        </table>
</fieldset>
{/if}