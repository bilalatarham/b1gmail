<section id="content" class="single-wrapper" style="margin-bottom: 30px;">
    <div class="grey-background wow fadeIn">
        <div class="container">
            <div class="heading-block page-title wow fadeInUp">
                <h1>{lng p="dsTitel"}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                {$Verantwortung}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-group" id="ds">
                    {foreach from=$ContentArray item=con}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#ds" href="#ds-{$con.ID}"><h1 class="panel-title">{$con.Title}</h1></a></h4>
                            </div>
                            <div id="ds-{$con.ID}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    {$con.Content}
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                {$DSAbschluss}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel-group" id="dsinfo">
                    {if $LogWebAccStatus == 'yes'}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#dsinfo" href="#dsinfo-webacc"><h1 class="panel-title">{lng p="dsLogsWebAcc"}</h1></a></h4>
                        </div>
                        <div id="dsinfo-webacc" class="panel-collapse collapse in">
                            <table class="table">
                                <tr>
                                    <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                                    <td>{$LogWebAccInhalt}</td>
                                </tr>
                                <tr>
                                    <td><strong>{lng p="dsZeit"}:</strong></td>
                                    <td>{$LogWebAccInter} Tage</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    {/if}

                    {if $LogWebErrStatus == 'yes'}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#dsinfo" href="#dsinfo-weberr"><h1 class="panel-title">{lng p="dsLogsWebErr"}</h1></a></h4>
                        </div>
                        <div id="dsinfo-weberr" class="panel-collapse collapse">
                            <table class="table">
                                <tr>
                                    <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                                    <td>{$LogWebErrInhalt}</td>
                                </tr>
                                <tr>
                                    <td><strong>{lng p="dsZeit"}:</strong></td>
                                    <td>{$LogWebErrInter} Tage</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    {/if}

                    {if $LogB1GStatus == 'yes'}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#dsinfo" href="#dsinfo-b1g"><h1 class="panel-title">{lng p="dsLogsWebMail"}</h1></a></h4>
                            </div>
                            <div id="dsinfo-b1g" class="panel-collapse collapse">
                                <table class="table">
                                    <tr>
                                        <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                                        <td>{$LogB1GInhalt}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>{lng p="dsZeit"}:</strong></td>
                                        <td>{$LogB1GInter} Tage</td>
                                    </tr>
                                    {if $LogArchiveCron == 'yes'}
                                        <tr>
                                            <td><strong>{lng p="dsLogArchive"}:</strong></td>
                                            <td>{$LogB1GSInter} Tage</td>
                                        </tr>
                                    {/if}
                                </table>
                            </div>
                        </div>
                    {/if}

                    {if $LogB1GSStatus == 'yes'}
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#dsinfo" href="#dsinfo-b1gs"><h1 class="panel-title">{lng p="dsLogsMail"}</h1></a></h4>
                            </div>
                            <div id="dsinfo-b1gs" class="panel-collapse collapse">
                                <table class="table">
                                    <tr>
                                        <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                                        <td>{$LogB1GSInhalt}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>{lng p="dsZeit"}:</strong></td>
                                        <td>{$LogB1GSInter} Tage</td>
                                    </tr>
                                    {if $LogArchiveCron == 'yes'}
                                        <tr>
                                            <td><strong>{lng p="dsLogArchive"}:</strong></td>
                                            <td>{$LogB1GSInter} Tage</td>
                                        </tr>
                                    {/if}
                                </table>
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</section>
