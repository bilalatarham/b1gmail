<fieldset>
	<legend>{lng p="prefs"}</legend>
	
	<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/modSR32.png" border="0" alt="" width="32" height="32" /></td>
			<td>
				<div style="line-height:1.5em;">{lng p="SRueb"}</div>
				<blockquote>
					<div style="width:450px;">
						<div class="td1" style="width:165px; text-align:left; float:left; padding:4px 0px 5px 0px;"><b>&nbsp;{lng p="SRakt"}&nbsp;</b></div>
						<div style="text-align:right; padding:2px 253px 5px 0px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="SRasp" value="SRasp" {if $Rasp}checked="checked"{/if}></div>
						<div class="td1" style="width:165px; text-align:left; float:left; padding:4px 0px 5px 0px;"><b>&nbsp;{lng p="SBw"}&nbsp;</b></div>
						<div style="text-align:right; padding:2px 188px 5px 12px;">
							<select name="sBericht">
								<option value="5"{if $Wsp==5} selected="selected"{/if}>&nbsp;{lng p="SBfre"}&nbsp;</option>
								<option value="0"{if $Wsp==0} selected="selected"{/if}>&nbsp;{lng p="SBson"}&nbsp;</option>
							</select>
						</div>
					</div>
				</blockquote>
			</td>
		</tr>
		<tr>
			<td>
				<table width="50%" cellpadding="3" cellspacing="0" style="border:1px solid #0000FF; text-align:center;">
					<tr>
						<td colspan="4" style="border-bottom:1px solid #0000FF;">
							<div><b>{lng p="uSta"}</b></div>
						</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #0000FF; width:25%;">
							<div align="left"><b>{lng p="uOpt"}</b></div>
						</td>
						<td style="border-left:1px solid #0000FF; border-bottom:1px solid #0000FF; width:25%;">
							<div><b>{lng p="uAktT"}</b></div>
						</td>
						<td style="border-left:1px solid #0000FF; border-bottom:1px solid #0000FF; width:25%;">
							<div><b>{lng p="uAktW"}</b></div>
						</td>
						<td style="border-left:1px solid #0000FF; border-bottom:1px solid #0000FF; width:25%;">
							<div><b>{lng p="uDeakt"}</b></div>
						</td>
					</tr>
					<tr>
						<td>
							<div align="left"><b>{lng p="uOptU"}</b></div>
						</td>
						<td style="border-left:1px solid #0000FF;">
							<div>{$zaehlerT}</div>
						</td>
						<td style="border-left:1px solid #0000FF;">
							<div>{$zaehlerW}</div>
						</td>
						<td style="border-left:1px solid #0000FF;">
							<div>{$zaehlerD}</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<p>
		<div style="float:right;">
			<input class="button" type="submit" value=" {lng p="save"} " />
		</div>
	</p>
	</form>
</fieldset>