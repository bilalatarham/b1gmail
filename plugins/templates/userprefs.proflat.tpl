<form name="f1" id="f1" method="post" action="prefs.php?action=userprefs_proflat&do=save&sid={$sid}">
    <div id="contentHeader">
        <div class="left">
            {lng p="userprefs_proflat_headline"}
        </div>
        <div class="right">
			<button id="templates">{lng p="userprefs_proflat_templates"}</button>
            <button id="resetvals">{lng p="reset"}</button>
            <input type="submit" value="{lng p="save"}" />
        </div>
    </div>
    <div class="scrollContainer">
        <table>
            <tr>
                <td>
                    <label for="topbarBG">{lng p="userprefs_proflat_topbarBG"} </label>
                    <div id="topbarBG_picker"></div>
                    <input type="hidden" name="topbarBG" id="topbarBG" value="{if $usertopbarBG}{$usertopbarBG}{else}{$templatePrefs.topbarBG}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="topbarColor">{lng p="userprefs_proflat_topbarColor"} </label>
                    <div id="topbarColor_picker"></div>
                    <input type="hidden" name="topbarColor" id="topbarColor" value="{if $usertopbarColor}{$usertopbarColor}{else}{$templatePrefs.topbarColor}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="mainmenuBG">{lng p="userprefs_proflat_mainmenuBG"} </label>
                    <div id="mainmenuBG_picker"></div>
                    <input type="hidden" name="mainmenuBG" id="mainmenuBG" value="{if $usermainmenuBG}{$usermainmenuBG}{else}{$templatePrefs.mainmenuBG}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="topbarColor">{lng p="userprefs_proflat_mainmenuColor"} </label>
                    <div id="mainmenuColor_picker"></div>
                    <input type="hidden" name="mainmenuColor" id="mainmenuColor" value="{if $usermainmenuColor}{$usermainmenuColor}{else}{$templatePrefs.mainmenuColor}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="maincontentBG">{lng p="userprefs_proflat_maincontentBG"} </label>
                    <div id="maincontentBG_picker"></div>
                    <input type="hidden" name="maincontentBG" id="maincontentBG" value="{if $usermaincontentBG}{$usermaincontentBG}{else}{$templatePrefs.maincontentBG}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="widgetBG1">{lng p="userprefs_proflat_widgetBG1"} </label>
                    <div id="widgetBG1_picker"></div>
                    <input type="hidden" name="widgetBG1" id="widgetBG1" value="{if $userwidgetBG1}{$userwidgetBG1}{else}{$templatePrefs.widgetBG1}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="widgetBG2">{lng p="userprefs_proflat_widgetBG2"} </label>
                    <div id="widgetBG2_picker"></div>
                    <input type="hidden" name="widgetBG2" id="widgetBG2" value="{if $userwidgetBG2}{$userwidgetBG2}{else}{$templatePrefs.widgetBG2}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="widgetBG3">{lng p="userprefs_proflat_widgetBG3"} </label>
                    <div id="widgetBG3_picker"></div>
                    <input type="hidden" name="widgetBG3" id="widgetBG3" value="{if $userwidgetBG3}{$userwidgetBG3}{else}{$templatePrefs.widgetBG3}{/if}" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="widgetBG4">{lng p="userprefs_proflat_widgetBG4"}:</label>
                    <div id="widgetBG4_picker"></div>
                    <input type="hidden" name="widgetBG4" id="widgetBG4" value="{if $userwidgetBG4}{$userwidgetBG4}{else}{$templatePrefs.widgetBG4}{/if}" />
                </td>
            </tr>			
        </table>
		{hook id="userprefs.proflat.tpl:afterTable"}
        <input type="hidden" name="reset" id="reset_pf_usersettings" value="no" />		
    </div>
</form>

<div id="templates-holder" style="display: none">
	<div id="templates-box">
		<table>
		<tr>
			<i class="fa fa-times fa-3x" id="templates-close"></i>
		</tr>
		{if $pf_userpefs_admincolors_main}
			{foreach from=$pf_userpefs_admincolors item=item key=key}
				<tr class="template-select">
					<td>
						<div class="preview-holder">
							<div class="preview-topbar" style="background-color:#{$pf_userpefs_admincolors_main[$key][0]}"></div>
							<div class="preview-leftmenu" style="background-color:#{$pf_userpefs_admincolors_main[$key][1]}"></div>
							<div class="preview-maincontent" style="background-color:#{$pf_userpefs_admincolors_main[$key][2]}"></div>
						</div>
					</td>
					<td>{$pf_userpefs_admindesc[$key]}</td>
					<input type="hidden" value="{$pf_userpefs_admincolors[$key]}" />
				</tr>
			{/foreach}
		{/if}
		</table>
	</div>
</div>