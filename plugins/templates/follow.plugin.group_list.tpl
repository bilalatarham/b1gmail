<script src="../plugins/templates/follow.plugin.overlibjs.tpl" type="text/javascript" language="javascript"></script>
<fieldset>
        <legend>{lng p="follow_lang_group_wahl"}</legend>
         {$GroupLink}
</fieldset>

<fieldset>
        <legend>{lng p="follow_lang_resp_new"}</legend>
<p align="center">&raquo; <a href="{$pageURL}&action=editor&mod=group&groupID={$groupID}&sid={$sid}">{lng p="follow_lang_new_group"} ({$titel})</a> &laquo;&nbsp;<a href="javascript:void(0);" onmouseover="return overlib('{lng p="follow_lang_new_responder"}');" onmouseout="return nd();"><img src="../plugins/templates/images/follow_plugin_info.gif" width="15" height="15" border="0" alt="" /></a></p>
</fieldset>

<form action="{$pageURL}&sid={$sid}" method="post" onsubmit="spin(this)" name="f1">
<input type="hidden" name="groupID" id="action" value="{$groupID}" />
<input type="hidden" name="action" id="action" value="groups" />
<input type="hidden" name="singleAction" id="singleAction" value="" />
<input type="hidden" name="singleID" id="singleID" value="" />
<fieldset>
        <legend>{lng p="follow_lang_resp_text"} {$titel}</legend>

        <table class="list">
                <tr>
                        <th width="20">&nbsp;</th>
                        <th>{lng p="follow_lang_betreff"}</th>
                        <th width="50">{lng p="follow_lang_tag"}</th>
                        <th width="60">{lng p="follow_lang_versand"}</th>
                        <th width="60">{lng p="follow_lang_group_format"}</th>
                        <th width="95">&nbsp;</th>

                </tr>

                {foreach from=$follow_resp item=follow_resp}
                {cycle name=class values="td1,td2" assign=class}
                <tr class="{$class}">
                        <td align="center"><img src="../plugins/templates/images/follow_plugin_aktiv_{$follow_resp.aktiv}.png" border="0" width="16" height="16" alt="" /></td>
                        <td>{$follow_resp.betreff}</td>
                        <td>{$follow_resp.tage}</td>
                        <td>{$follow_resp.erhalten}</td>
                        <td>{if $follow_resp.textMode =="html"}{lng p="htmltext"}{else}{lng p="plaintext"}{/if}</td>
                        <td>
                                <a href="{$pageURL}&action=editor&mod=group&groupID={$groupID}&id={$follow_resp.id}&sid={$sid}"><img src="{$tpldir}images/edit.png" border="0" alt="{lng p="edit"}" width="16" height="16" /></a>
                                <a href="javascript:singleAction('delete', '{$follow_resp.id}');" onclick="return confirm('{lng p="follow_lang_loeschen"}');"><img src="{$tpldir}images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                                {if $follow_resp.aktiv =="ja"}
                                <a href="javascript:singleAction('deaktivieren', '{$follow_resp.id}');"><img src="../plugins/templates/images/follow_plugin_set_deaktiv.png" border="0" title="deaktivieren" alt="deaktivieren" width="16" height="16" /></a>
                                {else}
                                <a href="javascript:singleAction('aktivieren', '{$follow_resp.id}');"><img src="../plugins/templates/images/follow_plugin_set_aktiv.png" border="0" title="aktivieren" alt="aktivieren" width="16" height="16" /></a>
                                {/if}
                                <a href="{$pageURL}&action=send&id={$follow_resp.id}&sid={$sid}"><img src="../plugins/templates/images/follow_plugin_testing.png" border="0" title="TestMail" alt="TestMail"  width="16" height="16" /></a>
                       </td>
                </tr>
                {/foreach}
        </table>
</fieldset>
</form>