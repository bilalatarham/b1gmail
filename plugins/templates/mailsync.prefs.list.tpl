{if $LogList}<div id="vSep1">{/if}
    <div class="contentHeader">
        <div class="left">
            <img src="plugins/templates/images/mailsync.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="ms"}
        </div>
    </div>

    <form name="f1" method="post" action="prefs.php?action=ms&page=list&sid={$SID}">

        <div class="scrollContainer withBottomBar">
            <table class="bigTable">
                <tr>
                    <th class="listTableHead" width="20"><input type="checkbox" id="allChecker" onclick="checkAll(this.checked, document.forms.f1, 'sync');" /></th>
                    <th class="listTableHead" width="20">&nbsp;</th>
                    <th class="listTableHead">{lng p="msServerIP"}</th>
                    <th class="listTableHead">{lng p="msMailAdresse"}</th>
                    <th class="listTableHead">{lng p="msOrdner"}</th>
                    <th class="listTableHead" width="55">{lng p="msAnzMails"}</th>
                    <th class="listTableHead" width="55">{lng p="msAnzFolders"}</th>
                    <th class="listTableHead" width="220">{lng p="msDatum"}</th>
                    <th class="listTableHead" width="55">&nbsp;</th>
                </tr>
                <tbody class="listTBody">
                {foreach from=$MailList item=ml}
                    {cycle values="listTableTD,listTableTD2" assign="class"}
                    <tr>
                        <td class="{$class}" nowrap="nowrap">&nbsp;<input type="checkbox" id="sync_{$ml.ID}" name="sync[]" value="{$ml.ID}" /></td>
                        <td class="{$class}" nowrap="nowrap"><img src="plugins/templates/images/{text value=$ml.Status}" width="16" height="16" title="{text value=$ml.StatusText}" /></td>
                        <td class="{$class}" nowrap="nowrap">&nbsp;{text value=$ml.SourceServer cut=100} ({text value=$ml.SourceType cut=100}, {text value=$ml.SourcePort cut=100}, {text value=$ml.SourceCert cut=100})</td>
                        <td class="listTableTDActive">&nbsp;{text value=$ml.SourceMail cut=100}</td>
                        <td class="{$class}">&nbsp;{text value=$ml.TargetFolder cut=100}</td>
                        <td class="{$class}">&nbsp;{$ml.AnzMails}</td>
                        <td class="{$class}">&nbsp;{$ml.AnzFolders}</td>
                        <td class="{$class}">&nbsp;{$ml.JobDate}</td>
                        <td class="{$class}" nowrap="nowrap">
                            <a href="prefs.php?action=ms&page=list&do=log&id={$ml.ID}&sid={$SID}"><img src="../plugins/templates/images/mailsync_log.png" width="16" height="16" border="0" alt="{lng p="msBTNLogs"}" align="absmiddle" /></a>
                            <a onclick="return confirm('{lng p="msBTNLoeschenInfo"}');" href="prefs.php?action=ms&page=list&do=delete&id={$ml.ID}&sid={$SID}"><img src="{$tpldir}images/li/ico_delete.png" width="16" height="16" border="0" alt="{lng p="msBTNLoeschen"}" align="absmiddle" /></a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>

        <div id="contentFooter">
            <div class="left">
                <select class="smallInput" name="do2">
                    <option value="-">------ {lng p="msAktion"} ------</option>
                    <option value="delete">{lng p="msBTNLoeschen"}</option>
                </select>
                <input class="smallInput" type="submit" value="OK" />
            </div>
            <div class="right">
                <button type="button" onclick="document.location.href='prefs.php?action=ms&page=add&sid={$SID}';">
                    <img src="{$tpldir}images/li/ico_add.png" width="16" height="16" border="0" alt="" align="absmiddle" />
                    {lng p="msBTNMigration"}
                </button>
            </div>
        </div>

    </form>
    {if $LogList}
</div>


<div id="vSepSep"></div>
<div id="vSep2">
    <div class="contentHeader">
        <div class="left">
            <img src="../plugins/templates/images/mailsync_log.png" width="16" height="16" border="0" alt="" align="absmiddle" />
            {lng p="msLogs"} ({lng p="msRefresh"} <span id="timer"></span>s)
        </div>
    </div>
    <div class="scrollContainer withBottomBar">
        <table class="bigTable">
            <tr>
                <th class="listTableHead" style="width: 110px;">{lng p="msDatum"}</th>
                <th class="listTableHead">{lng p="msNachricht"}</th>
            </tr>
            <tbody class="listTBody">
            {foreach from=$LogList item=lo}
                {cycle values="listTableTD,listTableTD2" assign="class"}
                <tr>
                    <td class="listTableTDActive">&nbsp;{$lo.LogDate}</td>
                    <td class="{$class}">&nbsp;{text value=$lo.Nachricht}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    <div id="contentFooter">&nbsp;</div>
</div>
<script>
    {literal}
    function checklength(i) {
        'use strict';
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    var minutes, seconds, count, counter, timer;
    count = 31; //seconds
    counter = setInterval(timer, 1000);

    function timer() {
        'use strict';
        count = count - 1;
        minutes = checklength(Math.floor(count / 60));
        seconds = checklength(count - minutes * 60);
        if (count < 0) {
            clearInterval(counter);
            return;
        }
        document.getElementById("timer").innerHTML = seconds;
        if (count === 0) {
            location.reload();
        }
    }
    {/literal}
</script>
{/if}

<script language="javascript">
    <!--
    registerLoadAction('initVSep()');
    //-->
</script>
