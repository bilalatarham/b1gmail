<fieldset>
	<legend>{lng p="checkpassword_name"}</legend>
	<table>
		<tr>
			<td width="48"><img src="../plugins/templates/images/checkpassword_logo.png" width="48" height="48" border="0" alt="" /></td>
			<td width="10">&nbsp;</td>
			<td><b>{lng p="checkpassword_name"}</b><br />{lng p="checkpassword_text"}</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend>Welchen Wert gebe ich als Zeitraum ein?</legend>

	<p>Sie k&ouml;nnen jeden belieben Zeitraum eintragen. Der Zeitraum wird in Sekunden gerechnet.<br /><br />
	Eine Stunde ist: 60*60 = 3600<br />
	Ein Tag ist: 24*60*60 = 86400<br />
	90 Tage sind: 90*24*60*60 = 7776000</p>
</fieldset>

<fieldset>
	<legend>Was bedeutet das Feld "Pflicht"?</legend>

	<p>Mit der Hilfe dieses Feldes k&ouml;nnen Sie bestimmen, ob der Benutzer das Passwort &auml;ndern muss oder kann.<br />
	Wenn das Feld gesetzt ist, muss der Benutzer das Passwort &auml;ndern um in seinen E-Mailaccount zu gelangen.</p>
</fieldset>