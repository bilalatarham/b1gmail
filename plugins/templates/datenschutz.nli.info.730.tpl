<table class="nliTable">
    <tr>
        <td class="nliIconTD"><img src="/plugins/templates/images/datenschutz.png" width="32" height="32" border="0" alt="" /></td>
        <td class="nliTD">

            <h3>{lng p="dsTitel"}</h3>

            {$Verantwortung}
            <br /><br />

            {foreach from=$ContentArray item=con}
                <strong>{$con.Title}</strong><br />
                {$con.Content}
                <br />
                <br />
            {/foreach}

            <br /><br />
            {$DSAbschluss}

            <br /><br />
            <br /><br />
            {if $LogWebAccStatus == 'yes'}
                <h3>{lng p="dsLogsWebAcc"}</h3>
                <table class="nliTable">
                    <tr>
                        <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                        <td>{$LogWebAccInhalt}</td>
                    </tr>
                    <tr>
                        <td><strong>{lng p="dsZeit"}:</strong></td>
                        <td>{$LogWebAccInter} Tage</td>
                    </tr>
                </table>
                <br /><br />
            {/if}

            {if $LogWebErrStatus == 'yes'}
                <h3>{lng p="dsLogsWebErr"}</h3>
                <table class="nliTable">
                    <tr>
                        <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                        <td>{$LogWebErrInhalt}</td>
                    </tr>
                    <tr>
                        <td><strong>{lng p="dsZeit"}:</strong></td>
                        <td>{$LogWebErrInter} Tage</td>
                    </tr>
                </table>
                <br /><br />
            {/if}

            {if $LogB1GStatus == 'yes'}
                <h3>{lng p="dsLogsWebMail"}</h3>
                <table class="nliTable">
                    <tr>
                        <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                        <td>{$LogB1GInhalt}</td>
                    </tr>
                    <tr>
                        <td><strong>{lng p="dsZeit"}:</strong></td>
                        <td>{$LogB1GInter} Tage</td>
                    </tr>
                    {if $LogArchiveCron == 'yes'}
                        <tr>
                            <td><strong>{lng p="dsLogArchive"}:</strong></td>
                            <td>{$LogB1GSInter} Tage</td>
                        </tr>
                    {/if}
                </table>
                <br /><br />
            {/if}

            {if $LogB1GSStatus == 'yes'}
                <h3>{lng p="dsLogsMail"}</h3>
                <table class="nliTable">
                    <tr>
                        <td style="width: 110px;"><strong>{lng p="dsInhalt"}:</strong></td>
                        <td>{$LogB1GSInhalt}</td>
                    </tr>
                    <tr>
                        <td><strong>{lng p="dsZeit"}:</strong></td>
                        <td>{$LogB1GSInter} Tage</td>
                    </tr>
                    {if $LogArchiveCron == 'yes'}
                        <tr>
                            <td><strong>{lng p="dsLogArchive"}:</strong></td>
                            <td>{$LogB1GSInter} Tage</td>
                        </tr>
                    {/if}
                </table>
                <br /><br />
            {/if}
        </td>
    </tr>
</table>

