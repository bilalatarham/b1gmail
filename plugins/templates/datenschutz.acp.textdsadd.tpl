<fieldset>
    <legend>{lng p="dsDatenschutz"} {lng p="dsAdd"}</legend>

    {$MSG}

    <form action="{$pageURL}&do=text&action=add&sid={$sid}" method="post" onsubmit="EBID('title').focus();if(EBID('title').value.length<2) return(false);editor.submit();spin(this)">
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="dsSortierung"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="sortierung" id="sortierung" value="" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="title"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="title" id="title" value="" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="dsLang"}:</td>
                <td class="td2"><select style="width:85%;" name="lang">
                        {foreach from=$languages key=langID item=lang}
                            <option value="{$langID}"{if $langID==$selectedLang} selected="selected"{/if}>{text value=$lang.title}</option>
                        {/foreach}
                    </select></td>
            </tr>
            <tr>
                <td colspan="2" style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
                    {if $b1gVersion == '7.3.0'}
                        <textarea name="text" id="text" class="plainTextArea" style="width:100%;height:300px;"></textarea>
                        <script language="javascript" src="../clientlib/wysiwyg.js"></script>
                        <script language="javascript">
                            <!--
                            var editor = new htmlEditor('text', '{$usertpldir}/images/editor/');
                            editor.init();
                            registerLoadAction('editor.start()');
                            //-->
                        </script>
                    {else}
                        <textarea name="text" id="text" class="plainTextArea" style="width:100%;height:300px;"></textarea>
                        <script language="javascript" src="../clientlib/wysiwyg.js"></script>
                        <script type="text/javascript" src="../clientlib/ckeditor/ckeditor.js"></script>
                        <script language="javascript">
                            <!--
                            var editor = new htmlEditor('text');
                            editor.height = 300;
                            editor.init();
                            registerLoadAction('editor.start()');
                            //-->
                        </script>
                    {/if}
                </td>
            </tr>
        </table>

        <p align="right">
            <input class="button" type="submit" name="add" value=" {lng p="dsAdd"} " />
        </p>
    </form>
</fieldset>