{* $Id: pacc.user.order.done.tpl,v 1.5 2008/08/04 11:51:43 patrick Exp $ *}
<h1><img src="plugins/templates/images/pacc_subscriptions.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="pacc_order"}: {text value=$package.titel}</h1>

<table class="listTable">
	<tr>
		<th class="listTableHead"> {lng p="pacc_order"}</th>
	</tr>
	<tr>
		<td class="listTableRight">
			<p>
				{lng p="pacc_thanks"}
				{$thanks}
			</p>
			
			<p>
				{if $paymentMethod=='paypal'}
					<form id="paymentForm" action="https://www.paypal.com/cgi-bin/webscr" method="post">
						<input type="hidden" name="cmd" value="_xclick" />
						<input type="hidden" name="business" value="{$ppMail}" />
						<input type="hidden" name="item_name" value="{text value=$itemName}" />
						<input type="hidden" name="item_number" value="{$paymentID}" />
						<input type="hidden" name="amount" value="{$ppAmount}" />
						<input type="hidden" name="no_shipping" value="1" />
						<input type="hidden" name="notify_url" value="{$notifyURL}" />
						<input type="hidden" name="return" value="{$returnURL}" />
						<input type="hidden" name="no_note" value="1" />
						<input type="hidden" name="currency_code" value="{$currency}" />
						<input type="image" style="border:0px;margin-bottom:5px;" src="https://www.paypal.com/de_DE/i/btn/x-click-but23.gif" border="0" name="submit" alt="" />
					</form>
				{elseif $paymentMethod=='sofortueberweisung'}
					<form id="paymentForm" action="https://www.sofortueberweisung.de/payment/start" method="post">
						<input type="hidden" name="user_id" value="{$suKdNr}" />
						<input type="hidden" name="project_id" value="{$suPrjNr}" />
						<input type="hidden" name="amount" value="{$suAmount}" />
						<input type="hidden" name="reason_1" value="Order {$itemNumber}" />
						<input type="hidden" name="reason_2" value="{text value=$itemName}" />
						<input type="hidden" name="user_variable_0" value="{$itemNumber}" />
						<input type="hidden" name="user_variable_1" value="{$sid}" />
						<input type="hidden" name="user_variable_2" value="{$returnURL}" />
						<input type="hidden" name="user_variable_3" value="{$failURL}" />
						<input type="submit" style="margin-bottom:5px;" value=" {lng p="pacc_paysu"} " />
					</form>
				{elseif $paymentMethod=='banktransfer'}
					<table>
						<tr>
							<td width="150">{lng p="pacc_kto_inh"}:</td>
							<td>{text value=$pacc_prefs.vk_kto_inh}</td>
						</tr>
						<tr>
							<td>{lng p="pacc_kto_nr"}:</td>
							<td>{text value=$pacc_prefs.vk_kto_nr}</td>
						</tr>
						<tr>
							<td>{lng p="pacc_kto_inst"}:</td>
							<td>{text value=$pacc_prefs.vk_kto_blz}
								({text value=$pacc_prefs.vk_kto_inst})</td>
						</tr>
						<tr>
							<td><b>{lng p="pacc_subject"}:</b></td>
							<td><b>{$vkCode}</b></td>
						</tr>
						<tr>
							<td><b>{lng p="pacc_amount"}:</b></td>
							<td><b>{$vkAmount}</b></td>
						</tr>
						
						{if $pacc_prefs.vk_kto_iban&&$pacc_prefs.vk_kto_bic}
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td>{lng p="pacc_kto_iban"}:</td>
							<td>{text value=$pacc_prefs.vk_kto_iban}</td>
						</tr>
						<tr>
							<td>{lng p="pacc_kto_bic"}:</td>
							<td>{text value=$pacc_prefs.vk_kto_bic}</td>
						</tr>
						{/if}
					</table>
					
					<p>
						{lng p="pacc_thanks_banktransfer2"}
					</p>
					
					{if $pacc_prefs.sendrg=='yes'}<center>
						<input style="margin-bottom:8px;" type="button" onclick="openOverlay('prefs.php?action=pacc_mod&do=showInvoice&id={$itemNumber}&sid={$sid}','{lng p="pacc_invoice"}',600,550)" value=" {lng p="pacc_showinvoice"} " />
					</center>{/if}
				{elseif $paymentMethod=='none'}
					{lng p="pacc_activated"}
				{/if}
			</p>
		</td>
	</tr>
</table>

{if $paymentMethod=='paypal'||$paymentMethod=='sofortueberweisung'}
{literal}<script language="javascript">
<!--
	function paccPay()
	{
		EBID('paymentForm').submit();
	}

	registerLoadAction(paccPay);
//-->
</script>{/literal}
{/if}
