<fieldset>
	<legend>{lng p="prefs"}</legend>
	
	<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/search32.png" border="0" alt="" width="32" height="32" /></td>
			<td>
				Hier k&ouml;nnen Sie die Vorlaufzeit f&uuml;r die Geburtstagsanzeige einstellen.
				<blockquote>
						<b>Vorlaufzeit in Tagen: </b>
					<input type="text" name="vorlaufzeit" value="{text value=$vlz.vorlaufzeit}" size="2" /><br />
				</blockquote>
			</td>
		</tr>
	</table>
	<p>
		<div style="float:right;">
			<input type="submit" value=" {lng p="save"} " />
		</div>
	</p>
	</form>
</fieldset>