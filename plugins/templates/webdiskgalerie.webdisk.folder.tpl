<h1><img src="{$tpldir}images/li/tab_ico_webdisk.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <a href="webdisk.php?sid={$sid}">{lng p="webdisk"}:/</a>{foreach from=$currentPath item=folder}<a href="webdisk.php?action=webdiskgalerie&folder={$folder.id}&sid={$sid}">{text value=$folder.title}/</a>{/foreach}</h1>
{if $isShared}
	<div class="note" style="margin-bottom:1em;">
		<small>{lng p="webdiskgalerie_share"}</small><br />
		<img src="./templates/default/images/li/ico_share.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <a target="_blank" href="{$galerieURL}" style="color:blue;">{$galerieURL}</a>
	</div>
{/if}

<table width="100%" class="wdIconTable">
	<tr>
		{assign var="i" value=0}
		{foreach from=$images item=element}
		{assign var="i" value=$i+1}
		{if $i==6}
		{assign var="i" value=1}
	</tr>
	<tr>
		{/if}
			<td style="vertical-align:bottom;" class="webdiskItem">
				<a id="wli_{$element.type}_{$element.id}" href="webdisk.php?action=getpicfile&picid={$element.id}&sid={$sid}" rel="lightbox[galerie]" title="{$element.title}">
					<img src="webdisk.php?action=getthumbfile&picid={$element.id}&sid={$sid}" border="0" width="100px" height="100px"/><br />{text value=$element.title cut=13 escape=true}
				</a>
			</td>
			{if $i==6}
			</tr>
			<tr>
			{/if}
		{/foreach}
		{if $i<6}
		{math assign="i" equation="x - y" x=5 y=$i}
		{section loop=$i name=rest}
			<td>&nbsp;</td> 
		{/section}
		{/if}
	</tr>
</table><br />

{if $folders_count > 0}
<table width="100%" class="wdIconTable">
	<tr>
		{assign var="i" value=0}
		{foreach from=$folders item=item}
		{assign var="i" value=$i+1}
		{if $i==6}
		{assign var="i" value=1}
	</tr>
	<tr>
		{/if}
		<td class="webdiskItem">
<a title="{text value=$item.title}" href="javascript:void(0);" onclick="webdiskShowInfo({$item.type}, '{text value=$item.title escape=true}', '{text value=$item.title cut=20 escape=true}', '{size bytes=$item.size}', '{$item.ext}', '{date timestamp=$item.created nice=true}', {$item.id}, {if $item.share}true{else}false{/if}, {if $item.viewable}true{else}false{/if});" class="webdiskLink" ondblclick="document.location.href='webdisk.php?action=webdiskgalerie&folder={$item.id}&sid={$sid}';">
				<img src="webdisk.php?action=displayExtension&ext={$item.ext}&sid={$sid}" border="0" alt=""><br /><span id="wd_{$item.type}_{$item.id}">{text value=$item.title cut=15}</span></a>
		</td>
		{/foreach}
		{if $i<6}
		{math assign="i" equation="x - y" x=5 y=$i}
		{section loop=$i name=rest}
			<td class="webdiskItem">&nbsp;</td> 
		{/section}
		{/if}
	</tr>
</table>
{/if}