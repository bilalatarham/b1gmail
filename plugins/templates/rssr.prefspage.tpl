<h1><img src="plugins/templates/images/rssr_icon16.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="rssr"}</h1>
<form action="{$pageURL}?action=rssr&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
<p>{$err}</p>
<table class="listTable">
	<tr>
		<th class="listTableHead" colspan="2">RSS-Reader</th>
  </tr>
  <tr>
  	<td class="listTableLeftDesc"><img height="16" border="0" width="16" alt="" src="./plugins/templates/images/rssr_icon16.png"/></td>
  	<td class="listTableRightDesc">Neue Feeds</td>
  </tr>
	<tr>
    	<td class="listTableLeft">Titel*: </td>
        <td><input type="text" size="25" name="feedtitel" value="{$feeds.0.titel}"></td>
    </tr>
    <tr>
    	<td  class="listTableLeft">Link zum Feed: </td>
        <td><input type="text" size="25" name="feedlink"  value="{$feeds.0.link}"></td>
    </tr>
    <tr>
    	<td  class="listTableLeft">Anzahl der Eintr&auml;ge in der &Uuml;bersicht: </td>
        <td><input type="text" size="25" name="show"  value="{$feeds.0.show}"></td>
    </tr>
    <tr>
    	<td  class="listTableLeft"></td>
        <td><input type="submit" value="&Uuml;bernehmen" name="submit"><input type="reset" value="Zur&uuml;cksetzen"></td>
    </tr>
</table>
<p>*Bei leerem Feld wird der Titel aus dem RSS-Feed &uuml;bernommen.</p>
<br />
