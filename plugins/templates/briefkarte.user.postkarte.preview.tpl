
<h1><img src="./plugins/templates/images/modbriefkarte_bk_send.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="bk_karte"}</h1>

<form name="f1" method="post" action="start.php?action=briefkarte&do=PostkarteSenden&sid={$sid}" enctype="multipart/form-data">
<input type="hidden" name="bk_land" id="bk_land" value="{text value=$bk_land allowEmpty=false}">
<input type="hidden" name="bkkey" id="bkkey" value="{text value=$bkkey allowEmpty=false}">
<input type="hidden" name="bkid" id="bkid" value="{text value=$bkid allowEmpty=false}">
<input type="hidden" name="bk_text_pk" id="bk_text_pk" value="{text value=$bk_text_pk_org allowEmpty=true}">
<input type="hidden" name="bk_pk_feld_0" id="bk_pk_feld_0" value="{text value=$bk_pk_feld_0 allowEmpty=true}">
<input type="hidden" name="bk_pk_feld_1" id="bk_pk_feld_1" value="{text value=$bk_pk_feld_1 allowEmpty=true}">
<input type="hidden" name="bk_pk_feld_4" id="bk_pk_feld_4" value="{text value=$bk_pk_feld_4 allowEmpty=true}">
<input type="hidden" name="bk_pk_feld_5" id="bk_pk_feld_5" value="{text value=$bk_pk_feld_5 allowEmpty=true}">
<input type="hidden" name="bk_pk_feld_6" id="bk_pk_feld_6" value="{text value=$bk_pk_feld_6 allowEmpty=true}">
<input type="hidden" name="bk_pk_feld_7" id="bk_pk_feld_7" value="{text value=$bk_pk_feld_7 allowEmpty=true}">
	<table class="listTable">
		<tr>
			<th class="listTableHead">{lng p="bk_prevueb1"}</th>
		</tr>
		<tr>
			<td align="center"><img src="{$bk_postkarte_bild}" style="width:591px; height:394px; max-width:591px; max-height:394px;"></td>
		</tr>
	</table>
	<br />
	<table class="listTable">
		<tr>
			<th class="listTableHead">{lng p="bk_prevueb2"}</th>
		</tr>
		<tr>
			<td align="center">
				<div style="width:591px; height:394px; background-image:url(./plugins/templates/images/modbriefkarte_preview_hinten.png); max-width:591px; max-height:394px; margin:0px; border:1px solid #000000;">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td valign="top" rowspan="6" style="width:287px; height:390px;">
								<div style="padding:10px 5px 0px 10px; text-align:justify; font-family: Arial, sans-serif; font-size:15px;">{$bk_text_pk}</div>
							</td>
							<td style="height:190px;">
								<div>&nbsp;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="height:15px;">
								<div style="padding:0px 0px 0px 25px; font-family: Arial, sans-serif; font-size:x-small;">{if $bk_pk_feld_0}{$bk_pk_feld_0}, {$bk_pk_feld_1}{else}{$bk_pk_feld_1}{/if}</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="height:15px;">
								<div style="padding:15px 0px 0px 25px; font-family: Arial, sans-serif; font-size:15px;">{$bk_pk_feld_4}</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="height:15px;">
								<div style="padding:3px 0px 3px 25px; font-family: Arial, sans-serif; font-size:15px;">{$bk_pk_feld_5}</div>
							</td>
						</tr>
						<tr>
							<td valign="top" style="height:15px;">
								<div style="padding:0px 0px 0px 25px; font-family: Arial, sans-serif; font-size:15px;">{$bk_pk_feld_6}</div>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div style="padding:3px 0px 0px 25px; font-family: Arial, sans-serif; font-size:15px;">{$bk_pk_feld_7}</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<table class="listTable">
		<tr>
			<td class="listTableLeftDescTopLine" style="height:7px;"></td>
			<td class="listTableRightDesc" style="height:7px;"></td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding: 5px 5px 5px 5px;">
				<input type="button" value="{lng p="bk_karte"}" id="sendButton" onclick="document.forms.f1.submit();" {if $accCredits==false} disabled="disabled"{/if}/>
				<input type="button" value="{lng p="reset"}" onclick="window.location.href = 'start.php?action=briefkarte&do=PostkarteRemove&bkkey={$bkkey}&bkid={$bkid}&sid={$sid}';"/>
				{if $accCredits==false}<div class="note" id="errorMsg">{lng p="bk_pricewarning"}</div>{/if}
			</td>
		</tr>
		<tr>
			<td colspan="2" class="listTableRightDesc"><div><small><b>Hinweis:</b> {lng p="bk_prevhinw"}</small></div></td>
		</tr>
	</table>
</form>