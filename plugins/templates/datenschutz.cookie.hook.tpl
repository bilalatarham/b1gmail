<script type="text/javascript" src="{$selfurl}plugins/js/datenschutz_cookieinfo.js?{fileDateSig file="../../plugins/js/datenschutz_cookieinfo.js"}"></script>
<script type="text/javascript">{literal}
    function getCookie(name) {
        var b = document.cookie.match('(^|[^;]+)\\s*' + name + '\\s*=\\s*([^;]+)');
        return b ? b.pop() : '';
    }{/literal}{if $CookieJavaScriptAllow !== ''}{literal}
    function allowedJSScripts(){
        {/literal}{$CookieJavaScriptAllow}{literal}
    }
{/literal}{/if}{literal}{/literal}{if $CookieJavaScriptDeny !== ''}{literal}
    function denyJSScripts(){
        {/literal}{$CookieJavaScriptDeny}{literal}
    }{/literal}{/if}{literal}{/literal}{if $CookieJavaScriptDenyCookie == 'yes'}{literal}
    function deleteAllCookies() {
        var cookies = document.cookie.split(";");
        const allowedCookies = [{/literal}{$CookieSystem}{literal}];

        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var cookiename = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            var name = cookiename.trim()
            if (name.startsWith("sessionSecret_") || allowedCookies.includes(name)) {
                //console.log("System Cookie: "+name);
            } else {
                document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
                //console.log("Deny: "+name);
            }
        }
    }{/literal}{/if}{literal}

    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#{/literal}{$CookieDesignBG}{literal}",
                "text": "#{/literal}{$CookieDesignFG}{literal}"
            },
            "button": {
                "background": "#{/literal}{$CookieDesignLinkBG}{literal}",
                "text": "#{/literal}{$CookieDesignLinkFG}{literal}"
            }
        },
        "theme": "{/literal}{$CookieDesignTheme}{literal}",
        {/literal}{if $CookieDesignPosition}{literal}"position": "{/literal}{$CookieDesignPosition}{literal}",{/literal}{/if}{literal}
        {/literal}{if $CookieDesignStatic}{literal}"static": {/literal}{$CookieDesignStatic}{literal},{/literal}{/if}{literal}
        "type": "{/literal}{$CookieDesignType}{literal}",
        "content": {
            "message": "{/literal}{$CookieText}{literal}",
            "deny": "{/literal}{$CookieClose}{literal}",
            "allow": "{/literal}{$CookieAllow}{literal}",
            "link": "{/literal}{$CookieLinkText}{literal}",
            "href": "{/literal}{$CookieLink}{literal}"
        }
    });

{/literal}{if $CookieDesignType == "opt-in" OR $CookieDesignType == "opt-out"}{literal}
    const cookieConsent = getCookie('cookieconsent_status');
    if (cookieConsent === 'allow') {
{/literal}{if $CookieJavaScriptAllow !== ''}{literal}        allowedJSScripts();{/literal}{/if}{literal}
    } else {
{/literal}{if $CookieJavaScriptDeny !== ''}{literal}        denyJSScripts();{/literal}{/if}{literal}
{/literal}{if $CookieJavaScriptDenyCookie == 'yes'}{literal}        deleteAllCookies();{/literal}{/if}{literal}
    }
{/literal}{/if}{literal}{/literal}</script>