<div class="innerWidget">
	{* <table cellspacing="0" cellpadding="2" width="100%">
		<tr>
			<td align="center">{progressBar value=$bmwidget_webdiskspace_spaceUsed max=$bmwidget_webdiskspace_spaceLimit width=250}</td>
		</tr>
		<tr>
			<td>{size bytes=$bmwidget_webdiskspace_spaceUsed} / {size bytes=$bmwidget_webdiskspace_spaceLimit} {lng p="used"}</td>
		</tr>
	</table> *}

	{* <div class="dragable-box content-box">
		<div class="widget-sp-email sp-webdisc d-flex">
			<div class="webdisc-content">
				<p class="main-title fw-med">Space</p>
				<p class="sub-title fw-sbold">Webdisc</p>
				<p class="used-space mb-0 pt-3" used="{$bmwidget_webdiskspace_spaceUsed}" total="{$bmwidget_webdiskspace_spaceLimit}">{size bytes=$bmwidget_webdiskspace_spaceUsed} / {size bytes=$bmwidget_webdiskspace_spaceLimit} {lng p="used"}</p>
			</div>
			<div class="widget-graph">
				<img src="{$tpldir}frontend_assets/img/chart-img.png" class="img-fluid" />
				<span class="chart-value fw-bold">{size bytes=$bmwidget_webdiskspace_spaceLimit}</span>
			</div>
		</div>
	</div> *}

	{assign var="percentage" value=$bmwidget_webdiskspace_spaceUsed/$bmwidget_webdiskspace_spaceLimit}

	<div class="dragable-box content-box" percentage={math equation="x * y" x=$percentage y=100}>
		<div class="widget-sp-email sp-webdisc">
			<div class="widget-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-webdisc-blue.svg" class="img-fluid" />
			</div>
			<p class="main-title fw-med">Space</p>
			<p class="sub-title fw-sbold">Webdisc</p>
			<div class="progress-container text-center">
				<p class="progress-value">{size bytes=$bmwidget_webdiskspace_spaceUsed} / {size bytes=$bmwidget_webdiskspace_spaceLimit} {lng p="used"}</p>
				<div class="progress">
					<div class="progress-bar" role="progressbar" used="{$bmwidget_webdiskspace_spaceUsed}" total="{$bmwidget_webdiskspace_spaceLimit}" style="width: {math equation="x * y" x=$percentage y=100}%" aria-valuenow="{math equation="x * y" x=$percentage y=100}" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
		</div>
	</div>
</div>