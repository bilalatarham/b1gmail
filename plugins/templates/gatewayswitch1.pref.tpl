<fieldset>
	<legend>{lng p="gatewayswitch_name"}</legend>
	
	<table>
		<tr>
			<td width="48"><img src="../plugins/templates/images/gatewayswitch_logo.png" width="48" height="48" border="0" alt="" /></td>
			<td width="10">&nbsp;</td>
			<td><b>{lng p="gatewayswitch_name"}</b><br />{lng p="gatewayswitch_text"}</td>
		</tr>
	</table>
</fieldset>

<form action="plugin.page.php?plugin=gatewayswitch&action=page1&sid={$sid}" method="post" onsubmit="spin(this)" name="f1">
<input type="hidden" name="sortBy" id="sortBy" value="{$sortBy}" />
<input type="hidden" name="sortOrder" id="sortOrder" value="{$sortOrder}" />
<fieldset>
	<legend>{lng p="gateways"}</legend>

	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="80"><a href="javascript:updateSort('sort');">{lng p="priority"} {if $sortBy=='sort'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th width="250"><a href="javascript:updateSort('receive_method');">{lng p="recvmethod"} {if $sortBy=='receive_method'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th width="400"><a href="javascript:updateSort('pop3_host');">{lng p="pop3host"} {if $sortBy=='pop3_host'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th width="80"><a href="javascript:updateSort('pop3_port');">{lng p="pop3port"} {if $sortBy=='pop3_port'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th>{lng p="pop3user"}</th>
			<th width="80">&nbsp;</th>
		</tr>

		{foreach from=$gateways item=gateway}
		{cycle name=class values="td1,td2" assign=class}
			<tr class="{$class}">
				<td>{if $gateway.sort==$switch.gateway} <img src="./templates/images/ok.png" border="0" alt="{lng p="ok"}" width="16" height="16" /> {/if}</td>
				<td align="center"><center>{$gateway.sort}</center></td>
				<td align="center"><center>{if $gateway.receive_method!=pipe}{lng p="pop3gateway"}{else}{lng p="pipeetc"}{/if}</center></td>
				<td align="center">{$gateway.pop3_host}</td>
				<td align="center">{$gateway.pop3_port}</td>
				<td align="center">{$gateway.pop3_user}</td>
				<td><center>
					<a href="plugin.page.php?plugin=gatewayswitch&action=page2&id={$gateway.id}&sid={$sid}"><img src="./templates/images/edit.png" border="0" alt="{lng p="edit"}" width="16" height="16" /></a>
					<a href="plugin.page.php?plugin=gatewayswitch&action=page1&do=delete&id={$gateway.id}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');"><img src="./templates/images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a></center>
				</td>
			</tr>
		{/foreach}

	</table>
</fieldset>
</form>

<fieldset>
	<legend>{lng p="gateway"} {lng p="gatewayswitch_change"}</legend>
	
	<div id="form">
		<form action="plugin.page.php?plugin=gatewayswitch&action=page1&do=switch&sid={$sid}" method="post">
		<table>
			<tr>
				<td width="40" valign="top"><img src="../plugins/templates/images/gatewayswitch_logo.png" border="0" alt="" width="32" height="32" /></td>
				<td valign="top">{lng p="gatewayswitch_priority_change"}</td>
			</tr>
		</table>

		<p align="right">
			{lng p="priority"}:
			<input type="text" name="gatewaysortnum" value="" size="5" />
			<input type="submit" value=" {lng p="execute"} " class="button"/>
		</p>
		</form>
	</div>

</fieldset>


{if $switch.receive_method=="pop3"}
<fieldset>
	<legend>{lng p="pop3gateway"}</legend>

	<div id="form">
		<table>
			<tr>
				<td width="40" valign="top"><img src="{$tpldir}images/fetch.png" border="0" alt="" width="32" height="32" /></td>
				<td valign="top">{lng p="pop3fetch_desc"}<br><b>{lng p="gatewayswitch_fetch"}</td>
			</tr>
		</table>

		<p align="right">
			{lng p="opsperpage"}:
			<input type="text" name="perpage" id="perpage" value="5" size="5" />
			<input type="button" onclick="fetchPOP3()" value=" {lng p="execute"} " class="button"/>
		</p>
	</div>
</fieldset>
{/if}