<h1><img src="{$tpldir}images/li/tab_ico_webdisk.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="webdisk"}-{lng p="sharing"} ({text value=$folderName})</h1>
	
<form action="webdisk.php?action=saveShareSettings&folder={$folderID}&id={$id}&sid={$sid}" method="post">
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="sharing"}</th>
		</tr>
		<tr>
			<td class="listTableLeft">{lng p="folder"}:</td>
			<td class="listTableRight">{text value=$folderName}</td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="shareFolder">{lng p="share"}:</label></td>
			<td class="listTableRight"><input type="checkbox" name="shareFolder" id="shareFolder" {if $folderShared}checked="checked" {/if}/></td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="galerie_modus">{lng p="webdiskgalerie_galerie_mode"}:</label></td>
			<td class="listTableRight"><input type="checkbox" name="galerie_modus" id="galerie_modus" {if $galerie_modus}checked="checked" {/if}/></td>
		</tr>
		<tr>
			<td class="listTableLeft"><label for="password">{lng p="password"}:</label></td>
			<td class="listTableRight"><input type="password" id="password" name="sharePW" value="{text value=$folderPW allowEmpty=true}" size="30" /></td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight">
				<input type="submit" value="{lng p="ok"}" />
				<input type="reset" value="{lng p="reset"}" />
			</td>
		</tr>
	</table>
</form>
