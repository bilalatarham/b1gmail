<fieldset>
           <legend><b>Wichtige Info</b></legend>
            <p align="left">Bevor das AddOn Follow up Responder, im vollen Umfang genutzt werden kann muss der Versand Start des Autoresponders festgelegt werden.<br /> Dieses ist nur zu beginn einmalig notwendig, da ein Update vollzogen wird bei allen derzeitigen User die vor der Installation vom AddOn im System sind.</p>
            <br />
            <p align="left"><i>Aber keine Angst bestehende Daten werden nicht ver&auml;ndert, es wird ein Zeitstempel in einem zusatzfeld Eingetragen.</i></p>
            <br />
            <b>Versand Start Registrierung </b><br />
           <p align="left">Wenn Sie B1Gmail erst vor kurzem in Betrieb genommen haben oder nur eine kleine Anzahl von Usern haben, sollten Sie dieses Option w&auml;hlen.</p>
            <br />
            <b>Versand Start Plugin Installation</b><br />
            <p align="left">Bei dieser Funktion erhalten alle den gleichen Zeitstempel Tag 0 beginnt also sobald Sie speichern gedr&uuml;ckt haben.</p>
            <br />
            <b>Versandpause:</b><br />
            <p align="left">Diese sollten Sie im jedem fall Aktivieren (wirkt sich aber nur bei Versand Start Plugin Installation aus). Beim Aktivieren werden alle User innerhalb von einem Zeitfenster von 60 Minuten verteilt somit ist eine bessere Auslastung des Mailversandes garantiert.</p>
            <br />
            <u>Erkl&auml;rung</u><br />
            <p align="left">Um das Vorgehen besser zu verstehen hier die Hintergrundinformation dazu: </p>
            <br />
            <p align="left">Der Versand der Responder Nachrichten erfolgt in 24 Stunden Rhythmus wobei als Ausgangspunkt das Registrierungsdatum oder das Datum von Heute (Bei Plugin Installation) als anhaltsPunkt genommen wird.</p>
            <br />
            <p align="left">Nehmen wir einmal an Sie erstellen eine Responder Serie aus 3 Nachrichten wobei die Nachrichten jeweils am 1, 5 und 8 Tag verschickt werden sollen. Wenn Sie nun Versand Start nach Registrierung ausw&auml;hlen und Sie haben einen User der Bereits 3 Monate sprich 90 Tage im System vorhanden ist, bekommt dieser alle 3 Mails auf einmal zugeschickt.  W&auml;hlen Sie aber Versand Start Plugin aus erh&auml;lt der User die Nachrichten ab heute gesehen, sprich morgen die erste Nachricht in 5 Tagen dann eine weitere u.s.w!</p>
            <br />
            <p align="left">Dieser Vorgang muss also nur einmal vollzogen werden und kann nur durch deinstallieren des Plugins R&uuml;ckg&auml;ngig gemacht werden.</p>
</fieldset>


<form action="{$pageURL}&action=setup&sid={$sid}" method="post" onsubmit="spin(this)" name="f1">
<fieldset>
           <legend>Versand Start einstellen</legend>
           <table class="list">
                <tr>
                        <td class="td1" width="120">Versand Start:</td>
                        <td class="td2">
                                <input type="radio" name="vstart" value="reg_date" />
                                <label for="vstart_reg_date"><b>Registrierung Datum</b></label>

                                <input type="radio" name="vstart" value="plug_datum" />
                                <label for="vstart_plug_date"><b>Plugin Installation</b></label>
                        </td>
                </tr>
                <tr>
                      <td class="td1" width="120">Versandpause:</td>
                        <td class="td2">
                                <input type="checkbox" name="vstart_pause"  />
                                        <label for="vstart_pause"><b>Wirkt sich nur bei Versand Start Plugin aus.</b></label><br />
                        </td>
               </tr>
         </table>
<p>

        <div align="center">
                <input type="submit" value=" {lng p="save"} " id="submitButton" />&nbsp;
        </div>
</p>
</fieldset>
</form>