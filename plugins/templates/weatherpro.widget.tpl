<div class="innerWidget">
{literal}
<style type="text/css">
/*
* Wetter
*/
#wetter {
height: 126px;
overflow: hidden;
color: #50a7bf;
font-size: 11px;
line-height: 13px;
margin:auto; 
width:285px;
padding-bottom: 5px;
}

#wetter h1 {
color: #8b8b8b;
font-size: 12px;
font-weight: normal;
margin-bottom: 3px;
}

#wetter div.today {
border-top: 1px dotted #8b8b8b;
border-bottom: 1px dotted #8b8b8b;
height: 47px;
padding-top: 3px;
padding-bottom: 3px;
}

#wetter div.today img {
padding-top: 3px;
}

#wetter #fc_icon_0 {
float: left;
width: 50px;
}

#wetter #info_td {
float: left;
padding-left: 6px;
}

#wetter #info_td2 {
float: left;
padding-left: 6px;
}

#wetter #forecast_1,
#wetter #forecast_2,
#wetter #forecast_3,
#wetter #forecast_4 {
float: left;
width: 60px;
padding-top: 6px;
padding-left: 5px;
}

#wetter #fc_icon_1,
#wetter #fc_icon_2,
#wetter #fc_icon_3,
#wetter #fc_icon_4 {
float: left;
}

#wetter #fc_day_1,
#wetter #fc_day_2,
#wetter #fc_day_3,
#wetter #fc_day_4 {
float: left;
color: #68868e;
font-weight: bold;
font-size: 13px;
padding-left: 3px;
padding-top: 4px;
}

#wetter #fc_temp_1,
#wetter #fc_temp_2,
#wetter #fc_temp_3,
#wetter #fc_temp_4 {
float: left;
padding-top: 3px;
}
</style>
{/literal}
{if $user_err}
<div>{lng p="weatherpro_error"} <a class="pageNav" href="prefs.php?action=contact&sid={$sid}">[&gt;&gt;]</a> </div>
{else}
<div id="wetter"><h1>{lng p="weatherpro_start"} {$wetter.stadt|replace:"+":" "}</h1>
	<div class="today">
		<div id="fc_icon_0"><img id="fc_icon_img_0" src="{$wetter.0.icon}" width="40" height="40" alt="{$wetter.0.zustand}" /></div>
		<div id="info_td">
			<div style="color:#68868e;font-size:13px;font-weight: bold">{lng p="weatherpro_day"}</div>
			<div>{$wetter.0.zustand}</div>
			<div>{lng p="weatherpro_current"} {$wetter.0.temperatur}{$wetter.0.einheit}</div>
		</div>
		<div id="info_td2">
			<div>&nbsp;</div>
			<div>{$wetter.0.wind}</div>
			<div>{$wetter.0.luftfeuchtigkeit}</div>
		</div>
	</div>
	<div id="forecast">
		<div id="forecast_1">
			<div id="fc_icon_1"><img id="fc_icon_img_1" src="{$wetter.1.icon}" width="25" height="25" alt="{$wetter.1.zustand}" /></div>
			<div id="fc_day_1">{$wetter.1.wochentag}</div>
			<div id="fc_temp_1">{$wetter.1.tiefsttemperatur}&deg; | {$wetter.1.hoechsttemperatur}&deg;</div>
		</div>
		<div id="forecast_2">
			<div id="fc_icon_2"><img id="fc_icon_img_2" src="{$wetter.2.icon}" width="25" height="25" alt="{$wetter.2.zustand}" /></div>
			<div id="fc_day_2">{$wetter.2.wochentag}</div>
			<div id="fc_temp_2">{$wetter.2.tiefsttemperatur}&deg; | {$wetter.2.hoechsttemperatur}&deg;</div>
		</div>
		<div id="forecast_3">
			<div id="fc_icon_3"><img id="fc_icon_img_3" src="{$wetter.3.icon}" width="25" height="25" alt="{$wetter.3.zustand}" /></div>
			<div id="fc_day_3">{$wetter.3.wochentag}</div>
			<div id="fc_temp_3">{$wetter.3.tiefsttemperatur}&deg; | {$wetter.3.hoechsttemperatur}&deg;</div>
		</div>
		<div id="forecast_4">
			<div id="fc_icon_4"><img id="fc_icon_img_4" src="{$wetter.4.icon}" width="25" height="25" alt="{$wetter.4.zustand}" /></div>
			<div id="fc_day_4">{$wetter.4.wochentag}</div>
			<div id="fc_temp_4">{$wetter.4.tiefsttemperatur}&deg; | {$wetter.4.hoechsttemperatur}&deg;</div>
		</div>
	</div>
</div>
{/if}
</div>