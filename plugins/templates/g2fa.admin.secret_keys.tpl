<form action="{$page_url}&amp;sid={$sid}&amp;action=secret_keys" method="post">
    <fieldset>
    	<legend>{lng p="search"}</legend>
        <table width="100%">
            <tr>
                <td align="left" rowspan="3" valign="top" width="40"><img src="{$tpldir}images/user_search.png" border="0" alt="" width="32" height="32" /></td>
                <td class="td1" width="150">{lng p="searchfor"}:</td>
                <td class="td2"><input type="text" name="query" value="" placeholder="{lng p="email"}, {lng p="firstname"}, {lng p="lastname"}" size="40"></td>
            </tr>
        </table>
        <div style="float:right" class="buttons">
            <input class="button" type="submit" value="{lng p="search"}">
        </div>
    </fieldset>
</form>

<table class="list">
	<tr>
        <th>{lng p="user"}</th>
		<th>{lng p="g2fa_secret_key"}</th>
		<th width="30">&nbsp;</th>
	</tr>

	{foreach from=$secret_keys item=secret_key}
	    {cycle name=class values="td1,td2" assign=class}
    	<tr class="{$class}">
    		<td>
                {$secret_key.email}<br>
    			<small>{$secret_key.nachname}, {$secret_key.vorname}</small>
            </td>
    		<td>{$secret_key.secret}</td>
    		<td>
                <a href="{$page_url}&amp;sid={$sid}&amp;action=secret_keys&amp;do=delete&amp;user_id={$secret_key.user_id}" onclick="return confirm('{lng p="realdel"}');">
                    <img src="{$tpldir}images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16">
                </a>
    		</td>
    	</tr>
	{/foreach}
</table>
