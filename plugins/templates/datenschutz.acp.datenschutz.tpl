<fieldset>
    <legend>{lng p="language"}</legend>

    <form action="{$pageURL}&do=datenschutz&action=add&sid={$sid}" method="post">
        <center>
            <table>
                <tr>
                    <td>{lng p="language"}:</td>
                    <td><select name="lang">
                            {foreach from=$languages key=langID item=lang}
                                <option value="{$langID}"{if $langID==$selectedLang} selected="selected"{/if}>{text value=$lang.title}</option>
                            {/foreach}
                        </select></td>
                    <td><input class="button" type="submit" value=" {lng p="ok"} &raquo; " /></td>
                </tr>
            </table>
        </center>
    </form>
</fieldset>

<fieldset>
    <legend>{lng p="dsDatenschutz"}</legend>

    <table class="list">
        <tr>
            <th width="30">{lng p="dsSortierung"}</th>
            <th>{lng p="title"}</th>
            <th width="55">&nbsp;</th>
        </tr>

        {foreach from=$DocContent item=cont}
            {cycle name=class values="td1,td2" assign=class}
            <tr class="{$class}">
                <td>{text value=$cont.Sortierung}</td>
                <td>{text value=$cont.Title cut=100}</td>
                <td>
                    <a href="{$pageURL}&do=edit&id={$cont.ID}&sid={$sid}" title="{lng p="edit"}"><img src="../plugins/templates/images/datenschutz_edit.png" border="0" alt="{lng p="edit"}" width="16" height="16" /></a>
                    <a href="{$pageURL}&do=datenschutz&delete={$cont.ID}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="../plugins/templates/images/datenschutz_delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                </td>
            </tr>
        {/foreach}
    </table>
</fieldset>

<fieldset>
    <legend>{lng p="dsDatenschutz"} {lng p="dsAdd"}</legend>

    {$MSG}

    <form action="{$pageURL}&do=datenschutz&action=add&sid={$sid}" method="post" onsubmit="EBID('title').focus();if(EBID('title').value.length<2) return(false);editor.submit();spin(this)">
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="dsSortierung"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="sortierung" id="sortierung" value="" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="title"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="title" id="title" value="" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="dsLang"}:</td>
                <td class="td2"><select style="width:85%;" name="lang">
                        {foreach from=$languages key=langID item=lang}
                            <option value="{$langID}"{if $langID==$selectedLang} selected="selected"{/if}>{text value=$lang.title}</option>
                        {/foreach}
                    </select></td>
            </tr>
            <tr>
                <td colspan="2" style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
                    {if $b1gVersion == '7.3.0'}
                        <textarea name="text" id="text" class="plainTextArea" style="width:100%;height:300px;"></textarea>
                        <script language="javascript" src="../clientlib/wysiwyg.js"></script>
                        <script language="javascript">
                            <!--
                            var editor = new htmlEditor('text', '{$usertpldir}/images/editor/');
                            editor.init();
                            registerLoadAction('editor.start()');
                            //-->
                        </script>
                    {else}
                        <textarea name="text" id="text" class="plainTextArea" style="width:100%;height:300px;"></textarea>
                        <script language="javascript" src="../clientlib/wysiwyg.js"></script>
                        <script type="text/javascript" src="../clientlib/ckeditor/ckeditor.js"></script>
                        <script language="javascript">
                            <!--
                            var editor = new htmlEditor('text');
                            editor.height = 300;
                            editor.init();
                            registerLoadAction('editor.start()');
                            //-->
                        </script>
                    {/if}
                </td>
            </tr>
        </table>

        <p align="right">
            <input class="button" type="submit" name="add" value=" {lng p="dsAdd"} " />
        </p>
    </form>
</fieldset>