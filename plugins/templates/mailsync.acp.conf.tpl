<form action="{$pageURL}&do=conf&sid={$sid}" method="post" enctype="multipart/form-data">
    {$MSG}

    <table style="width: 100%; margin: 0px; padding: 0px;">
        <tr style="vertical-align: top;">
            <td style="width: 50%; margin: 0px; padding: 0px;">
                <fieldset>
                    <legend>{lng p="msConfig"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="msConfigDebug"}:</td>
                            <td class="td2">
                                <input id="debug" name="debug" type="checkbox" {if $Debug == 'yes'}checked="checked"{/if} />
                                <label for="debug"><b>{lng p="msConfigDebugInfo"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="msConfigSelfSigned"}:</td>
                            <td class="td2">
                                <input id="selfsignedcert" name="selfsignedcert" type="checkbox" onclick="return confirm('{lng p="msConfigSelfSignedBox"}');" {if $SelfSignedCert == 'yes'}checked="checked"{/if} />
                                <label for="selfsignedcert"><b>{lng p="msConfigSelfSignedInfo"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="msConfigMG"}:</td>
                            <td class="td2">
                                <select style="width:100%;" name="mailgruppe" id="mailgruppe">
                                    <option>--- {lng p="msConfigMGNo"} ---</option>
                                    {foreach from=$MailGruppen item=mg}
                                        <option value="{$mg.ID}"{if $mg.ID==$SyncGroup} selected="selected"{/if}>{$mg.Titel}</option>
                                    {/foreach}
                                </select>
                                <small>{lng p="msConfigMGInfo"}</small>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td style="width: 50%; margin: 0px; padding: 0px;">
                <fieldset>
                    <legend>{lng p="msConfigCron"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="msConfigCLI"}:</td>
                            <td class="td2">
                                <input id="cronovercli" name="cronovercli" type="checkbox" {if $CronOverCLI == 'yes'}checked="checked"{/if} />
                                <label for="cronovercli"><b>{lng p="msConfigCLIInfo"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="msConfigCronLast"}:</td>
                            <td class="td2">{$LastCronJob}</td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <p align="right">
        <input class="button" type="submit" name="confsave" value=" {lng p="msConfigSave"} " />
    </p>
</form>