<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>{lng p="addressbook"}</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script language="javascript">
	<!--
		var tplDir = '{$tpldir}';
	//-->
	</script>
	<script src="clientlang.php" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/common.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/loggedin.js" type="text/javascript" language="javascript"></script>
	<script src="{$tpldir}js/dialog.js" type="text/javascript" language="javascript"></script>
</head>

<body onload="documentLoader()">

	<table width="100%">
		<tr>
			<td colspan="2" height="127">
				<div class="addressDiv" style="height: 330px;" id="addresses"></div>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="button" onclick="parent.hideOverlay()" value="{lng p="cancel"}" />
				<input type="button" onclick="submitPkDialog()" value="{lng p="ok"}" />
			</td>
		</tr>
	</table>
	<script language="javascript">
	<!--
		registerLoadAction(initNumberDialog);
	
		var Addr = [];
				
		{literal}function initNumberDialog()
		{
			{/literal}{foreach from=$adressen item=address}
			Addr.push(["{text noentities=true escape=true value=$address.lastname}, {text noentities=true escape=true value=$address.firstname}",
										"{text noentities=true escape=true value=$address.strassenr}",
									  	"{text noentities=true escape=true value=$address.ort}"]);
			{/foreach}

			initNumbers(Addr);
			{literal}
		}{/literal}
        
        {literal}function submitPkDialog()
        {
            {/literal}
            var elem = lastAddress.id.substring(lastAddress.id.lastIndexOf('_')+1);
            var name = Addr[elem][0];
            var strhnr = Addr[elem][1];
            var plzort = Addr[elem][2];
            
            parent.document.getElementById('bk_pk_feld_5').value = name;
            parent.document.getElementById('bk_pk_feld_6').value = strhnr;
            parent.document.getElementById('bk_pk_feld_7').value = plzort;

    		parent.hideOverlay();
            {literal}
        }{/literal}
	//-->
	</script>
	
</body>
</html>
