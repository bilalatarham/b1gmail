<fieldset>
	<legend>{lng p="gatewayswitch_name"}</legend>
	
	<table>
		<tr>
			<td width="48"><img src="../plugins/templates/images/gatewayswitch_logo.png" width="48" height="48" border="0" alt="" /></td>
			<td width="10">&nbsp;</td>
			<td><b>{lng p="gatewayswitch_name"}</b><br />{lng p="gatewayswitch_text"}</td>
		</tr>
	</table>
</fieldset>

<form action="plugin.page.php?plugin=gatewayswitch&action=page3&do=save&sid={$sid}" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend>{lng p="prefs"}</legend>

		<table>
			<tr>
				<td class="td1" width="300">{lng p="gatewayswitch_oncron"}</td>
				<td class="td2"><input name="change_oncron" {if $gateway.gateway_oncron==1}checked="checked"{/if} type="checkbox" /> <a href="plugin.page.php?plugin=gatewayswitch&action=page4&sid={$sid}"><img src="./templates/images/help.png" border="0" alt="{lng p="help"}" width="16" height="16" /></a></td>
			</tr>
			<tr>
				<td class="td1" width="300">{lng p="gatewayswitch_onfilehandler"}</td>
				<td class="td2"><input name="change_filehandler" {if $gateway.gateway_filehandler==1}checked="checked"{/if} type="checkbox" /> <a href="plugin.page.php?plugin=gatewayswitch&action=page4&sid={$sid}"><img src="./templates/images/help.png" border="0" alt="{lng p="help"}" width="16" height="16" /></a></td>
			</tr>
		</table>

		<p align="right">
			<input type="submit" value=" {lng p="save"} " class="button"/>
		</p>
	</fieldset>
</form>