{if $_tplname=='modern'}
<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="cancelmembership"}
	</div>
</div>

<div class="scrollContainer"><div class="pad">
{else}
<h1><img src="{$tpldir}images/li/ico_membership.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="cancelmembership"}</h1>
{/if}

<table>
	<tr>
		<td valign="top" width="64" align="center"><img src="{$tpldir}images/li/msg.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			{lng p="pacc_cancelwarning"}
			<br /><br />
			<table class="listTable">
				<tr>
					<th class="listTableHead" colspan="2"> {lng p="pacc_activesubscription"}</th>
				</tr>
				<tr>
					<td class="listTableLeft">{lng p="pacc_package"}:</td>
					<td class="listTableRight">
						<img src="plugins/templates/images/pacc_packages48.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						{text value=$activeSubscription.package.titel}
					</td>
				</tr>
				<tr>
					<td class="listTableLeft">{lng p="pacc_lastpayment"}:</td>
					<td class="listTableRight">
						{date timestamp=$activeSubscription.letzte_zahlung}
					</td>
				</tr>
				<tr>
					<td class="listTableLeft">{lng p="pacc_validuntil"}:</td>
					<td class="listTableRight">
						{if $activeSubscription.ablauf<=1}({lng p="unlimited"}){else}{date timestamp=$activeSubscription.ablauf}{/if}
					</td>
				</tr>
			</table>
			<br />
			<input type="button" value="&laquo; {lng p="back"}" onclick="history.back();" />
			<input type="button" class="primary" value=" {lng p="pacc_next"} (30) " onclick="document.location.href='prefs.php?action=membership&do=cancelAccount&paccContinue=true&sid={$sid}';" disabled="disabled" id="cancelButton" />
		</td>
	</tr>
</table>


<script language="javascript">
<!--
	{literal}var i = 30;

	function cancelTimer()
	{
		i--;

		if(i==0)
		{
			EBID('cancelButton').value = '{/literal}{lng p="pacc_next"}{literal} >>';
			EBID('cancelButton').disabled = false;
		}
		else
		{
			EBID('cancelButton').value = '{/literal}{lng p="pacc_next"}{literal} (' + i + ')';
			window.setTimeout('cancelTimer()', 1000);
		}
	}

	window.setTimeout('cancelTimer()', 1000);{/literal}
//-->
</script>

{if $_tplname=='modern'}
</div></div>
{/if}
