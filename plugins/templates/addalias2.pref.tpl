<fieldset>
	<legend>{lng p="addalias_name"}</legend>
	
	<table>
		<tr>
			<td width="48"><img src="../plugins/templates/images/addalias_logo.png" width="48" height="48" border="0" alt="" /></td>
			<td width="10">&nbsp;</td>
			<td><b>{lng p="addalias_name"}</b><br />{lng p="addalias_text"}</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend>Wie erstelle ich einen Alias f&uuml;r einen Benutzer?</legend>

	<p>W&auml;hlen Sie die Gruppe aus, in der Benutzer ist, dem Sie den Alias erstellen wollen.<br />
	Nach dem Klicken auf Weiter, werden Ihnen die Benutzer aus der Gruppe angezeigt. W&auml;hlen Sie dort den Benutzer aus.<br />
	Danach k&ouml;nnen Sie entweder die externe Adresse (zum senden) oder die interne Adresse (zum empfangen und senden) eintragen.<br />
	Nach dem Klicken auf "Ausf&uuml;hren" wird der Alias erstellt.</p>
</fieldset>