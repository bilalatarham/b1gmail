<fieldset>
	<legend>{lng p="updates"}</legend>
	{lng p="mb_app_updating"}
	<div id="updateSpinner"></div>	
</fieldset>
<script>
{literal}
	spin(EBID("updateSpinner"));
{/literal}
</script>