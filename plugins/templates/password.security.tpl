<div id="password-security">
	<div id="password-security-smallletters"><i class="fa fa-smile-o"></i><i class="fa fa-meh-o"></i> {lng p="pwsecurity_smallletters"}</div>
	<div id="password-security-capitalletters"><i class="fa fa-smile-o"></i><i class="fa fa-meh-o"></i> {lng p="pwsecurity_capitalletters"}</div>
	<div id="password-security-digits"><i class="fa fa-smile-o"></i><i class="fa fa-meh-o"></i> {lng p="pwsecurity_digits"}</div>
	<div id="password-security-specialchars"><i class="fa fa-smile-o"></i><i class="fa fa-meh-o"></i> {lng p="pwsecurity_specialchars"}</div>
</div>