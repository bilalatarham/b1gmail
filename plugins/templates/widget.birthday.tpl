<div class="innerWidget">
 <table align="center" cellpadding="0" cellspacing="0" class="moduletable">
  <tr>
   <td>
    <table border="0" cellspacing="1" style="border-collapse: collapse;" width="100%" id="wdtable1">
     <tr>
      <td>
	{if !empty($birthday)}
		{foreach from=$birthday key=myId item=i}
			{if $i.alter > 0}
				<div align="left">&bull; <a href="{$i.link}" title="Geburtstags - E-Mail versenden!">{$i.vorname} {$i.nachname} wird {if $i.tage==0}<b>heute</b>{else}{if $i.tage==1}<b>morgen</b>{elseif $i.tage==2}<b>&uuml;bermorgen</b>{else}in {$i.tage} Tagen{/if}{/if} {$i.alter} Jahr{if $i.alter!=1}e{/if} alt.</a></div>
			{/if}
		{/foreach}
	{else}
		<div align="left">&bull; Keine Geburtstage innerhalb der n&auml;chsten {$vorlaufzeit} Tage.</div>
	{/if}
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </table>
</div>  