<fieldset>
    <legend>{lng p="dsDerefTitle"} {lng p="dsEdit"}</legend>

    {$MSG}

    <form action="{$pageURL}&do=deref&sid={$sid}"method="post" onsubmit="EBID('derefstatus').focus();if(EBID('derefstatus').value.length<2) return(false);editor.submit();spin(this)">
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="dsDerefStatus"}:</td>
                <td class="td2">
                    <input id="derefstatus" name="derefstatus" type="checkbox" {if $DerefStatus == 'yes'}checked="checked"{/if} />
                    <label for="autopdfgen"><b>{lng p="dsDerefStatusBesch"}</b></label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
                    <textarea name="dereftemplate" id="dereftemplate" style="width:100%;height:300px;">{$DerefTemplate}</textarea>
                </td>
            </tr>
        </table>

        <p align="right">
            <input class="button" type="submit" name="derefsave" value=" {lng p="dsEdit"} " />
        </p>
    </form>
</fieldset>