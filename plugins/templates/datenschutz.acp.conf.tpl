<form action="{$pageURL}&do=conf&sid={$sid}" method="post">
    {$MSG}

    <fieldset>
        <legend>{lng p="dsAllgemein"}</legend>
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="dsShowImpressum"}:</td>
                <td class="td2">
                    <input id="showimpressum" name="showimpressum" type="checkbox" {if $ShowImpressum == 'yes'}checked="checked"{/if} />
                    <label for="showimpressum"><b>{lng p="dsShowImpressumBesch"}</b></label>
                </td>
            </tr>
            <tr>
                <td class="td1">{lng p="dsAutoPDFGen"}:</td>
                <td class="td2">
                    <input id="autopdfgen" name="autopdfgen" type="checkbox" {if $AutoPDFGen == 'yes'}checked="checked"{/if} />
                    <label for="autopdfgen"><b>{lng p="dsAutoPDFGenBesch"}</b></label>
                </td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="dsReportKosten"}:</td>
                <td class="td2">
                    <input type="text" id="reportkosten" name="reportkosten" style="width:100%;" value="{$ReportKosten}" />
                </td>
            </tr>
        </table>
    </fieldset>

    <table style="width: 100%; margin: 0px; padding: 0px;">
        <tr style="vertical-align: top;">
            <td style="width: 50%; margin: 0px; padding: 0px;">
                <fieldset>
                    <legend>{lng p="dsGesetzestexte"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsDatenschutz"}:</td>
                            <td class="td2"><a href="{$pageURL}&do=text&sid={$sid}">{lng p="dsSystemLink"}</a></td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="text_tos"}:</td>
                            <td class="td2"><a href="prefs.languages.php?action=texts&sid={$sid}">{lng p="dsSystemLink"}</a></td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="text_imprint"}:</td>
                            <td class="td2"><a href="prefs.languages.php?action=texts&sid={$sid}">{lng p="dsSystemLink"}</a></td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend>{lng p="dsProfileFelder"}</legend>
                    <table width="100%">
                        {foreach from=$DBPFs item=pfs}
                            <tr>
                                <td class="td1" width="200">{$pfs.feld}:</td>
                                <td class="td2" width="200">
                                    <select name="pfsAnzeige_{$pfs.id}">
                                        <option value="AllgemeinStart" {if $pfs.show.Anzeigen == 'AllgemeinStart'}selected{/if}>Allgemein: Start</option>
                                        <option value="AllgemeinEnde" {if $pfs.show.Anzeigen == 'AllgemeinEnde'}selected{/if}>Allgemein: Ende</option>
                                    </select>
                                </td>
                                <td class="td2">
                                    <input id="pfs_{$pfs.id}" name="pfs[]" value="{$pfs.id}" type="checkbox" {if $pfs.show.PFID == $pfs.id}checked="checked"{/if} />
                                    <label for="pfs_{$pfs.id}"><b>{lng p="dsAktivieren"}</b></label>
                                </td>
                            </tr>
                        {/foreach}
                    </table>
                </fieldset>
            </td>
            <td style="width: 50%; margin: 0px; padding: 0px;">
                <fieldset>
                    <legend>{lng p="dsPDFOptionen"}</legend>
                    <i>{lng p="dsPDFOptionenInfo"}</i>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFMails"}:</td>
                            <td class="td2">
                                <input id="pdfmails" name="pdfmails" type="checkbox" {if $PDFMails == 'yes'}checked="checked"{/if} />
                                <label for="pdfmails"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFKalender"}:</td>
                            <td class="td2">
                                <input id="pdfkalender" name="pdfkalender" type="checkbox" {if $PDFKalender == 'yes'}checked="checked"{/if} />
                                <label for="pdfkalender"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFKontakte"}:</td>
                            <td class="td2">
                                <input id="pdfkontakte" name="pdfkontakte" type="checkbox" {if $PDFKontakte == 'yes'}checked="checked"{/if} />
                                <label for="pdfkontakte"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFNotizen"}:</td>
                            <td class="td2">
                                <input id="pdfnotizen" name="pdfnotizen" type="checkbox" {if $PDFNotizen == 'yes'}checked="checked"{/if} />
                                <label for="pdfnotizen"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFAufgaben"}:</td>
                            <td class="td2">
                                <input id="pdfaufgaben" name="pdfaufgaben" type="checkbox" {if $PDFAufgaben == 'yes'}checked="checked"{/if} />
                                <label for="pdfaufgaben"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFDaten"}:</td>
                            <td class="td2">
                                <input id="pdfdaten" name="pdfdaten" type="checkbox" {if $PDFDaten == 'yes'}checked="checked"{/if} />
                                <label for="pdfdaten"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsPDFBest"}:</td>
                            <td class="td2">
                                <input id="pdfbest" name="pdfbest" type="checkbox" {if $PDFBest == 'yes'}checked="checked"{/if} />
                                <label for="pdfbest"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsGesetzestexte"}:</td>
                            <td class="td2">
                                <input id="pdfgesetz" name="pdfgesetz" type="checkbox" {if $PDFGesetzestexte == 'yes'}checked="checked"{/if} />
                                <label for="pdfgesetz"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 0px; padding: 0px;">
        <tr style="vertical-align: top;">
            <td style="width: 50%; margin: 0px; padding: 0px;">
                <fieldset>
                    <legend>{lng p="dsLogsWebAcc"}</legend>
                    <i>{lng p="dsLogsInfo"}</i>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsStatus"}:</td>
                            <td class="td2">
                                <input id="logwebaccstatus" name="logwebaccstatus" type="checkbox" {if $LogWebAccStatus == 'yes'}checked="checked"{/if} />
                                <label for="logwebaccstatus"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsInhalt"}:</td>
                            <td class="td2">
                                <input type="text" id="logwebaccinhalt" name="logwebaccinhalt" style="width:100%;" value="{$LogWebAccInhalt}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsZeit"}:</td>
                            <td class="td2">
                                <input type="text" id="logwebaccinter" name="logwebaccinter" style="width:80%;" value="{$LogWebAccInter}" /> Tage
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend>{lng p="dsLogsWebErr"}</legend>
                    <i>{lng p="dsLogsInfo"}</i>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsStatus"}:</td>
                            <td class="td2">
                                <input id="logweberrstatus" name="logweberrstatus" type="checkbox" {if $LogWebErrStatus == 'yes'}checked="checked"{/if} />
                                <label for="logweberrstatus"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsInhalt"}:</td>
                            <td class="td2">
                                <input type="text" id="logweberrinhalt" name="logweberrinhalt" style="width:100%;" value="{$LogWebErrInhalt}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsZeit"}:</td>
                            <td class="td2">
                                <input type="text" id="logweberrinter" name="logweberrinter" style="width:80%;" value="{$LogWebErrInter}" /> Tage
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td style="width: 50%; margin: 0px; padding: 0px;">


                <fieldset>
                    <legend>{lng p="dsLogsB1GMail"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsStatus"}:</td>
                            <td class="td2">
                                <input id="logb1gstatus" name="logb1gstatus" type="checkbox" {if $LogB1GStatus == 'yes'}checked="checked"{/if} />
                                <label for="logb1gstatus"><b>{lng p="dsAnzeigen"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsArchiv"}:</td>
                            <td class="td2">
                                <input id="logb1garchive" name="logb1garchive" type="checkbox" {if $LogB1GArchive == 'yes'}checked="checked"{/if} />
                                <label for="logb1garchive"><b>{lng p="dsAktivieren"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsCron"}:</td>
                            <td class="td2">
                                <input id="logb1gcron" name="logb1gcron" type="checkbox" {if $LogB1GCron == 'yes'}checked="checked"{/if} />
                                <label for="logb1gcron"><b>{lng p="dsAktivieren"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsInhalt"}:</td>
                            <td class="td2">
                                <input type="text" id="logb1ginhalt" name="logb1ginhalt" style="width:100%;" value="{$LogB1GInhalt}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsZeit"}:</td>
                            <td class="td2">
                                <input type="text" id="logb1ginter" name="logb1ginter" style="width:80%;" value="{$LogB1GInter}" /> Tage
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsLastLog"}:</td>
                            <td class="td2">
                                <input type="text" style="width:100%;" value="{$B1GMailLogLastDate}" disabled />
                            </td>
                        </tr>
                    </table>
                </fieldset>

                {if $LogB1GSInstalled == 1}
                    <fieldset>
                        <legend>{lng p="dsLogsB1GMailServer"}</legend>
                        <table width="100%">
                            <tr>
                                <td class="td1" width="180">{lng p="dsStatus"}:</td>
                                <td class="td2">
                                    <input id="logb1gsstatus" name="logb1gsstatus" type="checkbox" {if $LogB1GSStatus == 'yes'}checked="checked"{/if} />
                                    <label for="logb1gsstatus"><b>{lng p="dsAnzeigen"}</b></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="td1" width="180">{lng p="dsArchiv"}:</td>
                                <td class="td2">
                                    <input id="logarchivestatus" name="logarchivestatus" type="checkbox" {if $LogB1GSArchive == 1}checked="checked"{/if} />
                                    <label for="logarchivestatus"><b>{lng p="dsAktivieren"}</b></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="td1" width="180">{lng p="dsCron"}:</td>
                                <td class="td2">
                                    <input id="logb1gscron" name="logb1gscron" type="checkbox" {if $LogB1GSCron == 1}checked="checked"{/if} />
                                    <label for="logb1gscron"><b>{lng p="dsAktivieren"}</b></label>
                                </td>
                            </tr>
                            <tr>
                                <td class="td1" width="180">{lng p="dsInhalt"}:</td>
                                <td class="td2">
                                    <input type="text" id="logb1gsinhalt" name="logb1gsinhalt" style="width:100%;" value="{$LogB1GSInhalt}" />
                                </td>
                            </tr>
                            <tr>
                                <td class="td1" width="180">{lng p="dsZeit"}:</td>
                                <td class="td2">
                                    <input type="text" id="logb1gsinter" name="logb1gsinter" style="width:80%;" value="{$LogB1GSInter}" /> Tage
                                </td>
                            </tr>
                            <tr>
                                <td class="td1" width="180">{lng p="dsLastLog"}:</td>
                                <td class="td2">
                                    <input type="text" style="width:100%;" value="{$B1GMailSLogLastDate}" disabled />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                {/if}

                <fieldset>
                    <legend>{lng p="dsLogArchive"}</legend>
                    <table width="100%">
                        <tr>
                            <td class="td1" width="180">{lng p="dsCron"}:</td>
                            <td class="td2">
                                <input id="logarchivecron" name="logarchivecron" type="checkbox" {if $LogArchiveCron == 'yes'}checked="checked"{/if} />
                                <label for="logarchivecron"><b>{lng p="dsAktivieren"}</b></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1" width="180">{lng p="dsZeit"}:</td>
                            <td class="td2">
                                <input type="text" id="logarchiveinter" name="logarchiveinter" style="width:80%;" value="{$LogArchiveInter}" /> Tage
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>




    <p align="right" style="padding-right: 3px;">
        <input class="button" type="submit" name="confsave" value=" {lng p="dsSave"} " />
    </p>
</form>