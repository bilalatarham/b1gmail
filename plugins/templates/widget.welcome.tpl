<div class="innerWidget">
	{* {$bmwidget_welcome_welcomeText}<br /><br />
	
	<a href="email.php?sid={$sid}"><img width="16" height="16" src="{$tpldir}images/li/menu_ico_inbox.png" align="absmiddle" border="0" />
	{$bmwidget_welcome_mails}</a><br />
	
	<a href="organizer.calendar.php?sid={$sid}"><img width="16" height="16" src="{$tpldir}images/li/ico_calendar.png" align="absmiddle" border="0" />
	{$bmwidget_welcome_dates}</a><br />
	
	<a href="organizer.todo.php?sid={$sid}"><img width="16" height="16" src="{$tpldir}images/li/ico_todo.png" align="absmiddle" border="0" />
	{$bmwidget_welcome_tasks}</a><br /> *}

	<div class="dragable-box content-box">
		<div class="box-title">
			<span class="tl-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-welcome.svg" class="img-fluid" />
			</span>
			<h3 class="tl-title fw-med">Welcome</h3>
		</div>
		<div class="widget-welcome">
			<p>{$bmwidget_welcome_welcomeText}</p>
			<div class="notifications">
				<a href="email.php?sid={$sid}" class="notif-item">
					<span class="notif-icon">
						<img src="{$tpldir}frontend_assets/img/ic-message.svg" class="img-fluid" />
					</span>
					<span class="notif-dot"></span>
					<span class="notif-text">{$bmwidget_welcome_mails}</span>
				</a>
				<a href="organizer.calendar.php?sid={$sid}" class="notif-item">
					<span class="notif-icon">
						<img src="{$tpldir}frontend_assets/img/menu-ic-calendar.svg" class="img-fluid" />
					</span>
					<span class="notif-dot"></span>
					<span class="notif-text">{$bmwidget_welcome_dates}</span>
				</a>
				<a href="organizer.todo.php?sid={$sid}" class="notif-item">
					<span class="notif-icon">
						<img src="{$tpldir}frontend_assets/img/ic-clipboard.svg" class="img-fluid" />
					</span>
					<span class="notif-dot"></span>
					<span class="notif-text">{$bmwidget_welcome_tasks}</span>
				</a>
			</div>
		</div>
	</div>
</div>