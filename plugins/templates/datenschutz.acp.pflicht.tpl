<div style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px; color: #333; background-color: #f5f5f5; border-color: #ddd;">
    {lng p="dsPflichtBeshr"}
</div>

<fieldset>
    <legend>{lng p="group"}</legend>

    <form action="{$pageURL}&do=pflicht&mg={$MailGruppe}&sid={$sid}" method="post">
        <center>
            <table>
                <tr>
                    <td>{lng p="group"}:</td>
                    <td><select name="mg">
                            {foreach from=$MailGruppen item=mg}
                                <option value="{$mg.ID}" {if $mg.ID==$MailGruppe}selected="selected"{/if}>{$mg.Titel}</option>
                            {/foreach}
                        </select></td>
                    <td><input class="button" type="submit" value=" {lng p="ok"} &raquo; " /></td>
                </tr>
            </table>
        </center>
    </form>
</fieldset>

{if $MailGruppe}
    <form action="{$pageURL}&do=pflicht&mg={$MailGruppe}&sid={$sid}" method="post" enctype="multipart/form-data">
        {$MSG}

        <fieldset>
            <legend>{lng p="fields"}</legend>

            <table class="list">
                <tr>
                    <th width="20">&nbsp;</th>
                    <th>{lng p="field"}</th>
                    <th width="120">{lng p="oblig"}</th>
                    <th width="120">{lng p="available"}</th>
                    <th width="120">{lng p="notavailable"}</th>
                    <th width="120">{lng p="dsPflichtNicht"}</th>
                </tr>

                <tr class="td1">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="salutation"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_anrede" value="p"{if $f_anrede_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_anrede" value="v"{if $f_anrede_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_anrede" value="n"{if $f_anrede_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_anrede" value=""{if $f_anrede_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td2">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="firstname"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_vorname" value="p"{if $f_vorname_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_vorname" value="v"{if $f_vorname_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_vorname" value="n"{if $f_vorname_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_vorname" value=""{if $f_vorname_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td1">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="lastname"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_nachname" value="p"{if $f_nachname_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_nachname" value="v"{if $f_nachname_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_nachname" value="n"{if $f_nachname_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_nachname" value=""{if $f_nachname_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td2">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="address"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_strasse" value="p"{if $f_strasse_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_strasse" value="v"{if $f_strasse_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_strasse" value="n"{if $f_strasse_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_strasse" value=""{if $f_strasse_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td1">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="tel"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_telefon" value="p"{if $f_telefon_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_telefon" value="v"{if $f_telefon_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_telefon" value="n"{if $f_telefon_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_telefon" value=""{if $f_telefon_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td2">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="fax"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_fax" value="p"{if $f_fax_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_fax" value="v"{if $f_fax_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_fax" value="n"{if $f_fax_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_fax" value=""{if $f_fax_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td1">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="altmail"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_alternativ" value="p"{if $f_alternativ_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_alternativ" value="v"{if $f_alternativ_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_alternativ" value="n"{if $f_alternativ_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_alternativ" value=""{if $f_alternativ_status==''} checked="checked"{/if} /></td>
                </tr>
                <tr class="td2">
                    <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                    <td>{lng p="cellphone"}</td>
                    <td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value="p"{if $f_mail2sms_nummer_status=='p'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value="v"{if $f_mail2sms_nummer_status=='v'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value="n"{if $f_mail2sms_nummer_status=='n'} checked="checked"{/if} /></td>
                    <td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value=""{if $f_mail2sms_nummer_status==''} checked="checked"{/if} /></td>
                </tr>
            </table>
        </fieldset>

        <!--
        <fieldset>
            <legend>{lng p="profilefields"}</legend>

            <table class="list">
                <tr>
                    <th width="20">&nbsp;</th>
                    <th>{lng p="field"}</th>
                    <th width="165">{lng p="oblig"}</th>
                    <th width="165">{lng p="notavailable"}</th>
                </tr>
                {foreach from=$DBPFs item=pfs}
                    <tr class="{$pfs.class}">
                        <td><img src="{$tpldir}images/field.png" border="0" alt="" width="16" height="16" /></td>
                        <td>{$pfs.feld}</td>
                        <td style="text-align:center;"><input type="radio" name="{$pfs.id}" value="yes"{if $pfs.status=='yes'} checked="checked"{/if} /></td>
                        <td style="text-align:center;"><input type="radio" name="{$pfs.id}" value="no"{if $pfs.status=='no'} checked="checked"{/if} /></td>
                    </tr>
                {/foreach}
            </table>
        </fieldset>
        -->

        <p align="right" style="padding-right: 3px;">
            <input class="button" type="submit" name="pflichtsave" value=" {lng p="dsSave"} " />
        </p>
    </form>
{/if}