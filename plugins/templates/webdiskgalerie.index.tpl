<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{$service_title}{if $pageTitle} - {text value=$pageTitle}{/if}</title>
	
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{$tpldir}style/notloggedin.css"/>
	{if $isgalerie}<link rel="stylesheet" type="text/css" href="./plugins/css/webdiskgalerie_gal.css" />{/if}
	{if $isslideshow}<link rel="stylesheet" type="text/css" href="./plugins/css/webdiskgalerie_side.css" />{/if}
	
	<script type="text/javascript" src="./plugins/js/prototype.js"></script>
<script type="text/javascript" src="./plugins/js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="./plugins/js/lightbox.js"></script>
</head>
<!-- body -->
<body>
	{include file="$page"}
</body>
</html>