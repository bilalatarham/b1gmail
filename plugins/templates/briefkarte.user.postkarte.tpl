
<h1><img src="./plugins/templates/images/modbriefkarte_bk_send.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="bk_karte"}</h1>

<form name="f1" method="post" action="start.php?action=briefkarte&do=preview&sid={$sid}" enctype="multipart/form-data">
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2">{lng p="bk_versandnach"}</th>
		</tr>		
		<tr>
			<td bgcolor="#f3f3f3" align="center">
				<br />
					<input type="radio" name="bk_land" value="de" checked="checked"> {lng p="bk_de"}
    				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="bk_land" value="eu"> {lng p="bk_welt"}
				<br /><br />
			</td>
		</tr>
	</table>
	<br />
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2">{lng p="bk_dateiupload"}</th>
		</tr>		
		<tr>
			<td bgcolor="#f3f3f3" align="center">
				<br /><input type="file" name="bk_datei" size="40"><br /><br />
			</td>
		</tr>
		<tr>
			<td class="listTableRightDesc" colspan="2" style="padding: 3px 0px 3px 5px;"><small>{lng p="bk_dateiupload_info"}</small></td>
		</tr>
	</table>
	<br />
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="bk_from"}</th>
		</tr>
		<tr>
			<td class="listTableLeft">{lng p="bk_to_z0"}:</td>
			<td class="listTableRight"><input onfocus="this.select()" type="text" size="75" id="bk_pk_feld_0" name="bk_pk_feld_0" value="{text value=$bk_pk_feld_0 allowEmpty=true}"></td>
		</tr>
		<tr>
			<td class="listTableLeft">* {lng p="bk_from_z1"}:</td>
			<td class="listTableRight"><input onfocus="this.select()" type="text" size="75" id="bk_pk_feld_1" name="bk_pk_feld_1" value="{text value=$bk_pk_feld_1 allowEmpty=true}"></td>
		</tr>
		<tr>
			<td class="listTableLeftDescBottomLine" style="height:5px;"></td>
			<td class="listTableRightDesc" style="height:5px;"></td>
		</tr>
	</table>
	<br />
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="bk_to"}</th>
		</tr>
		<tr>
			<td class="listTableLeft">{lng p="bk_to_z0"}:</td>
			<td class="listTableRight"><input onfocus="this.select()" type="text" size="75" id="bk_pk_feld_4" name="bk_pk_feld_4" value=""></td>
		</tr>
		<tr>
			<td class="listTableLeft">* {lng p="bk_to_z1"}:</td>
			<td class="listTableRight"><input onfocus="this.select()" type="text" size="75" id="bk_pk_feld_5" name="bk_pk_feld_5" value="">
                <span id="addrDiv_to">
					<a href="javascript:openPkAddressbook()">
						<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						{lng p="fromaddr"}
					</a>
				</span>
            </td>
		</tr>
		<tr>
			<td class="listTableLeft">* {lng p="bk_to_z2"}:</td>
			<td class="listTableRight"><input onfocus="this.select()" type="text" size="75" id="bk_pk_feld_6" name="bk_pk_feld_6" value=""></td>
		</tr>
		<tr>
			<td class="listTableLeft">* {lng p="bk_to_z3"}:</td>
			<td class="listTableRight"><input onfocus="this.select()" type="text" size="75" id="bk_pk_feld_7" name="bk_pk_feld_7" value=""></td>
		</tr>
		<tr>
			<td class="listTableLeftDescBottomLine" style="height:5px;"></td>
			<td class="listTableRightDesc" style="height:5px;"></td>
		</tr>
		<tr>
			<td class="listTableCompose" colspan="2">
				<textarea class="composeTextarea" name="bk_text_pk" id="bk_text_pk" style="width:100%;height:180px;" onKeyUp="updateMaxPostkarteChars(this); maxEnters(this,11);" ></textarea>
			</td>
		</tr>
		<tr>
			<td class="listTableLeftDescTopLine">{lng p="chars"}:</td>
			<td class="listTableRightDesc">
				<div style="float:left; padding-top: 2px;">
					{progressBar value=0 max=750 width=100 name="charCountBar"}
				</div>
				<div style="float:left">
					&nbsp;
				</div>
				<div style="float:left" id="charCountDiv">
					0 / 750
				</div>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding: 5px 5px 5px 5px;">
				<input type="button" value="{lng p="bk_vorschau"}" id="sendButton" onclick="if(!checkBriefkarteComposeForm() || !checkMaxAbsenderZeichen()) return(false); document.forms.f1.submit();" />
				<input type="reset" value="{lng p="reset"}" onclick="return askReset();"/>
			</td>
		</tr>
	</table>
</form>

{literal}
<script language="javascript">
<!--
	function checkBriefkarteComposeForm()
	{
		if(((EBID('bk_pk_feld_1') && EBID('bk_pk_feld_1').value.length < 3)
			|| (EBID('bk_pk_feld_5') && EBID('bk_pk_feld_5').value.length < 3)
			|| (EBID('bk_pk_feld_6') && EBID('bk_pk_feld_6').value.length < 3)
			|| (EBID('bk_pk_feld_7') && EBID('bk_pk_feld_7').value.length < 3)
			|| EBID('bk_text_pk').value.length) < 3)	
		{
			alert(lang['fillin']);
			return(false);
		}
		return(true);
	}
	
	function checkMaxAbsenderZeichen()
	{
		if((EBID('bk_pk_feld_0').value.length
			+ EBID('bk_pk_feld_1').value.length) > 50)
		{
			alert('{/literal}{lng p="bk_absender_error"}{literal}');
			return(false);
		}
		return(true);
	}
	
	function updateMaxPostkarteChars(field)
	{
		var length = field.value.length;
		
		// crop, if needed - pr�ft max. zeichen
		if(length > 750)
			field.value = field.value.substring(0, 750);
		length = field.value.length;
		
		// update text
		EBID('charCountDiv').innerHTML = length + ' / ' + 750;
		
		// progressbar width?
		var pbWidth = 0;
		if(isNaN(parseInt(EBID('pb_charCountBar_value').width)))
			pbWidth = parseInt(EBID('pb_charCountBar_free').width);
		else
			pbWidth = parseInt(EBID('pb_charCountBar_value').width) + parseInt(EBID('pb_charCountBar_free').width);
		var newValueWidth = Math.ceil(pbWidth/750 * length),
			newFreeWidth = pbWidth - newValueWidth;
		EBID('pb_charCountBar_free').style.width = newFreeWidth + 'px';
		EBID('pb_charCountBar_value').style.width = ((newValueWidth == 0) ? 1 : newValueWidth) + 'px';
	}

	function maxEnters(textarea,limit)
	{
		var anz = limit-1;
		var val=textarea.value.replace(/\r/g,'').split('\n');
		if(val.length>limit) 
        {
			alert('{/literal}{lng p="bk_enter"}{literal} '+anz+'x !');
			textarea.value=val.slice(0,-1).join('\n');
		}
	}
    
   	function openPkAddressbook()
	{	
		openOverlay('start.php?action=briefkarte&do=addressBook&sid=' + currentSID,
			lang['addressbook'],
			450,
			380,
			true);
	}
//-->
</script>
{/literal}