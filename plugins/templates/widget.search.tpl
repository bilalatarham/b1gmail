<div class="innerWidget" style="text-align:center;">
	{* <form action="start.php?sid={$sid}&action=search" method="post" target="_blank">
		<table cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td><input type="text" name="q" value="" style="width:100%;" /></td>
				<td width="105" align="right"><input type="submit" value=" {lng p="websearch"} " /></td>
			</tr>
		</table>
	</form> *}
	<div class="dragable-box content-box">
		<div class="box-title">
			<span class="tl-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-search.svg" class="img-fluid" />
			</span>
			<h3 class="tl-title fw-med">Web Search</h3>
		</div>
		<div class="widget-search pt-2 pb-2">
			<div class="search-form">
				<form action="start.php?sid={$sid}&action=search" method="post" target="_blank">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">
								<img src="{$tpldir}frontend_assets/img/ic-search.svg" class="img-fluid" />
							</span>
						</div>
						<input name="q" value="" type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon1">
						<input class="btn-primary search-btn" type="submit" value=" {lng p="websearch"} " />
					</div>
				</form>
			</div> 
		</div>
	</div>
</div>