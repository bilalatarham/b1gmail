<div id="contentHeader">
	<div class="left">
		<img src="{$tpldir}images/li/ico_composesms.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		{lng p="sendfreesms"}
	</div>
</div>


<div class="scrollContainer"><div class="pad">
<form name="f1" method="post" action="sms.php?action=freesms&do=sendFreeSMS&sid={$sid}">
	<table class="listTable">
		<tr>
			<th class="listTableHead" colspan="2"> {lng p="sendfreesms"}</th>
		</tr>
		<tr>
			<td class="listTableLeftDescBottomLine">* {lng p="to"}:</td>
			<td class="listTableRightDesc">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td width="364">
							{mobileNr name="to" size="350px"}
						</td>
						<td>
							<span id="addrDiv_to">
								<a href="javascript:openCellphoneAddressbook('{$sid}')">
									<img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
									{lng p="fromaddr"}
								</a>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="listTableCompose" colspan="2">
				<textarea class="composeTextarea" name="FreeSmsText" id="FreeSmsText" style="width:100%;height:180px;" onkeyup="updateMaxCharsFreeSMS(this, {$maxChars})"></textarea>
			</td>
		</tr>
		<tr>
			<td class="listTableLeftDescTopLine">{lng p="chars"}:</td>
			<td class="listTableRightDesc">
				<div style="float:left; padding-top: 2px;">
					{progressBar value=0 max=$maxChars width=100 name="charCountBar"}
				</div>
				<div style="float:left">
					&nbsp;&nbsp;
				</div>
				<div style="float:left" id="charCountDiv">
					0 / {$maxChars}
				</div>
				<div style="float:left">
					&nbsp;&bull; {lng p="freesms_heute"}: {if !$UserFreeSMSday}0{else}{$UserFreeSMSday}{/if}/{if !$GrpFreeSMSday}0{else}{$GrpFreeSMSday}{/if}
				</div>
				<div style="float:left">
					&nbsp;&bull; {lng p="freesms_monat"}: {if !$UserFreeSMSmonth}0{else}{$UserFreeSMSmonth}{/if}/{if !$GrpFreeSMSmonth}0{else}{$GrpFreeSMSmonth}{/if}
				</div>
			</td>
		</tr>
		<tr>
			<td class="listTableLeft">&nbsp;</td>
			<td class="listTableRight" style="padding: 5px 5px 5px 5px;">
				<input type="button" value="{lng p="send2sms"}" id="sendButton" onclick="if(!checkFreeSmsComposeForm()) return(false); document.forms.f1.submit();" {if $freeCredits==false} disabled="disabled"{/if}/>
				<input type="reset" value="{lng p="reset"}" onclick="return askReset();"/>
				{if $freeCredits==false}<div class="note" id="errorMsg">{lng p="freesmswarning"}</div>{/if}
			</td>
		</tr>
	</table>
</form>

{literal}
<script language="javascript">
<!--
	function checkFreeSmsComposeForm()
	{
		if((EBID('to') && EBID('to').value.length < 3)
			|| (EBID('to_no') && EBID('to_no').value.length < 3)
			|| EBID('FreeSmsText').value.length < 3)
		{
			alert(lang['fillin']);
			return(false);
		}
		return(true);
	}
	
	function updateMaxCharsFreeSMS(field, maxChars)
	{
		var length = field.value.length;
		
		// crop, if needed
		if(length > maxChars)
			field.value = field.value.substring(0, maxChars);
		length = field.value.length;
							
		// update text
		EBID('charCountDiv').innerHTML = length + ' / ' + maxChars;
		
		// progressbar width?
		var pbWidth = parseInt(EBID('pb_charCountBar').style.width);
		var newValueWidth = Math.ceil(pbWidth/maxChars * length);
		if(newValueWidth > pbWidth-2)
			newValueWidth = pbWidth-2;
		EBID('pb_charCountBar_value').style.width = ((newValueWidth == 0) ? 1 : newValueWidth) + 'px';
		
		//alte version
		//var pbWidth = 0;
		//if(isNaN(parseInt(EBID('pb_charCountBar_value').width)))
		//	pbWidth = parseInt(EBID('pb_charCountBar_free').width);
		//else
		//	pbWidth = parseInt(EBID('pb_charCountBar_value').width) + parseInt(EBID('pb_charCountBar_free').width);
		//var newValueWidth = Math.ceil(pbWidth/maxChars * length),
		//	newFreeWidth = pbWidth - newValueWidth;
		//EBID('pb_charCountBar_free').width = newFreeWidth;
		//EBID('pb_charCountBar_value').width = newValueWidth == 0 ? 1 : newValueWidth;
	}
	
	function openCellphoneAddressbook(sid)
	{	
		openOverlay('organizer.addressbook.php?sid=' + sid + '&action=numberPopup',
			lang['addressbook'],
			450,
			380,
			true);
	}
	
//-->
</script>
{/literal}

</div></div>