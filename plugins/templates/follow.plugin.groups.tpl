<script src="../plugins/templates/follow.plugin.overlibjs.tpl" type="text/javascript" language="javascript"></script>

<fieldset>
        <legend>{lng p="groups"}</legend>

        <form name="f1" action="{$pageURL}&action=groups&sid={$sid}" method="post">
        <table class="list">
                <tr>
                        <th width="20">&nbsp;</th>
                        <th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'group_');"><img src="{$tpldir}images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
                        <th>{lng p="title"}</th>
                        <th>{lng p="follow_lang_group_to"}</th>
                        <th>{lng p="follow_lang_versand"} {lng p="follow_lang_email"}</th>
                        <th>{lng p="follow_lang_group_abs"}</th>
                        <th width="70">{lng p="follow_lang_group_resp"}</th>
                        <th width="85">&nbsp;</th>
                </tr>

                {foreach from=$groups item=group}
                {cycle name=class values="td1,td2" assign=class}
                <tr class="{$class}">
                        <td align="center"><img src="{$tpldir}images/ico_group{if $group.follow_resp_aktiv=="yes"}_default{/if}.png" border="0" width="16" height="16" alt="" /></td>
                        <td align="center"><input type="checkbox" name="group_{$group.id}" /></td>
                        {if $group.follow_resp_aktiv=="yes"}
                        <td><a href="{$pageURL}&action=groups&do=resp&groupID={$group.id}&sid={$sid}">{text value=$group.titel}</a><br /><small>{$group.members} {lng p="members"}</small></td>
                        <td>{$group.membersResp} {lng p="members"}</td>
                        <td>{if $group.sendto=="altmails"}{lng p="altmails"}{else}{lng p="mailboxes"}{/if}</td>
                        <td>{$group.from}</td>
                        {else}
                        <td colspan="4">{text value=$group.titel}<br /><small>{$group.members} {lng p="members"}</small></td>
                        {/if}
                        <td align="center">{$group.respG}<br /><small><font color="#008000">{$group.respA}</font> / <font color="#FF0000">{$group.respD}</font></small></td>
                        <td>
                                {if $group.follow_resp_aktiv=="yes"}
                                <a href="{$pageURL}&action=groups&do=aktiv&groupID={$group.id}&sid={$sid}"><img src="{$tpldir}images/edit.png" border="0" alt="{lng p="edit"}" width="16" height="16" /></a>
                                <a href="{$pageURL}&action=groups&do=inaktiv&groupID={$group.id}&sid={$sid}" onclick="return confirm('{lng p="realdel"}');"><img src="{$tpldir}images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                                &nbsp;<a href="{$pageURL}&action=groups&do=resp&groupID={$group.id}&sid={$sid}"><img src="../plugins/templates/images/follow_plugin_grouplist.png" border="0" alt="" width="16" height="16" /></a>
                                {else}
                                <a href="{$pageURL}&action=groups&do=aktiv&groupID={$group.id}&sid={$sid}"><img src="{$tpldir}images/go.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
                                {/if}
                        </td>
                </tr>
                {/foreach}

                <tr>
                        <td class="footer" colspan="8">
                                <div style="float:left;">
                                        {lng p="action"}: <select name="massAction" class="smallInput">
                                                <option value="-">------------</option>

                                                <optgroup label="{lng p="actions"}">
                                                        <option value="deaktivieren">deaktivieren</option>
                                                </optgroup>
                                        </select>&nbsp;
                                </div>
                                <div style="float:left;">
                                        <input type="submit" name="executeMassAction" value=" {lng p="execute"} " class="smallInput" />
                                </div>
                        </td>
                </tr>
        </table>
        </form>
</fieldset>

<fieldset>
        <legend>{lng p="follow_lang_user_groups"}</legend>

{if $groupSwitch}
<form name="f2" action="{$pageURL}&=set&sid={$sid}" onsubmit="spin(this)" method="post">
<input type="hidden" name="action" id="action" value="groups" />
<input type="hidden" name="do" id="action" value="groupSwitch" />


        <table class="list">
                <tr>
                        <th width="20">&nbsp;</th>
                        <th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f2,'groupSwitch_');"><img src="{$tpldir}images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
                        <th>{lng p="id"}</th>
                        <th>{lng p="email"}</th>
                        <th>{lng p="name"}</th>
                        <th>vorher</th>
                        <th>aktuell</th>
                </tr>

                {foreach from=$groupSwitch item=groupSwitch}
                {cycle name=class values="td1,td2" assign=class}
                <tr class="{$class}">
                        <td align="center"><img src="{$tpldir}images/user_{$groupSwitch.statusImg}.png" border="0" width="16" height="16" alt="" /></td>
                        <td align="center"><input type="checkbox" name="groupSwitch_{$groupSwitch.id}" /></td>
                        <td>{$groupSwitch.id}</td>
                        <td><a href="users.php?do=edit&id={$groupSwitch.id}&sid={$sid}">{$groupSwitch.email}</a><br /></td>
                        <td>{text value=$groupSwitch.nachname cut=20}, {text value=$groupSwitch.vorname cut=20}</td>
                        <td>{lng p="group"}: {text value=$groupSwitch.groupNameFollow cut=25}</td>
                        <td>{lng p="group"}: {text value=$groupSwitch.groupName cut=25}</td>
                </tr>
                {/foreach}

                <tr>
                        <td class="footer" colspan="7">
                                <div style="float:left;">
                                        {lng p="action"}: <select name="grpMassAction" class="smallInput">
                                                <option value="-">------------</option>

                                                <optgroup label="{lng p="actions"}">
                                                        <option value="beginn">{lng p="follow_lang_grpBeginn"}</option>
                                                        <option value="ende">{lng p="follow_lang_grpEnde"}</option>
                                                </optgroup>
                                        </select>&nbsp;
                                </div>
                                <div style="float:left;">
                                        <input type="submit" name="grpSubMassAction" value=" {lng p="execute"} " class="smallInput" />
                                </div>
                        </td>
                </tr>
        </table>
        </form>
{else}
<form name="f1" action="{$pageURL}&action=groups&groupCheck=set&sid={$sid}" method="post">
<div style="float:left;">{lng p="follow_lang_group_respon"}&nbsp;

         <input type="submit" name="groupCheck" value=" {lng p="execute"} " class="smallInput" />
</div>
</form>
{/if}
</fieldset>