<div class="innerWidget">
{* <table cellspacing="0" width="100%" style="table-layout:fixed;">
	{foreach from=$bmwidget_email_items item=folder key=folderID}
		<tr>
			<td width="20" align="center"><img width="16" height="16" src="{$tpldir}images/li/menu_ico_{$folder.icon}.png" border="0" alt="" align="absmiddle" /></td>
			<td style="text-overflow:ellipsis;overflow:hidden;"><a href="email.php?folder={$folderID}&sid={$sid}">{$folder.text}</a></td>
			<td align="left" width="50"><i class="fa fa-envelope-o"></i> {$folder.allMails}</td>
			<td align="left" width="45"><i class="fa fa-flag-o"></i> {if $folder.flaggedMails>0}<b>{$folder.flaggedMails}</b>{else}-{/if}</td>
			<td align="left" width="45"><i class="fa fa-envelope"></i> {if $folder.unreadMails>0}<b>{$folder.unreadMails}</b>{else}-{/if}</td>
		</tr>
	{/foreach}
</table> *}

	<div class="dragable-box content-box email-box">
		<div class="box-title title-inv">
			<a href="#">
				<span class="box-settings float-right">
					{* <img src="{$tpldir}frontend_assets/img/header-ic-settings-w.svg" class="img-fluid">  *}
					<img src="{$tpldir}frontend_assets/img/header-ic-settings-w.svg" class="img-fluid" data-toggle="modal" data-target="#myModal"> 
				</span>
			</a>
			<span class="tl-icon">
				<img src="{$tpldir}frontend_assets/img/menu-ic-email-w.svg" class="img-fluid" />
			</span>
			<h3 class="tl-title fw-med text-white">Email</h3>
		</div>
		<div class="email-widget pb-3">
			{foreach from=$bmwidget_email_items item=folder key=folderID}
				<div class="item d-flex align-items-center">
					<div class="item-label">
						<a href="email.php?folder={$folderID}&sid={$sid}">
							<span class="lbl-icon">
								<img src="{$tpldir}frontend_assets/img/menu_ico_{$folder.icon}.svg" class="img-fluid">
							</span>
							<span class="lbl-text fw-sbold text-white">{$folder.text}</span>
						</a>
					</div>
					<div class="bar-line"></div>
					<div class="info-icons">
						<div class="total-count d-inline-block">
							<span class="info-ic">
								<img src="{$tpldir}frontend_assets/img/menu-ic-email-w.svg" class="img-fluid" />
							</span>
							<span class="info-text text-white">{$folder.allMails}</span>
						</div>
						<div class="info-flag d-inline-block">
							<span class="ic-flag">
								<img src="{$tpldir}frontend_assets/img/ic-flag.svg" class="img-fluid" />
								{if $folder.flaggedMails>0}<b>{$folder.flaggedMails}</b>{else}-{/if}
							</span>
						</div>
						<div class="info-mailbox d-inline-block">
							<span class="ic-mailbox">
								<img src="{$tpldir}frontend_assets/img/ic-email-fill.svg" class="img-fluid" />
								{if $folder.unreadMails>0}<b>{$folder.unreadMails}</b>{else}-{/if}
							</span>
						</div>
					</div>
				</div>
			{/foreach}
		</div>
	</div>

	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<form action="start.php?action=storeEmailWidgetPrefs&name=BMPlugin_Widget_EMail&sid={$sid}" method="post">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Email: Preferences</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" name="save" value="true" />
						
						<fieldset>
							<legend>{lng p="prefs"}</legend>
							
							<input type="checkbox" name="hideSystemFolders" id="hideSystemFolders"{if $hideSystemFolders} checked="checked"{/if} />
							<label for="hideSystemFolders">{lng p="hidesystemfolders"}</label><br />
							
							<input type="checkbox" name="hideCustomFolders" id="hideCustomFolders"{if $hideCustomFolders} checked="checked"{/if} />
							<label for="hideCustomFolders">{lng p="hidecustomfolders"}</label><br />
							
							<input type="checkbox" name="hideIntelliFolders" id="hideIntelliFolders"{if $hideIntelliFolders} checked="checked"{/if} />
							<label for="hideIntelliFolders">{lng p="hideintellifolders"}</label>
						</fieldset>
				
						{* <p align="right">
							<input type="button" onclick="parent.hideOverlay()" value="{lng p="cancel"}" />
							<input type="submit" value="{lng p="ok"}" />
						</p> *}
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-secondary">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>