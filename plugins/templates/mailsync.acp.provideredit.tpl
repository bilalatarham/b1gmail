{$MSG}

<fieldset>
    <legend>{lng p="uiText"} {lng p="uiTextAdd"}</legend>

    <form action="{$pageURL}&do=provider&action=edit&id={$ID}&sid={$sid}" method="post" onsubmit="EBID('title').focus();if(EBID('title').value.length<2) return(false);editor.submit();spin(this)">
        <table width="100%">
            <tr>
                <td class="td1" width="180">{lng p="msActivate"}:</td>
                <td class="td2"><input id="activate" name="activate" type="checkbox" {if $Status == 'yes'}checked="checked"{/if} /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="msName"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="name" id="name" value="{$Name}" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="msServer"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="server" id="server" value="{$Server}" /></td>
            </tr>
            <tr>
                <td class="td1" width="180">{lng p="msPort"}:</td>
                <td class="td2"><input type="text" style="width:85%;" name="port" id="port" value="{$Port}" /></td>
            </tr>
            <tr>
                <td class="td1">{lng p="msProtokoll"}:</td>
                <td class="td2"><select name="type" class="form-control formred" style="width: 277px;">
                        <option value="imap" {if $Cert == ''}selected{/if}>IMAP ({lng p="msNotEncrypted"})</option>
                        <option value="imaptls" {if $Cert == 'tls'}selected{/if}>IMAP + STARTTLS ({lng p="msEncrypted"})</option>
                        <option value="imapssl" {if $Cert == 'ssl'}selected{/if}>IMAP + SSL ({lng p="msEncrypted"})</option>
                    </select></td>
            </tr>
        </table>

        <p align="right">
            <input class="button" type="submit" name="provider-edit" value=" {lng p="uiSave"} " />
        </p>
    </form>

</fieldset>