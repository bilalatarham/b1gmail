{* $Id: pacc.admin.payments.tpl,v 1.3 2008/07/04 10:56:14 patrick Exp $ *}
<form action="{$pageURL}&action=payments&filter=true&sid={$sid}" method="post" onsubmit="if(EBID('massAction').value!='download') spin(this)" name="f1">
<input type="hidden" name="page" id="page" value="{$pageNo}" />
<input type="hidden" name="sortBy" id="sortBy" value="{$sortBy}" />
<input type="hidden" name="sortOrder" id="sortOrder" value="{$sortOrder}" />
<input type="hidden" name="singleAction" id="singleAction" value="" />
<input type="hidden" name="singleID" id="singleID" value="" />

<fieldset>
	<legend>{lng p="pacc_payments"}</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'payment[');"><img src="{$tpldir}images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><a href="javascript:updateSort('benutzer');">{lng p="user"}
				{if $sortBy=='benutzer'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th><a href="javascript:updateSort('paket');">{lng p="pacc_package"}
				{if $sortBy=='paket'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th width="135"><a href="javascript:updateSort('betrag');">{lng p="pacc_amount"}
				{if $sortBy=='betrag'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th width="145"><a href="javascript:updateSort('datum');">{lng p="date"}
				{if $sortBy=='datum'}<img src="{$tpldir}images/sort_{$sortOrder}.png" border="0" alt="" width="7" height="6" align="absmiddle" />{/if}</a></th>
			<th width="65">&nbsp;</th>
		</tr>
		
		{foreach from=$payments item=payment}
		{cycle name=class values="td1,td2" assign=class}
		<tr class="{$class}">
			<td align="center"><img src="../plugins/templates/images/pacc_{if $payment.fertig}yes{else}no{/if}.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><input type="checkbox" name="payment[{$payment.id}]" /></td>
			<td><a href="users.php?do=edit&id={$payment.user.id}&sid={$sid}">{text value=$payment.user.email cut=25}</a><br />
				<small>{text value=$payment.user.nachname cut=20}, {text value=$payment.user.vorname cut=20}</small></td>
			<td><a href="{$pageURL}&action=packages&do=edit&id={$payment.package.id}&sid={$sid}">{if $payment.package.deleted}<font color="#666666">{/if}{text value=$payment.package.title cut=20}{if $payment.package.deleted}</font>{/if}</a><br />
				<small>{$payment.interval}</small></td>
			<td>{$payment.amount}<br /><small>{$payment.method}</small></td>
			<td>{date timestamp=$payment.datum nice=true}</td>
			<td>
				{if $payment.rechnung}<a href="javascript:void(0);" onclick="openWindow('{$pageURL}&action=payments&do=showInvoice&id={$payment.id}&sid={$sid}','invoice_{$payment.id}',640,480);" title="{lng p="pacc_showinvoice"}"><img src="{$tpldir}images/file.png" border="0" alt="{lng p="pacc_showinvoice"}" width="16" height="16" /></a>{/if}
				{if !$payment.fertig}<a href="javascript:singleAction('activate', '{$payment.id}');" title="{lng p="pacc_activatepayment"}"><img src="{$tpldir}images/unlock.png" border="0" alt="{lng p="pacc_activatepayment"}" width="16" height="16" /></a>{/if}
				<a href="javascript:singleAction('delete', '{$payment.id}');" onclick="return confirm('{lng p="realdel"}');" title="{lng p="delete"}"><img src="{$tpldir}images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a>
			</td>
		</tr>
		{/foreach}
		
		<tr>
			<td class="footer" colspan="7">
				<div style="float:left;">
					{lng p="action"}: <select name="massAction" id="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="{lng p="actions"}">
							<option value="download">{lng p="pacc_downloadinvoices"}</option>
							<option value="activate">{lng p="pacc_activatepayment"}</option>
							<option value="delete">{lng p="delete"}</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" {lng p="execute"} " class="smallInput" />
				</div>
				<div style="float:right;padding-top:3px;">
					{lng p="pages"}: {pageNav page=$pageNo pages=$pageCount on=" <span class=\"pageNav\"><b>[.t]</b></span> " off=" <span class=\"pageNav\"><a href=\"javascript:updatePage(.s);\">.t</a></span> "}&nbsp;
				</div>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend>{lng p="filter"}</legend>
	
	<table width="100%">
		<tr>
			<td width="40" valign="top" rowspan="2"><img src="{$tpldir}images/filter.png" border="0" alt="" width="32" height="32" /></td>
			<td class="td1" width="80">{lng p="pacc_packages"}:</td>
			<td class="td2">
				{foreach from=$packages item=package key=packageID}
					<input type="checkbox" name="packages[{$packageID}]" id="package_{$packageID}"{if $package.checked} checked="checked"{/if} />
						<label for="package_{$packageID}"><b>{if $package.deleted}<font color="#666666">{/if}{text value=$package.title}{if $package.deleted}</font>{/if}</b></label><br />
				{/foreach}
			</td>
			<td class="td1" width="80">{lng p="pacc_paymentmethods"}:</td>
			<td class="td2">
				{foreach from=$paymentMethods item=paymentmethod key=paymentmethodID}
					<input type="checkbox" name="paymentMethods[{$paymentmethodID}]" id="paymentmethod_{$paymentmethodID}"{if $paymentmethod.checked} checked="checked"{/if} />
						<label for="paymentmethod_{$paymentmethodID}"><b>{lng p="pacc_$paymentmethodID"}</b></label><br />
				{/foreach}
			</td>
		</tr>
	</table>
	
	<p align="right">
		{lng p="perpage"}: 
		<input type="text" name="perPage" value="{$perPage}" size="5" />
		<input type="submit" value=" {lng p="apply"} " />
	</p>
</fieldset>

</form>
