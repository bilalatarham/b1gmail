{if $error}
	<fieldset>
		<legend>Fehler</legend>
		
		<table width="100%" id="noticeTable">
			<tbody>
				<tr>
					<td width="20" valign="top">
						<img border="0" align="absmiddle" src="templates/images/error.png">
					</td>
					<td valign="top">{$error}</td>
					<td valign="top" align="right"></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
{/if}
<fieldset>
	<legend>Hinzufügen</legend>	
	<form action="{$pageURL}&sid={$sid}" method="post" onsubmit="spin(this);">
		<table width="100%">
			<tr>
				<td width="250"><label for="whitelist_add">Benutzer hinzufügen (ID oder E-Mail)</label>:</td>
				<td><input id="whitelist_add" name="whitelist_add" type="text" value="{$value}" /></td>
			</tr>
		</table>
		
		<input class="button" type="submit" value="{lng p='save'}" />
	</form>
</fieldset>