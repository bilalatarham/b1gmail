<fieldset>
	<legend>Free-SMS - {lng p="prefs"}</legend>
	<form action="{$pageURL}&action=freeSMSgateways&do=save&sid={$sid}" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="9"><img src="../plugins/templates/images/modlargesms32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150">{lng p="freeSMSanzeigen"}</td>
				<td class="td2"><input type="checkbox" size="10" id="status" name="status" {if $status}checked="checked"{/if} /></td>
			</tr>
			<tr>
				<td class="td1" width="150">{lng p="freeSMSgw"}:</td>
				<td class="td2"><textarea style="width:85%;" id="gw" name="gw" rows="2">{text value=$gateway allowEmpty=true}</textarea></td>
			</tr>
			<tr>
				<td class="td1">{lng p="largesmsgwvar"}:</td>
				<td class="td2">{lng p="largesmsgwuser"}: %%user%% | {lng p="largesmsgwpw"}: %%pass%% | {lng p="largesmsgwto"}: %%to%% | {lng p="largesmsgwmsg"}: %%msg%%</td>
			</tr>
			<tr>
				<td class="td1">{lng p="largesmsgwoptvar"}:</td>
				<td class="td2">{lng p="largesmsgwemail"}: %%useremail%% | {lng p="largesmsgwuserid"}: %%userid%% </td>
			</tr>
			<tr>
				<td class="td1">{lng p="returnvalue"}:</td>
				<td class="td2"><input type="text" size="10" id="rueckgabewert" name="rueckgabewert" value="{text value=$rueckgabewert allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="user"}:</td>
				<td class="td2"><input type="text" size="36" id="user" name="user" value="{text value=$user allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="password"}:</td>
				<td class="td2"><input type="password" autocomplete="off" size="36" id="pass" name="pass" value="{text value=$pass allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="smssig"}:</td>
				<td class="td2"><input type="text" size="36" id="signatur" name="signatur" value="{text value=$signatur allowEmpty=true}" /></td>
			</tr>
			<tr>
				<td class="td1">{lng p="smsreloadsperre"}:</td>
				<td class="td2"><input type="text" size="10" id="reloadsperre" name="reloadsperre" value="{text value=$reloadsperre allowEmpty=true}" />&nbsp;{lng p="smsreloadsperreh"}</td>
			</tr>
		</table>
		<p align="right">
			<input type="submit" value=" {lng p="save"} " />
		</p>
	</form>
</fieldset>
