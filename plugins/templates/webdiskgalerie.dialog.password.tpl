<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Password</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset={$charset}" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="{$tpldir}style/dialog.css" rel="stylesheet" type="text/css" />
</head>

<body onload="document.getElementById('pw').focus()">
	<center><br />
	<div style="background-color:#dedede; padding:5px 7px 7px 7px; width:450px; -moz-border-radius:10px;">
		<table width="450" cellspacing="0">
			<tr>
				<td colspan="3">
					<div style="line-height:1.4em; text-align:justify;">{lng p="protected_desc"}</div>
				</td>
			</tr>
			<tr>
				<td colspan="3" style="height:8px;"></td>
			</tr>
			<tr>
				<td width="20%" align="left">
					<img src="plugins/templates/images/gal_crypt.png">&nbsp;{lng p="password"}:
				</td>
				<form action="index.php?action={if $galerie}galerie{else}slideshow{/if}&gal={$folder}" method="post">
				<td width="60%" align="left">
						<input type="password" name="pw" id="pw" size="30" />
				</td>
				<td width="20%" align="right">
						<input type="submit" value="&nbsp;{lng p="ok"}&nbsp;" />&nbsp;
				</td>
				</form>
			</tr>
		</table>
	</div>
	</center>
</body>

</html>
