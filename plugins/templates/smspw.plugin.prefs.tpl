<fieldset>
	<legend>SMS-PW - {lng p="prefs"}</legend>
	<form action="{$pageURL}&sid={$sid}&do=save" method="post" onsubmit="spin(this)">
	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/modSMSPW32.png" border="0" alt="" width="32" height="32" /></td>
			<td>
				<blockquote>
					<div style="width:500px;">
						<div class="td1" style="width:165px; text-align:right; float:left; padding:4px 0px 5px 0px;">&nbsp;{lng p="smspw"}:&nbsp;</div>
						<div style="text-align:left; padding:2px 0px 5px 0px;">&nbsp;&nbsp;&nbsp;<input type="checkbox" name="smspw" value="smspw" {if $smspw}checked="checked"{/if}></div>
						<div class="td1" style="width:165px; text-align:right; float:left; padding:4px 0px 5px 0px;">&nbsp;{lng p="smspwuserkosten"}:&nbsp;</div>
						<div style="text-align:right; padding:2px 228px 5px 0px;">
							<select name="smspwuserkosten">
								<option value="1"{if $smspwuserkosten==1} selected="selected"{/if}>{lng p="smspwuser"}</option>
								<option value="0"{if $smspwuserkosten==0} selected="selected"{/if}>{lng p="smspwprovider"}</option>
							</select>
						</div>
					</div>
				</blockquote>
			</td>
		</tr>
	</table>
	<p>
		<div style="float:right;">
			<input type="submit" value=" {lng p="save"} " />
		</div>
	</p>
	</form>
</fieldset>