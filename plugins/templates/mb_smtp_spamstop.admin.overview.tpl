<fieldset>
	<legend>Whitelist</legend>	
	{if !$whitelistedUsers}
		Die Whitelist ist leer!
	{else}
		<table class="list">
			<tbody>
				<tr>
					<th>Nutzer-ID</th>
					<th>Haupt-E-Mail</th>
					<th>Aus Whitelist entfernen</th>
				</tr>
				{foreach from=$whitelistedUsers key=id item=value}
				{cycle name=class values="td1,td2" assign=class}
					<tr class="{$class}">
						<td>{$id}</td>
						<td>{$value}</td>
						<td><a href="{$pageURL}&sid={$sid}&delete={$id}"><img src="templates/images/delete.png" border="0" alt="{lng p="delete"}" width="16" height="16" /></a></td>
					</tr>
				{/foreach}		
			</tbody>
		</table>
	{/if}
</fieldset>