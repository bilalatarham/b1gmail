<?php
/*
 * Plugin gatewayswitch
 */
define('PLUGIN_GATEWAYSWITCH_TOKEN',		'');
class gatewayswitch extends BMPlugin 
{
	/*
	* Eigenschaften des Plugins
	*/
	function gatewayswitch()
	{
		$this->name					= 'POP3-Gateway Switch';
		$this->version				= '1.6.0';
		$this->designedfor			= '7.3.0';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'dotaachen';
		$this->mail					= 'b1g@dotaachen.net';
		$this->web 					= 'http://b1g.dotaachen.net';	

		$this->update_url			= 'http://my.b1gmail.com/update_service/';
		$this->website				= 'http://my.b1gmail.com/details/28/';

		$this->admin_pages			= true;
		$this->admin_page_title		= 'POP3-Gateway Switch';
		$this->admin_page_icon		= "gatewayswitch_icon.png";
	}

	/*
	*  Link  und Tabs im Adminbereich 
	*/
	function AdminHandler()
	{
		global $tpl, $lang_admin;

		// Plugin aufruf ohne Action
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'page1';

		// Tabs im Adminbereich
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['gateways'],
				'link'		=> $this->_adminLink() . '&action=page1&',
				'active'	=> $_REQUEST['action'] == 'page1',
				'icon'		=> '../plugins/templates/images/gatewayswitch_logo.png'
			),
			1 => array(
				'title'		=> $lang_admin['create'],
				'link'		=> $this->_adminLink() . '&action=page2&',
				'active'	=> $_REQUEST['action'] == 'page2',
				'icon'		=> './templates/images/extension_add.png'
			),
			2 => array(
				'title'		=> $lang_admin['prefs'],
				'link'		=> $this->_adminLink() . '&action=page3&',
				'active'	=> $_REQUEST['action'] == 'page3',
				'icon'		=> './templates/images/ico_prefs_common.png'
			),
			3 => array(
				'title'		=> $lang_admin['faq'],
				'link'		=> $this->_adminLink() . '&action=page4&',
				'active'	=> $_REQUEST['action'] == 'page4',
				'icon'		=> './templates/images/faq32.png'
			)
		);
		$tpl->assign('tabs', $tabs);

		// Plugin aufruf mit Action 
		if($_REQUEST['action'] == 'page1') {
			$tpl->assign('page', $this->_templatePath('gatewayswitch1.pref.tpl'));
			$this->_Page1();
		} else if($_REQUEST['action'] == 'page2') {
			$tpl->assign('page', $this->_templatePath('gatewayswitch2.pref.tpl'));
			$this->_Page2();
		} else if($_REQUEST['action'] == 'page3') {
			$tpl->assign('page', $this->_templatePath('gatewayswitch3.pref.tpl'));
			$this->_Page3();
		} else if($_REQUEST['action'] == 'page4') {
			$tpl->assign('page', $this->_templatePath('gatewayswitch4.pref.tpl'));
		}
	}

	/*
	*  Sprach variablen
	*/
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		$lang_admin['gatewayswitch_name']					= "POP3-Gateway Switch";
		$lang_admin['gatewayswitch_text']					= "Sie wollen mehr als ein POP3 Gateway abrufen? POP3 Gateway Switch macht dies automatisch f&uuml;r Sie.";

		if (strpos($lang, 'deutsch') !== false) {
			$lang_admin['gatewayswitch_priority_change']	= 'Tragen Sie die den Wert der Priorit&auml;t des neuen Gateways ein.';
			$lang_admin['gatewayswitch_change']				= 'wechseln';
			$lang_admin['gatewayswitch_fetch'] 				= 'Nach dem Ausf&uuml;hren landen Sie auf der Seite "Einstellungen &raquo; Wartung &raquo; POP3-Gateway", dies hei&szlig;t nicht, dass der Prozess abgebrochen wurde!</b>';
			$lang_admin['gatewayswitch_oncron']				= 'Wechseln beim Cronjob zulassen?';
			$lang_admin['gatewayswitch_onfilehandler']		= 'Wechseln auf der Seite zulassen?';
		} else {
			$lang_admin['gatewayswitch_priority_change']	= 'Priority of the new Gateway.';
			$lang_admin['gatewayswitch_change']				= 'change';
			$lang_admin['gatewayswitch_fetch'] 				= '';
			$lang_admin['gatewayswitch_oncron']				= 'Allow changing on the Cronjob?';
			$lang_admin['gatewayswitch_onfilehandler']		= 'Allow changing on the Site?';
		}
	}

	/*
	 * installation routine
	 */	
	function Install()
	{
		global $db;

		// adds gateway  and gateway_count to prefs table
		$db->Query('ALTER TABLE `{pre}prefs` ADD `gateway` INT( 11 ) NOT NULL DEFAULT "1"');
		$db->Query('ALTER TABLE `{pre}prefs` ADD `gateway_count` INT( 11 ) NOT NULL DEFAULT "1"');
		// adds gateway_filehandler and gateway_oncron
		$db->Query('ALTER TABLE `{pre}prefs` ADD `gateway_filehandler` INT( 1 ) NOT NULL DEFAULT "1"');
		$db->Query('ALTER TABLE `{pre}prefs` ADD `gateway_oncron` INT( 1 ) NOT NULL DEFAULT "1"');

		// create gateway table 
		$db->Query('CREATE TABLE IF NOT EXISTS `bm60_plugin_gatewayswitch` (
			`id` int(11) NOT NULL auto_increment,
			`sort` int(11) NOT NULL,
			`receive_method` enum(\'pop3\',\'pipe\') NOT NULL default \'pop3\',
			`pop3_host` varchar(255) NOT NULL,
			`pop3_port` int(6) NOT NULL,
			`pop3_user` varchar(255) NOT NULL,
			`pop3_pass` varchar(255) NOT NULL,
			`fetchcount` int(11) NOT NULL,
			PRIMARY KEY  (`id`)) ENGINE=MyISAM AUTO_INCREMENT=1 ;');
   
		// Select von prefs  fuer den ersten  gateway
		$res = $db->Query('SELECT * FROM {pre}prefs LIMIT 1');
		$db_prefs = $res->FetchArray();
		$res->Free();

		// Insert in gateway von ersten gateway
		$db->Query('INSERT INTO {pre}plugin_gatewayswitch(sort, receive_method, pop3_host, pop3_port, pop3_user, pop3_pass, fetchcount) VALUES(?,?,?,?,?,?,?)', 
			(int) 1,
			$db_prefs['receive_method'],
			$db_prefs['pop3_host'],
			(int) $db_prefs['pop3_port'],
			$db_prefs['pop3_user'],
			$db_prefs['pop3_pass'],
			(int) $db_prefs['fetchcount']);

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		global $db;

		$db->Query('ALTER TABLE `{pre}prefs`
			DROP `gateway`,
			DROP `gateway_count`,
			DROP `gateway_filehandler`,
			DROP `gateway_oncron`');

		$db->Query('DROP TABLE {pre}plugin_gatewayswitch');

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	*  Abfragen aller Gateways , loeschen einzelener und switchen
	*/
	function _Page1()
	{
		global $tpl, $db;

		// delete 
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'delete')
		{
			//abfrage von gateway_count
			$res = $db->Query('SELECT gateway_count FROM {pre}prefs LIMIT 1');
			$prefs = $res->FetchArray();
			$res->Free();

			//abfrage von sort des zu loeschenden gateways
			$res = $db->Query('SELECT sort FROM {pre}plugin_gatewayswitch WHERE id=? LIMIT 1', 
				(int) $_REQUEST['id']);
			$gateway = $res->FetchArray();
			$res->Free();

			//loeschen des gateways
			$db->Query('Delete FROM {pre}plugin_gatewayswitch Where id=?',
				(int) $_REQUEST['id']);

			//sort nr neu einstellen
			$alt_sort = $gateway[sort]+1;
			$neu_sort = $gateway[sort];

			while($prefs[gateway_count]  >= $alt_sort) {
				$db->Query('UPDATE {pre}plugin_gatewayswitch SET sort=? WHERE sort=?',
					(int) $neu_sort,
					(int) $alt_sort);

				$alt_sort = $alt_sort+1;
				$neu_sort = $neu_sort+1;
			}

			// update des gateway_count  wertes
			$db->Query('UPDATE {pre}prefs SET gateway_count=? WHERE id=?',
				(int) $prefs[gateway_count]-1,
				(int) 1);	
		}

		//switch im adminbereich
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'switch')
		{
			if($_REQUEST['gatewaysortnum'] != "") {
				$this->_SwitchParam($_REQUEST['gatewaysortnum']);
			} else {
				PutLog("POP3-Gateway Switch: Reihenfolge nicht gültig. Bitte geben sie einen passenden Wert ein.", PRIO_ERROR, "gatewayswitch.plugin.php", 192);
			}
		}

		// gateway von prefs abfragen und auf Page1 ausgeben		
		$res2 = $db->Query('SELECT gateway, pop3_host, receive_method FROM {pre}prefs LIMIT 1');
		$switch = $res2->FetchArray();
		$res2->Free();

		$sortBy = isset($_REQUEST['sortBy']) ? $_REQUEST['sortBy'] : 'sort';
		$sortOrder = isset($_REQUEST['sortOrder']) ? strtolower($_REQUEST['sortOrder']) : 'asc';

		// gateways abfragen und auf Page1 ausgeben
		$gateways = array();
		$res = $db->Query('SELECT * FROM {pre}plugin_gatewayswitch ORDER by ' . $sortBy . ' ' . $sortOrder);
		while($row = $res->FetchArray())
		{
			$gateways[$row['id']] = array(
				'id'				=> $row['id'],
				'sort'				=> $row['sort'],
				'receive_method'	=> $row['receive_method'],
				'pop3_host'			=> $row['pop3_host'],
				'pop3_port'			=> $row['pop3_port'],
				'pop3_user'			=> $row['pop3_user']
			);
		}
		$res->Free();

		$tpl->assign('sortBy', $sortBy);
		$tpl->assign('sortOrder', $sortOrder);

		$tpl->assign('switch', $switch);
		$tpl->assign('gateways', $gateways);
	}

	/*
	*  Eintragen neuer oder bearbeiten altern Gateways
	*/	
	function _Page2()
	{
		global $tpl, $db;

		// neuen gateway hinzufuegen
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$res = $db->Query('INSERT INTO {pre}plugin_gatewayswitch(sort, receive_method, pop3_host, pop3_port, pop3_user, pop3_pass, fetchcount) VALUES(?,?,?,?,?,?,?)', 
				(int) $_REQUEST['sort'],
				$_REQUEST['receive_method'],
				$_REQUEST['pop3_host'],
				(int) $_REQUEST['pop3_port'],
				$_REQUEST['pop3_user'],
				$_REQUEST['pop3_pass'],
				(int) $_REQUEST['fetchcount']);
			$res->Free();

			$res = $db->Query('SELECT gateway_count FROM {pre}prefs LIMIT 1');
			$prefs = $res->FetchArray();

			$db->Query('UPDATE {pre}prefs SET gateway_count=? WHERE id=?',
			(int) $prefs[gateway_count]+1,
			(int) 1);	
			$res->Free();

			header('Location: plugin.page.php?plugin=gatewayswitch&action=page1&sid=' . session_id());
		}

		// gateway updaten
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'update')
		{
			$db->Query('UPDATE {pre}plugin_gatewayswitch SET sort=?,receive_method=?,pop3_host=?,pop3_port=?,pop3_user=?,pop3_pass=?,fetchcount=? WHERE id=?',
				(int) $_REQUEST['sort'],
				$_REQUEST['receive_method'],
				$_REQUEST['pop3_host'],
				(int) $_REQUEST['pop3_port'],
				$_REQUEST['pop3_user'],
				$_REQUEST['pop3_pass'],
				(int) $_REQUEST['fetchcount'],
				(int) $_REQUEST['id']);

			header('Location: plugin.page.php?plugin=gatewayswitch&action=page1&sid=' . session_id());
		}

		//gateway daten fuer update abrufen
		if(isset($_REQUEST['id']))
		{
			// get config
			$res = $db->Query('SELECT * FROM {pre}plugin_gatewayswitch WHERE id=?', 
				(int) $_REQUEST['id']);
			$gateways = $res->FetchArray();
			$res->Free();

			$tpl->assign('gateway', $gateways);
			$tpl->assign('id', true);
		} else {
			$tpl->assign('id', false);
		}
	}

	/*
	*  Einstellungen speichern
	*/
	function _Page3()
	{
		global $tpl, $db;

		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}prefs SET gateway_oncron=?,gateway_filehandler=? WHERE id=?',
				(int) isset($_REQUEST['change_oncron']) ? 1 : 0,
				(int) isset($_REQUEST['change_filehandler']) ? 1 : 0,
				(int) 1);	
		}

		$res = $db->Query('SELECT gateway_oncron, gateway_filehandler FROM {pre}prefs LIMIT 1');
		$gateways = $res->FetchArray();
		$res->Free();

		$tpl->assign('gateway', $gateways);
	}

	// Funktion Switch
	function _Switch()
	{
		global $tpl, $db, $bm_prefs;

		//gateway und gateway_count abfragen
		$res = $db->Query('SELECT gateway, gateway_count FROM {pre}prefs WHERE id=1');
		$db_prefs = $res->FetchArray();

		// gateway eins hochzaehlen oder auf 1 setzen
		if($db_prefs[gateway_count] != $db_prefs[gateway]) {
			$gateway = $db_prefs[gateway]+1;
		} else {
			$gateway = 1;
		}

		// gateway aus plugin_gatewayswitch abfragen
		$res = $db->Query('SELECT * FROM {pre}plugin_gatewayswitch WHERE sort=? Limit 1', 
			(int) $gateway);
		$db_gateway = $res->FetchArray();
		$res->Free();

		// bei falscher sort, wird einfach wieder auf 1 gesetzt und alles faengt wieder von vorne an
		if($db_gateway[sort] != $gateway) {

			//b1gMail-Logs: falscher sort
			PutLog("POP3-Gateway Switch: Reihenfolge nicht gültig. Reihenfolge wurde auf 1 gesetzt.", PRIO_ERROR, __FILE__, __LINE__);

			$gateway = 1;
			$res = $db->Query('SELECT * FROM {pre}plugin_gatewayswitch WHERE sort=? Limit 1', 
				(int) $gateway);
			$db_gateway = $res->FetchArray();
			$res->Free();
		}
	
		// receive_method herausfinden
		if($db_gateway['receive_method'] != "pop3") {
			$receive_method = 2;
		} else {
			$receive_method = 1;
		}
	
		// Prefs fuer cron.php aendern
		$db->Query('UPDATE {pre}prefs SET receive_method=?,pop3_host=?,pop3_port=?,pop3_user=?,pop3_pass=?,fetchcount=?,gateway=? WHERE id=?',
			$receive_method,
			$db_gateway['pop3_host'],
			(int) $db_gateway['pop3_port'],
			$db_gateway['pop3_user'],
			$db_gateway['pop3_pass'],
			(int) $db_gateway['fetchcount'],
			(int) $gateway,
			(int) 1);
	}	

	// Funktion Switch mit Paramtern
	function _SwitchParam($sortnum)
	{
		global $db;

		//gateway_count abfragen
		$res = $db->Query('SELECT gateway_count FROM {pre}prefs WHERE id=?',
			(int) 1);
		$db_prefs = $res->FetchArray();
		
		if($sortnum <= $db_prefs[gateway_count]) {
		
			// gateway aus plugin_gatewayswitch abfragen
			$res = $db->Query('SELECT * FROM {pre}plugin_gatewayswitch WHERE sort=? Limit 1', 
				(int) $sortnum);
			$db_gateway = $res->FetchArray();
			$res->Free();

			// bei falscher sort, wird einfach wieder auf 1 gesetzt und alles faengt wieder von vorne an
			if($db_gateway[sort] != $sortnum) {
				PutLog("POP3-Gateway Switch: Reihenfolge nicht gültig. Bitte geben sie einen passenden Wert ein.", PRIO_ERROR, __FILE__, __LINE__);
				$switch = false;
			} else {
			
				// receive_method herausfinden
				if($db_gateway['receive_method'] != "pop3") {
					$receive_method = 2;
				} else {
					$receive_method = 1;
				}
			
				// Prefs fuer cron.php aendern
				$db->Query('UPDATE {pre}prefs SET receive_method=?,pop3_host=?,pop3_port=?,pop3_user=?,pop3_pass=?,fetchcount=?,gateway=? WHERE id=?',
					$receive_method,
					$db_gateway['pop3_host'],
					(int) $db_gateway['pop3_port'],
					$db_gateway['pop3_user'],
					$db_gateway['pop3_pass'],
					(int) $db_gateway['fetchcount'],
					(int) $db_gateway['sort'],
					(int) 1);
			}
		} else {
			PutLog("POP3-Gateway Switch: Reihenfolge nicht gültig. Bitte geben sie einen passenden Wert ein.", PRIO_ERROR, __FILE__, __LINE__);
		}
	}

	/*
	*  Gateway Switch
	*/
	function OnCron()
	{
		global $db;

		$res = $db->Query('SELECT gateway_oncron FROM {pre}prefs LIMIT 1');
		$db_prefs = $res->FetchArray();
		$res->Free();

		if($db_prefs['gateway_oncron'] == 1)
		{
			$this->_Switch();
		}
	}

	function FileHandler($file, $action)
	{
		global $db;

		if ($file == "index.php" AND $action == "gatewayswitch")
		{
			if((isset($_REQUEST['token']) AND $_REQUEST['token'] == PLUGIN_GATEWAYSWITCH_TOKEN) OR (PLUGIN_GATEWAYSWITCH_TOKEN == ""))
			{
				$res = $db->Query('SELECT gateway_filehandler FROM {pre}prefs LIMIT 1');
				$db_prefs = $res->FetchArray();
				$res->Free();
	
				if($db_prefs['gateway_filehandler'] == 1)
				{
					$this->_Switch();
					echo "OK - ";
					echo time();
					exit;
				} else {
					DisplayError(__LINE__, "Deaktiviert.", "Diese Funktion wurden vom Administrator deaktiviert.", "Einstellbar im Adminbereich unter \"Plugin -> POP3-Gateway Switch -> Einstellungen\".", __FILE__, __LINE__);
					exit;
				}
			}
		}
	}
}
/*
 * register plugin
 */
$plugins->registerPlugin('gatewayswitch');
?>