<?php
/**
 * Google Two Factor Authentication Plugin for b1gMail
 *
 * @author Nicolas Pecher
 * @copyright 2018 Nicolas Pecher
 */
class G2FA extends BMPlugin
{
    private $googleAuthenticator;

    public function __construct()
    {
        // meta data
		$this->name	       = 'Google Two Factor Authentication';
		$this->author      = 'Nicolas Pecher';
		$this->version     = '1.0.0';
		$this->website     = 'https://www.webrex.at';
        $this->update_url  = 'http://service.b1gmail.com/plugin_updates/';
        $this->mail        = 'office@webrex.at';
        $this->designedfor = '7.4.0';
        $this->type	       = BMPLUGIN_DEFAULT;

        // admin settings
        $this->admin_pages      = true;
        $this->admin_page_title = 'Google 2FA';
        $this->admin_page_icon  = 'g2fa_ico.png';

        $this->googleAuthenticator = new PHPGangsta_GoogleAuthenticator;
    }

    public function Install(): bool
    {
        global $db, $bm_prefs;

        $g2faDatabaseStructure = [
            'g2fa_secrets' => [
                'fields' => [
                    ['user_id', 'int(11)', 'NO'],
                    ['secret', 'varchar(255)', 'NO'],
                ],
                'indexes' => [
                    'user_id' => ['user_id'],
                ],
            ],
            'g2fa_prefs' => [
                'fields' => [
                    ['ga_app_title', 'varchar(255)', 'NO'],
                    ['qrcode_width', 'int(11)', 'NO'],
                    ['qrcode_height', 'int(11)', 'NO'],
                ],
                'indexes' => [],
            ],
        ];
        SyncDBStruct($g2faDatabaseStructure);
        $db->Query(
            'INSERT INTO g2fa_prefs(ga_app_title, qrcode_width, qrcode_height) VALUES(?, ?, ?)',
            $bm_prefs['titel'],
            200,
            200
        );

        return true;
    }

    public function Uninstall(): bool
    {
        global $db;

        $db->Query("DROP TABLE IF EXISTS g2fa_secrets");
        $db->Query("DROP TABLE IF EXISTS g2fa_prefs");

        return true;
    }

    public function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
        $lang_user['g2fa'] = 'Google 2FA';
        $lang_user['g2fa_code'] = 'Google Authenticator Code';
        $lang_admin['g2fa_qrcode'] = 'QR Code';
        $lang_admin['g2fa_ga_app_title'] = 'Google Authenticator App Title';
		if ($lang == 'deutsch') {
            $lang_user['prefs_d_g2fa'] = 'Sichern Sie Ihren Account mit einer Zwei-Faktor-Authentisierung ab.';
            $lang_user['g2fa_install_app'] = 'App installieren';
            $lang_user['g2fa_install_app_description'] = 'Zur Verwendung der Zwei-Faktor-Authentisierung müssen Sie zunächst die '
                                                       . '<b><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">'
                                                       . 'Google Authenticator</a></b> App auf Ihrem Smartphone installieren.';
            $lang_user['g2fa_configure_app'] = 'App konfigurieren';
            $lang_user['g2fa_configure_app_description'] = 'Sie dürfen diese Seite nicht verlassen, solange Sie nicht die Google Authenticator App '
                                                         . 'konfiguriert haben. Scannen Sie dazu den folgenden QR-Code oder geben Sie den geheimen '
                                                         . 'Schlüssel manuell in Ihre App ein.';
            $lang_user['g2fa_settings'] = 'Einstellungen';
            $lang_user['g2fa_state'] = 'Status';
            $lang_user['g2fa_state_enabled'] = 'Zwei-Faktor-Authentisierung ist für Ihren Account aktiviert.';
            $lang_user['g2fa_state_disabled'] = 'Zwei-Faktor-Authentisierung ist für Ihren Account deaktiviert.';
            $lang_user['g2fa_new_secret_description'] = 'Um einen neuen geheimen Schlüssel für Ihren Account zu erstellen, klicken Sie auf den folgenden Button.';
            $lang_user['g2fa_enable'] = 'Google 2FA aktivieren';
            $lang_user['g2fa_disable'] = 'Google 2FA deaktivieren';
            $lang_user['g2fa_new_secret'] = 'Neuen Schlüssel generieren';
            $lang_user['g2fa_setup_description'] = 'Scannen Sie den folgenden QR-Code mit der Google Authenticator App.';
            $lang_user['g2fa_invalid_code'] = 'Der angegebene Google Authenticator Code ist ungültig.';
            $lang_admin['g2fa_secret_key'] = $lang_user['g2fa_secret_key'] = 'Geheimer Schlüssel';
            $lang_admin['g2fa_secret_keys'] = 'Geheime Schlüssel';
            $lang_admin['g2fa_qrcode_width'] = 'Breite des QR-Codes';
            $lang_admin['g2fa_qrcode_height'] = 'Höhe des QR-Codes';
		} else {
            $lang_user['prefs_d_g2fa'] = 'Secure your account with two-factor authentication.';
            $lang_user['g2fa_install_app'] = 'Install app';
            $lang_user['g2fa_install_app_description'] = 'To use two-step authentication, you must first install the '
                                                       . '<b><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank">'
                                                       . 'Google Authenticator</a></b> App on your smartphone';
            $lang_user['g2fa_configure_app'] = 'Configure app';
            $lang_user['g2fa_configure_app_description'] = 'You may not leave this page unless you have configured the Google Authenticator App. '
                                                         . 'To do so scan the following QR code or manually enter the secret code in your App.';
            $lang_user['g2fa_settings'] = 'Settings';
            $lang_user['g2fa_state'] = 'State';
            $lang_user['g2fa_state_enabled'] = 'Two-level authentication is enabled for your account.';
            $lang_user['g2fa_state_disabled'] = 'Two-level authentication is disabled for your account.';
            $lang_user['g2fa_new_secret_description'] = 'To create a new secret key for your account, click on the following button.';
            $lang_user['g2fa_enable'] = 'Enable Google 2FA';
            $lang_user['g2fa_disable'] = 'Disable Google 2FA';
            $lang_user['g2fa_new_secret'] = 'Generate new key';
            $lang_user['g2fa_invalid_code'] = 'The specified Google Authenticator code is invalid.';
            $lang_admin['g2fa_secret_key'] = $lang_user['g2fa_secret_key'] = 'Secret key';
            $lang_admin['g2fa_secret_keys'] = 'Secret keys';
            $lang_admin['g2fa_qrcode_width'] = 'Width of the QR code';
            $lang_admin['g2fa_qrcode_height'] = 'Height of the QR code';
        }
	}

    public function FileHandler($file, $action)
    {
        global $db, $tpl, $lang_user;

        if (!RequestPrivileges(PRIVILEGES_USER, true)) {
            return;
        }

        $secret = $this->getUserSecret();
        if (isset($secret) && !$this->isG2faPassed()) {
            if ($file !== 'index.php') {
                $this->redirect('index.php');
            }
            if ($code = ($_REQUEST['g2fa_code'] ?? '')) {
                if ($this->googleAuthenticator->verifyCode($secret, $code)) {
                    $this->passG2fa()->redirect('start.php');
                } else {
                    $tpl->assign('g2fa_error', $lang_user['g2fa_invalid_code']);
                }
            }
            if (empty($action)) {
                $_REQUEST['action'] = 'login';
            }
            if ($_REQUEST['action'] === 'login') {
                $tpl->assign('page', $this->_templatePath('g2fa.login.tpl'));
                $tpl->display('nli/index.tpl');
                exit;
            }
        }

        if ($file === 'prefs.php') {
            $GLOBALS['prefsItems']['g2fa'] = true;
            $GLOBALS['prefsIcons']['g2fa'] = 'plugins/templates/images/g2fa_ico.png';
            $GLOBALS['prefsImages']['g2fa'] = 'plugins/templates/images/g2fa_prefs.png';
        }
    }

    public function UserPrefsPageHandler($action): bool
    {
        global $db, $tpl, $userRow;

        if ($action !== 'g2fa') {
            return false;
        }

        $userSecret = $this->getUserSecret();
        $command = $_REQUEST['do'] ?? '';
        if ($command === 'create_secret') {
            $secret = $this->googleAuthenticator->createSecret();
            if ($userSecret) {
                $db->Query(
                    'UPDATE g2fa_secrets SET secret = ? WHERE user_id = ?',
                    $secret,
                    $userRow['id']
                );
            } else {
                $db->Query(
                    'INSERT INTO g2fa_secrets(user_id, secret) VALUES(?, ?)',
                    $userRow['id'],
                    $secret
                );
            }
            $_SESSION['g2fa_new_secret'] = true;
            $this->passG2fa()->redirect('prefs.php', ['action' => 'g2fa']);
        } elseif ($command === 'disable') {
            $db->Query('DELETE FROM g2fa_secrets WHERE user_id = ?', $userRow['id']);
            $this->redirect('prefs.php', ['action' => 'g2fa']);
        }

        if (isset($_SESSION['g2fa_new_secret'])) {
            unset($_SESSION['g2fa_new_secret']);
            $tpl->assign('g2fa_secret', $userSecret);
            $prefs = $this->getPrefs();
            $tpl->assign('g2fa_qrcode_url', $this->googleAuthenticator->getQRCodeGoogleUrl(
                $userRow['email'],
                $userSecret,
                $prefs['ga_app_title'],
                [
                    'width' => $prefs['qrcode_width'],
                    'height' => $prefs['qrcode_height']
                ]
            ));
        }
        $tpl->assign('g2fa_enabled', isset($userSecret));
        $tpl->assign('pageContent', $this->_templatePath('g2fa.user.prefs.tpl'));
        $tpl->display('li/index.tpl');

        return true;
    }

    public function AdminHandler()
    {
        global $db, $tpl, $lang_admin, $bm_prefs;

        $action = $_REQUEST['action'] ?? 'prefs';
        $command = $_REQUEST['do'] ?? '';

        $tpl->assign('tabs', [
            [
                'title'  => $lang_admin['prefs'],
                'link'   => $this->_adminLink() . '&',
                'icon'   => 'templates/images/ico_prefs_misc.png',
                'active' => ($action === 'prefs'),
            ],
            [
                'title'  => $lang_admin['g2fa_secret_keys'],
                'link'   => $this->_adminLink() . '&action=secret_keys&',
                'icon'   => 'templates/images/ico_prefs_login.png',
                'active' => ($action === 'secret_keys'),
            ],
        ]);

        $tpl->assign('page_url', $this->_adminLink());
        if ($action === 'prefs') {
            if ($command === 'save') {
                $db->Query(
                    'UPDATE g2fa_prefs SET ga_app_title = ?, qrcode_width = ?, qrcode_height = ?',
                    ($_REQUEST['ga_app_title'] ?? $bm_prefs['titel']),
                    ($_REQUEST['qrcode_width'] ?? 200),
                    ($_REQUEST['qrcode_height'] ?? 200)
                );
            }
            $tpl->assign('g2fa_prefs', $this->getPrefs());
            $tpl->assign('page', $this->_templatePath('g2fa.admin.prefs.tpl'));
        } elseif ($action === 'secret_keys') {
            if ($command === 'delete') {
                if ($userId = ($_REQUEST['user_id'] ?? null)) {
                    $db->Query('DELETE FROM g2fa_secrets WHERE user_id = ?', $userId);
                    $this->redirect('plugin.page.php', ['plugin' => 'G2FA', 'action' => 'secret_keys']);
                }
            }
            $sql  = "SELECT u.email, u.vorname, u.nachname, g.user_id, g.secret ";
            $sql .= "FROM bm60_users u ";
            $sql .= "LEFT JOIN g2fa_secrets g ON g.user_id = u.id WHERE g.secret IS NOT NULL";
            if ($query = $_REQUEST['query'] ?? null) {
                $sql .= " AND (u.email LIKE '%".$db->Escape($query)."%' ";
                $sql .= "OR u.vorname LIKE '%".$db->Escape($query)."%' ";
                $sql .= "OR u.nachname LIKE '%".$db->Escape($query)."%')";
            }
            $secretKeys = [];
            $res = $db->Query($sql);
            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                $secretKeys[] = $row;
            }
            $res->Free();
            $tpl->assign('secret_keys', $secretKeys);
            $tpl->assign('page', $this->_templatePath('g2fa.admin.secret_keys.tpl'));
        }
    }

    private function getPrefs(): array
    {
        global $db;

        return $db->Query('SELECT * FROM g2fa_prefs LIMIT 1')->FetchArray();
    }

    private function getUserSecret()
    {
        global $db, $userRow;

        $result = $db->Query(
            'SELECT secret FROM g2fa_secrets WHERE user_id = ?',
            $userRow['id']
        );

        return $result->RowCount() === 1 ? $result->FetchArray()['secret'] : null;
    }

    private function isG2faPassed(): bool
    {
        return isset($_SESSION['g2fa_passed']);
    }

    private function passG2fa()
    {
        $_SESSION['g2fa_passed'] = true;

        return $this;
    }

    private function redirect(string $file, array $params = [], bool $withSID = true)
    {
        if ($withSID) {
            $params['sid'] = session_id();
        }
        if (!empty($params)) {
            $file .= '?' . http_build_query($params);
        }
        header('Location: ' . $file);
        exit;
    }
}

/**
 * PHP Class for handling Google Authenticator 2-factor authentication.
 *
 * @author Michael Kliewe
 * @copyright 2012 Michael Kliewe
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 *
 * @link http://www.phpgangsta.de/
 */
class PHPGangsta_GoogleAuthenticator
{
    protected $_codeLength = 6;

    /**
     * Create new secret.
     * 16 characters, randomly chosen from the allowed base32 characters.
     *
     * @param int $secretLength
     *
     * @return string
     */
    public function createSecret($secretLength = 16)
    {
        $validChars = $this->_getBase32LookupTable();

        // Valid secret lengths are 80 to 640 bits
        if ($secretLength < 16 || $secretLength > 128) {
            throw new Exception('Bad secret length');
        }
        $secret = '';
        $rnd = false;
        if (function_exists('random_bytes')) {
            $rnd = random_bytes($secretLength);
        } elseif (function_exists('mcrypt_create_iv')) {
            $rnd = mcrypt_create_iv($secretLength, MCRYPT_DEV_URANDOM);
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $rnd = openssl_random_pseudo_bytes($secretLength, $cryptoStrong);
            if (!$cryptoStrong) {
                $rnd = false;
            }
        }
        if ($rnd !== false) {
            for ($i = 0; $i < $secretLength; ++$i) {
                $secret .= $validChars[ord($rnd[$i]) & 31];
            }
        } else {
            throw new Exception('No source of secure random');
        }

        return $secret;
    }

    /**
     * Calculate the code, with given secret and point in time.
     *
     * @param string   $secret
     * @param int|null $timeSlice
     *
     * @return string
     */
    public function getCode($secret, $timeSlice = null)
    {
        if ($timeSlice === null) {
            $timeSlice = floor(time() / 30);
        }

        $secretkey = $this->_base32Decode($secret);

        // Pack time into binary string
        $time = chr(0).chr(0).chr(0).chr(0).pack('N*', $timeSlice);
        // Hash it with users secret key
        $hm = hash_hmac('SHA1', $time, $secretkey, true);
        // Use last nipple of result as index/offset
        $offset = ord(substr($hm, -1)) & 0x0F;
        // grab 4 bytes of the result
        $hashpart = substr($hm, $offset, 4);

        // Unpak binary value
        $value = unpack('N', $hashpart);
        $value = $value[1];
        // Only 32 bits
        $value = $value & 0x7FFFFFFF;

        $modulo = pow(10, $this->_codeLength);

        return str_pad($value % $modulo, $this->_codeLength, '0', STR_PAD_LEFT);
    }

    /**
     * Get QR-Code URL for image, from google charts.
     *
     * @param string $name
     * @param string $secret
     * @param string $title
     * @param array  $params
     *
     * @return string
     */
    public function getQRCodeGoogleUrl($name, $secret, $title = null, $params = array())
    {
        $width = !empty($params['width']) && (int) $params['width'] > 0 ? (int) $params['width'] : 200;
        $height = !empty($params['height']) && (int) $params['height'] > 0 ? (int) $params['height'] : 200;
        $level = !empty($params['level']) && array_search($params['level'], array('L', 'M', 'Q', 'H')) !== false ? $params['level'] : 'M';

        $urlencoded = urlencode('otpauth://totp/'.$name.'?secret='.$secret.'');
        if (isset($title)) {
            $urlencoded .= urlencode('&issuer='.urlencode($title));
        }

        return 'https://chart.googleapis.com/chart?chs='.$width.'x'.$height.'&chld='.$level.'|0&cht=qr&chl='.$urlencoded.'';
    }

    /**
     * Check if the code is correct. This will accept codes starting from $discrepancy*30sec ago to $discrepancy*30sec from now.
     *
     * @param string   $secret
     * @param string   $code
     * @param int      $discrepancy      This is the allowed time drift in 30 second units (8 means 4 minutes before or after)
     * @param int|null $currentTimeSlice time slice if we want use other that time()
     *
     * @return bool
     */
    public function verifyCode($secret, $code, $discrepancy = 1, $currentTimeSlice = null)
    {
        if ($currentTimeSlice === null) {
            $currentTimeSlice = floor(time() / 30);
        }

        if (strlen($code) != 6) {
            return false;
        }

        for ($i = -$discrepancy; $i <= $discrepancy; ++$i) {
            $calculatedCode = $this->getCode($secret, $currentTimeSlice + $i);
            if ($this->timingSafeEquals($calculatedCode, $code)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set the code length, should be >=6.
     *
     * @param int $length
     *
     * @return PHPGangsta_GoogleAuthenticator
     */
    public function setCodeLength($length)
    {
        $this->_codeLength = $length;

        return $this;
    }

    /**
     * Helper class to decode base32.
     *
     * @param $secret
     *
     * @return bool|string
     */
    protected function _base32Decode($secret)
    {
        if (empty($secret)) {
            return '';
        }

        $base32chars = $this->_getBase32LookupTable();
        $base32charsFlipped = array_flip($base32chars);

        $paddingCharCount = substr_count($secret, $base32chars[32]);
        $allowedValues = array(6, 4, 3, 1, 0);
        if (!in_array($paddingCharCount, $allowedValues)) {
            return false;
        }
        for ($i = 0; $i < 4; ++$i) {
            if ($paddingCharCount == $allowedValues[$i] &&
                substr($secret, -($allowedValues[$i])) != str_repeat($base32chars[32], $allowedValues[$i])) {
                return false;
            }
        }
        $secret = str_replace('=', '', $secret);
        $secret = str_split($secret);
        $binaryString = '';
        for ($i = 0; $i < count($secret); $i = $i + 8) {
            $x = '';
            if (!in_array($secret[$i], $base32chars)) {
                return false;
            }
            for ($j = 0; $j < 8; ++$j) {
                $x .= str_pad(base_convert(@$base32charsFlipped[@$secret[$i + $j]], 10, 2), 5, '0', STR_PAD_LEFT);
            }
            $eightBits = str_split($x, 8);
            for ($z = 0; $z < count($eightBits); ++$z) {
                $binaryString .= (($y = chr(base_convert($eightBits[$z], 2, 10))) || ord($y) == 48) ? $y : '';
            }
        }

        return $binaryString;
    }

    /**
     * Get array with all 32 characters for decoding from/encoding to base32.
     *
     * @return array
     */
    protected function _getBase32LookupTable()
    {
        return array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', //  7
            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 15
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 23
            'Y', 'Z', '2', '3', '4', '5', '6', '7', // 31
            '=',  // padding char
        );
    }

    /**
     * A timing safe equals comparison
     * more info here: http://blog.ircmaxell.com/2014/11/its-all-about-time.html.
     *
     * @param string $safeString The internal (safe) value to be checked
     * @param string $userString The user submitted (unsafe) value
     *
     * @return bool True if the two strings are identical
     */
    private function timingSafeEquals($safeString, $userString)
    {
        if (function_exists('hash_equals')) {
            return hash_equals($safeString, $userString);
        }
        $safeLen = strlen($safeString);
        $userLen = strlen($userString);

        if ($userLen != $safeLen) {
            return false;
        }

        $result = 0;

        for ($i = 0; $i < $userLen; ++$i) {
            $result |= (ord($safeString[$i]) ^ ord($userString[$i]));
        }

        // They are only identical strings if $result is exactly 0...
        return $result === 0;
    }
}

$plugins->registerPlugin('G2FA');
