<?php 
/*
 * 
 * (c) Informant => http://www.speedloc.de
 * 
 * Redistribution of this code without explicit permission is forbidden!
 *
 *
 */

class logoutbutton extends BMPlugin 
{	
	function logoutbutton()
	{	
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Logoutbutton';
		$this->author			= 'Informant';
		$this->version			= '1.0';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
	}
	
    function getUserPages($loggedin)
	{
		global $lang_user;
        
        if(!$loggedin)
			return(array());

		return(array(
			'microblogging' => array(
					'icon'		=> 'ico_logout',
					'link'		=> 'start.php?action=logout&sid=',
					'text'		=> $lang_user['logout'],
					'iconDir'	=> './templates/aikq_v7.3/images/li/',
					'order'		=> 999
				)));
	}
    
}

$plugins->registerPlugin('logoutbutton');

?>