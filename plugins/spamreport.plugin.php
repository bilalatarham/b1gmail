<?php 
/*
 * 
 * (c) IT- und Wbl�sungen Kretschmar for B1Gmail v7.2 and higher
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: spamreport.plugin.php,v 1.2.0 2010/01/22 20:30:00 Informant $
 *
 */

class SPAMREPORT extends BMPlugin 
{
	function SPAMREPORT()
	{		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Spam-Report';
		$this->author			= 'Informant';
		$this->version			= '1.2.0';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
		
		// admin pages
		$this->admin_pages		= true;
		$this->admin_page_title	= 'Spam-Report';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		global $currentCharset;
        $_lang_custom = $_lang_user = $_lang_client = $_lang_admin = array();

		if($lang == 'deutsch')
		{
			// user
			$_lang_user['sBericht']				= 'Spam-Bericht:';
			$_lang_user['sDeakt']				= 'deaktiviert';
			$_lang_user['sAktT']				= 't&auml;glich';
			$_lang_user['sAktW']				= 'w&ouml;chentlich';
			// admin
			$_lang_admin['SRueb']				= 'Hier k&ouml;nnen Sie die Option "SPAM-Bericht" f&uuml;r die User aktivieren/deaktivieren, sowie einstellen, wann der w&ouml;chentliche Bericht gesendet wird!';
			$_lang_admin['SRakt']				= 'SPAM-Bericht aktivieren?';
			$_lang_admin['SBfre']				= 'Freitag';
			$_lang_admin['SBson']				= 'Sonntag';
			$_lang_admin['SBw']					= 'W&ouml;chentlicher Bericht?';
			$_lang_admin['uSta']				= 'User-Konfiguration';
			$_lang_admin['uDeakt']				= 'deaktiviert';
			$_lang_admin['uAktT']				= 't&auml;glich';
			$_lang_admin['uAktW']				= 'w&ouml;chentlich';
			$_lang_admin['uOpt']				= 'Spam-Bericht:';
			$_lang_admin['uOptU']				= 'User-Anzahl:';
			// system
			$_lang_custom['sr_tBetreff']		= 'Ihr t�glicher SPAM-Bericht!';
			$_lang_custom['sr_wBetreff']		= 'Ihr w�chentlicher SPAM-Bericht!';
			$_lang_custom['sr_spamreportTXT']	= 'Sehr geehrte Damen und Herren,' . "\n\n"
												. 'Sie haben %%anzahl%% ungelesene E-Mail(s) in Ihrem SPAM-Ordner. Ihren SPAM-Ordner k�nnen Sie unter http://www.%%domain%% nach dem Login mit Ihren Benutzerdaten einsehen!' . "\n\n\n"
												. 'Auflistung der sich im SPAM-Ordner befindlichen E-Mail(s) mit Absender und Betreff:'
												. '%%spaminhalt%%' . "\n\n"
												. '(Diese E-Mail wurde automatisch erstellt!)';
		}
		else
		{
			$_lang_user['sBericht']				= 'spam-report:';
			$_lang_user['sDeakt']				= 'disabled';
			$_lang_user['sAktT']				= 'daily';
			$_lang_user['sAktW']				= 'weekly';
			$_lang_admin['SRueb']				= 'Here you can select "spam-report" for the user to enable/disable, when the weekly report is sent!';
			$_lang_admin['SRakt']				= 'SPAM-report activate?';
			$_lang_admin['SBfre']				= 'friday';
			$_lang_admin['SBson']				= 'sunday';
			$_lang_admin['SBw']					= 'weekly report?';
			$_lang_admin['uSta']				= 'user-config';
			$_lang_admin['uDeakt']				= 'disabled';
			$_lang_admin['uAktT']				= 'daily';
			$_lang_admin['uAktW']				= 'weekly';
			$_lang_admin['uOpt']				= 'spam-report:';
			$_lang_admin['uOptU']				= 'user-number:';
			$_lang_custom['sr_tBetreff']		= 'Your daily SPAM-Report!';
			$_lang_custom['sr_wBetreff']		= 'Your weekly SPAM-Report!';
			$_lang_custom['sr_spamreportTXT']	= 'Ladies and Gentlemen,' . "\n\n"
												. 'you have %%anzahl%% of unread e-mail(s) in your SPAM-folder. Your SPAM-folder, you can view here http://www.%%domain%% after login with your User!' . "\n\n\n"
												. 'Listing of the SPAM-folder e-mail(s) with sender and subject:'
												. '%%spaminhalt%%' . "\n\n"
												. '(This e-mail was automatically created!)';
		}
        
        // convert charset
		$arrays = array('admin', 'client', 'user', 'custom');
		foreach($arrays as $array)
		{
            $destArray = sprintf('lang_%s', $array);
            $srcArray  = '_' . $destArray;

            if(!isset($$srcArray))
                continue;

            foreach($$srcArray as $key=>$val)
            {
                if(function_exists('CharsetDecode') && !in_array(strtolower($currentCharset), array('iso-8859-1', 'iso-8859-15')))
                    $val = CharsetDecode($val, 'iso-8859-15');
                ${$destArray}[$key] = $val;
            }
		}
	}

	function Install()
	{
		global $db, $bm_prefs;

		// user spam-report auf aktiviert w�chentlich (standard) setzen
		$db->Query('ALTER TABLE {pre}users ADD USP tinyint(4) NOT NULL DEFAULT 2');
		// spam-report gesendet auf nein setzen
		$db->Query('ALTER TABLE {pre}users ADD SSP tinyint(4) NOT NULL DEFAULT 0');
		// wann der w�chentliche bericht gesendet wird (0 = sonntag, 5 = freitag)
		$db->Query('ALTER TABLE {pre}prefs ADD WSP tinyint(4) NOT NULL DEFAULT 0');
		// spam-report aktiviert (standard) setzen
		$db->Query('ALTER TABLE {pre}prefs ADD ASP tinyint(4) NOT NULL DEFAULT 1');

		PutLog('Spam-Report Plugin was successfull installed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		global $db;
		
		// eintr�ge l�schen
		$db->Query('ALTER TABLE {pre}users DROP USP');
		$db->Query('ALTER TABLE {pre}users DROP SSP');
		$db->Query('ALTER TABLE {pre}prefs DROP WSP');
		$db->Query('ALTER TABLE {pre}prefs DROP ASP');
		
		PutLog('Spam-Report Plugin was successfull removed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function AdminHandler()
	{
		global $tpl, $plugins, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'prefs';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['prefs'],
				'link'      => $this->_adminLink() . '&action=prefs&',
				'active'	=> $_REQUEST['action'] == 'prefs')
			);

		$tpl->assign('tabs', $tabs);
		
		if($_REQUEST['action'] == 'prefs')
			$this->_prefsPage();
	}
	
	function _prefsPage()
	{
		global $tpl, $db, $bm_prefs;

		$Rasp = $bm_prefs['ASP'];
		$wsp = $bm_prefs['WSP'];
		$zae = 0;
		$zae2 = 0;
		$zae3 = 0;
		
		// anzahl t�gliche-berichte ermitteln
		$sql2 = $db->Query('SELECT USP FROM {pre}users WHERE USP = 1');
		while($row = $sql2->FetchArray(MYSQLI_ASSOC))
		{
			$zae = $zae + 1;
		}
		$sql2->Free();
		
		// anzahl w�chentliche-berichte ermitteln
		$sql3 = $db->Query('SELECT USP FROM {pre}users WHERE USP = 2');
		while($row = $sql3->FetchArray(MYSQLI_ASSOC))
		{
			$zae2 = $zae2 + 1;
		}
		$sql3->Free();
		
		// anzahl deaktivierte-berichte ermitteln
		$sql4 = $db->Query('SELECT USP FROM {pre}users WHERE USP = 0');
		while($row = $sql4->FetchArray(MYSQLI_ASSOC))
		{
			$zae3 = $zae3 + 1;
		}
		$sql4->Free();
		
		// speichern
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}prefs SET ASP=?',
				isset($_REQUEST['SRasp']) ? 1 : 0
				);
				
			$Rasp = isset($_REQUEST['SRasp']) ? 1 : 0;
			
			$db->Query('UPDATE {pre}prefs SET WSP=?',
				$_REQUEST['sBericht']
				);
				
			$wsp = $_REQUEST['sBericht'];
		}
		
		// an template �bergeben
		$tpl->assign('zaehlerT', $zae);
		$tpl->assign('zaehlerW', $zae2);
		$tpl->assign('zaehlerD', $zae3);
		$tpl->assign('Rasp', $Rasp);
		$tpl->assign('Wsp', $wsp);
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('spamreport.plugin.prefs.tpl'));
		return(true);
	}
		
	function OnCron()
	{
		global $bm_prefs, $db, $lang_custom, $lang_admin;
		
		// ASP = admin spam report aktivier ja/nein
		// USP = user spam report t�glich = 1, w�chentlich = 2 und deaktiviert = 0
		// SSP = spam report heute bereits gesendet ja/nein
		// WSP = w�chentlicher bericht (0 = sonntag, 5 = freitag)
		

		$asp = $bm_prefs['ASP'];
		$wsp = $bm_prefs['WSP'];
		
		$stunde = date("H");
		$minute = date("i");
		// wochentag ermitteln (sonntag (0), montag (1), dienstag (2), mittwoch (3), donnerstag (4), freitag (5), samstag (6))
		$we = date('w'); 

		if($asp == 1)
		{
			if($stunde == 12 || $stunde == 13 || $stunde == 23)
			{
				$sql = $db->Query("SELECT * FROM {pre}users WHERE USP = 1 OR USP = 2");
				// system-mail-absender
				$mailabs = $bm_prefs['passmail_abs'];
				// erste domain aus der datenbank der variable zuweisen
				//list($domain) = explode(':', $bm_prefs['domains']);
				list($domain) = array_values($bm_prefs['domains'])[0];
				$debug_blabla = var_export($bm_prefs['domains'], true);
				if(file_exists("blabla.txt")) {
				    $dateizumschreiben=fopen("blabla.txt", "w");
				} else {
				    $dateizumschreiben=fopen("blabla.txt", "a");
				}
				fwrite($dateizumschreiben, $debug_blabla);
				fclose($dateizumschreiben);
				if($stunde == 23 && $minute >= 30)
				{	
					while($row = $sql->FetchArray(MYSQLI_ASSOC))
					{
						// user spam-report
						$usp = $row['USP'];
						// spam-report gesendet?
						$ssp = $row['SSP'];
						// user-spamfilter an/aus (yes/no)
						$uSpam = $row['spamfilter'];
						// user-id
						$userID = $row['id'];
						// user-status -> locked = nicht aktiviert, yes = gesperrt und no = freigeschaltet/aktiviert
						$uStatus = $row['gesperrt'];
						// user-mail-addy
						$userMail = $row['email'];
						$userObject = _new('BMUser', array($userID));
						
						if ($usp == 1)
						{
							$betreff = $lang_custom['sr_tBetreff'];
						}
						elseif($usp == 2)
						{
							$betreff = $lang_custom['sr_wBetreff'];
						}

						// spam-report t�glich aktiviert aber noch nicht gesendet, user muss aktiviert sein und spamfilter muss beim user aktiviert sein
						if($usp == 1 && $ssp == 0 && $uStatus == "no" && $uSpam == "yes")
						{
							if(!class_exists('BMMailbox'))
    								include(B1GMAIL_DIR . 'serverlib/mailbox.class.php'); 
							$instanz = _new('BMMailbox', array($userID, $userMail, $userObject));
							// anzahl der mails im order spam (false), bei true nur ausgabe der ungelesenen spam-mails
							$anzahl = $instanz->GetMailCount(FOLDER_SPAM, true);

							// holt die inhalte der spam-emails from, to, subject etc. in das array $mails
							$mails = $instanz->GetMailList(FOLDER_SPAM);

							// nur senden, wenn spamanzahl gr��er 0 ist		  
							if($anzahl > 0)
							{
								// absender und betreff der mail(s) auslesen, welche im spam-ordner liegen
								$zae = 1;
								$spaminhalt = "\n\n";
								foreach($mails as $mail_from)
								{
									$spaminhalt .= $zae . ".) " .$mail_from['from'] . " - " . $mail_from['subject'] . "\n";
									$zae = $zae+1;
								}
								$spaminhalt .= "\n";
			 
								// ersetzt die variable %%anzahl%%, %%domain%% und %%spaminhalt%% im spamreportTXT
								$vars = array('anzahl' => $anzahl,
										  	  'domain' => $domain,
											'spaminhalt' => $spaminhalt); 
								// spam-report senden mit systemmail-einstellungen
								SystemMail($mailabs, $userMail, $betreff, 'sr_spamreportTXT', $vars, $userID);
								
								// spam-report auf gesendet setzen
								$db->Query("UPDATE {pre}users SET SSP = 1 WHERE id = ?", $userID);
							}
						}
						// spam-report w�chentlich aktiviert aber noch nicht gesendet, user muss aktiviert sein, spamfilter muss beim user aktiviert sein und wochentag der aus der db ist
						if($usp == 2 && $ssp == 0 && $uStatus == "no" && $uSpam == "yes" && $we == $wsp)
						{
							if(!class_exists('BMMailbox'))
    								include(B1GMAIL_DIR . 'serverlib/mailbox.class.php'); 
							$instanz = _new('BMMailbox', array($userID, $userMail, $userObject));
							// anzahl der mails im order spam (false), bei true nur ausgabe der ungelesenen spam-mails
							$anzahl = $instanz->GetMailCount(FOLDER_SPAM, true);
							
							// holt die inhalte der spam-emails from, to, subject etc. in das array $mails
							$mails = $instanz->GetMailList(FOLDER_SPAM);

							// nur senden, wenn spamanzahl gr��er 0 ist			  
							if($anzahl > 0)
							{
								// absender und betreff der mail(s) auslesen, welche im spam-ordner liegen
								$zae = 1;
								$spaminhalt = "\n\n";
								foreach($mails as $mail_from)
								{
									$spaminhalt .= $zae . ".) " .$mail_from['from'] . " - " . $mail_from['subject'] . "\n";
									$zae = $zae+1;
								}
								$spaminhalt .= "\n";
			 
								// ersetzt die variable %%anzahl%%, %%domain%% und %%spaminhalt%% im spamreportTXT
								$vars = array('anzahl' => $anzahl,
										 	  'domain' => $domain,
											'spaminhalt' => $spaminhalt); 
								// spam-report senden mit systemmail-einstellungen
								SystemMail($mailabs, $userMail, $betreff, 'sr_spamreportTXT', $vars, $userID);
								
								// spam-report auf gesendet setzen
								$db->Query("UPDATE {pre}users SET SSP = 1 WHERE id = ?", $userID);
							}
						}
					}
					// PutLog("SPAM-Report(s) were sent!", PRIO_PLUGIN, __FILE__, __LINE__);
				}
				elseif($stunde >= 12 && $stunde <= 13)
				{
					while($row = $sql->FetchArray(MYSQLI_ASSOC))
					{
						// user spam-report
						$usp = $row['USP'];
						// spam-report gesendet?
						$ssp = $row['SSP'];
						// user-spamfilter an/aus (yes/no)
						$uSpam = $row['spamfilter'];
						// user-id
						$userID = $row['id'];
						// user-status -> locked = nicht aktiviert, yes = gesperrt und no = freigeschaltet/aktiviert
						$uStatus = $row['gesperrt'];
						// user-mail-addy
						$userMail = $row['email'];
						$userObject = _new('BMUser', array($userID));

						// spam-report aktiviert und bereits gesendet
						if($usp != 0 && $ssp == 1 && $uStatus == "no" && $uSpam == "yes")
						{						
							// spam-reports auf 0 setzen, damit diese (am neuen tag/in der neuen woche) wieder gesendet werden k�nnen
							$db->Query("UPDATE {pre}users SET SSP = 0 WHERE id = ?", $userID);
						}
						// wenn user aktiviert ist und spamfilter beim user deaktiviert ist, spambericht ebenfalls auf deaktiviert setzen, aber nur wenn der spambericht nicht schon deaktiviert ist
						elseif($uStatus == "no" && $uSpam == "no" && $usp != 0)
						{
							$db->Query("UPDATE {pre}users SET USP = 0 WHERE id = ?", $userID);
						}
					}
				}
				$sql->Free();
			}
		}
	}

	function BeforeDisplayTemplate($resourceName, &$tpl)
	{
		global $db, $tpl, $groupRow, $userRow, $bm_prefs;

		if($_REQUEST['action'] == 'antispam' && $bm_prefs['ASP'] && $resourceName == 'li/index.tpl' && preg_match('/prefs.php/', $_SERVER['REQUEST_URI']))
		{
			// �bergabe spam-einstellung vom user aus db
			$tpl->assign('sBericht', $userRow['USP']);
			// spambericht on/off �bergeben
			$tpl->assign('asp', $bm_prefs['ASP']);
			$tpl->assign('pageContent', $this->_templatePath('spamreport.userprefs.antispam.tpl'));
		}
	}
	
	// prefs.php config
	function FileHandler($file, $action) 
	{
		global $tpl, $db, $userRow, $bm_prefs, $thisUser;

		// update der user-einstellung
		if($file == 'prefs.php' && $action == 'antispam' && isset($_REQUEST['do']) && $_REQUEST['do'] == 'save' && $bm_prefs['ASP'])
		{
			// update db vom user
			$db->Query('UPDATE {pre}users SET USP = ? WHERE id = ?', $_REQUEST['wBericht'], $thisUser->_id);
		}
	}
}

$plugins->registerPlugin('SPAMREPORT');

?>