<?php
/* standardmäßig 1800 Sekunden bis zum Logout */
/* WICHTIG: jQuery einbinden
* im li/index.tpl: {if $security_autologout}var security_autologout = {$security_autologout};{/if}*/
define("SECURITY_PLUGIN_INTERVAL", 1800);

class security extends BMPlugin
{
	function security()
	{

		$this->name        = 'Sicherheits-Plugin';
		$this->author      = 'Martin Buchalik, Angepasst von Sebijk';
		$this->web         = 'http://martin-buchalik.de';
		$this->mail        = 'support@martin-buchalik.de';
		$this->version     = '1.0.4';
		$this->designedfor = '7.4.0';
		$this->type        = BMPLUGIN_DEFAULT;

		$this->admin_pages = false;
	}



	function Install()
	{
		global $db;
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}security_plugin (
							`usersession` VARCHAR(128) NOT NULL,
							`lastaction` INT(10) NOT NULL,
							PRIMARY KEY (`usersession`))');
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}


	function Uninstall()
	{
		global $db;

		$db->Query('DELETE FROM {pre}userprefs WHERE `key`=?', 'security_plugin_interval');
		$db->Query('DROP TABLE {pre}security_plugin');
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}


	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if ($lang == 'deutsch') {
			$lang_user['userprefs_security']          = 'Sicherheit';
			$lang_user['prefs_d_userprefs_security']  = 'Sicherheits-Einstellungen für Ihr Benutzer-Konto';
			$lang_user['userprefs_security_headline'] = 'Sicherheits-Einstellungen';

			$lang_user['userprefs_security_exp1'] = 'Hier können Sie die Logoutzeit einstellen.';
			$lang_user['userprefs_security_exp2'] = 'Der erste Regler ist für das Infomationsfeld was Ihnen angzeigt wann Sie automatisch abgemeldet werden.';
			$lang_user['userprefs_security_exp3'] = 'Sie können die Session in diesen Feld jederzeit verlängern. Der zweite Regler ist für die Logoutzeit.';

			$lang_user['security_autologout']    = 'Automatischer Logout';
			$lang_user['security_desc']          = 'Klicken Sie auf "Sitzung verlängern", um nicht in wenigen Minuten aus Sicherheitsgründen ausgeloggt zu werden.';
			$lang_user['security_extendsession'] = 'Sitzung verlängern';
		} else {
			$lang_user['userprefs_security']          = 'Security';
			$lang_user['prefs_d_userprefs_security']  = 'Security settings for your account';
			$lang_user['userprefs_security_headline'] = 'Security settings';

			$lang_user['userprefs_security_exp1'] = 'This is the first line';
			$lang_user['userprefs_security_exp1'] = 'This is the second line';
			$lang_user['userprefs_security_exp1'] = 'This is the third line';

			$lang_user['security_autologout']    = 'Automatic logout';
			$lang_user['security_desc']          = 'Please click on "Extend session" in order not to get logged out in a few minutes for security reasons.';
			$lang_user['security_extendsession'] = 'Extend session';
		}

	}
	function OnLogin($userID, $interface = 'web')
	{
		global $db;
		$security_session = session_id() . $userID;
		$security_session = sha1($security_session);

		$time = time();
		$res  = $db->Query('SELECT `lastaction` FROM {pre}security_plugin WHERE `usersession`=?', $security_session);

		if ($res->RowCount() == 0) {
			$db->Query('INSERT INTO {pre}security_plugin(`usersession`, `lastaction`) VALUES(?,?)', $security_session, $time);
		}

	}

	function OnLogout($userID)
	{
		global $db;
		$security_session = session_id() . $userID;
		$security_session = sha1($security_session);

		$res = $db->Query('SELECT `lastaction` FROM {pre}security_plugin WHERE `usersession`=?', $security_session);

		if ($res->RowCount() != 0) {
			$db->Query('DELETE FROM {pre}security_plugin WHERE `usersession`=? ', $security_session);
		}

	}

	function OnCron()
	{
		global $db;

		$deletetime = time() - TIME_ONE_DAY;
		$db->Query('DELETE FROM {pre}security_plugin WHERE `lastaction`<? ', $deletetime);
	}

	function FileHandler($file, $action)
	{
		if ($file == 'prefs.php') {
			$GLOBALS['prefsItems']['userprefs_security']  = true;
			$GLOBALS['prefsImages']['userprefs_security'] = 'plugins/templates/images/userprefs_security_ico48.png';
			$GLOBALS['prefsIcons']['userprefs_security']  = 'plugins/templates/images/userprefs_security_ico16.png';
			if ($action == 'userprefs_security')
				$this->SecurityHandler();
		}
	}

	function UserPrefsPageHandler($action)
	{
		global $tpl;

		if ($action != 'userprefs_security')
			return (false);

		$tpl->assign('pageContent', $this->_templatePath('userprefs.security.tpl'));
		$tpl->display('li/index.tpl');

		return (true);
	}

	function SecurityHandler()
	{
		global $db, $thisUser, $thisUser, $userRow, $tpl, $bm_prefs;
		if (isset($_REQUEST['do']) && $_REQUEST['do'] == 'save' && isset($_POST['startdisplay']) && isset($_POST['autologout'])) {
			$startdisplay = $_POST['startdisplay'];
			$autologout   = $_POST['autologout'];
			if (ctype_digit($startdisplay) && ctype_digit($autologout)) {
				if ($startdisplay < 20 || $startdisplay > 590 || $autologout < 30 || $autologout > 600 || $startdisplay > $autologout) {
					$startdisplay = 20;
					$autologout = 30;
				}
				$interval = $startdisplay . "," . $autologout;
				$thisUser->setPref('security_plugin_interval', $interval);
			}
		}
		$tpl->assign('anrede', $userRow['anrede']);
		$tpl->assign('vorname', $userRow['vorname']);
		$tpl->assign('nachname', $userRow['nachname']);
		$tpl->assign('strasse', $userRow['strasse']);
		$tpl->assign('hnr', $userRow['hnr']);
		$tpl->assign('plz', $userRow['plz']);
		$tpl->assign('ort', $userRow['ort']);
		$tpl->assign('land', $userRow['land']);
		$tpl->assign('tel', $userRow['tel']);
		$tpl->assign('fax', $userRow['fax']);
		$tpl->assign('altmail', $userRow['altmail']);
		$tpl->assign('mail2sms_nummer', $userRow['mail2sms_nummer']);

		// profile fields
		$profileFields = array();
		$profileData   = array();
		if (strlen($userRow['profilfelder']) > 2) {
			$profileData = @unserialize($userRow['profilfelder']);
			if (!is_array($profileData))
				$profileData = array();
		}
		$res = $db->Query('SELECT id,pflicht,typ,feld,extra FROM {pre}profilfelder WHERE show_li=\'yes\' ORDER BY feld ASC');
		while ($row = $res->FetchArray())
			$profileFields[] = array(
				'id' => $row['id'],
				'title' => $row['feld'],
				'needed' => $row['pflicht'] == 'yes',
				'type' => $row['typ'],
				'extra' => explode(',', $row['extra']),
				'value' => isset($profileData[$row['id']]) ? $profileData[$row['id']] : false
			);
		$res->Free();

		// required fields
		$tpl->assign('f_anrede', $bm_prefs['f_anrede']);
		$tpl->assign('f_strasse', $bm_prefs['f_strasse']);
		$tpl->assign('f_telefon', $bm_prefs['f_telefon']);
		$tpl->assign('f_fax', $bm_prefs['f_fax']);
		$tpl->assign('f_alternativ', $bm_prefs['f_alternativ']);
		$tpl->assign('f_mail2sms_nummer', $bm_prefs['f_mail2sms_nummer']);

		// display
		$tpl->assign('profileFields', $profileFields);
		$tpl->assign('countryList', CountryList());
	}

	function BeforeDisplayTemplate($resourceName, &$tpl)
	{
		global $lang_user, $db, $thisUser, $userRow;

		if (isset($thisUser)) {

			$security_session = session_id() . (int) $thisUser->_id;
			$security_session = sha1($security_session);

			$time     = time();
			$interval = $thisUser->getPref('security_plugin_interval');
			if (strpos($interval, ',') !== false) {
				$interval     = explode(',', $interval);
				$startdisplay = $interval[0] * TIME_ONE_MINUTE;
				$autologout   = $interval[1] * TIME_ONE_MINUTE;

				$tpl->assign('security_startdisplay', $startdisplay);
				$tpl->assign('security_autologout', $autologout);
			}


			$res = $db->Query('SELECT `lastaction` FROM {pre}security_plugin WHERE `usersession`=?', $security_session);

			if ($res->RowCount() > 0) {
				while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
					$lastaction = (int) $row['lastaction'];
				}
				if (!isset($autologout) || $autologout < 1) {
					$autologout = SECURITY_PLUGIN_INTERVAL;
				}
				if (($time - $lastaction) < $autologout) {
					$db->Query('UPDATE {pre}security_plugin SET `lastaction`=? WHERE `usersession`=?', $time, $security_session);
				} else {
					BMUser::Logout();
					header('Location: start.php');
				}

				$res->Free();

			}
		}
		if (isset($_REQUEST['action']) && $_REQUEST['action'] == "userprefs_security") {
			$tpl->addJSFile("li", "./plugins/js/noUiSlider/jquery.nouislider.all.min.js");
			$tpl->addJSFile("li", "./plugins/js/noUiSlider/libLink/jquery.liblink.js");
			$tpl->addCSSFile("li", "./plugins/js/noUiSlider/jquery.nouislider.min.css");


			$tpl->addJSFile("li", "./plugins/js/security.js");
			$tpl->addCSSFile("li", "./plugins/css/security.css");
		}
		$tpl->addCSSFile("li", "./plugins/css/security-ui.css");
		$tpl->addJSFile("li", "./plugins/js/security-ui.js");

	}


}
/**
 * register plugin
 */
$plugins->registerPlugin('security');
?>
