<?php
	class zendesk extends BMPlugin
	{
		function zendesk()
		{
			
			$this->name        = 'Zendesk-Integration';
			$this->author      = 'Martin Buchalik';
			$this->web         = 'http://martin-buchalik.de';
			$this->mail        = 'support@martin-buchalik.de';
			$this->version     = '1.0.1';
			$this->designedfor = '7.3.0';
			$this->type        = BMPLUGIN_DEFAULT;
			
			$this->admin_pages = false;
		}
		
		
		/* ===== Installation ===== */
		
		function Install()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		
		/* ===== Uninstall ===== */
		
		function Uninstall()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
		{
			if ($lang == 'deutsch') {				
				$lang_user['help']         = 'Hilfe';
			} else {
				$lang_user['help']         = 'Help';
			}
		}
		
		/* ===== Integrate our js file ===== */
		
		function BeforeDisplayTemplate($resourceName, &$tpl)
		{
			if(!IsMobileUserAgent()) {
				$tpl->addJSFile("li", "./plugins/js/zn-dsk.js");
			}
		}
	}
	
	/* ===== register plugin ===== */
	$plugins->registerPlugin('zendesk');
?>