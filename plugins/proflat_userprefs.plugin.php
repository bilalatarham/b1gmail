<?php
if (!IsMobileUserAgent()) {
	class proflatuserprefs extends BMPlugin
	{
		function proflatuserprefs()
		{
			
			$this->name        = 'Benutzereinstellungen pro-flat';
			$this->author      = 'Martin Buchalik';
			$this->web         = 'http://martin-buchalik.de';
			$this->mail        = 'support@martin-buchalik.de';
			$this->version     = '1.0.5';
			$this->designedfor = '7.3.0';
			$this->type        = BMPLUGIN_DEFAULT;
			
			$this->admin_pages      = true;
			$this->admin_page_title = 'Proflat-Design';
		}
		
		
		
		function Install()
		{
			global $db;
			
			$db->Query('CREATE TABLE IF NOT EXISTS {pre}proflat_userprefs_admin (
							`id` INT NOT NULL AUTO_INCREMENT,
							`colors` VARCHAR(255) NOT NULL,
							`desc` VARCHAR(255) NOT NULL,							
							PRIMARY KEY (`id`))');
			
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		
		function Uninstall()
		{
			global $db;
			$db->Query('DELETE FROM {pre}userprefs WHERE `key`=?', 'proflat_userprefs_colors');
			$db->Query('DROP TABLE {pre}proflat_userprefs_admin');
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		function AdminHandler()
		{
			global $tpl, $plugins, $lang_admin;
			
			if (!isset($_REQUEST['action']))
				$_REQUEST['action'] = 'proflat_userprefs_admin';
			
			$tabs = array(
				0 => array(
					'title' => 'Designs',
					'link' => $this->_adminLink() . '&',
					'active' => $_REQUEST['action'] == 'proflat_userprefs_admin'
				)
			);
			
			$tpl->assign('tabs', $tabs);
			
			if ($_REQUEST['action'] == 'proflat_userprefs_admin')
				$this->_prefsPage();
		}
		
		
		function _prefsPage()
		{
			global $tpl, $db, $bm_prefs;
			
			// save?
			if (isset($_REQUEST['do']) && $_REQUEST['do'] == 'save') {
				if (isset($_POST['colors']) && isset($_POST['desc'])) {
					$ids    = $_POST['id'];
					$colors = $_POST['colors'];
					$desc   = $_POST['desc'];
					
					foreach ($colors as $key => $value) {
						$value = $db->Escape($value);
						$value = str_replace(' ', '', $value);
						$value = str_replace('#', '', $value);
						$desc[$key]   = $db->Escape($desc[$key]);
						$res          = $db->Query('SELECT colors FROM {pre}proflat_userprefs_admin WHERE id=?', $ids[$key]);
						if ($value != "" && substr_count($value, ",") == 8) {
							if ($res->RowCount() == 0) {
								$db->Query('INSERT INTO {pre}proflat_userprefs_admin(`colors`, `desc`) VALUES(?,?)', $value, $desc[$key]);
							} else {
								$db->Query('UPDATE {pre}proflat_userprefs_admin SET `colors`=?, `desc`=? WHERE `id`=?', $value, $desc[$key], $ids[$key]);
							}
						}
						$res->Free();
					}
				}
			} elseif (isset($_REQUEST['do']) && $_REQUEST['do'] == 'delete' && ctype_digit($_REQUEST['item'])) {
				$db->Query('DELETE FROM {pre}proflat_userprefs_admin WHERE id=?', (int) $_REQUEST['item']);
			}
			
			$res = $db->Query('SELECT colors FROM {pre}proflat_userprefs_admin');
			
			if ($res->RowCount() > 0) {
				$res = $db->Query('SELECT `id`, `colors`, `desc` FROM {pre}proflat_userprefs_admin');
				while ($row = $res->FetchArray(MYSQL_ASSOC)) {
					$displayids[]    = $row['id'];
					$displaycolors[] = $row['colors'];
					$displaydesc[]   = $row['desc'];
				}
			}
			$res->Free();
			
			$tpl->assign('pfids', $displayids);
			$tpl->assign('pfcolors', $displaycolors);
			$tpl->assign('pfdesc', $displaydesc);
			$tpl->assign('pageURL', $this->_adminLink());
			$tpl->assign('page', $this->_templatePath('pf-design.admin.tpl'));
		}
		
		function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
		{
			if ($lang == 'deutsch') {
				
				$lang_user['userprefs_proflat']         = 'Design';
				$lang_user['prefs_d_userprefs_proflat'] = 'Ver&auml;ndern Sie das Aussehen Ihres Benutzer-Bereiches';
				
				if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'userprefs_proflat') {
					
					$lang_user['userprefs_proflat_templates']      = 'Vorlagen';
					
					$lang_user['userprefs_proflat_headline']      = 'Design-Einstellungen';
					$lang_user['userprefs_proflat_topbarBG']      = 'Hintergrund-Farbe der oberen Leiste';
					$lang_user['userprefs_proflat_topbarColor']   = 'Schrift-Farbe der oberen Leiste';
					$lang_user['userprefs_proflat_mainmenuBG']    = 'Hintergrund-Farbe des linken Menüs';
					$lang_user['userprefs_proflat_mainmenuColor'] = 'Schrift-Farbe des linken Menüs';
					$lang_user['userprefs_proflat_maincontentBG'] = 'Hintergrund des Haupt-Inhalts';
					$lang_user['userprefs_proflat_widgetBG1']     = 'Farbe für alle nicht in der nachfolgenden Liste enthaltenen Widgets';
					$lang_user['userprefs_proflat_widgetBG2']     = 'Farbe für die Widgets Aufgaben, WebdiskDND und Speicherplatz(E-Mail)';
					$lang_user['userprefs_proflat_widgetBG3']     = 'Farbe für die Widgets E-Mail (Ordnerübersicht) und Quicklinks';
					$lang_user['userprefs_proflat_widgetBG4']     = 'Farbe für das Kalender-Widget';
				}
				
			} else {
				$lang_user['userprefs_proflat']         = 'Design';
				$lang_user['prefs_d_userprefs_proflat'] = 'Change the look and feel of your personal user area';
				
				if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'userprefs_proflat') {
					
					$lang_user['userprefs_proflat_templates']      = 'Templates';
					
					$lang_user['userprefs_proflat_headline']      = 'Design Settings';
					$lang_user['userprefs_proflat_topbarBG']      = 'Background color of the top bar';
					$lang_user['userprefs_proflat_topbarColor']   = 'Color of the top bar font';
					$lang_user['userprefs_proflat_mainmenuBG']    = 'Background color of the left menu';
					$lang_user['userprefs_proflat_mainmenuColor'] = 'Color of the left menu font';
					$lang_user['userprefs_proflat_maincontentBG'] = 'Background color of the main content';
					$lang_user['userprefs_proflat_widgetBG1']     = 'Color for all widgets not found in the following list';
					$lang_user['userprefs_proflat_widgetBG2']     = 'Color for the widgets Tasks, WebdiskDND and Space(E-Mail)';
					$lang_user['userprefs_proflat_widgetBG3']     = 'Color for the widgets E-Mail (folder overview) and Quicklinks';
					$lang_user['userprefs_proflat_widgetBG4']     = 'Color for the calendar-widget';
				}
				
			}
			
		}
		
		function FileHandler($file, $action)
		{
			if ($file == 'prefs.php') {
				$GLOBALS['prefsItems']['userprefs_proflat']  = true;
				$GLOBALS['prefsImages']['userprefs_proflat'] = 'plugins/templates/images/userprefs_proflat_ico48.png';
				$GLOBALS['prefsIcons']['userprefs_proflat']  = 'plugins/templates/images/userprefs_proflat_ico16.png';
				if ($action == 'userprefs_proflat')
					$this->ProflatHandler();
			}
		}
		
		function UserPrefsPageHandler($action)
		{
			global $tpl;
			
			if ($action != 'userprefs_proflat')
				return (false);
			
			$tpl->assign('pageContent', $this->_templatePath('userprefs.proflat.tpl'));
			$tpl->display('li/index.tpl');
			
			return (true);
		}
		
		function ProflatHandler()
		{
			global $db, $thisUser;
			
			
			$check_posts = array(
				"topbarBG",
				"topbarColor",
				"mainmenuBG",
				"mainmenuColor",
				"maincontentBG",
				"widgetBG1",
				"widgetBG2",
				"widgetBG3",
				"widgetBG4"
			);
			$posted      = !array_diff($check_posts, array_keys($_POST));
			if (isset($_REQUEST['do']) && $_REQUEST['do'] == 'save' && isset($_POST['reset']) && $_POST['reset'] == 'yes') {
				$thisUser->DeletePref('proflat_userprefs_colors');
			}
			
			elseif (isset($_REQUEST['do']) && $_REQUEST['do'] == 'save' && $posted) {
				$save = "";
				foreach ($check_posts as $value) {
					$value = $db->Escape($_POST[$value]);
					if (ctype_alnum($value) && strlen($value) < 7) {
						if ($save == "") {
							$save = $value;
						} else {
							$save = $save . "," . $value;
						}
					} else {
						$save = false;
						break;
					}
				}
				if ($save) {
					$thisUser->setPref('proflat_userprefs_colors', $save);
				}
			}
			
			
		}
		
		function BeforeDisplayTemplate($resourceName, &$tpl)
		{
			global $lang_user, $db, $thisUser;
			
			$elements = array(
				"topbarBG",
				"topbarColor",
				"mainmenuBG",
				"mainmenuColor",
				"maincontentBG",
				"widgetBG1",
				"widgetBG2",
				"widgetBG3",
				"widgetBG4"
			);
			
			if ($thisUser && $thisUser->getPref('proflat_userprefs_colors')) {
				$userdesign = $thisUser->getPref('proflat_userprefs_colors');
				$userdesign = explode(',', $userdesign);
				
				$i = 0;
				foreach ($elements as $element) {
					$tpl->assign('user' . $element, $userdesign[$i]);
					$i++;
				}
				$tpl->addJSFile("li", "./plugins/js/proflat_userprefs_display.js");
			} else {
				foreach ($elements as $element) {
					$tpl->assign('user' . $element, '');
				}
			}
			
			if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'userprefs_proflat') {
				$tpl->addJSFile("li", "./plugins/js/colpick-jQuery-Color-Picker/js/colpick.js");
				$tpl->addJSFile("li", "./plugins/js/proflat_userprefs.js");
				$tpl->addCSSFile("li", "./plugins/js/colpick-jQuery-Color-Picker/css/colpick.css");
				$tpl->addCSSFile("li", "./plugins/css/proflat_userprefs.css");
				$tpl->assign('is_proflat_userprefs', true);
				
				$res = $db->Query('SELECT `colors`, `desc` FROM {pre}proflat_userprefs_admin');
				if ($res->RowCount() == 0) {
					$maincolors = $admincolors = $admindesc = false;
				} else {
					while ($row = $res->FetchArray(MYSQL_ASSOC)) {
						$admincolors[] = $row['colors'];
						$admindesc[]   = $row['desc'];
					}
					foreach ($admincolors as $key => $value) {
						$value = explode(',', $value);
						foreach ($value as $vkey => $vvalue) {
							$admincolor[$key][$vkey] = $vvalue;
						}
							$maincolors[$key][0] = $admincolor[$key][0];
							$maincolors[$key][1] = $admincolor[$key][2];
							$maincolors[$key][2] = $admincolor[$key][4];
					}
				}
				
				$res->Free();
				
				
				$tpl->assign('pf_userpefs_admincolors', $admincolors);
				$tpl->assign('pf_userpefs_admincolors_main', $maincolors);
				$tpl->assign('pf_userpefs_admindesc', $admindesc);
			}
			
			
		}
		
		
	}
	/**
	 * register plugin
	 */
	$plugins->registerPlugin('proflatuserprefs');
}
?>