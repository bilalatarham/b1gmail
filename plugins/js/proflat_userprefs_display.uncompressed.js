$( document ).ready(function() {
	$("#mainToolbar").css('background', '#'+topbarBG);
	$("#mainToolbar").css('color', '#'+topbarColor);
	$("#mainMenu").css('background', '#'+mainmenuBG);
	$("#mainMenu").css('color', '#'+mainmenuColor);
	$("#mainContent").css('background', '#'+maincontentBG);
	$(".dragItem").css('background', '#'+widgetBG1);
	$("#BMPlugin_Widget_Tasks, #dc_startBoxesdragItem_BMPlugin_Widget_Tasks .dragBar, #BMPlugin_Widget_WebdiskDND, #BMPlugin_Widget_Mailspace").css('background', '#'+widgetBG2);
	$("#BMPlugin_Widget_EMail, #BMPlugin_Widget_Quicklinks").css('background', '#'+widgetBG3);
	$("#BMPlugin_Widget_Calendar").css('background', '#'+widgetBG4);
});