$(document).ready(function() {
    $("#bottomLayer_attachments .contentHeader .left").prepend('<input type="checkbox" id="selectallatt" />');
    $("#selectallatt").click(function() {
        $('input[name="att[]"]').attr('checked', this.checked);
    });

    $('input[name="att[]"]').click(function() {

        if ($('input[name="att[]"]').length == $('input[name="att[]"]:checked').length) {
            $("#selectallatt").attr("checked", "checked");
        } else {
            $("#selectallatt").removeAttr("checked");
        }

    });
});