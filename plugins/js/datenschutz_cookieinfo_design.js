window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "text": "#FFFFFF",
                "background": "#000000",
            },
            "button": {
                "text": "#000000",
                "background": "#f1d600",
            }
        },
        "theme": "edgeless",
        "content": {
            "message": "Wir verwenden Cookies, um Ihnen den bestmöglichen Service zu gewährleisten. Wenn Sie auf der Seite weitersurfen stimmen Sie der Nutzung zu.",
            "dismiss": "Ich stimme zu",
            "link": "Weitere Informationen",
            "href": "/datenschutz"
        }
    })
});