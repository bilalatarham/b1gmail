var security_timerid;
var security_timeoutid;
var security_sec = security_autologout;
function timercontent() {
	var security_minutes = Math.floor(security_sec/60);
	var security_seconds = security_sec - (security_minutes*60);
	var security_lngminutes = " Minuten";
	var security_lngminute = " Minute";
	var security_lngseconds = " Sekunden";
	var security_lngsecond = " Sekunde";

	
	if(security_minutes == 1)
		var security_displayminutes = security_lngminute;
	else
		var security_displayminutes = security_lngminutes;
	if(security_seconds == 1)
		var security_displayseconds = security_lngsecond;
	else
		var security_displayseconds = security_lngseconds;  
	$('#security-overlay.show #security-countdown.running').text(security_minutes + security_displayminutes+" und "+security_seconds + security_displayseconds+" verbleibend");

   security_sec--;
   if (security_sec == -1) {    
	 $('#security-countdown').removeClass("running");
	 document.location.href='start.php?sid='+currentSID+'&action=logout';
   } 
};
function maintimer() {
	security_sec = security_autologout - security_startdisplay;
	security_timerid = setInterval(timercontent, 1000);
	$("#security-overlay").addClass("show");
}
function starttimer() {
	security_timeoutid = window.setTimeout(maintimer, security_startdisplay * 1000);
}
$(document).ready(function() {	
	$("#security-keep").click(function() {
		$.ajax({
                type: "POST",
                async: true,
                url: 'start.php?sid=' + currentSID,
                data: "",
                success: function(data) {
                   clearTimeout(security_timeoutid);
				   clearInterval(security_timerid);
				   starttimer();
                }
            });
		
		$("#security-overlay").removeClass("show");
	});
});
starttimer();