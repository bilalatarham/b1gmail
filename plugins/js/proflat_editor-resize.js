$(document).ready(function() {
    $("#contentHeader .left").append('<i id="pf-editor-maximize" class="fa fa-chevron-up" style="cursor: pointer;"></i><i id="pf-editor-showall" class="fa fa-chevron-down" style="display: none; cursor: pointer;"></i>');
	
	$("#pf-editor-maximize").on( "click", function() {
		$("#composeHeader").hide("slow", function(){ $("#composeHeader").height(0); composeSizer(true); });
		$(this).hide("slow");
		$("#pf-editor-showall").show("slow");
	});
	$("#pf-editor-showall").on( "click", function() {
		$("#composeHeader").height("auto");
		$("#composeHeader").show("slow", function(){ composeSizer(true); });
		$(this).hide("slow");
		$("#pf-editor-maximize").show("slow");
	});
});