$(document).ready(function() {
    //go to the next stage
    $(".step").click(function() {
        $(this).parent().parent().hide("slow");
        $(this).parent().parent().next().removeClass("hidden");
    });
    $("#pop3extra1end").click(function() {
        $(".extrastage1").hide("slow");
        $(".stage2").show("slow");
        return (false);
    });
    //add another pop3 account
    $("#pop3goback").click(function() {
        $('#pop3form').trigger("reset");
        $("#pop3account").addClass("hidden");
        $("#pop3account").hide();
        $("#pop3serverlist").select2('val', '');
        $(this).parent().parent().addClass("hidden");
        $(".stage2").show("slow");
    });
    $("#pop3addfolder").click(function() {
        $(".stage2").hide("slow");
        $(".extrastage1").show("slow");
        return (false);
    });
    $("#pop3savefolder").click(function() {
        if ($("#pop3foldertitel").val().length > 1) {
            $(".pop3loading").show();
            $.ajax({
                type: "POST",
                async: true,
                url: 'email.folders.php?action=createFolder&sid=' + currentSID,
                data: $("#pop3folderform").serialize(),
                success: function(data) {
                    var page = "start.php?usertour=folderlist&sid=" + currentSID;
                    $("#pop3targetholder").load(page + " #pop3target", function() {
                        $(".pop3loading").hide();
                        $(".extrastage1").hide("slow");
                        $(".stage2").show("slow");
                        $("#p_target").select2();
                    });
                }
            });
        } else {
            $("#pop3foldererror").show();
        }
        return (false);
    });
    //update pop3 account information
    $('#pop3serverlist').select2().on("change", function(e) {
        var page = "start.php?usertour=pop3servers&server=" + $("#pop3serverlist").val() + "&sid=" + currentSID;
        $(".pop3loading").show();
        $("#pop3server").load(page + " #usertour_data", function() {
            $("#pop3server").html($("#usertour_data").html());
            var pop3server = $("#pop3server").html().trim();
            pop3server = pop3server.split(',');
            if (pop3server[0] == "other") {
                $("#pop3advanced").show();
                $("#p_host").val('');
            } else {
                $("#pop3advanced").hide();
                $("#p_host").val(pop3server[0]);
            }
            $("#p_port").val(pop3server[1]);
            if (pop3server[2] == "no")
                $('#p_ssl').prop('checked', false);
            else
                $('#p_ssl').prop('checked', true);
            $(".pop3loading").hide();
            $("#pop3account").show();
        });
    });
    //activate select2 for pop3 settings
    $("#pop3serverlist").select2({
        placeholder: pleasechoose,
        allowClear: true
    });
    $('#p_target').select2();
    //save pop3 settings
    $("#savepop3").click(function() {
        $(".pop3loading").show();
        $.ajax({
            type: "POST",
            async: true,
            url: 'prefs.php?action=extpop3&do=createAccount&sid=' + currentSID,
            data: $("#pop3form").serialize(),
            success: function(data) {
                if (data.indexOf(pop3loginerror) != -1) {
                    $("#pop3error").html(pop3loginerror);
                    $("#pop3error").show();
                } else if (data.indexOf(pop3ownerror) != -1) {
                    $("#pop3error").html(pop3ownerror);
                    $("#pop3error").show();
                } else if (data.indexOf(toomanyaccounts) != -1) {
                    $("#pop3error").html(toomanyaccounts);
                    $("#pop3error").show();
                } else {
                    $("#pop3error").hide();
                    $("#pop3form").parent().hide("slow");
                    $("#pop3form").parent().next().removeClass("hidden");
                }
                $(".pop3loading").hide();
            }
        });
        return (false);
    });
    //go to step2
    $(".endpop3").click(function() {
        $("#usertour_main").html('<i class="fa fa-cog fa-spin fa-2x"></i>');
        page = "start.php?usertour=step2&sid=" + currentSID;
        $("#usertour").load(page + " #usertour_main", function() {
            $("#email_domain").select2();
        });
    });

    $(document).on('click', '#savealias', function() {
        $("#aliasloading").show();
        $.ajax({
            type: "POST",
            async: true,
            url: 'prefs.php?action=aliases&do=create&sid=' + currentSID,
            data: $("#aliasform").serialize(),
            success: function(data) {
                if (data.indexOf(addressinvalid) != -1) {
                    $("#aliaserror").html(addressinvalid);
                    $("#aliaserror").show();
                } else if (data.indexOf(toomanyaliases) != -1) {
                    $("#aliaserror").html(toomanyaliases);
                    $("#aliaserror").show();
                } else if (data.indexOf(addresstaken) != -1) {
                    $("#aliaserror").html(addresstaken);
                    $("#aliaserror").show();
                } else {
                    $("#aliaserror").hide();
                    $("#aliasform").parent().hide("slow");
                    $("#aliasform").parent().next().removeClass("hidden");

                }
                $("#aliasloading").hide();
            }
        });
        return (false);
    });
    $(document).on('click', '#savesignature', function() {
        if ($("#signaturetitel").val().length > 1) {
            $("#signatureloading").show();
            $.ajax({
                type: "POST",
                async: true,
                url: 'prefs.php?action=signatures&do=createSignature&sid=' + currentSID,
                data: $("#signatureform").serialize(),
                success: function(data) {
                    $("#signatureerror").hide();
                    $("#signatureform").parent().hide("slow");
                    $("#signatureform").parent().next().removeClass("hidden");
                    $("#signatureloading").hide();
                }

            });
        } else {
            $("#signatureerror").show();
        }
        return (false);
    });

});

$(document).on('change', '#typ_1', function() {
    if (this.checked) {
        $("#alias1").show().addClass("fadeInUp");
        $("#savealias").show().removeClass("fadeInUp").addClass("fadeInUp");
        $("#alias2").hide().removeClass("fadeInUp");
    }
});
$(document).on('change', '#typ_3', function() {
    if (this.checked) {
        $("#alias2").show().addClass("fadeInUp");
        $("#savealias").show().removeClass("fadeInUp").addClass("fadeInUp");
        $("#alias1").hide().removeClass("fadeInUp");
    }
});
$(document).on('click', '#aliasgoback', function() {
    $('#aliasform').trigger("reset");
    $("#alias2").hide();
    $("#savealias").hide();
    $("#addressAvailabilityIndicator").html("");
    $(this).parent().parent().addClass("hidden");
    $(".stage1").show("slow");
});
$(document).on('click', '.endalias', function() {
    $("#usertour_main").html('<i class="fa fa-cog fa-spin fa-2x"></i>');
    page = "start.php?usertour=step3&sid=" + currentSID;
    $("#usertour").load(page + " #usertour_main", function() {});
});
$(document).on('click', '#signaturegoback', function() {
    $('#signatureform').trigger("reset");
    $(this).parent().parent().addClass("hidden");
    $(".stage1").show("slow");
});
$(document).on('click', '.endsignature', function() {
    $("#usertour_main").html('<i class="fa fa-cog fa-spin fa-2x"></i>');
    page = "start.php?usertour=step4&sid=" + currentSID;
    $("#usertour").load(page + " #usertour_main", function() {});
});
$(document).on('click', '#startaddressbookimport', function() {
    openOverlay('organizer.addressbook.php?sid=' + currentSID + '&action=importDialogStart',
        lang['import'],
        440,
        140,
        true);
});

//end the tour
$(document).on('click', '.endtour', function() {
    $("#usertour").hide("slow");
    $.ajax({
        type: "POST",
        async: true,
        url: 'start.php?usertour=end&sid=' + currentSID,
        data: '',
        success: function(data) {

        }
    });
    setTimeout(
        function() {
            $("#usertour").remove();
        }, 1000);
    return (false);
});