$(document).ready(function() {
    var pos = ["topbarBG", "topbarColor", "mainmenuBG", "mainmenuColor", "maincontentBG", "widgetBG1", "widgetBG2", "widgetBG3", "widgetBG4"];
    jQuery.each(pos, function(i, val) {
        $('#' + val + '_picker').colpick({
            onChange: function(hsb, hex, rgb) {
                $("#" + val).val(hex);
                if (val == "topbarBG")
                    $("#mainToolbar").css('background', '#' + hex);
                else if (val == "topbarColor")
                    $("#mainToolbar").css('color', '#' + hex);
                else if (val == "mainmenuBG")
                    $("#mainMenu").css('background', '#' + hex);
                else if (val == "mainmenuColor")
                    $("#mainMenu").css('color', '#' + hex);
                else if (val == "maincontentBG")
                    $("#mainContent").css('background', '#' + hex);
                else if (val == "widgetBG1")
                    $(".dragItem").css('background', '#' + hex);
                else if (val == "widgetBG2")
                    $("#BMPlugin_Widget_Tasks, #dc_startBoxesdragItem_BMPlugin_Widget_Tasks .dragBar, #BMPlugin_Widget_WebdiskDND, #BMPlugin_Widget_Mailspace").css('background', '#' + hex);
                else if (val == "widgetBG3")
                    $("#BMPlugin_Widget_EMail, #BMPlugin_Widget_Quicklinks").css('background', '#' + hex);
                else if (val == "widgetBG4")
                    $("#BMPlugin_Widget_Calendar").css('background', '#' + hex);
            },
            flat: true,
            layout: 'hex',
            submit: 0
        });
        $('#' + val + '_picker').colpickSetColor($("#" + val).val(), true);
    });
    $('#resetvals').click(function() {
        $('#reset_pf_usersettings').val('yes');
        $('#f1').submit();
    });
	 $('.template-select').click(function() {
        var newcolors = $(this).find("input").val();
		newcolors = newcolors.split(',');
		for (i = 0; i < newcolors.length; i++) { 
			$('#' + pos[i] + '_picker').colpickSetColor(newcolors[i]);
		}
		$('#templates-holder').hide("slow");
    });
	 $('#templates').click(function() {
			$('#templates-holder').show("slow");
			return(false);
	});
	$('#templates-close').click(function() {
		$('#templates-holder').hide("slow");								 
	});
});