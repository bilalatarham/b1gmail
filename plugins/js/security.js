if (typeof security_startdisplay !== 'undefined')
	var settings_startdisplay = security_startdisplay/60;
else
	var settings_startdisplay = 30;
if (typeof security_autologout !== 'undefined')
	var settings_autologout = security_autologout/60;
else
	var settings_autologout = 40;
$(document).ready(function() {

		
    $(".slider").noUiSlider({
		start: [settings_startdisplay, settings_autologout],
		handles: 2,
		margin: 10,
		connect: true,
		range: {
			'min': 20,
			'max': 600
		},
		format: wNumb({
			decimals: 0
		})
	});
	$(".slider").Link('lower').to($("#startdisplay"));
	$(".slider").Link('upper').to($("#autologout"));
	$(".slider").Link('lower').to('-inline-<div class="tooltip left"></div>', function ( value ) {
		$(this).html(
			'<div class="toolheading">Benachrichtigung anzeigen: </div>' +
			'<span>nach ' + value + ' Minuten</span>'
		);
	});
	$(".slider").Link('upper').to('-inline-<div class="tooltip right"></div>', function ( value ) {
		$(this).html(
			'<div class="toolheading">Automatischer Logout: </div>' +
			'<span>nach ' + value + ' Minuten</span>'
		);
	});
});
  