$(document).ready(function() {



    $('#pass1').on('input', function() {

        if (/\d/.test(this.value)) {
            digits = true;
            $('#password-security-digits').addClass('inpw');
        } else {
            digits = false;
            $('#password-security-digits').removeClass('inpw');
        }
        if (/[a-z]/.test(this.value)) {
            smallletters = true;
            $('#password-security-smallletters').addClass('inpw');
        } else {
            smallletters = false;
            $('#password-security-smallletters').removeClass('inpw');
        }
        if (/[A-Z]/.test(this.value)) {
            capitalletters = true;
            $('#password-security-capitalletters').addClass('inpw');
        } else {
            capitalletters = false;
            $('#password-security-capitalletters').removeClass('inpw');
        }
        if (/[^a-zA-Z_0-9]/.test(this.value)) {
            specialchars = true;
            $('#password-security-specialchars').addClass('inpw');
        } else {
            specialchars = false;
            $('#password-security-specialchars').removeClass('inpw');
        }

        var differentchars = '';
        var valuemap = this.value.split('');
        for (var i = 0; i < valuemap.length; i++) {
            if (differentchars.indexOf(valuemap[i]) == -1) {
                differentchars += valuemap[i];
            }
        }

        length = this.value.length;

        var pwsecurity = 0;
        if (length > 5)
            pwsecurity++;
        if (differentchars.length > 6)
            pwsecurity++;
        if (digits)
            pwsecurity++;
        if (smallletters)
            pwsecurity++;
        if (capitalletters)
            pwsecurity++;
        if (specialchars)
            pwsecurity++;
        if (length < 3)
            pwsecurity = pwsecurity / 2;
        if (differentchars.length < 3)
            pwsecurity = pwsecurity / 2;

        $('#password-security-bar').width(pwsecurity / 6 * 100 + '%');
		var color = '#D92300';
		if(pwsecurity > 2)
			color = '#F5EE1D';
		if(pwsecurity > 4)
			color = '#4dcc01';
		$('#password-security-bar').css("background-color", color);
    });

});