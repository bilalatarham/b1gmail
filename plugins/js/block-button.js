$(document).ready(function() {
	
	//click handler for our two possible actions
	$(document).on("click", ".block-button-action", function(e) {
		var block_data = block_action = "";
		
		//get the mail address (or domain) we want to block
		block_data = $(this).find(".block-button-data").text();
		block_data = encodeURIComponent(block_data);
		if($(this).is("#block-button-fullmail"))
			block_action = "fullmail";
		else
			block_action = "ending";	
			
		blockbutton.nextStep();
		
		$.ajax({
			type: "GET",
			url: "email.read.php?addBlockFilterTo=" + block_data + "&type=" + block_action + "&sid=" + currentSID,		
			success: function(data) {	
				blockbutton.nextStep();
				valid_json = true;
				//check if the response if valid JSON
				try {
					data = $.parseJSON(data);
				} catch (error) {
					valid_json = false;
				}
				if (valid_json) {
					if (data["status"] == "success") {
						$("#block-button-status").html(
							'<i class="fa fa-check fa-5x"></i>'+
							 data["message"]+
							 '<br/>'
														 );
					}
					else if (data["status"] == "error") {	
						$("#block-button-status").html(
							'<i class="fa fa-exclamation-triangle fa-5x red"></i>'+
							 data["message"]+
							 '<br/>'
														 );
					}
				} else {
					$("#block-button-status").html('<i class="fa fa-exclamation-triangle fa-5x red"></i>');
				}

			}	
		});
		
	});
});


var blockbutton = {
	/* ===== openOverlay ===== 
	----- mailAddress: The full email address we want to be able to proceed in our overlay
	*/
	openOverlay: function(mailAddress) {
		var mailDomain;
		if(!mailAddress.length)
			return;
			
		mailDomain = mailAddress.split('@');
		mailDomain = mailDomain[mailDomain.length-1];
		mailDomain = '@' + mailDomain;
		
		$("#block-button-fullmail").find(".block-button-data").text(mailAddress);
		$("#block-button-ending").find(".block-button-data").text(mailDomain);
		
		$("#block-button-container .block-button-step").addClass("hidden").removeClass("visible").hide();
		$("#block-button-container .block-button-step").first().addClass("visible").removeClass("hidden").show();
		
		$("#block-button-overlay").show();
		$("#block-button-container").fadeIn("slow");
	},
	/* ===== nextStep =====
	Show the next step in our overlay (and hide the current one)
	*/
	nextStep: function () {
		$("#block-button-container .block-button-step.visible").removeClass("visible").hide("slow", function() {
			$(this).addClass("hidden");
		}).next().addClass("visible").fadeIn("slow", function() {
			$(this).removeClass("hidden");														   
		});	
	},
	/* ===== closeOverlay =====
	Close the overlay
	*/
	closeOverlay: function() {
		$("#block-button-overlay").fadeOut("slow");
		$("#block-button-container").fadeOut("fast");
	}
}