<?php
class blockbutton extends BMPlugin
{
	function blockbutton()
	{
		
		$this->name        = 'Block-Button bei E-Mail-Lesen-Ansicht';
		$this->author      = 'Martin Buchalik';
		$this->web         = 'http://martin-buchalik.de';
		$this->mail        = 'support@martin-buchalik.de';
		$this->version     = '1.0.0';
		$this->designedfor = '7.3.0';
		$this->type        = BMPLUGIN_DEFAULT;
		
		$this->admin_pages = false;
	}
	
	
	/* ===== Installation ===== */
	
	function Install()
	{
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	
	/* ===== Uninstall ===== */
	
	function Uninstall()
	{
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	/* ===== Language variables ===== */
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if ($lang == 'deutsch') {
			$lang_user['block-button']                 = 'Blocken';
			$lang_user['block-button_filter-existing'] = 'Der Filter existiert bereits!';
			$lang_user['block-button_filter-success']  = 'Filter erfolgreich erstellt!';
		} else {
			$lang_user['block-button']                 = 'Block';
			$lang_user['block-button_filter-existing'] = 'Filter already existing!';
			$lang_user['block-button_filter-success']  = 'Filter created successfully!';
		}
	}
	
	
	function FileHandler($file, $action)
	{
		global $tpl, $thisUser, $lang_user;
		//only affect email.php and email.read.php - we don't want to add our files somewhere else!
		if ($file != "email.php" && $file != "email.read.php" || IsMobileUserAgent())
			return;
		//proceed our ajax request
		if (isset($_REQUEST['addBlockFilterTo']) && strlen($_REQUEST['addBlockFilterTo']) > 0) {
			$filterAppliedTo = $_REQUEST['addBlockFilterTo'];
			$filtername      = "Block-Filter " . $filterAppliedTo;
			$result          = array();
			$blocktype       = 1;
			
			if (isset($_REQUEST['type']) && $_REQUEST['type'] == "ending")
				$blocktype = 6;
			
			$filters         = $thisUser->GetFilters();
			$filter_existing = false;
			foreach ($filters as $value) {
				if ($value["title"] == $filtername) {
					$filter_existing = true;
					break;
				}
			}
			if ($filter_existing) {
				$result["status"]  = "error";
				$result["message"] = $lang_user['block-button_filter-existing'];
				echo json_encode($result);
				exit();
			}
			//Add a new filter
			$filterID = $thisUser->AddFilter($filtername, 1);
			
			//Get the condition ID
			$conditions  = $thisUser->GetFilterConditions($filterID);
			$conditionID = key($conditions);
			
			//Change the condition to fit our needs
			$thisUser->UpdateFilterCondition($conditionID, $filterID, 2, $blocktype, $filterAppliedTo);
			
			//Get the action ID
			$actions  = $thisUser->GetFilterActions($filterID);
			$actionID = key($actions);
			
			//Change the action to fit our needs
			$thisUser->UpdateFilterAction($actionID, $filterID, 3, 0, "");
			
			$result["status"]  = "success";
			$result["message"] = $lang_user['block-button_filter-success'];
			
			echo json_encode($result);
			exit();
			
		}
		
		$tpl->addJSFile("li", "./plugins/js/block-button.js");
		$tpl->addCSSFile("li", "./plugins/css/block-button.css");
		$tpl->registerHook("email.folder.tpl:foot", $this->_templatePath('../../plugins/templates/block-button_overlay.tpl'));
		$tpl->registerHook("email.read.tpl:foot", $this->_templatePath('../../plugins/templates/block-button_overlay.tpl'));
	}
}

/* ===== register plugin ===== */
$plugins->registerPlugin('blockbutton');
?>