<?php
/*
 * Plugin smstan_plugin
 */
// link automatisch anzeigen
define("PLUGIN_SMSTAN_LINK", true);
class smstan_plugin extends BMPlugin 
{
	/*
	 * Eigenschaften des Plugins
	 */
	function smstan_plugin()
	{
		$this->name					= 'smsTAN';
		$this->version				= '1.2.1';
		$this->designedfor			= '7.3.0';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'dotaachen';
		$this->mail					= 'b1g@dotaachen.net';
		$this->web 					= 'http://b1g.dotaachen.net';

		$this->update_url			= 'http://my.b1gmail.com/update_service/';
		$this->website				= 'http://my.b1gmail.com/details/129/';

		$this->admin_pages			= true;
		$this->admin_page_title		= 'smsTAN';
		$this->admin_page_icon		= "smstan.logo.16.png";

		$this->RegisterGroupOption('smstan_plugin', FIELD_CHECKBOX, 'smsTAN?');
	}

	/*
	* Link und Tabs im Adminbereich 
	*/
	function AdminHandler()
	{
		global $tpl, $lang_admin;

		// Plugin aufruf ohne Action
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'page1';

		// Tabs im Adminbereich
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['common'],
				'link'		=> $this->_adminLink() . '&action=page1&',
				'active'	=> $_REQUEST['action'] == 'page1',
				'icon'		=> '../plugins/templates/images/smstan.logo.png'
			),
			1 => array(
				'title'		=> $lang_admin['lastlogin'],
				'link'		=> $this->_adminLink() . '&action=page2&',
				'active'	=> $_REQUEST['action'] == 'page2',
				'icon'		=> '../plugins/templates/images/smstan.logo.png'
			),
		);
		$tpl->assign('tabs', $tabs);

		// Plugin aufruf mit Action 
		if($_REQUEST['action'] == 'page1') {
			$tpl->assign('page', $this->_templatePath('smstan.page1.acp.tpl'));
			$this->_Page1();
		} else if($_REQUEST['action'] == 'page2') {
			$tpl->assign('page', $this->_templatePath('smstan.page2.acp.tpl'));
			$this->_Page2();
		}
	}

	/*
	 *  Sprach variablen
	 */
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		$lang_user['smstan'] 		= $lang_admin['smstan_name']= 'smsTAN';
		$lang_user['smstan_text']	= $lang_admin['smstan_text']= 'Ihnen wird per SMS ein Bestätigungscode auf Ihr Mobiltelefon gesendet. Die Anmeldung muss anschlie&szlig;end mit diesem Bestätigungscode best&auml;tigt werden.';
		$lang_user['smstan_text2']								= 'Bitte geben Sie im unteren Feld Ihren Bestätigungscode ein.';
		$lang_user['smstan_tan']								= 'Bestätigungscode';
		$lang_user['smstan_requesttan']							= 'Anfordern';
		$lang_user['smstan_codeexist']							= 'Bitte haben Sie daf&uuml;r Verst&auml;tndnis, dass eine TAN aus Sicherheitsgr&uuml;nden nur einmal innerhalb von 15 Minuten angefordert werden kann.';
		$lang_user['smstan_grouperror']							= 'Sie sind nicht berechtigt, dass smsTAN Verfahren zu nutzen.';
		$lang_user['smstan_badcode']							= 'Die angegebene TAN stimmt nicht mit der in unseren Unterlagen hinterlegten TAN &uuml;berein. Bitte versuchen Sie es erneut.';
		$lang_user['smstan_priceerror']							= 'Bedauerlicherweise weist Ihr Account nicht gen&uuml;gend Credits auf, um Ihnen das Passwort per SMS zu senden. Bitte wenden Sie sich direkt an den Support!';
		$lang_user['smstan_nosms_nummer']						= 'Leider ist in Ihrem Account keine Handy Nummer hinterlegt. Tragen Sie diese bitte ein, um das smsTAN Verfahren nutzen zu k&ouml;nnen.';
		$lang_custom['smstan_sms_text'] 						= "Ihr neuer Bestätigungscode für %%user%% lautet: %%code%%";
		$lang_admin['text_smstan_sms_text'] 					= $this->name . ': SMS Text';
		$lang_admin['smstan_abbuchen']							= 'SMS vom Benutzer abbuchen';

		$lang_user['smstan_free']								= 'Ihnen entstehen durch die Nutzung dieses Services keine Kosten. Der Versand des Bestätigungscode ist kostenlos.';
		$lang_user['smstan_price']								= 'Der Versand der TAN mittels SMS wird mit %%credits%% Credits abgerechnet.';

		$lang_user['smstan_allprice']							= 'Ihnen wurden innerhalb der letzten 30 Tage f&uuml;r diesen Dienst %%credits%% Credits abgebucht.';
		$lang_user['smstan']									= 'smsTAN';
		$lang_user['prefs_d_smstan']							= 'Ihnen wird, wie beim Online Banking, per SMS eine TAN auf Ihr Mobiltelefon gesendet.';
		$lang_user['ip']										= $lang_admin['ip'];
	}

	/*
	 * installation routine
	 */
	function Install()
	{
		global $db;

		$db->Query('Create TABLE IF NOT EXISTS {pre}mod_smstan_keys (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`userid` INT(11) NOT NULL,
			`code` VARCHAR(1000) NOT NULL,
			`time` INT(11) NOT NULL,
			PRIMARY KEY (`id`))');

		$db->Query('Create TABLE IF NOT EXISTS {pre}mod_smstan_log (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`userid` INT(11) NOT NULL,
			`IP` VARCHAR(40) NOT NULL,
			`time` INT(11) NOT NULL,
			`credits` INT(3) NOT NULL,
			PRIMARY KEY (`id`))');

		$db->Query('Create TABLE IF NOT EXISTS {pre}mod_smstan_banip (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`IP` VARCHAR(40) NOT NULL,
			`time` INT(11) NOT NULL,
			PRIMARY KEY (`id`))');

		$this->_setPref("fromno", "");
		$this->_setPref("smstype", 0);
		$this->_setPref("chargefromuser", 1);
			
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		global $db;

		$db->Query('DROP TABLE {pre}mod_smstan_keys');
		$db->Query('DROP TABLE {pre}mod_smstan_log');
		$db->Query('DROP TABLE {pre}mod_smstan_banip');

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
    }

	function _Page1()
	{
		global $tpl, $db;

		// save
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$this->_setPref("fromno", 			$_REQUEST['fromno']);
			$this->_setPref("smstype",			$_REQUEST['smstype']);
			$this->_setPref("chargefromuser",	(int)isset($_REQUEST['chargefromuser']) ? 1 : 0);
		}
		
		$res = $db->Query('SELECT COUNT(ip) AS c FROM {pre}mod_smstan_banip');
		$row = $res->FetchArray(MYSQLI_ASSOC);
		$res->Free();

		$tpl->assign('banips', 			$row['c']);
		$tpl->assign('fromno', 			$this->_getPref("fromno"));
		$tpl->assign('allsmstype',		$this->GetSMSTypes());
		$tpl->assign('smstype',			$this->_getPref("smstype"));
		$tpl->assign('chargefromuser', 	$this->_getPref("chargefromuser"));
	}

	function _Page2()
	{
		global $tpl, $db;
		
		$logs = array();
		$res = $db->Query('SELECT * FROM {pre}mod_smstan_log Order by time DESC');
		while($row = $res->FetchArray())
		{
			$logs[] = 			$row;
		}
		$res->Free();
		$tpl->assign('logs', 		$logs);
	}

	function OnCron()
	{
		global $db;
		$db->Query('DELETE FROM {pre}mod_smstan_keys WHERE time<UNIX_TIMESTAMP()');
		$db->Query('DELETE FROM {pre}mod_smstan_log WHERE time<(UNIX_TIMESTAMP()-(30*24*60*60))');
		$db->Query('DELETE FROM {pre}mod_smstan_banip WHERE time<UNIX_TIMESTAMP()');
	}

	function getUserPages($loggedin)
	{
		global $lang_user;

		if($loggedin)
			return(array());

		if(PLUGIN_SMSTAN_LINK)
		{
			return(array(
				'smstan' => array(
					'text'	=> $lang_user['smstan'],
					'link'	=> 'index.php?action=smstan'
				)
			));
		} else {
			return(array());
		}
	}

	/*
	 * Prefs anzeigen
	 */
	function UserPrefsPageHandler($action)
	{
		global $tpl, $db, $userRow, $thisUser, $lang_user;

		if($action == "smstan")
		{
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'del')
			{
				$db->Query('Delete FROM {pre}mod_smstan_log Where id=? AND userid=?', 
					(int) $_REQUEST['id'],
					(int) $userRow["id"]);
			}
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
			{
				$smstan_allow 		= isset($_REQUEST['smstan_allow']) ? 1 : 0;
				$thisUser->SetPref("smstan_disable", 	!$smstan_allow);
			}

			$logs = array();
			$res = $db->Query('SELECT * FROM {pre}mod_smstan_log Where userid=? Order by time DESC Limit 10',
				(int)$userRow["id"]);
			while($row = $res->FetchArray())
			{
				$logs[] = 			$row;
			}
			$res->Free();
			
			$pricetext		= $lang_user['smstan_allprice'];
			$preis			= $this->GetSumPaidCredits($userRow["id"]);
			$preis			= isset($preis) ? $preis : 0;
			$pricetext 		= str_replace('%%credits%%', $preis, $pricetext);

			$tpl->assign('pricetext', 		$pricetext);
			$tpl->assign('chargefromuser', 	$this->_getPref("chargefromuser"));
			
			$tpl->assign('logs', 			$logs);
			$tpl->assign('smstan_allow', 	!$thisUser->GetPref("smstan_disable"));

			$tpl->assign('pageContent', $this->_templatePath('smstan.prefspage.tpl'));
			$tpl->display('li/index.tpl');
			return(true);
		}
		return(false);
	}

	/*
	*  Anzeigen der Seiten
	*/
	function FileHandler($file, $action)
	{
		global $tpl, $db, $bm_prefs, $lang_user, $lang_custom, $prefsItems, $prefsImages, $prefsIcons, $groupRow;

		if($file=='prefs.php')
		{
			if(!$this->GetGroupOptionValue('smstan_plugin', $groupRow['id']))
				return(false);

			$prefsItems['smstan']		= true;
			$prefsImages['smstan']		= './plugins/templates/images/smstan.logo.48.png';
			$prefsIcons['smstan']		= './plugins/templates/images/smstan.logo.16.png';
		}

		if($file=='index.php' && $action=='smstan')
		{
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == "send")
			{
				if(!class_exists('BMSMS'))
					include(B1GMAIL_DIR . 'serverlib/sms.class.php');

				$email 		= (isset($_REQUEST['email_full']) ? $_REQUEST['email_full'] : $_REQUEST['email_local'] . '@' . $_REQUEST['email_domain']);
				$userID 	= BMUser::GetID($email);
				$ip			= $_SERVER['REMOTE_ADDR'];

				if($this->isIpBan($ip)) {
					$this->_badlogin($lang_user['smstan_banip']);
				}
				if(!$userID) {
					$this->_badlogin($lang_user['baduser']);
				} else if($this->CodeExist($userID)) {
					$this->_badlogin($lang_user['smstan_codeexist']);
				}

				$thisUser 	= _new('BMUser', array($userID));
				$userRow	= $thisUser->_row;
				$group		= $thisUser->GetGroup();
				$toNo		= $userRow['mail2sms_nummer'];

				if($toNo == "") {
					$this->_badlogin($lang_user['smstan_nosms_nummer']);
				}
				if($thisUser->GetPref("smstan_disable")) {
					$this->_badlogin($lang_user['smstan_grouperror']);
				}

				if(!$this->GetGroupOptionValue('smstan_plugin', $group->_id)) {
					$this->_badlogin($lang_user['smstan_grouperror']);
				}

				$smsTyp 	= $this->_getPref("smstype");
				$preis		= $this->GetPrice($smsTyp);
				if($this->_getPref("chargefromuser")) {
					$userMoney	= $thisUser->GetBalance();

					if($userMoney < $preis)
					{
						$this->_badlogin($lang_user['smstan_priceerror']);
					}
				}

				$sms 		= _new('BMSMS', array($userID, &$thisUser));
				$codeID 	= $this->RequestCode($userID);
				$code 		= $this->GetCode($codeID);

				$text		= $this->_parse($lang_custom['smstan_sms_text'], $userRow, $code);
				$fromNo		= $this->_getPref("fromno");

				$result = $sms->Send($fromNo, $toNo, $text, $this->_getPref("smstype"), $this->_getPref("chargefromuser"), false);
				PutLog('smsTAN Sent SMS Key <'.$code.'> to <'.$toNo.'>', PRIO_PLUGIN, __FILE__, __LINE__);
				if($result)
				{
					$this->RequestBan($ip);
					$this->LogRequest($userID, $ip, $preis);

					$tpl->assign('pageTitle', $lang_user['smstan']." ".$lang_user['login']);
					$tpl->assign('languageList', GetAvailableLanguages());
					$tpl->assign('page', $this->_templatePath('smstan.login2.tpl'));
					$tpl->display('nli/index.tpl');
					exit();
				} else {
					$this->_badlogin($lang_user['smssendfailed']);
				}
			} else if(isset($_REQUEST['do']) && $_REQUEST['do'] == "smslogin") {
				$code 		= $_REQUEST['code'];
				$userID 	= $this->GetUserFromCode($code);
				$ip			= $_SERVER['REMOTE_ADDR'];

				if($this->isIpBan($ip)) {
					$this->_badlogin($lang_user['smstan_banip']);
				}
				if(!$userID) {
					$this->RequestBan($ip);
					$this->_badlogin($lang_user['smstan_badcode']);
				}
				$codeID 	= $this->CodeExist($userID);
				if(!$codeID || ($this->GetCode($codeID) != $code)) {
					$this->RequestBan($ip);
					$this->_badlogin($lang_user['smstan_badcode']);
				}
				$this->ReleaseCode($codeID);

				$res = $db->Query('SELECT email, passwort FROM {pre}users Where id=? Limit 1', 
					(int) $userID);
				$codeuser = $res->FetchArray();
				$res->Free();

				list($result, $param) = BMUser::Login($codeuser['email'], $codeuser['passwort'], true, true, "", true);
				// login ok?
				if($result == USER_OK)
				{
					// stats
					Add2Stat('login');
					PutLog('smsTAN Login OK ID ('.$userID.')', PRIO_PLUGIN, __FILE__, __LINE__);
					// delete cookies
					setcookie('bm_savedUser', 		'', 			time() - TIME_ONE_HOUR);
					setcookie('bm_savedPassword', 	'', 			time() - TIME_ONE_HOUR);
					setcookie('bm_savedLanguage', 	'', 			time() - TIME_ONE_HOUR);
					setcookie('bm_savedSSL', 		'', 			time() - TIME_ONE_HOUR);
					// register language
					$_SESSION['bm_sessionLanguage'] = $bm_prefs['language'];

					header('Location: start.php?sid=' . $param);
					exit();
				} else {
					// sms validation input?
					if($result == USER_LOCKED && $requiresValidation)
					{
						$tpl->assign('email',		$email);
						$tpl->assign('password',	strlen($password) == 32 ? $password : md5($password));
						$tpl->assign('savelogin',	isset($_POST['savelogin']));
						$tpl->assign('language',	$language);
						$tpl->assign('page',		'nli/login.smsvalidation.tpl');
					} else {
						// tell user what happened
						switch($result)
						{
						case USER_BAD_PASSWORD:
							$tpl->assign('msg',	sprintf($lang_user['badlogin'], $param));
							break;
						case USER_DOES_NOT_EXIST:
							$tpl->assign('msg', $lang_user['baduser']);
							break;
						case USER_LOCKED:
							$tpl->assign('msg', $lang_user['userlocked']);
							break;
						case USER_LOGIN_BLOCK:
							$tpl->assign('msg', sprintf($lang_user['loginblocked'], FormatDate($param)));
							break;
						}
						$tpl->assign('page',	'nli/loginresult.tpl');
					}
					$tpl->assign('languageList', GetAvailableLanguages());
					$tpl->display('nli/index.tpl');
					exit();
				}
			} else {
				$pricetext;
				if($this->_getPref("chargefromuser")) {
					$pricetext		= $lang_user['smstan_price'];

					$smsTyp 	= $this->_getPref("smstype");
					$preis		= $this->GetPrice($smsTyp);

					$pricetext 		= str_replace('%%credits%%', $preis, $pricetext);
				} else {
					$pricetext		= $lang_user['smstan_free'];
				}

				$tpl->assign('pricetext',			$pricetext);
				$tpl->assign('pageTitle',			$lang_user['smstan']." ".$lang_user['login']);
				$tpl->assign('domain_combobox',		$bm_prefs['domain_combobox'] == 'yes');
				$tpl->assign('domainList', 			GetDomainList('login'));
				$tpl->assign('languageList', 		GetAvailableLanguages());
				$tpl->assign('page', 				$this->_templatePath('smstan.login.tpl'));
				$tpl->display('nli/index.tpl');
				exit();
			}
		}
	}

	function _badlogin($msg)
	{
		global $tpl, $lang_user;
		$tpl->assign('pageTitle', $lang_user['smstan']." ".$lang_user['login']);
		$tpl->assign('languageList', GetAvailableLanguages());
		$tpl->assign('msg', $msg);
		$tpl->assign('page', 'nli/loginresult.tpl');
		$tpl->display('nli/index.tpl');
		exit();
	}

	function RequestCode($userid)
	{
		global $db;
		
		$code = $this->CodeGen(8);
		
		$db->Query('INSERT INTO {pre}mod_smstan_keys(userid,code,time) VALUES(?,?,?)',
			$userid,
			$code,
			time()+15*60);
		
		return($db->InsertId());
	}

	function ReleaseCode($id)
	{
		global $db;

		$db->Query('UPDATE {pre}mod_smstan_keys SET code="" WHERE id=?',
			$id);
	}

	function CodeGen($chars)
	{
		$vocals = 'aeiouAEIOU1234567890';
		$cons = 'bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ1234567890';
		$result = '';
		
		for($i = 0; $i < $chars; $i++)
			if($i % 2 == 0)
				$result .= $vocals[ mt_rand(0, strlen($vocals)-1) ];
			else
				$result .= $cons[ mt_rand(0, strlen($cons)-1) ];
		
		return($result);
	}

	function GetCode($id)
	{
		global $db;
		
		$res = $db->Query('SELECT code FROM {pre}mod_smstan_keys WHERE id=? AND time>UNIX_TIMESTAMP()',	
			$id);
		$row = $res->FetchArray();
		$res->Free();
		return($row['code']);
	}
	
	function GetUserFromCode($code)
	{
		global $db;
		
		$res = $db->Query('SELECT userid FROM {pre}mod_smstan_keys WHERE code=? AND time>UNIX_TIMESTAMP()',	
			$code);
		$row = $res->FetchArray();
		$res->Free();
		return($row['userid']);
	}

	function CodeExist($userid)
	{
		global $db;

		$res = $db->Query('SELECT id, code, time FROM {pre}mod_smstan_keys WHERE userid=? AND time>UNIX_TIMESTAMP()',	
			$userid);
		if($res->RowCount() == 1)
		{
			$row = $res->FetchArray(MYSQLI_NUM);
			$res->Free();
			return($row[0]);
		} else {
			$res->Free();
			return(false);
		}
	}

	function _parse($text, $userRow, $code) {
        global $bm_prefs;
        $text = str_replace('%%user%%', $userRow['email'], $text);
        $text = str_replace('%%wddomain%%', str_replace('@', '.', $userRow['email']), $text);
        $text = str_replace('%%selfurl%%', $bm_prefs['selfurl'], $text);
        $text = str_replace('%%hostname%%', $bm_prefs['b1gmta_host'], $text);
        $text = str_replace('%%projecttitle%%', $bm_prefs['titel'], $text);
		$text = str_replace('%%code%%', $code, $text);
        return $text;
    }

	function GetSMSTypes()
	{
		global $db;

		$result = array();
		$res = $db->Query('SELECT id,titel FROM {pre}smstypen ORDER BY titel ASC');
		while($row = $res->FetchArray(MYSQLI_ASSOC))
			$result[$row['id']] = array(
				'id'		=> $row['id'],
				'title'		=> $row['titel'],
			);
		$res->Free();
		
		return($result);
	}

	function isIpBan($ip)
	{
		global $db;

		$res = $db->Query('SELECT COUNT(ip) AS cip FROM {pre}mod_smstan_banip WHERE ip=? AND time>UNIX_TIMESTAMP()',	
			$ip);
		$row = $res->FetchArray(MYSQLI_ASSOC);
		if($row['cip'] <= 5)
			return false;
			
		return true;
	}

	function RequestBan($ip)
	{
		global $db;
		
		$code = $this->CodeGen(8);
		
		$db->Query('INSERT INTO {pre}mod_smstan_banip(ip,time) VALUES(?,?)',
			$ip,
			time()+15*60);
		
		return($db->InsertId());
	}
	
	function GetPrice($type)
	{
		global $db;

		if($type==0) {
			$sql = $db->Query("SELECT price FROM {pre}smstypen WHERE std = ?", 
				1);
		} else {
			$sql = $db->Query("SELECT price FROM {pre}smstypen WHERE id = ?", 
				$type);
		}
		$row = $sql->FetchArray();
		$preis = $row['price'];
		$sql->Free();
		return $preis;
	}
	
	function LogRequest($userID, $ip, $preis)
	{
		global $db;

		$db->Query('INSERT INTO {pre}mod_smstan_log(userid,ip,time, credits) VALUES(?,?,?,?)',
			$userID,
			$ip,
			time(),
			$preis);
	}
	
	function GetSumPaidCredits($userID)
	{
		global $db;

		$res = $db->Query('SELECT SUM(credits) as paid FROM {pre}mod_smstan_log WHERE userid=?',	
			$userID);
		$row = $res->FetchArray(MYSQLI_ASSOC);
		$res->Free();
		return $row['paid'];
	}
}
/*
 * register plugin
 */
$plugins->registerPlugin('smstan_plugin');
?>
