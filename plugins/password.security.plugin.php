<?php
	class pwsecurity extends BMPlugin
	{
		function pwsecurity()
		{
			
			$this->name        = 'Passwort-Sicherheit';
			$this->author      = 'Martin Buchalik';
			$this->web         = 'http://martin-buchalik.de';
			$this->mail        = 'support@martin-buchalik.de';
			$this->version     = '1.0.1';
			$this->designedfor = '7.3.0';
			$this->type        = BMPLUGIN_DEFAULT;
			
			$this->admin_pages = false;
		}
		
		
		/* ===== Installation ===== */
		
		function Install()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		
		/* ===== Uninstall ===== */
		
		function Uninstall()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
		{
			if ($lang == 'deutsch') {				
				$lang_user['pwsecurity_smallletters']         = 'Kleinbuchstaben';
				$lang_user['pwsecurity_capitalletters']       = 'Großbuchstaben';
				$lang_user['pwsecurity_digits']        		  = 'Zahlen';
				$lang_user['pwsecurity_specialchars']         = 'Sonderzeichen';
			} else {
				$lang_user['pwsecurity_smallletters']         = 'small letters';
				$lang_user['pwsecurity_capitalletters']       = 'capital letters';
				$lang_user['pwsecurity_digits']        		  = 'numbers';
				$lang_user['pwsecurity_specialchars']         = 'special characters';
			}
		}
		
		
		/* ===== Explaination ===== */
		
		function BeforeDisplayTemplate($resourceName, &$tpl)
		{	
			if($_REQUEST['action'] == 'signup') {
				$tpl->addJSFile("nli", "./clientlib/jquery/jquery-1.8.2.min.js");
				$tpl->addJSFile("nli", "./plugins/js/password.security.js");
				$tpl->addCSSFile("nli", "./plugins/css/password-security.css");
				$tpl->registerHook("password.security:steps", $this->_templatePath('../../plugins/templates/password.security.tpl'));
			}
		}
	}
	
	/* ===== register plugin ===== */
	$plugins->registerPlugin('pwsecurity');
?>