<?php 
/*
 * 
 * (c) IT- und Webl�sungen Kretschmar - www.iscgr.de
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: calenderbridge.plugin.php,v 1.0 2011/11/27 17:30:00 Informant $
 *
 * Anpassung der Datei /b1gmail/kalender/system/apps/core/controllers/login.php -> ganz unten die funktion �ndern in:
 *
 *     public function hashPassword($username, $password) {
 *		//$salt = 'xQ._0_2' . md5($username[0] . $password . $username[(strlen($username) - 1)]);
 *		//return hash('sha256', $salt . $password);
 *		return md5($password);
 *    }
 */

class calenderbridge extends BMPlugin 
{
	function calenderbridge()
	{
		global $lang_user;
		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'calenderbridge';
		$this->author			= 'Informant';
		$this->version			= '1.0';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
		}
		else
		{
		}
	}

	function Install()
	{
		PutLog('Calenderbridge Plugin was successfull installed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		PutLog('Calenderbridge Plugin was successfull removed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function OnSignup($userid, $usermail)
	{
		global $db;
		
		$res = $db->Query('SELECT passwort FROM {pre}users WHERE id=? LIMIT 1', $userid);
        $row = $res->FetchArray();
        $pw = $row['passwort'];
        $res->free();
		
		$username = explode("@", $usermail);

		$db->Query("INSERT INTO kalender.spc_users(activated, admin_id, role, username, password, email, timezone, language, theme) VALUES(?,?,?,?,?,?,?,?,?)", 1, '1-', 'user', $username[0], $pw, $usermail, 'Europe/Berlin', 'de', 'Aristo2');
	}
	
	function OnUserPasswordChange($userID, $oldPasswordMD5, $newPasswordMD5, $newPasswordPlain)
	{
		global $db;
		
		$db->Query('UPDATE kalender.spc_users SET password=? WHERE password=?', $newPasswordMD5, $oldPasswordMD5);
	}
	
	function OnDeleteUser($userID)
	{
		global $db;
		
		$res = $db->Query('SELECT email FROM {pre}users WHERE id=? LIMIT 1', $userID);
        $row = $res->FetchArray();
        $email = $row['email'];
        $res->free();
		
		$username = explode("@", $email);
		
		$db->Query('DELETE FROM kalender.spc_users WHERE username=?', $username[0]);
	}
	
	function FileHandler($file, $action)
	{
		global $db;
		/*
		if($file == 'index.php' && $action == 'kal-import')
		{
			$i = 1;
			while($i <= 414)
			{
				$pw = '';
				$res = $db->Query('SELECT passwort, email FROM {pre}users WHERE id=?', $i);
				$row = $res->FetchArray();
				$pw = $row['passwort'];
				$email = $row['email'];
				$res->free();
				
				$username = explode("@", $email);
				if(!empty($pw))
				{
					$db->Query("INSERT INTO kalender.spc_users(activated, admin_id, role, username, password, email, timezone, language, theme) VALUES(?,?,?,?,?,?,?,?,?)", 1, '1-', 'user', $username[0], $pw, $email, 'Europe/Berlin', 'de', 'Aristo2');
					echo "<br>username: ".$username[0]." pw: ".$pw;
				}
				$i++;
			}
		}
		*/
	}
}

$plugins->registerPlugin('calenderbridge');

?>
