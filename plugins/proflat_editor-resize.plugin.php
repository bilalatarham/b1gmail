<?php
	class proflateditorresize extends BMPlugin
	{
		function proflateditorresize()
		{
			
			$this->name        = 'Pro-Flat: Editor-Fenster maximierbar';
			$this->author      = 'Martin Buchalik';
			$this->web         = 'http://martin-buchalik.de';
			$this->mail        = 'support@martin-buchalik.de';
			$this->version     = '1.0.0';
			$this->designedfor = '7.3.0';
			$this->type        = BMPLUGIN_DEFAULT;
			
			$this->admin_pages = false;
		}
		
		
		/* ===== Installation ===== */
		
		function Install()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		
		/* ===== Uninstall ===== */
		
		function Uninstall()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		
		/* ===== Add our js file ===== */
		
		function BeforeDisplayTemplate($resourceName, &$tpl)
		{
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == "compose") {
				$tpl->addJSFile("li", "./plugins/js/proflat_editor-resize.js");
			}
		}
	}
	
	/* ===== register plugin ===== */
	$plugins->registerPlugin('proflateditorresize');
?>