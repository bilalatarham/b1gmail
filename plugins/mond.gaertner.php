<?php

/**

 * Plugin: Der kleine Mondg�rtner

 *

 */

class MondGaertner extends BMPlugin 

{

   function MondGaertner()

   {

      $this->name             = 'Der kleine Mondg&auml;rtner';

      $this->author           = 'Markus Albert';

      $this->web              = 'http://www.maalbert.de';

      $this->mail             = 'software@maalbert.de';

      $this->version          = '2.0.1';

      $this->designedfor         = '7.0.0';

      $this->type             = BMPLUGIN_WIDGET;

      

      $this->widgetTitle         = 'Der kleine Mondg&auml;rtner';

      $this->widgetTemplate      = 'widget.Mond.Gaertner.tpl';

   }

   

   function isWidgetSuitable($for)

   {

      return($for == BMWIDGET_START);

   }

   

   function renderWidget()

   {

      global $tpl; 

      
//Ausgabe aktuelle Zeit 
      $tpl->assign('Mond_Tag', date("d.m.Y"));
	  $tpl->assign('Mond_Uhr', date("H:i"));	
	  $startjahr = 1999;
//Startdatum und Vollmondz�hlung		
		$startdatum= mktime(3, 49, 0, 1, 02, $startjahr);  // Startdatum  02.01.1999 3:49
		$heute= time ();                          // Heute
		$diff= $heute - $startdatum;
		$anzahl_tage = round ($diff / 86400);
		
		$anzahl_vollmonde = $anzahl_tage / 29.530588; //29.530588 Tage von Vollmond zu Vollmond
		
//Mondphasenberechnung
		$mondphase = $anzahl_vollmonde - floor($anzahl_vollmonde);
		
//Grobe Mondphasenauswertung		
		if($mondphase < 0.25)
    		{
    			$tpl->assign('Uberschrift_1', 'Richtung abnehmender Halbmond');
    			$tpl->assign('Ausgabe_1', '
								<LI>Wie w&auml;re es mit etwas D&uuml;nger f&uuml;r Ihre Pflanzen?</LI>
    							<LI>Lange Lagerzeiten sind zu erwarten, wenn Sie nun Gem&uuml;se ernten.</LI>
    							<LI>Pflegen Sie Ihre Pflanzen, Krankheiten lassen sich zur Zeit besonders gut bek&auml;mpfen.</LI>
							');
    		
    		}
		elseif($mondphase < 0.5)
			{
				$tpl->assign('Uberschrift_1', 'Neumond');
				$tpl->assign('Ausgabe_1', '
									<LI>F&uuml;r Rasen m&auml;hen und nachs&auml;en ist momentan eine g&uuml;nstige Zeitspanne</LI>
									<LI>Vergessen Sie nicht zu D&uuml;ngen, die optimale zeit ist bald vorbei.</LI>
									<LI>Lange Lagerzeiten sind zu erwarten, wenn Sie nun Gem&uuml;se ernten.</LI>
									<LI>Pflegen Sie Ihre Pflanzen, Krankheiten lassen sich zur Zeit besonders gut bek&auml;mpfen.</LI>
									<LI>Ihre Hecke k&ouml;nnte mal wieder einen Schnitt vertragen! Oder sind Sie anderer Meinung?</LI>
						');
			}						
		elseif($mondphase < 0.75)
			{
				$tpl->assign('Uberschrift_1', 'Richtung zunehmender Halbmond');
				$tpl->assign('Ausgabe_1', '
									<LI>F&uuml;r Rasen m&auml;hen und nachs&auml;en ist momentan eine g&uuml;nstige Zeitspanne</LI>
									<LI>Eine Gute Zeit hat begonnen um Blatt-, Bl&uuml;h-, und Fruchtgem&uuml;se zu s&auml;en oder zu pflanzen</LI>
							');
			}
		elseif($mondphase >= 0.75)
			{
				$tpl->assign('Uberschrift_1', 'Richtung Vollmond');
				$tpl->assign('Ausgabe_1', '
									<LI>F&uuml;r Rasen m&auml;hen und nachs&auml;en ist momentan eine g&uuml;nstige Zeitspanne</LI>
									<LI>Die Gute Zeit neigt sich dem Ende um Blatt-, Bl&uuml;h-, und Fruchtgem&uuml;se zu s&auml;en oder zu pflanzen</LI>
						');
			}
		else {
			$tpl->assign('Ausgabe_1', '<LI>Fehler in der Berechnung</LI>');
		}
//Mondphasen Tipps
	$tpl->assign('Uberschrift_2', 'Besondere Tipps:');
	if($mondphase < 0.05 or $mondphase > 0.95 ) //Vollmondphase
    		{
    			$tpl->assign('Ausgabe_2', '
										<LI>Wir sind mitten in der Vollmondphase, nicht s&auml;en oder pflanzen! </LI>
										<LI>Bitte keine Geh&ouml;lze schneiden.</LI>
										<LI>Aber genau jetzt ist D&uuml;nger besonders effektiv.</LI>				
				');
    		}
	elseif($mondphase > 0.05 and $mondphase < 0.1)	//Kurz nach Vollmond
			{
				$tpl->assign('Ausgabe_2', '
										<LI>Schon Kartoffeln gesetzt? Es w&auml;re gerade perfekt!</LI>
										<LI>Blattgem&uuml;&szlig;e k&ouml;nnen Sie heute mit gutem Gewissen s&auml;en oder pflanzen.</LI>
										<LI>Es w&auml;re auch ein sehr guter Zeitpunkt um Zimmerpflanzen umzutopfen.</LI>				
				');
			}							
	elseif($mondphase > 0.45 and $mondphase < 0.5) //Kurz vor Neumond
			{
				$tpl->assign('Ausgabe_2', '
										<LI>Besuchen Sie doch mal Ihren Kr&auml;utergarten und ernten Wurzelkr&auml;uter.</LI>
										<LI>Diese haben dann eine besondere Wirkung, wenn man sie f&uuml;r medizinische Zwecke einsetzt.</LI>
										<LI>Auch der Geschmack ist intensiver.</LI>
										<LI>Genau jetzt sollten Sie Obstb&auml;ume und Rosen veredeln.<LI>
				
				');
			}		
	elseif($mondphase > 0.48 and $mondphase < 0.55)	//Neumond
			{
				$tpl->assign('Ausgabe_2', '
										<LI>Bitte nichts s&auml;en oder pflanzen.</LI>
										<LI>Kranke B&auml;ume heute geschnitten, werden wunderbar genesen.</LI>
										<LI>Machen Sie Heute dem Unkraut den Garaus und es wird nicht so schnell wiederkommen.</LI>
				');
			}
	elseif($mondphase > 0.9 )	//Kurz vor Vollmond
			{
				$tpl->assign('Ausgabe_2', '
										<LI>Ernten Sie heute Kr&auml;uter um von ihrer starken Wirkung zu profitieren!</LI>				
				');
				if (date("n") == 12 and date("w")<=24){
					$tpl->assign('Ausgabe_1', 'Den Weihnachtsbaum, den Sie heute frisch schlagen wird Ihnen lange freude bereiten.');
				}
			}
	else{
		$tpl->assign('Ausgabe_2', 'Momentan keine');
	}	
   }

}

 

/**

 * register plugin

 */

$plugins->registerPlugin('MondGaertner');
?>