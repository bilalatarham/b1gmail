<?php

/**

 * Plugin: RSS-Reader

 *

 */

class rssr extends BMPlugin 

{

   function rssr()

   {

      $this->name             = 'RSS-Reader';

      $this->author           = 'Pilleslife';

      $this->web              = 'http://www.pilleslife.de';

      $this->mail             = 'webmaster@pilleslife.de';

      $this->version          = '1.0';

      $this->designedfor         = '7.0.0';

      $this->type             = BMPLUGIN_WIDGET;

      

      $this->widgetTitle         = 'RSS-Reader';

      $this->widgetTemplate      = 'rssr.widget.tpl';
	  
	  $this->update_url = 'http://my.b1gmail.com/update_service/';  

   }

   

   function isWidgetSuitable($for)

   {

      return($for == BMWIDGET_START);

   }

 	function Install()
	{
		global $db;
		
		$db->Query("CREATE TABLE `{pre}rssr` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`owner` INT NOT NULL ,`link` VARCHAR( 255 ) NOT NULL ,`titel` VARCHAR( 255 ) NOT NULL ,`show` INT NOT NULL) ENGINE = MYISAM ;");
		
		return(true);
		
	}
		
	function Uninstall()
	{
		global $db;
		
		// drop prefs table
		$db->Query('DROP TABLE {pre}rssr');
		
		return(true);
	}
	
		function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)

   {

      $lang_user['rssr'] = 'RSS-Reader';

      $lang_user['prefs_d_rssr'] = 'Hier k&ouml;nnen sie die Einstellungen f&uuml;r das RSS-Reader Widget vornehmen.';

   }
   
   function FileHandler($file, $action)

   {

      if($file=='prefs.php')

      {

         $GLOBALS['prefsItems']['rssr'] = true;

         $GLOBALS['prefsImages']['rssr'] = 'plugins/templates/images/rssr_icon48.png';

         $GLOBALS['prefsIcons']['rssr'] = 'plugins/templates/images/rssr_icon16.png';

      }

   }
   
   
  function UserPrefsPageHandler($action)

   {

      global $tpl, $db, $userRow;

      
      if($action != 'rssr')
					{
			         return(false);
					}	
			
			$err="";
			
	if(isset($_GET["do"]))
		{
			if($_GET["do"]=="save")
				{
					if(!isset($_POST["feedlink"]))
						{
						$err.="Leider ist kein Link zum Feed gesetzt.<br />";
						}
					else
						{
							$rss=@simplexml_load_file($_POST["feedlink"]);
							if(!$rss)
								{
								$err .="Konnte das RSS Feed nicht laden.<br />";	
								}
								
								
							if($_POST["feedtitel"]=="")
								{
									$rss=simplexml_load_file($_POST["feedlink"]);
									$feedtitel=$rss->channel[0]->title;
								}
							else
								{
										$feedtitel=$_POST["feedtitel"];
								}
			
								
						}
						
						if(!$_POST["show"]>0)
							{
								$err .="Sie haben leider nicht, oder falsch, angegeben, wie viele Eintr&auml;ge angezeigt werden sollen.<br />";
							}
						
				
				
					if($err=="")
						{
							$db->Query("DELETE FROM `{pre}rssr` WHERE `{pre}rssr`.`owner` = ? LIMIT 1",$userRow["id"]);
							$db->Query("INSERT INTO `{pre}rssr` (`owner` , `link` , `titel` , `show` ) VALUES (?, ?, ?, ?)",$userRow["id"],$_POST["feedlink"],$feedtitel,$_POST["show"]);
							$err="Eintrag erfolgreich hinzugef&uuml;gt, bzw. ge&auml;ndert.";
						}
				}
		}	  
					
			
			
		$feeds = array();
		$res = $db->Query("SELECT * FROM `{pre}rssr` WHERE `owner` =? LIMIT 1",$userRow["id"]);
		while($row = $res->FetchArray(MYSQL_ASSOC))
		{
			$feeds[] = $row;
		}
		$res->Free();
		
		$tpl->assign('feeds', $feeds);		
			
			$tpl->assign('err', $err);	
			$tpl->assign('pageURL', $_SERVER['PHP_SELF']);
      $tpl->assign('pageContent', $this->_templatePath('rssr.prefspage.tpl'));

      $tpl->display('li/index.tpl');

      return(true);
      
    }
	
	
function renderWidget()

   {

      global $tpl,$db,$userRow;

				$res=$db->Query("SELECT * FROM `{pre}rssr` WHERE `owner` =?",$userRow["id"]);
				$rssr="";
				while($row = $res->FetchArray(MYSQL_ASSOC))
					{
					$eintrag["titel"]= $row["titel"];
					$rss=simplexml_load_file($row["link"]);
					
						for($z=0;$z<=$row["show"];$z++)
							{
							/*$rssr[$z]["titel"]=utf8_decode($rss->channel[0]->item[$z]->title);
							$rssr[$z]["description"]=utf8_decode(htmlspecialchars($rss->channel[0]->item[$z]->description));
							$rssr[$z]["link"]=utf8_decode($rss->channel[0]->item[$z]->link);*/
							$rssr[$z]["titel"]=$rss->channel[0]->item[$z]->title;
							$rssr[$z]["description"]=htmlspecialchars($rss->channel[0]->item[$z]->description);
							$rssr[$z]["link"]=$rss->channel[0]->item[$z]->link;
							$rssr[$z]["id"]=$z;
							}
					}
				$res->Free();
        



      $tpl->assign('rssr', $rssr);

   }
	
	
}

$plugins->registerPlugin('rssr');
?>
