<?php
/*
 * b1gMail news plugin
 * (c) 2002-2008 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: news.plugin.php,v 1.2 2008/10/30 21:05:13 patrick Exp $
 *
 */

/**
 * News plugin
 *
 */
class NewsPlugin extends BMPlugin 
{
	function NewsPlugin()
	{
		// extract version
		sscanf('$Revision: 1.2 $', chr(36) . 'Revision: %d.%d ' . chr(36),
			$vMajor,
			$vMinor);
		
		// plugin info
		$this->type					= BMPLUGIN_DEFAULT;
		$this->name					= 'News';
		$this->author				= 'Patrick Schlangen';
		$this->web					= 'http://www.b1g.de/';
		$this->mail					= 'ps@b1g.de';
		$this->version				= sprintf('%d.%d', $vMajor, $vMinor);
		
		$this->admin_pages 			= true;
		$this->admin_page_title 	= 'News';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			$lang_admin['news_news']				= 'News';
			$lang_admin['news_addnews']				= 'News hinzuf&uuml;gen';
			
			$lang_user['news_news']					= 'News';
			$lang_user['news_nonews']				= 'Es konnten keine News gefunden werden.';
			$lang_user['news_text']					= 'Hier finden Sie aktuelle Informationen und Ank&uuml;ndigungen.';
		}
		else 
		{
			$lang_admin['news_news']				= 'News';
			$lang_admin['news_addnews']				= 'Add news';
			
			$lang_user['news_news']					= 'News';
			$lang_user['news_nonews']				= 'Could not find any news.';
			$lang_user['news_text']					= 'Here you can find current information and announcements regarding our service.';
		}
	}
	
	function Install()
	{
		global $db;
		
		// db struct
		$databaseStructure = 
			  'YToxOntzOjk6ImJtNjBfbmV3cyI7YToyOntzOjY6ImZpZWxkcyI7YTo2OntpOjA7YTo2OntpOjA'
			. '7czo2OiJuZXdzaWQiO2k6MTtzOjc6ImludCgxMSkiO2k6MjtzOjI6Ik5PIjtpOjM7czozOiJQUk'
			. 'kiO2k6NDtOO2k6NTtzOjE0OiJhdXRvX2luY3JlbWVudCI7fWk6MTthOjY6e2k6MDtzOjQ6ImRhd'
			. 'GUiO2k6MTtzOjc6ImludCgxMSkiO2k6MjtzOjI6Ik5PIjtpOjM7czowOiIiO2k6NDtzOjE6IjAi'
			. 'O2k6NTtzOjA6IiI7fWk6MjthOjY6e2k6MDtzOjU6InRpdGxlIjtpOjE7czoxMjoidmFyY2hhcig'
			. 'xMjgpIjtpOjI7czoyOiJOTyI7aTozO3M6MDoiIjtpOjQ7czowOiIiO2k6NTtzOjA6IiI7fWk6Mz'
			. 'thOjY6e2k6MDtzOjQ6InRleHQiO2k6MTtzOjQ6InRleHQiO2k6MjtzOjI6Ik5PIjtpOjM7czowO'
			. 'iIiO2k6NDtzOjA6IiI7aTo1O3M6MDoiIjt9aTo0O2E6Njp7aTowO3M6ODoibG9nZ2VkaW4iO2k6'
			. 'MTtzOjIzOiJlbnVtKCdsaScsJ25saScsJ2JvdGgnKSI7aToyO3M6MjoiTk8iO2k6MztzOjA6IiI'
			. '7aTo0O3M6NDoiYm90aCI7aTo1O3M6MDoiIjt9aTo1O2E6Njp7aTowO3M6NjoiZ3JvdXBzIjtpOj'
			. 'E7czoxMToidmFyY2hhcig2NCkiO2k6MjtzOjI6Ik5PIjtpOjM7czowOiIiO2k6NDtzOjE6IioiO'
			. '2k6NTtzOjA6IiI7fX1zOjc6ImluZGV4ZXMiO2E6MTp7czo3OiJQUklNQVJZIjthOjE6e2k6MDtz'
			. 'OjY6Im5ld3NpZCI7fX19fQ==';
		$databaseStructure = unserialize(base64_decode($databaseStructure));
		
		// sync struct
		SyncDBStruct($databaseStructure);
		
		// log
		PutLog(sprintf('%s v%s installed',
			$this->name,
			$this->version),
			PRIO_PLUGIN,
			__FILE__,
			__LINE__);
		
		return(true);
	}
	
	function AdminHandler()
	{
		global $tpl, $bm_prefs, $lang_admin, $db;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'news';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['news_news'],
				'link'		=> $this->_adminLink() . '&',
				'active'	=> $_REQUEST['action'] == 'news'
			)
		);
		
		$tpl->assign('tabs', $tabs);
		$tpl->assign('pageURL', $this->_adminLink());
		
		if($_REQUEST['action'] == 'news')
		{ 
			//
			// overview (+ add, delete)
			//
			if(!isset($_REQUEST['do']))
			{
				if(isset($_REQUEST['add']))
				{
					if(isset($_REQUEST['all_groups']))
						$groups = '*';
					else 
						$groups = implode(',', is_array($_REQUEST['groups']) ? $_REQUEST['groups'] : array());
					$db->Query('INSERT INTO {pre}news(`title`,`date`,`loggedin`,`groups`,`text`) VALUES(?,?,?,?,?)',
						$_REQUEST['title'],
						time(),
						$_REQUEST['loggedin'],
						$groups,
						$_REQUEST['text']);
				}
				
				else if(isset($_REQUEST['delete']))
				{
					$db->Query('DELETE FROM {pre}news WHERE `newsid`=?',
						(int)$_REQUEST['delete']);
				}
				
				$news = array();
				$res = $db->Query('SELECT `newsid`,`title`,`date`,`loggedin` FROM {pre}news ORDER BY `newsid` DESC');
				while($row = $res->FetchArray(MYSQLI_ASSOC))
					$news[$row['newsid']] = $row;
				$res->Free();
				
				$tpl->assign('usertpldir', B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
				$tpl->assign('groups', BMGroup::GetSimpleGroupList());
				$tpl->assign('news', $news);
				$tpl->assign('page', $this->_templatePath('news.admin.tpl'));
			}
			
			//
			// edit
			//
			else if($_REQUEST['do'] == 'edit'
				&& isset($_REQUEST['id']))
			{
				if(isset($_REQUEST['save']))
				{
					if(isset($_REQUEST['all_groups']))
						$groups = '*';
					else 
						$groups = implode(',', is_array($_REQUEST['groups']) ? $_REQUEST['groups'] : array());
					$db->Query('UPDATE {pre}news SET `title`=?,`loggedin`=?,`groups`=?,`text`=? WHERE `newsid`=?',
						$_REQUEST['title'],
						$_REQUEST['loggedin'],
						$groups,
						$_REQUEST['text'],
						(int)$_REQUEST['id']);
					header('Location: ' . $this->_adminLink() . '&sid=' . session_id());
					exit();
				}
				
				// fetch news
				$news = array();
				$res = $db->Query('SELECT `newsid`,`title`,`text`,`loggedin`,`groups` FROM {pre}news WHERE `newsid`=?',
					(int)$_REQUEST['id']);
				if($res->RowCount() != 1)
					exit();
				$news = $res->FetchArray();;
				$res->Free();
				
				// process groups
				$groups = BMGroup::GetSimpleGroupList();
				if($news['groups'] != '*')
				{
					$newsGroups = explode(',', $news['groups']);
					
					foreach($groups as $key=>$val)
					{
						if(in_array($val['id'], $newsGroups))
							$groups[$key]['checked'] = true;
					}
				}
				
				$tpl->assign('usertpldir', B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
				$tpl->assign('groups', $groups);
				$tpl->assign('news', $news);
				$tpl->assign('page', $this->_templatePath('news.admin.edit.tpl'));
			}
		}
	}
	
	function FileHandler($file, $action)
	{
		global $tpl, $groupRow;
		
		if($file == 'index.php' && $action == 'newsPlugin')
		{
			$news = $this->_getNews(false);
			
			$tpl->assign('news', $news);
			$tpl->assign('page', $this->_templatePath('news.notloggedin.tpl'));
			$tpl->display('nli/index.tpl');
			
			exit();
		}
		else if($file == 'start.php' && $action == 'newsPlugin')
		{
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'showNews' && isset($_REQUEST['id']))
			{
				$news = $this->_getNews(true, $groupRow['id']);
				
				if(isset($news[$_REQUEST['id']]))
				{
					$tpl->assign('news', $news[$_REQUEST['id']]);
					$tpl->display($this->_templatePath('news.show.tpl'));
					exit();
				}
			}
		}
	}
	
	function getUserPages($loggedin)
	{
		global $lang_user;
		
		if($loggedin)
			return(array());
		
		if(count($this->_getNews(false)) < 1)
			return(array());
		
		return(array(
			'news' => array(
				'text'	=> $lang_user['news_news'],
				'link'	=> 'index.php?action=newsPlugin'
			)
		));
	}
	
	function _getNews($loggedin, $groupID = 0, $sortField = 'date', $sortDirection = 'DESC')
	{
		global $db;
		
		$result = array();
		$res = $db->Query('SELECT `newsid`,`date`,`title`,`text` FROM {pre}news WHERE (`loggedin`=? OR `loggedin`=?) AND (`loggedin`=? OR `groups`=? OR `groups`=? OR `groups` LIKE ? OR `groups` LIKE ? OR `groups` LIKE ?) ORDER BY `' . $sortField . '` ' . $sortDirection,
			$loggedin ? 'li' : 'nli',
			'both',
			'nli',
			'*',
			$groupID,
			$groupID . ',%',
			'%,' . $groupID . ',%',
			'%,' . $groupID);
		while($row = $res->FetchArray(MYSQLI_ASSOC))
		{
			$result[$row['newsid']] = $row;
		}
		$res->Free();
		
		return($result);
	}
}

/**
 * News widget
 * 
 */
class NewsWidget extends BMPlugin 
{
	function NewsWidget()
	{
		$this->type				= BMPLUGIN_WIDGET;
		$this->name				= 'News widget';
		$this->author			= 'Patrick Schlangen';
		$this->web				= 'http://www.b1g.de/';
		$this->mail				= 'ps@b1g.de';
		$this->widgetTemplate	= 'widget.news.tpl';
		$this->widgetTitle		= 'News';
	}
	
	function isWidgetSuitable($for)
	{
		return($for == BMWIDGET_START
				|| $for == BMWIDGET_ORGANIZER);
	}
	
	function renderWidget()
	{
		global $groupRow, $tpl;
		$tpl->assign('bmwidget_news_news', NewsPlugin::_getNews(true, $groupRow['id']));
		return(true);
	}
}

/**
 * register plugin + widget
 */
$plugins->registerPlugin('NewsPlugin');
$plugins->registerPlugin('NewsWidget');
?>
