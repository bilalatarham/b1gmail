<?php 
/*
 * 
 * (c) Speedloc Datacenter ::: www.speedloc.de
 * 
 * Redistribution of this code without explicit permission is forbidden!
 *
 * $Id: postfach.plugin.php, v2.1 2015/05/09 18:30:00 Informant $
 *
 */

class postfachorder extends BMPlugin 
{	
	function postfachorder()
	{	
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Postfach-Order';
		$this->author			= 'Informant';
		$this->version			= '2.1';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		global $currentCharset;
       
		if($lang == 'deutsch')
		{
			$lang_user['pf_pac_uebersicht']		= 'Paket�bersicht und Preise';
			$lang_user['pacc_accunit']			= 'Abrechnungseinheit';
			$lang_user['pacc_jede']				= 'jede';
			$lang_user['pacc_woche']			= 'Woche';
			$lang_user['pacc_jeden']			= 'jeden';
			$lang_user['pacc_monat']			= 'Monat';
			$lang_user['pacc_jedes']			= 'jedes';
			$lang_user['pacc_jahr']				= 'Jahr';
			$lang_user['pacc_alle']				= 'alle';
			$lang_user['pacc_wochen']			= 'Wochen';
			$lang_user['pacc_monate']			= 'Monate';
			$lang_user['pacc_jahre']			= 'Jahre';
			$lang_user['pacc_einmalig']			= 'einmalig';
			$lang_user['pf_mail_betreff']		= 'Ihr Tarifwechsel';
			$lang_user['pf_mail_betreff_b']		= 'Tarifwechsel des Benutzers: ';
			$lang_user['pf_ueberschrift']		= 'Tarifwechsel via Postfach';
			$lang_user['pf_ueberschrift2']		= 'Tarifwechsel mit Bitcoin';
			$lang_user['pf_laufzeit']			= 'Welche Laufzeit w�nschen Sie?';
			$lang_user['pf_order']				= 'Paket unverbindlich bestellen!';
			$lang_user['pf_paket']				= 'In welches Paket m�chten Sie wechseln?';
			$lang_user['pf_send_ok']			= 'Vielen Dank f�r Ihren Auftrag. Sobald das Geld in unserem Postfach eingegangen ist, werden wir Ihren Tarifwechsel durchf�hren. Informationen zu unserem Postfach erhalten Sie in K�rze per E-Mail.';
			$lang_custom['pf_mailTXT']			= 'Sehr geehrte Damen und Herren,' . "\n\n"
												. 'wir Danken Ihnen f�r die Bestellung des Premium-Paketes.' . "\n\n"
                                                . 'Damit der Tarifwechsel abgeschlossen werden kann, senden Sie uns bitte die daf�r notwendige Geb�hr in H�he von %%amount%% Euro mit dem Betreff %%vkcode%% an folgendes Postfach:' . "\n\n"
												. 'Testname Testvorname' . "\n"
												. 'Testpostfach' . "\n"
												. 'Testplz Testort' . "\n\n"
												. '(Diese E-Mail wurde automatisch erstellt!)';
			$lang_custom['pf_mailTXT_bc']		= 'Sehr geehrte Damen und Herren,' . "\n\n"
												. 'wir Danken Ihnen f�r die Bestellung des Premium-Paketes.' . "\n\n"
                                                . 'Damit der Tarifwechsel abgeschlossen werden kann, senden Sie uns bitte die daf�r notwendige Geb�hr in H�he von %%coins%% Bitcoins an folgendes Bitcoin-Konto:' . "\n\n"
												. '%%bcacc%%' . "\n\n"
												. '(Diese E-Mail wurde automatisch erstellt!)';
		}
		else
		{
			// englisch bei bedarf erweitern
		}
		
		// betreff + mail-text ��� in utf-8 / iso umwandeln
        if(function_exists('CharsetDecode') && !in_array(strtolower($currentCharset), array('iso-8859-1', 'iso-8859-15')))
        {
            foreach($lang_custom as $key => $val)
                if(substr($key, 0, 3) == 'pf_')
                    $lang_custom[$key] = CharsetDecode($val, 'iso-8859-15');
            foreach($lang_user as $key=>$val)
                if(substr($key, 0, 3) == 'pf_')
                    $lang_user[$key] = CharsetDecode($val, 'iso-8859-15');
        }
	}
	
	function FileHandler($file, $action)
	{
		global $db, $tpl, $thisUser, $userRow, $lang_custom, $lang_user, $bm_prefs;
		
		if($file == 'prefs.php' && $action == 'postfachorder')
        {			
			// order
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'order')
			{
				$pac = trim($_REQUEST['pac']);
				$lz = trim($_REQUEST['lz']);
								
				if($pac == '' OR $lz == '')
					exit('No Hacking please!');
				
				if(!class_exists('BMPayment'))
					include(B1GMAIL_DIR . 'serverlib/payment.class.php');
				
				$coinStatus = 0;
				// wird bitcoin genutzt?
				if(isset($_REQUEST['bitcoin']) && $_REQUEST['bitcoin'] == 1)
				{
					$coinStatus = 1;
				}

				// get pac-id 
				$res = $db->Query('SELECT id FROM {pre}mod_premium_packages WHERE titel=? AND geloescht=0', $pac);
				$row = $res->FetchArray();
                $pacID = $row['id'];
        		$res->Free();
				
				// get package
				$res = $db->Query('SELECT id,titel,beschreibung,abrechnung,abrechnung_t,preis_cent FROM {pre}mod_premium_packages WHERE id=? AND geloescht=0', $pacID);
				if($res->RowCount() == 1)
				{
					$package = $res->FetchArray(MYSQL_ASSOC);
					$res->Free();
				}
				else 
					die('Invalid package');
				
				// check runtime
				if($package['abrechnung'] == 'einmalig')
				{
					$abrechnung_t = -1;
					$amount = $package['preis_cent'];
				}
				else 
				{
					$abrechnung_t = (int)$lz;
					if($abrechnung_t < 1 || $abrechnung_t % $package['abrechnung_t'] != 0)
						$invalidFields[] = 'abrechnung_t';
					else 
						$amount = ($abrechnung_t/$package['abrechnung_t']) * $package['preis_cent'];
				}
							
				// wenn bitcoins genutzt wird, coin-wert nutzen
				if($coinStatus == 1)
				{
					$bitcoins = $this->getBitcoins($amount);
				}
				
				// cart
				$cart = array();
				$cart[] = array(
					'key'		=> 'PAcc.order.' . $pacID,
					'count'		=> ($package['preis_cent'] == 0 ? 0 : $amount/$package['preis_cent']),
					'amount'	=> $package['preis_cent'],
					'total'		=> $amount,
					'text'		=> $package['titel']
									. ' (' . $lang_user['pacc_accunit'] . ': '
									. $this->_intervalStr($package['abrechnung'], $package['abrechnung_t'], true)
									. ')'
				);
				
				$vkCode = BMPayment::GenerateVKCode();
				
				$db->Query('INSERT INTO {pre}orders(userid,vkcode,cart,paymethod,amount,inv_firstname,inv_lastname,inv_street,inv_no,inv_zip,inv_city,inv_country,created,activated,status) 
							VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
							$thisUser->_id, $vkCode, serialize($cart), 1, $amount, $userRow['vorname'], $userRow['nachname'], $userRow['strasse'], $userRow['hnr'], $userRow['plz'], $userRow['ort'], $userRow['land'], time(), 0, 0);
				
				// 2 Kommastellen
				$amount = number_format($amount/100, 2, ',', '.');

				$betreff = $lang_user['pf_mail_betreff_b'].$userRow['email'];
				if($coinStatus == 1) {
					$vars = array('coins' => $bitcoins,
								  'bcacc' => $this->getBitcoinAccount()); 
					SystemMail($bm_prefs['passmail_abs'], $userRow['email'], $lang_user['pf_mail_betreff'], 'pf_mailTXT_bc', $vars);
					SystemMail($bm_prefs['passmail_abs'], $bm_prefs['passmail_abs'], $betreff, 'pf_mailTXT_bc', $vars);
				} else {
					$vars = array('amount' => $amount,
								  'vkcode' => $vkCode); 
					SystemMail($bm_prefs['passmail_abs'], $userRow['email'], $lang_user['pf_mail_betreff'], 'pf_mailTXT', $vars);
					SystemMail($bm_prefs['passmail_abs'], $bm_prefs['passmail_abs'], $betreff, 'pf_mailTXT', $vars);
				}
				
				$tpl->assign('title', $lang_user['pf_ueberschrift']);
				$tpl->assign('msg', $lang_user['pf_send_ok']);
				$tpl->assign('backLink', 'start.php?sid='.session_id());
				$tpl->assign('pageContent', 'li/msg.tpl');
				$tpl->display('li/index.tpl');
				exit();
			}
			
			$res = $db->Query('SELECT * FROM {pre}mod_premium_packages WHERE geloescht=0');
			while($row = $res->FetchArray(MYSQL_ASSOC))
			{
				$pacs[] = $row;
			}
            $res->Free();
			
			$tpl->assign('pacs', $pacs);
			$tpl->assign('pageContent', $this->_templatePath('postfach.order.tpl'));
			$tpl->display('li/index.tpl');
			exit();
		}
	}
	
	function _intervalStr($abrechnung, $abrechnung_t, $withoutPrefix = false)
	{
		global $lang_user;
		
		if($abrechnung_t == 1)
		{
			switch($abrechnung)
			{
			case 'wochen':
				return ($withoutPrefix ? '1 ' : $lang_user['pacc_jede'] . ' ') . $lang_user['pacc_woche'];
				break;
			case 'monate':
				return ($withoutPrefix ? '1 ' : $lang_user['pacc_jeden'] . ' ') . $lang_user['pacc_monat'];
				break;
			case 'jahre':
				return ($withoutPrefix ? '1 ' : $lang_user['pacc_jedes'] . ' ') . $lang_user['pacc_jahr'];
				break;
			case 'einmalig':
				return $lang_user['pacc_einmalig'];
				break;
			}
		}
		else
		{
			switch($abrechnung)
			{
			case 'wochen':
				return ($withoutPrefix ? '' : $lang_user['pacc_alle'] . ' ') . $abrechnung_t . ' ' . $lang_user['pacc_wochen'];
				break;
			case 'monate':
				return ($withoutPrefix ? '' : $lang_user['pacc_alle'] . ' ') . $abrechnung_t . ' ' . $lang_user['pacc_monate'];
				break;
			case 'jahre':
				return ($withoutPrefix ? '' : $lang_user['pacc_alle'] . ' ') . $abrechnung_t . ' ' . $lang_user['pacc_jahre'];
				break;
			case 'einmalig':
				return $lang_user['pacc_einmalig'];
				break;
			}
		}
	}
	
	function getBitcoinAccount()
	{
		// namens-daten holen
		$bcacc = file_get_contents("./bitcoinaccounts/bitcoinaccounts.txt");
		// zeilen untereinander lesen
		$zeilen = explode("\n", $bcacc);
		// zeilen per zufall mischen
		shuffle($zeilen);
		// 1. zeile holen und zur�ckgeben
		return $zeilen[0];
	}
	
	function getBitcoins($preis)
	{
		// Cent in Euro umrechnen
		$preis = $preis/100;
		
		// URL generieren
		$url = "https://blockchain.info/tobtc?currency=EUR&value=$preis";
		
		// is cURL installed yet?
		if (!function_exists('curl_init')) {
			die('Sorry cURL is not installed!');
		}
	 
		// OK cool - then let's create a new cURL resource handle
		$ch = curl_init();
	 
		// Now set some options (most are optional)
	 
		// Set URL to download
		curl_setopt($ch, CURLOPT_URL, $url);
	 
		// Set a referer
		curl_setopt($ch, CURLOPT_REFERER, "http://www.aikq.de");
	 
		// User agent
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0");
	 
		// SSL-URL
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	
		// Include header in result? (0 = yes, 1 = no)
		curl_setopt($ch, CURLOPT_HEADER, 0);
	 
		// Should cURL return or print out the data? (true = return, false = print)
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 
		// Timeout in seconds
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	 
		// Download the given URL, and return output
		$output = curl_exec($ch);
	 
		// Close the cURL resource, and free system resources
		curl_close($ch);

		return $output;
	}
}

$plugins->registerPlugin('postfachorder');

?>