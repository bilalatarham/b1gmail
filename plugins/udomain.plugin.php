<?php 
/*
 * 
 * (c) Smart-Mail.de for b1g.de v7.0 and higher
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: udomain.plugin.php,v 1.0 2009/04/06 10:00:00 Informant $
 *
 */

class UDOMAIN extends BMPlugin 
{
	function UDOMAIN()
	{
		global $lang_user;
		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'User-Domain';
		$this->author			= 'Informant';
		$this->version			= '1.0';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			// user
			$lang_admin['udomain']				= 'User-Domain';
		}
		else
		{
			$lang_admin['udomain']				= 'User-Domain';
		}
	}

	function Install()
	{
		global $db, $bm_prefs;

		// ualiase erstellen
		$db->Query('ALTER TABLE {pre}users ADD ualiase VARCHAR(1000) NOT NULL');

		PutLog('User-Domain Plugin was successfull installed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		global $db;
		
		// eintrag l�schen
		$db->Query('ALTER TABLE {pre}users DROP ualiase');
		
		PutLog('User-Domain Plugin was successfull removed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}

	// users.php config
	function AfterInit()
	{
		global $db;

		// update der user-einstellung
		if((substr($_SERVER['SCRIPT_NAME'], -16) == '/admin/users.php') && $_REQUEST['do'] == 'edit' && $_REQUEST['save'] && RequestPrivileges(PRIVILEGES_ADMIN, true))
		{
			// update db vom user
			$db->Query('UPDATE {pre}users SET ualiase=? WHERE id=?', $_REQUEST['ualiase'], $_REQUEST['id']);
		}
	}

	// prefs.php config
	function FileHandler($file, $action) 
	{
		global $groupRow, $userRow;
		
		if($file == 'prefs.php' && $action == 'aliases')
		{
			if(trim($userRow['ualiase']) != '') 
			{
				if(trim($groupRow['saliase']) != '')
				{
				    $domainList = array();
                    $domainList = explode(';', $userRow['ualiase']);

					foreach($domainList as $domainAlias)
					{
						$groupRow['saliase'] .= ':' . $domainAlias; 
					}
				}
				else
                {
                    $domainList = array();
                    $domainList = explode(';', $userRow['ualiase']);

                    foreach($domainList as $domainAlias)
					{
					    if(!$groupRow['saliase'])
                            $groupRow['saliase'] = $domainAlias;
                        else
                            $groupRow['saliase'] .= ':' . $domainAlias;
					}
                }
			}
		}
	}
}
/**
 * register plugin
 */
$plugins->registerPlugin('UDOMAIN');

?>
