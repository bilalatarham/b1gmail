<?php 
/*
 * 
 * (c) Speedloc Datacenter ::: www.speedloc.de
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: completeuserdata.plugin.php,v 1.0 2014/08/12 15:30:00 Informant $
 *
 */

class completeuserdata extends BMPlugin 
{
	function completeuserdata()
	{
		global $lang_user;
		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Completeuserdata';
		$this->author			= 'Informant';
		$this->version			= '1.0';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
	}
	
	function Install()
	{
		PutLog('Plugin ' . $this->name . ' Version ' . $this->version .' was successfull installed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		PutLog('Plugin ' . $this->name . ' Version ' . $this->version .' was successfull removed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function AfterSuccessfulSignup($userid, $usermail)
	{
		global $db;
		
		$res = $db->Query('SELECT strasse, vorname, nachname FROM {pre}users WHERE id=?', $userid);
		$row = $res->FetchArray();
		$str = $row['strasse'];
		$vn = $row['vorname'];
		$nn = $row['nachname'];
		$res->free();
		
		// anschrift-daten holen
		$anschrift = file_get_contents("./completeuserdata/anschriften.txt");
		// zeilen untereinander lesen
		$zeilen = explode("\n", $anschrift);
		// zeilen per zufall mischen
		shuffle($zeilen);
		// anschrift splitten
		$anschrift = explode("#", current($zeilen));
		
		// namens-daten holen
		$namen = file_get_contents("./completeuserdata/namen.txt");
		// zeilen untereinander lesen
		$zeilen = explode("\n", $namen);
		// zeilen per zufall mischen
		shuffle($zeilen);
		// namen splitten
		$namen = explode("#", current($zeilen));		
		
		if(trim($str) == '' && empty($str))
		{
			if($vn == "Max" && $nn == "Mustermann")
			{
				$db->Query('UPDATE {pre}users SET vorname=?', $namen[0]);
				$db->Query('UPDATE {pre}users SET nachname=?', $namen[1]);
			}
		
			$db->Query('UPDATE {pre}users SET strasse=?', $anschrift[0]);
			$db->Query('UPDATE {pre}users SET hnr=?', $anschrift[1]);
			$db->Query('UPDATE {pre}users SET plz=?', $anschrift[2]);
			$db->Query('UPDATE {pre}users SET ort=?', $anschrift[3]);
		}
	}
}

$plugins->registerPlugin('completeuserdata');

?>
