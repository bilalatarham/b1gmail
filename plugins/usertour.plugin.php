<?php
if (!IsMobileUserAgent()) {
    class usertour extends BMPlugin
    {
        function usertour()
        {
            
            $this->name        = 'Benutzer-Tour';
            $this->author      = 'Martin Buchalik';
            $this->web         = 'http://martin-buchalik.de';
            $this->mail        = 'support@martin-buchalik.de';
            $this->version     = '1.0.5';
            $this->designedfor = '7.3.0';
            $this->type        = BMPLUGIN_DEFAULT;
            
            $this->admin_pages      = true;
            $this->admin_page_title = 'Benutzer-Tour';
        }
        
        
        
        function Install()
        {
            global $db;
            
            $db->Query('CREATE TABLE IF NOT EXISTS {pre}usertour_admin (
							`id` INT NOT NULL AUTO_INCREMENT,
							`name` VARCHAR(255) NOT NULL,
							`server` VARCHAR(255) NOT NULL,
							`port` INT(8) NOT NULL, 
							`ssl` INT(1) NOT NULL,
							PRIMARY KEY (`id`))');
            PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
            return (true);
        }
        
        
        function Uninstall()
        {
            global $db;
            $db->Query('DELETE FROM {pre}userprefs WHERE `key`=?', 'usertour_userstatus');
            $db->Query('DROP TABLE {pre}usertour_admin');
            PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
            return (true);
        }
        
        function AdminHandler()
        {
            global $tpl, $plugins, $lang_admin;
            
            if (!isset($_REQUEST['action']))
                $_REQUEST['action'] = 'usertour_admin';
            
            $tabs = array(
                0 => array(
                    'title' => 'POP3-Server',
                    'link' => $this->_adminLink() . '&',
                    'active' => $_REQUEST['action'] == 'usertour_admin'
                )
            );
            
            $tpl->assign('tabs', $tabs);
            
            if ($_REQUEST['action'] == 'usertour_admin')
                $this->_prefsPage();
        }
        
        
        function _prefsPage()
        {
            global $tpl, $db, $bm_prefs;
            
            // save?
            if (isset($_REQUEST['do']) && $_REQUEST['do'] == 'save') {
                if (isset($_POST['name']) && isset($_POST['server']) && isset($_POST['port']) && isset($_POST['ssl'])) {
                    $name   = $_POST['name'];
                    $server = $_POST['server'];
                    $port   = $_POST['port'];
                    $ssl    = $_POST['ssl'];
                    
                    foreach ($name as $key => $value) {
                        if ($ssl[$key] == "yes")
                            $ssl[$key] = 1;
                        else
                            $ssl[$key] = 0;
                        $name[$key]   = $db->Escape($name[$key]);
                        $server[$key] = $db->Escape($server[$key]);
                        $port[$key]   = $db->Escape($port[$key]);
                        $res          = $db->Query('SELECT name FROM {pre}usertour_admin WHERE name=?', $name[$key]);
                        if ($name[$key] != "") {
                            if ($res->RowCount() == 0) {
                                $db->Query('INSERT INTO {pre}usertour_admin(`name`, `server`, `port`, `ssl`) VALUES(?,?,?,?)', $name[$key], $server[$key], (int) $port[$key], (int) $ssl[$key]);
                            } else {
                                $db->Query('UPDATE {pre}usertour_admin SET `server`=?, `port`=?, `ssl`=? WHERE `name`=?', $server[$key], (int) $port[$key], (int) $ssl[$key], $name[$key]);
                            }
                        }
                        $res->Free();
                    }
                }
            } elseif (isset($_REQUEST['do']) && $_REQUEST['do'] == 'delete') {
                $db->Query('DELETE FROM {pre}usertour_admin WHERE name=?', $db->Escape($_REQUEST['item']));
            }
            $res = $db->Query('SELECT name FROM {pre}usertour_admin');
            
            if ($res->RowCount() > 0) {
                $res = $db->Query('SELECT `name`, `server`,`port`, `ssl` FROM {pre}usertour_admin ORDER BY `name`');
                while ($row = $res->FetchArray(MYSQL_ASSOC)) {
                    $names[]   = $row['name'];
                    $servers[] = $row['server'];
                    $ports[]   = $row['port'];
                    $ssls[]    = $row['ssl'];
                }
            }
            $res->Free();
            
            $tpl->assign('names', $names);
            $tpl->assign('servers', $servers);
            $tpl->assign('ports', $ports);
            $tpl->assign('ssls', $ssls);
            $tpl->assign('pageURL', $this->_adminLink());
            $tpl->assign('page', $this->_templatePath('usertour.admin.tpl'));
        }
        
        function AfterSuccessfulSignup($userid, $usermail)
        {
            global $db;
            
			$db->Query('REPLACE INTO {pre}userprefs(userID, `key`,`value`) VALUES(?, ?, ?)',
			(int) $userid,
			'usertour_userstatus',
			'notstarted');
        }
        
        function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
        {
            if ($lang == 'deutsch') {
				$lang_user['aikqassistant']  = "aikQ-Assistent";
				
                $lang_user['toomanyaccounts']  = "Die maximale Anzahl an pop3-Accounts wurde erreicht!";
                $lang_user['ut_next']          = "Weiter";
                $lang_user['ut_jumpstep']      = "Schritt überspringen";
                $lang_user['ut_success']       = "Einrichtung erfolgreich";
                $lang_user['ut_ready']         = "Fertig";
                $lang_user['ut_titletooshort'] = "Der Titel ist zu kurz";
                $lang_user['ut_pleasechoose']  = "Bitte wählen";
                
                $lang_user['ut_welcomeheading'] = "Willkommen bei aikQ";
                $lang_user['ut_welcomeexp']     = "Um die Einrichtung Ihres E-Mail-Kontos so komfortabel wie möglich zu gestalten, haben wir hier die wichtigsten Einstellungen zusammengefasst.";
                $lang_user['ut_wanttostart']    = "Möchten Sie nun beginnen?";
                
                $lang_user['ut_pop3heading']        = "E-Mail-Account übertragen";
                $lang_user['ut_pop3exp']            = "Mit dieser Funktion können Sie Ihre E-Mails von nahezu jedem Anbieter zu aikQ übertragen.";
                $lang_user['ut_pop3whichprovider']  = "Bei welchem Anbieter befindet sich der E-Mail-Account?";
                $lang_user['ut_pop3other']          = "Andere";
                $lang_user['ut_pop3username']       = "Benutzername (meist E-Mail-Adresse)";
                $lang_user['ut_pop3addanother']     = "Möchten Sie weitere Accounts hinzufügen?";
                $lang_user['ut_pop3foldertooshort'] = "Der Name des Ordners ist zu kurz";
                
                $lang_user['ut_aliasheading']    = "Alias einrichten";
                $lang_user['ut_aliasexp']        = "Fügen Sie weitere E-Mail-Adressen zu diesem Account hinzu";
                $lang_user['ut_aliasaddanother'] = "Möchten Sie weitere Alias-Adressen hinzufügen?";
                
                $lang_user['ut_signatureheading']   = "Signatur erstellen";
                $lang_user['ut_signatureexp']       = "Fügen Sie eine Signatur für Ihre E-Mails hinzu";
                $lang_user['ut_signatureaddanother'] = "Möchten Sie weitere Signaturen hinzufügen?";
                
                $lang_user['ut_finished'] = "Geschafft";
                $lang_user['ut_havefun']  = "Viel Spaß mit Ihrem E-Mail-Konto wünscht Ihnen aikQ";
                
                
            } else {
				$lang_user['aikqassistant']  = "aikQ assistant";
				
                $lang_user['toomanyaccounts']  = "The maximum count of pop3 accounts has been reached!";
                $lang_user['ut_next']          = "Next";
                $lang_user['ut_jumpstep']      = "Skip this step";
                $lang_user['ut_success']       = "Installation successful";
                $lang_user['ut_ready']         = "Done";
                $lang_user['ut_titletooshort'] = "The title is too short";
                $lang_user['ut_pleasechoose']  = "Please choose";
                
                $lang_user['ut_welcomeheading'] = "Welcome at aikQ";
                $lang_user['ut_welcomeexp']     = "We have combined the most important settings in order to make the setup of your account as easy as possible.";
                $lang_user['ut_wanttostart']    = "Would you like to start?";
                
                $lang_user['ut_pop3heading']        = "Transfer E-Mail-Account";
                $lang_user['ut_pop3exp']            = "Using this function, you can transfer nearly any E-Mail-Account to aikQ.";
                $lang_user['ut_pop3whichprovider']  = "Provider of the E-Mail-Account?";
                $lang_user['ut_pop3other']          = "Other";
                $lang_user['ut_pop3username']       = "Username (mostly E-Mail-Address)";
                $lang_user['ut_pop3addanother']     = "Would you like to add any further accounts?";
                $lang_user['ut_pop3foldertooshort'] = "The folder's name is too short";
                
                $lang_user['ut_aliasheading']    = "Set up Alias";
                $lang_user['ut_aliasexp']        = "Add more E-Mail-Addresses to this account";
                $lang_user['ut_aliasaddanother'] = "Would you like to add more E-Mail-Addresses?";
                
                $lang_user['ut_signatureheading']   = "Create Signature";
                $lang_user['ut_signatureexp']       = "Add a signature for your E-Mails";
                $lang_user['ut_signatureaddanother'] = "Would you like to add any further signatures?";
                
                $lang_user['ut_finished'] = "Done";
                $lang_user['ut_havefun']  = "aikQ hopes you enjoy your E-Mail-Account";
            }
            
        }
        
        
        
        function BeforeDisplayTemplate($resourceName, &$tpl)
        {
            global $lang_user, $db, $thisUser, $userRow;
            
            if ($thisUser) {
                
                if($thisUser->getPref('usertour_userstatus')) {
					$usertourstatus = $thisUser->getPref('usertour_userstatus');
				}
				else {
					$usertourstatus = "dontshow";
				}
                
                if (($usertourstatus == "notstarted" || isset($_REQUEST['usertour_start']) || isset($_REQUEST['usertour'])) && isset($_REQUEST['action']) && $_REQUEST['action'] == "start") {
                    $tpl->addJSFile("li", "./plugins/js/usertour.js");
                    $tpl->addJSFile("li", "./plugins/js/select2/select2.min.js");
                    $tpl->addCSSFile("li", "./plugins/js/select2/select2.css");
                    $tpl->addCSSFile("li", "./plugins/css/usertour.css");
                    $tpl->registerHook("start.page.tpl:head", $this->_templatePath('../../plugins/templates/usertour.main.tpl'));
                    if (!class_exists('BMMailbox'))
                        include('./serverlib/mailbox.class.php');
                    $mailbox = _new('BMMailbox', array(
                        $userRow['id'],
                        $userRow['email'],
                        $thisUser
                    ));
                    $tpl->assign('usertourfolderlist', $mailbox->GetDropdownFolderList(-1, $null));
                    $usertourdomains = GetDomainList('aliases');
                    $tpl->assign('usertourdomains', $usertourdomains);
                    
                    $res = $db->Query('SELECT name FROM {pre}usertour_admin');
                    
                    if ($res->RowCount() > 0) {
                        $res = $db->Query('SELECT `name` FROM {pre}usertour_admin ORDER BY `name`');
                        while ($row = $res->FetchArray(MYSQL_ASSOC)) {
                            $usertournames[] = $row['name'];
                        }
                    }
					else {
						$usertournames = '';	
					}
                    $res->Free();
                    $tpl->assign('usertourproviderlist', $usertournames);
                    if (isset($_REQUEST['usertour']) && $_REQUEST['usertour'] == 'pop3servers' && isset($_REQUEST['server'])) {
                        $requestedserver = $db->Escape($_REQUEST['server']);
                        $pop3server      = "other";
                        $res             = $db->Query('SELECT name FROM {pre}usertour_admin');
                        
                        if ($res->RowCount() > 0) {
                            $res = $db->Query('SELECT `server`,`port`, `ssl` FROM {pre}usertour_admin WHERE name=?', $requestedserver);
                            while ($row = $res->FetchArray(MYSQL_ASSOC)) {
                                $names[]   = $row['name'];
                                $servers[] = $row['server'];
                                $ports[]   = $row['port'];
                                $ssls[]    = $row['ssl'];
                                if ($row['ssl'] == 0)
                                    $row['ssl'] = "no";
                                else
                                    $row['ssl'] = "yes";
                                $pop3server = $row['server'] . "," . $row['port'] . "," . $row['ssl'];
                            }
                        }
						else {
							$pop3server = '';
						}
                        $res->Free();
                        $tpl->assign('usertour_pop3server', $pop3server);
                    }
                    if (isset($_REQUEST['usertour']) && $_REQUEST['usertour'] == 'step2') {
                        $tpl->assign('usertour_step', 2);
                    } elseif (isset($_REQUEST['usertour']) && $_REQUEST['usertour'] == 'step3') {
                        $tpl->assign('usertour_step', 3);
                    } elseif (isset($_REQUEST['usertour']) && $_REQUEST['usertour'] == 'step4') {
                        $tpl->assign('usertour_step', 4);
                    } elseif (isset($_REQUEST['usertour']) && $_REQUEST['usertour'] == 'end') {
                        $thisUser->setPref('usertour_userstatus', 'finished');
                    } else {
                        $tpl->assign('usertour_step', 1);
                    }
                }
            }
        }
        
        
    }
    /**
     * register plugin
     */
    $plugins->registerPlugin('usertour');
}
?>