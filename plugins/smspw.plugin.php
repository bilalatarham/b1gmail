<?php 
/*
 * 
 * (c) Informant
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: smspw.plugin.php,v 1.2 2014/04/20 18:30:00 Informant $
 *
 */

class SMSPW extends BMPlugin 
{
	function SMSPW()
	{
		global $lang_user;
		
		$this->type			= BMPLUGIN_DEFAULT;
		$this->name			= 'SMS-PW';
		$this->author			= 'Informant';
		$this->version			= '1.2';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
		
		// admin pages
		$this->admin_pages		= true;
		$this->admin_page_title		= 'SMS-PW';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		global $currentCharset;
		$_lang_custom = $_lang_user = $_lang_client = $_lang_admin = array();

		if($lang == 'deutsch')
		{
			// misc
			$_lang_user['smspwnr']			= 'Handynummer';
			$_lang_user['smspwka']			= 'Ein unbekannter Fehler ist aufgetreten. Bitte versuchen Sie es erneut oder wenden Sie sich direkt an den Support.';
			$_lang_user['smspwreloadsperre']	= 'Bitte haben Sie daf&uuml;r Verst&auml;ndnis, dass das Passwort aus Sicherheitsgr&uuml;nden nur einmal innerhalb von 7 Tagen angefordert werden kann.';
			$_lang_user['smspwpreiserror']		= 'Bedauerlicherweise weist Ihr Account nicht gen&uuml;gend Credits auf, um Ihnen das Passwort per SMS zu senden. Bitte wenden Sie sich direkt an den Support!';
			$_lang_user['smspwrautherror']		= 'Bedauerlicherweise haben Sie Ihre Handynummer noch nicht verifiziert. Aus Datenschutzgr&uuml;nden k&ouml;nnen wir Ihnen das Passwort daher nicht zusenden. Bitte wenden Sie sich direkt an den Support!';
			$_lang_user['smspwresetsuccess']	= 'Ein neues Passwort wurde Ihnen per SMS zugesendet!';
			$_lang_user['smspwreseterror']		= 'Bedauerlicherweise wurde die von Ihnen eingegebene Handynummer bei mehreren Benutzern im System gefunden. Aus diesem Grund k&ouml;nnen wir Ihnen das Passwort nicht per SMS zusenden. Bitte wenden Sie sich direkt an den Support!';
			$_lang_user['smspwresetnotfound']	= 'Die von Ihnen eingegebene Handynummer wurde nicht gefunden!';
			// admin
			$_lang_admin['smspw']			= 'SMS-PW aktivieren';
			$_lang_admin['smspwuserkosten']		= 'Kostentr&auml;ger';
			$_lang_admin['smspwuser']		= 'User/Kunde';
			$_lang_admin['smspwprovider']		= 'Anbieter';
			// system
			$_lang_custom['smspwtxt']		= 'Ihr neues Passwort f�r %%mailadresse%% lautet: %%npw%%';
            		$_lang_custom['smsmsgtxt']		= 'Sie haben Ihr Passwort vergessen und keine alternative E-Mailadresse angegeben? Kein Problem - Sofern Sie Ihre Handynummer hinterlegt haben, k�nnen Sie sich an diese ein neues Passwort zusenden lassen.';
		}
		else
		{
			$_lang_user['smspwnr']			= 'Handynumber';
			$_lang_user['smspwka']			= 'An unknown error has occurred. Please try again or contact us directly at the support.';
			$_lang_user['smspwreloadsperre']	= 'Please understand that the password for security reasons only once within 7 days can be requested.';
			$_lang_user['smspwpreiserror']		= 'Unfortunately, your account is not enough credits on to bring you the password via SMS to be sent. Please refer directly to the Support!';
			$_lang_user['smspwrautherror']		= 'Unfortunately, your phone number has not yet been verified. For privacy reasons we can not therefore be the password to you. Please refer directly to the Support!';
			$_lang_user['smspwresetsuccess']	= 'A new password has been sent to you via SMS!';
			$_lang_user['smspwreseterror']		= 'Unfortunately, the mobile number you entered for multiple users on the system. For this reason, we can offer you the password is not sent via SMS. Please refer directly to the Support!';
			$_lang_user['smspwresetnotfound']	= 'The mobile number you entered was not found!';
			$_lang_admin['smspw']			= 'activate SMS-PW';
			$_lang_admin['smspwuserkosten']		= 'cost unit';
			$_lang_admin['smspwuser']		= 'user/customer';
			$_lang_admin['smspwprovider']		= 'provider';
			$_lang_custom['smspwtxt']		= 'Your new password for %%mailadresse%% is: %%npw%%';
            		$_lang_custom['smsmsgtxt']		= 'Forgot your password, and no alternate email address? No problem - if your phone number stored, you can go to these a new password sent to you.';
		}

	        // convert charset
		$arrays = array('admin', 'client', 'user', 'custom');
		foreach($arrays as $array)
		{
            		$destArray = sprintf('lang_%s', $array);
            		$srcArray  = '_' . $destArray;

            		if(!isset($$srcArray))
                		continue;

            		foreach($$srcArray as $key=>$val)
            		{
                		if(function_exists('CharsetDecode') && !in_array(strtolower($currentCharset), array('iso-8859-1', 'iso-8859-15')))
                    			$val = CharsetDecode($val, 'iso-8859-15');
                		${$destArray}[$key] = $val;
            		}
		}
	}

	function Install()
	{
		global $db;

		// sms-pw aktivieren/deaktivieren, kosten tragen user/anbieter und text der sms
		$db->Query('Create TABLE {pre}smspw (`id` INT(11) NOT NULL AUTO_INCREMENT, smspw TINYINT(4) NOT NULL DEFAULT 1, smspwuserkosten TINYINT(4) NOT NULL DEFAULT 1, PRIMARY KEY (`id`))');
		$db->Query("INSERT INTO {pre}smspw(smspw,smspwuserkosten) VALUES(1,1)");

		
		PutLog('SMS-PW Plugin was successfull installed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		global $db;
		
		// eintr�ge l�schen
		$db->Query('DROP TABLE {pre}smspw');
		// alle key-eintr�ge l�schen
		$db->Query('DELETE FROM {pre}userprefs WHERE `key`=?', 'smspw_time');
		
		PutLog('SMS-PW Plugin was successfull removed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function AdminHandler()
	{
		global $tpl, $plugins, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'prefs';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['prefs'],
				'link'      => $this->_adminLink() . '&action=prefs&',
				'active'	=> $_REQUEST['action'] == 'prefs')
			);

		$tpl->assign('tabs', $tabs);
		
		if($_REQUEST['action'] == 'prefs')
			$this->_prefsPage();
	}
	
	function _prefsPage()
	{
		global $tpl, $db;
		
		// sms-pw daten holen
		$sql = $db->Query("SELECT * FROM {pre}smspw");
		$row = $sql->FetchArray();
		$smspwuserkosten = $row['smspwuserkosten'];
		$smspw = $row['smspw'];
		$sql->Free();
						
		// speichern
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}smspw SET smspw=?',
				isset($_REQUEST['smspw'])
				);
			$smspw = isset($_REQUEST['smspw']) ? 1 : 0;

			$db->Query('UPDATE {pre}smspw SET smspwuserkosten=?',
				$_REQUEST['smspwuserkosten']
				);
			$smspwuserkosten = $_REQUEST['smspwuserkosten'];
		}
		
		// daten an template �bergeben
		$tpl->assign('smspwuserkosten', $smspwuserkosten);
		$tpl->assign('smspw', $smspw);
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('smspw.plugin.prefs.tpl'));
		return(true);
	}
	
	function FileHandler($file, $action) 
	{
		global $tpl, $db, $userRow, $bm_prefs, $lang_user, $lang_custom;
		
		
		// sms-pw
		if($file == 'index.php' && $action == 'lostpw')
		{
			// sms-pw daten holen
			$sql = $db->Query("SELECT * FROM {pre}smspw");
			$row = $sql->FetchArray();
			$smspwuserkosten = $row['smspwuserkosten'];
			$smspw = $row['smspw'];
			$sql->Free();
		
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'sendsmspw')
			{
				// abfangen der eingegebenen handynummer und sicherheitspr�fung
				//$smspwnr = mysql_real_escape_string($_REQUEST['smspwnr']);
				$smspwnr = $db->Escape($_REQUEST['smspwnr']);
				
				// ggf. vorhandene leerzeichen rausfiltern
				$smspwnr = ereg_replace(" ","",$smspwnr);
				// ggf. vorhandene "-" rausfiltern
				$smspwnr = ereg_replace("-","",$smspwnr);
				// ggf. vorhandene "/" rausfiltern
				$smspwnr = ereg_replace("/","",$smspwnr);
				
				// pr�fen, ob eine handynummer eingegeben wurde
				if($smspwnr == '')
				{
					// input-feld "smspwnr" bei fehler rot markieren
					$tpl->assign('invalidFields', 'smspwnr');
				}
				else 
				{
					// z�hler
					$zae = 0;
					$akttime = time();
					
					$sql = $db->Query("SELECT gesperrt, id FROM {pre}users WHERE mail2sms_nummer = ? AND gesperrt =?", $smspwnr, 'no');
					while($row = $sql->FetchArray(MYSQL_ASSOC))
					{
							$userid = $row['id'];
							$zae++;
					}
					$sql->Free();
					
					// genau einen user mit der eingegebenen handynummer gefunden
					if($zae == 1)
					{
						// userdaten holen
						$sql = $db->Query("SELECT email, sms_validation, gruppe, sms_kontigent FROM {pre}users WHERE id = ?", $userid);
						$row = $sql->FetchArray();
						$mail = $row['email'];
						$uauth = $row['sms_validation'];
						$gruppe = $row['gruppe'];
						$ugeld = $row['sms_kontigent'];
						$sql->Free();
												
						// gruppendaten holen
						$sql = $db->Query("SELECT smsvalidation, sms_monat FROM {pre}gruppen WHERE id = ?", $gruppe);
						$row = $sql->FetchArray();
						$gauth = $row['smsvalidation'];
						$sql->Free();
						
						// standard sms-gateway holen
						$sgw = $bm_prefs['sms_gateway'];
						
						// preis vom std-gateway ermitteln
						$sql = $db->Query("SELECT price, typ FROM {pre}smstypen WHERE std = ?", 1);
						$row = $sql->FetchArray();
						$preis = $row['price'];
						$typ = $row['typ'];
						$sql->Free();
						
						// gateway daten holen
						$sql = $db->Query("SELECT getstring, user, pass, success FROM {pre}smsgateways WHERE id = ?", $sgw);
						$row = $sql->FetchArray();
						$gw = $row['getstring'];
						$gwuser = $row['user'];
						$gwpass = $row['pass'];
						$gwsuccess = $row['success'];
						$sql->Free();
												
						// user-objekt erstellen (f�r credit-berechnung und credit-abrechnung)
						$userObject = _new('BMUser', array($userid));
						
						// zeit der zuletzt versendeten pw-sms vom user holen
						$utime = $userObject->GetPref('smspw_time');
						
						// holen der gesamt credits von user- und gruppen-credits
						$ugeld = $userObject->GetBalance();
						
						// neues pw generieren
						$string_laenge = 7;
					    srand ((double)microtime()*1000000);
					    $zufall = rand();
					    $npw = substr(md5($zufall), 0, $string_laenge);
						$smspw = $npw;
		
						//salt codierung vom passwort
						$saltpw = $this->genPrivKey($length = 8);
						$npw = md5(md5($npw).$saltpw);
		    
					    // sms-text mit daten f�llen
					    $smspwtxt = $lang_custom['smspwtxt'];
					    $smspwtxt = str_replace("%%npw%%", $smspw, $smspwtxt);
					    $smspwtxt = str_replace("%%mailadresse%%", $mail, $smspwtxt);
						
						// gateway zusammen bauen
						$gw = str_replace("%%user%%", $gwuser, $gw);
						$gw = str_replace("%%passwort%%", $gwpass, $gw);
						$gw = str_replace("%%typ%%", $typ, $gw);
						$gw = str_replace("%%msg%%", urlencode($smspwtxt), $gw);
						$gw = str_replace("%%from%%", $bm_prefs['mail2sms_abs'], $gw);
						$gw = str_replace("%%to%%", $smspwnr, $gw);
						$gw = str_replace("%%usermail%%", $mail, $gw);
						$gw = str_replace("%%userid%%", $userid, $gw);
						//in utf-8 an smstrade �bertragen
						$gw = $gw."&charset=UTF-8";

						// reloadsperre - 7 tage
						$reloadtime = $akttime - 604800;
						
						// pr�fen, ob user au�erhalb der reloadsperre ist
						if($utime <= $reloadtime)
						{
							// wenn sms-verifizierung bei der gruppe des users aktiviert ist
							if($gauth == "yes")
							{
								// pr�fen ob user handynr. verifiziert hat
								if($uauth != 0)
								{
									// pr�fen, ob user kosten tr�gt
									if($smspwuserkosten == 1)
									{
										// pr�fen, ob user genug geld f�r die sms hat
										if($ugeld >= $preis)
										{
											// pr�fen, ob verwendete klasse eingebunden ist - wenn nicht, dann die klasse einbinden
											if(!class_exists('BMHTTP'))
												include(B1GMAIL_DIR . 'serverlib/http.class.php');										
											// pw per sms senden
											// werte�bergabe an den gateway						
											$http = _new('BMHTTP', array($gw));
											// r�ckgabewert abfangen
											$result = $http->DownloadToString();

											// die ersten 3 zeichen rausschneiden
											$result = substr($result, 0, 3);

											if($result == $gwsuccess)
											{
												// sms-preis vom user abziehen
												$userObject->Debit($preis*(-1));
												// sendezeit der sms beim user updaten
												$userObject->SetPref('smspw_time', $akttime);
												// user-pw updaten
												$db->Query('UPDATE {pre}users SET passwort=?, passwort_salt=? WHERE id = ?', $npw, $saltpw, $userid);
												// erfolgs-msg anzeigen
												$tpl->assign('msgok', $lang_user['smspwresetsuccess']);
												// log-eintrag schreiben
												PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") successfully!", PRIO_PLUGIN, __FILE__, __LINE__);
											}
											else
											{
												$tpl->assign('msgerror', $lang_user['smspwka']);
                                                PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") failed! Result: ".$result, PRIO_WARNING, __FILE__, __LINE__);
											}
										}
										else
										{
											$tpl->assign('msgerror', $lang_user['smspwpreiserror']);
										}
									}
									// anbieter tr�gt kosten
									else
									{
										// pr�fen, ob verwendete klasse eingebunden ist - wenn nicht, dann die klasse einbinden
										if(!class_exists('BMHTTP'))
											include(B1GMAIL_DIR . 'serverlib/http.class.php');	
										// pw per sms senden
										// werte�bergabe an den gateway						
										$http = _new('BMHTTP', array($gw));
										// r�ckgabewert abfangen
										$result = $http->DownloadToString();
										
										// die ersten 3 zeichen rausschneiden
										$result = substr($result, 0, 3);
										
										if($result == $gwsuccess)
										{
											// sendezeit der sms beim user updaten
											$userObject->SetPref('smspw_time', $akttime);
											// user-pw updaten
											$db->Query('UPDATE {pre}users SET passwort=?, passwort_salt=? WHERE id = ?', $npw, $saltpw, $userid);
											// erfolgs-msg anzeigen
											$tpl->assign('msgok', $lang_user['smspwresetsuccess']);
											// log-eintrag schreiben
											PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") successfully!", PRIO_PLUGIN, __FILE__, __LINE__);
										}
										else
										{
											$tpl->assign('msgerror', $lang_user['smspwka']);
                                            PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") failed! Result: ".$result, PRIO_WARNING, __FILE__, __LINE__);
										}
									}
								}
								else
								{
									$tpl->assign('msgerror', $lang_user['smspwrautherror']);
								}
							}
							else
							{
								// pr�fen, ob user kosten tr�gt
								if($smspwuserkosten == 1)
								{
									// pr�fen, ob user genug geld f�r die sms hat
									if($ugeld >= $preis)
									{
										// pr�fen, ob verwendete klasse eingebunden ist - wenn nicht, dann die klasse einbinden
										if(!class_exists('BMHTTP'))
											include(B1GMAIL_DIR . 'serverlib/http.class.php');											
										// pw per sms senden
										// werte�bergabe an den gateway						
										$http = _new('BMHTTP', array($gw));
										// r�ckgabewert abfangen
										$result = $http->DownloadToString();
										
										// die ersten 3 zeichen rausschneiden
										$result = substr($result, 0, 3);
										
										if($result == $gwsuccess)
										{
											// sms-preis vom user abziehen
											$userObject->Debit($preis*(-1));
											/// sendezeit der sms beim user updaten
											$userObject->SetPref('smspw_time', $akttime);
											// user-pw updaten
											$db->Query('UPDATE {pre}users SET passwort=?, passwort_salt=? WHERE id = ?', $npw, $saltpw, $userid);
											// erfolgs-msg anzeigen
											$tpl->assign('msgok', $lang_user['smspwresetsuccess']);
											// log-eintrag schreiben
											PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") successfully!", PRIO_PLUGIN, __FILE__, __LINE__);
										}
										else
										{
											$tpl->assign('msgerror', $lang_user['smspwka']);
                                            PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") failed! Result: ".$result, PRIO_WARNING, __FILE__, __LINE__);
										}
									}
									else
									{
										$tpl->assign('msgerror', $lang_user['smspwpreiserror']);
									}
								}
								// anbieter tr�gt kosten
								else
								{
									// pr�fen, ob verwendete klasse eingebunden ist - wenn nicht, dann die klasse einbinden
									if(!class_exists('BMHTTP'))
										include(B1GMAIL_DIR . 'serverlib/http.class.php');	
									// pw per sms senden
									// werte�bergabe an den gateway						
									$http = _new('BMHTTP', array($gw));
									// r�ckgabewert abfangen
									$result = $http->DownloadToString();
									
									// die ersten 3 zeichen rausschneiden
									$result = substr($result, 0, 3);
									
									if($result == $gwsuccess)
									{
										// sendezeit der sms beim user updaten
										$userObject->SetPref('smspw_time', $akttime);
										// user-pw updaten
										$db->Query('UPDATE {pre}users SET passwort=?, passwort_salt=? WHERE id = ?', $npw, $saltpw, $userid);
										// erfolgs-msg anzeigen
										$tpl->assign('msgok', $lang_user['smspwresetsuccess']);
										// log-eintrag schreiben
										PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") successfully!", PRIO_PLUGIN, __FILE__, __LINE__);
									}
									else
									{
										$tpl->assign('msgerror', $lang_user['smspwka']);
                                        PutLog("SMS-PW send a new Password via SMS to the User <" . $mail . "> (" . $userid . ") failed! Result: ".$result, PRIO_WARNING, __FILE__, __LINE__);
									}
								}
							}
						}
						// user ist in der reloadsperre
						else
						{
							$tpl->assign('msgerror', $lang_user['smspwreloadsperre']);
						}
					}
					// mehr als einen user mit der eingegebenen handynummer gefunden
					elseif($zae > 1)
					{
						$tpl->assign('msgerror', $lang_user['smspwreseterror']);
					}
					// handynummer wurde im system nicht gefunden
					else
					{
						$tpl->assign('msgerror', $lang_user['smspwresetnotfound']);
					}
				}
			}
			$tpl->assign('smsmsgtxt', $lang_custom['smsmsgtxt']);
			$tpl->assign('smspw', $smspw);
			$tpl->assign('page', 'nli/lostpw.tpl');
			$tpl->assign('domainList', GetDomainList('login'));
			$tpl->assign('invalidFields', array('email_local_pw'));
            $tpl->display('nli/index.tpl');
			exit();
		}
		// blendet sms-pw auch ein, wenn man auf der lostPassword-seite ist
		if($file == 'index.php' && $action == 'lostPassword')
		{
			$tpl->assign('smsmsgtxt', $lang_custom['smsmsgtxt']);
			$tpl->assign('smspw', $smspw);
		}
	}
	
	function genPrivKey($length = 15)
    {       
        $char_control  = "";
        $chars_for_pw  = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        $chars_for_pw .= "23456789";
        $chars_for_pw .= "abcdefghijkmnpqrstuvwxyz";
		
        srand((double) microtime() * 1000000);
        
        for($i=0; $i<$length; $i++)
        {
            $number = rand(0, strlen($chars_for_pw));
            $char_control .= $chars_for_pw[$number];
        }
                    
        return $char_control;
    }
}

$plugins->registerPlugin('SMSPW');

?>