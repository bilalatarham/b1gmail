<?php
class wordpressfeed extends BMPlugin
{
	function wordpressfeed()
	{
		
		$this->name        = 'Wordpress-Feed';
		$this->author      = 'Martin Buchalik';
		$this->web         = 'http://martin-buchalik.de';
		$this->mail        = 'support@martin-buchalik.de';
		$this->version     = '1.0.1';
		$this->designedfor = '7.3.0';
		$this->type        = BMPLUGIN_DEFAULT;
		
		$this->admin_pages = false;
	}
	
	
	
	function Install()
	{
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	
	function Uninstall()
	{
		
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	
	function BeforeDisplayTemplate($resourceName, &$tpl)
	{
		if($_REQUEST['action'] == "login") {
			$tpl->addJSFile("nli", "./clientlib/jquery/jquery-1.8.2.min.js");
			$tpl->addJSFile("nli", "./plugins/js/wordpress-feed.js");
			$tpl->addCSSFile("nli", "./plugins/css/wordpress-feed.css");
		}
	}
	
	
}
/**
 * register plugin
 */
$plugins->registerPlugin('wordpressfeed');
?>