<?php
/**
 * B1GMail
 *
 * E-Mail Sync
 *
 * PHP version 5.6
 *
 * @link        https://www.onesystems.ch
 * @copyright   OneSystems GmbH 2009-2020
 * @author      Michael Kleger <michael.kleger@onesystems.ch>
 * @version     1.1.4
 *
 * @datum       27.03.2018 --> Erstelldatum
 * @update      15.05.2018 --> Kleienre anpassungen
 *              16.05.2018 --> Weisse Seite beim Benutzer löschen behoben
 *              08.06.2018 --> Englische übersetzung durch Webconcept+
 *              05.07.2018 --> CronJob nur noch über CLI
 *              10.07.2018 --> MySQLi Support
 *              02.08.2018 --> Transfer Gruppe
 *              22.09.2018 --> CronJob über CLI
 *              05.11.2018 --> STARTTLS
 *              14.11.2018 --> Provider Vorauswahl Vorbereitungen für Version 1.1.0
 *              04.12.2018 --> Plugin nicht ausgeben wen b1gMailServer nicht vorhanden ist
 *                             SSL / SSL mit Selbst Signiertem Zertifikat und STARTTLS Anmeldung am internen E-Mail Server überarbeitet
 *              13.12.2018 --> Debug Modus
 *              01.02.2019 --> Log Modus
 *              14.02.2019 --> Provider auswahl
 *                             Log Cleanup
 *              04.03.2019 --> Defektes Template
 *              02.06.2019 --> PHP Mcrypt ablösung
 *              22.05.2020 --> Update API angepasst
 */

// MailSync Link automatisch anzeigen
define("MailSync_ACTIVATE", 1);

// MySQLi / MySQL Support
if(!defined('MYSQLI_NUM'))
    define('MYSQLI_NUM',			MYSQL_NUM);
if(!defined('MYSQLI_ASSOC'))
    define('MYSQLI_ASSOC',			MYSQL_ASSOC);
if(!defined('MYSQLI_BOTH'))
    define('MYSQLI_BOTH',			MYSQL_BOTH);

class MailSync extends BMPlugin {
    function MailSync() {
        // Allgemein
        $this->type				    = BMPLUGIN_DEFAULT;
        $this->name				    = 'Mail Sync';
        $this->author               = 'Mail Town E-Mail Service';
        $this->web                  = 'https://mail.town';
        $this->mail                 = 'info@mail.town';
        $this->version			    = '1.1.4';
        $this->designedfor          = '7.4.0';

        // Updates
        $this->website              = 'https://mail.town/produkte/b1gmail/mailsync/';
        $this->update_url 		    = 'https://update.mail.town/';
        $this->update_name          = 'MailSync';

        // Admin
        $this->admin_pages			= true;
        $this->admin_page_title		= 'Mail Sync';
        $this->admin_page_icon		= 'mailsync.png';

        // Gruppen Option
        $this->RegisterGroupOption('mailsync',
            FIELD_CHECKBOX,
            'Mail Sync');

    }

    function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang) {
        global $currentCharset;

        if($lang == 'deutsch') {
            // Prefs
            $_lang_user['ms']                           = 'Mail Umzug';
            $_lang_user['msInfo']                       = 'Alle angegebenen Passw&ouml;rter werden sicher verschl&uuml;sselt in der Datenbank abgelegt und nach Abschluss der Migration wieder gel&ouml;scht.';
            $_lang_user['msLogs']                       = 'Umzugs Log anzeigen';
            $_lang_user['msAnzMails']                   = 'E-Mails';
            $_lang_user['msAnzFolders']                 = 'Ordner';
            $_lang_user['msRefresh']                    = 'Seite wird aktualisiert in ';
            $_lang_user['msSourceTitle']                = 'Bisherigen Anbieter';

            $_lang_user['msProvider']                   = 'E-Mail-Anbieter';
            $_lang_user['msSelectProvider']             = 'Anbieter ausw&auml;hlen';
            $_lang_user['msSelectOwnProvider']          = 'Eigener Provider angeben';
            $_lang_user['msNoProvider']                 = 'Anbieter nicht vorhanden';
            $_lang_user['msServerIP']                   = 'Server / IP';
            $_lang_user['msMailAdresse']                = 'E-Mail-Adresse';
            $_lang_user['msMailServer']                 = 'E-Mail Server';
            $_lang_user['msPasswort']                   = 'Passwort';
            $_lang_user['msPort']                       = 'Port';
            $_lang_user['msCert']                       = 'Verschl&uuml;sselung';
            $_lang_user['msOrdner']                     = 'Ordner';
            $_lang_user['msOrdnerName']                 = 'Kopierte E-Mails';
            $_lang_user['msDatum']                      = 'Datum';
            $_lang_user['msNachricht']                  = 'Nachricht';
            $_lang_user['msBTNLogs']                    = 'Logs';
            $_lang_user['msBTNLoeschen']                = 'L&ouml;schen';
            $_lang_user['msBTNLoeschenInfo']            = 'Eintrag wirklich unwiderruflich entfernen?';
            $_lang_user['msBTNMigration']               = 'Migration hinzuf&uuml;gen';
            $_lang_user['msAktion']                     = 'Aktion';
            $_lang_user['msEncrypted']                  = 'Verschl&uuml;sselte Verbindung';
            $_lang_user['msNotEncrypted']               = 'Keine verschl&uuml;sselte Verbindung';

            $_lang_user['msAddMail']                    = 'E-Mail-Adresse hinzuf&uuml;gen';
            $_lang_user['msSpeichern']                  = 'Speichern';
            $_lang_user['msZuruck']                     = 'Zur&uuml;ck zur Liste';

            $_lang_user['msMSGEMail']                   = 'Bitte eine E-Mail Adresse angeben';
            $_lang_user['msMSGPasswort']                = 'Bitte ein Passwort angeben';
            $_lang_user['msMSGServer']                  = 'Bitte einen E-Mail Server angeben';
            $_lang_user['msMSGbAnbieter']               = 'Anmelden beim bisherigen Anbieter fehlgeschlagen';
            $_lang_user['msMSGnAnbieter']               = 'Anmelden an Ihrem Konto fehlgeschlagen';
            $_lang_user['msMSGStart']                   = 'Auftrag wurde gespeichert und wird demn&auml;chst ausgef&uuml;hrt, es wird ein Status E-Mail an Ihre E-Mail-Adresse verschickt.';
            $_lang_user['msMSGAddJob']                  = 'Neuer Job wurde erfasst';

            $_lang_user['msSyncEnd']                    = 'Synchronisation beendet';
            $_lang_user['msSyncPlan']                   = 'Synchronisation geplant';
            $_lang_user['msSyncOn']                     = 'Synchronisation l&auml;uft';

            $_lang_user['msLogStart']                   = 'Synchronisation gestartet';
            $_lang_user['msLogMigriert']                = 'Mails migriert';
            $_lang_user['msLogEnde']                    = 'Synchronisation beendet';
            $_lang_user['msLogFehlerVerbindung']        = 'Fehler: Keine Verbindung zum alten IMAP Server';

            $_lang_user['prefs_d_ms']                   = 'Migrieren Sie Ihre bestehenden E-Mail-Konten ganz einfach in Ihr Postfach.';
            $_lang_user['ms_add']                       = 'E-Mail-Adresse hinzuf&uuml;gen';

            // Admin
            $_lang_admin['msList']                      = 'Mail Sync';
            $_lang_admin['msInfoUpdate']                = 'Ein Update f&uuml;r das <strong>'. $this->name .'</strong> Plugin mit der Version <strong>%s</strong> steht zu Verf&uuml;gung, bitte aktualisieren Sie das Plugin demn&auml;chst.';
            $_lang_admin['msAnzMails']                  = 'E-Mails';
            $_lang_admin['msAnzFolders']                = 'Ordner';
            $_lang_admin['msInfoDebug']                 = 'MailSync: Das Logging von Debug-Meldungen ist derzeit aktiviert!';
            $_lang_admin['msInfoCronJob']               = 'Der CronJob f&uuml;r das MailSync Plugin wurde nicht ausgef&uuml;hrt!<br />Letzte Ausf&uuml;hrung am: ';
            $_lang_admin['msInfoCronJobAdmin']          = 'F&uuml;r das "Mail Sync" Plugin wird der b1gMailServer / b1gMailServerAdmin vorausgesetzt!';
            $_lang_admin['msAdd']                       = 'Job Hinzuf&uuml;gen';
            $_lang_admin['msLogs']                      = 'Logs';
            $_lang_admin['msJobID']                     = 'Job ID';
            $_lang_admin['msHilfe']                     = 'Hilfe';
            $_lang_admin['msHilfeErweiterungen']        = 'Erweiterungen';
            $_lang_admin['msHilfeErweiterungenBesch']   = 'Folgende Erweiterungen m&uuml;ssen &uuml;ber den <strong>Browser</strong> und auch &uuml;ber die <strong>Konsole (CronJob)</strong> geladen sein damit Mail Sync korrekt funktioniert.';
            $_lang_admin['msSupport']                   = 'Hilfe & Support';
            $_lang_admin['msUserID']                    = 'Benutzer ID';
            $_lang_admin['msUser']                      = 'Benutzer';
            $_lang_admin['msStatus']                    = 'Status';
            $_lang_admin['msDatum']                     = 'Datum';
            $_lang_admin['msActivate']                  = 'Aktivieren';
            $_lang_admin['msName']                      = 'Provider Name';
            $_lang_admin['msServer']                    = 'Server / IP';
            $_lang_admin['msProtokoll']                 = 'Protokoll';
            $_lang_admin['msType']                      = 'Type';
            $_lang_admin['msPort']                      = 'Port';
            $_lang_admin['msCert']                      = 'Zertifikat';
            $_lang_admin['msSMail']                     = 'Bisherige E-Mail-Adresse';
            $_lang_admin['msPasswort']                  = 'Passwort';
            $_lang_admin['msTOrdner']                   = 'Ordner';
            $_lang_admin['msEncrypted']                 = 'Verschl&uuml;sselte Verbindung';
            $_lang_admin['msNotEncrypted']              = 'Keine verschl&uuml;sselte Verbindung';
            $_lang_admin['msSpeichern']                 = 'Speichern';


            $_lang_admin['msProvider']                  = 'Provider';
            $_lang_admin['msTextAdd']                   = 'hinzuf&uuml;gen';

            $_lang_admin['msConfig']                    = 'Einstellungen';
            $_lang_admin['msConfigMSG']                 = 'Einstellungen wurde gespeichert';
            $_lang_admin['msConfigMG']                  = 'Mail Gruppe';
            $_lang_admin['msConfigMGNo']                = 'Keine Tempor&auml;re Sync Gruppe';
            $_lang_admin['msConfigMGInfo']              = 'Tempor&auml;re Sync Gruppe wen IMAP Option fehlt.';
            $_lang_admin['msConfigDebug']               = 'Debugmodus';
            $_lang_admin['msConfigDebugInfo']           = 'Debug-Meldungen loggen?';
            $_lang_admin['msConfigSelfSigned']          = 'Zertifikat';
            $_lang_admin['msConfigSelfSignedInfo']      = 'Selbst Signiertes Zertifikat?';
            $_lang_admin['msConfigSelfSignedBox']       = 'Diese Option sollte wirklich nur dann aktiviert werden wen ein Selbst Signiertes Zertifikat im Einsatz ist!';
            $_lang_admin['msConfigCron']                = 'CronJob';
            $_lang_admin['msConfigCLI']                 = 'Ausf&uuml;hrung';
            $_lang_admin['msConfigCLIInfo']             = 'CronJob wird nur &uuml;ber die CLI ausgef&uuml;hrt';
            $_lang_admin['msConfigCronLast']            = 'Ausf&uuml;hrungszeit';
            $_lang_admin['msConfigSave']                = 'Speichern';

            $_lang_admin['msCopyright']                 = 'Copyright';
            $_lang_admin['msLegende']                   = 'Legende';
            $_lang_admin['msLegendeStatus']             = 'Synchronisationssatus';
            $_lang_admin['msStatusON']                  = 'Synchronisation l&auml;uft';
            $_lang_admin['msStatusONG']                 = 'Synchronisation geplant';
            $_lang_admin['msStatusOFF']                 = 'Synchronisation beendet';
            $_lang_admin['msLastCronJob']               = 'CronJob wurde zuletzt ausgef&uuml;hrt am';

            // Info Mails
            $_lang_custom['notify_ms']	                = '<strong>Mail Umzug</strong><br /> %s';
            $_lang_custom['mailsync_start']             = "Die E-Mail Synchronisation wurde gestartet";
            $_lang_custom['mailsync_notify_start']      = "Die E-Mail Synchronisation wurde gestartet";
            $_lang_custom['mailsync_ende']              = "Die E-Mail Synchronisation wurde beendet \n\n"
                ."In der Ordnerverwaltung finden Sie nun Ihre Migrierten E-Mail Ordner die Sie zuerst in der Spalte Abonniert aktivieren m&uuml;ssen. \n"
                ."Nun k&ouml;nnen Sie die E-Mails an den richtigen Ort verschieben und die Migrations Ordner l&ouml;schen. \n\n"
                ."Das wichtigste zum Schluss: \n"
                ."F&uuml;r die Migration haben wir Ihre Passw&ouml;rter verschl&uuml;sselt zwischengespeichert damit wir Ihre Mails umziehen k&ouml;nnen, bitte &auml;ndern Sie zur Sicherheit als letztes noch Ihr E-Mail Passwort.";
            $_lang_custom['mailsync_notify_ende']       = "Die E-Mail Synchronisation wurde beendet";
            $_lang_custom['mailsync_betreff']           = "[Status] Mail Umzug";
            $_lang_custom['mailsync_msg']		        = "Sehr geehrte Damen und Herren, \n\n"
                . "%%msg%% \n\n"
                . "(Diese E-Mail wurde automatisch erstellt!)";
        } else {
            // Prefs
            $_lang_user['ms']                           = 'Email Migration';
            $_lang_user['msInfo']                       = 'All specified passwords are securely encrypted and stored in the database and are recovered after the migration is complete.';
            $_lang_user['msLogs']                       = 'Show migration log';
            $_lang_user['msAnzMails']                   = 'E-Mails';
            $_lang_user['msAnzFolders']                 = 'Folders';
            $_lang_user['msRefresh']                    = 'Page is being updated in ';
            $_lang_user['msSourceTitle']                = 'Previous provider';

            $_lang_user['msProvider']                   = 'E-mail provider';
            $_lang_user['msSelectProvider']             = 'Select provider';
            $_lang_user['msSelectOwnProvider']          = 'Specify your own provider';
            $_lang_user['msNoProvider']                 = 'Provider not available';
            $_lang_user['msServerIP']                   = 'Server / IP';
            $_lang_user['msMailAdresse']                = 'Email address';
            $_lang_user['msMailServer']                 = 'Mail Server';
            $_lang_user['msPasswort']                   = 'Password';
            $_lang_user['msPort']                       = 'Port';
            $_lang_user['msCert']                       = 'Encryption';
            $_lang_user['msOrdner']                     = 'Folder';
            $_lang_user['msOrdnerName']                 = 'Migrated emails';
            $_lang_user['msDatum']                      = 'Date';
            $_lang_user['msNachricht']                  = 'Message';
            $_lang_user['msBTNLogs']                    = 'Logs';
            $_lang_user['msBTNLoeschen']                = 'Delete';
            $_lang_user['msBTNLoeschenInfo']            = 'Remove entry irrevocably?';
            $_lang_user['msBTNMigration']               = 'Add migration';
            $_lang_user['msAktion']                     = 'Action';
            $_lang_user['msEncrypted']                  = 'Encrypted connection';
            $_lang_user['msNotEncrypted']               = 'No encrypted connection';

            $_lang_user['msAddMail']                    = 'Add email address';
            $_lang_user['msSpeichern']                  = 'Save';
            $_lang_user['msZuruck']                     = 'Back to list';

            $_lang_user['msMSGEMail']                   = 'Please enter an email address';
            $_lang_user['msMSGPasswort']                = 'Please enter a password';
            $_lang_user['msMSGServer']                  = 'Please specify an email server';
            $_lang_user['msMSGbAnbieter']               = 'Logon to previous provider failed';
            $_lang_user['msMSGnAnbieter']               = 'Logon to your account failed';
            $_lang_user['msMSMSGStart']                 = 'Job has been saved and will be executed shortly, a status email will be sent to your email address.';
            $_lang_user['msMSMSGAddJob']                = 'New job has been created';

            $_lang_user['msSyncEnd']                    = 'Migration finished';
            $_lang_user['msSyncPlan']                   = 'Migration scheduled';
            $_lang_user['msSyncOn']                     = 'Migration running';

            $_lang_user['msLogStart']                   = 'Migration started';
            $_lang_user['msLogMigriert']                = 'Emails migrated';
            $_lang_user['msLogEnde']                    = 'Migration finished';
            $_lang_user['msLogFehlerVerbindung']        = 'Error: No connection to old IMAP server';

            $_lang_user['prefs_d_ms']                   = 'Easily migrate your old email accounts to your mailbox.';
            $_lang_user['ms_add']                       = 'Add email address';

            // Admin
            $_lang_admin['msList']                      = 'Mail Sync';
            $_lang_admin['msInfoUpdate']                = 'An update for the <strong>'. $this->name .'</strong> plugin with the version <strong>%s</strong> is available, please update the plugin soon.';
            $_lang_admin['msAnzMails']                  = 'E-Mails';
            $_lang_admin['msAnzFolders']                = 'Folders';
            $_lang_admin['msInfoDebug']                 = 'MailSync: The logging of debug messages is currently activated!';
            $_lang_admin['msInfoCronJob']               = 'The CronJob for the MailSync plugin was not executed!<br />Last execution on: ';
            $_lang_admin['msInfoCronJobAdmin']          = 'For the "Mail Sync" plugin is the b1gMailServer / b1gMailServerAdmin required!';
            $_lang_admin['msAdd']                       = 'Add job';
            $_lang_admin['msLogs']                      = 'Logs';
            $_lang_admin['msJobID']                     = 'Job ID';
            $_lang_admin['msHilfe']                     = 'Help';
            $_lang_admin['msHilfeErweiterungen']        = 'Extensions';
            $_lang_admin['msHilfeErweiterungenBesch']   = 'The following extensions must be loaded via the <strong> Browser </strong> and also via the <strong> Console (CronJob) </strong> for Mail Sync to work properly.';
            $_lang_admin['msSupport']                   = 'Help & Support';
            $_lang_admin['msUserID']                    = 'User ID';
            $_lang_admin['msUser']                      = 'User';
            $_lang_admin['msStatus']                    = 'Status';
            $_lang_admin['msDatum']                     = 'Date';
            $_lang_admin['msActivate']                  = 'Activate';
            $_lang_admin['msName']                      = 'Provider name';
            $_lang_admin['msServer']                    = 'Server / IP';
            $_lang_admin['msProtokoll']                 = 'Protocol';
            $_lang_admin['msType']                      = 'Type';
            $_lang_admin['msPort']                      = 'Port';
            $_lang_admin['msCert']                      = 'Certificate';
            $_lang_admin['msSMail']                     = 'Previous email address';
            $_lang_admin['msPasswort']                  = 'Password';
            $_lang_admin['msTOrdner']                   = 'Folder';
            $_lang_admin['msEncrypted']                 = 'Encrypted connection';
            $_lang_admin['msNotEncrypted']              = 'No encrypted connection';
            $_lang_admin['msSpeichern']                 = 'Save';

            $_lang_admin['msConfig']                    = 'Settings';
            $_lang_admin['msConfigMSG']                 = 'Settings has been saved';
            $_lang_admin['msConfigMG']                  = 'Mail group';
            $_lang_admin['msConfigMGNo']                = 'No temporal sync group';
            $_lang_admin['msConfigMGInfo']              = 'Temporal Sync Group whom IMAP option is missing.';
            $_lang_admin['msConfigDebug']               = 'Debug mode';
            $_lang_admin['msConfigDebugInfo']           = 'Log debug messages?';
            $_lang_admin['msConfigSelfSigned']          = 'Certificate';
            $_lang_admin['msConfigSelfSignedInfo']      = 'Self-signed certificate?';
            $_lang_admin['msConfigSelfSignedBox']       = 'This option should only be activated if a self-signed certificate is in use!';
            $_lang_admin['msConfigCron']                = 'CronJob';
            $_lang_admin['msConfigCLI']                 = 'Execution';
            $_lang_admin['msConfigCLIInfo']             = 'CronJob is executed via the CLI only';
            $_lang_admin['msConfigCronLast']            = 'Execution time';
            $_lang_admin['msConfigSave']                = 'Save';

            $_lang_admin['msCopyright']                 = 'Copyright';
            $_lang_admin['msLegende']                   = 'Legend';
            $_lang_admin['msLegendeStatus']             = 'Synchronization Satus';
            $_lang_admin['msStatusON']                  = 'Migration running';
            $_lang_admin['msStatusONG']                 = 'Migration planned';
            $_lang_admin['msStatusOFF']                 = 'Migration finished';
            $_lang_admin['msLastCronJob']               = 'CronJob was last executed on';

            // Info Mails
            $_lang_custom['notify_ms']	                = '<strong>Email Migration</strong><br /> %s';
            $_lang_custom['mailsync_start']             = "Email migration started";
            $_lang_custom['mailsync_notify_start']      = "Email migration started";
            $_lang_custom['mailsync_ende']              = "Email migration finished. \n\n"
                ."In the folder management you will now find your migrated email folders which you must first activate in the Subscribed column. \n"
                ."Now you can move the emails to the right place and the migration folders. \n\n"
                ."The most important at the end: \n"
                ."For the migration, we encrypted your passwords so that we could move your emails. Please change your passwords for security reasons!";
            $_lang_custom['mailsync_notify_ende']       = "Email migration finished.";
            $_lang_custom['mailsync_betreff']           = "[Status] Email Migration";
            $_lang_custom['mailsync_msg']		        = "Dear Ladies and Gentlemen, \n\n"
                . "%%msg%% \n\n"
                . "(This email was created automatically, please do not reply!)";
        }

        // convert charset
        global $currentCharset;
        $arrays = array('admin', 'client', 'user', 'custom');
        foreach($arrays as $array)
        {
            $destArray = sprintf('lang_%s', $array);
            $srcArray  = '_' . $destArray;

            if(!isset($$srcArray))
                continue;

            foreach($$srcArray as $key=>$val)
            {
                if(function_exists('CharsetDecode') && !in_array(strtolower($currentCharset), array('iso-8859-1', 'iso-8859-15')))
                    $val = CharsetDecode($val, 'iso-8859-15');
                ${$destArray}[$key] = $val;
            }
        }
    }

    function Install() {
        global $db, $bm_prefs;

        $CronDate = date('Y-m-d H:i:s');

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_mailsync` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `UserID` int(11) DEFAULT NULL,
                      `Status` int(11) DEFAULT NULL,
                      `AnzMails` int(11) NOT NULL DEFAULT '0',
                      `AnzFolders` int(11) NOT NULL DEFAULT '0',
                      `JobDate` datetime DEFAULT NULL,
                      `SourceServer` varchar(200) DEFAULT NULL,
                      `SourceType` varchar(200) DEFAULT NULL,
                      `SourcePort` varchar(200) DEFAULT NULL,
                      `SourceCert` varchar(200) DEFAULT NULL,
                      `SourceMail` varchar(200) DEFAULT NULL,
                      `SourcePW` varchar(200) DEFAULT NULL,
                      `TargetMail` varchar(200) DEFAULT NULL,
                      `TargetPW` varchar(200) DEFAULT NULL,
                      `TargetFolder` varchar(200) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        // Update 1.0.9 Anzahl
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_mailsync` LIKE ?', 'AnzMails');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_mailsync` ADD `AnzMails` int(11) NOT NULL DEFAULT '0' AFTER Status;");
            $db->Query("ALTER TABLE `{pre}mod_mailsync` ADD `AnzFolders` int(11) NOT NULL DEFAULT '0' AFTER AnzMails;");
        }
        $res->Free();

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_mailsync_provider` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Status` enum('yes','no') NOT NULL DEFAULT 'no',
                      `Name` varchar(100) DEFAULT NULL,
                      `Server` varchar(100) DEFAULT NULL,
                      `Type` varchar(10) DEFAULT 'imap',
                      `Port` int(5) DEFAULT '143',
                      `Cert` varchar(3) DEFAULT 'tls',
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query('INSERT IGNORE INTO `{pre}mod_mailsync_provider` (`ID`, `Status`, `Name`, `Server`, `Type`, `Port`, `Cert`) VALUES
                    (1, \'yes\', \'Outlook\', \'imap-mail.outlook.com\', \'imap\', 993, \'ssl\'),
                    (2, \'yes\', \'GMail\', \'imap.gmail.com\', \'imap\', 993, \'ssl\'),
                    (3, \'yes\', \'GMX\', \'imap.gmx.net\', \'imap\', 993, \'ssl\'),
                    (4, \'yes\', \'Web.de\', \'imap.web.de\', \'imap\', 993, \'ssl\'),
                    (5, \'yes\', \'Swisscom / Bluewin\', \'imaps.bluewin.ch\', \'imap\', 993, \'ssl\');');


        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_mailsync_conf` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `LicenseKey` varchar(100),
                      `PWDMethod` varchar(100),
                      `PWDKey` varchar(100),
                      `PWDIv` varchar(100),
                      `Debug` enum('yes','no') NOT NULL DEFAULT 'no',
                      `SyncGroup` int(11) DEFAULT NULL,
                      `SelfSignedCert` enum('yes','no') NOT NULL DEFAULT 'no',
                      `CronOverCLI` enum('yes','no') NOT NULL DEFAULT 'yes',
                      `LastCronJob` datetime DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");
        $db->Query('INSERT IGNORE INTO `{pre}mod_mailsync_conf` (`ID`, `PWDMethod`, `PWDKey`, `PWDIv`, `LastCronJob`) VALUES (?,?,?,?,?)',
                1,
                'AES-256-CBC',
                hash('sha256', time().'key'),
                hash('sha256', time().'iv'),
                $CronDate);

        // Update 1.0.9 Self Signed Zertifikat
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_mailsync_conf` LIKE ?', 'Debug');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_mailsync_conf` ADD `Debug` enum('yes','no') NOT NULL DEFAULT 'no' AFTER ID;");
            $db->Query("ALTER TABLE `{pre}mod_mailsync_conf` ADD `SelfSignedCert` enum('yes','no') NOT NULL DEFAULT 'no' AFTER SyncGroup;");
        }
        $res->Free();

        // Update 1.1.2 Lizenz, PWCrpyt
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_mailsync_conf` LIKE ?', 'LicenseKey');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_mailsync_conf` ADD `LicenseKey` varchar(100) AFTER ID;");
            $db->Query("ALTER TABLE `{pre}mod_mailsync_conf` ADD `PWDMethod` varchar(100) AFTER LicenseKey;");
            $db->Query("ALTER TABLE `{pre}mod_mailsync_conf` ADD `PWDKey` varchar(100) AFTER PWDMethod;");
            $db->Query("ALTER TABLE `{pre}mod_mailsync_conf` ADD `PWDIv` varchar(100) AFTER PWDKey;");

            $db->Query('UPDATE {pre}mod_mailsync_conf SET `PWDMethod`=?, `PWDKey`=?, `PWDIv`=? WHERE `ID`=?',
                'AES-256-CBC',
                hash('sha256', time().'key'),
                hash('sha256', time().'iv'),
                1);
        }
        $res->Free();

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_mailsync_logs` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `UserID` int(11) DEFAULT NULL,
                      `JobID` int(11) DEFAULT NULL,
                      `LogDate` datetime DEFAULT NULL,
                      `Status` enum('ok','info','error') NOT NULL DEFAULT 'info',
                      `Nachricht` text NOT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        // Update 1.0.9 Logs
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_mailsync_logs` LIKE ?', 'Status');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_mailsync_logs` ADD `Status` enum('ok','info','error') NOT NULL DEFAULT 'info' AFTER LogDate;");
        }
        $res->Free();

        // Update Info
        $UpdateInfo = array(
            'internalName'  => $this->update_name,
            'LicNr'         => B1GMAIL_LICNR,
            'LicDom'        => B1GMAIL_LICDOMAIN,
            'DlDate'        => B1GMAIL_DLDATE,
            'SystemName'    => $bm_prefs['titel'],
            'SystemUrl'     => $bm_prefs['selfurl'],
            'MTAHost'       => $bm_prefs['b1gmta_host'],
            'IP'            => $_SERVER['SERVER_ADDR'],
            'PluginVersion' => $this->version,
            'SystemVersion' => $this->designedfor,
            'PHPVersion'    => phpversion()
        );

        $ch = curl_init($this->update_url .'index.php?action=updateVersion');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($UpdateInfo));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length: ' . strlen(json_encode($UpdateInfo))));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        PutLog($this->name. ' Version ' . $this->version . ' was successfull installed!', PRIO_PLUGIN, __FILE__, __LINE__);
        return(true);
    }

    function Uninstall() {
        global $db, $bm_prefs;

        // Lizenz
        unlink($bm_prefs['selffolder'] .'mailsync.license.txt');

        // DataFolder
        $files = glob($bm_prefs['datafolder'] . 'mailsync/*');
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }
        rmdir($bm_prefs['datafolder'] . 'mailsync');

        $db->Query('DROP TABLE IF EXISTS {pre}mod_mailsync');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_mailsync_conf');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_mailsync_logs');

        PutLog($this->name. ' Version ' . $this->version . ' was successfull removed!', PRIO_PLUGIN, __FILE__, __LINE__);
        return(true);
    }


    /**
     * Admin Hinweis
     */
    function getNotices() {
        global $db, $lang_admin;
        $notices = array();

        // Plugin Update
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->update_url .'index.php?action=getLatestVersion&internalName='. $this->update_name .'&b1gMailVersion='. $this->designedfor);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        $queryResult = unserialize(curl_exec($ch));
        curl_close($ch);
        if($this->IsVersionNewer($queryResult['latestVersion'], $this->version)) {
            $notices[] = array(
                'type' => 'update',
                'text' => sprintf($lang_admin['msInfoUpdate'], $queryResult['latestVersion']));
        }

        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {
            $res = $db->Query("SELECT `Debug`, `LastCronJob` FROM {pre}mod_mailsync_conf WHERE ID = ?", 1);
            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                $msDebug        = $row['Debug'];
                $msLastCronJob  = $row['LastCronJob'];
            }
            $res->Free();

            // Aktuelle Zeit
            $dsAktDate = date("Y-m-d H:i:s", strtotime("-1 hours", time()));

            // Debug
            if ($msDebug == 'yes') {
                $notices[] = array(
                    'type' => 'info',
                    'text' => $lang_admin['msInfoDebug'],
                    'link' => $this->_adminLink() . '&do=conf&');
            }

            // CronJob
            if ($msLastCronJob < $dsAktDate) {
                $notices[] = array(
                    'type' => 'error',
                    'text' => $lang_admin['msInfoCronJob'] . date('d.m.Y H:i:s', strtotime($msLastCronJob)),
                    'link' => $this->_adminLink() . '&do=conf&');
            }
        } else {
            $notices[] = array('type' => 'error',
                'text' => $lang_admin['msInfoCronJobAdmin'],
                'link' => $this->_adminLink() . '&do=hilfe&');
        }

        return ($notices);
    }


    /**
     * Passwort Crypt / Decrypt
     */
    function encrypt_decrypt($action, $string) {
        $output = false;
        $MSConfig = $this->_getMSInfos();

        $iv = substr($MSConfig['PWDIv'], 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $MSConfig['PWDMethod'], $MSConfig['PWDKey'], 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $MSConfig['PWDMethod'], $MSConfig['PWDKey'], 0, $iv);
        }

        return $output;
    }


    /**
     * Login
     */
    public function _MailLogin($userid, $server, $port, $type, $sec=NULL, $username, $password) {
        global $db;

        $MSConfig   = $this->_getMSInfos();

        // Zertifikat
        if ($sec !== '') {
            $sec = "/". $sec;
        } else {
            $sec = NULL;
        }

        // Benutzer aktualisieren
        $UserGroup      = $this->_getUserGroup($userid);
        $IMAPOption     = $this->ShowGroupIMAPOption($UserGroup);

        if ($MSConfig['SyncGroup'] > 0 && $IMAPOption == 'no') {
            $db->Query('UPDATE {pre}users SET `gruppe`=? WHERE `ID`=?',
                $MSConfig['SyncGroup'],
                $userid);
        }

        // Login
        $mbox = @imap_open("{". $server .":". $port ."/". $type . $sec ."}", $username, $password);
        if ($MSConfig['Debug'] == 'yes') {
            foreach (imap_errors() as $error) {
                PutLog($this->name .' Host: "'. $server .':'. $port .'/'. $type . $sec .'",  IMAP Error: "'. $error .'"', '4');
            }
        }
        $check = imap_check($mbox);

        // Benutzer aktualisieren
        if ($MSConfig['SyncGroup'] > 0 && $IMAPOption == 'no') {
            $db->Query('UPDATE {pre}users SET `gruppe`=? WHERE `ID`=?',
                $UserGroup,
                $userid);
        }

        if ($check == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
        imap_close($mbox);
    }


    /**
     * Logs
     */
    function _Logs($userid, $job, $msg, $status = 'info') {
        global $db;

        $MSConfig = $this->_getMSInfos();
        if ($MSConfig['Debug'] == 'yes') {
            if ($status == 'info') {
                $logstatus = 'PRIO_NOTE';
            } else if ($status == 'warning') {
                $logstatus = 'PRIO_WARNING';
            } else if ($status == 'error') {
                $logstatus = 'PRIO_ERROR';
            }

            PutLog($this->name .' User ID: "'. $userid .'", Job ID: "'. $job .'", Message: "'. $msg .'"', $logstatus);
        }

        $db->Query('INSERT INTO {pre}mod_mailsync_logs (`UserID`,`JobID`,`LogDate`,`Status`,`Nachricht`) VALUES(?,?,?,?,?)',
            $userid,
            $job,
            date('Y-m-d H:i:s'),
            $status,
            $msg);
    }


    /**
     * Update Anzahl
     */
    function _updateAnzahl($id, $anz, $table) {
        global $db;

        $db->Query('UPDATE {pre}mod_mailsync SET `'. $table .'`=? WHERE `ID`=?',
            $anz,
            (int)$id);
    }


    /**
     * System Informationen
     */
    function _getMSInfos() {
        global $db;

        $result = array();
        $res = $db->Query('SELECT * FROM {pre}mod_mailsync_conf');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $result = $row;
        }
        $res->Free();

        return($result);
    }

    function _getSInfos() {
        global $db;

        $result = array();
        $res = $db->Query('SELECT * FROM {pre}prefs');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $result = $row;
        }
        $res->Free();

        return($result);
    }

    function _getMBSInfos() {
        global $db;

        $result = array();
        $res = $db->Query('SELECT * FROM {pre}bms_prefs');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $result = $row;
        }
        $res->Free();

        return($result);
    }

    function _getMailAdress($id) {
        global $db;

        $res = $db->Query("SELECT email FROM {pre}users WHERE id = ?",
            $id);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $result = $row['email'];
        }
        $res->Free();

        if (isset($result)) {
            return $result .' ('. $id .')';
        } else {
            return 'Unknown ('. $id .')';
        }
    }

    function _getUserGroup($userid) {
        global $db;

        $resuser = $db->Query('SELECT `gruppe` FROM {pre}users WHERE id=? LIMIT 1', $userid);
        while($row = $resuser->FetchArray(MYSQLI_ASSOC)) {
            return $row['gruppe'];
        }
        $resuser->Free();
    }

    function _getJobExist($id=NULL) {
        global $db;

        $jobid = 0;

        $resjob = $db->Query('SELECT `ID` FROM {pre}mod_mailsync WHERE ID = ?', $id);
        while($row = $resjob->FetchArray(MYSQLI_ASSOC)) {
            $jobid = $row['ID'];
        }
        $resjob->Free();

        return $jobid;
    }

    function _getProviderList($id=NULL) {
        global $db;

        $provList = NULL;
        if (isset($id)) {
            $res = $db->Query('SELECT * FROM {pre}mod_mailsync_provider WHERE ID =? ORDER BY Name', $id);
        } else {
            $res = $db->Query('SELECT * FROM {pre}mod_mailsync_provider WHERE Status =? ORDER BY Name', 'yes');
        }

        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $provList[$row['ID']] = array(
                "ID"            => $row['ID'],
                "Name"          => $row['Name'],
                "Server"        => $row['Server'],
                "Type"          => $row['Type'],
                "Port"          => $row['Port'],
                "Cert"          => $row['Cert']
            );
        }
        $res->Free();

        if (is_array($provList)) {
            return $provList;
        }
    }


    /**
     * Info E-Mail verschicken
     */
    function sendInfoMail($To, $msg) {
        global $bm_prefs, $lang_custom;

        $vars = array('msg' => $msg);
        SystemMail($bm_prefs['notify_to'], trim($To), $lang_custom['mailsync_betreff'], 'mailsync_msg', $vars, 0);
    }


    /**
     * Migrations Ordner
     */
    function MigratedFolders($jobFile, $Folder){
        @file_put_contents($jobFile, $Folder . "\n", FILE_APPEND);
    }


    /**
     * IMAP Option
     */
    function ShowGroupIMAPOption($gid) {
        global $db;

        $rescon = $db->Query('SELECT * FROM {pre}gruppen WHERE id =? LIMIT 1', $gid);
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            return $row['imap'];
        }
        $rescon->Free();
    }


    /**
     * B1GMail Module
     */
    function _BMModuleExists($module=NULL) {
        global $db;

        $res = $db->Query('SELECT `installed` FROM {pre}mods WHERE modname =? AND installed =?',
            $module,
            1);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            return $row['installed'];
        }
        $res->Free();

    }

    function _ConvertTOUTF8($text) {
        return html_entity_decode(mb_convert_encoding($text, 'UTF-8', 'auto'), ENT_QUOTES, 'UTF-8');
    }


    /**
     * Benutzer cleanup
     */
    function OnDeleteUser($userID) {
        global $db;

        $db->Query('DELETE FROM {pre}mod_mailsync WHERE UserID='. $userID);
        $db->Query('DELETE FROM {pre}mod_mailsync_logs WHERE UserID='. $userID);
    }


    /**
     * CronJob
     */
    function OnCron() {
        global $db, $sinfo, $lang_custom, $lang_user;

        $MSConfig = $this->_getMSInfos();

        // Cleanup
        $akt            = date("Y-m-d H:i", time());
        $cleanmonth     = date('Y-m-d H:i', strtotime($akt . ' -2 month'));

        $reslog = $db->Query('SELECT `JobID` FROM {pre}mod_mailsync_logs WHERE LogDate <= ?', $cleanmonth);
        while($row = $reslog->FetchArray(MYSQLI_ASSOC)) {
            $jobExist = $this->_getJobExist($row['JobID']);
            if ($jobExist == 0) {
                $db->Query('DELETE FROM {pre}mod_mailsync_logs WHERE `JobID`=?',
                    (int)$row['JobID']);
            }
        }
        $reslog->Free();


        // Jobs ausführen
        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {
            $sinfo      = $this->_getSInfos();
            $bmsinfo    = $this->_getMBSInfos();

            // Konfiguration
            $SyncGroup      = $MSConfig['SyncGroup'];
            $CronOverCLI    = $MSConfig['CronOverCLI'];

            // Prüfen ob cron.php nicht über den Browser gestartet wird
            if (($CronOverCLI == 'yes' && php_sapi_name() == 'cli') || $CronOverCLI == 'no') {

                // Debug
                if ($MSConfig['Debug'] == 'yes') {
                    PutLog($this->name .' Cron Start over "'. strtoupper(php_sapi_name()) .'"');
                }
                // Cron Zeit
                $db->Query('UPDATE {pre}mod_mailsync_conf SET `LastCronJob`=? WHERE `ID`=?',
                    date('Y-m-d H:i:s'),
                    1);


                // Job Ordner erstellen
                $SyncDataFolder = $sinfo['datafolder'] . 'mailsync';
                if (!file_exists($SyncDataFolder)) {
                    mkdir($SyncDataFolder, 0777, true);
                }

                // CronJob Starten
                $cronres = $db->Query('SELECT * FROM {pre}mod_mailsync WHERE Status = ?',
                    '1');
                while ($row = $cronres->FetchArray(MYSQLI_ASSOC)) {

                    $AnzMails   = 0;
                    $AnzFolders = 0;

                    $UserID     = $row['UserID'];
                    $JobID      = $row['ID'];

                    // Debug
                    if ($MSConfig['Debug'] == 'yes') {
                        PutLog($this->name .' Job "'. $JobID .'" Start for User ID "'. $UserID .'"');
                    }

                    // Benutzer aktualisieren
                    $UserGroup = $this->_getUserGroup($UserID);
                    $IMAPOption = $this->ShowGroupIMAPOption($UserGroup);
                    if ($SyncGroup > 0 && $IMAPOption == 'no') {
                        $db->Query('UPDATE {pre}users SET `gruppe`=? WHERE `ID`=?',
                            $SyncGroup,
                            $UserID);

                        // Debug
                        if ($MSConfig['Debug'] == 'yes') {
                            PutLog($this->name .' User ID "'. $UserID .'" move in Sync Group "'. $SyncGroup .'"');
                        }
                    }

                    // Status aktualisieren
                    $db->Query('UPDATE {pre}mod_mailsync SET `Status`=? WHERE `ID`=?',
                        '2',
                        (int)$JobID);

                    // Debug
                    if ($MSConfig['Debug'] == 'yes') {
                        PutLog($this->name .' Job "'. $JobID .'" Sync Start');
                    }

                    $migratedfolders = array();

                    if (isset($row['SourceCert'])) {
			if($row['SourceCert']=="") {
				$SourceCert = NULL;
			} else {
	                        $SourceCert = "/" . $row['SourceCert'];
			}
                    } else {
                        $SourceCert = NULL;
                    }

                    // Zertifikat prüfen
                    if ($bmsinfo['user_imapssl'] == 1 && $bmsinfo['user_imapport'] == '143') {
                        if ($MSConfig['SelfSignedCert'] == 'yes') {
                            $usessl = '/novalidate-cert';
                        } else {
                            $usessl = '/tls';
                        }
                    } else if ($bmsinfo['user_imapssl'] == 1) {
                        if ($MSConfig['SelfSignedCert'] == 'yes') {
                            $usessl = '/novalidate-cert';
                        } else {
                            $usessl = '/ssl';
                        }
                    } else {
                        if ($MSConfig['SelfSignedCert'] == 'yes') {
                            $usessl = '/novalidate-cert';
                        } else {
                            $usessl = '';
                        }
                    }

                    $SourceCon = "{" . $row['SourceServer'] . ":" . $row['SourcePort'] . "/" . $row['SourceType'] . $SourceCert . "}";
                    $TargetCon = "{" . $bmsinfo['user_imapserver'] . ":" . $bmsinfo['user_imapport'] . "/imap" . $usessl . "}";

                    $SourcePW = $this->encrypt_decrypt('decrypt', $row['SourcePW']);
                    $SourceIMAP = imap_open($SourceCon, $row['SourceMail'], $SourcePW) or die("can't connect: " . imap_last_error());
                    $SourceFolders = imap_list($SourceIMAP, $SourceCon, "*");

                    imap_close($SourceIMAP);

                    // Benachrichtigung
                    if (B1GMAIL_VERSION == '7.4.0') {
                        $db->Query('INSERT INTO {pre}notifications (`userid`,`date`,`flags`,`text_phrase`,`text_params`,`link`,`icon`,`class`) VALUES(?,?,?,?,?,?,?,?)',
                            $UserID,
                            time(),
                            1,
                            'notify_ms',
                            $lang_custom['mailsync_notify_start'],
                            'prefs.php?action=ms&',
                            'plugins/templates/images/mailsync.png',
                            '::MailSync');
                        $this->sendInfoMail($row['TargetMail'], $lang_custom['mailsync_start']);
                    } else {
                        $this->sendInfoMail($row['TargetMail'], $lang_custom['mailsync_start']);
                    }

                    $this->_Logs($UserID, $JobID, $lang_user['msLogStart']);
		    $this->_Logs($UserID, $JobID, "blabl");
                    if ($SourceFolders !== false) {

                        foreach ($SourceFolders as $value) {
                            $FolderPath = str_replace($SourceCon, "", $value);

                            // Überprüfe ob Migration noch nicht abgeschlossen wurde
                            if (is_file($SyncDataFolder . '/' . $JobID)) {
                                $migratedfolders = explode("\n", file_get_contents($SyncDataFolder . '/' . $JobID));
                            }

                            // Verbindung aufbauen
                            $TargetPW = $this->encrypt_decrypt('decrypt', $row['TargetPW']);
                            $TargetIMAP = imap_open($TargetCon . $row['TargetFolder'], $row['TargetMail'], $TargetPW)
                            or die("can't connect: " . imap_last_error());

                            // Erstelle Migrations Ordner
                            if (!imap_createmailbox($TargetIMAP, mb_convert_encoding($TargetCon . $row['TargetFolder'], "UTF7-IMAP", "ISO-8859-1") . '/')) {
                                // Ordner existiert
                            }
                            imap_close($TargetIMAP);

                            if (!in_array($FolderPath, $migratedfolders)) {
                                // Migrations Ordner löschen
                                $targetpath = explode("/", $FolderPath);
                                array_pop($targetpath);
                                $targetroot = mb_convert_encoding(implode("/", $targetpath), "UTF7-IMAP", "ISO-8859-1");

                                // Verbindung aufbauen
                                $TargetPW = $this->encrypt_decrypt('decrypt', $row['TargetPW']);
                                $TargetIMAP = imap_open($TargetCon . $row['TargetFolder'] . $targetroot, $row['TargetMail'], $TargetPW) or die("can't connect: " . imap_last_error());

                                // Migrations Ordner erstellen
                                if (!imap_createmailbox($TargetIMAP, $this->_ConvertTOUTF8($TargetCon . $row['TargetFolder'] . '/' . $FolderPath) .'/')) {
                                    // Ordner existiert
                                }

                                // Verbindung beenden
                                imap_close($TargetIMAP);

                                // Verbindung aufbauen
                                $SourcePW = $this->encrypt_decrypt('decrypt', $row['SourcePW']);
                                $TargetPW = $this->encrypt_decrypt('decrypt', $row['TargetPW']);
                                $SourceIMAP = imap_open($SourceCon . $FolderPath, $row['SourceMail'], $SourcePW) or die("can't connect: " . imap_last_error());
                                $TargetIMAP = imap_open($this->_ConvertTOUTF8($TargetCon . $row['TargetFolder'] . '/' . $FolderPath), $row['TargetMail'], $TargetPW) or die("can't connect: " . imap_last_error());

                                // Mailbox informationen
                                $MC = imap_check($SourceIMAP);

                                // E-Mails kopieren
                                $result = imap_fetch_overview($SourceIMAP, "1:{$MC->Nmsgs}", 0);

                                foreach ($result as $overview) {
                                    $message = imap_fetchheader($SourceIMAP, $overview->msgno) . imap_body($SourceIMAP, $overview->msgno);
                                    if (imap_append($TargetIMAP, $this->_ConvertTOUTF8($TargetCon . $row['TargetFolder'] . '/' . $FolderPath), $message, "\\Seen")) {
                                        $this->_updateAnzahl($JobID, $AnzMails++, 'AnzMails');
                                    } else {
                                        $this->_Logs($UserID, $JobID, "Fehler: Kann E-Mail " . $overview->subject . " von " . $overview->date . " nicht in Ordner ". $this->_ConvertTOUTF8($FolderPath) ." kopieren", "error");
                                    }
                                }

                                // Update
                                $this->_updateAnzahl($JobID, $AnzFolders++, 'AnzFolders');

                                // Log erstellen
                                $this->_Logs($UserID, $JobID, $this->_ConvertTOUTF8($FolderPath) ." ". $MC->Nmsgs .' '. $lang_user['msLogMigriert']);

                                // Verbindung beenden
                                imap_close($SourceIMAP);
                                imap_close($TargetIMAP);

                                // Status speichern
                                $this->MigratedFolders($SyncDataFolder . '/' . $JobID, $row['TargetFolder'] . '/' . $FolderPath);
                            }
                        }

                        // Migration beenden
                        if (file_exists($SyncDataFolder . '/' . $JobID)) {
                            unlink($SyncDataFolder . '/' . $JobID);
                        }

                        // Benutzer Gruppe zurücksetzen
                        if ($SyncGroup > 0 && $IMAPOption == 'no') {
                            $db->Query('UPDATE {pre}users SET `gruppe`=? WHERE `ID`=?',
                                $UserGroup,
                                $UserID);
                        }

                        $db->Query('UPDATE {pre}mod_mailsync SET `Status`=?, `SourcePW`=?, `TargetPW`=?  WHERE `ID`=?',
                            '0',
                            '',
                            '',
                            (int)$JobID);

                        // Benachrichtigung
                        if (B1GMAIL_VERSION == '7.4.0') {
                            $db->Query('INSERT INTO {pre}notifications (`userid`,`date`,`flags`,`text_phrase`,`text_params`,`link`,`icon`,`class`) VALUES(?,?,?,?,?,?,?,?)',
                                $UserID,
                                time(),
                                1,
                                'notify_ms',
                                $lang_custom['mailsync_notify_ende'],
                                'prefs.php?action=ms&',
                                'plugins/templates/images/mailsync.png',
                                '::MailSync');
                            $this->sendInfoMail($row['TargetMail'], $lang_custom['mailsync_ende']);
                        } else {
                            $this->sendInfoMail($row['TargetMail'], $lang_custom['mailsync_ende']);
                        }

                        $this->_Logs($UserID, $JobID, $lang_user['msLogEnde']);
                    } else {
                        $this->_Logs($UserID, $JobID, $lang_user['msLogFehlerVerbindung']);
                    }


                }
                $cronres->Free();

                // Debug
                if ($MSConfig['Debug'] == 'yes') {
                    PutLog($this->name .' CronJob End');
                }
            }
        } else {
            PutLog($this->name. ' f&uuml;r das Plugin wird der b1gMailServer / b1gMailServerAdmin vorausgesetzt!', 4);
        }
    }


    /**
     * Template
     */
    function BeforeDisplayTemplate($resourceName, &$tpl) {
        global $tpl;

        // LI
        $tpl->addCSSFile("li",      B1GMAIL_REL ."plugins/css/mailsync.css");
    }


    /**
     * Frontend Seite
     */
    function FileHandler($file, $action) {
        global $userRow;

        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1 && $this->GetGroupOptionValue('mailsync', $userRow['gruppe'])) {
            if ($file == 'prefs.php') {
                $GLOBALS['prefsItems']['ms'] = true;
                $GLOBALS['prefsImages']['ms'] = 'plugins/templates/images/mailsync.png';
                $GLOBALS['prefsIcons']['ms'] = 'plugins/templates/images/mailsync.png';
            }
        }
    }

    function UserPrefsPageHandler($actGetGroupOptionValueion) {
        global $tpl, $db, $userRow, $mailbox, $bm_prefs, $lang_user;

        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {

            if($this->GetGroupOptionValue('mailsync', $userRow['gruppe']) && $_REQUEST['action'] == 'ms') {
                $tpl->assign('SID', session_id());

                // Jobs ausgeben
                if ($_REQUEST['page'] == 'list' || empty($_REQUEST['page'])) {

                    // Einzelner eintrag löschen
                    if ($_REQUEST['do'] == 'delete' && isset($_REQUEST['id'])) {
                        $this->_Logs($userRow['id'], $_REQUEST['id'], "Job mit der ID \"" . $_REQUEST['id'] . "\" wurde entfernt");
                        $db->Query('DELETE FROM {pre}mod_mailsync WHERE TargetMail =? AND `id`=?',
                            $userRow['email'],
                            (int)$_REQUEST['id']);
                    }

                    // Multi eintrag löschen
                    if ($_REQUEST['do2'] == 'delete') {
                        foreach ($_REQUEST['sync'] as $sync) {
                            $db->Query('DELETE FROM {pre}mod_mailsync WHERE TargetMail =? AND `id`=?',
                                $userRow['email'],
                                (int)$sync);
                        }
                    }

                    // Liste ausgeben
                    $mailList = array();
                    $res = $db->Query('SELECT * FROM {pre}mod_mailsync WHERE UserID =? ORDER BY JobDate DESC',
                        $userRow['id']);
                    while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                        if ($row['Status'] == 0) {
                            $status = 'mailsync_grey.png';
                            $status_text = $lang_user['msSyncEnd'];
                        } else if ($row['Status'] == 1) {
                            $status = 'mailsync_yellow.png';
                            $status_text = $lang_user['msSyncPlan'];
                        } else {
                            $status = 'mailsync_blue.png';
                            $status_text = $lang_user['msSyncOn'];
                        }

                        $mailList[$row['ID']] = array(
                            "ID"            => $row['ID'],
                            "UserID"        => $row['UserID'],
                            "Status"        => $status,
                            "StatusText"    => $status_text,
                            "AnzMails"      => $row['AnzMails'],
                            "AnzFolders"    => $row['AnzFolders'],
                            "JobDate"       => date($userRow['datumsformat'], strtotime($row['JobDate'])),
                            "SourceServer"  => $row['SourceServer'],
                            "SourceType"    => $row['SourceType'],
                            "SourcePort"    => $row['SourcePort'],
                            "SourceCert"    => $row['SourceCert'],
                            "SourceMail"    => $row['SourceMail'],
                            "TargetMail"    => $row['TargetMail'],
                            "TargetFolder"  => $row['TargetFolder']
                        );
                    }
                    $res->Free();

                    // Logs ausgeben
                    $logList = NULL;
                    if ($_REQUEST['do'] == "log") {
                        $res = $db->Query('SELECT * FROM {pre}mod_mailsync_logs WHERE JobID =? AND UserID =? ORDER BY LogDate DESC',
                            $_REQUEST['id'],
                            $userRow['id']);
                        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                            $logList[$row['ID']] = array(
                                "ID" => $row['ID'],
                                "UserID" => $row['UserID'],
                                "LogDate" => date($userRow['datumsformat'], strtotime($row['LogDate'])),
                                "Nachricht" => $row['Nachricht']
                            );
                        }
                        $res->Free();
                    }

                    $tpl->assign('MailList', $mailList);
                    $tpl->assign('LogList', $logList);
                    $tpl->assign('pageContent', $this->_templatePath('mailsync.prefs.list.tpl'));
                    $tpl->display('li/index.tpl');

                    // Job hinzufügen
                } else if ($_REQUEST['page'] == 'add') {
                    $MSConfig       = $this->_getMSInfos();
                    $bmsinfo        = $this->_getMBSInfos();
                    $SystemInfos    = $this->_getSInfos();

                    $showmsg = NULL;

                    if (isset($_REQUEST['add'])) {
                        $save = $_REQUEST['add'];
                    } else {
                        $save = NULL;
                    }
                    if (isset($_REQUEST['sourcemail'])) {
                        $sourcemail = $_REQUEST['sourcemail'];
                    } else {
                        $sourcemail = NULL;
                    }
                    if (isset($_REQUEST['sourcepw'])) {
                        $sourcepw = $_REQUEST['sourcepw'];
                    } else {
                        $sourcepw = NULL;
                    }
                    if (isset($_REQUEST['sourceserver'])) {
                        $sourceserver = $_REQUEST['sourceserver'];
                    } else {
                        $sourceserver = NULL;
                    }
                    if (isset($_REQUEST['sourceport'])) {
                        $sourceport = $_REQUEST['sourceport'];
                    } else {
                        $sourceport = '993';
                    }
                    if (isset($_REQUEST['sourcecert'])) {
                        $sourcecert = $_REQUEST['sourcecert'];
                    } else {
                        $sourcecert = 'ssl';
                    }
                    if (isset($_REQUEST['targetpw'])) {
                        $targetpw = $_REQUEST['targetpw'];
                    } else {
                        $targetpw = NULL;
                    }
                    if (isset($_REQUEST['targetfolder'])) {
                        $targetfolder = $_REQUEST['targetfolder'];
                    } else {
                        $targetfolder = $lang_user['msOrdnerName'];
                    }

                    if (isset($save)) {
                        $error = false;
                        $showmsg = '<div class="infostate-error">';
                        if (empty($sourcemail)) {
                            $showmsg .= $lang_user['msMSGEMail'] . '<br />';
                            $error = true;
                        }
                        if (empty($sourcepw)) {
                            $showmsg .= $lang_user['msMSGPasswort'] . '<br />';
                            $error = true;
                        }
                        if (empty($sourceserver)) {
                            $showmsg .= $lang_user['msMSGServer'] . '<br />';
                            $error = true;
                        }
                        if (empty($targetpw)) {
                            $showmsg .= $lang_user['msMSGPasswort'] . '<br />';
                            $error = true;
                        }
                        if (empty($targetfolder)) {
                            $targetfolder = 'Migration';
                        }
                        if ($this->_MailLogin($userRow['id'], $sourceserver, $sourceport, 'imap', $sourcecert, $sourcemail, $sourcepw) == FALSE) {
                            $showmsg .= $lang_user['msMSGbAnbieter'] . '<br />';
                            $error = true;
                        }
                        if ($bmsinfo['user_imapssl'] == 1 && $bmsinfo['user_imapport'] == '143') {
                            if ($MSConfig['SelfSignedCert'] == 'yes') {
                                $usessl = 'novalidate-cert';
                            } else {
                                $usessl = 'tls';
                            }
                        } else if ($bmsinfo['user_imapssl'] == 1) {
                            if ($MSConfig['SelfSignedCert'] == 'yes') {
                                $usessl = 'novalidate-cert';
                            } else {
                                $usessl = 'ssl';
                            }
                        } else {
                            if ($MSConfig['SelfSignedCert'] == 'yes') {
                                $usessl = 'novalidate-cert';
                            } else {
                                $usessl = '';
                            }
                        }

                        if ($this->_MailLogin($userRow['id'], $bmsinfo['user_imapserver'], $bmsinfo['user_imapport'], 'imap', $usessl, $userRow['email'], $targetpw) == FALSE) {
                            $showmsg .= $lang_user['msMSGnAnbieter'] . '<br />';
                            $error = true;
                        }
                        $showmsg .= '</div>';


                        if ($error == false) {

                            $db->Query('INSERT INTO {pre}mod_mailsync (`Status`,`UserID`,`JobDate`,`SourceServer`,`SourceType`,`SourcePort`,`SourceCert`,`SourceMail`,`SourcePW`,`TargetMail`,`TargetPW`,`TargetFolder`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',
                                '1',
                                $userRow['id'],
                                date('Y-m-d H:i:s'),
                                $sourceserver,
                                'imap',
                                $sourceport,
                                $sourcecert,
                                $sourcemail,
                                $this->encrypt_decrypt('encrypt', $sourcepw),
                                $userRow['email'],
                                $this->encrypt_decrypt('encrypt', $targetpw),
                                $targetfolder);

                            $syncID = $db->InsertId();

                            $showmsg = '<div class="infostate-ok">';
                            $showmsg .= $lang_user['msMSGStart'] . '<br />';
                            $showmsg .= '</div>';

                            $this->_Logs($userRow['id'], $syncID, $lang_user['msMSGAddJob']);

                            // Formular bereinigen
                            $sourcemail = NULL;
                            $sourcepw = NULL;
                            $sourceserver = NULL;
                            $sourceport = NULL;
                            $sourcecert = NULL;
                            $targetpw = NULL;
                            $targetfolder = $lang_user['msOrdnerName'];
                        }
                    }

                    $tpl->assign('ShowMSG', $showmsg);
                    $tpl->assign('SourceProvider', $_REQUEST['provider']);
                    $tpl->assign('ProviderList', $this->_getProviderList());
                    $tpl->assign('SourceMail', $sourcemail);
                    $tpl->assign('SourcePW', $sourcepw);
                    if ($_REQUEST['provider'] !== 'noprov') {
                        $sprov = $this->_getProviderList($_REQUEST['provider']);
                        foreach ($sprov as $sp) {
                            $tpl->assign('SourceServer', $sp['Server']);
                            $tpl->assign('SourcePort', $sp['Port']);
                            $tpl->assign('SourceCert', $sp['Cert']);
                        }
                    } else {
                        $tpl->assign('SourceServer', $sourceserver);
                        $tpl->assign('SourcePort', $sourceport);
                        $tpl->assign('SourceCert', $sourcecert);
                    }
                    $tpl->assign('TargetPW', $targetpw);
                    $tpl->assign('TargetFolder', $targetfolder);

                    $tpl->assign('TargetTitel', $SystemInfos['titel']);
                    $tpl->assign('pageContent', $this->_templatePath('mailsync.prefs.add.tpl'));
                    $tpl->display('li/index.tpl');
                }
            }
        }
        return(true);
    }


    /**
     * Administration
     */
    function AdminHandler() {
        global $tpl, $lang_admin;

        if (!isset($_REQUEST['do']))
            $_REQUEST['do'] = 'list';

        $tabs = array(
            0 => array(
                'title' => $lang_admin['msList'],
                'icon' => '../plugins/templates/images/mailsync.png',
                'link' => $this->_adminLink() . '&do=list&',
                'active' => $_REQUEST['do'] == 'list'
            ),
            1 => array(
                'title' => $lang_admin['msProvider'],
                'icon' => './templates/images/country32.png',
                'link' => $this->_adminLink() . '&do=provider&',
                'active' => $_REQUEST['do'] == 'provider'
            ),
            2 => array(
                'title' => $lang_admin['msLogs'],
                'icon' => '../plugins/templates/images/mailsync_log.png',
                'link' => $this->_adminLink() . '&do=logs&',
                'active' => $_REQUEST['do'] == 'logs'
            ),
            3 => array(
                'title' => $lang_admin['msConfig'],
                'icon' => '../plugins/templates/images/mailsync_settings.png',
                'link' => $this->_adminLink() . '&do=conf&',
                'active' => $_REQUEST['do'] == 'conf'
            ),
            4 => array(
                'title' => $lang_admin['msHilfe'],
                'icon' => '../plugins/templates/images/mailsync_help.png',
                'link' => $this->_adminLink() . '&do=hilfe&',
                'active' => $_REQUEST['do'] == 'hilfe'
            )
        );

        $tpl->assign('tabs', $tabs);

        if($_REQUEST['do'] == 'list')           { $this->_listPage(); }
        if($_REQUEST['do'] == 'provider')       { $this->_providerPage(); }
        if($_REQUEST['do'] == 'provideredit')   { $this->_providereditPage(); }
        if($_REQUEST['do'] == 'logs')           { $this->_logsPage(); }
        if($_REQUEST['do'] == 'conf')           { $this->_confPage(); }
        if($_REQUEST['do'] == 'hilfe')          { $this->_hilfePage(); }
    }


    /**
     * Job Liste
     */
    function _listPage() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        $MSConfig = $this->_getMSInfos();

        // Logs
        if ($MSConfig['Debug'] == 'yes') {
            $prioImg = array(
                PRIO_DEBUG      => 'debug',
                PRIO_ERROR      => 'error',
                PRIO_NOTE       => 'info',
                PRIO_WARNING    => 'warning',
                PRIO_PLUGIN     => 'plugin'
            );
            $start = isset($_REQUEST['startDay']) ? SmartyDateTime('start')
                : (isset($_REQUEST['start']) ? (int)$_REQUEST['start']
                    : mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            $end = isset($_REQUEST['endDay']) ? SmartyDateTime('end') + 59
                : (isset($_REQUEST['end']) ? (int)$_REQUEST['end']
                    : time());
            $addQ = isset($_REQUEST['q']) && trim($_REQUEST['q']) != ''
                ? ' AND eintrag LIKE \'%' . $db->Escape($_REQUEST['q']) . '%\''
                : '';
            $prio = isset($_REQUEST['prio']) && is_array($_REQUEST['prio'])
                ? $_REQUEST['prio']
                : array(PRIO_DEBUG => false, PRIO_ERROR => true, PRIO_NOTE => true,
                    PRIO_WARNING => true, PRIO_PLUGIN => true);

            $entries = array();
            $res = $db->Query('SELECT prio,eintrag,zeitstempel FROM {pre}logs WHERE zeitstempel>=' . $start . ' AND zeitstempel<=' . $end . $addQ . ' AND eintrag like "%Mail Sync%" AND prio IN ? ORDER BY id DESC',
                array_keys($prio));
            while ($row = $res->FetchArray()) {
                $row['prioImg'] = $prioImg[$row['prio']];
                $entries[] = $row;
            }
            $res->Free();

            $prioQ = '';
            foreach ($prio as $key => $val)
                $prioQ .= '&prio[' . ((int)$key) . ']=true';

            $tpl->assign('prioQ', $prioQ);
            $tpl->assign('prio', $prio);
            $tpl->assign('q', isset($_REQUEST['q']) ? $_REQUEST['q'] : '');
            $tpl->assign('ueQ', isset($_REQUEST['q']) ? urlencode($_REQUEST['q']) : '');
            $tpl->assign('start', $start);
            $tpl->assign('end', $end);
            $tpl->assign('entries', $entries);
            $tpl->assign('msShowLog', TRUE);
        } else {
            $tpl->assign('msShowLog', FALSE);
        }



        // Löschen
        if ($_REQUEST['do'] == 'list' && $_REQUEST['delete']) {
            $db->Query('DELETE FROM {pre}mod_mailsync WHERE `ID`=?',
                (int)$_REQUEST['delete']);
        }

        // Liste
        $contents = array();
        $rescon = $db->Query('SELECT * FROM {pre}mod_mailsync ORDER BY `JobDate` DESC');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            if ($row['Status'] == 0) {
                $status         = 'mailsync_grey.png';
                $status_text    = $lang_admin['msStatusOFF'];
            } else if ($row['Status'] == 1) {
                $status         = 'mailsync_yellow.png';
                $status_text    = $lang_admin['msStatusONG'];
            } else {
                $status         = 'mailsync_blue.png';
                $status_text    = $lang_admin['msStatusON'];
            }

            $contents[$row['ID']] = array(
                "ID"            => $row['ID'],
                "UserID"        => $row['UserID'],
                "EMail"         => $this->_getMailAdress($row['UserID']),
                "Status"        => $status,
                "StatusText"    => $status_text,
                "AnzMails"      => $row['AnzMails'],
                "AnzFolders"    => $row['AnzFolders'],
                "JobDate"       => date('d.m.Y H:i:s', strtotime($row['JobDate'])),
                "SourceServer"  => $row['SourceServer'],
                "SourceType"    => $row['SourceType'],
                "SourcePort"    => $row['SourcePort'],
                "SourceCert"    => $row['SourceCert'],
                "SourceMail"    => $row['SourceMail'],
                "TargetMail"    => $row['TargetMail'],
                "TargetFolder"  => $row['TargetFolder']
            );
        }
        $rescon->Free();


        // assign
        $tpl->assign('usertpldir',      B1GMAIL_REL .'templates/'. $bm_prefs['template'] .'/');
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('List',            $contents);
        $tpl->assign('page',            $this->_templatePath('mailsync.acp.list.tpl'));
    }


    /**
     * Provider
     */
    function _providerPage() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        // Speichern
        if ((isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit') && isset($_REQUEST['id'])) {
            $MSG  = '<div class="infostate-ok">';

            if ($_REQUEST['activate'] == 'on') {
                $dbactivate = 'yes';
            } else {
                $dbactivate = 'no';
            }

            if ($_REQUEST['type'] == 'imaptls') {
                $cert = 'tls';
            } else if ($_REQUEST['type'] == 'imapssl') {
                $cert = 'ssl';
            } else {
                $cert = '';
            }

            $db->Query('UPDATE {pre}mod_mailsync_provider SET `Status`=?, `Name`=?, `Server`=?, `Type`=?, `Port`=?, `Cert`=? WHERE `ID`=?',
                $dbactivate,
                $_REQUEST['name'],
                $_REQUEST['server'],
                'imap',
                $_REQUEST['port'],
                $cert,
                (int)$_REQUEST['id']);

            $MSG .= 'Text wurde bearbeitet';
            $MSG .= '</div>';
        }

        // Erstellen
        $MSG = NULL;
        if (isset($_REQUEST['provider-save'])) {
            $MSG  = '<div class="infostate-ok">';

            if ($_REQUEST['activate'] == 'on') {
                $dbactivate = 'yes';
            } else {
                $dbactivate = 'no';
            }

            if ($_REQUEST['type'] == 'tls') {
                $cert = 'tls';
            } else if ($_REQUEST['type'] == 'ssl') {
                $cert = 'ssl';
            } else {
                $cert = '';
            }

            // Erstellen
            $db->Query('INSERT INTO {pre}mod_mailsync_provider (`Status`, `Name`, `Server`, `Type`, `Port`, `Cert`) VALUES (?, ?, ?, ?, ?, ?)',
                $dbactivate,
                $_REQUEST['name'],
                $_REQUEST['server'],
                'imap',
                $_REQUEST['port'],
                $cert);

            $MSG .= 'Provider wurde angelegt<br />';
            $MSG .= '</div>';
        }

        // löschen
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'provider') && isset($_REQUEST['delete'])) {
            $db->Query('DELETE FROM {pre}mod_mailsync_provider WHERE `ID`=?',
                (int)$_REQUEST['delete']);
        }

        // Texte
        $restext = $db->Query('SELECT * FROM {pre}mod_mailsync_provider ORDER BY Name');
        while($row = $restext->FetchArray(MYSQLI_ASSOC)) {
            $contents[$row['ID']] = array(
                "ID"        => $row['ID'],
                "Status"    => $row['Status'],
                "Name"      => $row['Name'],
                "Server"    => $row['Server'],
                "Type"      => $row['Type'],
                "Port"      => $row['Port'],
                "Cert"      => $row['Cert']
            );
        }
        $restext->Free();

        // assign
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('msProvider',      $contents);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('page',            $this->_templatePath('mailsync.acp.provider.tpl'));
    }


    /**
     * Provider bearbeiten
     */
    function _providereditPage() {
        global $tpl, $db, $bm_prefs;

        // Anleitungen
        $rescon = $db->Query('SELECT * FROM {pre}mod_mailsync_provider WHERE ID = "'. $_REQUEST['id'] .'" LIMIT 1');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('ID',          $row['ID']);
            $tpl->assign('Status',      $row['Status']);
            $tpl->assign('Name',        $row['Name']);
            $tpl->assign('Server',      $row['Server']);
            $tpl->assign('Type',        $row['Type']);
            $tpl->assign('Port',        $row['Port']);
            $tpl->assign('Cert',        $row['Cert']);
        }
        $rescon->Free();

        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('usertpldir',      B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('page',            $this->_templatePath('mailsync.acp.provideredit.tpl'));
    }


    /**
     * Logs
     */
    function _logsPage() {
        global $tpl, $db;

        // Logs ausgeben
        $logList = NULL;
        if ($_REQUEST['do'] == "logs") {
            if (isset($_REQUEST['id'])) {
                $res = $db->Query('SELECT * FROM {pre}mod_mailsync_logs WHERE JobID =? ORDER BY LogDate DESC',
                    $_REQUEST['id']);
            } else {
                $res = $db->Query('SELECT * FROM {pre}mod_mailsync_logs ORDER BY LogDate DESC');
            }

            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {

                if ($row['Status'] == 'warning') {
                    $status = '<img src="../plugins/templates/images/mailsync_warning.png" width="16" height="16" />';
                } else if ($row['Status'] == 'error') {
                    $status = '<img src="../plugins/templates/images/mailsync_error.png" width="16" height="16" />';
                } else {
                    $status = '<img src="../plugins/templates/images/mailsync_info.png" width="16" height="16" />';
                }

                $logList[$row['ID']] = array(
                    "ID"        => $row['ID'],
                    "JobID"     => $row['JobID'],
                    "UserID"    => $row['UserID'],
                    "EMail"     => $this->_getMailAdress($row['UserID']),
                    "LogDate"   => date('d.m.Y H:i:s', strtotime($row['LogDate'])),
                    "Status"    => $status,
                    "Nachricht" => $row['Nachricht']
                );
            }
            $res->Free();
        }

        // assign
        $tpl->assign('pageURL',         $this->_adminLink());
        if (isset($_REQUEST['id'])) {
            $tpl->assign('JogLog',      FALSE);
        } else {
            $tpl->assign('JogLog',      TRUE);
        }
        $tpl->assign('LogList',         $logList);
        $tpl->assign('page',            $this->_templatePath('mailsync.acp.logs.tpl'));
    }


    /**
     * Konfiguration
     */
    function _confPage() {
        global $tpl, $db, $lang_admin;

        $MSG            = NULL;
        $ERROR          = FALSE;
        $MailGruppen    = NULL;

        // Speichern
        if (isset($_REQUEST['confsave'])) {
            if ($ERROR == FALSE) {
                if ($_REQUEST['debug'] == 'on') {
                    $_REQUEST['debug'] = 'yes';
                } else {
                    $_REQUEST['debug'] = 'no';
                }

                if ($_REQUEST['selfsignedcert'] == 'on') {
                    $_REQUEST['selfsignedcert'] = 'yes';
                } else {
                    $_REQUEST['selfsignedcert'] = 'no';
                }

                if ($_REQUEST['cronovercli'] == 'on') {
                    $_REQUEST['cronovercli'] = 'yes';
                } else {
                    $_REQUEST['cronovercli'] = 'no';
                }

                $db->Query('UPDATE {pre}mod_mailsync_conf SET `Debug`=?, `SelfSignedCert`=?, `SyncGroup`=?, `CronOverCLI`=? WHERE `ID`=?',
                    $_REQUEST['debug'],
                    $_REQUEST['selfsignedcert'],
                    $_REQUEST['mailgruppe'],
                    $_REQUEST['cronovercli'],
                    1);


                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['msConfigMSG'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Mail Gruppen
        $rescon = $db->Query('SELECT * FROM {pre}gruppen ORDER BY titel');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $MailGruppen[$row['id']] = array(
                "ID" => $row['id'],
                "Titel" => $row['titel']
            );
        }
        $rescon->Free();

        // Einstellungen
        $rescon = $db->Query('SELECT * FROM {pre}mod_mailsync_conf WHERE ID =? LIMIT 1', 1);
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('Debug',           $row['Debug']);
            $tpl->assign('SyncGroup',       $row['SyncGroup']);
            $tpl->assign('SelfSignedCert',  $row['SelfSignedCert']);
            $tpl->assign('CronOverCLI',     $row['CronOverCLI']);
            $tpl->assign('LastCronJob',     date('d.m.Y H:i', strtotime($row['LastCronJob'])));
        }
        $rescon->Free();


        // assign
        $tpl->assign('Version',         $this->version);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MailGruppen',     $MailGruppen);
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('page',            $this->_templatePath('mailsync.acp.conf.tpl'));
    }


    /**
     * Hilfe
     */
    function _hilfePage() {
        global $tpl, $db, $lang_admin, $bm_prefs;

        // Plugin
        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) { $PluginBMA = 'mailsync_check.png'; } else { $PluginBMA = 'mailsync_uncheck.png'; }

        // PHP Erweiterungen
        if (extension_loaded('imap')) {     $PHPExtIMAP = 'mailsync_check.png';     } else { $PHPExtIMAP = 'mailsync_uncheck.png'; }
        if (extension_loaded('openssl')) {  $PHPExtOSSL = 'mailsync_check.png';     } else { $PHPExtOSSL = 'mailsync_uncheck.png'; }
        if (extension_loaded('mbstring')) { $PHPExtMBSTRING = 'mailsync_check.png'; } else { $PHPExtMBSTRING = 'mailsync_uncheck.png'; }

        // assign
        $tpl->assign('Version',         $this->version);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('PluginBMA',       $PluginBMA);
        $tpl->assign('PHPExtIMAP',      $PHPExtIMAP);
        $tpl->assign('PHPExtOSSL',      $PHPExtOSSL);
        $tpl->assign('PHPExtMBSTRING',  $PHPExtMBSTRING);
        $tpl->assign('page',            $this->_templatePath('mailsync.acp.hilfe.tpl'));
    }

}

$plugins->registerPlugin('MailSync');

?>
