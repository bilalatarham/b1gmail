<?php
/* ========== mb_smtop_spamstop ==========
This plugin will add an admin page where we can find potential spammers. 
Users can be whitelisted in order not to be shown again. 
Also, it is possible to include/exclude whole groups.
*/
class mb_smtop_spamstop extends BMPlugin
{
	function mb_smtop_spamstop()
	{
		
		$this->name        = 'SMTP Spammer erkennen';
		$this->author      = 'Martin Buchalik';
		$this->web         = 'http://martin-buchalik.de';
		$this->mail        = 'support@martin-buchalik.de';
		$this->version     = '1.0.1';
		$this->designedfor = '7.3.0';
		$this->type        = BMPLUGIN_DEFAULT;
		
		$this->admin_pages      = true;
		$this->admin_page_title = 'SMTP-Spamstop';
	}
	
	
	/* ===== Installation ===== */
	
	function Install()
	{
		global $db;
		
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}mb_smtp_spamstop (
							`id` INT(11) NOT NULL AUTO_INCREMENT,
							`userid` INT(11) NOT NULL,													
							PRIMARY KEY (`id`),
							INDEX(`userid`))');
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}mb_smtp_spamstop_groups (
							`id` INT(11) NOT NULL AUTO_INCREMENT,
							`groupid` INT(11) NOT NULL,													
							PRIMARY KEY (`id`),
							INDEX(`groupid`))');
		
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	
	/* ===== Uninstall ===== */
	
	function Uninstall()
	{
		global $db;
		
		$db->Query('DROP TABLE {pre}mb_smtp_spamstop');
		$db->Query('DROP TABLE {pre}mb_smtp_spamstop_groups');
		
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	/* ===== AdminHandler ===== */
	function AdminHandler()
	{
		global $tpl, $plugins;
		
		if (!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'spammers';
		
		$tabs = array(
			0 => array(
				'title' => 'Potenzielle Spammer',
				'icon' => '../admin/templates/images/antispam_dnsbl.png',
				'link' => $this->_adminLink() . '&action=spammers&',
				'active' => $_REQUEST['action'] == 'spammers'
			),
			1 => array(
				'title' => 'Whitelist',
				'icon' => '../admin/templates/images/cert32.png',
				'link' => $this->_adminLink() . '&action=whitelist&',
				'active' => $_REQUEST['action'] == 'whitelist'
			),
			2 => array(
				'title' => 'Hinzufügen',
				'icon' => '../admin/templates/images/add32.png',
				'link' => $this->_adminLink() . '&action=add&',
				'active' => $_REQUEST['action'] == 'add'
			),
			3 => array(
				'title' => 'Gruppen',
				'icon' => '../admin/templates/images/group32.png',
				'link' => $this->_adminLink() . '&action=groups&',
				'active' => $_REQUEST['action'] == 'groups'
			)			
		);
		
		$tpl->assign('tabs', $tabs);
		
		if ($_REQUEST['action'] == 'spammers') {
			$this->Spammers();
		} elseif ($_REQUEST['action'] == 'add') {
			$this->Add();
		} elseif ($_REQUEST['action'] == 'groups') {
			$this->Groups();
		} elseif ($_REQUEST['action'] == 'whitelist') {
			$this->WhitelistOverview();
		}
	}
	
	/* ===== Spammers ===== */
	function Spammers()
	{
		global $tpl, $db;
		
		$tpl->assign('spammers', $this->GetPotentialSpammers());
		$tpl->assign('pluginURL', $this->_adminLink() . '&');
		$tpl->assign('pageURL', $this->_adminLink() . '&action=spammers&');
		$tpl->assign('page', $this->_templatePath('mb_smtp_spamstop.admin.spammers.tpl'));
	}
	
	/* ===== GetPotentialSpammers ===== 
	This function will either return false or an array with the user ID as key and user email as value if the following conditions are met:
	- last_pop3 is 0
	- last_imap is 0
	- last_smtp is > 0
	- (user)id is not listed in our whitelist table
	- (user)group is listed in our group table
	*/
	function GetPotentialSpammers()
	{
		global $db;
		
		$res = $db->Query('SELECT id, email, sent_mails, gesperrt FROM {pre}users WHERE last_pop3 = ? AND last_imap = ? AND last_smtp > ? AND id NOT IN(SELECT userid FROM {pre}mb_smtp_spamstop) AND gruppe IN(SELECT groupid FROM {pre}mb_smtp_spamstop_groups)', 0, 0, 0);
		if ($res->RowCount() == 0) {
			$res->Free();
			return false;
		}
		
		while ($row = $res->FetchArray(MYSQL_ASSOC)) {
			$result[$row['id']] = array("email" => $row['email'], "sent_mails" => $row['sent_mails'], "gesperrt" => $row['gesperrt']);
		}
		$res->Free();
		
		return $result;
	}
	
	/* ===== WhitelistOverview ===== */
	function WhitelistOverview()
	{
		global $tpl, $db;
		
		if (isset($_REQUEST['delete'])) {
			$db->Query('DELETE FROM {pre}mb_smtp_spamstop WHERE userid=?', $_REQUEST['delete']);
		}
		
		$whitelistedUsers = false;
		
		$res = $db->Query('SELECT id, email FROM {pre}users WHERE id IN(SELECT userid FROM {pre}mb_smtp_spamstop)');
		if ($res->RowCount() > 0) {
			while ($row = $res->FetchArray(MYSQL_ASSOC)) {
				$whitelistedUsers[$row['id']] = $row['email'];
			}
		}
		$res->Free();
		
		$tpl->assign('whitelistedUsers', $whitelistedUsers);
		$tpl->assign('pageURL', $this->_adminLink() . '&action=whitelist&');
		$tpl->assign('page', $this->_templatePath('mb_smtp_spamstop.admin.whitelist.tpl'));
		
	}
	
	/* ===== Add ===== */
	function Add()
	{
		global $tpl, $db;
		
		$error = false;
		$value = "";
		
		if (isset($_REQUEST['whitelist_add'])) {
			$result = $this->AddToWhitelist($_REQUEST['whitelist_add']);
			if ($result === true) {
				header('Location: ' . $this->_adminLink() . '&action=whitelist&sid=' . session_id());
				exit();
			} else {
				$error = $result;
				$value = $_POST['whitelist_add'];
			}
		}
		
		$tpl->assign('error', $error);
		$tpl->assign('value', $value);
		$tpl->assign('pageURL', $this->_adminLink() . '&action=add&');
		$tpl->assign('page', $this->_templatePath('mb_smtp_spamstop.admin.add.tpl'));
	}
	
	/* ===== AddToWhitelist =====
	This function will try to add a user to our whitelist based on the ID or mail address. 
	If the return is true, the user has beed added successfully. 
	Otherwise, we will return a string with an error message.
	
	----- $user: User ID or mail address.		
	*/
	function AddToWhitelist($user)
	{
		global $db;
		
		$user = trim($user);
		if (ctype_digit($user)) {
			$res = $db->Query('SELECT userid FROM {pre}mb_smtp_spamstop WHERE userid = ?', $user);
			if ($res->RowCount() > 0) {
				$res->Free();
				return "Der Nutzer befindet sich bereits auf der Whitelist!";
			}
			$res->Free();
			
			$res = $db->Query('SELECT id FROM {pre}users WHERE id = ?', $user);
			if ($res->RowCount() == 0) {
				$res->Free();
				return "Nutzer nicht gefunden!";
			}
			$res->Free();
			
			$db->Query('INSERT INTO {pre}mb_smtp_spamstop(userid) VALUES(?)', $user);
			return true;
		} else {
			//Try to find our user by main email
			$res = $db->Query('SELECT id FROM {pre}users WHERE email = ?', $user);
			if ($res->RowCount() == 0) {
				$res->Free();
				
				//No user found? Check aliases!
				$res = $db->Query('SELECT user FROM {pre}aliase WHERE email = ?', $user);
				if ($res->RowCount() != 1) {
					$res->Free();
					return "Nutzer nicht gefunden!";
				}
				while ($row = $res->FetchArray(MYSQL_ASSOC)) {
					$userID = $row['user'];
				}
				$res->Free();
			} else {
				while ($row = $res->FetchArray(MYSQL_ASSOC)) {
					$userID = $row['id'];
				}
			}
			$res->Free();
			
			//Now we got our userID so run this function again.
			return $this->AddToWhitelist($userID);
		}
		
	}
	
	/* ===== Groups ===== */
	function Groups()
	{
		global $tpl, $db;
		
		$res = $db->Query('SELECT id, titel FROM {pre}gruppen');
		//It is impossible that no groups are registered in the system so we don't need to check if the result is empty or not.			
		while ($row = $res->FetchArray(MYSQL_ASSOC)) {
			$groups[$row['id']] = array(
				"titel" => $row['titel'],
				"enabled" => false
			);
		}
		
		if (isset($_REQUEST['do']) && $_REQUEST['do'] == "updateGroups") {
			//We don't expect a large amount of groups so the best solution is to clear the table and re-create it "entry-by-entry".
			$db->Query('TRUNCATE TABLE {pre}mb_smtp_spamstop_groups');
			foreach ($groups as $key => $value) {
				if (isset($_POST['group_' . $key]))
					$db->Query('INSERT INTO {pre}mb_smtp_spamstop_groups(groupid) VALUES(?)', $key);
			}
		}
		
		$res->Free();
		$res = $db->Query('SELECT groupid FROM {pre}mb_smtp_spamstop_groups');
		while ($row = $res->FetchArray(MYSQL_ASSOC)) {
			if (isset($groups[$row['groupid']])) {
				$groups[$row['groupid']]["enabled"] = true;
			} else {
				//The group doesn't exist so we should delete the database entry!
				$db->Query('DELETE FROM {pre}mb_smtp_spamstop_groups WHERE groupid=?', $row['groupid']);
			}
		}
		$res->Free();
		
		$tpl->assign('groups', $groups);
		$tpl->assign('pageURL', $this->_adminLink() . '&action=groups&');
		$tpl->assign('page', $this->_templatePath('mb_smtp_spamstop.admin.groups.tpl'));
	}
}

/* ===== register plugin ===== */
$plugins->registerPlugin('mb_smtop_spamstop');
?>