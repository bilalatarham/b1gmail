<?php
/*
	API versions:
	1: until mb_app 1.1.0. Still supported.
	2: 1.2.0 - 1.4.0. Still supported.
	3: 1.5.0 - (current)

	The following changes were made that lead to a new API version:
	1 --> 2: Switch from sha1 to sha256 when dealing with the authentication tokens.
	2 --> 3: Upgrade to b1gmail 7.4.
*/

// UI field type for passwords.
define('MB_APP_FIELD_PASSWORD', 128);

// Should the old APNs functions be used? NOTE: This will be removed in the future.
define('MB_APP_USE_LEGACY_APNS_FUNCTIONS', false);

class mb_app extends BMPlugin {

	function __construct() {
		$this->name        = 'Smartphone-App';
		$this->author      = 'Martin Buchalik';
		$this->web         = 'http://martin-buchalik.de';
		$this->mail        = 'support@martin-buchalik.de';
		// Do not change the version number since it can lock the API completely.
		$this->version     = '1.6.0';
		$this->designedfor = '7.4.0';
		$this->type        = BMPLUGIN_DEFAULT;

		$this->admin_pages      = true;
		$this->admin_page_title = 'mb_app';
		$this->admin_page_icon  = 'mb_app_16.png';
	}

	/* ===== Install ===== */
	function Install() {
		global $db;
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}mb_app (
							id INT(11) AUTO_INCREMENT,
							userid INT(11) NOT NULL,
							added INT(11) NOT NULL,
							lastaccessed INT(11) NOT NULL,
							ipAdded VARCHAR(255) NOT NULL,
							ipLastaccessed VARCHAR(255) NOT NULL,
							language VARCHAR(255) NOT NULL,
							accesskey VARCHAR(255) NOT NULL,
							accesstoken VARCHAR(255) NOT NULL,
							platform VARCHAR(255) NOT NULL,
							pushid VARCHAR(255) NOT NULL,
							pushactive VARCHAR(255) NOT NULL,
							PRIMARY KEY (id),
							INDEX(userid),
							INDEX(lastaccessed),
							INDEX(pushid))');

		$db->Query('CREATE TABLE IF NOT EXISTS {pre}mb_app_prefs (
							`key` VARCHAR(255),
							value TEXT NOT NULL,
							PRIMARY KEY (`key`))');
		$db->Query('REPLACE INTO {pre}mb_app_prefs(`key`, value) VALUES(?,?)', 'version', $this->version);

		PutLog(
			sprintf('Plugin "%s" - %s has been installed.',
				$this->name,
				$this->version
			),
			PRIO_PLUGIN,
			__FILE__,
			__LINE__
		);

		return true;
	}

	/* ===== Uninstall ===== */
	function Uninstall() {
		global $db;
		$db->Query('DROP TABLE IF EXISTS {pre}mb_app');
		$db->Query('DROP TABLE IF EXISTS {pre}mb_app_prefs');

		PutLog(
			sprintf('Plugin "%s" - %s has been uninstalled.',
				$this->name,
				$this->version
			),
			PRIO_PLUGIN,
			__FILE__,
			__LINE__
		);

		return true;
	}

	/* ===== Language variables ===== */
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang) {
		if ($lang == 'deutsch') {
			$lang_user['mb_app'] = 'Smartphone-App';
			$lang_user['prefs_d_mb_app'] = 'Zugänge zur Smartphone-App verwalten';

			$lang_user['mb_app_prefspage_platform'] = 'Plattform';
			$lang_user['mb_app_prefspage_ip'] = 'IP-Adresse';
			$lang_user['mb_app_prefspage_added'] = 'Hinzugefügt';
			$lang_user['mb_app_prefspage_lastaccessed'] = 'Letzter Zugriff';
			$lang_user['mb_app_prefspage_push'] = 'Push-Benachrichtigungen';
			$lang_user['mb_app_delete'] = 'Zugang entfernen';
			$lang_user['mb_app_prefspage_os_other'] = 'Andere';

			/* ===== Admin ===== */
			$lang_admin['mb_app_fcm_api_access_key'] = 'FCM API Access Key';
			$lang_admin['mb_app_notification_title'] = 'Titel der Push-Meldungen';
			$lang_admin['mb_app_apns_topic'] = 'APNs: Topic';
			$lang_admin['mb_app_apns_cert_password'] = 'APNs: Passwort des Push-Zertifikats';
			$lang_admin['mb_app_deleteaccessafter'] = 'Zugang automatisch löschen nach';
			$lang_admin['mb_app_days_after'] = 'Tagen';
			$lang_admin['mb_app_ads_show'] = 'Werbung anzeigen';
			$lang_admin['mb_app_ads_admobid'] = 'Admob-ID';
			$lang_admin['mb_app_use_imageproxy'] = 'Bilder-Proxy nutzen';
			$lang_admin['mb_app_updating'] = 'Installiere Updates - bitte warten. Schließen Sie diese Seite unter keinen Umständen!';
			$lang_admin['mb_app_update_error'] = 'Beim Installieren der Updates ist ein Fehler aufgetreten. Siehe Logs für weitere Informationen.';


			$lang_admin['mb_app_debug_userinput'] = 'ID oder Haupt-E-Mail-Adresse';
			$lang_admin['mb_app_debug_fcm_desc'] = 'Dieses Tool erlaubt es, eine Push-Mitteilung an einen bestimmten Nutzer zu senden.<br />' .
													'Es werden Informationen über die Kommunikation zwischen dem Plugin und der FCM-API ausgegeben.<br />' .
													'Der Nutzer muss mindestens auf einem Android-Gerät einen aktiven Zugang besitzen.';
			$lang_admin['mb_app_debug_apns_desc'] = 'Dieses Tool erlaubt es, eine Push-Mitteilung an einen bestimmten Nutzer zu senden.<br />' .
													'Es werden Informationen über die Kommunikation zwischen dem Plugin und der APNs-API ausgegeben.<br />' .
													'Der Nutzer muss mindestens auf einem iOS-Gerät einen aktiven Zugang besitzen.';
		} else {
			$lang_user['mb_app'] = 'Smartphone app';
			$lang_user['prefs_d_mb_app'] = 'Manage access to the smartphone app';

			$lang_user['mb_app_prefspage_platform'] = 'Platform';
			$lang_user['mb_app_prefspage_ip'] = 'IP address';
			$lang_user['mb_app_prefspage_added'] = 'Added';
			$lang_user['mb_app_prefspage_lastaccessed'] = 'Last access';
			$lang_user['mb_app_prefspage_push'] = 'Push notifications';
			$lang_user['mb_app_delete'] = 'Remove access';
			$lang_user['mb_app_prefspage_os_other'] = 'Other';

			/* ===== Admin ===== */
			$lang_admin['mb_app_fcm_api_access_key'] = 'FCM API Access Key';
			$lang_admin['mb_app_notification_title'] = 'Title of push messages';
			$lang_admin['mb_app_apns_topic'] = 'APNs: Topic';
			$lang_admin['mb_app_apns_cert_password'] = 'APNs: Passwort of the push certificate';
			$lang_admin['mb_app_deleteaccessafter'] = 'Delete access after';
			$lang_admin['mb_app_days_after'] = 'Days';
			$lang_admin['mb_app_ads_show'] = 'Show ads';
			$lang_admin['mb_app_ads_admobid'] = 'Admob ID';
			$lang_admin['mb_app_use_imageproxy'] = 'Use image proxy';
			$lang_admin['mb_app_updating'] = 'Installing updates - please wait. Do not close this page!';
			$lang_admin['mb_app_update_error'] = 'An error occured while trying to install updates. Open logs for more details.';

			$lang_admin['mb_app_debug_userinput'] = 'ID or main mail address';
			$lang_admin['mb_app_debug_fcm_desc'] = 'This tool allows us to send a push message to a specific user.<br />' .
													'We will see some information about the communication between this plugin and FCM<br />' .
													'The user needs to have at least one Android device registered to our app.';
			$lang_admin['mb_app_debug_apns_desc'] = 'This tool allows us to send a push message to a specific user.<br />' .
													'We will see some information about the communication between this plugin and APNs<br />' .
													'The user needs to have at least one iOS device registered to our app.';
		}

		$lang_user['mb_app_prefspage_os_android'] = 'Android';
		$lang_user['mb_app_prefspage_os_ios'] = 'iOS';

		$lang_admin['mb_app_debug'] = 'Debug';
		$lang_admin['mb_app_fcm'] = 'FCM';
		$lang_admin['mb_app_apns'] = 'APNs';
	}

	/* ===== get_app_pref =====
		Fetches a preference by key.
	----- $key: Key of our preference.

	return:
		If the entry is 'yes' or 'no', true or false is returned.
		If no entries were found, false is returned.
		Otherwise, the preference is returned as a string.
	*/
	function get_app_pref($key) {
		global $db;

		$result = false;

		$res = $db->Query('SELECT value FROM {pre}mb_app_prefs WHERE `key`=?',
			$key);
		if($res->RowCount() == 1) {
			while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
				$result = $row['value'];
			}
		}
		$res->Free();

		if($result == 'yes')
			$result = true;
		elseif($result == 'no')
			$result = false;

		return $result;
	}

	/* ===== set_app_pref =====
	----- $key: Key of our preference (must be unique)
	----- $value: Value of our preference.
	*/
	function set_app_pref($key, $value = '') {
		global $db;

		if($value === true)
			$value = 'yes';
		elseif($value === false)
			$value = 'no';

		$db->Query('REPLACE INTO {pre}mb_app_prefs(`key`, value) VALUES(?,?)',
			$key,
			$value);
	}

	/* ===== delete_app_pref =====
	----- $key: Key of our preference we want to delete.
	*/
	function delete_app_pref($key) {
		global $db;

		$db->Query('DELETE FROM {pre}mb_app_prefs WHERE `key`=?',
			$key);
	}

	/* ===== AdminHandler =====
		Checks for updates and installs them. If no updates were found, show our start page.
	*/
	function AdminHandler() {
		global $tpl, $plugins, $lang_admin;

		if($this->update_required()) {
			$tpl->assign('tabs', array());
			$tpl->assign('page', $this->_templatePath('mb_app.admin.update.tpl'));

			if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'update') {
				$this->update_step();
				header('Refresh: 1');
			} else {
				header('Location: ' . $this->_adminLink() . '&do=update&sid=' . session_id());
			}
			return;
		}

		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'prefs';

		$tabs = array(
			0 => array(
				'title' => $lang_admin['prefs'],
				'icon' => '../admin/templates/images/db_struct.png',
				'link' => $this->_adminLink() . '&',
				'active' => $_REQUEST['action'] == 'prefs'
			),
			1 => array(
				'title' => $lang_admin['mb_app_debug'],
				'icon' => '../admin/templates/images/db_optimize.png',
				'link' => $this->_adminLink() . '&action=debug&',
				'active' => $_REQUEST['action'] == 'debug'
			)
		);

		$tpl->assign('tabs', $tabs);

		if($_REQUEST['action'] == 'prefs') {
			$this->admin_prefs();
		} else if($_REQUEST['action'] == 'debug') {
			$this->admin_debug();
		}

	}

	/* ===== admin_prefs ===== */
	function admin_prefs() {
		global $tpl, $db;

		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save') {
			$fields = $this->admin_prefs_fields();
			foreach($fields as $key => $value) {
				if($value['type'] == FIELD_CHECKBOX)
					$this->set_app_pref($key, isset($_POST[$key]));
				else {
					if(isset($_POST[$key])) {
						$this->set_app_pref($key, $_POST[$key]);
					}
				}
			}
		}

		$prefs = array();

		$res = $db->Query('SELECT `key`, value FROM {pre}mb_app_prefs');
		while($row = $res->FetchArray(MYSQLI_ASSOC)) {
			$prefs[$row['key']] = $row['value'];
		}
		$res->Free();

		$fields = $this->admin_prefs_fields();

		foreach($fields as $key => $value) {
			if(isset($prefs[$key])) {
				$fields[$key]['value'] = $prefs[$key];
				if($fields[$key]['type'] == FIELD_CHECKBOX) {
					if($fields[$key]['value'] == 'yes')
						$fields[$key]['value'] = true;
					else
						$fields[$key]['value'] = false;
				} elseif($fields[$key]['type'] == FIELD_TEXT) {
					$fields[$key]['value'] = trim($fields[$key]['value']);
				}
			} else {
				$fields[$key]['value'] = $fields[$key]['default'];
				$this->set_app_pref($key, $fields[$key]['default']);
			}
		}

		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('fields', $fields);
		$tpl->assign('page', $this->_templatePath('mb_app.admin.prefs.tpl'));
	}

	/* ===== admin_prefs_fields =====
	return: The fields we will see in our admin page.
	*/
	function admin_prefs_fields() {
		global $lang_admin;

		$fields = array(
			// API access key for Firebase Cloud Messaging.
			'fcm_api_access_key' => array(
				'title' => $lang_admin['mb_app_fcm_api_access_key'] . ':',
				'type' => MB_APP_FIELD_PASSWORD,
				'default' => ''
			),
			// The title we want to show in our FCM notification.
			'fcm_notification_title' => array(
				'title' => 'FCM: ' . $lang_admin['mb_app_notification_title'] . ':',
				'type' => FIELD_TEXT,
				'default' => 'App'
			),
			// Topic for APNs notifications.
			'apns_topic' => array(
				'title' => $lang_admin['mb_app_apns_topic'] . ':',
				'type' => FIELD_TEXT,
				'default' => ''
			),
			// Password to the push certificate
			'apns_cert_password' => array(
				'title' => $lang_admin['mb_app_apns_cert_password'] . ':',
				'type' => MB_APP_FIELD_PASSWORD,
				'default' => ''
			),
			// Do we want to show ads? (Ads still aren't shown if the user group settings don't allow it)
			'ads_show' => array(
				'title' => $lang_admin['mb_app_ads_show'] . '?',
				'type' => FIELD_CHECKBOX,
				'default' => false
			),
			// Admob ID
			'ads_admobid' => array(
				'title' => $lang_admin['mb_app_ads_admobid'] . ':',
				'type' => FIELD_TEXT,
				'default' => ''
			),
			// Delete the user access after x days
			'access_autodelete' => array(
				'title' => $lang_admin['mb_app_deleteaccessafter'] . ':',
				'type' => FIELD_DROPDOWN,
				'options' => array(
					1 => '1 ' . $lang_admin['day'],
					3 => '3 ' . $lang_admin['mb_app_days_after'],
					7 => '7 ' . $lang_admin['mb_app_days_after'],
					14 => '14 ' . $lang_admin['mb_app_days_after'],
					21 => '21 ' . $lang_admin['mb_app_days_after'],
					30 => '30 ' . $lang_admin['mb_app_days_after'],
					90 => '90 ' . $lang_admin['mb_app_days_after'],
					180 => '180 ' . $lang_admin['mb_app_days_after'],
					365 => '365 ' . $lang_admin['mb_app_days_after']
				),
				'default' => 14
			),
			// Use the image proxy? (EXPERIMENTAL!)
			'imageproxy' => array(
				'title' => $lang_admin['mb_app_use_imageproxy'] . '?',
				'type' => FIELD_CHECKBOX,
				'default' => true
			)
		);

		return $fields;
	}

	/* ===== admin_debug =====
		Show a page where we can start our FCM debugging.
	*/
	function admin_debug() {
		global $tpl, $db;

		if(isset($_REQUEST['debug'])) {
			// Turn on error reporting and also show them on screen.
			// (We don't want to disable the error log since some users might expect to find the error messages here as well.)
			error_reporting(E_ALL);
			ini_set('display_errors', true);

			if($_REQUEST['debug'] == 'fcm') {
				$this->admin_debug_fcm();
				exit();
			}
			if($_REQUEST['debug'] == 'apns') {
				$this->admin_debug_apns();
				exit();
			}
		}

		$tpl->assign('pageURL', $this->_adminLink() . '&action=debug');
		$tpl->assign('page', $this->_templatePath('mb_app.admin.debug.tpl'));
	}

	/* ===== admin_debug_fcm =====
		Start fcm debugging.
	*/
	function admin_debug_fcm() {
		global $db;

		if(!isset($_REQUEST['user'])) {
			echo 'No user requested.';
			exit();
		}
		$user = $_REQUEST['user'];

		// Was the request an ID or the main email address?
		if(is_numeric($user))
			$res = $db->Query('SELECT id FROM {pre}users WHERE id=?', $user);
		else
			$res = $db->Query('SELECT id FROM {pre}users WHERE email=?', $user);

		if($res->RowCount() != 1) {
			echo 'User not found.';
			exit();
		}
		while($row = $res->FetchArray(MYSQLI_ASSOC)) {
			$userid = $row['id'];
		}
		$res->Free();

		$messages['de'] = 'FCM Test-Nachricht';
		$messages['en'] = 'FCM test message';

		$this->sendPushToID($userid, $messages, 1, true, false, true);
	}

	/* ===== admin_debug_apns =====
		Start fcm debugging.
	*/
	function admin_debug_apns() {
		global $db;

		if(!isset($_REQUEST['user'])) {
			echo 'No user requested.';
			exit();
		}
		$user = $_REQUEST['user'];

		// Was the request an ID or the main email address?
		if(is_numeric($user))
			$res = $db->Query('SELECT id FROM {pre}users WHERE id=?', $user);
		else
			$res = $db->Query('SELECT id FROM {pre}users WHERE email=?', $user);

		if($res->RowCount() != 1) {
			echo 'User not found.';
			exit();
		}
		while($row = $res->FetchArray(MYSQLI_ASSOC)) {
			$userid = $row['id'];
		}
		$res->Free();

		$messages['de'] = 'APNs Test-Nachricht';
		$messages['en'] = 'APNs test message';

		$this->sendPushToID($userid, $messages, 1, false, true, true);
	}

	/* ===== update_required =====
		Is an update required?

	return: True, if an update is required. False, if not.
	*/
	function update_required() {
		global $db;

		$required = true;

		$res = $db->Query('SELECT value FROM {pre}mb_app_prefs WHERE `key`=?', 'version');
		if($res->RowCount() == 1) {
			while($row = $res->FetchArray(MYSQLI_ASSOC)) {
				if($row['value'] == $this->version)
					$required = false;
				else
					$required = true;
			}
		}
		$res->Free();

		return $required;
	}

	/* ===== update_step =====
		Performs one update step to the next version.
		By using this mechanism, it is not necessary to install every single possible version of this plugin if you want to update from version x to version y.
		We remember what changed between these versions and update it step-by-step.
	*/
	function update_step() {
		global $db, $lang_admin;

		$from = '';
		$res = $db->Query('SELECT value FROM {pre}mb_app_prefs WHERE `key`=?', 'version');
		if($res->RowCount() == 1) {
			$row = $res->FetchArray(MYSQLI_ASSOC);
			$from = $row['value'];
		}
		$res->Free();

		// The following array represents all valid versions up to now. It is ordered by their release date (very first release on top of it, latest at the bottom).
		// An update step will always grab the current version from our table ($from) and perform one step to the next version.
		$versions = array(
			'0.9.7',
			'0.9.8',
			'0.9.9',
			'0.9.10',
			'0.9.11',
			'0.9.12',
			'0.9.15',
			'0.9.16',
			'1.0.0',
			'1.1.0',
			'1.2.0',
			'1.3.0',
			'1.4.0',
			'1.5.0',
			'1.6.0'
		);
		if(in_array($from, $versions)) {
			$next = array_search($from, $versions) + 1;
			if(!isset($versions[$next])) {
				$next = $this->version;
			} else
				$next = $versions[$next];
			$this->update_step_handler($from);

			$db->Query('REPLACE INTO {pre}mb_app_prefs(`key`, value) VALUES(?,?)', 'version', $next);

			PutLog(
				sprintf('mb_app: Successfully updated from version %s to version %s!',
					$from,
					$next
				),
				PRIO_PLUGIN,
				__FILE__,
				__LINE__
			);
		} else {
			PutLog(
				sprintf('mb_app: Update from version %s failed! The version number cannot be handled by the updater!',
					$from
				),
				PRIO_PLUGIN,
				__FILE__,
				__LINE__
			);

			exit($lang_admin['mb_app_update_error']);
		}
	}

	/* ===== update_step_handler =====
		Handles one update step based on a certain version (from)
	----- $from: Update from this version to the next.
	*/
	function update_step_handler($from) {
		global $db;

		switch($from) {
			case '1.0.0': {
				// Add an index for pushid.
				$db->Query('ALTER TABLE {pre}mb_app ADD INDEX(pushid)');
				$this->delete_app_pref('gcm_drysend');
				return;
			}
			case '1.4.0': {
				// Rename "gcm" to "fcm".
				$rename = array();
				$rename['gcm_api_access_key'] = 'fcm_api_access_key';
				$rename['gcm_notification_title'] = 'fcm_notification_title';

				foreach ($rename as $key => $value) {
					$db->Query('UPDATE {pre}mb_app_prefs SET `key`=? WHERE `key`=?',
						$value,
						$key
					);
				}
				return;
			}
		}
	}

	/* ===== AfterInit =====
		Here, we want to process the API request.
	*/
	function AfterInit() {
		global $db;
		// Only the mobile app calls have to be affected here
		if(!isset($_REQUEST['app-api']))
			return;

		if($this->update_required())
			exit();

		if(isset($_REQUEST['download']))
			$this->process_download();
		elseif(isset($_REQUEST['login']))
			$this->process_login();
		else
			$this->process_default();

		// We shouldn't reach this point.
		exit();
	}

	/* ===== process_download =====
		Process download requests.
	*/
	function process_download() {
		global $db;

		$headers = apache_request_headers();
		$usermail = $accesskey = $accesstoken = $timestamp = '';

		foreach($headers as $header => $value) {
			$header = strtolower($header);
			if($header == 'usermail')
				$usermail = $value;
			elseif($header == 'accesskey')
				$accesskey = $value;
			elseif($header == 'accesstoken')
				$accesstoken = $value;
			elseif($header == 'timestamp')
				$timestamp = $value;
		}
		$userid = $this->check_access($usermail, $accesskey, $accesstoken, $timestamp);
		if(!$userid) {
			header('HTTP/1.1 401 Unauthorized');
			exit();
		}

		$thisUser = _new('BMUser', array(
			$userid
		));

		$userRow = $thisUser->Fetch();
		if($userRow === false || !is_array($userRow) || $userRow['gesperrt'] != 'no') {
			exit();
		}

		$thisGroup = $thisUser->GetGroup();

		$groupRow = $thisGroup->Fetch();
		if($groupRow === false || !is_array($groupRow)) {
			PutLog(sprintf('Group <%d> of user <%s> not found!', $userRow['gruppe'], $userRow['email']), PRIO_WARNING, __FILE__, __LINE__);
			exit();
		}

		require(B1GMAIL_DIR . 'plugins/mb_app/download.php');
		exit();
	}

	/* ===== process_login =====
		Process login requests.
	*/
	function process_login() {
		global $db;

		if(!isset($_REQUEST['password'], $_REQUEST['email_full']))
			exit();

		$password = $_REQUEST['password'];
		$email = $_REQUEST['email_full'];

		list($result, $param) = BMUser::Login($email, $password, false, true);

		// Auth successful?
		if($result == USER_OK) {
			$json_response['status'] = 'success';

			// 32 Bytes = 256 Bits.
			$num_random_bytes = 32;

			// Generate random tokens.
			$crypto_strong = array();
			$crypto_strong[0] = $crypto_strong[1] = false;
			$random[0] = openssl_random_pseudo_bytes($num_random_bytes, $crypto_strong[0]);
			$random[1] = openssl_random_pseudo_bytes($num_random_bytes, $crypto_strong[1]);

			for($i = 0; $i < count($random); $i++) {
				if(!$random[$i] || !$crypto_strong[$i] || strlen($random[$i]) != $num_random_bytes) {
					PutLog('Failed to generate random token.', PRIO_ERROR, __FILE__, __LINE__);
					$json_response['status'] = 'error';
					echo json_encode($json_response);
					exit();
				}
			}
			$accesskey = hash('sha256', $random[0]);
			$accesstoken = hash('sha256', $random[1]);

			// Determine if the request was made by an Android, iOS or other device.
			$platform = $this->get_platform();

			$pushid = '';
			if(isset($_POST['pushid'])) {
				$pushid = $this->prepare_pushid($_POST['pushid'], $platform);
				// Remove all entries with the same push id.
				if(strlen($pushid) > 0)
					$db->Query('DELETE FROM {pre}mb_app WHERE pushid=? AND platform=?', $pushid, $platform);
			}
			$language = 'de';
			if(isset($_POST['language']) && $_POST['language'] == 'en')
				$language = 'en';
			// Create our new access data set.
			$db->Query('INSERT INTO {pre}mb_app(userid, added, lastaccessed, ipAdded, ipLastaccessed, accesskey, accesstoken, language, platform, pushid, pushactive)
						VALUES(?,?,?,?,?,?,?,?,?,?,?)',
				$param,
				time(),
				time(),
				$_SERVER['REMOTE_ADDR'],
				$_SERVER['REMOTE_ADDR'],
				$accesskey,
				$accesstoken,
				$language,
				$platform,
				$pushid,
				'yes'
			);

			$res = $db->Query('SELECT email FROM {pre}users WHERE id=?',
					$param);
			if($res->RowCount() == 1) {
				$row = $res->FetchArray(MYSQLI_ASSOC);
				$email = $row['email'];
			} else {
				// We shouldn't reach this point.
				exit();
			}
			$res->Free();

			$json_response['content']['accesskey'] = $accesskey;
			$json_response['content']['accesstoken'] = $accesstoken;
			$json_response['content']['usermail'] = $email;

			$thisUser = _new('BMUser', array(
				$param
			));

			$userRow = $thisUser->Fetch();
			if($userRow === false || !is_array($userRow) || $userRow['gesperrt'] != 'no') {
				exit();
			}

			$thisGroup = $thisUser->GetGroup();

			$groupRow = $thisGroup->Fetch();
			if($groupRow === false || !is_array($groupRow)) {
				PutLog(sprintf('Group <%d> of user <%s> not found!', $userRow['gruppe'], $userRow['email']), PRIO_WARNING, __FILE__, __LINE__);
				exit();
			}

			$this->InitialData($json_response, $thisUser, $userRow, $groupRow);

		} else {
			$json_response['status'] = 'error';

			if($result == USER_LOCKED || $result == USER_LOGIN_BLOCK)
				$json_response['error'] = 'user_locked';
			elseif($result == USER_BAD_PASSWORD || $result == USER_DOES_NOT_EXIST)
				$json_response['error'] = 'user_wronglogin';
			else
				$json_response['error'] = 'user_loginerror';
		}

		echo json_encode($json_response);
		exit();
	}

	/* ===== process_default =====
		Process requests that aren't handled by another specific "process_" function.
	*/
	function process_default() {
		global $bm_prefs, $currentCharset, $db, $groupRow, $thisGroup, $thisUser, $userRow;

		if(!isset($_POST['usermail'], $_POST['accesskey'], $_POST['accesstoken'], $_POST['timestamp']))
			exit();

		$usermail = $_POST['usermail'];
		$accesskey = $_POST['accesskey'];
		$accesstoken = $_POST['accesstoken'];
		$timestamp = $_POST['timestamp'];

		$userid = $this->check_access($usermail, $accesskey, $accesstoken, $timestamp);
		if(!$userid) {
			exit();
		}

		$thisUser = _new('BMUser', array(
			$userid
		));

		$userRow = $thisUser->Fetch();
		if($userRow === false || !is_array($userRow) || $userRow['gesperrt'] != 'no') {
			exit();
		}

		$thisGroup = $thisUser->GetGroup();

		$groupRow = $thisGroup->Fetch();
		if($groupRow === false || !is_array($groupRow)) {
			PutLog(sprintf('Group <%d> of user <%s> not found!', $userRow['gruppe'], $userRow['email']), PRIO_WARNING, __FILE__, __LINE__);
			exit();
		}

		if(!isset($_REQUEST['action']))
			exit();

		$json_response['status'] = 'success';

		/* ----- mail ----- */
		if($_REQUEST['action'] == 'email')
			require(B1GMAIL_DIR . 'plugins/mb_app/email.php');
		/* ----- addressbook ----- */
		elseif($_REQUEST['action'] == 'addressbook')
			require(B1GMAIL_DIR . 'plugins/mb_app/addressbook.php');
		/* ----- webdisk ----- */
		elseif($_REQUEST['action'] == 'webdisk')
			require(B1GMAIL_DIR . 'plugins/mb_app/webdisk.php');
		/* ----- settings ----- */
		elseif($_REQUEST['action'] == 'settings')
			require(B1GMAIL_DIR . 'plugins/mb_app/settings.php');
		/* ----- logout ----- */
		elseif($_REQUEST['action'] == 'logout')
			$db->Query('DELETE FROM {pre}mb_app WHERE userid=? AND accesskey=? ', $userid, $_POST['accesskey']);
		/* ----- updateUserData ----- */
		elseif($_REQUEST['action'] == 'updateUserData') {
			$this->InitialData($json_response, $thisUser, $userRow, $groupRow);
			if(isset($_POST['pushid'])) {
				$pushid = $this->prepare_pushID($_POST['pushid'], $this->get_platform());
				if(strlen($pushid) > 0)
					$db->Query('UPDATE {pre}mb_app SET pushid=? WHERE userid=? AND accesskey=?', $_POST['pushid'], $userid, $accesskey);
			}
		}

		echo json_encode($json_response);
		exit();
	}

	/* ===== get_platform =====
		Determine the platform the user has connected from.

	return: 'android', 'ios' or 'other'
	*/
	function get_platform() {
		$platform = 'other';

		if(stripos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false)
			$platform = 'android';
		else if(stripos($_SERVER['HTTP_USER_AGENT'], 'iPod') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false)
			$platform = 'ios';

		return $platform;
	}

	/* ===== check_access =====
	----- $email: Main mail address of the user.
	----- $accesskey: The accesskey of the current request.
	----- $accesstoken: The accesstoken that should belong to the accesskey.
	----- $timestamp: Timestamp when the user tried to get access. Currently, a request cannot be older than 5 minutes.

	return: user ID or false
	*/
	function check_access($email, $accesskey, $accesstoken, $timestamp) {
		global $db;

		if(time() - $timestamp > 300)
			return false;

		$res = $db->Query('SELECT id, passwort FROM {pre}users WHERE email=? ',
				$email);

		if($res->RowCount() == 1) {
			$row = $res->FetchArray(MYSQLI_ASSOC);
			$password = $row['passwort'];
			$userid = (int) $row['id'];
		} else {
			$res->Free();
			http_response_code(401);
			exit();
		}
		$res->Free();

		$res = $db->Query('SELECT accesstoken FROM {pre}mb_app WHERE userid=? AND accesskey=? ',
				$userid,
				$accesskey);
		if($res->RowCount() == 1) {
			$row = $res->FetchArray(MYSQLI_ASSOC);
			$selected_token = $row['accesstoken'];
		} else {
			http_response_code(401);
			exit();
		}
		$res->Free();

		/* -- app-api < 2 -- */
		if(isset($_REQUEST['app-api']) && (int) $_REQUEST['app-api'] == 1)
			$selected_token = sha1($timestamp . $selected_token);
		else
			$selected_token = hash('sha256', $timestamp . $selected_token);

		if($selected_token != $accesstoken) {
			$db->Query('DELETE FROM {pre}mb_app WHERE userid=? AND accesskey=? ',
				$userid,
				$accesskey
			);
			PutLog('A wrong access token has been used.', PRIO_WARNING, __FILE__, __LINE__);
			http_response_code(401);
			exit();
		}

		list($result, $param) = BMUser::Login($email, $password, false, false, '', true);
		if($result == USER_OK) {
			if(isset($_REQUEST['language'])) {
				if ($_REQUEST['language'] == 'de')
					$language = 'de';
				else
					$language = 'en';

				$db->Query('UPDATE {pre}mb_app SET lastaccessed=?, ipLastaccessed=?, language=? WHERE userid=? AND accesskey=?',
					time(),
					$_SERVER['REMOTE_ADDR'],
					$language,
					$userid,
					$accesskey
				);
			} else
				$db->Query('UPDATE {pre}mb_app SET lastaccessed=?, ipLastaccessed=? WHERE userid=? AND accesskey=?',
					time(),
					$_SERVER['REMOTE_ADDR'],
					$userid,
					$accesskey
				);

			return $userid;
		}
		return false;
	}

	/* ===== InitialData =====
		Add some useful data like prefs to our $json_response.

	----- &$json_response: Reference to our response variable.
	----- $thisUser
	----- $userRow
	----- $groupRow
	*/
	function InitialData(&$json_response, $thisUser, $userRow, $groupRow) {
		global $db;

		$json_response['content']['possiblesenders'] = $thisUser->GetPossibleSenders();
		$json_response['content']['userprefs']['defaultSender'] = $userRow['defaultSender'];

		// Ads
		$json_response['content']['prefs']['ads']['show'] = ($groupRow['ads'] == 'yes' && $this->get_app_pref('ads_show'));
		$json_response['content']['prefs']['ads']['admobid'] = $this->get_app_pref('ads_admobid');
	}

	/* ===== OnCron =====
		Delete old entries on cron.
	*/
	function OnCron() {
		global $db;

		$deleteTime = time() - $this->get_app_pref('access_autodelete') * TIME_ONE_DAY;
		$db->Query('DELETE FROM {pre}mb_app WHERE lastaccessed < ?',
			$deleteTime);
	}

	/* ===== OnDeleteUser =====
		Delete old entries when a user is deleted.
	----- $userID: The ID of the user that is currently getting deleted.
	*/
	function OnDeleteUser($userID) {
		global $db;
		$db->Query('DELETE FROM {pre}mb_app WHERE userid=?',
			$userID);
	}

	/* ===== FileHandler =====
		Add our prefs page.
	*/
	function FileHandler($file, $action) {
		if($file != 'prefs.php')
			return;

		$GLOBALS['prefsItems']['mb_app'] = true;
		$GLOBALS['prefsImages']['mb_app'] = 'plugins/templates/images/mb_app_48.png';
		$GLOBALS['prefsIcons']['mb_app'] = 'plugins/templates/images/mb_app_16.png';
	}

	/* ===== UserPrefsPageHandler =====
		Load our template file for the prefs page.
	*/
	function UserPrefsPageHandler($action) {
		global $db, $thisUser, $tpl;

		if($action != 'mb_app')
			return false;

		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'delete' && isset($_REQUEST['id'])) {
			$db->Query('DELETE FROM {pre}mb_app WHERE id = ? AND userid = ?',
				$_REQUEST['id'],
				$thisUser->_id);

			header('Location: prefs.php?action=mb_app&sid=' . session_id());
			exit();
		}

		$appaccounts = array();

		$res = $db->Query('SELECT id, added, lastaccessed, ipAdded, ipLastaccessed, platform, pushactive FROM {pre}mb_app WHERE userid=?',
			$thisUser->_id);

		while($row = $res->FetchArray(MYSQLI_ASSOC)) {
			$appaccounts[] = array(
				'id' => $row['id'],
				'added' => $row['added'],
				'lastaccessed' => $row['lastaccessed'],
				'ipAdded' => $row['ipAdded'],
				'ipLastaccessed' => $row['ipLastaccessed'],
				'platform' => $row['platform'],
				'pushactive' => $row['pushactive']
			);
		}
		$res->Free();

		$tpl->assign('appaccounts', $appaccounts);
		$tpl->assign('pageContent', $this->_templatePath('mb_app.prefs.main.tpl'));
		$tpl->display('li/index.tpl');

		return true;
	}

	/* ===== AfterReceiveMail =====
		Call our push function after receiving a mail.
	*/
	function AfterReceiveMail(&$mail, &$mailbox, &$user) {
		$count = $mailbox->GetMailCount(-1, true);

		// The user has probably set up a filter that will mark the email as read. If no unread emails were found in the inbox, we don't want to send a notification.
		if($count < 1)
			return;

		if($count == 1) {
			$messages['de'] = '1 ungelesene E-Mail';
			$messages['en'] = '1 unread email';
		} else {
			$messages['de'] = sprintf('%d ungelesene E-Mails', $count);
			$messages['en'] = sprintf('%d unread emails', $count);
		}
		$this->sendPushToID($user->_id, $messages, $count);
	}

	/* ===== prepare_pushid =====
	----- $pushid: The push ID sent from our app.
	----- $platform: The platform the request came from.

	return: An empty string if the push ID was invalid, or the push ID.
	*/
	function prepare_pushid($pushid, $platform) {
		if($platform == 'ios') {
			if(strlen($pushid) != 64 || !ctype_xdigit($pushid))
				$pushid = '';
		}

		return trim($pushid);
	}

	/* ===== sendPushToID =====
		Send push notifications to this user ID telling that a new mail was received by the system.
	----- $userid: ID of the user we want to send our push notification to
	----- $messages: An array containing the German and English message we want to send
	----- $badgeNumber: The badge number we want to set
	----- $use_fcm: Do we want to push using FCM?
	----- $use_apns: Do we want to push using APNs?
	----- $debug: Do we want to show debugging information?
	*/
	function sendPushToID($userid, $messages, $badgeNumber, $use_fcm = true, $use_apns = true, $debug = false) {
		global $db;

		// Make sure we are really using an integer as this might cause problems (the count probably won't be set at all)
		$badgeNumber = (int) $badgeNumber;

		$res = $db->Query('SELECT platform, pushid, language FROM {pre}mb_app WHERE userid=? AND pushactive=? AND length(pushid)>0',
			$userid,
			'yes'
		);
		if($res->RowCount() == 0) {
			if($debug)
				echo 'No device found we could push to.';
			return;
		}
		while($row = $res->FetchArray(MYSQLI_ASSOC)) {
			$push[$row['platform']][$row['language']][] = $row['pushid'];
		}
		$res->Free();
		// If we are in debug mode, determine whether we found devices we can push to
		if($debug) {
			if($use_fcm && !isset($push['android'])) {
				echo 'No device found we could push to.';
			}
			if($use_apns && !isset($push['ios'])) {
				echo 'No device found we could push to.';
			}
		}

		if (isset($push['android']) && $use_fcm) {
			if (isset($push['android']['de']))
				$this->sendPush_fcm($push['android']['de'], $messages['de'], $badgeNumber, $debug);
			if (isset($push['android']['en']))
				$this->sendPush_fcm($push['android']['en'], $messages['en'], $badgeNumber, $debug);
		}

		if (isset($push['ios']) && $use_apns) {
			if (isset($push['ios']['de']))
				$this->sendPush_apns($push['ios']['de'], $messages['de'], $badgeNumber, $debug);
			if (isset($push['ios']['en']))
				$this->sendPush_apns($push['ios']['en'], $messages['en'], $badgeNumber, $debug);
		}

	}

	/* ===== sendPush_fcm =====
		Send a push notification to Android devices using FCM.
	----- $pushTo: Array with push IDs
	----- $message: The message we want to send
	----- $badgeNumber: Set the application's badge number (if supported by the device)
	----- $debug: Do we want to show debugging information?
	*/
	function sendPush_fcm($pushTo, $message, $badgeNumber, $debug = false) {
		global $db;

		if($debug) {
			echo 'Starting debug: sendPush_fcm<br>';
			echo 'Pushing to: ' . json_encode($pushTo) . '<br>';
		}

		$api_access_key = trim($this->get_app_pref('fcm_api_access_key'));
		if(strlen($api_access_key) < 1) {
			if($debug)
				echo 'No FCM access key set<br>';
			return;
		}

		// Some devices have a status led - define the color in RGB when a notification was received.
		$ledColor = array(
			0,
			255,
			255,
			255
		);

		$msg = array(
			'message' => $message,
			'badge' => $badgeNumber,
			'title' => $this->get_app_pref('fcm_notification_title'),
			'soundname' => 'default',
			'ledColor' => $ledColor
		);

		// We need to set a collapse_key here to make sure that a new notification will override the old one when the device is not connected to the internet.
		$fields = array(
			'collapse_key' => 'single_notification',
			'registration_ids' => $pushTo,
			'data' => $msg
		);

		$headers = array(
			'Authorization: key=' . $api_access_key,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result_raw = curl_exec($ch);
		$result = json_decode($result_raw);
		if(empty($result)) {
			if($debug) {
				if($result_raw === false)
					echo 'curl error: ' . curl_error($ch) . '<br>';
				else
					echo 'Got bad response:<br>' . HTMLFormat($result_raw);
			}
			return;
		}

		if($debug)
			echo 'result: ' . HTMLFormat($result_raw) . '<br>';

		curl_close($ch);

		foreach($result->results as $key => $value) {
			if(isset($result->results[$key]->registration_id) || isset($result->results[$key]->error) && $result->results[$key]->error == 'NotRegistered') {
				// Delete old registration IDs.
				$db->Query('DELETE FROM {pre}mb_app WHERE pushid=? AND platform=?',
					$pushTo[$key],
					'android'
				);
			}
		}
	}

	/* ===== sendPush_apns =====
		Send a push notification to iOS devices using APNs.
	----- $pushTo: Array with push IDs
	----- $message: The message we want to send
	----- $badgeNumber: Set the application's badge number (if supported by the device)
	----- $debug: Do we want to show debugging information?
	*/
	function sendPush_apns($pushTo, $message, $badgeNumber, $debug = false) {
		global $db;

		if(MB_APP_USE_LEGACY_APNS_FUNCTIONS) {
			if($debug)
				echo 'sendPush_apns: Using legacy method.';

			$this->sendPush_apns_legacy($pushTo, $message, $badgeNumber, $debug);
			return;
		}

		if($debug) {
			echo 'Starting debug: sendPush_apns<br>';
			echo 'Pushing to: ' . json_encode($pushTo) . '<br>';
		}

		$apns_host = 'https://api.push.apple.com/3/device/';

		$apns_topic = trim($this->get_app_pref('apns_topic'));

		$apns_cert = B1GMAIL_DIR . 'plugins/mb_app/apns/apns_cert.pem';

		if(!file_exists($apns_cert)) {
			if($debug)
				echo 'File ' . HTMLFormat($apns_cert) . ' does not exist<br/>';
			return;
		}

		$apns_cert_password = trim($this->get_app_pref('apns_cert_password'));
		if(strlen($apns_cert_password) < 1) {
			if($debug)
				echo 'No APNs password set<br>';
			return;
		}

		$header = array(
			'apns-topic: ' . $apns_topic,
			'apns-collapse-id: single_notification'
		);

		$payload = array();
		$payload['aps'] = array(
			'alert' => $message,
			'badge' => $badgeNumber,
			'sound' => 'default'
		);
		$payload = json_encode($payload);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSLCERT, $apns_cert);
		curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $apns_cert_password);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$inactive_device_tokens = [];
		foreach($pushTo as $token) {
			if($debug)
				echo 'Processing device token: ' . HTMLFormat($token) . '<br>';

			$url = $apns_host . $token;
			curl_setopt($ch, CURLOPT_URL, $url);

			$response = curl_exec($ch);

			if(curl_errno($ch)) {
				if($debug)
					echo HTMLFormat(curl_error($ch)) . '<br>';
				continue;
			}
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			if($debug) {
				echo 'Response: ' . HTMLFormat($response) . '<br>';
				echo 'Response HTTP code: ' . HTMLFormat($http_code) . '<br>';
			}

			if($http_code == 410) {
				if($debug)
					echo 'The device token is inactive.<br>';
				$inactive_device_tokens[] = $token;
			}
		}
		curl_close($ch);

		echo 'Finished sending.<br>';

		if(count($inactive_device_tokens) > 0) {
			if($debug)
				echo 'Deleting inactive device tokens:' . json_encode($inactive_device_tokens) . '<br/>';

			$db->Query('DELETE FROM {pre}mb_app WHERE pushid IN ? AND platform=?',
				$inactive_device_tokens,
				'ios'
			);
		}
	}

	/* ===== sendPush_apns_legacy =====
		NOTE: This method will be removed in future versions.
		Send a push notification to iOS devices using APNs.

	----- $pushTo: Array with push IDs
	----- $message: The message we want to send
	----- $badgeNumber: Set the application's badge number (if supported by the device)
	----- $debug: Do we want to show debugging information?
	*/
	function sendPush_apns_legacy($pushTo, $message, $badgeNumber, $debug = false) {
		if($debug) {
			echo 'Starting debug: sendPush_apns<br />';
			echo 'Pushing to: ' . json_encode($pushTo) . '<br />';
		}

		$apns_host = 'ssl://gateway.push.apple.com:2195';
		$apns_cert = B1GMAIL_DIR . 'plugins/mb_app/apns/apns_cert.pem';

		if(!file_exists($apns_cert)) {
			if($debug)
				echo 'File ' . $apns_cert . ' does not exist<br />';
			return;
		}

		$apns_cert_password = trim($this->get_app_pref('apns_cert_password'));
		if(strlen($apns_cert_password) < 1) {
			if($debug)
				echo 'No APNs password set<br />';
			return;
		}

		$payload['aps'] = array(
			'alert' => $message,
			'badge' => $badgeNumber,
			'sound' => 'default'
		);
		$payload = json_encode($payload);

		foreach($pushTo as $key => $value) {
			$inner =
				// Device token
			      chr(1)
			    . pack('n', 32)
			    . pack('H*', $value)
				// Payload
			    . chr(2)
			    . pack('n', strlen($payload))
			    . $payload
				// Message ID
			    . chr(3)
			    . pack('n', 4)
			    . pack('N', 1)
				// Expiration date
			    . chr(4)
			    . pack('n', 4)
			    . pack('N', time() + TIME_ONE_WEEK)
				// Priority
			    . chr(5)
			    . pack('n', 1)
			    . chr(10);
			$notification =
			      chr(2)
			    . pack('N', strlen($inner))
			    . $inner;

			$stream = stream_context_create();
			stream_context_set_option($stream, 'ssl', 'local_cert', $apns_cert);
			stream_context_set_option($stream, 'ssl', 'passphrase', $apns_cert_password);

			$apns = stream_socket_client($apns_host, $errno, $errstr, 60, STREAM_CLIENT_CONNECT, $stream);
			if(!$apns) {
				// An error occured
				if($debug)
					echo 'An error occured trying to connect to APNs. Errno: ' . $errno . ', errstr: ' . $errstr . '<br />';
				return;
			}

			stream_set_blocking($apns, 0);
			// Send our message to the server
			$result = fwrite($apns, $notification, strlen($notification));
			// Close the connection
			fclose($apns);
		}
		if($debug)
			echo 'Finished sending<br />';

		$this->sendPush_apns_legacy_feedback($debug);
	}

	/* ===== sendPush_apns_legacy_feedback =====
		NOTE: This method will be removed in future versions.

		Connect to the APNs feedback service to retrieve expired push IDs.
		A token expires when the app has been uninstalled and one push message was sent to this device.
		The expired tokens will be returned only once.

	----- $debug: Do we want to show debugging information?
	*/
	function sendPush_apns_legacy_feedback($debug = false) {
		global $db;

		$apns_cert = B1GMAIL_DIR . 'plugins/mb_app/apns/apns_cert.pem';
		if(!file_exists($apns_cert))
			return;

		$apns_cert_password = trim($this->get_app_pref('apns_cert_password'));
		if(strlen($apns_cert_password) < 1)
			return;

	    $stream = stream_context_create();
	    stream_context_set_option($stream, 'ssl', 'local_cert', $apns_cert);
		stream_context_set_option($stream, 'ssl', 'passphrase', $apns_cert_password);

	    $apns = stream_socket_client('ssl://feedback.push.apple.com:2196', $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $stream);
		if(!$apns) {
			// An error occured
			if($debug)
				echo 'An error occured trying to connect to APNs feedback service. Errno: ' . $errno . ', errstr: ' . $errstr . '<br />';
			return;
		}

		$feedback = array();
	    while(!feof($apns)) {
	        $data = fread($apns, 38);
	        if(strlen($data)) {
	            $feedback[] = unpack("N1timestamp/n1length/H*token", $data);
	        }
	    }
	    fclose($apns);

		foreach($feedback as $key => $value) {
			if($debug)
				echo 'Feedback: Delete token ' . $value['token'] . '<br />';

			// Delete the expired token. Make sure this token hasn't been re-registered by checking the timestamp.
			if(isset($value['timestamp']) && isset ($value['token']))
				$db->Query('DELETE FROM {pre}mb_app WHERE pushid=? AND added<? AND platform=?', $value['token'], $value['timestamp'], 'ios');
		}
	}

	/* ===== formatAppEMailText =====
	(This function is based on the one found in serverlib/common.inc.php)

	----- $in: Input
	----- $html: For HTML output?
	*/
	function formatAppEMailText($in, $html = true) {
		global $currentCharset;

		if(strtolower($currentCharset) == 'utf-8' || strtolower($currentCharset) == 'utf8')
			$pcreSuffix = 'u';
		else
			$pcreSuffix = '';

		// html entities
		$in = HTMLFormat($in);

		// for HTML output?
		if($html) {
			$in = nl2br($in);

			// tabs
			$in = str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;', $in);

			// links
			$bmLinks = array();
			$in = preg_replace_callback("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/=?&@%]/",
				function($matches) use(&$bmLinks) {
					$bmLinks[] = $matches[0];
					return(sprintf(':::_b1gMailLink:%d_:::', count($bmLinks)-1));
				},
				$in);

			// build links
			foreach($bmLinks as $i=>$link) {
				$in = str_replace(sprintf(':::_b1gMailLink:%d_:::', $i),
								  sprintf('<a href="%s" target="_blank">%s</a>', $link, $link),
								  $in);
			}
		}

		return $in;
	}

	/* ===== formatAppEMailHTMLText =====
	(This function is based on the one found in serverlib/common.inc.php)

	----- $in: Input
	----- $showExternal: Enable external objects and scripts?
	----- $attachments: Attachment list (for CID replacement)
	----- $mailID: The ID of the mail for which we want to format the text.
	*/
	function formatAppEMailHTMLText($in, $showExternal = false, $attachments = array(), $mailID = -1) {
		global $currentCharset;

		if(!class_exists('MB_APP_BMHTMLEMailFormatter'))
			include(B1GMAIL_DIR . 'plugins/mb_app/serverlib/htmlemailformatter.class.php');

		$formatter = _new('MB_APP_BMHTMLEMailFormatter', array($in, $currentCharset));
		$formatter->setLevel(0);
		$formatter->setAllowExternal($showExternal);
		$formatter->setAttachments($attachments);
		$formatter->setReplyMode(false);

		$formatter->setComposeBaseURL('mailto:');
		$formatter->setAttachmentBaseURL('inlineAttachment/id=' . $mailID . '/attachment=');

		$result = $formatter->format();

		return $result;
	}
}

// Register the plugin.
$plugins->registerPlugin('mb_app');
?>
