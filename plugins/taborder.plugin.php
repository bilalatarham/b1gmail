<?php
/*
 * b1gMail Tab order plugin
 * (c) 2002-2009 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: taborder.plugin.php,v 1.3 2009/04/15 09:57:54 patrick Exp $
 *
 */

/**
 * Tab order plugin
 *
 */
class TabOrderPlugin extends BMPlugin 
{
	function TabOrderPlugin()
	{
		// extract version
		sscanf('$Revision: 1.3 $', chr(36) . 'Revision: %d.%d ' . chr(36),
			$vMajor,
			$vMinor);
		
		// plugin info
		$this->type					= BMPLUGIN_DEFAULT;
		$this->name					= 'Tab order';
		$this->author				= 'Patrick Schlangen';
		$this->web					= 'http://www.b1g.de/';
		$this->mail					= 'ps@b1g.de';
		$this->version				= sprintf('%d.%d', $vMajor, $vMinor);
		$this->update_url			= 'http://my.b1gmail.com/update_service/';
		$this->website				= 'http://my.b1gmail.com/details/58/';
		
		$this->admin_pages 			= true;
		$this->admin_page_title 	= 'Tab-Reihenfolge';
	}
	
	function Install()
	{
		// install database structure
		$databaseStructure = 
			  'YToxOntzOjE3OiJibTYwX21vZF90YWJvcmRlciI7YToyOntzOjY6ImZpZWxkcyI7YToyOntpOjA'
			. '7YTo2OntpOjA7czozOiJrZXkiO2k6MTtzOjExOiJ2YXJjaGFyKDY0KSI7aToyO3M6MjoiTk8iO2'
			. 'k6MztzOjM6IlBSSSI7aTo0O3M6MDoiIjtpOjU7czowOiIiO31pOjE7YTo2OntpOjA7czo1OiJvc'
			. 'mRlciI7aToxO3M6NzoiaW50KDExKSI7aToyO3M6MjoiTk8iO2k6MztzOjA6IiI7aTo0O3M6MToi'
			. 'MCI7aTo1O3M6MDoiIjt9fXM6NzoiaW5kZXhlcyI7YToxOntzOjc6IlBSSU1BUlkiO2E6MTp7aTo'
			. 'wO3M6Mzoia2V5Ijt9fX19';
		$databaseStructure = unserialize(base64_decode($databaseStructure));
		SyncDBStruct($databaseStructure);
		
		// log
		PutLog(sprintf('%s v%s installed',
			$this->name,
			$this->version),
			PRIO_PLUGIN,
			__FILE__,
			__LINE__);
		
		return(true);
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			$lang_admin['taor_taborder']			= 'Tab-Reihenfolge';
			$lang_admin['taor_pos']					= 'Position';
		}
		else 
		{
			$lang_admin['taor_taborder']			= 'Tab order';
			$lang_admin['taor_pos']					= 'Position';
		}
	}
	
	function AdminHandler()
	{
		global $db, $tpl, $bm_prefs, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'start';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['taor_taborder'],
				'link'		=> $this->_adminLink() . '&',
				'active'	=> $_REQUEST['action'] == 'start'
			)
		);
		$tpl->assign('tabs', $tabs);
		$tpl->assign('action', $_REQUEST['action']);
		$tpl->assign('pageURL', $this->_adminLink());

		if(isset($_REQUEST['save']) && isset($_REQUEST['order']) && is_array($_REQUEST['order']))
		{
			foreach($_REQUEST['order'] as $key=>$order)
				$db->Query('REPLACE INTO {pre}mod_taborder(`key`,`order`) VALUES(?,?)',
					$key,
					$order);
		}
		
		$tpl->assign('usertpldir', B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
		$tpl->assign('pageTabs', $this->_getPageTabs());
		$tpl->assign('page', $this->_templatePath('taor.taborder.tpl'));
	}
	
	function BeforePageTabsAssign(&$pageTabs)
	{
		global $db;
		
		$res = $db->Query('SELECT `key`,`order` FROM {pre}mod_taborder');
		while($row = $res->FetchArray(MYSQL_ASSOC))
		{
			if(isset($pageTabs[$row['key']]))
				$pageTabs[$row['key']]['order'] = $row['order'];
		}
		$res->Free();
		
		uasort($pageTabs, 'TemplateTabSort');
	}
	
	function _getPageTabs()
	{
		global $lang_user, $plugins, $groupRow, $bm_prefs;
		
		$pageTabs = array(
			'start' => array(
				'icon'		=> 'start',
				'order'		=> 100
			),
			'email' => array(
				'icon'		=> 'email',
				'text'		=> $lang_user['email'],
				'order'		=> 200
			),
			'sms' => array(
				'icon'		=> 'sms',
				'text'		=> $lang_user['sms'],
				'order'		=> 300
			),
			'organizer' => array(
				'icon'		=> 'organizer',
				'text'		=> $lang_user['organizer'],
				'order'		=> 400
			),
			'webdisk' => array(
				'icon'		=> 'webdisk',
				'text'		=> $lang_user['webdisk'],
				'order'		=> 500
			)
		);
		
		if(!isset($groupRow) || !is_array($groupRow))
			$groupRow = array('id' => $bm_prefs['std_gruppe']);
		
		$moduleResult = $plugins->callFunction('getUserPages', false, true, array(true));
		foreach($moduleResult as $userPages)
			$pageTabs = array_merge($pageTabs, $userPages);
			
		$pageTabs = array_merge($pageTabs, array(
			'prefs' => array(
				'icon'		=> 'prefs',
				'text'		=> $lang_user['prefs'],
				'order'		=> 600
			)));
		
		// sort by order
		uasort($pageTabs, 'TemplateTabSort');
		ModuleFunction('BeforePageTabsAssign', array(&$pageTabs));
		
		return($pageTabs);
	}
}

/**
 * register plugin
 */
$plugins->registerPlugin('TabOrderPlugin');
?>