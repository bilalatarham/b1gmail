<?php
/*
 * b1gMail widget order plugin
 * (c) 2002-2008 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: widgetorder.plugin.php,v 1.2 2008/10/30 19:29:07 patrick Exp $
 *
 */

/**
 * Widget order plugin
 *
 */
class WidgetOrderPlugin extends BMPlugin 
{
	function WidgetOrderPlugin()
	{
		// extract version
		sscanf('$Revision: 1.2 $', chr(36) . 'Revision: %d.%d ' . chr(36),
			$vMajor,
			$vMinor);
		
		// plugin info
		$this->type					= BMPLUGIN_DEFAULT;
		$this->name					= 'Widget layouts';
		$this->author				= 'Patrick Schlangen';
		$this->web					= 'http://www.b1g.de/';
		$this->mail					= 'ps@b1g.de';
		$this->version				= sprintf('%d.%d', $vMajor, $vMinor);
		
		$this->admin_pages 			= true;
		$this->admin_page_title 	= 'Widget-Layouts';
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			$lang_admin['wior_startboard']			= 'Start-Dashboard';
			$lang_admin['wior_organizerboard']		= 'Organizer-Dashboard';
			$lang_admin['wior_defaultlayout']		= 'Standard-Layout';
			$lang_admin['wior_addremove']			= 'Widgets hinzuf&uuml;gen/entfernen';
			$lang_admin['wior_resetdesc']			= 'Das Widget-Layout aller Benutzer, die einer der folgenden Gruppen angeh&ouml;ren, auf das aktuell gespeicherte Standard-Layout zur&uuml;cksetzen:';
		}
		else 
		{
			$lang_admin['wior_startboard']			= 'Start dashboard';
			$lang_admin['wior_organizerboard']		= 'Organizer dashboard';
			$lang_admin['wior_defaultlayout']		= 'Default layout';
			$lang_admin['wior_addremove']			= 'Add/remove widgets';
			$lang_admin['wior_resetdesc']			= 'Reset the widget layout of all users, who belong to one of the following groups, to the default layout:';
		}
	}
	
	function AdminHandler()
	{
		global $db, $tpl, $bm_prefs, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'start';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['wior_startboard'],
				'link'		=> $this->_adminLink() . '&',
				'active'	=> $_REQUEST['action'] == 'start'
			),
			1 => array(
				'title'		=> $lang_admin['wior_organizerboard'],
				'link'		=> $this->_adminLink() . '&action=organizer&',
				'active'	=> $_REQUEST['action'] == 'organizer'
			)
		);
		
		if(!class_exists('BMDashboard'))
			include(B1GMAIL_DIR . 'serverlib/dashboard.class.php');
		
		if($_REQUEST['action'] == 'start')
		{
			$widgetType = BMWIDGET_START;
			$orderKey = 'widget_order_start';
		}
		else if($_REQUEST['action'] == 'organizer')
		{
			$widgetType = BMWIDGET_ORGANIZER;
			$orderKey = 'widget_order_organizer';
		}
		else
			die('Invalid action');
		
		$dashboard = _new('BMDashboard', array($widgetType));
		$tpl->assign('tabs', $tabs);
		$tpl->assign('action', $_REQUEST['action']);
		$tpl->assign('pageURL', $this->_adminLink());
		
		//
		// overview
		//
		if(!isset($_REQUEST['do']))
		{
			// save order?
			if(isset($_REQUEST['saveOrder'])
				&& isset($_REQUEST['order']))
			{
				$widgetOrder = trim($_REQUEST['order']);
				
				if($dashboard->checkWidgetOrder($widgetOrder))
				{
					$db->Query('UPDATE {pre}prefs SET ' . $orderKey . '=?',
						$widgetOrder);
					$bm_prefs[$orderKey] = $widgetOrder;
				}
			}
			
			// reset order?
			if(isset($_REQUEST['resetOrder'])
				&& isset($_REQUEST['groups'])
				&& is_array($_REQUEST['groups']))
			{
				$db->Query('UPDATE {pre}userprefs SET `value`=? WHERE `key`=? AND `userid` IN (SELECT `id` FROM {pre}users WHERE `gruppe` IN ?)',
					$bm_prefs[$orderKey],
					$orderKey == 'widget_order_start' ? 'widgetOrderStart' : 'widgetOrderOrganizer',
					$_REQUEST['groups']);
			}
			
			$widgetOrder = $bm_prefs[$orderKey];
			$widgets = $this->_getWidgetArray($widgetType, $widgetOrder);
			
			$tpl->assign('groups', BMGroup::GetSimpleGroupList());
			$tpl->assign('widgetOrder', $widgetOrder);
			$tpl->assign('widgets', $widgets);
			$tpl->assign('page', $this->_templatePath('wior.widgetorder.tpl'));
		}
		
		//
		// add/remove
		//
		else if($_REQUEST['do'] == 'addremove')
		{
			$widgetOrder = $bm_prefs[$orderKey];
			
			// save?
			if(isset($_REQUEST['save']))
			{
				$widgetOrder = $dashboard->generateOrderStringFromPostForm($widgetOrder);
				$db->Query('UPDATE {pre}prefs SET ' . $orderKey . '=?',
					$widgetOrder);
				header('Location: ' . $this->_adminLink(true) . '&action=' . $_REQUEST['action']);
				exit();
			}
			
			$tpl->assign('possibleWidgets', $dashboard->getPossibleWidgets($widgetOrder));
			$tpl->assign('page', $this->_templatePath('wior.addremove.tpl'));
		}
	}
	
	function _getWidgetArray($type, $widgetOrder)
	{
		global $plugins;
		
		$widgetList = explode(',', str_replace(';', ',', $widgetOrder));	
		$tplWidgets = array();
		$widgets = $plugins->getWidgetsSuitableFor($type);
		foreach($widgets as $widget)
		{
			if(in_array($widget, $widgetList))
			{
				$tplWidgets[$widget] = array(
					'title'			=> $plugins->getParam('widgetTitle', $widget)
				);
			}
		}
		
		return($tplWidgets);
	}
}

/**
 * register plugin
 */
$plugins->registerPlugin('WidgetOrderPlugin');
?>