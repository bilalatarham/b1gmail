<?php
// If this file has been included from somewhere else, don't allow it to be proceeded!
if(!isset($json_response))
	exit();

// No do-Request? Something must have gone wrong
if(!isset($_REQUEST['do']))
	exit();

if(!class_exists('BMAddressbook'))
	include('./serverlib/addressbook.class.php');

$book = _new('BMAddressbook', array($userRow['id']));

/* ===== getaddresslist ===== */
if($_REQUEST['do'] == "getaddresslist") {
	$addressList = $book->GetAddressBook('*', -1, 'nachname', 'ASC', true);
	foreach($addressList as $listKey => $listValue) {
		foreach($listValue as $entryKey => $entryValue) {
			$data = @unserialize($entryValue["picture"]);
			if(!$data)
				continue;
			$picture = 'data:' . $data['mimeType'] . ';base64,' . base64_encode($data['data']);
			$addressList[$listKey][$entryKey]['picture'] = $picture;
		}
	}
	$json_response['content']['addresslist'] = $addressList;
}
?>
