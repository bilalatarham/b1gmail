<?php
if(!isset($thisUser))
	exit();
// No do-Request? Something must have gone wrong
if(!isset($_REQUEST['action']) || !isset($_REQUEST['do']))
	exit();

/* ===== Download mail attachment ===== */
if($_REQUEST['action'] == 'email' && $_REQUEST['do'] == 'downloadAttachment'
		&& isset($_REQUEST['attachment'])
		&& isset($_REQUEST['id']))
{
if(!class_exists('BMMailbox'))
	include(B1GMAIL_DIR.'serverlib/mailbox.class.php');
$mailbox = _new('BMMailbox', array($userRow['id'], $userRow['email'], $thisUser));

	$mail = $mailbox->GetMail((int)$_REQUEST['id']);

	if($mail !== false)
	{
		$parts = $mail->GetPartList();
		if(isset($parts[$_REQUEST['attachment']]))
		{
			$part = $parts[$_REQUEST['attachment']];

			header('Pragma: public');
			if(isset($part['charset']) && trim($part['charset']) != '')
				header('Content-Type: ' . $part['content-type'] . '; charset=' . $part['charset']);
			else
				header('Content-Type: ' . $part['content-type']);
			header(sprintf('Content-Disposition: attachment; filename="%s"',
						addslashes($part['filename'])));

			$attData = &$part['body'];
			$attData->Init();
			while($block = $attData->DecodeBlock(PART_CHUNK_SIZE))
			{
				echo $block;
			}
			$attData->Finish();
		}
	} else {
		http_response_code(403);
	}
	exit();
}
/* ===== Download webdisk file ===== */
else if($_REQUEST['action'] == 'webdisk' && $_REQUEST['do'] == 'downloadFile'
		&& isset($_REQUEST['id'])) {

	if(!class_exists('BMWebdisk'))
		include('./serverlib/webdisk.class.php');

	$webdisk = _new('BMWebdisk', array($userRow['id']));

	$fileInfo = $webdisk->GetFileInfo((int)$_REQUEST['id']);
	if($fileInfo !== false)	{
		if($groupRow['traffic'] <= 0 || ($userRow['traffic_down'] + $userRow['traffic_up'] + $fileInfo['size']) <= $groupRow['traffic'] + $userRow['traffic_add']) {
			// ok
			$speedLimit = $groupRow['wd_member_kbs'] <= 0 ? -1 : $groupRow['wd_member_kbs'];
			$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
				$fileInfo['size'],
				$userRow['id']);

			// send file
			header('Pragma: public');
			header('Content-Type: ' . $fileInfo['contenttype']);
			header('Content-Length: ' . $fileInfo['size']);
			header('Content-Disposition: attachment' . '; filename="' . addslashes($fileInfo['dateiname']) . '"');
			Add2Stat('wd_down', ceil($fileInfo['size']/1024));
			SendFileFP(BMBlobStorage::CreateProvider($fileInfo['blobstorage'], $userRow['id'])->loadBlob(BMBLOB_TYPE_WEBDISK, $fileInfo['id']),
				$speedLimit);
			exit();
		}
	}

	http_response_code(403);
	exit();
}
?>
