<?php
// If this file has been included from somewhere else, don't allow it to be proceeded!
if(!isset($json_response))
	exit();

// No do-Request? Something must have gone wrong
if(!isset($_REQUEST['do']))
	exit();

if($_REQUEST['do'] == 'getSettings') {
	$res = $db->Query('SELECT pushactive FROM {pre}mb_app WHERE userid=? AND accesskey=? ', $thisUser->_id, $accesskey);
	if ($res->RowCount() == 1) {
		while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
			$json_response['content']['pushactive'] = $row['pushactive'];
		}
	}
	$res->Free();
} elseif($_REQUEST['do'] == 'updateSettings') {
	if(isset($_REQUEST['pushactive']) && ($_REQUEST['pushactive'] == 'yes' || $_REQUEST['pushactive'] == 'no'))  {
		$db->Query('UPDATE {pre}mb_app SET pushactive=? WHERE userid=? AND accesskey=?', $_REQUEST['pushactive'], $userid, $accesskey);
	}
}

?>
