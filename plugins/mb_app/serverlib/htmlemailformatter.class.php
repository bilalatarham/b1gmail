<?php
// Based on htmlemailformatter.class.php found in b1gmail core.
if(!defined('B1GMAIL_INIT'))
	die('Directly calling this file is not supported');

if(!class_exists('BMHTMLEMailFormatter'))
	include(B1GMAIL_DIR . 'serverlib/htmlemailformatter.class.php');

if(class_exists('DOMDocument')) {
	class MB_APP_BMHTMLEMailFormatter extends BMHTMLEMailFormatter {
		protected function formatAttributes($node, $lcTag)
		{
			$result = '';
			$addTarget = '_blank';

			foreach($node->attributes as $name=>$attr)
			{
				$lcName = strtolower($name);
				$val = $node->getAttribute($name);
				$allow = false;

				for($i=0; $i<=$this->level; ++$i)
				{
					if(in_array($lcName, $this->allowedAttributes[$i]))
					{
						$allow = true;
						break;
					}
				}

				if($lcName == 'href' && preg_match('~^(http|https|ftp)://~i', $val))
				{
					// Changed for mb_app: Don't use "deref.php".
					//$val = ($this->replyMode ? '' : 'deref.php?') . $val;
					$allow = true;
				}
				else if($lcName == 'href' && preg_match('~^mailto:~i', $val))
				{
					$mailAddr = ExtractMailAddress($val);
					if($mailAddr)
					{
						$val = ($this->replyMode ? '' : $this->composeBaseURL) . urlencode(ExtractMailAddress($val));
						$addTarget = '_top';
						$allow = true;
					}
				}
				else if(($lcName == 'src' && $lcTag == 'img') || $lcName == 'background')
				{
					$isExternal = preg_match('~^.+://~', $val);
					$allow = !$isExternal || $this->allowExternal;

					if(!$isExternal && preg_match('~^cid:~i', $val) && !$this->replyMode)
					{
						$cid = substr($val, 4);
						if(isset($this->cidMap[$cid]))
						{
							$val = $this->attachmentBaseURL . $this->cidMap[$cid];
						}
						else
							$allow = false;
					}

					if($isExternal && !$this->allowExternal)
						$this->externalFiltered = true;
				}
				else if($lcName == 'style')
				{
					if(!$this->isExternalCSS($val) || $this->allowExternal)
						$allow = true;

					if($allow && preg_match('~cid:~i', $val) && !$this->replyMode)
					{
						foreach($this->cidMap as $cidKey=>$cidVal)
							$val = str_replace('cid:' . $cidKey, $this->attachmentBaseURL . $cidVal, $val);
					}
				}

				if($allow)
					$result .= ' ' . $name . '="' . htmlspecialchars($val) . '"';
			}

			if($lcTag == 'a' && $addTarget)
				$result .= ' target="' . $addTarget . '"';

			return($result);
		}
	}
} else {
	// PHP DOM extension not installed, fall back to legacy approach.
	class MB_APP_BMHTMLEMailFormatter extends BMHTMLEMailFormatter {
		public function format()
		{
			$scriptParams = array(
				'onabort', 'onblur', 'onchange', 'onclick', 'ondblclick', 'onerror', 'onfocus',
				'onkeydown', 'onkeypress', 'onkeyup', 'onload', 'onmousedown', 'onmousemove',
				'onmouseout', 'onmouseover', 'onmouseup', 'onreset', 'onresize', 'onselect', 'onsubmit',
				'onunload'
			);

			$in = $this->htmlCode;
			$in = preg_replace("/(" . implode('|', $scriptParams) . ")=([\"']*)/i", "blocked_\\1=\\2", $in);
			$in = preg_replace("/<(object|embed|script)([^>]*)>/i", "<span blocked=\"\\1\" style=\"display:none;\">", $in);
			$in = preg_replace("/<\/(object|embed|script)([^>]*)>/i", "</span>", $in);
			$in = preg_replace("/href=([\"']*)javascript\:/i", "blocked_href=\\1javascript:", $in);
			$in = preg_replace_callback('/<[^>]*>/', array($this, 'tagProcessor'), $in);

			if(!$this->allowExternal)
			{
				$in = preg_replace("/(src|href|background)=([\"']*)([h])/i", "blocked_\\1=\\2\\3", $in);
			}
			else
			{
				// Changed for mb_app: Don't use deref.php.
				$in = preg_replace("/href=\"([a-zA-Z]{3,6}):\/\//i", 'target="_blank" href="\\1://', $in);
			}

			// Changed for mb_app: Use $this->composeBaseURL instead of $composeBaseURL.
			$in = preg_replace("/href=\"mailto\:([a-zA-Z0-9\.\_-]*\@[a-zA-Z0-9\.\_-]*\.[a-zA-Z0-9\.\_-]*)([\?]*)([^&]*)\"/i", 'target="_top" href="' . $this->composeBaseURL . '\\1&\\3"', $in);

			if(count($this->cidMap) > 0)
			{
				foreach($this->cidMap as $cid=>$key)
				{
					if(!empty($cid))
					{
						$in = str_replace('"cid:' . $cid . '"',
											'"' . $this->attachmentBaseURL . $key . '"',
											$in);
						$in = str_replace('\'cid:' . $cid . '\'',
											'\'' . $this->attachmentBaseURL . $key . '\'',
											$in);
					}
				}
			}

			return($in);
		}
	}
}

?>
