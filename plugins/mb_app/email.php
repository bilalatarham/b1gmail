<?php
// If this file has been included from somewhere else, don't allow it to be proceeded!
if(!isset($json_response))
	exit();

// No do-Request? Something must have gone wrong
if(!isset($_REQUEST['do']))
	exit();

// Create a new BMMailbox
if(!class_exists('BMMailbox'))
	include(B1GMAIL_DIR.'serverlib/mailbox.class.php');
$mailbox = _new('BMMailbox', array($userRow['id'], $userRow['email'], $thisUser));

/* ----- getfolder ----- */
if($_REQUEST['do'] == 'getfolder' && isset($_REQUEST['folderid'])) {
	if(!isset($folderList))
		$folderList = $mailbox->GetFolderList(true);
	// The folder doesn't exist?
	if(!isset($folderList[$_REQUEST['folderid']]))
		exit();

	$folderID = (int) $_REQUEST['folderid'];

	if(isset($_REQUEST['page']))
		$pageNo = (int) $_REQUEST['page'];
	else
		$pageNo = 1;
	$mailsPerPage = 25;
	$mailCount = $mailbox->GetMailCount($folderID);
	$pageCount = max(1, ceil($mailCount / max(1, $mailsPerPage)));
	$pageNo = min($pageCount, max(1, $pageNo));

	$mailList = $mailbox->GetMailList($folderID, $pageNo, $mailsPerPage);

	// Are we currently viewing an intelligent folder? If so: Find out in which (non-intelligent) folders the mails were really saved.
	// It is necessary for our "restore mail" function to find out if the email will be completely deleted or only moved to the trash after sending a delete request.
	if($mailbox->IsIntelligentFolder($folderID)) {
		$res = $db->Query('SELECT id, folder FROM {pre}mails WHERE id IN ?', array_keys($mailList));
			while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
				if(isset($mailList[(int) $row['id']]))
					$mailList[(int) $row['id']]['original_folder'] = $row['folder'];
			}
		$res->Free();
	}

	$json_response['content']['maillist'] = $mailList;
}
/* ----- emptyFolder ----- */
elseif($_REQUEST['do'] == 'emptyFolder' && isset($_REQUEST['folderid'])) {
	$folderID = (int) $_REQUEST['folderid'];
	// Only allow users to empty the trash. If we, some day, add an option to remove other folders, we should check if it really exists in our folderlist first.
	if($folderID != FOLDER_TRASH) {
		$json_response['status'] = 'error';
		return;
	}
	$mailbox->EmptyFolder($folderID);
}
/* ----- action ----- */
elseif($_REQUEST['do'] == 'action') {

		//
		// collect IDs
		//
		$mailIDs = array();

		if(isset($_POST['selectedMailIDs'], $_REQUEST['massAction']) && trim($_POST['selectedMailIDs']) != '')	{
			$mailAction = $_REQUEST['massAction'];
			$_mailIDs = explode(';', $_POST['selectedMailIDs']);
			foreach($_mailIDs as $_mailID)
				$mailIDs[] = (int) $_mailID;
		} elseif(isset($_REQUEST['id'], $_REQUEST['mailAction']))	{
			$mailAction = $_REQUEST['mailAction'];
			$mailIDs[] = (int) $_REQUEST['id'];
		} else {
			exit();
		}

		//
		// execute action
		//
		if(count($mailIDs) > 0)	{
			// delete
			if($mailAction == 'delete')	{
				foreach($mailIDs as $mailID)
					$mailbox->DeleteMail($mailID);
			}

			// markread
			else if($mailAction == 'markread') {
				foreach($mailIDs as $mailID)
					$mailbox->FlagMail(FLAG_UNREAD,
						false,
						$mailID);
			}

			// markunread
			else if($mailAction == 'markunread') {
				foreach($mailIDs as $mailID)
					$mailbox->FlagMail(FLAG_UNREAD,
						true,
						$mailID);
			}

			// mark
			else if($mailAction == 'mark') {
				foreach($mailIDs as $mailID)
					$mailbox->FlagMail(FLAG_FLAGGED,
						true,
						$mailID);
			}

			// unmark
			else if($mailAction == 'unmark') {
				foreach($mailIDs as $mailID)
					$mailbox->FlagMail(FLAG_FLAGGED,
						false,
						$mailID);
			}

			// mark done
			else if($mailAction == 'done') {
				foreach($mailIDs as $mailID)
					$mailbox->FlagMail(FLAG_DONE,
						true,
						$mailID);
			}

			// unmark done
			else if($mailAction == 'undone') {
				foreach($mailIDs as $mailID)
					$mailbox->FlagMail(FLAG_DONE,
						false,
						$mailID);
			}

			// mark as spam
			else if($mailAction == 'markspam') {
				foreach($mailIDs as $mailID)
					$mailbox->SetSpamStatus($mailID, true);
			}

			// mark as nonspam
			else if($mailAction == 'marknonspam') {
				foreach($mailIDs as $mailID)
					$mailbox->SetSpamStatus($mailID, false);
			}

			// move to group
			else if(substr($mailAction, 0, 7) == 'moveto_') {
				$destFolderID = (int) substr($_REQUEST['massAction'], 7);
				$mailbox->MoveMail($mailIDs, $destFolderID);
			}

			// move to group
			else if(substr($mailAction, 0, 6) == 'color_') {
				$newColor = (int) substr($_REQUEST['massAction'], 6);
				$mailbox->ColorMail($mailIDs, $newColor);
			}
		}
}
/* ----- readmail ----- */
elseif($_REQUEST['do'] == 'readmail' && isset($_REQUEST['id'])) {
	$mail = $mailbox->GetMail((int) $_REQUEST['id']);
	$possibleSenders = $thisUser->GetPossibleSenders();
	if($mail === false)
		exit();

	// unread? => mark as read
	if(($mail->flags & FLAG_UNREAD) != 0 && !isset($_REQUEST['unread']))
		$mailbox->FlagMail(FLAG_UNREAD, false, (int) $_REQUEST['id']);
	else if(($mail->flags & FLAG_UNREAD) == 0 && isset($_REQUEST['unread']))
		$mailbox->FlagMail(FLAG_UNREAD, true, (int) $_REQUEST['id']);

	// confirmation?
	$confirmationTo = 0;
	if(($mail->flags & FLAG_DNSENT) == 0
		&& $mail->_row['folder'] != FOLDER_OUTBOX)
	{
		$dispositionNotificationTo = $mail->GetHeaderValue('disposition-notification-to');
		if($dispositionNotificationTo)
			$confirmationTo = ExtractMailAddress($dispositionNotificationTo);
		if($confirmationTo == '')
			$confirmationTo = 0;
	}

	// get attachments
	$attachments = $mail->GetAttachments();

	// get text part
	$textParts = $mail->GetTextParts();
	if(isset($textParts['html'])) {
		$textMode = 'html';
		$text = $this->formatAppEMailHTMLText($textParts['html'],
			true,
			$attachments,
			(int) $_REQUEST['id']);
		// $pureText = htmlToText(preg_replace("|<style\b[^>]*>(.*?)</style>|s", "", $textParts['html']));
		// Remove style tags and their content first. Otherwise, we would see the content of these tags (css) later.
		$str = preg_replace('/<\s*?style.*?>.*?<\s*?\/\s*?style\s*?>/siu', '', $textParts['html']);
		// The following lines are similar to htmlToText() from common.inc.php. The only change here is that we use the "u" modifier.
		$str = str_replace(array("\r", "\n"), '', $str);
		$str = preg_replace('/\<p([^\>]*)\>/iu', "\r\n", $str);
		$str = preg_replace('/\<br([^\>]*)\>/iu', "\r\n", $str);
		$str = strip_tags(str_replace('>', '> ', $str));
		$str = DecodeHTMLEntities($str);
		$pureText = $str;
	} elseif(isset($textParts['text'])) {
		$textMode = 'text';
		$text = $this->formatAppEMailText($textParts['text'], true);
		// Since htmlToText removes line breaks but we want to keep them, we use nl2br before.
		$pureText = htmlToText(nl2br($textParts['text']));
	} else {
		$textMode = 'text';
		$text = '';
		$pureText = '';
	}
	$pureText = nl2br($pureText);

	// reply to
	if(($replyTo = $mail->GetHeaderValue('reply-to')) && $replyTo != '')
		$replyTo = $replyTo;
	else
		$replyTo = $mail->GetHeaderValue('from');

	// sender
	$recpList = array_merge(ExtractMailAddresses($mail->GetHeaderValue('to')),
								ExtractMailAddresses($mail->GetHeaderValue('cc')));
	$defaultSenderWithName = strchr($thisUser->GetDefaultSender(), '"') !== false;
	foreach($recpList as $val) {
		foreach($possibleSenders as $possibleSenderID=>$possibleSender) {
			if(strtolower(ExtractMailAddress($possibleSender)) == strtolower($val)) {
				if((strchr($possibleSender, '"') !== false && $defaultSenderWithName)
					|| (strchr($possibleSender, '"') === false && !$defaultSenderWithName))	{
					$replyFrom = $possibleSenderID;
					break;
				}
			}
		}
	}
	if(!isset($replyFrom))
		$replyFrom = $userRow['defaultSender'];

	// Our response
	$json_response['content'] = array(
		'attachments' => $attachments,
		'subject' => $mail->GetHeaderValue('subject'),
		'mailID' => $_REQUEST['id'],
		'from' => $mail->GetHeaderValue('from'),
		'to' => $mail->GetHeaderValue('to'),
		'cc' => $mail->GetHeaderValue('cc'),
		'date' => $mail->date,
		'text' => $text,
		'pureText' => $pureText,
		'textMode' => $textMode,
		'replyTo' => ExtractMailAddress($replyTo),
		'replySubject' => $userRow['re'] . ' ' . $mail->GetHeaderValue('subject'),
		'replyFrom' => $replyFrom,
		'forwardSubject' => $userRow['fwd'] . ' ' . $mail->GetHeaderValue('subject'),
		'confirmationTo' => $confirmationTo,
		// Older versions of the API didn't support "withfolderlist" and "withunreadcount" in the same way as today (the reported numbers were based on the values known before reading the email). So, we need to add it here.
		'folderlist' => $mailbox->GetFolderList(true),
		'unreadcount' => $mailbox->GetMailCount(-1, true)
	);
}
/* ----- imageProxy ----- */
else if($_REQUEST['do'] == 'imageProxy' && isset($_REQUEST['url'])) {

	if(!$this->get_app_pref('imageproxy')) {
		$json_response['status'] = 'error';
		$json_response['content'] = 'imageProxy_disabled';
		return;
	}

	$url = $_REQUEST['url'];

	$mime_type = '';

	// Load an inline image?
	if(substr($url, 0, 17) == 'inlineAttachment/') {
		$data = '';
		$urlData = explode('/', $url);
		$mailID = $attachment = false;
		foreach($urlData as $key=>$value) {
			if(substr($value, 0, 3) == 'id=')
				$mailID = (int) substr($value, 3);
			elseif(substr($value, 0, 11) == 'attachment=')
				$attachment = substr($value, 11);
		}
		if(!$mailID || ! $attachment) {
			$json_response['status'] = 'error';
			return;
		}

		$mail = $mailbox->GetMail($mailID);

		if($mail !== false)	{
			$parts = $mail->GetPartList();
			if(isset($parts[$attachment]))
			{
				$part = $parts[$attachment];

				$mime_type = $part['content-type'];

				$attData = &$part['body'];
				$attData->Init();
				while($block = $attData->DecodeBlock(PART_CHUNK_SIZE)) {
					$data .= $block;
				}
				$attData->Finish();
			}
		} else {
			$json_response['status'] = 'error';
			return;
		}
	// Load a remote image?
	} elseif(strtolower(substr($url, 0, 7)) == 'http://' || strtolower(substr($url, 0, 8) == 'https://')) {
		// Make sure that whitespaces are correctly encoded.
		$url = str_replace(' ', '%20', $url);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);

		// We want to use a "real" user agent
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
		$data = curl_exec($ch);
		// Get the returned mime type
		$mime_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
		curl_close ($ch);
	// Otherwise: Something must have gone wrong.
	} else {
		$json_response['status'] = 'error';
		return;
	}

	// Is the mime type allowed?
	$image_mime_types = array('image/jpeg', 'image/png', 'image/gif');
	if(!in_array($mime_type, $image_mime_types)) {
		$json_response['status'] = 'error';
		return;
	}

	// Is $data a real image?
	if(@imagecreatefromstring($data) === false) {
		$json_response['status'] = 'error';
		return;
	}
	$data = base64_encode($data);

	$image = 'data:' . $mime_type . ';base64,' . $data;

	$json_response['content'] = $image;
}
/* ----- sendMail ----- */
else if($_REQUEST['do'] == 'sendMail') {
	$json_response['status'] = 'error';

	// sanitize + expand addresses
	$_REQUEST['to'] = PrepareSendAddresses($_REQUEST['to']);
	$_REQUEST['cc'] = PrepareSendAddresses($_REQUEST['cc']);
	$_REQUEST['bcc'] = PrepareSendAddresses($_REQUEST['bcc']);

	// get recipients
	$recipients = ExtractMailAddresses($_REQUEST['to'] . ' ' . $_REQUEST['cc'] . ' ' . $_REQUEST['bcc']);

	// check if recipients are blocked
	$blockedRecipients = array();
	foreach($recipients as $recp)
		if(RecipientBlocked($recp))
			$blockedRecipients[] = $recp;

	// no recipients?
	if(count($recipients) > 0) {
		// too many recipients?
		if(count($recipients) > $groupRow['max_recps'])	{
			// The phrase we are using here is similar to $lang_admin['ap_comment_1'].
			AddAbusePoint($userRow['id'], BMAP_SEND_RECP_LIMIT,
				sprintf('mb_app compose form, %d recipients', count($recipients)));

			$json_response['error'] = 'toomanyrecipients';
			return;
		}

		// blocked recipients?
		if(count($blockedRecipients) > 0) {
			// The phrase we are using here is similar to $lang_admin['ap_comment_3'].
			AddAbusePoint($userRow['id'], BMAP_SEND_RECP_BLOCKED,
				sprintf('mb_app compose form, to %s', implode(', ', $blockedRecipients)));

			$json_response['error'] = 'blockedrecipients';
			return;
		}

		// over send limit?
		if(!$thisUser->MaySendMail(count($recipients))) {
			// The phrase we are using here is similar to $lang_admin['ap_comment_1'].
			AddAbusePoint($userRow['id'], BMAP_SEND_FREQ_LIMIT,
				sprintf('mb_app compose form, %d recipients', count($recipients)));

			$json_response['error'] = 'wait';
			return;
		} else {
			//
			// headers
			//
			$to = $_REQUEST['to'];
			$cc = $_REQUEST['cc'];
			$bcc = $_REQUEST['bcc'];

			// sender?
			$senderAddresses = $thisUser->GetPossibleSenders();
			if(isset($senderAddresses[$_REQUEST['from']]))
				$from = $senderAddresses[$_REQUEST['from']];
			else
				$from = $senderAddresses[0];

			// check if sender address is ext. an alias
			$senderAddressIsAlias = false;
			$aliases = $thisUser->GetAliases();
			foreach($aliases as $alias)	{
				if($alias['type'] == ALIAS_SENDER) {
					if(strtolower(ExtractMailAddress($alias['email']))
						== strtolower(ExtractMailAddress($from))) {
						$senderAddressIsAlias = true;
						break;
					}
				}
			}

			// prepare header fields
			$subject = trim(str_replace(array("\r", "\t", "\n"), '', $_REQUEST['subject']));
			$replyTo = $from;

			// build the mail
			if(!class_exists('BMMailBuilder'))
				include(B1GMAIL_DIR.'serverlib/mailbuilder.class.php');
			$mail = _new('BMMailBuilder');
			$mail->SetUserID($userRow['id']);

			// mandatory headers
			if($bm_prefs['write_xsenderip'] == 'yes')
				$mail->AddHeaderField('X-Sender-IP',$_SERVER['REMOTE_ADDR']);
			$mail->AddHeaderField('From', $from);
			$mail->AddHeaderField('Subject', $subject);
			$mail->AddHeaderField('Reply-To', $replyTo);

			// ext. alias sender?
			if($senderAddressIsAlias)
				$mail->SetMailFrom($userRow['email']);

			// optional headers
			if($to != '')
				$mail->AddHeaderField('To',	 	$to);
			if($cc != '')
				$mail->AddHeaderField('Cc', 	$cc);
			if($bcc != '')
				$mail->AddHeaderField('Bcc', 	$bcc);

			// mail confirmation?
			if(isset($_REQUEST['mailConfirmation']))
				$mail->AddHeaderField('Disposition-Notification-To', $replyTo);

			// ref headers
			if(isset($_REQUEST['reference']) && strpos($_REQUEST['reference'], ':') !== false) {
				list($type, $id) = explode(':', $_REQUEST['reference']);

				// Note: We will need the referenced mail later for our "included attachments".
				$referencedMail = $mailbox->GetMail($id);
				if($referencedMail !== false) {
					if($type == 'reply')
						$mail->AddHeaderField('In-Reply-To', $referencedMail->GetHeaderValue('message-id'));

					if($type == 'reply' || $type == 'forward') {
						$msgReferences = ExtractMessageIDs($referencedMail->GetHeaderValue('references'));
						$msgReferences[] = $referencedMail->GetHeaderValue('message-id');

						if(count($msgReferences) > 10)
							$msgReferences = array_slice($msgReferences, count($msgReferences)-10);

						$mail->AddHeaderField('References', implode(' ', $msgReferences));
					}
				}
			}

			//
			// add text
			//
			$mailText = $_REQUEST['text'];

			if(isset($_REQUEST['add-original']) && isset($_REQUEST['text-original']))
				$mailText .= "\n\n" . $_REQUEST['text-original'];

			$mailText .= GetSigStr('text');

			ModuleFunction('OnSendMail', array(&$mailText, false));
			$mail->AddText($mailText,
				'plain',
				$currentCharset);

			// Add attachments. Important: We will need $attSize later.
			$attSize = 0;
			if(isset($_FILES['attachment'], $_FILES['attachment']['name'])) {
				foreach($_FILES['attachment']['name'] as $key=>$value) {
					if(!isset($_FILES['attachment']['name'][$key]) || trim($_FILES['attachment']['name'][$key]) == '')
						continue;

					if($_FILES['attachment']['error'][$key] !== UPLOAD_ERR_OK) {
						$json_response['status'] = 'error';
						return;
					}

					$fileName = $_FILES['attachment']['name'][$key];
					$fileSize = filesize($_FILES['attachment']['tmp_name'][$key]);
					$mimeType = $_FILES['attachment']['type'][$key];

					if(($attSize + $fileSize) <= $groupRow['anlagen']) {
						if($tempFileFP = fopen($_FILES['attachment']['tmp_name'][$key], 'rb')) {
							$mail->AddAttachment($tempFileFP,
									$mimeType,
									$fileName);
							$attSize += $fileSize;
						}
					} else {
						$json_response['status'] = 'error';
						$json_response['error'] = 'attachments_filesize_toomuch';
						return;
					}
				}
			}

			// A list of temporary files we should clean up later.
			$attTempFiles = [];

			// Add "included" attachments. These are attachments that e.g. come from a redirection of the email. So, they haven't been uploaded by the user.
			if(isset($_REQUEST['included-attachments']) && is_array($_REQUEST['included-attachments'])) {
				// If there are "inluced" attachments but not reference to an email, something must have gone wrong.
				if(!isset($referencedMail) || $referencedMail === false) {
					$json_response['status'] = 'error';
					return;
				}

				$attachments = $referencedMail->GetAttachments();
				$parts = $referencedMail->GetPartList();

				// Remember if there was an error.
				$processing_ok = true;

				foreach($_REQUEST['included-attachments'] as $attID) {
					if(!isset($attachments[$attID], $parts[$attID])) {
						$json_response['status'] = 'error';

						$processing_ok = false;
						break;
					}

					$attInfo = $attachments[$attID];
					// get temp file
					$tempFileID = RequestTempFile($userRow['id'], time()+8*TIME_ONE_HOUR);
					$tempFileName = TempFileName($tempFileID);
					// The following line is different from the b1gmail implementation: Here, we also want to be able to read the file later.
					$tempFP = fopen($tempFileName, 'w+b');
					assert('is_resource($tempFP)');

					// add temp id to delete list
					$attTempFiles[] = array($tempFileID, $tempFP);

					// copy attachment to temp file
					$part = $parts[$attID];
					$attData = &$part['body'];
					$attData->Init();
					while($block = $attData->DecodeBlock(PART_CHUNK_SIZE)) {
						fwrite($tempFP, $block);
					}
					$attData->Finish();

					$fileSize = filesize($tempFileName);
					if(($attSize + $fileSize) <= $groupRow['anlagen']) {
						$mimeType = $attInfo['mimetype'];
						$fileName = $attInfo['filename'];
						$mail->AddAttachment($tempFP,
								$mimeType,
								$fileName);
						$attSize += $fileSize;
					} else {
						$json_response['status'] = 'error';
						$json_response['error'] = 'attachments_filesize_toomuch';

						$processing_ok = false;
						break;
					}
				}

				if(!$processing_ok) {
					// Clean up.
					foreach($attTempFiles as $attData) {
						list($tempFileID, $tempFileFP) = $attData;
						fclose($tempFileFP);
						ReleaseTempFile($userRow['id'], $tempFileID);
					}
					return;
				}
			}

			//
			// send!
			//
			$outboxFP = $mail->Send();

			//
			// ok?
			//
			if($outboxFP && is_resource($outboxFP)) {
				//
				// update stats
				//
				Add2Stat('send');
				$domains = GetDomainList();
				$local = false;
				foreach($domains as $domain)
					if(strpos(strtolower($to . $cc . $bcc), '@'.strtolower($domain)) !== false)
						$local = true;
				Add2Stat('send_'.($local ? 'intern' : 'extern'));
				$thisUser->AddSendStat(count($recipients));
				$thisUser->UpdateLastSend(count($recipients));

				//
				// add log entry
				//
				PutLog(sprintf('<%s> (%d, IP: %s) sends mail from <%s> to <%s> using app compose form',
					$userRow['email'],
					$userRow['id'],
					$_SERVER['REMOTE_ADDR'],
					ExtractMailAddress($from),
					implode('>, <', $recipients)),
					PRIO_NOTE,
					__FILE__,
					__LINE__);

				//
				// reference
				//
				if(isset($_REQUEST['reference']) && strpos($_REQUEST['reference'], ':') !== false) {
					list($type, $id) = explode(':', $_REQUEST['reference']);
					if($type == 'reply')
						$mailbox->FlagMail(FLAG_ANSWERED, true, (int)$id);
					else if($type == 'forward')
						$mailbox->FlagMail(FLAG_FORWARDED, true, (int)$id);
				}

				ModuleFunction('AfterSendMail', array($userRow['id'], ExtractMailAddress($from), $recipients, $outboxFP));

				//
				// save copy
				//
				$saveTo = FOLDER_OUTBOX;
				$mailObj = _new('BMMail', array(0, false, $outboxFP, false));
				$mailObj->Parse();
				$mailObj->ParseInfo();
				$mailbox->StoreMail($mailObj, $saveTo);

				//
				// clean up
				//
				$mail->CleanUp();

				foreach($attTempFiles as $attData) {
					list($tempFileID, $tempFileFP) = $attData;
					fclose($tempFileFP);
					ReleaseTempFile($userRow['id'], $tempFileID);
				}
				$json_response['status'] = 'success';
			} else {
				$json_response['error'] = 'sendfailed';
				return;
			}
		}
	} else {
		$json_response['error'] = 'norecipients';
		return;
	}
}
/* ----- sendConfirmation ----- */
else if($_REQUEST['do'] == 'sendConfirmation' && isset($_REQUEST['id'])) {
	$mail = $mailbox->GetMail((int)$_REQUEST['id']);

	if($mail !== false) {
		if($mail->SendDispositionNotification()) {
			$mailbox->FlagMail(FLAG_DNSENT, true, (int)$_REQUEST['id']);
			return;
		}
	}

	$json_response['status'] = 'error';
	$json_response['error'] = 'mail_confirmation';
}
/* ----- getSignatures ----- */
else if($_REQUEST['do'] == 'getSignatures') {
	$json_response['content']['signatures'] = $thisUser->GetSignatures();
}


// Add additional data to the response if requested.
if(isset($_REQUEST['withfolderlist'])) {
	$folderList = $mailbox->GetFolderList(true);
	$json_response['content']['folderlist'] = $folderList;
}
if(isset($_REQUEST['withunreadcount'])) {
	$unreadCount = $mailbox->GetMailCount(-1, true);
	$json_response['content']['unreadcount'] = $unreadCount;
}
if(isset($_REQUEST['withfolderdata']) && isset($_REQUEST['folderid'])) {
	// The folder doesn't exist?
	if(!isset($folderList))
		$folderList = $mailbox->GetFolderList(true);
	if(!isset($folderList[$_REQUEST['folderid']]))
		exit();

	$folderID = $_REQUEST['folderid'];

	$highestMailID = key($mailbox->GetMailList($folderID, 1, 1, 'id'));
	$json_response['content']['folderdata']['highestmailid'] = $highestMailID;
}
?>
