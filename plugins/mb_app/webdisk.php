<?php
// If this file has been included from somewhere else, don't allow it to be proceeded!
if(!isset($json_response))
	exit();

// No do-Request? Something must have gone wrong
if(!isset($_REQUEST['do']))
	exit();

if(!class_exists('BMWebdisk'))
	include('./serverlib/webdisk.class.php');
$webdisk = _new('BMWebdisk', array($userRow['id']));
$folderID = !isset($_REQUEST['folderid']) ? 0 : (int) $_REQUEST['folderid'];
$spaceLimit = $webdisk->GetSpaceLimit();
$usedSpace = $webdisk->GetUsedSpace();

/* ----- getfolder ----- */
if($_REQUEST['do'] == "getfolder" && isset($_REQUEST['folderid'])) {
	$json_response["content"] = $webdisk->GetFolderContent($folderID);
}
/* ----- renameItem ----- */
else if($_REQUEST['do'] == 'renameItem'
		&& isset($_REQUEST['type'])
		&& isset($_REQUEST['id'])
		&& isset($_REQUEST['name'])) {

	$newName = trim($_REQUEST['name']);

	if($_REQUEST['type'] == WEBDISK_ITEM_FILE) {
		$fileInfo = $webdisk->GetFileInfo((int) $_REQUEST['id']);

		if($fileInfo !== false) {
			if(strlen($newName) < 1) {

				$json_response["status"] = "error";
				$json_response["error"] = "filename_invalid";

				return;
			} else if($newName == $fileInfo['dateiname']
				|| $webdisk->FileExists($folderID, $newName)) {

				$json_response["status"] = "error";
				$json_response["error"] = "filename_existing";

				return;
			}

			$webdisk->RenameFile((int) $_REQUEST['id'], $newName);
			$json_response['content'] = WEBDISK_ITEM_FILE;
		}
	} else if($_REQUEST['type'] == WEBDISK_ITEM_FOLDER)	{
		$folderInfo = $webdisk->GetFolderInfo((int) $_REQUEST['id']);
		if($folderInfo !== false) {
			if(strlen($newName) < 1) {

				$json_response["status"] = "error";
				$json_response["error"] = "foldername_invalid";

				return;

			}
			elseif($newName == $folderInfo['titel']
				|| $webdisk->FolderExists($folderID, $newName)) {
				$json_response["status"] = "error";
				$json_response["error"] = "foldername_existing";

				return;
			}

			$webdisk->RenameFolder((int) $_REQUEST['id'], $newName);
			$json_response['content'] = WEBDISK_ITEM_FOLDER;
		}
	}
}
/* ----- deleteItem ----- */
else if($_REQUEST['do'] == 'deleteItem'
		&& isset($_REQUEST['type'])
		&& isset($_REQUEST['id'])) {
	if($_REQUEST['type'] == WEBDISK_ITEM_FILE)
	{
		$webdisk->DeleteFile((int) $_REQUEST['id']);
	}
	else
	{
		$webdisk->DeleteFolder((int) $_REQUEST['id']);
	}
}
/* ----- createFolder ----- */
else if($_REQUEST['do'] == 'createFolder' && isset($_REQUEST['title'])) {
	$folderName = trim($_REQUEST['title']);

	if($webdisk->FolderExists($folderID, $folderName)) {
		$json_response["status"] = "error";
		$json_response["error"] = "foldername_existing";
	} elseif(strlen($folderName) < 1) {
		$json_response["status"] = "error";
		$json_response["error"] = "foldername_invalid";
	} else {
		$newFolderID = $webdisk->CreateFolder($folderID, $folderName);
	}
}
/* ----- uploadFiles ----- */
else if($_REQUEST['do'] == 'uploadFiles'
		&& IsPOSTRequest()) {
	$error = $success = false;

	foreach($_FILES as $key=>$value) {
		if(is_array($value) && substr($key, 0, 4) == 'file' && isset($value['name']) && trim($value['name']) != '') {
			$fileName = isset($value['name']) ? $value['name'] : 'unknown';
			$fileSize = (int) $value['size'];
			$mimeType = $value['type'];

			if($mimeType == '' || $mimeType == 'application/octet-stream')
				$mimeType = GuessMIMEType($fileName);

			if($groupRow['traffic'] <= 0 || ($userRow['traffic_down'] + $userRow['traffic_up'] + $fileSize) <= $groupRow['traffic'] + $userRow['traffic_add']) {
				if($spaceLimit == -1 || $usedSpace+$fileSize <= $spaceLimit) {
					if(($fileID = $webdisk->CreateFile($folderID, $fileName, $mimeType, $fileSize)) !== false) {
						$tempFileID = RequestTempFile($userRow['id'], time() + TIME_ONE_HOUR);
						$tempFileName = TempFileName($tempFileID);

						if(!@move_uploaded_file($value['tmp_name'], $tempFileName)) {
							$webdisk->DeleteFile($fileID);
							$error = 'internalerror';

							// log
							PutLog(sprintf('Failed to move uploaded file <%s> to <%s>, deleting webdisk file',
								$value['tmp_name'],
								$tempFileName),
								PRIO_ERROR,
								__FILE__,
								__LINE__);
						} else {
							$sourceFP = fopen($tempFileName, 'rb');
							if($sourceFP
								&& BMBlobStorage::createDefaultWebdiskProvider($userRow['id'])->storeBlob(BMBLOB_TYPE_WEBDISK, $fileID, $sourceFP)) {
								fclose($sourceFP);

								$usedSpace += $fileSize;
								$db->Query('UPDATE {pre}users SET traffic_up=traffic_up+? WHERE id=?',
									$fileSize,
									$userRow['id']);
								Add2Stat('wd_up', ceil($fileSize/1024));
								$success = true;
							} else {
								$webdisk->DeleteFile($fileID);
								$error = 'internalerror';
							}
						}

						ReleaseTempFile($userRow['id'], $tempFileID);
					} else {
						$error = 'filename_existing';
					}
				} else {
					$error = 'nospace';
				}
			} else {
				$error = 'notraffic';
			}
		}
	}

	if($error || !$success) {
		$json_response['status'] = 'error';
		$json_response['error'] = $error;
	}
}
/* ===== previewImage ===== */
else if($_REQUEST['do'] == 'previewImage' && isset($_REQUEST['id'])) {
	$fileInfo = $webdisk->GetFileInfo((int) $_REQUEST['id']);
	if ($fileInfo === false) {
		$jsonjson_response['status'] = 'error';
		return;
	}

	$imageTypes = array('image/jpeg', 'image/gif', 'image/png', 'image/jpg', 'image/pjpeg');

	$imageWidth = 960;
	$imageHeight = 360;

	if(isset($_REQUEST['width']) && (int) $_REQUEST['width'] < $imageWidth)
		$imageWidth = (int) $_REQUEST['width'];
	if(isset($_REQUEST['height']) && (int) $_REQUEST['height'] < $imageHeight)
		$imageHeight = (int) $_REQUEST['height'];

	/* Does the user have enough traffic left to see the original file and is this file actually an image?
	   (the output size is not the same as $fileInfo['size'], but it is a good way to save much resources
			- otherwise we would always have to generate the smaller version and then check if the traffic is enough to display it)
	*/
	if(($groupRow['traffic'] <= 0 || ($userRow['traffic_down'] + $userRow['traffic_up'] + $fileInfo['size']) <= ($groupRow['traffic'] + $userRow['traffic_add'])) && in_array($fileInfo['contenttype'], $imageTypes)) {
		include(B1GMAIL_DIR . 'plugins/mb_app/3rdparty/SimpleImage/src/claviska/SimpleImage.php');
		try {
			$blob_fp = BMBlobStorage::CreateProvider($fileInfo['blobstorage'], $userRow['id'])->loadBlob(BMBLOB_TYPE_WEBDISK, $fileInfo['id']);

			$img_str = '';
			while(!feof($blob_fp)) {
				$img_str .= fread($blob_fp, 4096);
			}

			$img = new \claviska\SimpleImage();
			$img->fromString($img_str);
			$img->autoOrient();
			$img->thumbnail($imageWidth, $imageHeight);
			$result = $img->toDataUri();
			// Calculate the string size (this will be our file output
			$fileSize = strlen(base64_decode($result));

			// This is the actual test if our user has enough traffic left to display the smaller image - it is needed as in some cases the resized version might be larger than the original one.
			if(($userRow['traffic_down'] + $userRow['traffic_up'] + $fileSize) <= ($groupRow['traffic'] + $userRow['traffic_add'])) {
				$json_response['content'] = $result;

				$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
					$fileSize,
					$userRow['id']);
			} else {
				$json_response['status'] = 'error';
			}

		} catch(Exception $e) {
			$json_response['status'] = 'error';
		}
	} else {
		// Not enough traffic.
		$json_json_response['status'] = 'error';
		$json_respose['error'] = 'notraffic';
	}
}

?>
