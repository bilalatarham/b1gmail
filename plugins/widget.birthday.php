<?php 
/*
 * b1gMail7
 * (c) 2002-2008 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: widget.birthday.php,v 1.0 2008/02/23 13:00:00 Informant $
 *
 */

/**
 * Birthday widget
 *
 */
class BMPlugin_Widget_Birthday extends BMPlugin 
{


	function BMPlugin_Widget_Birthday()
	{
		global $lang_user;
		
		$this->type			= BMPLUGIN_WIDGET;
		$this->name			= 'Birthday widget';
		$this->author		= 'Informant';
		$this->widgetTemplate	= 'widget.birthday.tpl';
		$this->version		= '1.0';
		$this->widgetTitle	= 'Geburtstage';
		$this->widgetIcon   	= 'birthday_icon_12.png';
		
		// admin pages
		$this->admin_pages	= true;
		$this->admin_page_title	= 'Geburtstage';
	}
	
	function Install()
	{
		global $db, $bm_prefs;

		// zeile mit vorlaufzeit erstellen
		$db->Query('ALTER TABLE {pre}prefs ADD vorlaufzeit VARCHAR(255) NOT NULL DEFAULT 30');
		PutLog('Zeile vorlaufzeit in bm60_prefs Tabelle erstellt !',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		global $db;
		
		// zeile l�schen
		$db->Query('ALTER TABLE {pre}prefs DROP vorlaufzeit');
		PutLog('Zeile vorlaufzeit in bm60_prefs Tabelle gel�scht !',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function isWidgetSuitable($for)
	{
		return($for == BMWIDGET_START	|| $for == BMWIDGET_ORGANIZER);
	}
	
	function renderWidget()
	{
		global $db, $tpl, $thisUser;

		// Hier einstellen, wieviele Tage im vorraus angezeigt werden sollen.

		$res = $db->Query('SELECT vorlaufzeit FROM {pre}prefs');
		$row = $res->FetchArray();
		$vorlaufzeit = $row['vorlaufzeit'];
		$res->Free();

		$birthday_data = array();

		$res = $db->Query('SELECT 
				`id`,
				`vorname`,
				`nachname`,
				`email`,
				`geburtsdatum`
			FROM
				{pre}adressen
			WHERE
				user=?
			AND
				geburtsdatum != 0',
			$thisUser->_id);

		$age = array();
		$i = 0;
		$birthday = array();

		while($row = $res->FetchArray(MYSQL_ASSOC))
		{
			$gebdatum = $row["geburtsdatum"];

			$subject = "Alles Gute zum Geburtstag";

			if($row['email'] != '')
			{
				$link = "email.compose.php?to=$row[email]&subject=$subject&sid=" . session_id();
			}
			else
			{
				$link = "email.compose.php?subject=$subject&sid=" . session_id();
			}
     
			//Geburtstagsdaten bereitstellen
			$jahr = date('Y',$gebdatum);
			$mon  = date('n',$gebdatum);
			$tag  = date('j',$gebdatum);
     
			//Geburtstag f�r dieses Jahr berechnen
			$gebtag = mktime(0,0,0,$mon,$tag,date("Y"));
     
			//Zeit bis zum Geburtstag berechnen
			$timetobirthday = $gebtag - time();

			//Berechnung der Tage bis zum Geburtstag -> ceil rundet auf - auch 0,1
			$ttb = ceil($timetobirthday / 3600 / 24);

			//Vorlaufzeit in Sekunden umrechnen
			$vorlaufzeitinsekunden = $vorlaufzeit * 3600 * 24;

			//�berpr�fen ob in Vorlaufzeit
			if(($timetobirthday <= $vorlaufzeitinsekunden) && ($ttb >= 0))
			{
				//Geburtstag liegt in der Vorlaufzeit
				$birthday[$i]["userid"] = $row["id"];
				$birthday[$i]["nachname"] = $row["nachname"];
				$birthday[$i]["vorname"] = $row["vorname"];
				$birthday[$i]["link"] = $link;
				$birthday[$i]["tage"]= $ttb;
				//Alter berechnen
				$birthday[$i]["alter"] = (date('Y') - $jahr);

				$i++;
			}
		}

		//sortiert die Tage bis zum Geburtstag vom k�rzesten an
		for ($i=0;$i<count($birthday)-1;$i++)
		{
			for($j=$i+1;$j<count($birthday);$j++)
			{
				if($birthday[$i]["tage"] > $birthday[$j]["tage"])
				{
					$temp = $birthday[$i];
					$birthday[$i] = $birthday[$j];
					$birthday[$j] = $temp;
				}
			}
		}

		$tpl->assign('birthday', $birthday);
		$tpl->assign('vorlaufzeit', $vorlaufzeit);
		return(true);
	}
	
	/**
	 * admin handler
	 */
	function AdminHandler()
	{
		global $tpl, $plugins, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'prefs';
		
		$tabs = array(
			0 => array(
				'title'	=> $lang_admin['prefs'],
				'link'	=> $this->_adminLink() . '&',
				'active'	=> $_REQUEST['action'] == 'prefs'
			)
		);

		$tpl->assign('tabs', $tabs);
		
		if($_REQUEST['action'] == 'prefs')
			$this->_prefsPage();
	}
	
	function _prefsPage()
	{
		global $tpl, $db, $bm_prefs;
		
		// speichern
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}prefs SET vorlaufzeit=?',
				$_REQUEST['vorlaufzeit']
				);
		}
		
		// vorlaufzeit abrufen
		$res = $db->Query('SELECT vorlaufzeit FROM {pre}prefs');
		$vlz = $res->FetchArray();
		$res->Free();
			
		// an template �bergeben
		$tpl->assign('vlz', $vlz);
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('birthday.plugin.prefs.tpl'));
		return(true);
	}

}

/**
 * register widgets
 */
$plugins->registerPlugin('BMPlugin_Widget_Birthday');

?>