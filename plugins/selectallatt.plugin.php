<?php
	class selectallatt extends BMPlugin
	{
		function selectallatt()
		{
			
			$this->name        = 'Alle Anhänge auswählen';
			$this->author      = 'Martin Buchalik';
			$this->web         = 'http://martin-buchalik.de';
			$this->mail        = 'support@martin-buchalik.de';
			$this->version     = '1.0.0';
			$this->designedfor = '7.3.0';
			$this->type        = BMPLUGIN_DEFAULT;
			
			$this->admin_pages = false;
		}
		
		
		/* ===== Installation ===== */
		
		function Install()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}
		
		
		/* ===== Uninstall ===== */
		
		function Uninstall()
		{
			PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
			return (true);
		}		
		
		
		/* ===== Add the js file when reading a mail ===== */
		
		function BeforeDisplayTemplate($resourceName, &$tpl)
		{
			global $thisUser;
			
			if($thisUser->_id && isset($_REQUEST['action']) && $_REQUEST['action'] == 'read' && !IsMobileUserAgent()) {
				$tpl->addJSFile("li", "./plugins/js/selectallatt.js");
			}
		}
	}
	
	/* ===== register plugin ===== */
	$plugins->registerPlugin('selectallatt');
?>