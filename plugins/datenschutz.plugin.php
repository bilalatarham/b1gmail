<?php
/**
 * B1GMail
 *
 * Datenschutz (DSGVO)
 *
 * PHP version 5.6
 *
 * @link        https://www.onesystems.ch
 * @copyright   OneSystems GmbH 2009-2020
 * @author      Michael Kleger <michael.kleger@onesystems.ch>
 * @version     1.1.9
 */

// Datenschutz Link automatisch anzeigen
define("DATENSCHUTZ_ACTIVATE", 1);

// MySQLi / MySQL Support
if(!defined('MYSQLI_NUM'))
    define('MYSQLI_NUM',			MYSQL_NUM);
if(!defined('MYSQLI_ASSOC'))
    define('MYSQLI_ASSOC',			MYSQL_ASSOC);
if(!defined('MYSQLI_BOTH'))
    define('MYSQLI_BOTH',			MYSQL_BOTH);

class Datenschutz extends BMPlugin {
    function __construct() {
        // Allgemein
        $this->type				    = BMPLUGIN_DEFAULT;
        $this->name                 = 'Datenschutz';
        $this->author               = 'OneSystems';
        $this->web                  = 'https://www.onesystems.ch';
        $this->mail                 = 'info@onesystems.ch';
        $this->version			    = '1.1.10';
        $this->designedfor          = '7.4.0';

        // Updates
        $this->website              = 'https://mail.town/produkte/b1gmail/datenschutz/';
        $this->update_url           = 'https://update.mail.town/';
        $this->update_name          = 'Datenschutz';

        // Admin
        $this->admin_pages			= true;
        $this->admin_page_title		= 'Datenschutz';
        $this->admin_page_icon		= 'datenschutz.png';

        // Gruppen Option
        $this->RegisterGroupOption('datenschutz-av',
            FIELD_CHECKBOX,
            'Gruppe mit Datenschutz AV-Vertrag');

        // PHP Class
        $this->phpPluginClassesPath     = 'plugins/php/tcpdf/';
        $this->phpPluginClassesHashes   = array(
            '7c6faa8a729988da6be989e71286a7b4',     // tcpdf.php
            'bcb93bbeb8cf2831e49ff5541d277a1f',     // tcpdf_autoconfig.php
            'cbeb421f03d05020d1ff83725a711fa4',     // tcpdf_barcodes_1d.php
            '17bfd10e3232de9145f5b74a6ef6afac',     // tcpdf_barcodes_2d.php
            '6bb88a8a3d69511d1bf9e7af12ab5f47',     // tcpdf_import.php
            'ad061c8bc4f5df3138efe55c49e37f33'      // tcpdf_parser.php
        );
    }

    function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang) {
        global $currentCharset;

        if($lang == 'deutsch') {
            // Allgemeine Texte
            $_lang_custom['DatenschutzVerantwortung']           = "Verantwortliche Stelle im Sinne der Datenschutzgesetze, insbesondere der EU-Datenschutzgrundverordnung (DSGVO), ist:<br /><strong>[%name%]</strong><br />[%street%] [%streetnr%]<br />[%plz%] [%place%]<br />[%country%]<br /><br />Adresse und weitere Informationen muss in den \"Anpassbare Texte\" - \"DatenschutzVerantwortung\" erg&auml;nzt werden.<br />Einstellungen -> Sprachen -> Anpassbare Texte";
            $_lang_custom['DatenschutzAbschluss']               = "Gerichtsstand f&uuml;r s&auml;mtliche Streitigkeiten aus Rechtsverh&auml;ltnissen mit <strong>[%name%]</strong> erfolgen am Sitz der Gesellschaft.<br />ORT, MONAT JAHR<br /><br />Adresse und weitere Informationen muss in den \"Anpassbare Texte\" - \"DatenschutzAbschluss\" erg&auml;nzt werden.<br />Einstellungen -> Sprachen -> Anpassbare Texte";
            $_lang_custom['DatenschutzMailBetreff']             = "Datenschutz Report wurde erstellt";
            $_lang_custom['DatenschutzMailText']                = "Sehr geehrte Damen und Herren,\n\nder Datenschutz Report wurde erstellt und steht ihnen unter den \"Einstellungen\" - \"Datenschutz\" als PDF zu Verf&uuml;gung.\n\nMit freundlichen Gr&uuml;ssen\nService-Team\n\n(Diese E-Mail wurde automatisch erstellt!)";
            $_lang_custom['DatenschutzNewReportBetreff']        = "Datenschutz Report wartet auf Akzeptierung";
            $_lang_custom['DatenschutzNewReportText']           = "Sehr geehrte Damen und Herren,\n\nes wurde ein Datenschutz Report f&uuml;r den Benutzer \"%%USERNAME%%\" angefragt, bitte &uuml;ber die Administrationsseite akzeptieren.\n\nMit freundlichen Gr&uuml;ssen\nService-Team\n\n(Diese E-Mail wurde automatisch erstellt!)";
            $_lang_custom['DatenschutzPDFBegruessung']          = "[%anrede%]\n\nwir bedanken uns f&uuml;r Ihre Anfrage vom [%aktdatum%].\n\nIn den folgenden Seiten haben wir jene personenbezogenen Daten f&uuml;r Sie zusammengestellt die Ihrem Konto zugeordnet werden konnten.\n\n\n\nMit freundlichen Gr&uuml;ssen\n\n[%name%]";

            // Benachrichtigung
            $_lang_custom['notify_ds']	                        = '<strong>Datenschutz</strong><br /> %s';
            $_lang_custom['ds_notify_abschluss']                = 'Report wurde generiert';

            // Weiterletung
            $_lang_user['dsDerefTitle']                         = 'Weiterleitung';
            $_lang_user['dsDerefRedirect']                      = 'Der ausgew&auml;hlte Link befindet sich ausserhalt von unserer Seite, damit die Seite ge&ouml;ffnet wird w&auml;hlen Sie bitte den folgenden Link aus.';
            $_lang_user['dsDerefBack']                          = 'Falls Sie diese Seite nicht besuchen m&ouml;chten, <a href="#" onclick="self.close()">schliessen Sie dieses Fenster</a>.';

            // Impressum
            $_lang_user['dsImpressum']                          = 'Impressum';
            $_lang_user['dsConsumerDisputeResolution']          = 'Streitschlichtung und Verbraucherstreitbeilegung';
            $_lang_user['dsSocialMedia']                        = 'Social Media und andere Onlinepr&auml;senzen';
            $_lang_user['dsCopyrightNotices']                   = 'Haftungs- und Rechtshinweise';
            $_lang_user['dsPhotoCredits']                       = 'Bildquellen';

            // AV Speichern
            $_lang_user['dsAVMSGSave']                          = 'Ihr AV Vertrag wurde gespeichert, weitere Informationen finden Sie in den Datenschutz Einstellungen.';
            $_lang_user['dsAVAnzeigen']                         = 'AV Vertrag anzeigen';
            $_lang_user['dsAVAblehnen']                         = 'Das Benutzerkonto wurde deaktiviert da der AV-Vertrag nicht akzeptiert wurde.';
            $_lang_user['dsAVSchliessen']                       = 'Schliessen';

            // Allgemein
            $_lang_user['DatenschutzAcceptCookies']             = "Sie k&ouml;nnen sich erst anmelden wen Sie den Cookie Hinweis akzeptiert haben, bitte also zuerst unsere <a href=\"index.php?action=Datenschutz\">Richtlinien</a> durchlesen und mit \"Cookies erlauben\" best&auml;tigen.";

            // Registration
            $_lang_user['dsRegLink']                            = 'Datenschutzbestimmungen';
            $_lang_user['dsRegAccept']                          = 'Ich akzeptiere die ';
            $_lang_user['dsRegTextAccept']                      = 'Ich akzeptiere die Datenschutzbestimmungen';
            $_lang_user['dsRegNotAccept']                       = 'Ich akzeptiere die Datenschutzbestimmungen nicht';
            $_lang_user['dsRegError']                           = 'Der Datenschutz muss gelesen und akzeptiert werden damit die Registration abgeschlossen werden kann.';

            // Datenschutz Seite
            $_lang_user['dsTitel']                              = 'Datenschutz';
            $_lang_user['dsLogs']                               = 'Logs';
            $_lang_user['dsLogsWebAcc']                         = 'Webserver Access Logs';
            $_lang_user['dsLogsWebErr']                         = 'Webserver Error Logs';
            $_lang_user['dsLogsWebMail']                        = 'Webmail Logs';
            $_lang_user['dsLogsMail']                           = 'E-Mail Server Logs';
            $_lang_user['dsLogArchive']                         = 'Archivierung';
            $_lang_user['dsInhalt']                             = 'Enth&auml;lt';
            $_lang_user['dsZeit']                               = 'Vorhaltezeit';

            // Prefs
            $_lang_user['ds']                                   = 'Datenschutz';
            $_lang_user['prefs_d_ds']                           = 'Eine &Uuml;bersicht &uuml;ber die Daten die bei uns gespeichert sind.';
            $_lang_user['dsReport']                             = 'Datenschutz Report';
            $_lang_user['dsLoeschen']                           = 'Datenschutz Report neu erstellen';
            $_lang_user['dsLoeschenNachricht']                  = 'F&uuml;r eine erneute Erstellung des Datenschutz Reportes fallen unter Umst&auml;nden kosten an, sind Sie sicher das der Datenschutz Report neu erstellt werden soll?';
            $_lang_user['dsPrefsAuswahl']                       = 'Datenauswahl';

            $_lang_user['dsPrefsMSGAdd']                        = 'Aufgabe wird bearbeitet, sobald die Aufgabe abgeschlossen ist und das Dokument generiert wurde werden Sie per E-Mail Informiert.';

            $_lang_user['dsPrefsInformation']                   = 'Gem&auml;ss Art. 15 DSGVO haben Sie das Recht, einmalig und unentgeltlich Auskunft &uuml;ber die von uns gespeicherten, personenbezogenen Daten zu erhalten. Mit einem Klick auf den Button "Dokument erstellen" k&ouml;nnen Sie Ihre Daten anfordern. Sollten Sie dar&uuml;ber hinaus weitere Kopien ben&ouml;tigen oder die Auskunft zu einem sp&auml;teren Zeitpunkt erneut anfordern, sind wir gem&auml;ss Art. 12 DSGVO berechtigt, Ihnen eine Verwaltungsgeb&uuml;hr i.H.v. %KOSTEN% f&uuml;r jede weitere Auskunft in Rechnung zu stellen.';

            $_lang_user['dsPrefsAllgemein']                     = 'Allgemein';
            $_lang_user['dsPrefsAllgemeinBeschr']               = 'Allgemeine Informationen zu diesem Bereich';
            $_lang_user['dsPrefsProfile']                       = 'Benutzerprofil';
            $_lang_user['dsPrefsProfileBeschr']                 = 'Alle Profil Informationen anzeigen';
            $_lang_user['dsPrefsRZ']                            = 'Rechenzentrum';
            $_lang_user['dsPrefsRZBeschr']                      = 'Wo steht das Rechenzentrum?';
            $_lang_user['dsPrefsBackup']                        = 'Backup';
            $_lang_user['dsPrefsBackupBeschr']                  = 'Wo wird das Backup gespeichert?';
            $_lang_user['dsPrefsReg']                           = 'Registration';
            $_lang_user['dsPrefsRegBeschr']                     = 'Wann wurde mein E-Mail-Konto angelegt?';
            $_lang_user['dsPrefsLWeb']                          = 'Letzter Web Login';
            $_lang_user['dsPrefsLWebBeschr']                    = 'Letzte Anmeldung am Webmail';
            $_lang_user['dsPrefsLPOP']                          = 'Letzter POP3 Login';
            $_lang_user['dsPrefsLPOPBeschr']                    = 'Letzter POP3 zugriff';
            $_lang_user['dsPrefsLIMAP']                         = 'Letzter IMAP Login';
            $_lang_user['dsPrefsLIMAPBeschr']                   = 'Letzter IMAP zugriff';
            $_lang_user['dsPrefsLSMTP']                         = 'Letzter SMTP Login';
            $_lang_user['dsPrefsLSMTPBeschr']                   = 'Letzter SMTP zugriff';

            $_lang_user['dsDaten']                              = 'Pers&ouml;nliche Daten';
            $_lang_user['dsPrefsDaten']                         = 'Daten';
            $_lang_user['dsPrefsDatenBeschr']                   = 'Pers&ouml;nliche Daten (E-Mails, Kalender, Kontakte, etc.)';

            $_lang_user['dsPrefsBestellungen']                  = 'Bestellungen';
            $_lang_user['dsBestellAllgemein']                   = 'Allgemein';

            $_lang_user['dsModule']                             = 'Module';
            $_lang_user['dsModuleBeschr']                       = 'Alle Module die Daten Speichern werden ausgegeben';

            $_lang_user['dsPrefsInfo']                          = 'Informationen';
            $_lang_user['dsInfoGes']                            = 'Gesetzestexte';
            $_lang_user['dsInfoGesBeschr']                      = 'Alle Gesetzestexte (Bsp.: Impressum, AGBs und Datenschutz) ausgeben';

            $_lang_user['dsDokumentErstellen']                  = 'Dokument beantragen';
            $_lang_user['dsReportLoeschen']                     = 'Auswertung wirklich unwiderruflich entfernen?';

            $_lang_user['dsStatusTextEnd']                      = 'Die Generierung des Reportes ist abgeschlossen.';
            $_lang_user['dsStatusTextPlan']                     = 'Der Report wurde eingereicht, bitte warten Sie bis ein Administrator den Report akzeptiert.';
            $_lang_user['dsStatusTextAcce']                     = 'Die Reportanfrage wurde akzeptiert, wir werden Sie informieren wen die Generierung abgeschlossen wurde.';
            $_lang_user['dsStatusTextLauf']                     = 'Der Report wird aktuell Generiert, bitte haben Sie noch etwas Geduld.';

            // Prefs: PDF
            $_lang_user['dsPDFTitel']                           = 'Datenschutz Report';
            $_lang_user['dsPDFErstell']                         = 'Erstelldatum';
            $_lang_user['dsPDFAnrede']                          = 'Guten Tag';
            $_lang_user['dsPDFAnredeHerr']                      = 'Sehr geehrter Herr';
            $_lang_user['dsPDFAnredeFrau']                      = 'Sehr geehrter Frau';
            $_lang_user['dsPDFAllgemein']                       = 'Allgemein';
            $_lang_user['dsPDFProfil']                          = 'Benutzerprofil';
            $_lang_user['dsPDFMail']                            = 'E-Mail-Adresse';
            $_lang_user['dsPDFAMail']                           = 'Alternative E-Mail-Adresse';
            $_lang_user['dsPDFName']                            = 'Vor- und Nachname';
            $_lang_user['dsPDFStrasse']                         = 'Strasse und Nummer';
            $_lang_user['dsPDFOrt']                             = 'PLZ / Ort';
            $_lang_user['dsPDFLand']                            = 'Land';
            $_lang_user['dsPDFTel']                             = 'Telefonnummer';
            $_lang_user['dsPDFFax']                             = 'Faxnummer';
            $_lang_user['dsPDFReg']                             = 'Registration';
            $_lang_user['dsPDFRegDatum']                        = 'Datum';
            $_lang_user['dsPDFRegIP']                           = 'IP-Adresse';
            $_lang_user['dsPDFMails']                           = 'E-Mails';
            $_lang_user['dsPDFMailsAnz']                        = 'Anzahl E-Mails im Postfach';
            $_lang_user['dsPDFMailsAnzO']                       = 'Anzahl Ordner im Postfach';
            $_lang_user['dsPDFMailsSize']                       = 'Postfach gr&ouml;sse';
            $_lang_user['dsPDFMailsLMail']                      = 'Letzte E-Mail versendet';
            $_lang_user['dsPDFMailsLIP']                        = 'Letzte IP Adresse';
            $_lang_user['dsPDFMailsLWeb']                       = 'Letzter Web Login';
            $_lang_user['dsPDFMailsLPOP3']                      = 'Letzter POP3 Login';
            $_lang_user['dsPDFMailsLIMAP']                      = 'Letzter IMAP Login';
            $_lang_user['dsPDFMailsLSMTP']                      = 'Letzter SMTP Login';
            $_lang_user['dsPDFAMails']                          = 'Alle E-Mails';
            $_lang_user['dsPDFAMailsVon']                       = 'Von';
            $_lang_user['dsPDFAMailsAn']                        = 'An';
            $_lang_user['dsPDFAMailsDatum']                     = 'Datum';
            $_lang_user['dsPDFAMailsSize']                      = 'Gr&ouml;sse';
            $_lang_user['dsPDFKalender']                        = 'Kalender';
            $_lang_user['dsPDFKalenderAnz']                     = 'Anzahl Termine';
            $_lang_user['dsPDFKalenderGAnz']                    = 'Anzahl Termingruppen';
            $_lang_user['dsPDFKontakte']                        = 'Kontakte';
            $_lang_user['dsPDFKontakteAnz']                     = 'Anzahl Kontakte';
            $_lang_user['dsPDFKontakteGAnz']                    = 'Anzahl Kontaktgruppen';
            $_lang_user['dsPDFNotizen']                         = 'Notizen';
            $_lang_user['dsPDFNotizenAnz']                      = 'Anzahl Notizen';
            $_lang_user['dsPDFAufgaben']                        = 'Aufgaben';
            $_lang_user['dsPDFAufgabenAnz']                     = 'Anzahl Aufgaben';
            $_lang_user['dsPDFDaten']                           = 'Daten';
            $_lang_user['dsPDFDatenAnz']                        = 'Anzahl Dokumente';
            $_lang_user['dsPDFDatenOAnz']                       = 'Anzahl Ordner';
            $_lang_user['dsPDFDatenSize']                       = 'Dokumenten gr&ouml;sse';
            $_lang_user['dsPDFADaten']                          = 'Alle Daten';
            $_lang_user['dsPDFADatenName']                      = 'Dateiname';
            $_lang_user['dsPDFADatenSize']                      = 'Gr&ouml;sse';
            $_lang_user['dsPDFADatenErst']                      = 'Erstellt';
            $_lang_user['dsPDFADatenBear']                      = 'Bearbeitet';
            $_lang_user['dsPDFADatenLZ']                        = 'Letzter Zugriff';
            $_lang_user['dsPDFBest']                            = 'Bestellungen';
            $_lang_user['dsPDFBestAnz']                         = 'Anzahl Bestellungen';
            $_lang_user['dsPDFGes']                             = 'Gesetzestexte';
            $_lang_user['dsPDFGesAGB']                          = 'Allgemiene Gesch&auml;ftsbedingungen (AGBs)';
            $_lang_user['dsPDFGesImp']                          = 'Impressum';

            // Prefs: AV
            $_lang_user['dsAV']                                 = 'Auftragsverarbeitungsvertrag';
            $_lang_user['dsAVInfo']                             = 'Aufgrund des Datenschutzgesetzes (DSGVO) ist es notwendig, einen Auftragsverarbeitungsvertrag zwischen Ihnen und Uns abzuschliessen.<br />Bitte lesen Sei sich den Auftragsverabeitungsvertrag sorgf&auml;ltig durch und best&auml;tigen Sie bitte, dass Sie diesem Vertrag zustimmen. Erst mit Zustimmung des Vertrags ist eine Weiternutzung unseres E-Mail-Services m&ouml;glich.';
            $_lang_user['dsMSGAVZustimmen']                     = 'Bitte stimmen Sie dem AV-Vertrag zu';
            $_lang_user['dsAVBestatigung']                      = 'AV-Vertrag gem. Art. 28 DSGVO zustimmen';
            $_lang_user['dsAVZustimmen']                        = 'Zustimmen';
            $_lang_user['dsAVNichtZustimmen']                   = 'Nicht Zustimmen';
            $_lang_user['dsAVKontoLoeschen']                    = 'Damit Sie unser System weiter verwenden k&ouml;nnen m&uuml;ssen Sie dem Vertrag zustimmen, ansonsten k&ouml;nnen Sie unser System nicht weiter benutzen. Wen Sie dennoch diese Meldung best&auml;tigen wird Ihr Konto deaktiviert und bei der n&auml;chsten L&ouml;schung komplett von unserem System gel&ouml;scht.';
            $_lang_user['dsAVModalTitel']                       = 'ADV-Vereinbarung';
            $_lang_user['dsAVModalDatum']                       = 'Der Vertrag wurde am folgenden Datum abgeschlossen:';

            // Admin: Allgemein
            $_lang_admin['dsGeneral']                           = 'Allgemein';
            $_lang_admin['dsGeneralBasic']                      = 'Grundangaben';
            $_lang_admin['dsGeneralName']                       = 'Name:<br /><small>Vornamen und Name, bzw. Firma bei Unternehmen</small>';
            $_lang_admin['dsGeneralOptional']                   = 'Optionale Inforamtionen:<br /><small>c/o, Geb&auml;udenummer, etc.</small>';
            $_lang_admin['dsGeneralStreet']                     = 'Strasse und Hausnummer:<br /><small>&nbsp;</small>';
            $_lang_admin['dsGeneralPLZPlace']                   = 'Postleitzahl und der Ort:<br /><small>&nbsp;</small>';
            $_lang_admin['dsGeneralCountry']                    = 'Land:<br /><small>Zu empfehlen bei internationalem Publikum</small>';

            $_lang_admin['dsContact']                           = 'Kontaktm&ouml;glichkeiten';
            $_lang_admin['dsContactMail']                       = 'Kontakt E-Mail-Adresse';
            $_lang_admin['dsPrivacyMail']                       = 'Datenschutz E-Mail-Adresse';
            $_lang_admin['dsPhone']                             = 'Telefonnummer';
            $_lang_admin['dsFax']                               = 'Fax-Nummer';
            $_lang_admin['dsContactForm']                       = 'URL des Kontaktformulars';
            $_lang_admin['dsMore']                              = 'Weitere Kontaktm&ouml;glichkeiten';

            $_lang_admin['dsCompanySettings']                   = 'Firmeneinstellungen';
            $_lang_admin['dsAuthorizedToRepresent']             = 'Angaben zu Vertretungsberechtigten:<br /><small>Bei Unternehmen, Vereinen, Beh&ouml;rden</small>';
            $_lang_admin['dsUstID']                             = 'Umsatzsteuer Identifikationsnummer:<br /><small>USt-ID</small>';
            $_lang_admin['dsWidNr']                             = 'Wirtschafts-Identifikationsnummer:<br /><small>W-IdNr</small>';
            $_lang_admin['dsBusinessArea']                      = 'Gesch&auml;ftsbereich:<br /><small>In manchen Ländern, z.B. &Ouml;sterreich erforderlich</small>';
            $_lang_admin['dsAGBUrl']                            = 'Link zu Ihren AGB:<br /><small>Sofern diese online gestellt sind</small>';
            $_lang_admin['dsRegistryCourt']                     = 'Name des Registers:<br /><small>z.B. Name des Registergerichts</small>';
            $_lang_admin['dsRegistryCourtNr']                   = 'Nummer im Register:<br /><small>z.B. HRB 1234</small>';

            $_lang_admin['dsImpressum']                         = 'Zusatztexte f&uuml;r das Impressum';
            $_lang_admin['dsImpressumContent']                  = 'Impressum';
            $_lang_admin['dsImpressumContentDesc']              = 'Kompletter Impressumstext';
            $_lang_admin['dsConsumerDisputeResolution']         = 'Streitschlichtung und Verbraucherstreitbeilegung';
            $_lang_admin['dsConsumerDisputeResolutionDesc']     = 'Schliessen Sie mit Verbrauchern online Vertr&auml;ge &uuml;ber Waren oder Dienstleistungen?';
            $_lang_admin['dsSocialMedia']                       = 'Social Media und andere Onlinepr&auml;senzen';
            $_lang_admin['dsSocialMediaDesc']                   = 'Verlinken Sie von Profilen oder Seiten in sozialen Netzwerken oder auf Onlineplattformen auf Ihr Impressum auf der Website?';
            $_lang_admin['dsCopyrightNotices']                  = 'Haftungs- und Rechtshinweise';
            $_lang_admin['dsCopyrightNoticesDesc']              = 'M&ouml;chten Sie Hinweise zur Haftung, bzw. deren Ausschluss, Urheber- sowie Markenrechten aufnehmen?';
            $_lang_admin['dsPhotoCredits']                      = 'Bildquellen';
            $_lang_admin['dsPhotoCreditsDesc']                  = 'M&ouml;chten Sie Angaben zu Bildquellen im Impressum angeben?';


            $_lang_admin['dsLogo']                              = 'Firmenlogo';
            $_lang_admin['dsLogoBild']                          = 'Aktuelles Logo';
            $_lang_admin['dsLogoEntfernen']                     = 'Logo entfernen?';

            // Admin: Datenschutz
            $_lang_admin['dsText']                              = 'Texte';
            $_lang_admin['dsDatenschutz']                       = 'Datenschutz';
            $_lang_admin['dsNeueReports']                       = '<strong>%d</strong> Datenschutz Report(s) warten auf Genehmigung durch Sie.';
            $_lang_admin['dsInfoUpdate']                        = 'Ein Update f&uuml;r das <strong>'. $this->name .'</strong> Plugin mit der Version <strong>%s</strong> steht zu Verf&uuml;gung, bitte aktualisieren Sie das Plugin demn&auml;chst.';
            $_lang_admin['dsInfoPHPClassHash']                  = 'Ein oder mehrere PHP Klassen vom <strong>'. $this->name .'</strong> Plugin sind nicht mehr aktuell.';
            $_lang_admin['dsPHPMemory']                         = 'F&uuml;r den Datenschutz Report wird mindestens <strong>384 MB</strong> an PHP Memory ben&ouml;tigt, eingestellt ist aber nur <strong>%d MB</strong>. Dies kann zu Problemen beim generieren der PDFs f&uuml;hren!';
            $_lang_admin['dsTCPDFnot']                          = 'F&uuml;r das Datenschutz Plugin wird TCPDF ben&ouml;tigt, aktuell ist die PHP Klasse aber nicht vorhanden auf dem System!';

            // Admin: Info
            $_lang_admin['dsMSGSave']                           = 'Angaben wurden gespeichert';
            $_lang_admin['dsMSGTitel']                          = 'Bitte einen Titel angeben';
            $_lang_admin['dsMSGText']                           = 'Bitte einen Text angeben';
            $_lang_admin['dsMSGDSMail']                         = 'Bitte eine Datenschutz E-Mail-Adresse angeben';
            $_lang_admin['dsMSGLogoDel']                        = 'Logo wurde entfernt';
            $_lang_admin['dsMSGKonf']                           = 'Einstellungen wurde gespeichert';
            $_lang_admin['dsMSGSpalte']                         = 'Bitte eine Benutzer Spalte ausw&auml;hlen';
            $_lang_admin['dsMSGTextSave']                       = 'Datenschutztext wurde gespeichert';
            $_lang_admin['dsMSGTextDelSave']                    = 'Datenschutztext wurde gel&ouml;scht';
            $_lang_admin['dsMSGImprSave']                       = 'Impressumstexte wurden gespeichert';
            $_lang_admin['dsStatusTextEnd']                     = 'Reportgenerierung beendet';
            $_lang_admin['dsStatusTextPlan']                    = 'Reportgenerierung geplant';
            $_lang_admin['dsStatusTextAcce']                    = 'Reportgenerierung akzeptiert';
            $_lang_admin['dsStatusTextLauf']                    = 'Reportgenerierung l&auml;uft';

            $_lang_admin['dsSortierung']                        = 'Sortierung';
            $_lang_admin['dsLang']                              = 'Sprache';
            $_lang_admin['dsAbfr']                              = 'Abfragen';
            $_lang_admin['dsSave']                              = 'Speichern';
            $_lang_admin['dsAdd']                               = 'hinzuf&uuml;gen';
            $_lang_admin['dsEdit']                              = 'bearbeiten';

            // Admin: Reports
            $_lang_admin['dsReports']                           = 'Reports';
            $_lang_admin['dsReportShowAll']                     = 'Alle Reports anzeigen';
            $_lang_admin['dsReportShowNew']                     = 'Offene Reports anzeigen';
            $_lang_admin['dsReportsUser']                       = 'Benutzer';
            $_lang_admin['dsReportsDate']                       = 'Datum';
            $_lang_admin['dsReportsUpdate']                     = 'Update';
            $_lang_admin['dsReportsAktion']                     = 'Aktion';
            $_lang_admin['dsReportsAVUser']                     = 'Alle erstellten Reports anzeigen';

            // Admin: Module
            $_lang_admin['dsModule']                            = 'Module';
            $_lang_admin['dsModuleInfo']                        = 'Information';
            $_lang_admin['dsModuleInfoText']                    = 'Dieser Bereich ist f&uuml;r das h&auml;ndische hinzuf&uuml;gen von weiteren Datenquellen zum PDF Report, wichtig ist das in der Tabelle eine Benutzer ID oder E-Mail-Adresse f&uuml;r das zuordnen enthalten ist.';
            $_lang_admin['dsModuleAdd']                         = 'Datenquelle hinzuf&uuml;gen';
            $_lang_admin['dsModuleAddName']                     = 'Module Name';
            $_lang_admin['dsModuleAddTabelle']                  = 'Datenbank Tabelle';
            $_lang_admin['dsModuleAddBenutzer']                 = 'Benutzer';
            $_lang_admin['dsModuleAddBenutzerInfo']             = 'Dieser Benutzer wird nur f&uuml;r eine Test Abfrage benutzt, was und wie viele spalten im Report angezeigt werden.';
            $_lang_admin['dsModuleAddBenZuordnung']             = 'Benutzer zu Tabelle';
            $_lang_admin['dsModuleAddBenZuordnungInfo']         = 'Welche spalte beinhaltet die BenutzerID oder E-Mail-Adresse?';
            $_lang_admin['dsModuleAddShow']                     = 'Reportansicht';
            $_lang_admin['dsModuleName']                        = 'Name auf dem Report';
            $_lang_admin['dsModuleTabelle']                     = 'Datenbank Tabelle';
            $_lang_admin['dsModuleSpalten']                     = 'Ausgew&auml;hlte Spalten';
            $_lang_admin['dsModuleAll']                         = 'Alle bereits vorhandenen Module';
            $_lang_admin['dsModuleEx']                          = 'Externe Module';
            $_lang_admin['dsModuleExInfo']                      = 'Komplexe Module k&ouml;nnen in Eigenverantwortung nachgeladen werden, wichtig ist das wir f&uuml;r Fehler oder sonstige Probleme die dadurch entstehen keine Verantwortung &uuml;bernehmen.';
            $_lang_admin['dsModuleDatei']                       = 'Datei';

            // Admin: AV
            $_lang_admin['dsAV']                                = 'AV-Vertrag';

            // Admin: Cookie
            $_lang_admin['dsCookie']                            = 'Cookie';
            $_lang_admin['dsCookieInfo']                        = 'Cookie Information wird erstellt mit <a href="https://www.osano.com/" target="_blank">OSANO</a>, die beliebteste L&ouml;sung f&uuml;r das EU-Cookie-Gesetz';
            $_lang_admin['dsCookieAllgemein']                   = 'Allgemein';
            $_lang_admin['dsCookieAktivieren']                  = 'Cookie aktivieren';
            $_lang_admin['dsCookieNLIAktivierenBesch']          = 'Im Nichtangemeldeten Bereich anzeigen?';
            $_lang_admin['dsCookieLIAktivierenBesch']           = 'Im Angemeldeten Bereich anzeigen?';
            $_lang_admin['dsCookieDenyAktivieren']              = 'Cookies l&ouml;schen';
            $_lang_admin['dsCookieDenyAktivierenBesch']         = 'Wenn Cookies nicht erlaubt werden sollen alle bestehenden (bis auf System relevante Cookies) gel&ouml;scht werden?';
            $_lang_admin['dsCookieSystem']                      = 'System Cookies';
            $_lang_admin['dsCookieSystemInfo']                  = 'Ein Cookie pro Zeile';
            $_lang_admin['dsCookieSystemBesch']                 = 'Diese Cookies werden vom System ben&ouml;tigt und m&uuml;ssen immer geladen werden.';
            $_lang_admin['dsCookieKonfigurationJS']             = 'JavaScripte die geladen / entladen werden sobald die Cookie Info best&auml;tigt wird';
            $_lang_admin['dsCookieKonfigurationJSAllow']        = 'Cookies erlauben';
            $_lang_admin['dsCookieKonfigurationJSAllowBesch']   = 'Dieser Script Block wird geladen wen Cookies erlaubt sind';
            $_lang_admin['dsCookieKonfigurationJSDeny']         = 'Cookies nicht erlauben';
            $_lang_admin['dsCookieKonfigurationJSDenyBesch']    = 'Dieser Script Block wird geladen wen Cookies <strong>nicht</strong> erlaubt sind';
            $_lang_admin['dsCookieKonfigurationJSOpt']          = 'Optional';
            $_lang_admin['dsCookieKonfigurationJSOptBesch']     = 'Alternativ k&ouml;nnen mit dem folgenden Code Block eigene Scripte oder Bereiche aktiviert oder deaktiviert werden.';
            $_lang_admin['dsCookieDesign']                      = 'Darstellung';
            $_lang_admin['dsCookieDesignBG']                    = 'Info Hintergrund';
            $_lang_admin['dsCookieDesignFG']                    = 'Info Text Farbe';
            $_lang_admin['dsCookieDesignLinkBG']                = 'Knopf Hintergrund';
            $_lang_admin['dsCookieDesignLinkFG']                = 'Knopf Text Farbe';
            $_lang_admin['dsCookieTheme']                       = 'Template';
            $_lang_admin['dsCookiePosition']                    = 'Infobox Position';
            $_lang_admin['dsCookieType']                        = 'Einbindung';
            $_lang_admin['dsCookieTypeInfo']                    = 'Sagen Sie den Benutzern einfach, dass wir Cookies verwenden';
            $_lang_admin['dsCookieTypeOptout']                  = 'Benutzer k&ouml;nnen Cookies deaktivieren (Opt-Out)';
            $_lang_admin['dsCookieTypeOptin']                   = 'Bitten Sie die Benutzer, sich f&uuml;r Cookies zu entscheiden (Opt-In)';
            $_lang_admin['dsCookieKonfiguration']               = 'Konfiguration';
            $_lang_admin['dsCookieText']                        = 'Text';
            $_lang_admin['dsCookieClose']                       = 'Nicht erlauben';
            $_lang_admin['dsCookieAllow']                       = 'Erlauben';
            $_lang_admin['dsCookieLinkText']                    = 'Link Text';
            $_lang_admin['dsCookieLink']                        = 'Link';
            $_lang_admin['dsCookieSpeichern']                   = 'Speichern';
            $_lang_admin['dsCookieTemplate']                    = 'Cookie Template integration';
            $_lang_admin['dsMSGCookieKonf']                     = 'Konfiguration wurde gespeichert.';

            // Admin: Deref
            $_lang_admin['dsDerefTitle']                        = 'Weiterleitung';
            $_lang_admin['dsDerefStatus']                       = 'Status';
            $_lang_admin['dsDerefStatusBesch']                  = 'Informationsseite f&uuml;r externe Links aktivieren';
            $_lang_admin['dsDerefTemplate']                     = 'Template';
            $_lang_admin['dsMSGDeref']                          = 'Weiterleitung wurde gespeichert';
            $_lang_admin['dsMSGDerefBackup']                    = 'Backup der Datei "/templates/../nli/deref.tpl" wurde erstellt';

            // Admin: Konfiguration
            $_lang_admin['dsConfig']                            = 'Einstellungen';
            $_lang_admin['dsAllgemein']                         = 'Allgemein';
            $_lang_admin['dsReportKosten']                      = 'Report Kosten';
            $_lang_admin['dsGesetzestexte']                     = 'Gesetzestexte';
            $_lang_admin['dsPDFOptionen']                       = 'PDF Optionen';
            $_lang_admin['dsPDFOptionenInfo']                   = 'Hiermit werden folgende Informationen im PDF Report angezeigt.';
            $_lang_admin['dsShowImpressum']                     = 'Impressum';
            $_lang_admin['dsShowImpressumBesch']                = 'Datenschutz konformes Impressum anzeigen (mit den Angaben aus dem "Allgemein" Tab)';
            $_lang_admin['dsAutoPDFGen']                        = 'Automatisch generieren';
            $_lang_admin['dsAutoPDFGenBesch']                   = 'PDFs automatisch freigeben (ohne Best&auml;tigung durch den Administrator)';
            $_lang_admin['dsPDFMails']                          = 'E-Mails';
            $_lang_admin['dsPDFKalender']                       = 'Kalender';
            $_lang_admin['dsPDFKontakte']                       = 'Kontakte';
            $_lang_admin['dsPDFNotizen']                        = 'Notizen';
            $_lang_admin['dsPDFAufgaben']                       = 'Aufgaben';
            $_lang_admin['dsPDFDaten']                          = 'Daten';
            $_lang_admin['dsPDFBest']                           = 'Bestellungen';
            $_lang_admin['dsSystemLink']                        = 'Zu den Texten';
            $_lang_admin['dsProfileFelder']                     = 'ProfileFelder';
            $_lang_admin['dsFirstLogs']                         = 'Letzte System Logs';
            $_lang_admin['dsLogsInfo']                          = 'Diese Einstellung muss auf dem Webserver vorgenommen werden und ist rein Informativ f&uuml;r den Benutzer.';
            $_lang_admin['dsLogsWebAcc']                        = 'Webserver Access Logs';
            $_lang_admin['dsLogsWebErr']                        = 'Webserver Error Logs';
            $_lang_admin['dsLogsB1GMail']                       = 'B1GMail Logs';
            $_lang_admin['dsLogsB1GMailServer']                 = 'B1GMailServer Logs';
            $_lang_admin['dsLogArchive']                        = 'Log Archive';
            $_lang_admin['dsLastLog']                           = 'Letzter Logeintrag';
            $_lang_admin['dsInhalt']                            = 'Enth&auml;lt';
            $_lang_admin['dsAktivieren']                        = 'Aktivieren';
            $_lang_admin['dsAnzeigen']                          = 'Anzeigen';
            $_lang_admin['dsStatus']                            = 'Status';
            $_lang_admin['dsArchiv']                            = 'Archivieren';
            $_lang_admin['dsCron']                              = 'Automatisches L&ouml;schen';
            $_lang_admin['dsZeit']                              = 'Vorhaltezeit';

            // Admin: Pflicht
            $_lang_admin['dsPflicht']                           = 'Pflichtfelder';
            $_lang_admin['dsPflichtBeshr']                      = 'Mit dieser Konfiguration k&ouml;nnen die Global definierten Pflichtfelder pro Gruppe &uuml;bersteuert werden. Falls nichts ausgew&auml;hlt ist wird die Globale Option verwendet.';
            $_lang_admin['dsPflichtNicht']                      = 'Nicht ausgew&auml;hlt';

            // Admin: Hilfe
            $_lang_admin['dsHilfe']                             = 'Hilfe';
            $_lang_admin['dsSupport']                           = 'Hilfe & Support';
            $_lang_admin['dsLegende']                           = 'Legende';
            $_lang_admin['dsStatusOFF']                         = 'Reportgenerierung beendet';
            $_lang_admin['dsStatusONG']                         = 'Reportgenerierung geplant';
            $_lang_admin['dsStatusONA']                         = 'Reportgenerierung akzeptiert';
            $_lang_admin['dsStatusON']                          = 'Reportgenerierung l&auml;uft';
            $_lang_admin['dsTCPDFOn']                           = 'ist vorhadnen';
            $_lang_admin['dsTCPDFOff']                          = 'ist nicht vorhadnen';
            $_lang_admin['dsPlatzhalterVerz']                   = 'Platzhalterverzeichnis';
            $_lang_admin['dsPlatzhalter']                       = 'Platzhalter';
            $_lang_admin['dsPlatzhalterBesch']                  = 'Beschreibung';
            $_lang_admin['dsPlatzhalterInhalt']                 = 'Inhalt';
            $_lang_admin['dsVorlageLaden']                      = 'Vorlage Dokumente laden';
            $_lang_admin['dsVorlageLadenBeschr']                = 'Mit dieser Funktion k&ouml;nnen Textvorlagen in die Datenbank geladen werden.';
            $_lang_admin['dsVorlageWarnung']                    = 'Bestehende Eintr&auml;ge werden gel&ouml;scht!';
            $_lang_admin['dsVorlageImport']                     = 'Dokumentenvorlagen wurden Importiert, bitte passen Sie die n&ouml;tigen Platzhalter in den Texten an.';
        } else {
            // General texts
            $_lang_custom['DatenschutzVerantwortung']           = "Responsible body in the sense of the data protection laws, in particular the EU data protection basic regulation (DSGVO), is:<br /><strong>[%name%]</strong><br />[%street%] [%streetnr%]<br />[%plz%] [%place%]<br />[%country%]<br /><br />address and further information must in the \"Customizable texts\" - \"PrivacyResponsibility\" added become.<br />Settings -> Language -> Customizable texts";
            $_lang_custom['DatenschutzAbschluss']               = "jurisdiction forr all disputes out  legal relationships with <strong>[%name%]</strong> respectively at the Seat the society.<br />PLACE, MONTH, YEAR<br /><br />Addresses and more information must in the \"Customizable texts\" - \"PrivacyStatements\" added become.<br />ESettings -> Language -> Customizable texts";
            $_lang_custom['DatenschutzMailBetreff']             = "Privacy Report was created";
            $_lang_custom['DatenschutzMailText']                = "Dear Ladies and Gentlemen,\n\nthe Privacy Report has been created and available to you \"Settings\" - \"Privacy\" as PDF available. \n\nbest regards\nService-Team\n\n(This e-mail was automatically created!)";
            $_lang_custom['DatenschutzNewReportBetreff']        = "Privacy Report Waiting for Acceptance";
            $_lang_custom['DatenschutzNewReportText']           = "Dear Ladies and Gentlemen,\n\na privacy report has been requested for the user \"%%USERNAME%%\", please accept via the administration page.\n\nbest regards\nService-Team\n\n(This e-mail was automatically created!)";
            $_lang_custom['DatenschutzPDFBegruessung']          = "[%anrede%]\n\nwe thank you for your request of [%aktdatum%].\n\nOn the following pages we have compiled the personal data for you that could be assigned to your account.\n\n\n\nSincerely yours\n\n[%name%]";

            // Benachrichtigung
            $_lang_custom['notify_ds']	                        = '<strong>Privacy</strong><br /> %s';
            $_lang_custom['ds_notify_abschluss']                = 'Report was generated';

            // Weiterletung
            $_lang_user['dsDerefTitle']                         = 'Redirect';
            $_lang_user['dsDerefRedirect']                      = 'The selected link is outside of our page, so that the page is opened please select the following link.';
            $_lang_user['dsDerefBack']                          = 'If you do not want to visit that page, you can <a href="#" onclick="self.close()">close this window</a>.';

            // Impressum
            $_lang_user['dsImpressum']                          = 'Imprint';
            $_lang_user['dsConsumerDisputeResolution']          = 'Dispute settlement and consumer dispute settlement';
            $_lang_user['dsSocialMedia']                        = 'Social media and other online presences';
            $_lang_user['dsCopyrightNotices']                   = 'Liability and legal notices';
            $_lang_user['dsPhotoCredits']                       = 'Image sources';

            // AV Speichern
            $_lang_user['dsAVMSGSave']                          = 'Your AV contract has been saved, see the privacy settings for more information.';
            $_lang_user['dsAVAnzeigen']                         = 'Show AV contract';
            $_lang_user['dsAVAblehnen']                         = 'The user account was deactivated because the AV contract was not accepted.';
            $_lang_user['dsAVSchliessen']                       = 'close';

            // Allgemein
            $_lang_user['DatenschutzAcceptCookies']             = "You can only log in if you have accepted the cookie notice, so please read our <a href=\"index.php?action=Datenschutz\">guidelines</a> first and confirm with \"Allow cookies\".";

            // Registration
            $_lang_user['dsRegLink']                            = 'Privacy Policy';
            $_lang_user['dsRegAccept']                          = 'I accept the';
            $_lang_user['dsRegError']                           = 'The privacy must be read and accepted so that the registration can be completed';

            // Datenschutz Seite
            $_lang_user['dsTitel']                              = 'Privacy';
            $_lang_user['dsLogs']                               = 'Logs';
            $_lang_user['dsLogsWebAcc']                         = 'Webserver Access Logs';
            $_lang_user['dsLogsWebErr']                         = 'Webserver Error Logs';
            $_lang_user['dsLogsWebMail']                        = 'Webmail Logs';
            $_lang_user['dsLogsMail']                           = 'E-Mail Server Logs';
            $_lang_user['dsLogArchive']                         = 'Archiving';
            $_lang_user['dsInhalt']                             = 'Contains';
            $_lang_user['dsZeit']                               = 'Derivative';

            // Prefs
            $_lang_user['ds']                                   = 'Privacy';
            $_lang_user['prefs_d_ds']                           = 'An overview of the data stored by us.';
            $_lang_user['dsReport']                             = 'Privacy Report';
            $_lang_user['dsLoeschen']                           = 'Rebuild the privacy report';
            $_lang_user['dsLoeschenNachricht']                  = 'For a renewed creation of the data protection report you have to pay costs, are you sure that the data protection report is to be re-created?';
            $_lang_user['dsPrefsAuswahl']                       = 'Data selection';

            $_lang_user['dsPrefsMSGAdd']                        = 'Task is being processed, as soon as the task is completed and the document has been generated, you will be informed by e-mail.';

            $_lang_user['dsPrefsInformation']                   = 'According to Art. 15 DSGVO you have the right to receive one-time and free information about the personal data stored by us. By clicking on the button "Create document" you can request your data. Should you also require further copies or request the information again at a later date, we are entitled, in accordance with Art. 12 GDPR, to pay you a management fee i.H.v. %KOSTEN% for any further information.';

            $_lang_user['dsPrefsAllgemein']                     = 'General';
            $_lang_user['dsPrefsAllgemeinBeschr']               = 'General information about this area';
            $_lang_user['dsPrefsProfile']                       = 'Userprofile';
            $_lang_user['dsPrefsProfileBeschr']                 = 'Show all profile information';
            $_lang_user['dsPrefsVerarbeitung']                  = 'Processing purposes';
            $_lang_user['dsPrefsVerarbeitungBeschr']            = 'What is the data used for?';
            $_lang_user['dsPrefsDatenkat']                      = 'Data Categories';
            $_lang_user['dsPrefsDatenkatBeschr']                = 'What is being processed?';
            $_lang_user['dsPrefsDatenempf']                     = 'Data receiver';
            $_lang_user['dsPrefsDatenempfBeschr']               = 'To whom are the data shared?';
            $_lang_user['dsPrefsRZ']                            = 'Datacenter';
            $_lang_user['dsPrefsRZBeschr']                      = 'Where is the data center??';
            $_lang_user['dsPrefsBackup']                        = 'Backup';
            $_lang_user['dsPrefsBackupBeschr']                  = 'Where is the backup stored?';
            $_lang_user['dsPrefsReg']                           = 'Registration';
            $_lang_user['dsPrefsRegBeschr']                     = 'When was my e-mail account created?';
            $_lang_user['dsPrefsLWeb']                          = 'Last Web Login';
            $_lang_user['dsPrefsLWebBeschr']                    = 'Last login to Webmail';
            $_lang_user['dsPrefsLPOP']                          = 'Last POP3 Login';
            $_lang_user['dsPrefsLPOPBeschr']                    = 'Last POP3 access';
            $_lang_user['dsPrefsLIMAP']                         = 'Last IMAP Login';
            $_lang_user['dsPrefsLIMAPBeschr']                   = 'Last IMAP access';
            $_lang_user['dsPrefsLSMTP']                         = 'Last SMTP Login';
            $_lang_user['dsPrefsLSMTPBeschr']                   = 'Last SMTP access';

            $_lang_user['dsDaten']                              = 'Personal data';
            $_lang_user['dsPrefsDaten']                         = 'Data';
            $_lang_user['dsPrefsDatenBeschr']                   = 'Personal data (e-mails, calendars, contacts, etc.)';

            $_lang_user['dsPrefsBestellungen']                  = 'Orders';
            $_lang_user['dsBestellAllgemein']                   = 'General';

            $_lang_user['dsModule']                             = 'Modules';
            $_lang_user['dsModuleBeschr']                       = 'All modules saving the data are output';

            $_lang_user['dsPrefsInfo']                          = 'Information';
            $_lang_user['dsInfoGes']                            = 'Legal texts';
            $_lang_user['dsInfoGesBeschr']                      = 'All legal texts (Example.: imprint, conditions and Privacy) output';

            $_lang_user['dsDokumentErstellen']                  = 'Request a document';
            $_lang_user['dsReportLoeschen']                     = 'Really remove evaluation irrevocably?';

            $_lang_user['dsStatusTextEnd']                      = 'The generation of the report is completed.';
            $_lang_user['dsStatusTextPlan']                     = 'The report has been submitted, please wait until an administrator accepts the report.';
            $_lang_user['dsStatusTextAcce']                     = 'The report request has been accepted, we will inform you who the generation was completed.';
            $_lang_user['dsStatusTextLauf']                     = 'The report is currently being generated, please be patient.';

            // Prefs: PDF
            $_lang_user['dsPDFTitel']                           = 'Privacy report';
            $_lang_user['dsPDFErstell']                         = 'Creation date';
            $_lang_user['dsPDFAnrede']                          = 'Good day';
            $_lang_user['dsPDFAnredeHerr']                      = 'Dear sir';
            $_lang_user['dsPDFAnredeFrau']                      = 'Dear Mrs';
            $_lang_user['dsPDFAllgemein']                       = 'General';
            $_lang_user['dsPDFProfil']                          = 'User profile';
            $_lang_user['dsPDFMail']                            = 'E-Mail-Address';
            $_lang_user['dsPDFAMail']                           = 'Alternate Email Address';
            $_lang_user['dsPDFName']                            = 'First and Last Name';
            $_lang_user['dsPDFStrasse']                         = 'Street and Number';
            $_lang_user['dsPDFOrt']                             = 'Postcode / City';
            $_lang_user['dsPDFLand']                            = 'Land';
            $_lang_user['dsPDFTel']                             = 'Phone number';
            $_lang_user['dsPDFFax']                             = 'Fax number';
            $_lang_user['dsPDFReg']                             = 'Registration';
            $_lang_user['dsPDFRegDatum']                        = 'Date';
            $_lang_user['dsPDFRegIP']                           = 'IP-Address';
            $_lang_user['dsPDFDVerarbeitung']                   = 'Processing purposes';
            $_lang_user['dsPDFDDatenkat']                       = 'Data Categories';
            $_lang_user['dsPDFDDatenempf']                      = 'Data receiver';
            $_lang_user['dsPDFMails']                           = 'E-Mails';
            $_lang_user['dsPDFMailsAnz']                        = 'Number of emails in the mailbox';
            $_lang_user['dsPDFMailsAnzO']                       = 'Number of folders in the mailbox';
            $_lang_user['dsPDFMailsSize']                       = 'Mailbox size';
            $_lang_user['dsPDFMailsLMail']                      = 'Last e-mail sent';
            $_lang_user['dsPDFMailsLIP']                        = 'Last IP address';
            $_lang_user['dsPDFMailsLWeb']                       = 'Last web login';
            $_lang_user['dsPDFMailsLPOP3']                      = 'Last POP3 login';
            $_lang_user['dsPDFMailsLIMAP']                      = 'Last IMAP login';
            $_lang_user['dsPDFMailsLSMTP']                      = 'Last SMTP login';
            $_lang_user['dsPDFAMails']                          = 'All e-Mails';
            $_lang_user['dsPDFAMailsVon']                       = 'From';
            $_lang_user['dsPDFAMailsAn']                        = 'To';
            $_lang_user['dsPDFAMailsDatum']                     = 'Date';
            $_lang_user['dsPDFAMailsSize']                      = 'Size';
            $_lang_user['dsPDFKalender']                        = 'Calendar';
            $_lang_user['dsPDFKalenderAnz']                     = 'Number of appointments';
            $_lang_user['dsPDFKalenderGAnz']                    = 'Number of appointments groups';
            $_lang_user['dsPDFKontakte']                        = 'Contacts';
            $_lang_user['dsPDFKontakteAnz']                     = 'Number of contacts';
            $_lang_user['dsPDFKontakteGAnz']                    = 'Number of contact groups';
            $_lang_user['dsPDFNotizen']                         = 'Notes';
            $_lang_user['dsPDFNotizenAnz']                      = 'Number of notes';
            $_lang_user['dsPDFAufgaben']                        = 'Tasks';
            $_lang_user['dsPDFAufgabenAnz']                     = 'Number of tasks';
            $_lang_user['dsPDFDaten']                           = 'Dates';
            $_lang_user['dsPDFDatenAnz']                        = 'Number of documents';
            $_lang_user['dsPDFDatenOAnz']                       = 'Number of folders';
            $_lang_user['dsPDFDatenSize']                       = 'Document size';
            $_lang_user['dsPDFADaten']                          = 'All dates';
            $_lang_user['dsPDFADatenName']                      = 'Filename';
            $_lang_user['dsPDFADatenSize']                      = 'Size';
            $_lang_user['dsPDFADatenErst']                      = 'Created';
            $_lang_user['dsPDFADatenBear']                      = 'Processed';
            $_lang_user['dsPDFADatenLZ']                        = 'Last access';
            $_lang_user['dsPDFBest']                            = 'Orders';
            $_lang_user['dsPDFBestAnz']                         = 'Number of orders';
            $_lang_user['dsPDFVer']                             = 'Processing purposes / Data categories / Data receivers';
            $_lang_user['dsPDFGes']                             = 'Legal texts';
            $_lang_user['dsPDFGesAGB']                          = 'General Terms of Business (Conditions)';
            $_lang_user['dsPDFGesImp']                          = 'Imprint';

            // Prefs: AV
            $_lang_user['dsAV']                                 = 'Job processing contract';
            $_lang_user['dsAVInfo']                             = 'Due to the Data Protection Act (DSGVO) it is necessary to conclude a contract processing contract between you and Us.<br />Please read carefully the order processing contract and please confirm that you agree to this contract. Only with the agreement of the contract, a re-use of our e-mail service is possible.';
            $_lang_user['dsMSGAVZustimmen']                     = 'AV contract acc. Art. 28 DSGVO agree';
            $_lang_user['dsAVBestatigung']                      = 'Agree to order data processing contract';
            $_lang_user['dsAVZustimmen']                        = 'Agree';
            $_lang_user['dsAVNichtZustimmen']                   = 'Disagree';
            $_lang_user['dsAVKontoLoeschen']                    = 'In order to continue using our system, you may agree to the agreement, otherwise you may not continue to use our system. However, if you confirm this message, your account will be disabled and completely deleted from our system at the time of the next lischarge.';
            $_lang_user['dsAVModalTitel']                       = 'ADV agreement';
            $_lang_user['dsAVModalDatum']                       = 'The contract was concluded on the following date:';

            // Admin: Allgemein
            $_lang_admin['dsGeneral']                           = 'General';
            $_lang_admin['dsGeneralBasic']                      = 'Basic information';
            $_lang_admin['dsGeneralName']                       = 'Name:<br /><small>First name and surname, or company at company</small>';
            $_lang_admin['dsGeneralOptional']                   = 'Optional information:<br /><small>c/o, Building number, etc.</small>';
            $_lang_admin['dsGeneralStreet']                     = 'Street and house number:<br /><small>&nbsp;</small>';
            $_lang_admin['dsGeneralPLZPlace']                   = 'ZIP code and the place:<br /><small>&nbsp;</small>';
            $_lang_admin['dsGeneralCountry']                    = 'Country:<br /><small>Recommended for an international audience</small>';

            $_lang_admin['dsContact']                           = 'Contact options';
            $_lang_admin['dsContactMail']                       = 'Contact email address';
            $_lang_admin['dsPrivacyMail']                       = 'Privacy email address';
            $_lang_admin['dsPhone']                             = 'Phone number';
            $_lang_admin['dsFax']                               = 'Fax number';
            $_lang_admin['dsContactForm']                       = 'URL of the contact form';
            $_lang_admin['dsMore']                              = 'More contact options';

            $_lang_admin['dsCompanySettings']                   = 'Company settings';
            $_lang_admin['dsAuthorizedToRepresent']             = 'Information about authorized representatives:<br /><small>With companies, associations, authorities</small>';
            $_lang_admin['dsUstID']                             = 'VAT identification number:<br /><small>USt-ID</small>';
            $_lang_admin['dsWidNr']                             = 'Economic identification number:<br /><small>W-IdNr</small>';
            $_lang_admin['dsBusinessArea']                      = 'Business area:<br /><small>In some countries, e.g. Austria required</small>';
            $_lang_admin['dsAGBUrl']                            = 'Link to your terms and conditions:<br /><small>If they are online</small>';
            $_lang_admin['dsRegistryCourt']                     = 'Name of the register:<br /><small>e.g. Name of the registry court</small>';
            $_lang_admin['dsRegistryCourtNr']                   = 'Number in the register:<br /><small>e.g. HRB 1234</small>';

            $_lang_admin['dsImpressum']                         = 'Additional texts for the imprint';
            $_lang_admin['dsImpressumContent']                  = 'Imprint';
            $_lang_admin['dsImpressumContentDesc']              = 'Complete imprint text';
            $_lang_admin['dsConsumerDisputeResolution']         = 'Dispute settlement and consumer dispute settlement';
            $_lang_admin['dsConsumerDisputeResolutionDesc']     = 'Do you conclude contracts for goods or services with consumers online?';
            $_lang_admin['dsSocialMedia']                       = 'Social media and other online presences';
            $_lang_admin['dsSocialMediaDesc']                   = 'Do you link from profiles or pages in social networks or on online platforms to your imprint on the website?';
            $_lang_admin['dsCopyrightNotices']                  = 'Liability and legal notices';
            $_lang_admin['dsCopyrightNoticesDesc']              = 'Would you like to include information on liability or exclusion, copyright and trademark rights?';
            $_lang_admin['dsPhotoCredits']                      = 'Image sources';
            $_lang_admin['dsPhotoCreditsDesc']                  = 'Would you like to provide information on image sources in the imprint?';

            $_lang_admin['dsLogo']                              = 'Companylogo';
            $_lang_admin['dsLogoBild']                          = 'Current Logo';
            $_lang_admin['dsLogoEntfernen']                     = 'Remove logo?';

            // Admin: Datenschutz
            $_lang_admin['dsText']                              = 'Text';
            $_lang_admin['dsDatenschutz']                       = 'Privacy policy';
            $_lang_admin['dsNeueReports']                       = '<strong>%d</strong> Privacy Report(s) awaiting explanation by you.';
            $_lang_admin['dsInfoUpdate']                        = 'An update for the <strong>'. $this->name .'</strong> plugin with the version <strong>%s</strong> is available, please update the plugin soon.';
            $_lang_admin['dsInfoPHPClassHash']                  = 'One or more PHP classes from the <strong> '. $this->name .'</strong> Plugins are out of date';
            $_lang_admin['dsPHPMemory']                         = 'The privacy report requires at least <strong>384 MB</strong> of PHP Memory, but is only set to <strong>%d MB</strong>. This can lead to problems when generating the PDFs!';
            $_lang_admin['dsTCPDFnot']                          = 'For the privacy plugin TCPDF is required, currently the PHP class is not available on the system!';

            // Admin: Info
            $_lang_admin['dsMSGSave']                           = 'Information was saved';
            $_lang_admin['dsMSGTitel']                          = 'Please specify a title';
            $_lang_admin['dsMSGText']                           = 'Please specify a text';
            $_lang_admin['dsMSGDSMail']                         = 'Please specify a privacy e-mail address';
            $_lang_admin['dsMSGLogoDel']                        = 'Logo has been removed';
            $_lang_admin['dsMSGKonf']                           = 'Settings were saved';
            $_lang_admin['dsMSGSpalte']                         = 'Please select a user column';
            $_lang_admin['dsMSGTextSave']                       = 'Data protection text has been saved';
            $_lang_admin['dsMSGTextDelSave']                    = 'Data protection text has been deleted';
            $_lang_admin['dsMSGImprSave']                       = 'Imprint texts have been saved';
            $_lang_admin['dsStatusTextEnd']                     = 'Report generation completed';
            $_lang_admin['dsStatusTextPlan']                    = 'Report generation planned';
            $_lang_admin['dsStatusTextAcce']                    = 'Report generation accepted';
            $_lang_admin['dsStatusTextLauf']                    = 'Report generation is running';

            $_lang_admin['dsSortierung']                        = 'Sorting';
            $_lang_admin['dsLang']                              = 'Language';
            $_lang_admin['dsAbfr']                              = 'Interrogate';
            $_lang_admin['dsSave']                              = 'Save';
            $_lang_admin['dsAdd']                               = 'Add';
            $_lang_admin['dsEdit']                              = 'to edit';

            // Admin: Reports
            $_lang_admin['dsReports']                           = 'Reports';
            $_lang_admin['dsReportShowAll']                     = 'Show all reports';
            $_lang_admin['dsReportShowNew']                     = 'Show open reports';
            $_lang_admin['dsReportsUser']                       = 'User';
            $_lang_admin['dsReportsDate']                       = 'Date';
            $_lang_admin['dsReportsUpdate']                     = 'Update';
            $_lang_admin['dsReportsAktion']                     = 'Action';
            $_lang_admin['dsReportsAVUser']                     = 'Show all reports created';

            // Admin: Module
            $_lang_admin['dsModule']                            = 'Modules';
            $_lang_admin['dsModuleInfo']                        = 'Information';
            $_lang_admin['dsModuleInfoText']                    = 'This section is for manually adding more data sources to the PDF report, important is that the table contains a user ID or email address for the map.';
            $_lang_admin['dsModuleAdd']                         = 'Add a data source';
            $_lang_admin['dsModuleAddName']                     = 'Module Name';
            $_lang_admin['dsModuleAddTabelle']                  = 'Database table';
            $_lang_admin['dsModuleAddBenutzer']                 = 'User';
            $_lang_admin['dsModuleAddBenutzerInfo']             = 'This user is only used for a test query, what and how many columns are displayed in the report.';
            $_lang_admin['dsModuleAddBenZuordnung']             = 'User to table';
            $_lang_admin['dsModuleAddBenZuordnungInfo']         = 'Which column contains the user id or e-mail address?';
            $_lang_admin['dsModuleAddShow']                     = 'Report view';
            $_lang_admin['dsModuleName']                        = 'Name on the report';
            $_lang_admin['dsModuleTabelle']                     = 'Database table';
            $_lang_admin['dsModuleSpalten']                     = 'Selected columns';
            $_lang_admin['dsModuleAll']                         = 'All existing modules';
            $_lang_admin['dsModuleEx']                          = 'External modules';
            $_lang_admin['dsModuleExInfo']                      = 'Complex modules can be reloaded on their own responsibility, it is important that we do not take any responsibility for errors or other problems that arise as a result.';
            $_lang_admin['dsModuleDatei']                       = 'File';

            // Admin: AV
            $_lang_admin['dsAV']                                = 'AV contract';

            // Admin: Cookie
            $_lang_admin['dsCookie']                            = 'Cookie';
            $_lang_admin['dsCookieInfo']                        = 'Cookie information is created with <a href="https://www.osano.com/" target="_blank">OSANO</a>, the most popular solution to the EU cookie law';
            $_lang_admin['dsCookieAllgemein']                   = 'General';
            $_lang_admin['dsCookieAktivieren']                  = 'Activate cookie';
            $_lang_admin['dsCookieAktivierenBesch']             = 'Should the cookie info be displayed on the non-registered and registered area?';
            $_lang_admin['dsCookieDenyAktivieren']              = 'Delete cookies';
            $_lang_admin['dsCookieDenyAktivierenBesch']         = 'If cookies are not allowed, should all existing cookies (except those relevant to the system) be deleted?';
            $_lang_admin['dsCookieSystem']                      = 'System Cookies';
            $_lang_admin['dsCookieSystemInfo']                  = 'One cookie per line';
            $_lang_admin['dsCookieSystemBesch']                 = 'These cookies are required by the system and must always be loaded.';
            $_lang_admin['dsCookieKonfigurationJS']             = 'JavaScripts that are loaded / unloaded as soon as the cookie info is confirmed';
            $_lang_admin['dsCookieKonfigurationJSAllow']        = 'Allow cookies';
            $_lang_admin['dsCookieKonfigurationJSAllowBesch']   = 'This script block is loaded if cookies are allowed';
            $_lang_admin['dsCookieKonfigurationJSDeny']         = 'Do not allow cookies';
            $_lang_admin['dsCookieKonfigurationJSDenyBesch']    = 'This script block is loaded if cookies are <strong>not</strong> allowed';
            $_lang_admin['dsCookieKonfigurationJSOpt']          = 'Optional';
            $_lang_admin['dsCookieKonfigurationJSOptBesch']     = 'Alternatively, your own scripts or areas can be activated or deactivated with the following code block.';
            $_lang_admin['dsCookieDesign']                      = 'Presentation';
            $_lang_admin['dsCookieKonfigurationJS']             = 'JavaScripts that are loaded / unloaded as soon as the cookie info is confirmed';
            $_lang_admin['dsCookieKonfigurationJSAllow']        = 'Loaded';
            $_lang_admin['dsCookieKonfigurationJSDeny']         = 'Unloaded';
            $_lang_admin['dsCookieDesignBG']                    = 'Info background';
            $_lang_admin['dsCookieDesignFG']                    = 'Info text color';
            $_lang_admin['dsCookieDesignLinkBG']                = 'Button background';
            $_lang_admin['dsCookieDesignLinkFG']                = 'Button text color';
            $_lang_admin['dsCookieTheme']                       = 'Template';
            $_lang_admin['dsCookiePosition']                    = 'Info box position';
            $_lang_admin['dsCookieType']                        = 'Involvement';
            $_lang_admin['dsCookieTypeInfo']                    = 'Just tell users that we use cookies';
            $_lang_admin['dsCookieTypeOptout']                  = 'Let users opt out of cookies (Opt-Out)';
            $_lang_admin['dsCookieTypeOptin']                   = 'Ask users to opt into cookies (Opt-In)';
            $_lang_admin['dsCookieKonfiguration']               = 'Configuration';
            $_lang_admin['dsCookieText']                        = 'Text';
            $_lang_admin['dsCookieClose']                       = 'Do not allow';
            $_lang_admin['dsCookieAllow']                       = 'Allow';
            $_lang_admin['dsCookieLinkText']                    = 'Link Text';
            $_lang_admin['dsCookieLink']                        = 'Link';
            $_lang_admin['dsCookieSpeichern']                   = 'Save';
            $_lang_admin['dsCookieTemplate']                    = 'Cookie template integration';
            $_lang_admin['dsMSGCookieKonf']                     = 'Configuration has been saved.';

            // Admin: Deref
            $_lang_admin['dsDerefTitle']                        = 'Forwarding';
            $_lang_admin['dsDerefStatus']                       = 'Status';
            $_lang_admin['dsDerefStatusBesch']                  = 'Activate information page for external links';
            $_lang_admin['dsDerefTemplate']                     = 'Template';
            $_lang_admin['dsMSGDeref']                          = 'Forwarding was saved';
            $_lang_admin['dsMSGDerefBackup']                    = 'Backup of the file "/templates/../nli/deref.tpl" was created';

            // Admin: Konfiguration
            $_lang_admin['dsConfig']                            = 'Settings';
            $_lang_admin['dsAllgemein']                         = 'General';
            $_lang_admin['dsReportKosten']                      = 'Report costs';
            $_lang_admin['dsGesetzestexte']                     = 'Legal texts';
            $_lang_admin['dsPDFOptionen']                       = 'PDF options';
            $_lang_admin['dsPDFOptionenInfo']                   = 'This displays the following information in the PDF report.';
            $_lang_admin['dsShowImpressum']                     = 'Imprint';
            $_lang_admin['dsShowImpressumBesch']                = 'Show data protection compliant imprint (with the information from the "General" tab)';
            $_lang_admin['dsAutoPDFGen']                        = 'Generate automatically';
            $_lang_admin['dsAutoPDFGenBesch']                   = 'Automatically share PDFs (without administrator approval)';
            $_lang_admin['dsPDFMails']                          = 'E-Mails';
            $_lang_admin['dsPDFKalender']                       = 'Calendar';
            $_lang_admin['dsPDFKontakte']                       = 'Contacts';
            $_lang_admin['dsPDFNotizen']                        = 'Notes';
            $_lang_admin['dsPDFAufgaben']                       = 'Tasks';
            $_lang_admin['dsPDFDaten']                          = 'Files';
            $_lang_admin['dsPDFBest']                           = 'Orders';
            $_lang_admin['dsSystemLink']                        = 'To the texts';
            $_lang_admin['dsFirstLogs']                         = 'Last system logs';
            $_lang_admin['dsLogsInfo']                          = 'This setting must be made on the web server and is purely informative for the user.';
            $_lang_admin['dsLogsWebAcc']                        = 'Webserver access logs';
            $_lang_admin['dsLogsWebErr']                        = 'Webserver error logs';
            $_lang_admin['dsLogsB1GMail']                       = 'B1GMail Logs';
            $_lang_admin['dsLogsB1GMailServer']                 = 'B1GMailServer Logs';
            $_lang_admin['dsLogArchive']                        = 'Log archive';
            $_lang_admin['dsLastLog']                           = 'Last Log entry';
            $_lang_admin['dsInhalt']                            = 'Contains';
            $_lang_admin['dsAktivieren']                        = 'Activate';
            $_lang_admin['dsAnzeigen']                          = 'Show';
            $_lang_admin['dsStatus']                            = 'Status';
            $_lang_admin['dsArchiv']                            = 'Archive';
            $_lang_admin['dsCron']                              = 'Delete automatically';
            $_lang_admin['dsZeit']                              = 'Derivative';

            // Admin: Pflicht
            $_lang_admin['dsPflicht']                           = 'Required fields';
            $_lang_admin['dsPflichtBeshr']                      = 'With this configuration, the globally defined mandatory fields per group can be overridden. If nothing is selected, the Global option is used.';
            $_lang_admin['dsPflichtNicht']                      = 'Not selected';

            // Admin: Hilfe
            $_lang_admin['dsHilfe']                             = 'Help';
            $_lang_admin['dsSupport']                           = 'Help & Support';
            $_lang_admin['dsLegende']                           = 'Legend';
            $_lang_admin['dsStatusOFF']                         = 'Report generation completed';
            $_lang_admin['dsStatusONG']                         = 'Report generation planned';
            $_lang_admin['dsStatusONA']                         = 'Report generation accept';
            $_lang_admin['dsStatusON']                          = 'Report generation running';
            $_lang_admin['dsTCPDFOn']                           = 'is available';
            $_lang_admin['dsTCPDFOff']                          = 'is not present';
            $_lang_admin['dsPlatzhalterVerz']                   = 'Wildcard directory';
            $_lang_admin['dsPlatzhalter']                       = 'Wildcard';
            $_lang_admin['dsPlatzhalterBesch']                  = 'Description';
            $_lang_admin['dsPlatzhalterInhalt']                 = 'Content';
            $_lang_admin['dsVorlageLaden']                      = 'Template documents loaded';
            $_lang_admin['dsVorlageLadenBeschr']                = 'With this function text templates can be loaded into the database.';
            $_lang_admin['dsVorlageWarnung']                    = 'Existing entries will be canceled!';
            $_lang_admin['dsVorlageImport']                     = 'Document templates have been imported, please adjust the required placeholders in the texts.';
        }

        // convert charset
        global $currentCharset;
        $arrays = array('admin', 'client', 'user', 'custom');
        foreach($arrays as $array)
        {
            $destArray = sprintf('lang_%s', $array);
            $srcArray  = '_' . $destArray;

            if(!isset($$srcArray))
                continue;

            foreach($$srcArray as $key=>$val) {
                if(function_exists('CharsetDecode') && !in_array(strtolower($currentCharset), array('iso-8859-1', 'iso-8859-15')))
                    $val = CharsetDecode($val, 'iso-8859-15');
                ${$destArray}[$key] = $val;
            }
        }
    }

    function Install() {
        global $db, $bm_prefs;

        $languages = GetAvailableLanguages();

        // Sprachvariabeln löschen
        $db->Query('DELETE FROM {pre}texts WHERE `key`=?', 'DatenschutzPDFBegruessung');

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_cookie` (
                      `ID` int(11) NOT NULL,
                      `StatusNLI` enum('yes','no') NOT NULL DEFAULT 'no',
                      `StatusLI` enum('yes','no') NOT NULL DEFAULT 'no',
                      `SystemCookie` text NOT NULL,
                      `JavaScriptAllow` text NOT NULL,
                      `JavaScriptDeny` text NOT NULL,
                      `JavaScriptDenyCookie` enum('yes','no') NOT NULL DEFAULT 'yes',
                      `DesignBG` varchar(10) NOT NULL,
                      `DesignFG` varchar(10) NOT NULL,
                      `DesignLinkBG` varchar(10) NOT NULL,
                      `DesignLinkFG` varchar(10) NOT NULL,
                      `DesignTheme` varchar(10) NOT NULL,
                      `DesignPosition` varchar(20) NOT NULL,
                      `DesignStatic` varchar(10) NOT NULL,
                      `DesignType` varchar(10) NOT NULL,
                      `CookieText` text NOT NULL,
                      `CookieClose` varchar(100) NOT NULL,
                      `CookieAllow` varchar(100) NOT NULL,
                      `CookieLinkText` varchar(100) NOT NULL,
                      `CookieLink` varchar(100) NOT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        // Update 1.1.2 Cookie aktivieren
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_cookie` LIKE ?', 'Status');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `Status` enum('yes','no') NOT NULL DEFAULT 'no' AFTER ID;");
        }
        $res->Free();

        // Update 1.1.2 Cookie Design
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_cookie` LIKE ?', 'DesignLinkBG');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` DROP IF EXISTS `CookieName`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` CHANGE COLUMN `DesignLink` DesignLinkBG varchar(10)");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` CHANGE COLUMN `DesignAlign` DesignLinkFG varchar(10)");
            $db->Query('UPDATE {pre}mod_ds_cookie SET `DesignLinkBG`=?, `DesignLinkFG`=? WHERE `ID`=?',
                'f1d600',
                '000000',
                0);
        }
        $res->Free();

        // Update 1.1.8
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_cookie` LIKE ?', 'JavaScriptAllow');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` DROP IF EXISTS `JavaScript`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `SystemCookie` text DEFAULT NULL AFTER `Status`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `JavaScriptAllow` text DEFAULT NULL AFTER `SystemCookie`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `JavaScriptDeny` text DEFAULT NULL AFTER `JavaScriptAllow`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `JavaScriptDenyCookie` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER `JavaScriptDeny`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `DesignTheme` varchar(10) DEFAULT NULL AFTER `DesignLinkFG`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `DesignPosition` varchar(20) DEFAULT NULL AFTER `DesignTheme`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `DesignStatic` varchar(10) DEFAULT NULL AFTER `DesignPosition`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `DesignType` varchar(10) DEFAULT NULL AFTER `DesignStatic`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `CookieAllow` varchar(100) DEFAULT NULL AFTER `CookieClose`;");
            $db->Query('UPDATE {pre}mod_ds_cookie SET `SystemCookie`=?,`DesignTheme`=?, `DesignPosition`=?, `DesignStatic`=?, `DesignType`=?, `CookieText`=?, `CookieClose`=?, `CookieAllow`=?, `CookieLinkText`=? WHERE `ID`=?',
                'cookieconsent_status\r\nbm_language\r\nhSepWidth1\r\nhSepWidth1addr\r\nvSepHeight1',
                'edgeless',
                'bottom',
                'false',
                'opt-in',
                'a:2:{s:7:\"deutsch\";s:77:\"Wir verwenden Cookies, um Ihnen den bestmöglichen Service zu gewährleisten.\";s:7:\"english\";s:58:\"We use cookies to guarantee you the best possible service.\";}',
                'a:2:{s:7:\"deutsch\";s:22:\"Cookies nicht erlauben\";s:7:\"english\";s:20:\"Do not allow cookies\";}',
                'a:2:{s:7:\"deutsch\";s:16:\"Cookies erlauben\";s:7:\"english\";s:13:\"Allow cookies\";}',
                'a:2:{s:7:\"deutsch\";s:21:\"Weitere Informationen\";s:7:\"english\";s:22:\"Additional Information\";}',
                0);
        }
        $res->Free();

        // Update 1.1.9
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_cookie` LIKE ?', 'StatusNLI');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {

            // Alter Status
            $rescookie = $db->Query('SELECT `Status` FROM {pre}mod_ds_cookie WHERE `ID` = ?', 0);
            while ($cookierow = $rescookie->FetchArray(MYSQLI_ASSOC)) {
                $CookieStatus = $cookierow['Status'];
            }
            $rescookie->Free();

            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` DROP IF EXISTS `Status`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `StatusNLI` enum('yes','no') NOT NULL DEFAULT 'no';");
            $db->Query("ALTER TABLE `{pre}mod_ds_cookie` ADD `StatusLI` enum('yes','no') NOT NULL DEFAULT 'no';");
            $db->Query('UPDATE {pre}mod_ds_cookie SET `StatusNLI`=?,`StatusLI`=? WHERE `ID`=?', $CookieStatus, $CookieStatus, 0);
        }
        $res->Free();

        $db->Query('INSERT IGNORE INTO `{pre}mod_ds_cookie` (`ID`, `DesignBG`, `DesignFG`, `DesignLinkBG`, `DesignLinkFG`, `DesignTheme`, `DesignPosition`, `DesignStatic`, `DesignType`, `CookieText`, `CookieClose`, `CookieAllow`, `CookieLinkText`, `CookieLink`) VALUES
                (0,
                \'505B72\', 
                \'FFFFFF\', 
                \'F1D600\', 
                \'000000\', 
                \'edgeless\',
                \'\',
                \'false\',
                \'opt-in\',
                \'a:2:{s:7:\"deutsch\";s:77:\"Wir verwenden Cookies, um Ihnen den bestmöglichen Service zu gewährleisten.\";s:7:\"english\";s:58:\"We use cookies to guarantee you the best possible service.\";}\', 
                \'a:2:{s:7:\"deutsch\";s:22:\"Cookies nicht erlauben\";s:7:\"english\";s:20:\"Do not allow cookies\";}\', 
                \'a:2:{s:7:\"deutsch\";s:16:\"Cookies erlauben\";s:7:\"english\";s:13:\"Allow cookies\";}\',
                \'a:2:{s:7:\"deutsch\";s:21:\"Weitere Informationen\";s:7:\"english\";s:22:\"Additional Information\";}\', 
                \'/index.php?action=Datenschutz\');');

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_deref` (
                      `ID` int(11) NOT NULL,
                      `Status` enum('yes','no') NOT NULL DEFAULT 'no',
                      `Template` text NOT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query('INSERT IGNORE INTO `{pre}mod_ds_deref` (`ID`, `Template`) VALUES
                (0,
                \'<!DOCTYPE html>\r\n<html lang=\"{lng p=\"langCode\"}\">\r\n<head>\r\n    <title>{$service_title} - {lng p=\"dsDerefTitle\"}</title>\r\n\r\n    <meta http-equiv=\"content-type\" content=\"text/html; charset={$charset}\" />\r\n\r\n    <link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\" />\r\n    <style>\r\n        {literal}\r\n        body { text-align: center; padding: 20px; }\r\n        @media (min-width: 768px){\r\n            body{ padding-top: 150px; }\r\n        }\r\n        h1 { font-size: 50px; }\r\n        body { font: 20px Helvetica, sans-serif; color: #333; }\r\n        article { display: block; text-align: left; max-width: 650px; margin: 0 auto; }\r\n        a { color: #dc8100; text-decoration: none; }\r\n        a:hover { color: #333; text-decoration: none; }\r\n        {/literal}\r\n    </style>\r\n</head>\r\n<body>\r\n<article>\r\n    <h1>{lng p=\"dsDerefTitle\"}</h1>\r\n    <div>\r\n        <p>{lng p=\"dsDerefRedirect\"}<br /><a href=\"{$url}\" rel=\"noreferrer nofollow\">{text value=$url cut=100}</a></p>\r\n        <p>{lng p=\"dsDerefBack\"}</p>\r\n    </div>\r\n</article>\r\n</body>\r\n</html>\');');

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_conf` (
                      `ID` int(11) NOT NULL,
                      `AutoPDFGen` enum('yes','no') NOT NULL DEFAULT 'no',
                      `ReportKosten` varchar(100) NOT NULL,
                      `PDFMails` text NOT NULL,
                      `PDFKalender` text NOT NULL,
                      `PDFKontakte` text NOT NULL,
                      `PDFNotizen` text NOT NULL,
                      `PDFAufgaben` text NOT NULL,
                      `PDFDaten` text NOT NULL,
                      `PDFBest` text NOT NULL,
                      `PDFGesetzestexte` text NOT NULL,
                      `LogWebAccStatus` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogWebAccInter` varchar(11) NOT NULL,
                      `LogWebAccInhalt` text,
                      `LogWebErrStatus` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogWebErrInter` varchar(11) NOT NULL,
                      `LogWebErrInhalt` text,
                      `LogB1GCron` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogB1GStatus` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogB1GArchive` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogB1GInter` varchar(11) NOT NULL,
                      `LogB1GInhalt` text,
                      `LogB1GSStatus` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogB1GSInhalt` text,
                      `LogArchiveCron` enum('yes','no') NOT NULL DEFAULT 'no',
                      `LogArchiveInter` varchar(11) NOT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        // Update 1.1.1 Report Kosten
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'ReportKosten');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `ReportKosten` VARCHAR(100) AFTER DatenschutzMail;");
            $db->Query('UPDATE {pre}mod_ds_conf SET `ReportKosten`=? WHERE `ID`=?',
                '25 CHF',
                0);
        }
        $res->Free();

        // Update 1.1.2 RZ Loc und Backup entfernt
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'BackupLoc');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `BackupLoc`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `BackupVersionen`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `RZLoc`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `RZLink`;");
        }
        $res->Free();

        // Update 1.1.3 PDF Optionen in PDF Aktivieren
        //$tablename = NULL;
        //$res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'Verarbeitungszwecke');
        //$tablename = $res->FetchArray(MYSQLI_NUM);
        //if (!is_array($tablename)) {
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `Dateneinsicht`;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `Datennutzung`;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `Verarbeitungszwecke` text NOT NULL AFTER Logo;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `Datenkategorien` text NOT NULL AFTER Verarbeitungszwecke;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `Datenempfaenger` text NOT NULL AFTER Datenkategorien;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFMails` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER Datenempfaenger;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFKalender` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER PDFMails;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFKontakte` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER PDFKalender;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFNotizen` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER PDFKontakte;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFAufgaben` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER PDFNotizen;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFDaten` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER PDFAufgaben;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFBest` enum('yes','no') NOT NULL DEFAULT 'yes' AFTER PDFDaten;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFVerKatEmpf` enum('yes','no') NOT NULL DEFAULT 'no' AFTER PDFBest;");
        //    $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `PDFGesetzestexte` enum('yes','no') NOT NULL DEFAULT 'no' AFTER PDFVerKatEmpf;");
        //}
        //$res->Free();

        // Update 1.1.8 PDF Texte
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'Verarbeitungszwecke');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `Verarbeitungszwecke`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `Datenkategorien`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `Datenempfaenger`;");
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `PDFVerKatEmpf`;");
        }
        $res->Free();

        // Update 1.1.5 automatische PDF generierung
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'AutoPDFGen');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `AutoPDFGen` enum('yes','no') NOT NULL DEFAULT 'no' AFTER ID;");
        }
        $res->Free();

        // Update 1.2.0 Impressum
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'ShowImpressum');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}mod_ds_conf` ADD `ShowImpressum` enum('yes','no') NOT NULL DEFAULT 'no' AFTER ID;");
        }
        $res->Free();

        $db->Query('INSERT IGNORE INTO `{pre}mod_ds_conf` (`ID`, `ReportKosten`, `LogWebAccStatus`, `LogWebAccInter`, `LogWebAccInhalt`, `LogWebErrStatus`, `LogWebErrInter`, `LogWebErrInhalt`, `LogB1GCron`, `LogB1GStatus`, `LogB1GArchive`, `LogB1GInter`, `LogB1GInhalt`, `LogB1GSStatus`, `LogB1GSInhalt`, `LogArchiveCron`, `LogArchiveInter`) VALUES
                      (\'0\', \'25 CHF\', \'yes\', \'30\', \'Alle Zugriffe auf die Webseiten.\', \'yes\', \'30\', \'Fehlermeldungen von PHP, CGI und vom Webserver selbst.\', \'yes\', \'yes\', \'no\', \'30\', \'Webmail Informationen und Fehlermeldungen.\', \'yes\', \'Zeitpunkt sowie Absender und Empf&auml;nger von versendeten E-Mails.\', \'yes\', \'30\');');

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_conf_pfs` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Sortierung` int(11) NOT NULL,
                      `Anzeigen` varchar(100) DEFAULT NULL,
                      `PFID` varchar(200) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_general` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Name` varchar(100) NOT NULL,
                      `Value` text NOT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $db->Query('INSERT IGNORE INTO `{pre}mod_ds_general` (`ID`, `Name`, `Value`) VALUES
                    (1,  \'Status\', \'no\'),
                    (2,  \'Logo\', \'\'),
                    (3,  \'Name\', \''. $bm_prefs['titel'] .'\'),
                    (4,  \'Optional\', \'\'),
                    (5,  \'Street\', \'\'),
                    (6,  \'StreetNr\', \'\'),
                    (7,  \'PLZ\', \'\'),
                    (8,  \'Place\', \'\'),
                    (9,  \'Country\', \'\'),
                    (10, \'ContactEMail\', \''. $bm_prefs['contactform_to'] .'\'),
                    (11, \'PrivacyEMail\', \''. $bm_prefs['contactform_to'] .'\'),
                    (12, \'Phone\', \'\'),
                    (13, \'Fax\', \'\'),
                    (14, \'ContactForm\', \''. $bm_prefs['selfurl'] .'index.php?action=imprint\'),
                    (15, \'MoreContactOptions\', \'\'),
                    (16, \'AuthorizedToRepresent\', \'\'),
                    (17, \'UstID\', \'\'),
                    (18, \'WidNr\', \'\'),
                    (19, \'BusinessArea\', \'\'),
                    (20, \'AGBUrl\', \'\'),
                    (21, \'RegistryCourt\', \'\'),
                    (22, \'RegistryCourtNr\', \'\'),
                    (23, \'ConsumerDisputeResolution\', \'\'),
                    (24, \'SocialMedia\', \'no\'),
                    (25, \'CopyrightNotices\', \'no\'),
                    (26, \'PhotoCredits\', \'no\');');

        // Update 1.2.0 General
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}mod_ds_conf` LIKE ?', 'DatenschutzMail');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (is_array($tablename)) {
            $rescon = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID =? LIMIT 1', 0);
            while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $row['DatenschutzMail'], 'PrivacyEMail');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $row['Logo'], 'Logo');
                $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `DatenschutzMail`;");
                $db->Query("ALTER TABLE `{pre}mod_ds_conf` DROP IF EXISTS `Logo`;");
            }
        }
        $res->Free();

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_pflicht` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `GruppenID` int(11) NOT NULL,
                      `FeldName` varchar(100) DEFAULT NULL,
                      `Status` varchar(20) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_nli` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Sortierung` int(11) NOT NULL,
                      `Lang` varchar(10) DEFAULT NULL,
                      `Title` varchar(200) DEFAULT NULL,
                      `Content` text,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_av` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Lang` varchar(10) DEFAULT NULL,
                      `Title` varchar(200) DEFAULT NULL,
                      `Content` text,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_av_user` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `UserID` varchar(11) DEFAULT NULL,
                      `GroupID` varchar(11) DEFAULT NULL,
                      `DocDate` datetime DEFAULT NULL,
                      `Dokument` longblob DEFAULT NULL,
                      `DokumentSize` varchar(11) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_impressum` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Lang` varchar(10) DEFAULT NULL,
                      `Text` varchar(200) DEFAULT NULL,
                      `Content` text,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        $i = 1;
        foreach ($languages as $langID => $lang) {
            $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`ID`, `Lang`, `Text`) VALUES (\''. $i++ .'\', \''. $langID .'\', \'ImpressumContent\');');
            $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`ID`, `Lang`, `Text`) VALUES (\''. $i++ .'\', \''. $langID .'\', \'ConsumerDisputeResolution\');');
            $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`ID`, `Lang`, `Text`) VALUES (\''. $i++ .'\', \''. $langID .'\', \'SocialMedia\');');
            $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`ID`, `Lang`, `Text`) VALUES (\''. $i++ .'\', \''. $langID .'\', \'CopyrightNotices\');');
            $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`ID`, `Lang`, `Text`) VALUES (\''. $i++ .'\', \''. $langID .'\', \'PhotoCredits\');');
        }

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_report` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Lang` varchar(10) DEFAULT NULL,
                      `UserID` int(11) DEFAULT NULL,
                      `Status` int(11) DEFAULT NULL,
                      `JobDate` datetime DEFAULT NULL,
                      `JobUpdate` datetime DEFAULT NULL,
                      `Optionen` varchar(1000) DEFAULT NULL,
                      `Dokument` longblob DEFAULT NULL,
                      `DokumentSize` varchar(11) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_module` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Name` varchar(100) DEFAULT NULL,
                      `DBTabelle` varchar(100) DEFAULT NULL,
                      `DBSearchTabelle` varchar(100) DEFAULT NULL,
                      `DBSearchType` varchar(10) DEFAULT NULL,
                      `Spalten` varchar(1000) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");

        // Update 1.1.4 Pflichtfelder
        $tablename = NULL;
        $res = $db->Query('SHOW COLUMNS FROM `{pre}prefs` LIKE ?', 'f_vorname');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (!is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}prefs` ADD `f_vorname` enum('p', 'v', 'n') NOT NULL DEFAULT 'p' AFTER titel;");
            $db->Query("ALTER TABLE `{pre}prefs` ADD `f_nachname` enum('p', 'v', 'n') NOT NULL DEFAULT 'p' AFTER f_vorname;");
        }
        $res->Free();

        // Update Info
        $UpdateInfo = array(
            'internalName'  => $this->update_name,
            'LicNr'         => B1GMAIL_LICNR,
            'LicDom'        => B1GMAIL_LICDOMAIN,
            'DlDate'        => B1GMAIL_DLDATE,
            'SystemName'    => $bm_prefs['titel'],
            'SystemUrl'     => $bm_prefs['selfurl'],
            'MTAHost'       => $bm_prefs['b1gmta_host'],
            'IP'            => $_SERVER['SERVER_ADDR'],
            'PluginVersion' => $this->version,
            'SystemVersion' => $this->designedfor,
            'PHPVersion'    => phpversion()
        );

        $ch = curl_init($this->update_url .'index.php?action=updateVersion');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($UpdateInfo));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Content-Length: ' . strlen(json_encode($UpdateInfo))));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        PutLog($this->name. ' Version ' . $this->version . ' was successfull installed!', PRIO_PLUGIN, __FILE__, __LINE__);
        return(true);
    }

    function Uninstall() {
        global $db, $bm_prefs;

        // Daten / Ordner löschen
        $res = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID=?', 0);
        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $Logo = $this->dsGeneral('Logo');
            if (file_exists('../plugins/templates/images/'. $Logo)) {
                unlink('../plugins/templates/images/' . $Logo);
            }
        }

        // Deref Template
        if (file_exists($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl.bak')) {
            unlink($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl');
            rename($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl.bak', $bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl');
        }

        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_general');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_av');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_av_user');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_report');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_module');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_cookie');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_deref');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_conf');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_conf_pfs');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_pflicht');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_nli');
        $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_impressum');

        $res = $db->Query('SHOW COLUMNS FROM `{pre}prefs` LIKE ?', 'f_vorname');
        $tablename = $res->FetchArray(MYSQLI_NUM);
        if (is_array($tablename)) {
            $db->Query("ALTER TABLE `{pre}prefs` DROP IF EXISTS `f_vorname`;");
            $db->Query("ALTER TABLE `{pre}prefs` DROP IF EXISTS `f_nachname`;");
        }
        $res->Free();

        PutLog($this->name. ' Version ' . $this->version . ' was successfull removed!', PRIO_PLUGIN, __FILE__, __LINE__);
        return(true);
    }

    function checkPHPClassMD5Hases() {
        global $bm_prefs;

        $phpHashes      = NULL;
        $phpClassPath   = $this->phpPluginClassesPath;
        $phpClassHash   = $this->phpPluginClassesHashes;

        $phpClasses     = scandir($bm_prefs['selffolder'] . $phpClassPath);

        foreach ($phpClasses as $pc) {
            $hash = "";//md5_file($bm_prefs['selffolder'] . $phpClassPath . $pc);
            if (pathinfo($pc, PATHINFO_EXTENSION) == 'php' && in_array($hash, $phpClassHash)) {
                $phpHashes[] = array(
                    'Status' => 'ok',
                    'File'  => $pc,
                    'Hash'  => $hash
                );
            } else if (pathinfo($pc, PATHINFO_EXTENSION) == 'php') {
                $phpHashes[] = array(
                    'Status' => 'error',
                    'File'  => $pc,
                    'Hash'  => $hash
                );
            }
        }

        if (is_array($phpHashes)) {
            return $phpHashes;
        }
    }

    /**
     * Admin Hinweis
     */
    function getNotices() {
        global $db, $lang_admin;

        $notices = array();

        // Reports
        $res = $db->Query('SELECT COUNT(*) FROM {pre}mod_ds_report WHERE Status =?',
            '1');
        list($dsAnzReports) = $res->FetchArray(MYSQLI_NUM);
        $res->Free();

        if($dsAnzReports > 0) {
            $notices[] = array('type' => 'info',
                'text' => sprintf($lang_admin['dsNeueReports'], $dsAnzReports),
                'link' => $this->_adminLink() . '&do=reports&');
        }

        // PHP Classes
        $md5files = NULL;
        $phpHashes = $this->checkPHPClassMD5Hases();
        if (is_array($phpHashes)) {
            foreach ($phpHashes as $hash) {
                if ($hash['Status'] == 'error') {
                    $md5files .= "<li>". $hash['File'] ."</li>";
                }
            }
        }
        if (isset($md5files)) {
            $notices[] = array('type' => 'warning',
                'text' => $lang_admin['dsInfoPHPClassHash'] ."<ul>". $md5files ."</ul>",
                'link' => $this->_adminLink() . '&do=hilfe&');
        }

        // Plugin Update
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->update_url .'index.php?action=getLatestVersion&internalName='. $this->update_name .'&b1gMailVersion='. $this->designedfor);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        $queryResult = unserialize(curl_exec($ch));
        curl_close($ch);
        if($this->IsVersionNewer($queryResult['latestVersion'], $this->version)) {
            $notices[] = array(
                'type' => 'update',
                'text' => sprintf($lang_admin['dsInfoUpdate'], $queryResult['latestVersion']));
        }

        // TCPDF Warnung
        if (!@file_exists('../plugins/php/tcpdf/tcpdf.php')) {
            $notices[] = array('type' => 'warning',
                'text' => $lang_admin['dsTCPDFnot'],
                'link' => $this->_adminLink() . '&do=hilfe&');
        }

        // PHP Warnung
        $phpmemory      = ini_get('memory_limit');
        $phpmemtype     = substr($phpmemory, -1, 1);

        if ($phpmemtype == 'G') {
            $phpmemory = substr($phpmemory, 0, -1);
            $phpmemory = $phpmemory * 1024;
        } else {
            $phpmemory = substr($phpmemory, 0, -1);
            $phpmemory = $phpmemory;
        }
        if($phpmemory < 256) {
            $notices[] = array('type' => 'warning',
                'text' => sprintf($lang_admin['dsPHPMemory'], $phpmemory),
                'link' => '');
        }

        return($notices);
    }


    /**
     * Functions
     */
    function _placeholderReplace($content, $value=NULL) {
        global $db, $userRow;

        if (isset($value)) {
            $showdatum = date('d.m.Y', strtotime($value));
        } else {
            $showdatum = date('d.m.Y', time());
        }

        if ($this->_BMModuleExists('KidsMailAdmin') == 1) {
            $kundendaten    = $userRow['vorname'] .' '. $userRow['nachname'] .'<br />'. $userRow['strasse'] .' '. $userRow['hnr'] .'<br />'. $userRow['plz'] .' '. $userRow['ort'];
            $resamail = $db->Query("SELECT `altmail` FROM {pre}users WHERE id = ?", $userRow['id']);
            while($row = $resamail->FetchArray(MYSQLI_ASSOC)) {
                $kmamail = $row['altmail'];
            }
            $resamail->Free();

            if (isset($kmamail)) {
                $reskma = $db->Query("SELECT * FROM {pre}mod_kidsmailadmin_accounts WHERE EMail = ?", $kmamail);
                while ($row = $reskma->FetchArray(MYSQLI_ASSOC)) {
                    $kundendaten = '<strong>'. $row['Vorname'] .' '. $row['Nachname'] .'</strong><br /><small>'. $userRow['vorname'] .' '. $userRow['nachname'] .' ('. $userRow['email'] .')</small><br />'. $row['Strasse'] .' '. $row['SNummer'] .'<br />'. $row['PLZ'] .' '. $row['Ort'];
                }
                $reskma->Free();
            }
        } else {
            $kundendaten    = $userRow['vorname'] .' '. $userRow['nachname'] .'<br />'. $userRow['strasse'] .' '. $userRow['hnr'] .'<br />'. $userRow['plz'] .' '. $userRow['ort'];
        }
        $content = preg_replace("/\[%kundendaten%]/",       $kundendaten, $content);
        $content = preg_replace("/\[%aktdatum%]/",          $showdatum, $content);

        $content = preg_replace("/\[%name%]/",              $this->dsGeneral('Name'), $content);
        $content = preg_replace("/\[%optional%]/",          $this->dsGeneral('Optional'), $content);
        $content = preg_replace("/\[%street%]/",            $this->dsGeneral('Street'), $content);
        $content = preg_replace("/\[%streetnr%]/",          $this->dsGeneral('StreetNr'), $content);
        $content = preg_replace("/\[%plz%]/",               $this->dsGeneral('PLZ'), $content);
        $content = preg_replace("/\[%place%]/",             $this->dsGeneral('Place'), $content);
        $content = preg_replace("/\[%country%]/",           $this->dsGeneral('Country'), $content);
        $content = preg_replace("/\[%contactmail%]/",       $this->dsGeneral('ContactEMail'), $content);
        $content = preg_replace("/\[%privacymail%]/",       $this->dsGeneral('PrivacyEMail'), $content);
        $content = preg_replace("/\[%phone%]/",             $this->dsGeneral('Phone'), $content);
        $content = preg_replace("/\[%fax%]/",               $this->dsGeneral('Fax'), $content);
        $content = preg_replace("/\[%contactform%]/",       $this->dsGeneral('ContactForm'), $content);
        $content = preg_replace("/\[%contactmore%]/",       $this->dsGeneral('MoreContactOptions'), $content);
        $content = preg_replace("/\[%authorized%]/",        $this->dsGeneral('AuthorizedToRepresent'), $content);
        $content = preg_replace("/\[%ustid%]/",             $this->dsGeneral('UstID'), $content);
        $content = preg_replace("/\[%widnr%]/",             $this->dsGeneral('WidNr'), $content);
        $content = preg_replace("/\[%businessarea%]/",      $this->dsGeneral('BusinessArea'), $content);
        $content = preg_replace("/\[%agburl%]/",            $this->dsGeneral('AGBUrl'), $content);
        $content = preg_replace("/\[%registrycourt%]/",     $this->dsGeneral('RegistryCourt'), $content);
        $content = preg_replace("/\[%registrycourtnr%]/",   $this->dsGeneral('RegistryCourtNr'), $content);

        return $content;
    }

    function cleanText($str){
        $str = trim($str);
        $str = str_replace("ö" ,"&ouml;", $str);
        $str = str_replace("ä" ,"&auml;", $str);
        $str = str_replace("ü" ,"&uuml;", $str);
        $str = str_replace("Ö","&Ouml;", $str);
        $str = str_replace("Ä","&Auml;", $str);
        $str = str_replace("Ü","&Uuml;", $str);

        return $str;

    }

    function UserInfos($id) {
        global $db;

        $res = $db->Query("SELECT `vorname`, `nachname` FROM {pre}users WHERE id = ?",
            $id);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            return $row['vorname'] .' '. $row['nachname'];
        }
        $res->Free();

    }

    function UserEMail($id) {
        global $db;

        $res = $db->Query("SELECT `email` FROM {pre}users WHERE id = ?",
            $id);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            return $row['email'];
        }
        $res->Free();

    }

    function UserLand($id) {
        global $db;

        $res = $db->Query("SELECT `land` FROM {pre}staaten WHERE id = ?",
            $id);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            return $row['land'];
        }
        $res->Free();

    }

    function timestampToDate($date) {
        if ($date > 0) {
            return date('d.m.Y H:i:s', $date);
        } else {
            return 'n/a';
        }
    }

    function isTimeStamp($timestamp) {

        if (date('Y-m-d H:i:s', strtotime($timestamp)) == $timestamp) {
            return 1;
        } else if (((string) (int) $timestamp === $timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX)) {
            return 2;
        }
    }

    function showFilesize($size, $decimalplaces = 0) {
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB');

        for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) {
            $size /= 1024;
        }
        return round($size, $decimalplaces).' '.$sizes[$i];
    }

    function InfoCount($userid, $where, $table) {
        global $db;

        $res = $db->Query('SELECT COUNT(*) FROM {pre}'. $table .' WHERE '. $where .'=?',
            $userid);
        list($anz) = $res->FetchArray(MYSQLI_NUM);
        $res->Free();
        return $anz;
    }

    function sendInfoMail($From, $To, $Sub, $msg) {
        SystemMail($From, trim($To), $Sub, 'DatenschutzMailText', $msg, 0);
    }

    function showPFInfo($id) {
        global $db;

        $pfinfo = NULL;

        $resdspf = $db->Query('SELECT * FROM {pre}mod_ds_conf_pfs WHERE PFID = ?', $id);
        while($row = $resdspf->FetchArray(MYSQLI_ASSOC)) {
            $pfinfo = array(
                "ID"            => $row['ID'],
                "Softierung"    => $row['Sortierung'],
                "Anzeigen"      => $row['Anzeigen'],
                "PFID"          => $row['PFID']
            );
        }
        $resdspf->Free();

        if (is_array($pfinfo)) {
            return $pfinfo;
        }
    }

    function showPFValue($userid, $pfid) {
        global $db;

        $profilefeld    = NULL;
        $pf             = NULL;

        $respf = $db->Query('SELECT `feld` FROM {pre}profilfelder WHERE id = ?', $pfid);
        while($row = $respf->FetchArray(MYSQLI_ASSOC)) {
            $profilefeld = $row['feld'];
        }
        $respf->Free();

        $resdspf = $db->Query('SELECT `profilfelder` FROM {pre}users WHERE id = ?', $userid);
        while($row = $resdspf->FetchArray(MYSQLI_ASSOC)) {
            $pf = unserialize($row['profilfelder']);
        }
        $resdspf->Free();

        if (isset($profilefeld) && isset($pf)) {
            $rpf = array(
                "feld"  => $profilefeld,
                "inhalt" => $pf[$pfid]
            );
            PutLog(serialize($rpf));
            return $rpf;
        }
    }

    function showPflichtOption($gid, $name=NULL) {
        global $db;
        if (isset($gid) && isset($name)) {
            $resopt = $db->Query('SELECT * FROM {pre}mod_ds_pflicht WHERE GruppenID = ? AND FeldName = ?',
                $gid,
                $name);
            while ($row = $resopt->FetchArray(MYSQLI_ASSOC)) {
                return $row['Status'];
            }
            $resopt->Free();
        }
    }

    function checkProfile() {
        global $userRow, $bm_prefs, $db;

        $invalidFields = array();

        if(!isset($userRow) || !is_array($userRow))
            return(true);

        // anrede
        if(isset($bm_prefs['f_anrede']) && $bm_prefs['f_anrede'] == 'p'
            && !in_array($userRow['anrede'], array('herr', 'frau'))) {
            $invalidFields[] = 'salutation';
        }

        // vorname
        if($bm_prefs['f_vorname'] == 'p'
            && strlen(trim($userRow['vorname'])) < 2) {
            $invalidFields[] = 'vorname';
        }

        // vorname
        if($bm_prefs['f_nachname'] == 'p'
            && strlen(trim($userRow['nachname'])) < 2) {
            $invalidFields[] = 'nachname';
        }

        // 'strasse' group
        if($bm_prefs['f_strasse'] == 'p') {
            if(strlen(trim($userRow['strasse'])) < 3) {
                $invalidFields[] = 'strasse';
            }

            if(strlen(trim($userRow['hnr'])) < 1) {
                $invalidFields[] = 'hnr';
            }

            if(strlen(trim($userRow['plz'])) < 3) {
                $invalidFields[] = 'plz';
            }

            if(strlen(trim($userRow['ort'])) < 3) {
                $invalidFields[] = 'ort';
            }
        }

        // mail2sms_nummer
        if($bm_prefs['f_mail2sms_nummer'] == 'p'
            && strlen(trim($userRow['mail2sms_nummer'])) < 6) {
            $invalidFields[] = 'mail2sms_nummer';
        }

        // telefon
        if($bm_prefs['f_telefon'] == 'p'
            && strlen(trim($userRow['tel'])) < 5) {
            $invalidFields[] = 'tel';
        }

        // fax
        if($bm_prefs['f_fax'] == 'p'
            && strlen(trim($userRow['fax'])) < 5) {
            $invalidFields[] = 'fax';
        }

        // altmail
        if($bm_prefs['f_alternativ'] == 'p'
            && strlen(trim($userRow['altmail'])) < 5) {
            $invalidFields[] = 'altmail';
        }

        // profile fields
        $profileFields = @unserialize($userRow['profilfelder']);

        if(!is_array($profileFields))
            $profileFields = array();
        $res = $db->Query('SELECT id,rule,typ FROM {pre}profilfelder WHERE pflicht=? AND typ IN ?',
            'yes',
            array(FIELD_RADIO, FIELD_TEXT, FIELD_DATE));
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            if($row['typ'] == FIELD_RADIO) {
                if(!isset($profileFields[$row['id']])) {
                    $invalidFields[] = 'field_' . $row['id'];
                }
            } else if($row['typ'] == FIELD_TEXT) {
                if(!isset($profileFields[$row['id']])
                    || !preg_match('/'.$row['rule'].'/', $profileFields[$row['id']])) {
                    $invalidFields[] = 'field_' . $row['id'];
                }
            } else if($row['typ'] == FIELD_DATE) {
                if(!isset($profileFields[$row['id']])
                    || empty($profileFields[$row['id']])) {
                    $invalidFields[] = 'field_' . $row['id'] . 'Day';
                    $invalidFields[] = 'field_' . $row['id'] . 'Month';
                    $invalidFields[] = 'field_' . $row['id'] . 'Year';
                }
            }
        }
        $res->Free();

        if (is_array($invalidFields)) {
            return ($invalidFields);
        } else {
            return array();
        }
    }

    function dsGeneral($name) {
        global $db;

        $res = $db->Query("SELECT `Value` FROM {pre}mod_ds_general WHERE Name = ?",
            $name);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            return $row['Value'];
        }
        $res->Free();
    }


    /**
     * CronJob
     */
    function OnCron() {
        global $db, $bm_prefs, $lang_user, $lang_custom;

        // TCPDF aktiv
        $TCPDFOn = FALSE;
        if(!class_exists('TCPDF') && @file_exists('plugins/php/tcpdf/tcpdf.php')) {
            $TCPDFOn = TRUE;
            include_once 'plugins/php/tcpdf/tcpdf.php';
        }

        // Logs bereinigen
        $tsakt      = date("Hi", time());
        $tsmin      = str_replace(':', '', date('H:i', strtotime($tsakt . ' -2 minutes')));
        $tsplus     = str_replace(':', '', date('H:i', strtotime($tsakt . ' +2 minutes')));

        if ('0000' > $tsmin && '0000' < $tsplus) {
            $res = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID =? LIMIT 1', 0);
            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                $CronB1G            = $row['LogB1GCron'];
                $CronB1GInter       = $row['LogB1GInter'];
                $CronB1GArchive     = $row['LogB1GArchive'];
                $CronArchive        = $row['LogArchiveCron'];
                $CronArchiveInter   = $row['LogArchiveInter'];
            }
            $res->Free();


            // B1GMail Logs bereinigen
            if ($CronB1G == 'yes') {
                $b1gdeldate = strtotime('-' . $CronB1GInter . ' day');
                if ($CronB1GArchive == 'yes') {
                    $Archive = TRUE;
                } else {
                    $Archive = FALSE;
                }

                if (!ArchiveLogs($b1gdeldate, $Archive)) {
                    DisplayError(0x15, 'Cannot create log archive file',
                        'Failed to create a new log archive file. The archiving procedure has been aborted.',
                        'See logs.',
                        __FILE__,
                        __LINE__);
                } else {
                    PutLog(sprintf('Admin <%s> deleted all log entries before %s (save archive copy: %d)',
                        'DatenschutzPlugin',
                        date('r', $b1gdeldate),
                        TRUE ? 1 : 0),
                        PRIO_NOTE,
                        __FILE__,
                        __LINE__);
                }
            }

            // Archive bereinigen
            if ($CronArchive == 'yes') {
                foreach (glob(B1GMAIL_REL . 'logs/*.bz2') as $file) {
                    if (is_file($file)) {
                        if (time() - filemtime($file) >= 60 * 60 * 24 * $CronArchiveInter) {
                            unlink($file);
                        }
                    }
                }
            }
        }

        // Alte Reports / AVs bereinigen
        $ModUsers = array();
        $res = $db->Query('SELECT `UserID` FROM {pre}mod_ds_av_user');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $ModUsers[] = $row['UserID'];
        }
        $res->Free();

        $res = $db->Query('SELECT `UserID` FROM {pre}mod_ds_report');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $ModUsers[] = $row['UserID'];
        }
        $res->Free();

        $sysUser = array();
        $res = $db->Query('SELECT `id` FROM {pre}users');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $sysUser[] = $row['id'];
        }
        $res->Free();

        foreach ($ModUsers as $us) {
            if (!in_array($us, $sysUser)) {
                $db->Query('DELETE FROM {pre}mod_ds_av_user WHERE UserID=' . $us);
                $db->Query('DELETE FROM {pre}mod_ds_report WHERE UserID=' . $us);
            }
        }

        // Dokuement erstellen
        $Job = NULL;
        $res = $db->Query('SELECT * FROM {pre}mod_ds_report WHERE Status =? ORDER BY ID DESC',
            2);
        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $Job[$row['ID']] = array(
                'ID'        => $row['ID'],
                'Lang'      => $row['Lang'],
                'JobDate'   => $row['JobDate'],
                'UserID'    => $row['UserID'],
                'Optionen'  => $row['Optionen']
            );
        }
        $res->Free();

        if (is_array($Job)) {
            foreach ($Job as $j) {

                if ($TCPDFOn) {
                    $db->Query('UPDATE {pre}mod_ds_report SET `Status`=?, JobUpdate =? WHERE `ID`=?',
                        '3',
                        date('Y-m-d H:i:s'),
                        $j['ID']);

                    $Optionen       = unserialize($j['Optionen']);
                    $pdfOrdner      = $bm_prefs['datafolder'];
                    $dokDatum       = date("d.m.Y H:i:s");
                    $pdfAuthor      = $bm_prefs['titel'];
                    $pdfBenutzer    = $this->UserInfos($j['UserID']);
                    $pdf            = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                    $PDFDSMail                  = $this->dsGeneral('PrivacyEMail');
                    $PDFLogo                    = $this->dsGeneral('Logo');

                    $res = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID=?', 0);
                    while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                        $PDFMails                   = $row['PDFMails'];
                        $PDFKalender                = $row['PDFKalender'];
                        $PDFKontakte                = $row['PDFKontakte'];
                        $PDFNotizen                 = $row['PDFNotizen'];
                        $PDFAufgaben                = $row['PDFAufgaben'];
                        $PDFDaten                   = $row['PDFDaten'];
                        $PDFBest                    = $row['PDFBest'];
                        $PDFGesetzestexte           = $row['PDFGesetzestexte'];
                    }
                    $res->Free();

                    // Logo
                    if ($PDFLogo !== '' && file_exists('plugins/templates/images/'. $PDFLogo)) {
                        $pdfLogo        = '<img src="plugins/templates/images/'. $PDFLogo .'" style="width: 250px; height:auto;">';
                    } else {
                        $pdfLogo        = '<img src="plugins/templates/images/datenschutz_logo.png" style="width: 250px; height:auto;">';
                    }
                    $pdfName        = md5($dokDatum) .'.pdf';



                    // Profilefelder
                    $PDFPF = NULL;
                    $respfs = $db->Query('SELECT * FROM {pre}mod_ds_conf_pfs ORDER BY Sortierung ASC');
                    while ($row = $respfs->FetchArray(MYSQLI_ASSOC)) {
                        $PDFPF[$row['Anzeigen']][] = $this->showPFValue($j['UserID'], $row['PFID']);
                    }
                    $respfs->Free();

                    // Erstellung des PDF Dokuments
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor($pdfAuthor);
                    $pdf->SetTitle('Datenschutz Report: '. $pdfBenutzer);
                    $pdf->SetSubject('Datenschutz Report: '. $pdfBenutzer);
                    $pdf->setPrintHeader(false);
                    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                    $pdf->AddPage();


                    // Allgemein: Profile
                    $res = $db->Query('SELECT * FROM {pre}users WHERE `id` = ?',
                        $j['UserID']);
                    while($row = $res->FetchArray(MYSQLI_ASSOC)) {
                        $UserGruppe      = $row['gruppe'];
                        $MailSpace       = $this->showFilesize($row['mailspace_used'], 2);
                        $MailReceived    = $row['received_mails'];
                        $MailSent        = $row['sent_mails'];
                        $DiskSpace       = $this->showFilesize($row['diskspace_used'], 2);
                        $RegDate         = $this->timestampToDate($row['reg_date']);
                        $RegIP           = $row['reg_ip'];
                        $LastIP          = $row['ip'];
                        $WebLogin        = $this->timestampToDate($row['lastlogin']);
                        $POPLogin        = $this->timestampToDate($row['last_pop3']);
                        $IMAPLogin       = $this->timestampToDate($row['last_imap']);
                        $SMTPLogin       = $this->timestampToDate($row['last_smtp']);
                        $MailSend        = $this->timestampToDate($row['last_send']);

                        // Header
                        $html = '<table cellpadding="0" cellspacing="0" style="width: 100%; margin: 0px;">
                                <tr>
                                    <td style="padding: 0px 0px 0px 30px;">
                                        <small style="font-size: 10px;">'. $lang_user['dsPDFErstell'] .': '. $dokDatum .'</small>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <small style="margin-bottom: 10px; font-size: 10px; color: #cccccc;">'. $this->dsGeneral('Name') .' - '. $this->dsGeneral('Street') .' '. $this->dsGeneral('StreetNr') .' - '. $this->dsGeneral('PLZ') .' '. $this->dsGeneral('Place') .'</small><br />
                                        '. $row['vorname'] .' '. $row['nachname'] .'<br />
                                        '. $row['strasse'] .' '. $row['hnr'] .'<br />
                                        '. $row['plz'] .' '. $row['ort'] .'
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <p style="font-size:1.3em; font-weight: bold;">'. $lang_user['dsPDFTitel'] .'</p>
                                    </td>
                                    <td style="text-align: right; color: #999999; font-size: 9px;">
                                        '.nl2br(trim($pdfLogo)).'<br />
                                        <strong>'. $this->dsGeneral('Name') .'</strong><br />
                                        '. $this->dsGeneral('Street') .' '. $this->dsGeneral('StreetNr') .'<br />
                                        '. $this->dsGeneral('PLZ') .' '. $this->dsGeneral('Place') .', '. $this->dsGeneral('Country') .'<br /><br />
                                        Tel.: '. $this->dsGeneral('Phone') .'<br />
                                        E-Mail: '. $this->dsGeneral('PrivacyEMail') .'
                                    </td>
                                </tr>
                            </table><br /><br />';

                        if ($row['anrede'] == 'herr') {
                            $anrede = $lang_user['dsPDFAnredeHerr'] .' '. $row['vorname'] .' '. $row['nachname'] .',';
                        } else if ($row['anrede'] == 'frau') {
                            $anrede = $lang_user['dsPDFAnredeFrau'] .' '. $row['vorname'] .' '. $row['nachname'] .',';
                        } else {
                            $anrede = $lang_user['dsPDFAnrede'] .',';
                        }
                        $PDFBegruessung = $this->_placeholderReplace(nl2br($lang_custom['DatenschutzPDFBegruessung']), $j['JobUpdate']);
                        $PDFBegruessung = preg_replace("/\[%anrede%]/", $anrede, $PDFBegruessung);

                        $html .= '<div style="font-size: 12px;">'. $PDFBegruessung .'</div>';

                        $html = html_entity_decode($html);
                        $pdf->writeHTML($html, true, false, true, false, '');

                        // Allgemein
                        $htmlAllgemein1 .= '<h3>'. $lang_user['dsPDFAllgemein'] .'</h3>';
                        $htmlAllgemein1 .= '<table style="width: 100%;"><tr style="vertical-align: top;"><td style="width: 50%;">';

                        $htmlAllgemein1 .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlAllgemein1 .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">'. $lang_user['dsPDFProfil'] .'</td></tr><tr><td style="font-size: 12px;">';
                        if (is_array($PDFPF['AllgemeinStart'])) {
                            foreach ($PDFPF['AllgemeinStart'] as $PF) {
                                $htmlAllgemein1 .= '<strong>'. $PF['feld'] .'</strong><br />'. $PF['inhalt'] .'<br /><br />';
                            }
                        }
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFMail'] .'</strong><br />'. $row['email'] .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFAMail'] .'</strong><br />'. $row['altmail'] .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFName'] .'</strong><br />'. $row['vorname'] .' '. $row['nachname'] .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFStrasse'] .'</strong><br />'. $row['strasse'] .' '. $row['hnr'] .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFOrt'] .'</strong><br />'. $row['plz'] .' '. $row['ort'] .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFLand'] .'</strong><br />'. $this->UserLand($row['land']) .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFTel'] .'</strong><br />'. $row['tel'] .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFFax'] .'</strong><br />'. $row['fax'] .'<br /><br />';
                        if (is_array($PDFPF['AllgemeinEnde'])) {
                            foreach ($PDFPF['AllgemeinEnde'] as $PF) {
                                $htmlAllgemein1 .= '<strong>'. $PF['feld'] .'</strong><br />'. $PF['inhalt'] .'<br /><br />';
                            }
                        }
                    }
                    $res->Free();
                    $htmlAllgemein1 .= '</td></tr></table>';

                    $htmlAllgemein1 .= '</td><td style="width: 50%;">';

                    $htmlAllgemein1 .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                    $htmlAllgemein1 .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">'. $lang_user['dsPDFReg'] .'</td></tr><tr><td style="font-size: 12px;">';
                    $res = $db->Query('SELECT * FROM {pre}gruppen WHERE `id` = ?',
                        $UserGruppe);
                    while($row = $res->FetchArray(MYSQLI_ASSOC)) {
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFRegDatum'] .'</strong><br />'. $RegDate .'<br /><br />';
                        $htmlAllgemein1 .= '<strong>'. $lang_user['dsPDFRegIP'] .'</strong><br />'. $RegIP;

                    }
                    $res->Free();
                    $htmlAllgemein1 .= '</td></tr></table>';

                    $htmlAllgemein1 .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                    $htmlAllgemein1 .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">Login</td></tr><tr><td style="font-size: 12px;">';
                    $res = $db->Query('SELECT * FROM {pre}gruppen WHERE `id` = ?',
                        $UserGruppe);
                    while($row = $res->FetchArray(MYSQLI_ASSOC)) {
                        $htmlAllgemein1 .= '<strong>' . $lang_user['dsPDFMailsLIP'] . '</strong><br />' . $LastIP . '<br /><br />';
                        $htmlAllgemein1 .= '<strong>' . $lang_user['dsPDFMailsLWeb'] . '</strong><br />' . $WebLogin . '<br /><br />';
                        $htmlAllgemein1 .= '<strong>' . $lang_user['dsPDFMailsLPOP3'] . '</strong><br />' . $POPLogin . '<br /><br />';
                        $htmlAllgemein1 .= '<strong>' . $lang_user['dsPDFMailsLIMAP'] . '</strong><br />' . $IMAPLogin . '<br /><br />';
                        $htmlAllgemein1 .= '<strong>' . $lang_user['dsPDFMailsLSMTP'] . '</strong><br />' . $SMTPLogin . '<br /><br />';
                    }
                    $res->Free();
                    $htmlAllgemein1 .= '</td></tr></table>';

                    $htmlAllgemein1 .= '</td></tr></table>';

                    $pdf->AddPage();
                    $htmlAllgemein1 = html_entity_decode($htmlAllgemein1);
                    $pdf->writeHTML($htmlAllgemein1, true, false, true, false, '');


                    // Allgemein
                    $htmlDaten = NULL;
                    // E-Mail
                    if ($PDFMails == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFMails'] . '</td></tr><tr><td style="font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFMailsAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'userid', 'mails') . '<br /><br />';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFMailsAnzO'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'userid', 'folders') . '<br /><br />';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFMailsSize'] . '</strong><br />' . $MailSpace . '<br /><br />';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFMailsLMail'] . '</strong><br />' . $MailSend . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    // Kalender
                    if ($PDFKalender == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFKalender'] . '</td></tr><tr><td style="width: 50%; font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFKalenderAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'dates') . '<br /><br />';
                        $htmlDaten .= '</td><td style="width: 50%; font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFKalenderGAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'dates_groups') . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    // Kontakte
                    if ($PDFKontakte == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFKontakte'] . '</td></tr><tr><td style="width: 50%; font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFKontakteAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'adressen') . '<br /><br />';
                        $htmlDaten .= '</td><td style="width: 50%; font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFKontakteGAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'adressen_gruppen') . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    // Notizen
                    if ($PDFNotizen == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFNotizen'] . '</td></tr><tr><td style="font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFNotizenAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'notes') . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    // Aufgaben
                    if ($PDFAufgaben == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFAufgaben'] . '</td></tr><tr><td style="font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFAufgabenAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'tasks') . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    // Daten
                    if ($PDFDaten == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFDaten'] . '</td></tr><tr><td style="width: 50%; font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFDatenAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'diskfiles') . '<br /><br />';
                        $htmlDaten .= '</td><td style="width: 50%; font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFDatenOAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'user', 'diskfolders') . '<br /><br />';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFDatenSize'] . '</strong><br />' . $DiskSpace . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    // Bestellungen: Allgemein
                    if ($PDFBest == 'yes') {
                        $htmlDaten .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlDaten .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFBest'] . '</td></tr><tr><td style="font-size: 12px;">';
                        $htmlDaten .= '<strong>' . $lang_user['dsPDFBestAnz'] . '</strong><br />' . $this->InfoCount($j['UserID'], 'userid', 'orders') . '<br /><br />';
                        $htmlDaten .= '</td></tr></table>';
                    }

                    if (isset($htmlDaten)) {
                        $htmlDaten = html_entity_decode($htmlDaten);
                        $pdf->writeHTML($htmlDaten, true, false, true, false, '');
                    }


                    // Module
                    $htmlModule = NULL;

                    $resmod = $db->Query('SELECT * FROM {pre}mod_ds_module ORDER BY Name');
                    while($rowmod = $resmod->FetchArray(MYSQLI_ASSOC)) {
                        $DBName             = $rowmod['Name'];
                        $DBTabelle          = $rowmod['DBTabelle'];
                        $DBSearchTabelle    = $rowmod['DBSearchTabelle'];
                        $DBSpalten          = unserialize($rowmod['Spalten']);
                        if ($rowmod['DBSearchType'] == 'userid') {
                            $DBUserSearch   = $j['UserID'];
                        } else {
                            $DBUserSearch   = $this->UserEMail($j['UserID']);
                        }
                        $SpaltenCount = count($DBSpalten);

                        $htmlModule .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlModule .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;" colspan="'. $SpaltenCount .'">'. $DBName .'</td></tr>';

                        $i = 0;
                        $res = $db->Query('SELECT * FROM '. $DBTabelle .' WHERE '. $DBSearchTabelle .' = ?',
                            $DBUserSearch);
                        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                            $htmlModule .= '<tr>';
                            foreach ($DBSpalten as $spalten) {
                                if ($this->isTimeStamp($row[$spalten]) == 1) {
                                    $htmlModule .= '<td style="font-size: 12px;">' . date('d.m.Y H:i:s', strtotime($row[$spalten])) . '</td>';
                                } else if ($this->isTimeStamp($row[$spalten]) == 2) {
                                    $htmlModule .= '<td style="font-size: 12px;">' . $this->timestampToDate($row[$spalten]) . '</td>';
                                } else {
                                    $htmlModule .= '<td style="font-size: 12px;">' . $row[$spalten] . '</td>';
                                }
                            }
                            $htmlModule .= '</tr>';
                            $i++;
                        }
                        $res->Free();
                        $htmlModule .= '</table><br /><br />';
                    }
                    $resmod->Free();

                    // Externe Module
                    if (file_exists($bm_prefs['selffolder'] . 'plugins/dsmodule/')) {
                        foreach (@scandir($bm_prefs['selffolder'] . 'plugins/dsmodule/') as $dsModule) {
                            if ($dsModule !== '.' && $dsModule !== '..') {
                                require_once 'plugins/dsmodule/' . $dsModule;
                            }
                        }
                    }

                    if (isset($htmlModule)) {
                        $htmlModule = '<h3>'. $lang_user['dsModule'] .'</h3>' . $htmlModule;
                        $pdf->AddPage();
                        $htmlModule = html_entity_decode($htmlModule);
                        $pdf->writeHTML($htmlModule, true, false, true, false, '');
                    }

                    if ($PDFGesetzestexte == 'yes') {
                        // Datenschutz: AGB / Datenschutz
                        $htmlGesetz = '<h3>' . $lang_user['dsPDFGes'] . '</h3>';
                        $htmlGesetz .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlGesetz .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFGesAGB'] . '</td></tr>';
                        $res = $db->Query('SELECT * FROM {pre}texts WHERE `language` = ? AND `key` = ?', $j['Lang'], 'tos');
                        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                            $htmlGesetz .= '<tr><td style="font-size: 12px;">' . nl2br($row['text']) . '</td></tr>';
                        }
                        $res->Free();
                        $htmlGesetz .= '</table><br /><br />';


                        $htmlGesetz .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlGesetz .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['dsPDFGesImp'] . '</td></tr>';
                        $res = $db->Query('SELECT * FROM {pre}mod_ds_impressum WHERE `Lang` = ? AND `Text` = ?', $j['Lang'], 'ImpressumContent');
                        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                            $htmlGesetz .= '<tr><td style="font-size: 12px;">' . $this->_placeholderReplace($row['Content']) . '</td></tr>';
                        }
                        $res->Free();
                        $htmlGesetz .= '</table><br /><br />';


                        $htmlGesetz .= '<table cellpadding="5" cellspacing="0" style="width: 100%;" border="0">';
                        $htmlGesetz .= '<tr><td style="border-bottom: 1px solid #cccccc; padding: 5px;">' . $lang_user['ds'] . '</td></tr>';
                        $htmlGesetz .= '<tr><td style="font-size: 12px; border-bottom: 1px solid #efefef;">' . $this->_placeholderReplace($lang_custom['DatenschutzVerantwortung']) . '</td></tr>';
                        $res = $db->Query('SELECT * FROM {pre}mod_ds_nli WHERE `Lang` = ? ORDER BY `Sortierung`', $j['Lang']);
                        while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                            $htmlGesetz .= '<tr><td style="font-size: 12px; border-bottom: 1px solid #efefef;"><strong>' . str_replace("Â", "", $row['Title']) . '</strong><br />' . str_replace("Â", "", $row['Content']) . '</td></tr>';
                        }
                        $res->Free();
                        $htmlGesetz .= '<tr><td style="font-size: 12px; border-bottom: 1px solid #efefef;">' . $this->_placeholderReplace($lang_custom['DatenschutzAbschluss']) . '</td></tr>';
                        $htmlGesetz .= '</table><br /><br />';
                    }

                    if (isset($htmlGesetz)) {
                        $pdf->AddPage();
                        $htmlGesetz = html_entity_decode($htmlGesetz);
                        $pdf->writeHTML($htmlGesetz, true, false, true, false, '');
                    }

                    // PDF Erstellen
                    $pdf->Output($pdfOrdner . $pdfName, 'F');

                    // Temp PDF Import
                    $data = file_get_contents($pdfOrdner . $pdfName);
                    $size = filesize($pdfOrdner . $pdfName);
                    $db->Query('UPDATE {pre}mod_ds_report SET `Status`=?, `JobUpdate`=?, `Dokument`=?, `DokumentSize`=? WHERE `ID`=?',
                        0,
                        date('Y-m-d H:i:s'),
                        $data,
                        $size,
                        $j['ID']);

                    // PDF löschen
                    unlink($pdfOrdner . $pdfName);

                    // Benachrichtigung
                    $db->Query('INSERT INTO {pre}notifications (`userid`,`date`,`flags`,`text_phrase`,`text_params`,`link`,`icon`,`class`) VALUES(?,?,?,?,?,?,?,?)',
                        $j['UserID'],
                        time(),
                        1,
                        'notify_ds',
                        $lang_custom['ds_notify_abschluss'],
                        'prefs.php?action=ds&',
                        'plugins/templates/images/datenschutz.png',
                        '::Datenschutz');
                    $this->sendInfoMail($PDFDSMail, $this->UserEMail($j['UserID']), $lang_custom['DatenschutzMailBetreff'], $lang_custom['DatenschutzMailText']);
                }
            }
        }
    }


    /**
     * Template
     */
    function BeforeDisplayTemplate($resourceName, &$tpl) {
        global $tpl, $db, $bm_prefs;

        // Cookie Info
        $res = $db->Query('SELECT * FROM {pre}mod_ds_cookie WHERE ID = ?', 0);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            // NLI
            if ($row['StatusNLI'] == 'yes') {
                $tpl->addCSSFile("nli", $bm_prefs['selfurl'] ."plugins/css/datenschutz_cookieinfo.css");
            }

            // LI
            if ($row['StatusLI'] == 'yes') {
                $tpl->addCSSFile("li", $bm_prefs['selfurl'] ."plugins/css/datenschutz_cookieinfo.css");
            }
        }
        $res->Free();

        // LI
        $tpl->addCSSFile("li",      $bm_prefs['selfurl'] ."plugins/css/datenschutz.css");
    }


    /**
     * B1GMail Module
     */
    function _BMModuleExists($module=NULL) {
        global $db;

        $res = $db->Query('SELECT `installed` FROM {pre}mods WHERE modname =? AND installed =?',
            $module,
            1);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            return $row['installed'];
        }
        $res->Free();

    }


    /**
     * Frontend
     */
    function getUserPages($loggedin) {
        global $lang_user;

        if($loggedin)
            return(array());

        if(DATENSCHUTZ_ACTIVATE) {
            return(array(
                'Datenschutz' => array(
                    'text'	=> $lang_user['dsTitel'],
                    'link'	=> 'index.php?action=Datenschutz'
                )
            ));
        } else {
            return (array());
        }
    }


    function FileHandler($file, $action) {
        global $tpl, $db, $bm_prefs, $userRow, $lang_user, $lang_custom, $currentLanguage, $currentCharset;

        // language given?
        $selectedLang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'deutsch';

        // Deref
        $res = $db->Query('SELECT * FROM {pre}mod_ds_deref WHERE Status = ? AND ID = ?', 'yes', 0);
        $row = $res->FetchArray();
        $derefStatus    = $row['Status'];
        $derefTemplate  = $row['Template'];
        $res->Free();
        if ($file == 'deref.php' && $derefStatus == 'yes') {
            if (!file_exists($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl.bak')) {
                rename($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl', $bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl.bak');
                copy($bm_prefs['selffolder'] . 'plugins/templates/datenschutz.nli.deref.tpl', $bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl');
            }

            $url = $_SERVER['REQUEST_URI'];
            $sepPos = strpos($url, '?');
            if($sepPos !== false) {
                $targetURL = substr($url, $sepPos+1);
                $tpl->assign('url', HTMLFormat($targetURL));
                $tpl->display('nli/deref.tpl');
            }
            exit();
        }

        // Impressum
        $resimp = $db->Query('SELECT `ShowImpressum` FROM {pre}mod_ds_conf WHERE ID = ?', 0);
        $DBShowImpressum = $resimp->FetchArray()['ShowImpressum'];
        $resimp->Free();
        if(($file == 'index.php' && $action == 'imprint') && $DBShowImpressum == 'yes') {

            // Contact Form
            if($bm_prefs['contactform'] == 'yes') {
                if(!class_exists('BMCaptcha'))
                    include(B1GMAIL_DIR . 'serverlib/captcha.class.php');
                $captcha = BMCaptcha::createDefaultProvider();

                if(isset($_POST['do']) && $_POST['do'] == 'submitContactForm'){
                    $invalidFields = array();
                    $errorMsg = $lang_user['checkfields'];
                    $subject = 'b1gMail ' . $lang_user['contactform'];

                    if($bm_prefs['contactform_name'] == 'yes') {
                        $name = trim($_POST['name']);
                        if(strlen($name) < 2)
                            $invalidFields[] = 'name';
                    } else {
                        $name = '';
                    }

                    if($bm_prefs['contactform_subject'] == 'yes') {
                        $subject = $_POST['subject'] . ' (' . $subject . ')';
                        if(trim($_POST['subject']) == '')
                            $invalidFields[] = 'subject';
                    }

                    $email = ExtractMailAddress(trim($_POST['email']));
                    if(strlen($email) < 5)
                        $invalidFields[] = 'email';

                    $text = trim($_POST['text']);
                    if(strlen($text) < 5)
                        $invalidFields[] = 'text';

                    if(!$captcha->check()) {
                        $invalidFields[] = 'safecode';
                        $errorMsg .= ' ' . $lang_user['invalidcode'];
                    }

                    if(count($invalidFields) == 0) {
                        if($name)
                            $from = sprintf('"%s" <%s>', str_replace(array("\r", "\n", "\t", "\""), '', $name), $email);
                        else
                            $from = $email;

                        if(!class_exists('BMMailBuilder'))
                            include(B1GMAIL_DIR . 'serverlib/mailbuilder.class.php');

                        $mail = _new('BMMailBuilder', array(true));
                        $mail->SetUserID(USERID_SYSTEM);
                        $mail->AddHeaderField('From',				$from);
                        $mail->AddHeaderField('To',					$bm_prefs['contactform_to']);
                        $mail->AddHeaderField('Subject',			$subject);
                        if($bm_prefs['write_xsenderip'] == 'yes')
                            $mail->AddHeaderField('X-Sender-IP',	$_SERVER['REMOTE_ADDR']);
                        $mail->AddText($text, 'plain', $currentCharset);
                        $result = $mail->Send() !== false;
                        $mail->CleanUp();

                        if($result) {
                            $tpl->assign('success', true);
                        } else {
                            $tpl->assign('errorMsg', $lang_user['cform_senderror']);
                        }
                    } else {
                        $tpl->assign('invalidFields', $invalidFields);
                        $tpl->assign('errorMsg', $errorMsg);
                    }
                }

                $tpl->assign('captchaHTML', $captcha->getHTML());
                $tpl->assign('captchaInfo', $captcha->getInfo());
            }

            $rescon = $db->Query('SELECT * FROM {pre}mod_ds_impressum WHERE Lang = "'. $selectedLang .'"');
            while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                $tpl->assign($row['Text'] .'Title', $lang_user['ds'. $row['Text']]);
                $tpl->assign($row['Text'] .'Content', $this->_placeholderReplace($row['Content']));
            }
            $rescon->Free();

            $tpl->assign('contactform', $bm_prefs['contactform'] == 'yes');
            $tpl->assign('contactform_name', $bm_prefs['contactform_name'] == 'yes');
            $tpl->assign('contactform_subject', $bm_prefs['contactform_subject'] == 'yes');
            $tpl->assign('contactform_subjects', array_filter(array_map('trim', explode("\n", $lang_custom['contact_subjects']))));

            $tpl->assign('ConsumerDisputeResolution', $this->dsGeneral('ConsumerDisputeResolution') == 'yes');
            $tpl->assign('SocialMedia', $this->dsGeneral('SocialMedia') == 'yes');
            $tpl->assign('CopyrightNotices', $this->dsGeneral('CopyrightNotices') == 'yes');
            $tpl->assign('PhotoCredits', $this->dsGeneral('PhotoCredits') == 'yes');


            if (file_exists(B1GMAIL_REL ."plugins/templates/datenschutz.nli.imprint.custom.tpl")) {
                $tpl->assign('page', $this->_templatePath('datenschutz.nli.imprint.custom.tpl'));
            } else {
                $tpl->assign('page', $this->_templatePath('datenschutz.nli.imprint.tpl'));
            }
            $tpl->display('nli/index.tpl');
            exit();
        }

        // Hook
        $tpl->registerHook("li:index.tpl:beforeContent", B1GMAIL_DIR ."plugins/templates/datenschutz.li.hook.tpl");

        $res = $db->Query('SELECT * FROM {pre}mod_ds_cookie WHERE ID = ?', 0);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {

            // Cookies
            $showscookie = NULL;
            $scookie = explode("\n", str_replace("\r", "", $row['SystemCookie']));
            foreach ($scookie as $coo) {
                $showscookie .= ",'". $coo ."'";
            }
            $showscookie = ltrim($showscookie, ',');

            $tpl->assign('CookieSystem',                $showscookie);
            $tpl->assign('CookieJavaScriptAllow',       $row['JavaScriptAllow']);
            $tpl->assign('CookieJavaScriptDeny',        $row['JavaScriptDeny']);
            $tpl->assign('CookieJavaScriptDenyCookie',  $row['JavaScriptDenyCookie']);
            $tpl->assign('CookieDesignBG',              $row['DesignBG']);
            $tpl->assign('CookieDesignFG',              $row['DesignFG']);
            $tpl->assign('CookieDesignLinkBG',          $row['DesignLinkBG']);
            $tpl->assign('CookieDesignLinkFG',          $row['DesignLinkFG']);
            $tpl->assign('CookieDesignTheme',           $row['DesignTheme']);
            $tpl->assign('CookieDesignPosition',        $row['DesignPosition']);
            $tpl->assign('CookieDesignStatic',          $row['DesignStatic']);
            $tpl->assign('CookieDesignType',            $row['DesignType']);
            $tpl->assign('CookieText',                  unserialize($row['CookieText'])[$currentLanguage]);
            $tpl->assign('CookieClose',                 unserialize($row['CookieClose'])[$currentLanguage]);
            $tpl->assign('CookieAllow',                 unserialize($row['CookieAllow'])[$currentLanguage]);
            $tpl->assign('CookieLinkText',              unserialize($row['CookieLinkText'])[$currentLanguage]);
            $tpl->assign('CookieLink',                  $row['CookieLink']);

            if ($row['StatusLI'] == 'yes') { $tpl->registerHook("li:index.tpl:afterContent", B1GMAIL_DIR ."plugins/templates/datenschutz.cookie.hook.tpl"); }
            if ($row['StatusNLI'] == 'yes') { $tpl->registerHook("nli:index.tpl:afterContent", B1GMAIL_DIR ."plugins/templates/datenschutz.cookie.hook.tpl"); }
        }
        $res->Free();

        // Cookie Status
        if ($_COOKIE['cookieconsent_status'] == 'allow') {
            $tpl->assign('CookieStatus',                TRUE);
        } else {
            $tpl->assign('CookieStatus',                FALSE);
        }

        // Pflichtfelder
        $pflichtAnrede      = $this->showPflichtOption($userRow['gruppe'], 'f_anrede');
        $pflichtVorname     = $this->showPflichtOption($userRow['gruppe'], 'f_vorname');
        $pflichtNachname    = $this->showPflichtOption($userRow['gruppe'], 'f_nachname');
        $pflichtStrasse     = $this->showPflichtOption($userRow['gruppe'], 'f_strasse');
        $pflichtTelefon     = $this->showPflichtOption($userRow['gruppe'], 'f_telefon');
        $pflichtFax         = $this->showPflichtOption($userRow['gruppe'], 'f_fax');
        $pflichtAMail       = $this->showPflichtOption($userRow['gruppe'], 'f_alternativ');
        $pflichtSMS         = $this->showPflichtOption($userRow['gruppe'], 'f_mail2sms_nummer');

        if (isset($pflichtAnrede))      { $bm_prefs['f_anrede']             = $pflichtAnrede; }
        if (isset($pflichtVorname))     { $bm_prefs['f_vorname']            = $pflichtVorname; }
        if (isset($pflichtNachname))    { $bm_prefs['f_nachname']           = $pflichtNachname; }
        if (isset($pflichtStrasse))     { $bm_prefs['f_strasse']            = $pflichtStrasse; }
        if (isset($pflichtTelefon))     { $bm_prefs['f_telefon']            = $pflichtTelefon; }
        if (isset($pflichtFax))         { $bm_prefs['f_fax']                = $pflichtFax; }
        if (isset($pflichtAMail))       { $bm_prefs['f_alternativ']         = $pflichtAMail; }
        if (isset($pflichtSMS))         { $bm_prefs['f_mail2sms_nummer']    = $pflichtSMS; }


        // AV-Vertrag anzeigen
        $AVVertragAnzeigen = TRUE;
        $AVVertragAusgeben = NULL;

        // Pflichtfelder
        if($file != 'index.php') {
            $invalidFields = $this->checkProfile();
            if (!is_array($invalidFields)) {
                $invalidFields = array();
            }
            if (is_array($invalidFields) && (($this->_BMModuleExists('ProfileCheckPlugin') == 1 || count($invalidFields) > 0) && count($invalidFields) > 0)) {
                $AVVertragAnzeigen = FALSE;

                if ($file != 'index.php' && $file != 'deref.php' && !($file == 'prefs.php' && $action == 'contact')
                    && !($file == 'start.php' && $action == 'logout')
                    && !($file == 'start.php' && $action == 'usersearch')) {
                    header('Location: prefs.php?action=contact&sid=' . session_id());
                    exit();
                }
                $AVVertragAnzeigen = FALSE;
            }
        }

        // Konto als gelöscht markieren
        if (isset($_REQUEST['avkontoloeschen'])) {
            $db->Query('DELETE FROM {pre}mod_ds_av_user WHERE UserID =?',
                $userRow['id']);


            $db->Query('UPDATE {pre}users SET gesperrt =?, notes=CONCAT(notes,?) WHERE id =?',
                'delete',
                "\n[". date('r') ."] ". $lang_user['dsAVAblehnen'],
                $userRow['id']);

            PutLog($this->name. ' ID: '. $userRow['id']. ' - '. $lang_user['dsAVAblehnen']);

            header('Location: start.php?action=logout&sid=' . session_id());
            exit();
        }

        // AV Zustimmen
        if (isset($_REQUEST['avzustimmen'])) {
            $MSG    = NULL;
            $ERROR  = FALSE;

            $MSG = '<div style="margin: 20px; padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-radius: 4px;color: #a94442; background-color: #f2dede; border-color: #ebccd1;">';
            if (empty($_REQUEST['avbestatigen'])) {
                $MSG .= $lang_user['dsMSGAVZustimmen'] .'<br />';
                $ERROR = TRUE;
            }
            $MSG .= '</div>';

            if ($ERROR == FALSE) {
                if (@file_exists('plugins/php/tcpdf/tcpdf.php')) {
                    include_once 'plugins/php/tcpdf/tcpdf.php';

                    $pdfOrdner = $bm_prefs['datafolder'];
                    $dokDatum = date("d.m.Y H:i:s");
                    $pdfAuthor = $bm_prefs['titel'];
                    $pdfBenutzer = $this->UserInfos($userRow['id']);
                    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                    $PDFLogo = $this->dsGeneral('Logo');

                    // Logo
                    if ($PDFLogo !== '' && file_exists('plugins/templates/images/' . $PDFLogo)) {
                        $pdfLogo = '<img src="plugins/templates/images/' . $PDFLogo . '" style="width: 250px; height:auto;">';
                    } else {
                        $pdfLogo = '<img src="plugins/templates/images/datenschutz_logo.png" style="width: 250px; height:auto;">';
                    }
                    $pdfName = md5($dokDatum) . '.pdf';

                    // Erstellung des PDF Dokuments
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor($pdfAuthor);
                    $pdf->SetTitle('AV-Vertrag: ' . $pdfBenutzer);
                    $pdf->SetSubject('AV-Vertrag: ' . $pdfBenutzer);
                    $pdf->setPrintHeader(false);
                    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                    $pdf->AddPage();


                    // Header
                    $html = '<table cellpadding="0" cellspacing="0" style="width: 100%; margin: 0px;">
                                <tr>
                                    <td style="padding: 0px 0px 0px 30px;">
                                        <small style="font-size: 10px;">'. $lang_user['dsPDFErstell'] .': '. $dokDatum .'</small>
                                    </td>
                                    <td style="text-align: right; color: #999999; font-size: 9px;">
                                        '.nl2br(trim($pdfLogo)).'<br />
                                        <strong>'. $this->dsGeneral('Name') .'</strong><br />
                                        '. $this->dsGeneral('Street') .' '. $this->dsGeneral('StreetNr') .'<br />
                                        '. $this->dsGeneral('PLZ') .' '. $this->dsGeneral('Place') .', '. $this->dsGeneral('Country') .'<br /><br />
                                        Tel.: '. $this->dsGeneral('Phone') .'<br />
                                        E-Mail: '. $this->dsGeneral('PrivacyEMail') .'
                                    </td>
                                </tr>
                            </table><br /><br />';
                    $html = html_entity_decode($html);
                    $pdf->writeHTML($html, true, false, true, false, '');

                    // Allgemein
                    // Allgemein: AV
                    $resav = $db->Query('SELECT * FROM {pre}mod_ds_av WHERE Lang =? LIMIT 1',
                        $currentLanguage);
                    while ($row = $resav->FetchArray(MYSQLI_ASSOC)) {
                        $htmlAllgemein = '<h3>' . nl2br(str_replace("Â", "", $row['Title'])) . '</h3>';
                        $htmlAllgemein .= $this->_placeholderReplace(nl2br(str_replace("Â", "", $row['Content'])));
                    }
                    $resav->Free();
                    $htmlAllgemein = html_entity_decode($htmlAllgemein);
                    $pdf->writeHTML($htmlAllgemein, true, false, true, false, '');

                    $pdf->Output($pdfOrdner . $pdfName, 'F');

                    // Temp PDF Import
                    $data = file_get_contents($pdfOrdner . $pdfName);
                    $size = filesize($pdfOrdner . $pdfName);
                    $db->Query('INSERT INTO {pre}mod_ds_av_user (`UserID`,`GroupID`,`DocDate`,`Dokument`,`DokumentSize`) VALUES(?,?,?,?,?)',
                        $userRow['id'],
                        $userRow['gruppe'],
                        date('Y-m-d H:i:s'),
                        $data,
                        $size);

                    // PDF löschen
                    unlink($pdfOrdner . $pdfName);
                    $AVVertragAusgeben .= '<!-- Datenschutz: AV -->';
                    $AVVertragAusgeben .= '<div class="popup"><div class="popup-main">';
                    $AVVertragAusgeben .= '<div class="popup-header">'. $lang_user['dsTitel'] . '</div>';
                    $AVVertragAusgeben .= '<div class="popup-content">'. $lang_user['dsAVMSGSave'] .'</div>';
                    $AVVertragAusgeben .= '<div class="popup-footer"><div class="popup-footer-right"><a href="prefs.php?action=ds&sid='. session_id() .'" class="linkbutton">'. $lang_user['dsAVAnzeigen'] .'</a> <a href="' . $_SERVER['PHP_SELF'] . '?sid='. session_id() .'" class="linkbutton">'. $lang_user['dsAVSchliessen'] .'</a></div></div>';
                    $AVVertragAusgeben .= '</div></div>';
                }
            }
        }

        // Anstehender AV Vertrag
        if ($AVVertragAnzeigen == TRUE && isset($userRow['id']) && $this->GetGroupOptionValue('datenschutz-av', $userRow['gruppe'])) {
            $db->Query('DELETE FROM {pre}mod_ds_av_user WHERE UserID = ? AND GroupID NOT LIKE ?', $userRow['id'], $userRow['gruppe']);
            $resav = $db->Query('SELECT * FROM {pre}mod_ds_av_user WHERE UserID =? AND GroupID =?',
                $userRow['id'],
                $userRow['gruppe']);
            list($anz) = $resav->FetchArray(MYSQLI_NUM);
            $resav->Free();

            if ($anz == 0) {
                $rescon = $db->Query('SELECT * FROM {pre}mod_ds_av WHERE Lang =? LIMIT 1',
                    $currentLanguage);
                while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                    $AVVertragAusgeben .= '<!-- Datenschutz: AV -->';
                    $AVVertragAusgeben .= '<div class="popup"><div class="popup-main"><form name="f1" method="post" action="' . $_SERVER['PHP_SELF'] . '?sid=' . session_id() . '">';
                    $AVVertragAusgeben .= '<div class="popup-header">'. $lang_user['dsTitel'] . ' ' . nl2br($row['Title']) . '</div>';
                    $AVVertragAusgeben .= '<div class="popup-content">'. $MSG . $this->_placeholderReplace(nl2br($row['Content'])) . '</div>';
                    $AVVertragAusgeben .= '<div class="popup-footer"><div class="popup-footer-left"><input type="checkbox" name="avbestatigen" id="avbestatigen" /><label for="avbestatigen">' . $lang_user['dsAVBestatigung'] . '</label></div>';
                    $AVVertragAusgeben .= '<div class="popup-footer-right"><input type="submit" name="avzustimmen" class="primary" value="' . $lang_user['dsAVZustimmen'] . '" /><input type="submit" name="avkontoloeschen" onclick="return confirm(\'' . $lang_user['dsAVKontoLoeschen'] . '\');" href="prefs.php?action=ds&do=delete&id={$aufgabe.ID}&sid={$SID}" value="' . $lang_user['dsAVNichtZustimmen'] . '" /></div></div>';
                    $AVVertragAusgeben .= '</form></div></div>';
                }
                $rescon->Free();
            }
        }

        $tpl->assign('AVVertragAusgeben',     $AVVertragAusgeben);

        // LI
        if($file=='prefs.php') {
            $GLOBALS['prefsItems']['ds']    = true;
            $GLOBALS['prefsImages']['ds']   = 'plugins/templates/images/datenschutz.png';
            $GLOBALS['prefsIcons']['ds']    = 'plugins/templates/images/datenschutz.png';
        }

        // NLI
        if($file == 'index.php' && $action == 'Datenschutz') {

            // Content
            $contents   = NULL;
            $i          = 0;
            $rescon = $db->Query('SELECT * FROM {pre}mod_ds_nli WHERE Lang = ? ORDER BY `Sortierung`',
                $currentLanguage);
            while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                $contents[$row['ID']] = array(
                    "ID"        => $row['ID'],
                    "Numm"      => $i++,
                    "Title"     => $row['Title'],
                    "Content"   => $row['Content']
                );
            }
            $rescon->Free();

            // Einstellungen
            $rescof = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID =? LIMIT 1', 0);
            while($row = $rescof->FetchArray(MYSQLI_ASSOC)) {
                $tpl->assign('LogWebAccStatus',     $row['LogWebAccStatus']);
                $tpl->assign('LogWebAccInter',      $row['LogWebAccInter']);
                $tpl->assign('LogWebAccInhalt',     $row['LogWebAccInhalt']);
                $tpl->assign('LogWebErrStatus',     $row['LogWebErrStatus']);
                $tpl->assign('LogWebErrInter',      $row['LogWebErrInter']);
                $tpl->assign('LogWebErrInhalt',     $row['LogWebErrInhalt']);
                $tpl->assign('LogB1GStatus',        $row['LogB1GStatus']);
                $tpl->assign('LogB1GInter',         $row['LogB1GInter']);
                $tpl->assign('LogB1GInhalt',        $row['LogB1GInhalt']);
                $tpl->assign('LogB1GSStatus',       $row['LogB1GSStatus']);
                $tpl->assign('LogB1GSInhalt',       $row['LogB1GSInhalt']);
                $tpl->assign('LogArchiveInter',     $row['LogArchiveInter']);
                $tpl->assign('LogArchiveCron',      $row['LogArchiveCron']);
            }
            $rescof->Free();

            // B1GMailServer
            if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {
                $resbgs = $db->Query('SELECT * FROM {pre}bms_prefs WHERE ID =? LIMIT 1', 1);
                while ($row = $resbgs->FetchArray(MYSQLI_ASSOC)) {
                    $tpl->assign('LogB1GSInter',    $row['logs_autodelete_days']);
                    $tpl->assign('LogB1GSCron',     $row['logs_autodelete']);
                }
                $resbgs->Free();
            }

            $tpl->assign('Verantwortung',           $this->_placeholderReplace($lang_custom['DatenschutzVerantwortung']));
            $tpl->assign('DSAbschluss',             $this->_placeholderReplace($lang_custom['DatenschutzAbschluss']));
            $tpl->assign('ContentArray',            $contents);
            if (file_exists(B1GMAIL_REL ."plugins/templates/datenschutz.nli.info.custom.tpl")) {
                $tpl->assign('page',                $this->_templatePath('datenschutz.nli.info.custom.tpl'));
            } else {
                $tpl->assign('page',                $this->_templatePath('datenschutz.nli.info.tpl'));
            }
            $tpl->display('nli/index.tpl');
            exit();
        }
    }


    /**
     * Einstellungen
     */
    function UserPrefsPageHandler($action) {
        global $tpl, $db, $currentLanguage, $bm_prefs, $userRow, $lang_user, $lang_admin, $lang_custom;

        $MSG = NULL;

        if($action == 'ds') {

            if (isset($_REQUEST['speichern'])) {
                $save = $_REQUEST['speichern'];
            } else {
                $save = NULL;
            }

            // Report neu erstellen
            if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'renew') && isset($_REQUEST['id'])) {
                $db->Query('UPDATE {pre}mod_ds_report SET `Status`=? WHERE `ID`=?',
                    1,
                    $_REQUEST['id']);

                // Benachrichtigung
                $EMail = $this->dsGeneral('PrivacyEMail');
                $vars = array('USERNAME' => $this->UserInfos($userRow['id']));
                SystemMail($EMail, trim($EMail), $lang_custom['DatenschutzNewReportBetreff'], 'DatenschutzNewReportText', $vars, 0);

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_user['dsPrefsMSGAdd'] .' <br />';
                $MSG .= '</div>';
            }

            // Report Datei Downloaden
            if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'download') && isset($_REQUEST['id'])) {
                $res = $db->Query('SELECT * FROM {pre}mod_ds_report WHERE UserID =? AND ID =?',
                    $userRow['id'],
                    $_REQUEST['id']);
                while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                    header("Content-Type: application/pdf");
                    header("Content-Length: " . $row['DokumentSize']);
                    header("Content-Disposition: attachment; filename=ReportDownload.pdf");

                    echo $row['Dokument'];
                    exit();
                }
                $res->Free();
            }

            // AV Datei Downloaden
            if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'avdownload') && isset($_REQUEST['id'])) {
                $res = $db->Query('SELECT * FROM {pre}mod_ds_av_user WHERE UserID =? AND ID =?',
                    $userRow['id'],
                    $_REQUEST['id']);
                while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                    header("Content-Type: application/pdf");
                    header("Content-Length: " . $row['DokumentSize']);
                    header("Content-Disposition: attachment; filename=AVDownload.pdf");

                    echo $row['Dokument'];
                    exit();
                }
                $res->Free();
            }

            // Aufgabe erstellen
            if (isset($save)) {
                if ($_REQUEST['logwebaccstatus'] == 'on') {
                    $_REQUEST['logwebaccstatus'] = 'yes';
                } else {
                    $_REQUEST['logwebaccstatus'] = 'no';
                }

                // Optionen
                $Optionen = array(
                    'dsPrefsProfile'        => $_REQUEST['allgprofile'],
                    'dsPrefsEinsicht'       => $_REQUEST['allgeinsicht'],
                    'dsPrefsNutzung'        => $_REQUEST['allgnutzung'],
                    'dsPrefsRZ'             => $_REQUEST['allgrz'],
                    'dsPrefsBackup'         => $_REQUEST['allgbackup'],
                    'dsPrefsReg'            => $_REQUEST['allgreg'],
                    'dsPrefsLWeb'           => $_REQUEST['allglweb'],
                    'dsPrefsLPOP'           => $_REQUEST['allglpop'],
                    'dsPrefsLIMAP'          => $_REQUEST['allglimap'],
                    'dsPrefsLSMTP'          => $_REQUEST['allglsmtp'],
                    'dsDaten'               => $_REQUEST['daten'],
                    'dsBestellAllgemein'    => $_REQUEST['bestallgemein'],
                    'dsModule'              => $_REQUEST['module'],
                    'dsInfoGes'             => $_REQUEST['infoges']
                );

                $Optionen = serialize($Optionen);


                // Konfiguration
                $res = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID=?',
                    0);
                while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                    // Autogen
                    if ($row['AutoPDFGen'] == 'yes') {
                        $DBReportStatus = 2;
                    } else {
                        $DBReportStatus = 1;


                        // Benachrichtigung
                        $EMail = $this->dsGeneral('PrivacyEMail');
                        $vars = array('USERNAME' => $this->UserInfos($userRow['id']));
                        SystemMail($EMail, trim($EMail), $lang_custom['DatenschutzNewReportBetreff'], 'DatenschutzNewReportText', $vars, 0);
                    }
                }
                $res->Free();

                $db->Query('INSERT INTO {pre}mod_ds_report (`Status`,`Lang`,`UserID`,`JobDate`, `Optionen`) VALUES(?,?,?,?,?)',
                    $DBReportStatus,
                    $currentLanguage,
                    $userRow['id'],
                    date('Y-m-d H:i:s'),
                    $Optionen);

                // AutoGen
                if ($DBReportStatus = 2) {
                    $this->OnCron();
                }

                $MSG = '<div class="infostate-ok">';
                $MSG .= $lang_user['dsPrefsMSGAdd'] . ' <br />';
                $MSG .= '</div>';

            }

            // Aufgaben ausgeben
            $AufgabenArray = NULL;
            $rescon = $db->Query('SELECT * FROM {pre}mod_ds_report WHERE UserID =? LIMIT 1',
                $userRow['id']);
            while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                if ($row['Status'] == 0) {
                    $status_text = $lang_user['dsStatusTextEnd'];
                } else if ($row['Status'] == 1) {
                    $status_text = $lang_user['dsStatusTextPlan'];
                } else if ($row['Status'] == 2) {
                    $status_text = $lang_user['dsStatusTextAcce'];
                } else {
                    $status_text = $lang_user['dsStatusTextLauf'];
                }

                if (isset($row['JobUpdate'])) {
                    $JobUpdate = date($userRow['datumsformat'], strtotime($row['JobUpdate']));
                } else {
                    $JobUpdate = NULL;
                }

                $AufgabenArray[$row['ID']] = array(
                    "ID"            => $row['ID'],
                    "Status"        => $row['Status'],
                    "StatusText"    => $status_text,
                    "JobDate"       => date($userRow['datumsformat'], strtotime($row['JobDate'])),
                    "JobUpdate"     => $JobUpdate,
                    "Optionen"      => unserialize($row['Optionen']),
                    "Dokument"      => $row['Dokument'],
                    "DokumentSize"  => $this->showFilesize($row['DokumentSize'], 2)
                );
            }
            $rescon->Free();

            $rescon = $db->Query('SELECT * FROM {pre}mod_ds_av_user WHERE UserID =? ORDER BY ID DESC',
                $userRow['id']);
            while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {

                $AVArray[$row['ID']] = array(
                    "ID"        => $row['ID'],
                    "Date"      => date($userRow['datumsformat'], strtotime($row['DocDate'])),
                    "Dokument"  => $row['Dokument'],
                    "DokumentSize" => $this->showFilesize($row['DokumentSize'], 2)
                );
            }
            $rescon->Free();

            $res = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID=?',
                0);
            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                $tpl->assign('InfoMSG', str_replace(array("%KOSTEN%"), array($row['ReportKosten']), $lang_user['dsPrefsInformation']));
            }
            $res->Free();

            $tpl->assign('SID', session_id());
            $tpl->assign('MSG', $MSG);
            $tpl->assign('AufgabenArr', $AufgabenArray);
            $tpl->assign('AVArr',       $AVArray);
            $tpl->assign('pageContent', $this->_templatePath('datenschutz.prefs.info.tpl'));
            $tpl->display('li/index.tpl');
            return (true);
        }
    }


    /**
     * Administration
     */
    function AdminHandler() {
        global $tpl, $lang_admin;

        if ($this->dsGeneral('Status') == 'no') {
            if(!isset($_REQUEST['do'])) {
                $_REQUEST['do'] = 'general';
            }
        } else {
            if(!isset($_REQUEST['do'])) {
                $_REQUEST['do'] = 'reports';
            }
        }

        $tabs = array(
            0 => array(
                'title'		=> $lang_admin['dsGeneral'],
                'icon'		=> '../plugins/templates/images/datenschutz_company.png',
                'link'		=> $this->_adminLink() . '&do=general&',
                'active'	=> $_REQUEST['do'] == 'general'
            ),
            1 => array(
                'title'		=> $lang_admin['dsText'],
                'icon'		=> '../plugins/templates/images/datenschutz_text.png',
                'link'		=> $this->_adminLink() . '&do=text&',
                'active'	=> $_REQUEST['do'] == 'text'
            ),
            2 => array(
                'title'		=> $lang_admin['dsReports'],
                'icon'		=> '../plugins/templates/images/datenschutz_report.png',
                'link'		=> $this->_adminLink() . '&do=reports&',
                'active'	=> $_REQUEST['do'] == 'reports'
            ),
            3 => array(
                'title'		=> $lang_admin['dsModule'],
                'icon'		=> '../plugins/templates/images/datenschutz_module.png',
                'link'		=> $this->_adminLink() . '&do=module&',
                'active'	=> $_REQUEST['do'] == 'module'
            ),
            4 => array(
                'title'		=> $lang_admin['dsAV'],
                'icon'		=> '../plugins/templates/images/datenschutz_av.png',
                'link'		=> $this->_adminLink() . '&do=av&',
                'active'	=> $_REQUEST['do'] == 'av'
            ),
            5 => array(
                'title'		=> $lang_admin['dsCookie'],
                'icon'		=> '../plugins/templates/images/datenschutz_cookie.png',
                'link'		=> $this->_adminLink() . '&do=cookie&',
                'active'	=> $_REQUEST['do'] == 'cookie'
            ),
            6 => array(
                'title'		=> $lang_admin['dsDerefTitle'],
                'icon'		=> '../plugins/templates/images/datenschutz_deref.png',
                'link'		=> $this->_adminLink() . '&do=deref&',
                'active'	=> $_REQUEST['do'] == 'deref'
            ),
            7 => array(
                'title'		=> $lang_admin['dsConfig'],
                'icon'		=> '../plugins/templates/images/datenschutz_settings.png',
                'link'		=> $this->_adminLink() . '&do=conf&',
                'active'	=> $_REQUEST['do'] == 'conf'
            ),
            8 => array(
                'title'		=> $lang_admin['dsPflicht'],
                'icon'		=> '../plugins/templates/images/datenschutz_field.png',
                'link'		=> $this->_adminLink() . '&do=pflicht&',
                'active'	=> $_REQUEST['do'] == 'pflicht'
            ),
            9 => array(
                'title'		=> $lang_admin['dsHilfe'],
                'icon'		=> '../plugins/templates/images/datenschutz_help.png',
                'link'		=> $this->_adminLink() . '&do=hilfe&',
                'active'	=> $_REQUEST['do'] == 'hilfe'
            )
        );

        $tpl->assign('tabs', $tabs);

        if($_REQUEST['do'] == 'general')        { $this->_dsGeneral(); }
        if($_REQUEST['do'] == 'text')           { $this->_dsText(); }
        if($_REQUEST['do'] == 'dsadd')          { $this->_dsTextDatenschutzAdd(); }
        if($_REQUEST['do'] == 'dsedit')         { $this->_dsTextDatenschutzEdit(); }
        if($_REQUEST['do'] == 'reports')        { $this->_reportsPage(); }
        if($_REQUEST['do'] == 'module')         { $this->_modulePage(); }
        if($_REQUEST['do'] == 'av')             { $this->_avPage(); }
        if($_REQUEST['do'] == 'avuser')         { $this->_avuserPage(); }
        if($_REQUEST['do'] == 'cookie')         { $this->_cookiePage(); }
        if($_REQUEST['do'] == 'deref')          { $this->_derefPage(); }
        if($_REQUEST['do'] == 'conf')           { $this->_confPage(); }
        if($_REQUEST['do'] == 'pflicht')        { $this->_pflichtPage(); }
        if($_REQUEST['do'] == 'hilfe')          { $this->_hilfePage(); }
    }


    /**
     * Allgemeine Infos
     */
    function _dsGeneral() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        $MSG = NULL;

        // Logo löschen
        if ((isset($_REQUEST['do2']) && $_REQUEST['do2']) == 'dellogo') {
            $Logo = $this->dsGeneral('Logo');
            unlink('../plugins/templates/images/'. $Logo);

            $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', '', 'Logo');

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsMSGLogoDel'] .' <br />';
            $MSG .= '</div>';
        }

        // Speichern
        if (isset($_REQUEST['gensave'])) {
            $MSG = '<div class="infostate-error">';
            if(empty($_REQUEST['dsprivacymail'])) {
                $MSG  .= $lang_admin['dsMSGDSMail'] .' <br />';
                $ERROR = true;
            }
            $MSG .= '</div>';

            if ($ERROR == FALSE) {
                if ($_REQUEST['dsconsumerdis'] == 'on') {
                    $_REQUEST['dsconsumerdis'] = 'yes';
                } else {
                    $_REQUEST['dsconsumerdis'] = 'no';
                }

                if ($_REQUEST['dssocialmedia'] == 'on') {
                    $_REQUEST['dssocialmedia'] = 'yes';
                } else {
                    $_REQUEST['dssocialmedia'] = 'no';
                }

                if ($_REQUEST['dscopyrightnot'] == 'on') {
                    $_REQUEST['dscopyrightnot'] = 'yes';
                } else {
                    $_REQUEST['dscopyrightnot'] = 'no';
                }

                if ($_REQUEST['dsphotocredits'] == 'on') {
                    $_REQUEST['dsphotocredits'] = 'yes';
                } else {
                    $_REQUEST['dsphotocredits'] = 'no';
                }

                // Lgogo
                if (array_key_exists('logo', $_FILES)) {
                    if ($_FILES['logo']['tmp_name'] !== '') {
                        $BildTMPName = $_FILES['logo']['tmp_name'];
                        $BildExt = pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);
                        $NewName = 'datenschutz_pdf_logo.' . $BildExt;
                        if (isset($BildExt)) {
                            move_uploaded_file($BildTMPName, '../plugins/templates/images/' . $NewName);

                            $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $NewName, 'Logo');
                        }
                    }
                }

                // Grundangaben
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', 'yes', 'Status');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsname'], 'Name');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsoptional'], 'Optional');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsstreet'], 'Street');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsstreetnr'], 'StreetNr');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsplz'], 'PLZ');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsplace'], 'Place');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dscountry'], 'Country');

                // Kontaktmöglichkeiten
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dscontactmail'], 'ContactEMail');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsprivacymail'], 'PrivacyEMail');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsphone'], 'Phone');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsfax'], 'Fax');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dscontactform'], 'ContactForm');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsmore'], 'MoreContactOptions');

                // Firmeneinstellungen
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsauthorized'], 'AuthorizedToRepresent');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsustid'], 'UstID');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dswidnr'], 'WidNr');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsbusinessarea'], 'BusinessArea');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsagb'], 'AGBUrl');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsregistrycurt'], 'RegistryCourt');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsregistrycurtnr'], 'RegistryCourtNr');

                // Zusatztexte für das Impressum
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsconsumerdis'], 'ConsumerDisputeResolution');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dssocialmedia'], 'SocialMedia');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dscopyrightnot'], 'CopyrightNotices');
                $db->Query('UPDATE {pre}mod_ds_general SET `Value`=? WHERE `Name`=?', $_REQUEST['dsphotocredits'], 'PhotoCredits');

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGKonf'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Logo
        $tpl->assign('Logo',                        $this->dsGeneral('Logo'));

        // Grundangaben
        $tpl->assign('Name',                        $this->dsGeneral('Name'));
        $tpl->assign('Optional',                    $this->dsGeneral('Optional'));
        $tpl->assign('Street',                      $this->dsGeneral('Street'));
        $tpl->assign('StreetNr',                    $this->dsGeneral('StreetNr'));
        $tpl->assign('PLZ',                         $this->dsGeneral('PLZ'));
        $tpl->assign('Place',                       $this->dsGeneral('Place'));
        $tpl->assign('Country',                     $this->dsGeneral('Country'));

        // Kontaktmöglichkeiten
        $tpl->assign('ContactEMail',                $this->dsGeneral('ContactEMail'));
        $tpl->assign('PrivacyEMail',                $this->dsGeneral('PrivacyEMail'));
        $tpl->assign('Phone',                       $this->dsGeneral('Phone'));
        $tpl->assign('Fax',                         $this->dsGeneral('Fax'));
        $tpl->assign('ContactForm',                 $this->dsGeneral('ContactForm'));
        $tpl->assign('MoreContactOptions',          $this->dsGeneral('MoreContactOptions'));

        // Firmeneinstellungen
        $tpl->assign('AuthorizedToRepresent',       $this->dsGeneral('AuthorizedToRepresent'));
        $tpl->assign('UstID',                       $this->dsGeneral('UstID'));
        $tpl->assign('WidNr',                       $this->dsGeneral('WidNr'));
        $tpl->assign('BusinessArea',                $this->dsGeneral('BusinessArea'));
        $tpl->assign('AGBUrl',                      $this->dsGeneral('AGBUrl'));
        $tpl->assign('RegistryCourt',               $this->dsGeneral('RegistryCourt'));
        $tpl->assign('RegistryCourtNr',             $this->dsGeneral('RegistryCourtNr'));

        // Zusatztexte für das Impressum
        $tpl->assign('ConsumerDisputeResolution',   $this->dsGeneral('ConsumerDisputeResolution'));
        $tpl->assign('SocialMedia',                 $this->dsGeneral('SocialMedia'));
        $tpl->assign('CopyrightNotices',            $this->dsGeneral('CopyrightNotices'));
        $tpl->assign('PhotoCredits',                $this->dsGeneral('PhotoCredits'));

        // assign
        $tpl->assign('pageURL',                     $this->_adminLink());
        $tpl->assign('MSG',                         $MSG);
        $tpl->assign('page',                        $this->_templatePath('datenschutz.acp.general.tpl'));
    }


    /**
     * Datenschutz
     */
    function _dsText() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        // language given?
        $selectedLang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'deutsch';


        // Datenschutztext bearbeiten
        $MSG = NULL;
        if ((isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit') && isset($_REQUEST['id'])) {
            $MSG  = '<div class="infostate-error">';
            if (empty($_REQUEST['title'])) {
                $MSG .= $lang_admin['dsMSGTitel'] .'<br />';
                $ERROR = true;
            }
            if (empty($_REQUEST['text'])) {
                $MSG .= $lang_admin['dsMSGText'] .'<br />';
                $ERROR = true;
            }
            $MSG .= '</div>';

            if ($ERROR == FALSE) {
                $db->Query('UPDATE {pre}mod_ds_nli SET `Sortierung`=?,`Lang`=?,`Title`=?,`Content`=? WHERE `ID`=?',
                    $_REQUEST['sortierung'],
                    $_REQUEST['lang'],
                    $_REQUEST['title'],
                    $this->cleanText($_REQUEST['text']),
                    (int)$_REQUEST['id']);

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGTextSave'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Datenschutztext anlegen
        $MSG = NULL;
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'text') && (isset($_REQUEST['action']) && $_REQUEST['action'] == 'add')) {
            $MSG  = '<div class="infostate-error">';
            if (empty($_REQUEST['title'])) {
                $MSG .= $lang_admin['dsMSGTitel'] .'<br />';
                $ERROR = true;
            }
            if (empty($_REQUEST['text'])) {
                $MSG .= $lang_admin['dsMSGText'] .'<br />';
                $ERROR = true;
            }
            $MSG .= '</div>';

            if ($ERROR == FALSE) {
                $db->Query('INSERT INTO {pre}mod_ds_nli (`Sortierung`,`Lang`,`Title`,`Content`) VALUES(?,?,?,?)',
                    $_REQUEST['sortierung'],
                    $selectedLang,
                    $_REQUEST['title'],
                    $_REQUEST['text']);

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGTextSave'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Datenschutztext löschen
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'text') && isset($_REQUEST['dsdel'])) {
            $db->Query('DELETE FROM {pre}mod_ds_nli WHERE `ID`=?', (int)$_REQUEST['dsdel']);

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsMSGTextDelSave'] .' <br />';
            $MSG .= '</div>';
        }

        // Anleitungen
        $contents = array();
        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_nli WHERE Lang = "'. $selectedLang .'" ORDER BY `Sortierung`');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $dscontent[$row['ID']] = array(
                "ID"            => $row['ID'],
                "Sortierung"    => $row['Sortierung'],
                "Lang"          => $row['Lang'],
                "Title"         => $row['Title'],
                "Content"       => $row['Content']
            );
        }
        $rescon->Free();

        // Impressum bearbeiten
        $MSG = NULL;
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'text') && isset($_REQUEST['imprsave'])) {
            $db->Query('UPDATE {pre}mod_ds_impressum SET `Content`=? WHERE `Lang`=? AND `Text`=?', $_REQUEST['ImpressumContent'], $selectedLang, 'ImpressumContent');
            $db->Query('UPDATE {pre}mod_ds_impressum SET `Content`=? WHERE `Lang`=? AND `Text`=?', $_REQUEST['ConsumerDisputeResolution'], $selectedLang, 'ConsumerDisputeResolution');
            $db->Query('UPDATE {pre}mod_ds_impressum SET `Content`=? WHERE `Lang`=? AND `Text`=?', $_REQUEST['SocialMedia'], $selectedLang, 'SocialMedia');
            $db->Query('UPDATE {pre}mod_ds_impressum SET `Content`=? WHERE `Lang`=? AND `Text`=?', $_REQUEST['CopyrightNotices'], $selectedLang, 'CopyrightNotices');
            $db->Query('UPDATE {pre}mod_ds_impressum SET `Content`=? WHERE `Lang`=? AND `Text`=?', $_REQUEST['PhotoCredits'], $selectedLang, 'PhotoCredits');

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsMSGImprSave'] .' <br />';
            $MSG .= '</div>';
        }

        // Impressum
        $contents = array();
        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_impressum WHERE Lang = "'. $selectedLang .'"');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $impressum[$row['ID']] = array(
                "ID"            => $row['ID'],
                "Lang"          => $row['Lang'],
                "Text"          => $row['Text'],
                "Title"         => $lang_admin['ds'. $row['Text']],
                "Desc"          => $lang_admin['ds'. $row['Text'] .'Desc'],
                "Content"       => $row['Content']
            );
        }
        $rescon->Free();

        // get available languages
        $languages = GetAvailableLanguages();


        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('languages',       $languages);
        $tpl->assign('selectedLang',    $selectedLang);
        $tpl->assign('usertpldir',      B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('DSContent',       $dscontent);
        $tpl->assign('Impressum',       $impressum);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.text.tpl'));
    }


    /**
     * Datenschutz erstellen
     */
    function _dsTextDatenschutzAdd() {
        global $tpl;

        // language given?
        $selectedLang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'deutsch';

        // get available languages
        $languages = GetAvailableLanguages();

        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('languages',       $languages);
        $tpl->assign('selectedLang',    $selectedLang);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.textdsadd.tpl'));
    }


    /**
     * Datenschutz bearbeiten
     */
    function _dsTextDatenschutzEdit() {
        global $tpl, $db, $bm_prefs;

        // language given?
        $selectedLang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'deutsch';

        // Anleitungen
        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_nli WHERE ID = "'. $_REQUEST['id'] .'" LIMIT 1');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('ID',           $row['ID']);
            $tpl->assign('Sortierung',   $row['Sortierung']);
            $tpl->assign('Lang',         $row['Lang']);
            $tpl->assign('Title',        $row['Title']);
            $tpl->assign('Content',      $row['Content']);
        }
        $rescon->Free();

        // get available languages
        $languages = GetAvailableLanguages();

        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('languages',       $languages);
        $tpl->assign('selectedLang',    $selectedLang);
        $tpl->assign('usertpldir',      B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.textdsedit.tpl'));
    }


    /**
     * Reports
     */
    function _reportsPage() {
        global $tpl, $db, $lang_admin, $lang_custom;

        // Report akzeptieren
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'reports') && (isset($_REQUEST['do2']) && $_REQUEST['do2'] == 'accept') && isset($_REQUEST['id']) && isset($_REQUEST['userid'])) {
            $db->Query('UPDATE {pre}mod_ds_report SET `Status`=?, `JobUpdate`=? WHERE `ID`=?',
                2,
                date('Y-m-d H:i:s'),
                $_REQUEST['id']);
        }

        // Datei Downloaden
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'reports') && (isset($_REQUEST['do2']) && $_REQUEST['do2'] == 'download') && isset($_REQUEST['id'])) {
            $res = $db->Query('SELECT * FROM {pre}mod_ds_report WHERE ID =?',
                $_REQUEST['id']);
            while($row = $res->FetchArray(MYSQLI_ASSOC)) {
                header("Content-Type: application/pdf");
                header("Content-Length: " . $row['DokumentSize']);
                header("Content-Disposition: attachment; filename=ReportDownload.pdf");

                echo $row['Dokument'];
                exit();
            }
            $res->Free();
        }

        // Report löschen
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'reports') && isset($_REQUEST['delete'])) {
            $db->Query('DELETE FROM {pre}mod_ds_report WHERE `ID`=?',
                (int)$_REQUEST['delete']);
        }

        // Reports ausgeben
        $contents = array();

        // Anzahl
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'reports') && (isset($_REQUEST['show']) && $_REQUEST['show'] == 'all')) {
            $DBShow = '';
            $tpl->assign('ShowContent', 'new');
            $tpl->assign('ShowContentText', $lang_admin['dsReportShowNew']);
        } else {
            $DBShow = ' WHERE Status > 0';
            $tpl->assign('ShowContent', 'all');
            $tpl->assign('ShowContentText', $lang_admin['dsReportShowAll']);
        }

        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_report '. $DBShow .' ORDER BY `ID` DESC');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {

            if ($row['Status'] == 0) {
                $status         = 'datenschutz_grey.png';
                $status_text    = $lang_admin['dsStatusTextEnd'];
            } else if ($row['Status'] == 1) {
                $status         = 'datenschutz_yellow.png';
                $status_text    = $lang_admin['dsStatusTextPlan'];
            } else if ($row['Status'] == 2) {
                $status         = 'datenschutz_yellow_accepted.png';
                $status_text    = $lang_admin['dsStatusTextAcce'];
            } else {
                $status         = 'datenschutz_green.png';
                $status_text    = $lang_admin['dsStatusTextLauf'];
            }

            if (isset($row['JobDate'])) {
                $jobdate = date('d.m.Y H:i:s', strtotime($row['JobDate']));
            } else {
                $jobdate = NULL;
            }
            if (isset($row['JobUpdate'])) {
                $jobupdate = date('d.m.Y H:i:s', strtotime($row['JobUpdate']));
            } else {
                $jobupdate = NULL;
            }

            $contents[$row['ID']] = array(
                "ID"              => $row['ID'],
                "UserID"          => $row['UserID'],
                "UserInfo"        => $this->UserInfos($row['UserID']) .' ('. $row['UserID'] .')',
                "Status"          => $status,
                "StatusID"        => $row['Status'],
                "StatusText"      => $status_text,
                "JobDate"         => $jobdate,
                "JobUpdate"       => $jobupdate,
                "Aktion"          => $row['Dokument'],
                "DokumentSize"    => $this->showFilesize($row['DokumentSize'], 2)
            );
        }
        $rescon->Free();

        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('Content',         $contents);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.reports.tpl'));
    }


    /**
     * Module
     */
    function _modulePage() {
        global $tpl, $mysql, $db, $bm_prefs, $lang_admin;

        $MSG = NULL;

        // Tabellen löschen
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'module') && isset($_REQUEST['delete'])) {
            $db->Query('DELETE FROM {pre}mod_ds_module WHERE `ID`=?',
                (int)$_REQUEST['delete']);
        }

        // Tabellen ausgeben
        $dbtabellen = array();
        $rescon = $db->Query('SHOW TABLES FROM `' . $mysql['db'] . '`');
        while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $dbtabellen[] = $row;
        }
        $rescon->Free();

        // Benutzerdaten laden
        $res = $db->Query('SELECT `id`, `email`, `vorname`, `nachname` FROM bm60_users');
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $allusers[$row['id']] = array(
                "id"                => $row['id'],
                "email"             => $row['email'],
                "vorname"           => $row['vorname'],
                "nachname"          => $row['nachname'],
            );
        }
        $res->Free();

        // Tabelle abfragen
        if ((isset($_REQUEST['addname']) && $_REQUEST['addname'] !== '') && isset($_REQUEST['addtabelle']) || (isset($_GET['step']) && $_GET['step'] == 2)) {
            if (isset($_REQUEST['addmoduelsave'])) {
                $show = 'step3';

                $db->Query('INSERT INTO {pre}mod_ds_module (`Name`,`DBTabelle`,`DBSearchTabelle`,`DBSearchType`,`Spalten`) VALUES(?,?,?,?,?)',
                    $_REQUEST['name'],
                    $_REQUEST['tabelle'],
                    $_REQUEST['addusertabelle'],
                    $_REQUEST['addusertype'],
                    serialize($_REQUEST['addspalten']));

                $MSG = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGKonf'] . ' <br />';
                $MSG .= '</div>';

            } else {
                $show = 'step2';
                if (isset($_GET['step'])) {
                    $ModuleName = $_GET['name'];
                    $DBSearch = $_GET['tabelle'];
                } else {
                    $ModuleName = $_REQUEST['addname'];
                    $DBSearch = $_REQUEST['addtabelle'];
                }

                // Spalten ausgeben
                $dbspalten = array();
                $rescon = $db->Query('SHOW COLUMNS FROM `' . $DBSearch . '`');
                while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                    if ($row['Field'] == 'id' || $row['Field'] == 'ID') {
                        // id nicht ausgeben
                    } else {
                        $dbspalten[] = $row;
                    }
                }
                $rescon->Free();

                // Tabelle für PDF Ausgeben
                if ((isset($_REQUEST['addusertype']) && $_REQUEST['addusertype'] == 'userid')) {
                    $DBShowUserSearch   = $_REQUEST['adduser'];
                } else {
                    if (isset($_REQUEST['adduser'])) {
                        $DBShowUserSearch = $this->UserEMail($_REQUEST['adduser']);
                    }
                }

                if (isset($_REQUEST['addusertabelle']) && $_REQUEST['addusertabelle'] !== '') {
                    $DBUserTabelle = " WHERE ". $_REQUEST['addusertabelle'] ." = '". $DBShowUserSearch ."'";
                } else {
                    $DBUserTabelle = NULL;
                }
                $i = 0;
                $res = $db->Query('SELECT * FROM ' . $DBSearch . $DBUserTabelle);
                while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                    if (isset($_REQUEST['addspalten'])) {
                        foreach ($_REQUEST['addspalten'] as $spalten) {
                            if ($this->isTimeStamp($row[$spalten]) == 1) {
                                $tso[$i][$spalten] = date('d.m.Y H:i:s', strtotime($row[$spalten]));
                            } else if ($this->isTimeStamp($row[$spalten]) == 2) {
                                $tso[$i][$spalten] = $this->timestampToDate($row[$spalten]);
                            } else {
                                $tso[$i][$spalten] = $row[$spalten];
                            }
                        }
                    }
                    $i++;
                }
                $res->Free();

                $tpl->assign('AllUsers',                    $allusers);
                if (isset($_REQUEST['adduser'])) {
                    $tpl->assign('AddUser', $_REQUEST['adduser']);
                } else {
                    $tpl->assign('AddUser', '');
                }
                $tpl->assign('AddName',                     $ModuleName);
                $tpl->assign('AddTabelle',                  $DBSearch);
                $tpl->assign('URLTabelle',                  $DBSearch);
                $tpl->assign('DBSpalten',                   $dbspalten);
                if (isset($_REQUEST['addusertabelle'])) {
                    $tpl->assign('AddUserTabelle', $_REQUEST['addusertabelle']);
                } else {
                    $tpl->assign('AddUserTabelle', '');
                }
                if (isset($_REQUEST['addusertype'])) {
                    $tpl->assign('AddUserType', $_REQUEST['addusertype']);
                } else {
                    $tpl->assign('AddUserType', '');
                }
                if (isset($_REQUEST['addspalten'])) {
                    $tpl->assign('AddUserTabelleSpalten', $_REQUEST['addspalten']);
                } else {
                    $tpl->assign('AddUserTabelleSpalten', '');
                }
                if (isset($tso) && is_array($tso)) {
                    $tpl->assign('AddUserTabelleSpaltenOutput', $tso);
                } else {
                    $tpl->assign('AddUserTabelleSpaltenOutput', '');
                }
            }
        } else {
            $show = 'step1';
            if (isset($_REQUEST['addname'])) {
                $tpl->assign('AddName', $_REQUEST['addname']);
            } else {
                $tpl->assign('AddName', '');
            }
            if (isset($_REQUEST['addtabelle'])) {
                $tpl->assign('AddTabelle',      $_REQUEST['addtabelle']);
            } else {
                $tpl->assign('AddTabelle', '');
            }
        }

        // Module ausgeben
        $contents = array();
        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_module ORDER BY `Name`');
        while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $spalten = unserialize($row['Spalten']);
            $contents[$row['ID']] = array(
                "ID"        => $row['ID'],
                "Name"      => $row['Name'],
                "DBTabelle" => $row['DBTabelle'],
                "Spalten"   => unserialize($row['Spalten'])
            );
        }
        $rescon->Free();

        // Externe Module
        $module = NULL;
        if (file_exists($bm_prefs['selffolder'] . 'plugins/dsmodule/')) {
            foreach (@scandir($_SERVER['DOCUMENT_ROOT'] . '/plugins/dsmodule/') as $dsModule) {
                if ($dsModule !== '.' && $dsModule !== '..') {
                    $module[] = '/plugins/dsmodule/' . $dsModule;
                }
            }
        }


        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('Step',            $show);
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('DBTabellen',      $dbtabellen);
        $tpl->assign('Content',         $contents);
        $tpl->assign('Module',          $module);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.module.tpl'));
    }


    /**
     * Datenschutz
     */
    function _avPage() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        // language given?
        $selectedLang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'deutsch';

        // Speichern
        $MSG    = NULL;
        $ERROR  = FALSE;
        if ($_REQUEST['do'] == 'av' && isset($_REQUEST['edit'])) {
            $MSG = '<div class="infostate-error">';
            if (empty($_REQUEST['title'])) {
                $MSG .= $lang_admin['dsMSGTitel'] .'<br />';
                $ERROR = TRUE;
            }
            if (empty($_REQUEST['text'])) {
                $MSG .= $lang_admin['dsMSGText'] .'<br />';
                $ERROR = TRUE;
            }
            $MSG .= '</div>';

            if ($ERROR == FALSE) {
                $db->Query('UPDATE {pre}mod_ds_av SET `Lang`=?,`Title`=?,`Content`=? WHERE `Lang`=?',
                    $_REQUEST['lang'],
                    $_REQUEST['title'],
                    $this->cleanText($_REQUEST['text']),
                    $selectedLang);

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGSave'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Datenschutztext anlegen
        $MSG = NULL;
        if ($_REQUEST['do'] == 'av' && isset($_REQUEST['add'])) {
            $MSG = '<div class="infostate-error">';
            if (empty($_REQUEST['title'])) {
                $MSG .= $lang_admin['dsMSGTitel'] .'<br />';
            }
            if (empty($_REQUEST['text'])) {
                $MSG .= $lang_admin['dsMSGText'] .'<br />';
            }
            $MSG .= '</div>';

            if ($ERROR == FALSE) {
                $db->Query('INSERT INTO {pre}mod_ds_av (`Lang`,`Title`,`Content`) VALUES(?,?,?)',
                    $selectedLang,
                    $_REQUEST['title'],
                    $this->cleanText($_REQUEST['text']));

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGSave'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Anleitungen
        $contents = array();
        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_av WHERE Lang = ?',
            $selectedLang);
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('AVID',        $row['ID']);
            $tpl->assign('AVLang',      $row['Lang']);
            $tpl->assign('AVTitle',     $row['Title']);
            $tpl->assign('AVContent',   $row['Content']);
        }
        $rescon->Free();

        // get available languages
        $languages = GetAvailableLanguages();


        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('languages',       $languages);
        $tpl->assign('selectedLang',    $selectedLang);
        $tpl->assign('usertpldir',      B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.av.tpl'));
    }


    /**
     * Reports
     */
    function _avuserPage() {
        global $tpl, $db, $lang_admin, $lang_custom;

        // Datei Downloaden
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'avuser') && (isset($_REQUEST['do2']) && $_REQUEST['do2'] == 'download') && isset($_REQUEST['id'])) {
            $res = $db->Query('SELECT * FROM {pre}mod_ds_av_user WHERE ID =?',
                $_REQUEST['id']);
            while($row = $res->FetchArray(MYSQLI_ASSOC)) {
                header("Content-Type: application/pdf");
                header("Content-Length: " . $row['DokumentSize']);
                header("Content-Disposition: attachment; filename=ReportDownload.pdf");

                echo $row['Dokument'];
                exit();
            }
            $res->Free();
        }

        // Report löschen
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'avuser') && isset($_REQUEST['delete'])) {
            $db->Query('DELETE FROM {pre}mod_ds_av_user WHERE `ID`=?',
                (int)$_REQUEST['delete']);
        }

        // Reports ausgeben
        $contents = array();

        // Anzahl
        if ((isset($_REQUEST['do']) && $_REQUEST['do'] == 'avuser') && (isset($_REQUEST['show']) && $_REQUEST['show'] == 'all')) {
            $DBShow = '';
            $tpl->assign('ShowContent', 'new');
            $tpl->assign('ShowContentText', $lang_admin['dsReportShowNew']);
        } else {
            $DBShow = ' LIMIT 25';
            $tpl->assign('ShowContent', 'all');
            $tpl->assign('ShowContentText', $lang_admin['dsReportShowAll']);
        }

        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_av_user ORDER BY `ID` DESC '. $DBShow);
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {

            if (isset($row['DocDate'])) {
                $docdate = date('d.m.Y H:i:s', strtotime($row['DocDate']));
            } else {
                $docdate = NULL;
            }

            $contents[$row['ID']] = array(
                "ID"              => $row['ID'],
                "UserID"          => $row['UserID'],
                "UserInfo"        => $this->UserInfos($row['UserID']) .' ('. $row['UserID'] .')',
                "DocDate"         => $docdate,
                "Dokument"        => $row['Dokument'],
                "DokumentSize"    => $this->showFilesize($row['DokumentSize'], 2)
            );
        }
        $rescon->Free();

        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('Content',         $contents);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.avuser.tpl'));
    }


    /**
     * Cookie
     */
    function _cookiePage() {
        global $tpl, $db, $lang_admin, $bm_prefs;

        $MSG        = NULL;
        $languages  = GetAvailableLanguages();

        // Speichern
        if (isset($_REQUEST['cookiesave'])) {
            if ($_REQUEST['cookienliaktivieren'] == 'on') {
                $_REQUEST['cookienliaktivieren'] = 'yes';
            } else {
                $_REQUEST['cookienliaktivieren'] = 'no';
            }

            if ($_REQUEST['cookieliaktivieren'] == 'on') {
                $_REQUEST['cookieliaktivieren'] = 'yes';
            } else {
                $_REQUEST['cookieliaktivieren'] = 'no';
            }

            if ($_REQUEST['javascriptdenycookie'] == 'on') {
                $_REQUEST['javascriptdenycookie'] = 'yes';
            } else {
                $_REQUEST['javascriptdenycookie'] = 'no';
            }

            foreach ($languages as $key => $value) {
                $_REQUEST['cookietext'][$key]       = $_REQUEST[$key .'_cookietext'];
                $_REQUEST['cookieclose'][$key]      = $_REQUEST[$key .'_cookieclose'];
                $_REQUEST['cookieallow'][$key]      = $_REQUEST[$key .'_cookieallow'];
                $_REQUEST['cookielinktext'][$key]   = $_REQUEST[$key .'_cookielinktext'];
            }

            $db->Query('UPDATE {pre}mod_ds_cookie SET `StatusNLI`=?,`StatusLI`=?,`SystemCookie`=?,`JavaScriptAllow`=?,`JavaScriptDeny`=?,`JavaScriptDenyCookie`=?,`DesignBG`=?,`DesignFG`=?,`DesignLinkBG`=?,`DesignLinkFG`=?,`DesignTheme`=?,`DesignPosition`=?,`DesignType`=?,`CookieText`=?,`CookieClose`=?,`CookieAllow`=?,`CookieLinkText`=?,`CookieLink`=? WHERE `ID`=?',
                $_REQUEST['cookienliaktivieren'],
                $_REQUEST['cookieliaktivieren'],
                $_REQUEST['systemcookies'],
                $_REQUEST['javascriptallow'],
                $_REQUEST['javascriptdeny'],
                $_REQUEST['javascriptdenycookie'],
                $_REQUEST['designbg'],
                $_REQUEST['designfg'],
                $_REQUEST['designlinkbg'],
                $_REQUEST['designlinkfg'],
                $_REQUEST['designtheme'],
                $_REQUEST['designposition'],
                $_REQUEST['designtype'],
                serialize($_REQUEST['cookietext']),
                serialize($_REQUEST['cookieclose']),
                serialize($_REQUEST['cookieallow']),
                serialize($_REQUEST['cookielinktext']),
                $_REQUEST['cookielink'],
                0);

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsMSGCookieKonf'] .' <br />';
            $MSG .= '</div>';
        }


        $res = $db->Query('SELECT * FROM {pre}mod_ds_cookie WHERE ID = ?',
            0);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('CookieNLIAktivieren',         $row['StatusNLI']);
            $tpl->assign('CookieLIAktivieren',          $row['StatusLI']);
            $tpl->assign('CookieSystem',                $row['SystemCookie']);
            $tpl->assign('CookieJavaScriptAllow',       $row['JavaScriptAllow']);
            $tpl->assign('CookieJavaScriptDeny',        $row['JavaScriptDeny']);
            $tpl->assign('CookieJavaScriptDenyCookie',  $row['JavaScriptDenyCookie']);
            $tpl->assign('CookieDesignBG',              $row['DesignBG']);
            $tpl->assign('CookieDesignFG',              $row['DesignFG']);
            $tpl->assign('CookieDesignLinkBG',          $row['DesignLinkBG']);
            $tpl->assign('CookieDesignLinkFG',          $row['DesignLinkFG']);
            $tpl->assign('CookieDesignTheme',           $row['DesignTheme']);
            $tpl->assign('CookieDesignPosition',        $row['DesignPosition']);
            $tpl->assign('CookieDesignType',            $row['DesignType']);
            $tpl->assign('CookieText',                  unserialize($row['CookieText']));
            $tpl->assign('CookieClose',                 unserialize($row['CookieClose']));
            $tpl->assign('CookieAllow',                 unserialize($row['CookieAllow']));
            $tpl->assign('CookieLinkText',              unserialize($row['CookieLinkText']));
            $tpl->assign('CookieLink',                  $row['CookieLink']);
        }
        $res->Free();

        // assign
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('Languages',       $languages);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.cookie.tpl'));
    }


    /**
     * Deref
     */
    function _derefPage() {
        global $tpl, $db, $lang_admin, $bm_prefs;

        $MSG        = NULL;

        // Speichern
        if (isset($_REQUEST['derefsave'])) {
            if ($_REQUEST['derefstatus'] == 'on') {
                $_REQUEST['derefstatus'] = 'yes';
            } else {
                $_REQUEST['derefstatus'] = 'no';
            }

            $db->Query('UPDATE {pre}mod_ds_deref SET `Status`=?,`Template`=? WHERE `ID`=?',
                $_REQUEST['derefstatus'],
                $_REQUEST['dereftemplate'],
                0);

            $MSG  = '<div class="infostate-ok">';
            if (!file_exists($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl.bak')) {
                rename($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl', $bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl.bak');
                $MSG .= $lang_admin['dsMSGDerefBackup'] .' <br />';
            }
            file_put_contents($bm_prefs['selffolder'] .'templates/'. $bm_prefs['template'] .'/nli/deref.tpl', $_REQUEST['dereftemplate']);
            $MSG .= $lang_admin['dsMSGDeref'] .' <br />';
            $MSG .= '</div>';
        }


        $res = $db->Query('SELECT * FROM {pre}mod_ds_deref WHERE ID = ?', 0);
        while($row = $res->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('DerefStatus',     $row['Status']);
            $tpl->assign('DerefTemplate',   $row['Template']);
        }
        $res->Free();

        // assign
        $tpl->assign('pageURL',             $this->_adminLink());
        $tpl->assign('MSG',                 $MSG);
        $tpl->assign('page',                $this->_templatePath('datenschutz.acp.deref.tpl'));
    }


    /**
     * Einstellungen
     */
    function _confPage() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        $MSG    = NULL;


        // Logo löschen
        if ((isset($_REQUEST['do2']) && $_REQUEST['do2']) == 'dellogo') {
            $res = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID=?',
                0);
            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                unlink('../plugins/templates/images/'. $row['Logo']);

                $db->Query('UPDATE {pre}mod_ds_conf SET `Logo`=? WHERE `ID`=?',
                    '',
                    0);

                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGLogoDel'] .' <br />';
                $MSG .= '</div>';
            }

        }

        // Speichern
        if (isset($_REQUEST['confsave'])) {
            if ($ERROR == FALSE) {
                if ($_REQUEST['showimpressum'] == 'on') {
                    $_REQUEST['showimpressum'] = 'yes';
                } else {
                    $_REQUEST['showimpressum'] = 'no';
                }

                if ($_REQUEST['autopdfgen'] == 'on') {
                    $_REQUEST['autopdfgen'] = 'yes';
                } else {
                    $_REQUEST['autopdfgen'] = 'no';
                }

                if ($_REQUEST['pdfmails'] == 'on') {
                    $_REQUEST['pdfmails'] = 'yes';
                } else {
                    $_REQUEST['pdfmails'] = 'no';
                }

                if ($_REQUEST['pdfkalender'] == 'on') {
                    $_REQUEST['pdfkalender'] = 'yes';
                } else {
                    $_REQUEST['pdfkalender'] = 'no';
                }

                if ($_REQUEST['pdfkontakte'] == 'on') {
                    $_REQUEST['pdfkontakte'] = 'yes';
                } else {
                    $_REQUEST['pdfkontakte'] = 'no';
                }

                if ($_REQUEST['pdfnotizen'] == 'on') {
                    $_REQUEST['pdfnotizen'] = 'yes';
                } else {
                    $_REQUEST['pdfnotizen'] = 'no';
                }

                if ($_REQUEST['pdfaufgaben'] == 'on') {
                    $_REQUEST['pdfaufgaben'] = 'yes';
                } else {
                    $_REQUEST['pdfaufgaben'] = 'no';
                }

                if ($_REQUEST['pdfdaten'] == 'on') {
                    $_REQUEST['pdfdaten'] = 'yes';
                } else {
                    $_REQUEST['pdfdaten'] = 'no';
                }

                if ($_REQUEST['pdfbest'] == 'on') {
                    $_REQUEST['pdfbest'] = 'yes';
                } else {
                    $_REQUEST['pdfbest'] = 'no';
                }

                if ($_REQUEST['pdfgesetz'] == 'on') {
                    $_REQUEST['pdfgesetz'] = 'yes';
                } else {
                    $_REQUEST['pdfgesetz'] = 'no';
                }

                if ($_REQUEST['logwebaccstatus'] == 'on') {
                    $_REQUEST['logwebaccstatus'] = 'yes';
                } else {
                    $_REQUEST['logwebaccstatus'] = 'no';
                }

                if ($_REQUEST['logweberrstatus'] == 'on') {
                    $_REQUEST['logweberrstatus'] = 'yes';
                } else {
                    $_REQUEST['logweberrstatus'] = 'no';
                }

                if ($_REQUEST['logb1gcron'] == 'on') {
                    $_REQUEST['logb1gcron'] = 'yes';
                } else {
                    $_REQUEST['logb1gcron'] = 'no';
                }

                if ($_REQUEST['logb1gstatus'] == 'on') {
                    $_REQUEST['logb1gstatus'] = 'yes';
                } else {
                    $_REQUEST['logb1gstatus'] = 'no';
                }

                if ($_REQUEST['logb1garchive'] == 'on') {
                    $_REQUEST['logb1garchive'] = 'yes';
                } else {
                    $_REQUEST['logb1garchive'] = 'no';
                }

                if ($_REQUEST['logb1gscron'] == 'on') {
                    $_REQUEST['logb1gscron'] = '1';
                } else {
                    $_REQUEST['logb1gscron'] = '0';
                }

                if ($_REQUEST['logb1gsstatus'] == 'on') {
                    $_REQUEST['logb1gsstatus'] = 'yes';
                } else {
                    $_REQUEST['logb1gsstatus'] = 'no';
                }

                if ($_REQUEST['logarchivecron'] == 'on') {
                    $_REQUEST['logarchivecron'] = 'yes';
                } else {
                    $_REQUEST['logarchivecron'] = 'no';
                }

                if ($_REQUEST['logarchivestatus'] == 'on') {
                    $_REQUEST['logarchivestatus'] = '1';
                } else {
                    $_REQUEST['logarchivestatus'] = '0';
                }

                $db->Query('UPDATE {pre}mod_ds_conf SET 
                              `ShowImpressum`=?,      
                              `AutoPDFGen`=?,
                              `ReportKosten`=?,
                              `PDFMails`=?,
                              `PDFKalender`=?,
                              `PDFKontakte`=?,
                              `PDFNotizen`=?,
                              `PDFAufgaben`=?,
                              `PDFDaten`=?,
                              `PDFBest`=?,
                              `PDFGesetzestexte`=?,
                              `LogWebAccStatus`=?,
                              `LogWebAccInter`=?,
                              `LogWebAccInhalt`=?,
                              `LogWebErrStatus`=?,
                              `LogWebErrInter`=?,
                              `LogWebErrInhalt`=?,
                              `LogB1GCron`=?,
                              `LogB1GStatus`=?,
                              `LogB1GArchive`=?,
                              `LogB1GInter`=?,
                              `LogB1GInhalt`=?,
                              `LogB1GSStatus`=?,
                              `LogB1GSInhalt`=?,
                              `LogArchiveCron`=?,
                              `LogArchiveInter`=? 
                            WHERE `ID`=?',
                    $_REQUEST['showimpressum'],
                    $_REQUEST['autopdfgen'],
                    $_REQUEST['reportkosten'],
                    $_REQUEST['pdfmails'],
                    $_REQUEST['pdfkalender'],
                    $_REQUEST['pdfkontakte'],
                    $_REQUEST['pdfnotizen'],
                    $_REQUEST['pdfaufgaben'],
                    $_REQUEST['pdfdaten'],
                    $_REQUEST['pdfbest'],
                    $_REQUEST['pdfgesetz'],
                    $_REQUEST['logwebaccstatus'],
                    $_REQUEST['logwebaccinter'],
                    $_REQUEST['logwebaccinhalt'],
                    $_REQUEST['logweberrstatus'],
                    $_REQUEST['logweberrinter'],
                    $_REQUEST['logweberrinhalt'],
                    $_REQUEST['logb1gcron'],
                    $_REQUEST['logb1gstatus'],
                    $_REQUEST['logb1garchive'],
                    $_REQUEST['logb1ginter'],
                    $_REQUEST['logb1ginhalt'],
                    $_REQUEST['logb1gsstatus'],
                    $_REQUEST['logb1gsinhalt'],
                    $_REQUEST['logarchivecron'],
                    $_REQUEST['logarchiveinter'],
                    0);

                if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {
                    $db->Query('UPDATE {pre}bms_prefs SET `logs_autodelete`=?,`logs_autodelete_days`=?,`logs_autodelete_archive`=? WHERE `ID`=?',
                        $_REQUEST['logb1gscron'],
                        $_REQUEST['logb1gsinter'],
                        $_REQUEST['logarchivestatus'],
                        1);
                }

                // Profilefelder
                $db->Query('TRUNCATE TABLE {pre}mod_ds_conf_pfs');
                if(!empty($_REQUEST['pfs'])) {
                    foreach ($_REQUEST['pfs'] as $pf) {
                        $db->Query('INSERT INTO `{pre}mod_ds_conf_pfs` 
                                (`Sortierung`, `Anzeigen`, `PFID`)
                            VALUES
                                (1, \''. $_REQUEST['pfsAnzeige_'. $pf] .'\', '. $pf .');');
                    }
                }




                $MSG  = '<div class="infostate-ok">';
                $MSG .= $lang_admin['dsMSGKonf'] .' <br />';
                $MSG .= '</div>';
            }
        }

        // Profilefelder
        $respf = $db->Query('SELECT `id`, `feld` FROM {pre}profilfelder ORDER BY feld ASC');
        while($row = $respf->FetchArray(MYSQLI_ASSOC)) {
            $profilefelder[] = array(
                "id"    => $row['id'],
                "feld"  => $row['feld'],
                "show"  => $this->showPFInfo($row['id'])
            );
        }
        $respf->Free();
        $tpl->assign('DBPFs', $profilefelder);

        // B1GMail Log
        $rescon = $db->Query('SELECT * FROM {pre}logs ORDER BY ID ASC LIMIT 1');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('B1GMailLogLastDate',      date('d.m.Y H:i:s', $row['zeitstempel']));
        }
        $rescon->Free();

        // B1GMailServer Log
        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {
            $rescon = $db->Query('SELECT * FROM {pre}bms_logs ORDER BY ID ASC LIMIT 1');
            while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                $tpl->assign('B1GMailSLogLastDate', date('d.m.Y H:i:s', $row['iDate']));
            }
            $rescon->Free();
        } else {
            $tpl->assign('B1GMailSLogLastDate', '');
        }

        // Einstellungen
        $rescon = $db->Query('SELECT * FROM {pre}mod_ds_conf WHERE ID =? LIMIT 1', 0);
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $tpl->assign('ShowImpressum',       $row['ShowImpressum']);
            $tpl->assign('AutoPDFGen',          $row['AutoPDFGen']);
            $tpl->assign('ReportKosten',        $row['ReportKosten']);
            $tpl->assign('PDFMails',            $row['PDFMails']);
            $tpl->assign('PDFKalender',         $row['PDFKalender']);
            $tpl->assign('PDFKontakte',         $row['PDFKontakte']);
            $tpl->assign('PDFNotizen',          $row['PDFNotizen']);
            $tpl->assign('PDFAufgaben',         $row['PDFAufgaben']);
            $tpl->assign('PDFDaten',            $row['PDFDaten']);
            $tpl->assign('PDFBest',             $row['PDFBest']);
            $tpl->assign('PDFGesetzestexte',    $row['PDFGesetzestexte']);
            $tpl->assign('LogWebAccStatus',     $row['LogWebAccStatus']);
            $tpl->assign('LogWebAccInter',      $row['LogWebAccInter']);
            $tpl->assign('LogWebAccInhalt',     $row['LogWebAccInhalt']);
            $tpl->assign('LogWebErrStatus',     $row['LogWebErrStatus']);
            $tpl->assign('LogWebErrInter',      $row['LogWebErrInter']);
            $tpl->assign('LogWebErrInhalt',     $row['LogWebErrInhalt']);
            $tpl->assign('LogB1GStatus',        $row['LogB1GStatus']);
            $tpl->assign('LogB1GArchive',       $row['LogB1GArchive']);
            $tpl->assign('LogB1GInter',         $row['LogB1GInter']);
            $tpl->assign('LogB1GInhalt',        $row['LogB1GInhalt']);
            $tpl->assign('LogB1GCron',          $row['LogB1GCron']);
            $tpl->assign('LogB1GSInstalled',    $this->_BMModuleExists('B1GMailServerAdmin'));
            $tpl->assign('LogB1GSStatus',       $row['LogB1GSStatus']);
            $tpl->assign('LogB1GSInhalt',       $row['LogB1GSInhalt']);
            $tpl->assign('LogArchiveInter',     $row['LogArchiveInter']);
            $tpl->assign('LogArchiveCron',      $row['LogArchiveCron']);
        }
        $rescon->Free();

        // B1GMailServer
        if ($this->_BMModuleExists('B1GMailServerAdmin') == 1) {
            $rescon = $db->Query('SELECT * FROM {pre}bms_prefs WHERE ID =? LIMIT 1', 1);
            while ($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
                $tpl->assign('LogB1GSInter',    $row['logs_autodelete_days']);
                $tpl->assign('LogB1GSCron',     $row['logs_autodelete']);
                $tpl->assign('LogB1GSArchive',  $row['logs_autodelete_archive']);
            }
            $rescon->Free();
        }

        // Plugin: MailSync
        if ($this->_BMModuleExists('MailSync') == 1) {
            $tpl->assign('LogMailSync',    'on');
        }

        // assign
        $tpl->assign('b1gVersion',      B1GMAIL_VERSION);
        $tpl->assign('usertpldir',      B1GMAIL_REL . 'templates/' . $bm_prefs['template'] . '/');
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.conf.tpl'));
    }


    /**
     * Einstellungen
     */
    function _pflichtPage() {
        global $tpl, $db, $bm_prefs, $lang_admin;

        $MSG = NULL;
        if (isset($_REQUEST['mg'])) {
            $MailGruppe = $_REQUEST['mg'];
        } else {
            $MailGruppe = NULL;
        }
        $MailGruppen    = NULL;

        // Mail Gruppen
        $rescon = $db->Query('SELECT * FROM {pre}gruppen ORDER BY titel');
        while($row = $rescon->FetchArray(MYSQLI_ASSOC)) {
            $MailGruppen[$row['id']] = array(
                "ID" => $row['id'],
                "Titel" => $row['titel']
            );
        }
        $rescon->Free();

        if (isset($MailGruppe)) {
            // Speichern
            if (isset($_REQUEST['pflichtsave'])) {
                $db->Query('DELETE FROM {pre}mod_ds_pflicht WHERE GruppenID = ?', $MailGruppe);
                foreach ($_REQUEST as $key => $value) {
                    $notsave = array('pflichtsave', 'plugin', 'do', 'mg', 'sid');
                    if (!in_array($key, $notsave)) {
                        if (is_numeric($key) && $value !== '') { $value = 'no'; }
                        $db->Query('INSERT INTO `{pre}mod_ds_pflicht` 
                                (`GruppenID`, `FeldName`, `Status`)
                            VALUES
                                ('. $MailGruppe .', \''. $key .'\', \''. $value .'\');');
                    }
                }
            }

            // Felder
            $tpl->assign('f_anrede_status',             $this->showPflichtOption($MailGruppe, 'f_anrede'));
            $tpl->assign('f_vorname_status',            $this->showPflichtOption($MailGruppe, 'f_vorname'));
            $tpl->assign('f_nachname_status',           $this->showPflichtOption($MailGruppe, 'f_nachname'));
            $tpl->assign('f_strasse_status',            $this->showPflichtOption($MailGruppe, 'f_strasse'));
            $tpl->assign('f_telefon_status',            $this->showPflichtOption($MailGruppe, 'f_telefon'));
            $tpl->assign('f_fax_status',                $this->showPflichtOption($MailGruppe, 'f_fax'));
            $tpl->assign('f_alternativ_status',         $this->showPflichtOption($MailGruppe, 'f_alternativ'));
            $tpl->assign('f_mail2sms_nummer_status',    $this->showPflichtOption($MailGruppe, 'f_mail2sms_nummer'));

            // Profilefelder
            $i = 6;
            $res = $db->Query('SELECT * FROM {pre}profilfelder ORDER BY feld ASC');
            while ($row = $res->FetchArray(MYSQLI_ASSOC)) {
                if ($i % 2 != 0) {
                    $class = 'td2';
                } else {
                    $class = 'td1';
                }
                $profilefelder[] = array(
                    "class"     => $class,
                    "id"        => $row['id'],
                    "feld"      => $row['feld'],
                    "pflicht"   => $row['pflicht'],
                    "status"    => $this->showPflichtOption($MailGruppe, $row['id'])
                );
                $i++;
            }
            $res->Free();
        }

        // assign
        $tpl->assign('MailGruppe',      $MailGruppe);
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('MailGruppen',     $MailGruppen);
        $tpl->assign('DBPFs',           $profilefelder);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.pflicht.tpl'));
    }


    /**
     * Hilfe
     */
    function _hilfePage() {
        global $tpl, $db, $lang_admin, $bm_prefs;

        $MSG = NULL;
        $languages  = GetAvailableLanguages();

        // Einstellungen
        $NotifyMail = $this->dsGeneral('PrivacyEMail');

        // Datenschutz importieren
        if ((isset($_REQUEST['import']) && $_REQUEST['import'] == 'ds')) {
            // Alte einträge löschen
            $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_nli');

            // Neue einträge importieren
            $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_nli` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Sortierung` int(11) NOT NULL,
                      `Lang` varchar(10) DEFAULT NULL,
                      `Title` varchar(200) DEFAULT NULL,
                      `Content` text,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");
            $db->Query('INSERT IGNORE INTO `bm60_mod_ds_nli` (`ID`, `Sortierung`, `Lang`, `Title`, `Content`) VALUES
                        (1, 10, \'deutsch\', \'Ihre Betroffenenrechte\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Unter den angegebenen Kontaktdaten unseres Datenschutzbeauftragten k&ouml;nnen Sie jederzeit folgende Rechte aus&uuml;ben:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auskunft &uuml;ber Ihre bei uns gespeicherten Daten und deren Verarbeitung,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Berichtigung unrichtiger personenbezogener Daten,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">L&ouml;schung Ihrer bei uns gespeicherten Daten,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Einschr&auml;nkung der Datenverarbeitung, sofern wir Ihre Daten aufgrund gesetzlicher Pflichten noch nicht l&ouml;schen d&uuml;rfen,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Widerspruch gegen die Verarbeitung Ihrer Daten bei uns und</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Daten&uuml;bertragbarkeit, sofern Sie in die Datenverarbeitung eingewilligt haben oder einen Vertrag mit uns abgeschlossen haben.</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Sofern Sie uns eine Einwilligung erteilt haben, k&ouml;nnen Sie diese jederzeit mit Wirkung f&uuml;r die Zukunft widerrufen.<br /><br />Sie k&ouml;nnen sich jederzeit mit einer Beschwerde an die f&uuml;r Sie zust&auml;ndige Aufsichtsbeh&ouml;rde wenden. Ihre zust&auml;ndige Aufsichtsbeh&ouml;rde richtet sich nach dem Bundesland Ihres Wohnsitzes, Ihrer Arbeit oder der mutmasslichen Verletzung.</span>\'),
                        (2, 20, \'deutsch\', \'Zwecke der Datenverarbeitung durch die verantwortliche Stelle und Dritte\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wir verarbeiten Ihre personenbezogenen Daten nur zu den in dieser Datenschutzerkl&auml;rung genannten Zwecken. Eine &uuml;bermittlung Ihrer pers&ouml;nlichen Daten an Dritte zu anderen als den genannten Zwecken findet nicht statt. Wir geben Ihre pers&ouml;nlichen Daten nur an Dritte weiter, wenn:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Sie Ihre ausdr&uuml;ckliche Einwilligung dazu erteilt haben,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">die Verarbeitung zur Abwicklung eines Vertrags mit Ihnen erforderlich ist,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">die Verarbeitung zur Erf&uuml;llung einer rechtlichen Verpflichtung erforderlich ist,</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">die Verarbeitung zur Wahrung berechtigter Interessen erforderlich ist und kein Grund zur Annahme besteht, dass Sie ein &uuml;berwiegendes schutzw&uuml;rdiges Interesse an der Nichtweitergabe Ihrer Daten haben.­</span>\'),
                        (3, 30, \'deutsch\', \'L&ouml;schung bzw. Sperrung der Daten\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wir halten uns an die Grunds&auml;tze der Datenvermeidung und Datensparsamkeit. Wir speichern Ihre personenbezogenen Daten daher nur so lange, wie dies zur Erreichung der hier genannten Zwecke erforderlich ist oder wie es die vom Gesetzgeber vorgesehenen vielf&auml;ltigen Speicherfristen vorsehen. Nach Fortfall des jeweiligen Zweckes bzw. Ablauf dieser Fristen werden die entsprechenden Daten routinem&auml;ssig und entsprechend den gesetzlichen Vorschriften gesperrt oder gel&ouml;scht.­</span>\'),
                        (4, 40, \'deutsch\', \'Erfassung allgemeiner Informationen beim Besuch unserer Website\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wenn Sie auf unsere Website zugreifen, werden automatisch mittels eines Cookies Informationen allgemeiner Natur erfasst. Diese Informationen (Server-Logfiles) beinhalten etwa die Art des Webbrowsers, das verwendete Betriebssystem, den Domainnamen Ihres Internet-Service-Providers und &auml;hnliches. Hierbei handelt es sich ausschliesslich um Informationen, welche keine R&uuml;ckschl&uuml;sse auf Ihre Person zulassen.<br /><br />Diese Informationen sind technisch notwendig, um von Ihnen angeforderte Inhalte von Webseiten korrekt auszuliefern und fallen bei Nutzung des Internets zwingend an. Sie werden insbesondere zu folgenden Zwecken verarbeitet:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Sicherstellung eines problemlosen Verbindungsaufbaus der Website,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Sicherstellung einer reibungslosen Nutzung unserer Website,</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auswertung der Systemsicherheit und -stabilit&auml;t sowie</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">zu weiteren administrativen Zwecken.</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Die Verarbeitung Ihrer personenbezogenen Daten basiert auf unserem berechtigten Interesse aus den vorgenannten Zwecken zur Datenerhebung. Wir verwenden Ihre Daten nicht, um R&uuml;ckschl&uuml;sse auf Ihre Person zu ziehen. Empf&auml;nger der Daten sind nur die verantwortliche Stelle und ggf. Auftragsverarbeiter.<br /><br />Anonyme Informationen dieser Art werden von uns ggfs. statistisch ausgewertet, um unseren Internetauftritt und die dahinterstehende Technik zu optimieren.­</span>\'),
                        (5, 50, \'deutsch\', \'Cookies\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wie viele andere Webseiten verwenden wir auch so genannte „Cookies“. Cookies sind kleine Textdateien, die von einem Websiteserver auf Ihre Festplatte &uuml;bertragen werden. Hierdurch erhalten wir automatisch bestimmte Daten wie z. B. IP-Adresse, verwendeter Browser, Betriebssystem und Ihre Verbindung zum Internet.<br /><br />Cookies k&ouml;nnen nicht verwendet werden, um Programme zu starten oder Viren auf einen Computer zu &uuml;bertragen. Anhand der in Cookies enthaltenen Informationen k&ouml;nnen wir Ihnen die Navigation erleichtern und die korrekte Anzeige unserer Webseiten erm&ouml;glichen.<br /><br />In keinem Fall werden die von uns erfassten Daten an Dritte weitergegeben oder ohne Ihre Einwilligung eine Verkn&uuml;pfung mit personenbezogenen Daten hergestellt.<br /><br />Nat&uuml;rlich k&ouml;nnen Sie unsere Website grunds&auml;tzlich auch ohne Cookies betrachten. Internet-Browser sind regelm&auml;ssig so eingestellt, dass sie Cookies akzeptieren. Im Allgemeinen k&ouml;nnen Sie die Verwendung von Cookies jederzeit &uuml;ber die Einstellungen Ihres Browsers deaktivieren. Bitte verwenden Sie die Hilfefunktionen Ihres Internetbrowsers, um zu erfahren, wie Sie diese Einstellungen &auml;ndern k&ouml;nnen. Bitte beachten Sie, dass einzelne Funktionen unserer Website m&ouml;glicherweise nicht funktionieren, wenn Sie die Verwendung von Cookies deaktiviert haben.­</span>\'),
                        (6, 60, \'deutsch\', \'Registrierung auf unserer Webseite\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Bei der Registrierung f&uuml;r die Nutzung unserer personalisierten Leistungen werden einige personenbezogene Daten erhoben, wie Name, Anschrift, Kontakt- und Kommunikationsdaten wie Telefonnummer und E-Mail-Adresse. Sind Sie bei uns registriert, k&ouml;nnen Sie auf Inhalte und Leistungen zugreifen, die wir nur registrierten Nutzern anbieten. Angemeldete Nutzer haben zudem die M&ouml;glichkeit, bei Bedarf die bei Registrierung angegebenen Daten jederzeit zu &auml;ndern oder zu l&ouml;schen. Selbstverst&auml;ndlich erteilen wir Ihnen dar&uuml;ber hinaus jederzeit Auskunft &uuml;ber die von uns &uuml;ber Sie gespeicherten personenbezogenen Daten. Gerne berichtigen bzw. l&ouml;schen wir diese auch auf Ihren Wunsch, soweit keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Zur Kontaktaufnahme in diesem Zusammenhang nutzen Sie bitte die am Ende dieser Datenschutzerkl&auml;rung angegebenen Kontaktdaten.­</span>\'),
                        (7, 70, \'deutsch\', \'Erbringung kostenpflichtiger Leistungen\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Zur Erbringung kostenpflichtiger Leistungen werden von uns zus&auml;tzliche Daten erfragt, wie z.B. Zahlungsangaben, um Ihre Bestellung ausf&uuml;hren zu k&ouml;nnen. Wir speichern diese Daten in unseren Systemen bis die gesetzlichen Aufbewahrungsfristen abgelaufen sind.Zur Erbringung kostenpflichtiger Leistungen werden von uns zus&auml;tzliche Daten erfragt, wie z.B. Zahlungsangaben, um Ihre Bestellung ausf&uuml;hren zu k&ouml;nnen. Wir speichern diese Daten in unseren Systemen bis die gesetzlichen Aufbewahrungsfristen abgelaufen sind.­</span>\'),
                        (8, 80, \'deutsch\', \'SSL-Verschl&uuml;sselung\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Um die Sicherheit Ihrer Daten bei der &uuml;bertragung zu sch&uuml;tzen, verwenden wir dem aktuellen Stand der Technik entsprechende Verschl&uuml;sselungsverfahren (z. B. SSL) &uuml;ber HTTPS.Um die Sicherheit Ihrer Daten bei der &uuml;bertragung zu sch&uuml;tzen, verwenden wir dem aktuellen Stand der Technik entsprechende Verschl&uuml;sselungsverfahren (z. B. SSL) &uuml;ber HTTPS.­</span>\'),
                        (9, 90, \'deutsch\', \'Kommentarfunktion\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wenn Nutzer Kommentare auf unserer Website hinterlassen, werden neben diesen Angaben auch der Zeitpunkt ihrer Erstellung und der zuvor durch den Websitebesucher gew&auml;hlte Nutzername gespeichert. Dies dient unserer Sicherheit, da wir f&uuml;r widerrechtliche Inhalte auf unserer Webseite belangt werden k&ouml;nnen, auch wenn diese durch Benutzer erstellt wurden.­</span>\'),
                        (10, 100, \'deutsch\', \'Newsletter\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auf Grundlage Ihrer ausdr&uuml;cklich erteilten Einwilligung, &uuml;bersenden wir Ihnen regelm&auml;ssig unseren Newsletter bzw. vergleichbare Informationen per E-Mail an Ihre angegebene E-Mail-Adresse.<br /><br />F&uuml;r den Empfang des Newsletters ist die Angabe Ihrer E-Mail-Adresse ausreichend. Bei der Anmeldung zum Bezug unseres Newsletters werden die von Ihnen angegebenen Daten ausschliesslich f&uuml;r diesen Zweck verwendet. Abonnenten k&ouml;nnen auch &uuml;ber Umst&auml;nde per E-Mail informiert werden, die f&uuml;r den Dienst oder die Registrierung relevant sind (Beispielsweise &auml;nderungen des Newsletterangebots oder technische Gegebenheiten).<br /><br />F&uuml;r eine wirksame Registrierung ben&ouml;tigen wir eine valide E-Mail-Adresse. Um zu &uuml;berpr&uuml;fen, dass eine Anmeldung tats&auml;chlich durch den Inhaber einer E-Mail-Adresse erfolgt, setzen wir das „Double-opt-in“-Verfahren ein. Hierzu protokollieren wir die Bestellung des Newsletters, den Versand einer Best&auml;tigungsmail und den Eingang der hiermit angeforderten Antwort. Weitere Daten werden nicht erhoben. Die Daten werden ausschliesslich f&uuml;r den Newsletterversand verwendet und nicht an Dritte weitergegeben.<br /><br />Die Einwilligung zur Speicherung Ihrer pers&ouml;nlichen Daten und ihrer Nutzung f&uuml;r den Newsletterversand k&ouml;nnen Sie jederzeit widerrufen. In jedem Newsletter findet sich dazu ein entsprechender Link. Ausserdem k&ouml;nnen Sie sich jederzeit auch direkt auf dieser Webseite abmelden oder uns Ihren entsprechenden Wunsch &uuml;ber die am Ende dieser Datenschutzhinweise angegebene Kontaktm&ouml;glichkeit mitteilen.­</span>\'),
                        (11, 110, \'deutsch\', \'Kontaktformular\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Treten Sie bzgl. Fragen jeglicher Art per E-Mail oder Kontaktformular mit uns in Kontakt, erteilen Sie uns zum Zwecke der Kontaktaufnahme Ihre freiwillige Einwilligung. Hierf&uuml;r ist die Angabe einer validen E-Mail-Adresse erforderlich. Diese dient der Zuordnung der Anfrage und der anschliessenden Beantwortung derselben. Die Angabe weiterer Daten ist optional. Die von Ihnen gemachten Angaben werden zum Zwecke der Bearbeitung der Anfrage sowie f&uuml;r m&ouml;gliche Anschlussfragen gespeichert. Nach Erledigung der von Ihnen gestellten Anfrage werden personenbezogene Daten automatisch gel&ouml;scht.­</span>\'),
                        (12, 120, \'deutsch\', \'Verwendung von Google Analytics\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (folgend: Google). Google Analytics verwendet sog. „Cookies“, also Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Webseite durch Sie erm&ouml;glichen. Die durch das Cookie erzeugten Informationen &uuml;ber Ihre Benutzung dieser Webseite werden in der Regel an einen Server von Google in den USA &uuml;bertragen und dort gespeichert. Aufgrund der Aktivierung der IP-Anonymisierung auf diesen Webseiten, wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europ&auml;ischen Union oder in anderen Vertragsstaaten des Abkommens &uuml;ber den Europ&auml;ischen Wirtschaftsraum zuvor gek&uuml;rzt. Nur in Ausnahmef&auml;llen wird die volle IP-Adresse an einen Server von Google in den USA &uuml;bertragen und dort gek&uuml;rzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Webseite auszuwerten, um Reports &uuml;ber die Webseitenaktivit&auml;ten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegen&uuml;ber dem Webseitenbetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser &uuml;bermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengef&uuml;hrt.<br /><br />Die Zwecke der Datenverarbeitung liegen in der Auswertung der Nutzung der Website und in der Zusammenstellung von Reports &uuml;ber Aktivit&auml;ten auf der Website. Auf Grundlage der Nutzung der Website und des Internets sollen dann weitere verbundene Dienstleistungen erbracht werden. Die Verarbeitung beruht auf dem berechtigten Interesse des Webseitenbetreibers.<br /><br />Sie k&ouml;nnen die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen dieser Website vollumf&auml;nglich werden nutzen k&ouml;nnen. Sie k&ouml;nnen dar&uuml;ber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Webseite bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verf&uuml;gbare Browser-Plugin herunterladen und installieren: <a target=\"_blank\" href=\"http://tools.google.com/dlpage/gaoptout?hl=de\">Browser Add On zur Deaktivierung von Google Analytics</a>.<br /><br />Zus&auml;tzlich oder als Alternative zum Browser-Add-On k&ouml;nnen Sie das Tracking durch Google Analytics auf unseren Seiten unterbinden, indem Sie <a target=\"_blank\" href=\"https://www.activemind.de/datenschutz/datenschutzhinweis-generator/ergebnis/#\">diesen Link anklicken</a>. Dabei wird ein Opt-Out-Cookie auf Ihrem Ger&auml;t installiert. Damit wird die Erfassung durch Google Analytics f&uuml;r diese Website und f&uuml;r diesen Browser zuk&uuml;nftig verhindert, so lange das Cookie in Ihrem Browser installiert bleibt.­</span>\'),
                        (13, 130, \'deutsch\', \'Verwendung von Matomo\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Diese Website benutzt Matomo (ehemals Piwik), eine Open-Source-Software zur statistischen Auswertung von Besucherzugriffen. Matomo verwendet sog. Cookies, also Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm&ouml;glichen.<br /><br />Die durch das Cookie erzeugten Informationen &uuml;ber Ihre Nutzung des Internetangebotes werden auf einem Server in Schweiz gespeichert.<br /><br />Die IP-Adresse wird unmittelbar nach der Verarbeitung und vor deren Speicherung anonymisiert. Sie haben die M&ouml;glichkeit, die Installation der Cookies durch &auml;nderung der Einstellung Ihrer Browser-Software zu verhindern. Wir weisen Sie darauf hin, dass bei entsprechender Einstellung eventuell nicht mehr alle Funktionen dieser Website zur Verf&uuml;gung stehen.<br /><br />Sie k&ouml;nnen sich entscheiden, ob in Ihrem Browser ein eindeutiger Webanalyse-Cookie abgelegt werden darf, um dem Betreiber der Webseite die Erfassung und Analyse verschiedener statistischer Daten zu erm&ouml;glichen.­</span>\'),
                        (14, 140, \'deutsch\', \'Verwendung von Adobe Analytics (Omniture)\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Diese Website benutzt Adobe Analytics, einen Webanalysedienst der Adobe Systems Software Ireland Limited („Adobe“). Adobe Analytics verwendet sog. Cookies, also Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm&ouml;glichen. Wird ein Tracking Datensatz von einem Browser eines Webseitenbesuchers an die Adobe Datacenter &uuml;bermittelt, dann wird durch die von uns vorgenommene Servereinstellung gew&auml;hrleistet, dass vor der Geolokalisierung die IP-Adresse anonymisiert wird, d.h. dass das letzte Oktett der IP Adresse durch Nullen ersetzt wird. Vor Speicherung des Tracking-Pakets wird die IP Adresse durch einzelne generische IP Adressen ersetzt.<br /><br />Im Auftrag des Betreibers dieser Website wird Adobe diese Informationen benutzen, um die Nutzung der Website durch die Nutzer auszuwerten, um Reports &uuml;ber die Websiteaktivit&auml;ten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegen&uuml;ber dem Websitebetreiber zu erbringen. Die im Rahmen von Adobe Analytics von Ihrem Browser &uuml;bermittelte IP-Adresse wird nicht mit anderen Daten von Adobe zusammengef&uuml;hrt.<br /><br />Sie k&ouml;nnen die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern. Dieses Angebot weist die Nutzer jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s&auml;mtliche Funktionen dieser Website vollumf&auml;nglich werden nutzen k&ouml;nnen. Die Nutzer k&ouml;nnen dar&uuml;ber hinaus die Erfassung der durch das Cookie erzeugten und auf ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Adobe sowie die Verarbeitung dieser Daten durch Adobe verhindern, indem sie das unter dem folgenden Link verf&uuml;gbare Browser-Plug-In herunterladen und installieren: <a target=\"_blank\" href=\"http://www.adobe.com/de/privacy/opt-out.html­\">http://www.adobe.com/de/privacy/opt-out.html­</a></span>\'),
                        (15, 150, \'deutsch\', \'Analyse durch WiredMinds\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Unsere Website nutzt die Z&auml;hlpixeltechnologie der WiredMinds AG (<a target=\"_blank\" href=\"http://www.wiredminds.de\">www.wiredminds.de</a>) zur Analyse des Besucherverhaltens<br /><br />Dabei werden Daten erhoben, verarbeitet und gespeichert, aus denen unter einem Pseudonym Nutzungsprofile erstellt werden. Wo m&ouml;glich und sinnvoll, werden diese Nutzungsprofile vollst&auml;ndig anonymisiert. Hierzu k&ouml;nnen Cookies zum Einsatz kommen. Cookies sind kleine Textdateien, die im Internet-Browser des Besuchers gespeichert werden und zur Wiedererkennung des Internet-Browsers dienen. Die erhobenen Daten, die auch personenbezogene Daten beinhalten k&ouml;nnen, werden an WiredMinds &uuml;bermittelt oder direkt von WiredMinds erhoben. WiredMinds darf Informationen, die durch Besuche auf den Webseiten hinterlassen werden, nutzen, um anonymisierte Nutzungsprofile zu erstellen. Die dabei gewonnenen Daten werden ohne die gesondert erteilte Zustimmung des Betroffenen nicht benutzt, um den Besucher dieser Webseite pers&ouml;nlich zu identifizieren und sie werden nicht mit personenbezogenen Daten &uuml;ber den Tr&auml;ger des Pseudonyms zusammengef&uuml;hrt. Soweit IP-Adressen erfasst werden, erfolgt deren sofortige Anonymisierung durch L&ouml;schen des letzten Nummernblocks.<br /><br />Der Datenerhebung, -verarbeitung und -speicherung kann jederzeit mit Wirkung f&uuml;r die Zukunft unter folgendem Link widersprochen werden: <a target=\"_blank\" href=\"http://wm.wiredminds.de/track/cookie_mgr.php?mode=dont_track_ask&amp;websitesel=eba71dd827fce58f&amp;lang=de\">Vom Website-Tracking ausschliessen­</a></span>\'),
                        (16, 160, \'deutsch\', \'Nutzung des Skalierbaren Zentralen Messverfahrens\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Unsere Webseite nutzt das Messverfahren („SZMnG“) der INFOnline GmbH (https://www.INFOnline.de) zur Ermittlung statistischer Kennwerte &uuml;ber die Nutzung unserer Angebote. Ziel der Nutzungsmessung ist es, die Anzahl der Besuche auf unserer Website, die Anzahl der Websitebesucher und deren Surfverhalten statistisch – auf Basis eines einheitlichen Standardverfahrens – zu bestimmen und somit marktweit vergleichbare Werte zu erhalten.<br /><br />F&uuml;r alle Digital-Angebote, die Mitglied der Informationsgemeinschaft zur Feststellung der Verbreitung von Werbetr&auml;gern e.V. (IVW – http://www.ivw.eu) sind oder an den Studien der Arbeitsgemeinschaft Online-Forschung e.V. (AGOF – http://www.agof.de) teilnehmen, werden die Nutzungsstatistiken regelm&auml;ssig von der AGOF und der Arbeitsgemeinschaft Media-Analyse e.V. (agma – http://www.agma-mmc.de) zu Reichweiten weiter verarbeitet und mit dem Leistungswert „Unique User“ ver&ouml;ffentlicht sowie von der IVW mit den Leistungswerten „Page Impression“ und „Visits“. Diese Reichweiten und Statistiken k&ouml;nnen auf den jeweiligen Webseiten eingesehen werden.<br /><br /><strong>1. Rechtsgrundlage f&uuml;r die Verarbeitung</strong><br />Die Messung mittels des Messverfahrens SZMnG durch die INFOnline GmbH erfolgt mit berechtigtem Interesse nach Art. 6 Abs. 1 lit. f) DSGVO.<br /><br />Zweck der Verarbeitung der personenbezogenen Daten ist die Erstellung von Statistiken und die Bildung von Nutzerkategorien. Die Statistiken dienen dazu, die Nutzung unseres Angebots nachvollziehen und belegen zu k&ouml;nnen. Die Nutzerkategorien bilden die Grundlage f&uuml;r eine interessengerechte Ausrichtung von Werbemitteln bzw. Werbemassnahmen. Zur Vermarktung dieser Webseite ist eine Nutzungsmessung, welche eine Vergleichbarkeit zu anderen Marktteilnehmern gew&auml;hrleistet, unerl&auml;sslich. Unser berechtigtes Interesse ergibt sich aus der wirtschaftlichen Verwertbarkeit der sich aus den Statistiken und Nutzerkategorien ergebenden Erkenntnisse und dem Marktwert unserer Webseite -auch in direktem Vergleich mit Webseiten Dritter- der sich anhand der Statistiken ermitteln l&auml;sst.<br /><br />Dar&uuml;ber hinaus haben wir ein berechtigtes Interesse daran, die pseudonymisierten Daten der INFOnline, der AGOF und der IVW zum Zwecke der Marktforschung (AGOF, agma) und f&uuml;r statistische Zwecke (INFOnline, IVW) zur Verf&uuml;gung zu stellen. Weiterhin haben wir ein berechtigtes Interesse daran, die pseudonymisierten Daten der INFOnline zur Weiterentwicklung und Bereitstellung interessengerechter Werbemittel zur Verf&uuml;gung zu stellen.<br /><br /><strong>2. Art der Daten</strong><br />Die INFOnline GmbH erhebt die folgenden Daten, welche nach EU-DSGVO einen Personenbezug aufweisen:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">IP-Adresse: Im Internet ben&ouml;tigt jedes Ger&auml;t zur &uuml;bertragung von Daten eine eindeutige Adresse, die sogenannte IP-Adresse. Die zumindest kurzzeitige Speicherung der IP-Adresse ist aufgrund der Funktionsweise des Internets technisch erforderlich. Die IP-Adressen werden vor jeglicher Verarbeitung um 1 Byte gek&uuml;rzt und nur anonymisiert Es erfolgt keine Speicherung oder weitere Verarbeitung der ungek&uuml;rzten IP-Adressen.</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Einen zuf&auml;llig erzeugten Client-Identifier: Die Reichweitenverarbeitung verwendet zur Wiedererkennung von Computersystemen alternativ entweder ein Cookie mit der Kennung „ioam.de“, ein „Local Storage Object“ oder eine Signatur, die aus verschiedenen automatisch &uuml;bertragenen Informationen Ihres Browsers erstellt wird. Diese Kennung ist f&uuml;r einen Browser eindeutig, solange das Cookie oder Local Storage Object nicht gel&ouml;scht wird. Eine Messung der Daten und anschliessende Zuordnung zu dem jeweiligen Client-Identifier ist daher auch dann m&ouml;glich, wenn Sie andere Webseiten aufrufen, die ebenfalls das Messverfahren („SZMnG“) der INFOnline GmbH nutzen. Die G&uuml;ltigkeit des Cookies ist auf maximal 1 Jahr beschr&auml;nkt.</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\"><strong>3.&nbsp;Nutzung der Daten</strong><br />Das Messverfahren der INFOnline GmbH, welches auf dieser Webseite eingesetzt wird, ermittelt Nutzungsdaten. Dies geschieht, um die Leistungswerte Page Impressions, Visits und Clients zu erheben und weitere Kennzahlen daraus zu bilden (z.B. qualifizierte Clients). Dar&uuml;ber hinaus werden die gemessenen Daten wie folgt genutzt:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Eine sogenannte Geolokalisierung, also die Zuordnung eines Webseitenaufrufs zum Ort des Aufrufs, erfolgt ausschliesslich auf der Grundlage der anonymisierten IP-Adresse und nur bis zur geographischen Ebene der Bundesl&auml;nder / Regionen. Aus den so gewonnenen geographischen Informationen kann in keinem Fall ein R&uuml;ckschluss auf den konkreten Aufenthaltsort eines Nutzers gezogen werden.</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Die Nutzungsdaten eines technischen Clients (bspw. eines Browsers auf einem Ger&auml;t) werden webseiten&uuml;bergreifend zusammengef&uuml;hrt und in einer Datenbank gespeichert. Diese Informationen werden zur technischen Absch&auml;tzung der Sozioinformation Alter und Geschlecht verwendet und an die Dienstleister der AGOF zur weiteren Reichweitenverarbeitung &uuml;bergeben. Im Rahmen der AGOF-Studie werden auf Basis einer zuf&auml;lligen Stichprobe Soziomerkmale technisch abgesch&auml;tzt, welche sich den folgenden Kategorien zuordnen lassen: Alter, Geschlecht, Nationalit&auml;t, Berufliche T&auml;tigkeit, Familienstand, Allgemeine Angaben zum Haushalt, Haushalts-Einkommen, Wohnort, Internetnutzung, Online-Interessen, Nutzungsort, Nutzertyp.</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\"><strong>4.&nbsp;Speicherdauer der Daten</strong><br />Die vollst&auml;ndige IP-Adresse wird von der INFOnline GmbH nicht gespeichert. Die gek&uuml;rzte IP-Adresse wird maximal 60 Tage gespeichert. Die Nutzungsdaten in Verbindung mit dem eindeutigen Identifier werden maximal 6 Monate gespeichert.<br /><br /><strong>5. Weitergabe der Daten</strong><br />Die IP-Adresse wie auch die gek&uuml;rzte IP-Adresse werden nicht weitergegeben. F&uuml;r die Erstellung der AGOF-Studie werden Daten mit Client-Identifiern an die folgenden Dienstleister der AGOF weitergegeben:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Kantar Deutschland GmbH (<a target=\"_blank\" href=\"https://www.tns-infratest.com/\">https://www.tns-infratest.com/</a>)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Ankordata GmbH &amp; Co. KG (<a target=\"_blank\" href=\"http://www.ankordata.de/homepage/\">http://www.ankordata.de/homepage/</a>)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Interrogare GmbH (<a target=\"_blank\" href=\"https://www.interrogare.de/\">https://www.interrogare.de/</a>)</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\"><strong>6.&nbsp;Rechte der betroffenen Person</strong><br />Die betroffene Person hat folgende Rechte:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auskunftsrecht (Art. 15 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf Berichtigung (Art. 16 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Widerspruchsrecht (Art. 21 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf L&ouml;schung (Art. 17 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf Einschr&auml;nkung der Verarbeitung (Art. 18f. DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf Daten&uuml;bertragbarkeit (Art. 20 DSGVO)</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Bei Anfragen dieser Art, wenden Sie sich bitte an Kontaktdaten am Ende dieser Datenschutzerkl&auml;rung. Bitte beachten Sie, dass wir bei derartigen Anfragen sicherstellen m&uuml;ssen, dass es sich tats&auml;chlich um die betroffene Person handelt.<br /><br /><br /><strong>Widerspruchsrecht</strong><br />Wenn Sie an der Messung nicht teilnehmen m&ouml;chten, k&ouml;nnen Sie unter folgendem Link widersprechen: <a target=\"_blank\" href=\"https://optout.ioam.de\">https://optout.ioam.de</a><br /><br />Um einen Ausschluss von der Messung zu garantieren, ist es technisch notwendig, ein Cookie zu setzen. Sollten Sie die Cookies in Ihrem Browser l&ouml;schen, ist es notwendig, den Opt-Out-Vorgang unter dem oben genannten Link zu wiederholen.<br /><br />Die betroffene Person hat das Recht, bei einer Datenschutzbeh&ouml;rde Beschwerde einzulegen.<br /><br />Weitere Informationen zum Datenschutz im Messverfahren finden Sie auf der Webseite der INFOnline GmbH (<a target=\"_blank\" href=\"https://www.infonline.de\">https://www.infonline.de</a>), die das Messverfahren betreibt, der Datenschutzwebseite der AGOF (<a target=\"_blank\" href=\"http://www.agof.de/datenschutz\">http://www.agof.de/datenschutz</a>) und der Datenschutzwebseite der IVW (<a target=\"_blank\" href=\"http://www.ivw.eu\">http://www.ivw.eu</a>).</span>\'),
                        (17, 170, \'deutsch\', \'Nutzung des Skalierbaren Zentralen Messverfahrens durch eine Applikation\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Unsere Applikation nutzt das Messverfahren („SZMnG“) der INFOnline GmbH (<a target=\"_blank\" href=\"https://www.INFOnline.de\">https://www.INFOnline.de</a>) zur Ermittlung statistischer Kennwerte &uuml;ber die Nutzung unserer Angebote. Ziel der Nutzungsmessung ist es, die Nutzungsintensit&auml;t, die Anzahl der Nutzungen und Nutzer unserer Applikation und deren Surfverhalten statistisch– auf Basis eines einheitlichen Standardverfahrens – zu bestimmen und somit marktweit vergleichbare Werte zu erhalten.<br /><br />F&uuml;r alle Digital-Angebote, die Mitglied der Informationsgemeinschaft zur Feststellung der Verbreitung von Werbetr&auml;gern e.V. (IVW – http://www.ivw.eu) sind oder an den Studien der Arbeitsgemeinschaft Online-Forschung e.V. (AGOF – http://www.agof.de) teilnehmen, werden die Nutzungsstatistiken regelm&auml;ssig von der AGOF und der Arbeitsgemeinschaft Media-Analyse e.V. (agma – http://www.agma-mmc.de) zu Reichweiten weiter verarbeitet und mit dem Leistungswert „Unique User“ ver&ouml;ffentlicht sowie von der IVW mit den Leistungswerten „Page Impression“ und „Visits“. Diese Reichweiten und Statistiken k&ouml;nnen auf den jeweiligen Webseiten eingesehen werden.<br /><br /><strong>1.&nbsp;Rechtsgrundlage f&uuml;r die Verarbeitung</strong><br />Die Messung mittels des Messverfahrens SZMnG durch die INFOnline GmbH erfolgt mit berechtigtem Interesse nach Art. 6 Abs. 1 lit. f) DSGVO.<br /><br />Zweck der Verarbeitung der personenbezogenen Daten ist die Erstellung von Statistiken zur Bildung von Nutzerkategorien. Die Statistiken dienen uns dazu, die Nutzung unseres Angebots nachvollziehen und belegen zu k&ouml;nnen. Die Nutzerkategorien bilden die Grundlage f&uuml;r eine interessengerechte Ausrichtung von Werbemitteln bzw. Werbemassnahmen. Zur Vermarktung dieser Applikation ist eine Nutzungsmessung, welche eine Vergleichbarkeit zu anderen Marktteilnehmern gew&auml;hrleistet, unerl&auml;sslich. Unser berechtigtes Interesse ergibt sich aus der wirtschaftlichen Verwertbarkeit der sich aus den Statistiken und Nutzerkategorien ergebenden Erkenntnisse und dem Marktwert unserer Applikation -auch in direktem Vergleich mit Applikationen Dritter-, der sich anhand der Statistiken ermitteln l&auml;sst.<br /><br />Dar&uuml;ber hinaus haben wir ein berechtigtes Interesse daran, die pseudonymisierten Daten der INFOnline, der AGOF und der IVW zum Zwecke der Marktforschung (AGOF, agma) und f&uuml;r statistische Zwecke (IVW, INFOnline) zur Verf&uuml;gung zu stellen. Weiterhin haben wir ein berechtigtes Interesse daran, die pseudonymisierten Daten der INFOnline zur Weiterentwicklung und Bereitstellung interessengerechter Werbemittel zur Verf&uuml;gung zu stellen.<br /><br /><strong>2. Art der Daten</strong><br />Die INFOnline GmbH erhebt die folgenden Daten, welche nach DSGVO einen Personenbezug aufweisen:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">IP-Adresse: Im Internet ben&ouml;tigt jedes Ger&auml;t zur &uuml;bertragung von Daten eine eindeutige Adresse, die sogenannte IP-Adresse. Die zumindest kurzzeitige Speicherung der IP-Adresse ist aufgrund der Funktionsweise des Internets technisch erforderlich. Die IP-Adressen werden vor jeglicher Verarbeitung gek&uuml;rzt und nur anonymisiert weiterverarbeitet. Es erfolgt keine Speicherung oder Verarbeitung der ungek&uuml;rzten IP-Adressen.</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Einen Ger&auml;te-Identifier: Die Reichweitenmessung verwendet zur Wiedererkennung von Ger&auml;ten eindeutige Kennungen des Endger&auml;tes oder eine Signatur, die aus verschiedenen automatisch &uuml;bertragenen Informationen Ihres Ger&auml;tes erstellt wird. Eine Messung der Daten und anschliessende Zuordnung zu dem jeweiligen Identifier ist unter Umst&auml;nden auch dann m&ouml;glich, wenn Sie andere Applikationen aufrufen, die ebenfalls das Messverfahren („SZMnG“) der INFOnline GmbH nutzen.</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Folgende eindeutige Ger&auml;te-Kennungen k&ouml;nnen als Hash an die INFOnline GmbH &uuml;bermittelt werden:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Advertising-Identifier</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Installation-ID</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Android-ID</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Vendor-ID</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\"><strong>3.&nbsp;Nutzung der Daten</strong><br />Das Messverfahren der INFOnline GmbH, welches in dieser Applikation eingesetzt wird, ermittelt Nutzungsdaten. Dies geschieht, um die Leistungswerte Page Impressions, Visits und Clients zu erheben und weitere Kennzahlen daraus zu bilden (z.B. qualifizierte Clients). Dar&uuml;ber hinaus werden die gemessenen Daten wie folgt genutzt:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Eine sogenannte Geolokalisierung, also die Zuordnung eines Webseitenaufrufs zum Ort des Aufrufs, erfolgt ausschliesslich auf der Grundlage der anonymisierten IP-Adresse und nur bis zur geographischen Ebene der Bundesl&auml;nder / Regionen. Aus den so gewonnenen geographischen Informationen kann in keinem Fall ein R&uuml;ckschluss auf den konkreten Wohnort eines Nutzers gezogen werden.</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Die Nutzungsdaten eines technischen Clients (bspw. eines Browsers auf einem Ger&auml;t) werden applikations&uuml;bergreifend zusammengef&uuml;hrt und in einer Datenbank gespeichert. Diese Informationen werden zur technischen Absch&auml;tzung der Sozioinformation Alter und Geschlecht verwendet und an die Dienstleister der AGOF zur weiteren Reichweitenverarbeitung &uuml;bergeben. Im Rahmen der AGOF-Studie werden auf Basis einer zuf&auml;lligen Stichprobe Soziomerkmale technisch abgesch&auml;tzt, welche sich den folgenden Kategorien zuordnen lassen: Alter, Geschlecht, Nationalit&auml;t, Berufliche T&auml;tigkeit, Familienstand, Allgemeine Angaben zum Haushalt, Haushalts-Einkommen, Wohnort, Internetnutzung, Online-Interessen, Nutzungsort, Nutzertyp.</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\"><strong>4.&nbsp;Speicherdauer der Daten</strong><br />Die vollst&auml;ndige IP-Adresse wird von der INFOnline GmbH nicht gespeichert. Die gek&uuml;rzte IP-Adresse wird maximal 60 Tage gespeichert. Die Nutzungsdaten in Verbindung mit dem eindeutigen Identifier werden maximal 6 Monate gespeichert.<br /><br /><strong>5. Weitergabe der Daten</strong><br />Die IP-Adresse wie auch die gek&uuml;rzte IP-Adresse werden nicht weitergegeben. F&uuml;r die Erstellung der AGOF-Studie werden Daten mit Client-Identifiern an die folgenden Dienstleister der AGOF weitergegeben:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Kantar Deutschland GmbH (<a target=\"_blank\" href=\"https://www.tns-infratest.com/\">https://www.tns-infratest.com/</a>)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Ankordata GmbH &amp; Co. KG (<a target=\"_blank\" href=\"http://www.ankordata.de/homepage/\">http://www.ankordata.de/homepage/</a>)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Interrogare GmbH (<a target=\"_blank\" href=\"https://www.interrogare.de/\">https://www.interrogare.de/</a>)</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\"><strong>6.&nbsp;Rechte der betroffenen Person</strong><br />Die betroffene Person hat folgende Rechte:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auskunftsrecht (Art. 15 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf Berichtigung (Art. 16 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Widerspruchsrecht (Art. 21 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf L&ouml;schung (Art. 17 DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf Einschr&auml;nkung der Verarbeitung (Art. 18f. DSGVO)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Recht auf Daten&uuml;bertragbarkeit (Art. 20 DSGVO)</span></li></ul><br /><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Bei Anfragen dieser Art, wenden Sie sich bitte an Kontaktdaten am Ende dieser Datenschutzerkl&auml;rung. Bitte beachten Sie, dass wir bei derartigen Anfragen sicherstellen m&uuml;ssen, dass es sich tats&auml;chlich um die betroffene Person handelt.<br /><br /><strong>Widerspruchsrecht</strong><br />Wenn Sie an der Messung nicht teilnehmen m&ouml;chten, k&ouml;nnen Sie hier widersprechen:<br /><br />Die betroffene Person hat das Recht, bei einer Datenschutzbeh&ouml;rde Beschwerde einzulegen.<br /><br />Weitere Informationen zum Datenschutz im Messverfahren finden Sie auf der Webseite der INFOnline GmbH (https://www.infonline.de), die das Messverfahren betreibt, der Datenschutzwebseite der AGOF (<a target=\"_blank\" href=\"http://www.agof.de/datenschutz\">http://www.agof.de/datenschutz</a>) und der Datenschutzwebseite der IVW (<a target=\"_blank\" href=\"http://www.ivw.eu\">http://www.ivw.eu</a>).­</span>\'),
                        (18, 180, \'deutsch\', \'Verwendung von Scriptbibliotheken (Google Webfonts)\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Um unsere Inhalte browser&uuml;bergreifend korrekt und grafisch ansprechend darzustellen, verwenden wir auf dieser Website Scriptbibliotheken und Schriftbibliotheken wie z. B. Google Webfonts (<a target=\"_blank\" href=\"https://www.google.com/webfonts/\">https://www.google.com/webfonts/</a>). Google Webfonts werden zur Vermeidung mehrfachen Ladens in den Cache Ihres Browsers &uuml;bertragen. Falls der Browser die Google Webfonts nicht unterst&uuml;tzt oder den Zugriff unterbindet, werden Inhalte in einer Standardschrift angezeigt.<br /><br />Der Aufruf von Scriptbibliotheken oder Schriftbibliotheken l&ouml;st automatisch eine Verbindung zum Betreiber der Bibliothek aus. Dabei ist es theoretisch m&ouml;glich – aktuell allerdings auch unklar ob und ggf. zu welchen Zwecken – dass Betreiber entsprechender Bibliotheken Daten erheben.<br /><br />Die Datenschutzrichtlinie des Bibliothekbetreibers Google finden Sie hier: <a target=\"_blank\" href=\"https://www.google.com/policies/privacy/­\">https://www.google.com/policies/privacy/­</a></span>\'),
                        (19, 190, \'deutsch\', \'Verwendung von Adobe Typekit\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">­Wir setzen Adobe Typekit zur visuellen Gestaltung unserer Website ein. Typekit ist ein Dienst der Adobe Systems Software Ireland Ltd. der uns den Zugriff auf eine Schriftartenbibliothek gew&auml;hrt. Zur Einbindung der von uns benutzten Schriftarten, muss Ihr Browser eine Verbindung zu einem Server von Adobe in den USA aufbauen und die f&uuml;r unsere Website ben&ouml;tigte Schriftart herunterladen. Adobe erh&auml;lt hierdurch die Information, dass von Ihrer IP-Adresse unsere Website aufgerufen wurde. Weitere Informationen zu Adobe Typekit finden Sie in den Datenschutzhinweisen von Adobe, die Sie hier abrufen k&ouml;nnen: <a target=\"_blank\" href=\"http://www.adobe.com/privacy/typekit.html\">www.adobe.com/privacy/typekit.html</a></span>\'),
                        (20, 200, \'deutsch\', \'Verwendung von Google Maps\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Diese Webseite verwendet Google Maps API, um geographische Informationen visuell darzustellen. Bei der Nutzung von Google Maps werden von Google auch Daten &uuml;ber die Nutzung der Kartenfunktionen durch Besucher erhoben, verarbeitet und genutzt. N&auml;here Informationen &uuml;ber die Datenverarbeitung durch Google k&ouml;nnen Sie <a target=\"_blank\" href=\"http://www.google.com/privacypolicy.html\">den Google-Datenschutzhinweisen</a> entnehmen. Dort k&ouml;nnen Sie im Datenschutzcenter auch Ihre pers&ouml;nlichen Datenschutz-Einstellungen ver&auml;ndern.<br /><br />Ausf&uuml;hrliche Anleitungen zur Verwaltung der eigenen Daten im Zusammenhang mit Google-Produkten <a target=\"_blank\" href=\"http://www.dataliberation.org/\">finden Sie hier</a>.­</span>\'),
                        (21, 210, \'deutsch\', \'Eingebettete YouTube-Videos\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auf einigen unserer Webseiten betten wir Youtube-Videos ein. Betreiber der entsprechenden Plugins ist die YouTube, LLC, 901 Cherry Ave., San Bruno, CA 94066, USA. Wenn Sie eine Seite mit dem YouTube-Plugin besuchen, wird eine Verbindung zu Servern von Youtube hergestellt. Dabei wird Youtube mitgeteilt, welche Seiten Sie besuchen. Wenn Sie in Ihrem Youtube-Account eingeloggt sind, kann Youtube Ihr Surfverhalten Ihnen pers&ouml;nlich zuzuordnen. Dies verhindern Sie, indem Sie sich vorher aus Ihrem Youtube-Account ausloggen.<br /><br />Wird ein Youtube-Video gestartet, setzt der Anbieter Cookies ein, die Hinweise &uuml;ber das Nutzerverhalten sammeln.<br /><br />Wer das Speichern von Cookies f&uuml;r das Google-Ad-Programm deaktiviert hat, wird auch beim Anschauen von Youtube-Videos mit keinen solchen Cookies rechnen m&uuml;ssen. Youtube legt aber auch in anderen Cookies nicht-personenbezogene Nutzungsinformationen ab. M&ouml;chten Sie dies verhindern, so m&uuml;ssen Sie das Speichern von Cookies im Browser blockieren.<br /><br />Weitere Informationen zum Datenschutz bei „Youtube“ finden Sie in der Datenschutzerkl&auml;rung des Anbieters unter: <a target=\"_blank\" href=\"https://www.google.de/intl/de/policies/privacy/­\">https://www.google.de/intl/de/policies/privacy/­</a></span>\'),
                        (22, 220, \'deutsch\', \'Social Plugins\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Auf unseren Webseiten werden Social Plugins der unten aufgef&uuml;hrten Anbieter eingesetzt. Die Plugins k&ouml;nnen Sie daran erkennen, dass sie mit dem entsprechenden Logo gekennzeichnet sind.<br /><br />&uuml;ber diese Plugins werden unter Umst&auml;nden Informationen, zu denen auch personenbezogene Daten geh&ouml;ren k&ouml;nnen, an den Dienstebetreiber gesendet und ggf. von diesem genutzt. Wir verhindern die unbewusste und ungewollte Erfassung und &uuml;bertragung von Daten an den Diensteanbieter durch eine 2-Klick-L&ouml;sung. Um ein gew&uuml;nschtes Social Plugin zu aktivieren, muss dieses erst durch Klick auf den entsprechenden Schalter aktiviert werden. Erst durch diese Aktivierung des Plugins wird auch die Erfassung von Informationen und deren &uuml;bertragung an den Diensteanbieter ausgel&ouml;st. Wir erfassen selbst keine personenbezogenen Daten mittels der Social Plugins oder &uuml;ber deren Nutzung.<br /><br />Wir haben keinen Einfluss darauf, welche Daten ein aktiviertes Plugin erfasst und wie diese durch den Anbieter verwendet werden. Derzeit muss davon ausgegangen werden, dass eine direkte Verbindung zu den Diensten des Anbieters ausgebaut wird sowie mindestens die IP-Adresse und ger&auml;tebezogene Informationen erfasst und genutzt werden. Ebenfalls besteht die M&ouml;glichkeit, dass die Diensteanbieter versuchen, Cookies auf dem verwendeten Rechner zu speichern. Welche konkreten Daten hierbei erfasst und wie diese genutzt werden, entnehmen Sie bitte den Datenschutzhinweisen des jeweiligen Diensteanbieters. Hinweis: Falls Sie zeitgleich bei Facebook angemeldet sind, kann Facebook Sie als Besucher einer bestimmten Seite identifizieren.<br /><br />Wir haben auf unserer Website die Social-Media-Buttons folgender Unternehmen eingebunden:</span><ul><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Facebook (<a target=\"_blank\" href=\"https://www.facebook.com/policy.php\">https://www.facebook.com/policy.php</a>)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Twitter (<a target=\"_blank\" href=\"https://help.twitter.com/de/rules-and-policies/personal-information\">https://help.twitter.com/de/rules-and-policies/personal-information</a>)</span></li><li><span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Google+ (<a target=\"_blank\" href=\"https://policies.google.com/privacy\">https://policies.google.com/privacy</a>)­</span></li></ul>\'),
                        (23, 230, \'deutsch\', \'Google AdWords\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Unsere Webseite nutzt das Google Conversion-Tracking. Sind Sie &uuml;ber eine von Google geschaltete Anzeige auf unsere Webseite gelangt, wird von Google Adwords ein Cookie auf Ihrem Rechner gesetzt. Das Cookie f&uuml;r Conversion-Tracking wird gesetzt, wenn ein Nutzer auf eine von Google geschaltete Anzeige klickt. Diese Cookies verlieren nach 30 Tagen ihre G&uuml;ltigkeit und dienen nicht der pers&ouml;nlichen Identifizierung. Besucht der Nutzer bestimmte Seiten unserer Website und das Cookie ist noch nicht abgelaufen, k&ouml;nnen wir und Google erkennen, dass der Nutzer auf die Anzeige geklickt hat und zu dieser Seite weitergeleitet wurde. Jeder Google AdWords-Kunde erh&auml;lt ein anderes Cookie. Cookies k&ouml;nnen somit nicht &uuml;ber die Websites von AdWords-Kunden nachverfolgt werden. Die mithilfe des Conversion-Cookies eingeholten Informationen dienen dazu, Conversion-Statistiken f&uuml;r AdWords-Kunden zu erstellen, die sich f&uuml;r Conversion-Tracking entschieden haben. Die Kunden erfahren die Gesamtanzahl der Nutzer, die auf ihre Anzeige geklickt haben und zu einer mit einem Conversion-Tracking-Tag versehenen Seite weitergeleitet wurden. Sie erhalten jedoch keine Informationen, mit denen sich Nutzer pers&ouml;nlich identifizieren lassen.<br /><br />M&ouml;chten Sie nicht am Tracking teilnehmen, k&ouml;nnen Sie das hierf&uuml;r erforderliche Setzen eines Cookies ablehnen – etwa per Browser-Einstellung, die das automatische Setzen von Cookies generell deaktiviert oder Ihren Browser so einstellen, dass Cookies von der Domain „googleleadservices.com“ blockiert werden.<br /><br />Bitte beachten Sie, dass Sie die Opt-out-Cookies nicht l&ouml;schen d&uuml;rfen, solange Sie keine Aufzeichnung von Messdaten w&uuml;nschen. Haben Sie alle Ihre Cookies im Browser gel&ouml;scht, m&uuml;ssen Sie das jeweilige Opt-out Cookie erneut setzen.­</span>\'),
                        (24, 240, \'deutsch\', \'Einsatz von Google Remarketing\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Diese Webseite verwendet die Remarketing-Funktion der Google Inc. Die Funktion dient dazu, Webseitenbesuchern innerhalb des Google-Werbenetzwerks interessenbezogene Werbeanzeigen zu pr&auml;sentieren. Im Browser des Webseitenbesuchers wird ein sog. „Cookie“ gespeichert, der es erm&ouml;glicht, den Besucher wiederzuerkennen, wenn dieser Webseiten aufruft, die dem Werbenetzwerk von Google angeh&ouml;ren. Auf diesen Seiten k&ouml;nnen dem Besucher Werbeanzeigen pr&auml;sentiert werden, die sich auf Inhalte beziehen, die der Besucher zuvor auf Webseiten aufgerufen hat, die die Remarketing Funktion von Google verwenden.<br /><br />Nach eigenen Angaben erhebt Google bei diesem Vorgang keine personenbezogenen Daten. Sollten Sie die Funktion Remarketing von Google dennoch nicht w&uuml;nschen, k&ouml;nnen Sie diese grunds&auml;tzlich deaktivieren, indem Sie die entsprechenden Einstellungen unter <a target=\"_blank\" href=\"http://www.google.com/settings/ads\">http://www.google.com/settings/ads</a> vornehmen. Alternativ k&ouml;nnen Sie den Einsatz von Cookies f&uuml;r interessenbezogene Werbung &uuml;ber die Werbenetzwerkinitiative deaktivieren, indem Sie den Anweisungen unter <a target=\"_blank\" href=\"http://www.networkadvertising.org/managing/opt_out.asp\">http://www.networkadvertising.org/managing/opt_out.asp</a> folgen.­</span>\'),
                        (25, 250, \'deutsch\', \'&auml;nderung unserer Datenschutzbestimmungen\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wir behalten uns vor, diese Datenschutzerkl&auml;rung anzupassen, damit sie stets den aktuellen rechtlichen Anforderungen entspricht oder um &auml;nderungen unserer Leistungen in der Datenschutzerkl&auml;rung umzusetzen, z.B. bei der Einf&uuml;hrung neuer Services. F&uuml;r Ihren erneuten Besuch gilt dann die neue Datenschutzerkl&auml;rung.­</span>\'),
                        (26, 260, \'deutsch\', \'Fragen an den Datenschutzbeauftragten\', \'<span style=\"font-family:arial,helvetica,sans-serif; font-size:12px\">Wenn Sie Fragen zum Datenschutz haben, schreiben Sie uns bitte eine E-Mail oder wenden Sie sich direkt an die f&uuml;r den Datenschutz verantwortliche Person in unserer Organisation:<br /><a href=\"mailto:'. $NotifyMail .'\">'. $NotifyMail .'</a></span>\');');

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsVorlageImport'] .' <br />';
            $MSG .= '</div>';
        }

        // AV importieren
        if ((isset($_REQUEST['import']) && $_REQUEST['import'] == 'av')) {
            // Alte einträge löschen
            $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_av');

            // Neue einträge importieren
            $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_av` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Lang` varchar(10) DEFAULT NULL,
                      `Title` varchar(200) DEFAULT NULL,
                      `Content` text,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");
            $db->Query('INSERT INTO `{pre}mod_ds_av` (`ID`, `Lang`, `Title`, `Content`) VALUES
                        (1, \'deutsch\', \'Vereinbarung zur Auftragsdatenverarbeitung \', \'zwischen:<blockquote>[%kundendaten%]</blockquote>im Folgenden: <strong>Auftraggeber</strong><br /><br /><br /><br />und<blockquote><strong>[%name%]</strong><br />[%street%] [%streetnr%]<br />[%plz%] [%place%]<br />[%country%]</blockquote>im Folgenden: <strong>Auftragnehmer</strong><br /><br /> \n<ol>\n<li>\n<strong>Gegenstand und Anwendungsbereich der ADV-Vereinbarung</strong>\n<ol>\n<li>Diese Vereinbarung zur Auftragsdatenverarbeitung (\"ADV-Vereinbarung\") regelt die Pflichten, Rollen und Zust&auml;ndigkeiten vom&nbsp;Auftragnehmer und dem Auftraggeber&nbsp;in Bezug auf die Auftragsverarbeitung.</li>\n</ol>\n</li>\n<li>\n<strong>G&uuml;ltigkeit, Laufdauer, Verh&auml;ltnis zum Hosting-Vertrag</strong>\n<ol>\n<li>Der Auftragnehmer stellt diese ADV-Vereinbarung in den Einstellungen&nbsp;zum Abschluss in Bezug auf die bestellten&nbsp;Dienstleistungen bereit. Wenn der Auftraggeber der ADV-Vereinbarung durch Aktivierung eines Best&auml;tigungsfelds \"Zustimmen\"&nbsp;zustimmt, wird die ADV-Vereinbarung f&uuml;r die Vertragsparteien zum verbindlichen Bestandteil ihrer vertraglichen Vereinbarungen betreffend die Erbringung der Dienstleistungen. Sie gilt f&uuml;r die gesamte Dauer des Vertrags und gegebenenfalls dar&uuml;ber hinaus bis zur L&ouml;schung der von der Auftragsverarbeitung betroffenen personenbezogenen Daten (vgl. Ziff. 4.2) durch den Auftragnehmer.</li>\n<li>Die Bestimmungen dieser ADV-Vereinbarung erg&auml;nzen die Bestimmungen des Hosting-Vertrags. Sie schr&auml;nken die Rechte und Pflichten der Vertragsparteien in Bezug auf die Erbringung bzw. die Inanspruchnahme der Dienstleistungen nicht ein. Ihren Regelungsgegenstand betreffend gehen die Bestimmungen dieser ADV-Vereinbarung indes den Bestimmungen des Vertrags vor.</li>\n</ol>\n</li>\n<li>\n<strong>Anwendungsbereich der ADV-Vereinbarung</strong>\n<ol>\n<li>Diese ADV-Vereinbarung gilt (sobald ihr der Auftraggeber zugestimmt hat) in Bezug auf Auftragsverarbeitungen im Rahmen der vom Auftragnehmer gem&auml;ss Vertrag erbrachten Dienstleistungen.</li>\n<li>Diese ADV-Vereinbarung gilt ausdr&uuml;cklich nicht in Bezug auf Verarbeitungen personenbezogener Daten, bei denen der Auftragnehmer die Zwecke und Mittel der Verarbeitung bestimmt und somit unter dem Schweizerischen Bundesgesetz &uuml;ber den Datenschutz (DSG) oder allenfalls anwendbaren anderen Datenschutzgesetzen (insbesondere der EU-DSGVO) verantwortlich ist.&nbsp;Solche Verarbeitungen personenbezogener Daten, die der Auftragnehmer als Verantwortlicher vornimmt (z.B. Verarbeitungen personenbezogener Daten im Rahmen von E-Mails oder zu Zwecken der Leistungsabrechnung oder der Kommunikation mit dem Kunden) nimmt der Auftragnehmer in &uuml;bereinstimmung mit der Datenschutzerkl&auml;rung vom Auftragnehemer&nbsp;und den anwendbaren Datenschutzgesetzen vor.</li>\n</ol>\n</li>\n<li>\n<strong>Angaben zur Auftragsverarbeitung</strong>\n<ol>\n<li>Gegenstand und Zweck der Auftragsverarbeitung ist die Erbringung von Dienstleistungen durch den Auftragnehmer f&uuml;r den Auftraggeber. Die Auftragsverarbeitung besteht in der Speicherung, Bereitstellung, &uuml;bermittlung und L&ouml;schung von personenbezogenen Daten gem&auml;ss den Bestimmungen des Hosting-Vertrags.</li>\n<li>Von der Auftragsverarbeitung betroffen sind personenbezogene Daten, die der Kunde gem&auml;ss seiner Wahl auf der vom Auftragnehmer&nbsp;f&uuml;r die Leistungserbringung eingesetzten Infrastruktur speichert sowie Daten von Personen, denen der Auftraggeber Zugriff auf seine E-Mails oder Dokumenten gew&auml;hrt. Dabei handelt es sich insbesondere um personenbezogene Daten, die beim Aufrufen bzw. Ausf&uuml;hren und der Nutzung von E-Mails und weiteren Applikationen &uuml;blicherweise erhoben werden. Dazu geh&ouml;ren Protokolldaten, die bei der informatorischen Nutzung eines E-Mailservers oder einer Applikation automatisiert erhoben werden (z.B. die IP-Adresse und das Betriebssystem des Ger&auml;ts des Nutzers sowie das Datum und die Zugriffszeit des Browsers), vom Nutzer eingegebene Daten sowie vom Kunden erhobene Nutzungsdaten mit Personenbezug (nachstehend \"<strong>personenbezogene Daten</strong>\").</li>\n</ol>\n</li>\n<li>\n<strong>Rollen und Zust&auml;ndigkeitsbereiche</strong>\n<ol>\n<li>Der Auftraggeber best&auml;tigt und der Auftragnehmer anerkennt, dass der Auftraggeber f&uuml;r die Verarbeitung der personenbezogenen Daten nach anwendbaren Datenschutzgesetzen verantwortlich ist und bleibt. Der Auftraggeber nimmt somit die Rolle des Verantwortlichen ein.</li>\n<li>Der Auftragnehmer anerkennt, dass der Auftraggeber in der Rolle des Verantwortlichen verpflichtet ist, der Auftragnehmer bei Inanspruchnahme von Dienstleistungen einige seiner Pflichten aus der EU-DSGVO (oder anderen allenfalls anwendbaren Datenschutzgesetzen) vertraglich zu &uuml;berbinden.</li>\n<li>Der Auftragnemer nimmt in Bezug auf die Verarbeitung betroffener personenbezogener Daten die Rolle des Auftragsverarbeiters ein. Sofern der Auftragnehmer f&uuml;r diese Auftragsverarbeitung nicht ebenfalls der EU-DSGVO (oder den anderen allenfalls anwendbaren Datenschutzgesetzen) untersteht, so nimmt der Auftragnemer diese Rolle nur auf der Grundlage der vertraglichen Pflichten von dem Auftragnemer gem&auml;ss dieser ADV-Vereinbarung ein und wird nicht alleine deswegen unter der EU-DSGVO (oder den anderen allenfalls anwendbaren Datenschutzgesetzen) verpflichtet.</li>\n</ol>\n</li>\n<li>\n<strong>Pflichten vom Auftragnehmer</strong>\n<ol>\n<li>Der Auftragnemer verpflichtet sich, die personenbezogenen Daten nur zur Erbringung der Dienstleistungen gem&auml;ss Bestellung und vertraglichen Pflichten sowie gem&auml;ss dieser ADV-Vereinbarung zu verarbeiten.</li>\n<li>Der Auftragnemer ist dazu berechtigt, personenbezogene Daten des Kunden so zu verarbeiten, wie es die Erf&uuml;llung der Leistungspflichten aus dem Vertrag sowie dieser ADV-Vereinbarung beinhaltet. Auf entsprechende Anfrage ist der Auftragnemer bereit, weitergehende, die Auftragsverarbeitung betreffende Weisungen des Auftraggebers umzusetzen. Voraussetzung daf&uuml;r ist, dass diese f&uuml;r den Auftragnemer im Rahmen der vertraglich vereinbarten Dienstleistungen umsetzbar und objektiv zumutbar sind und nicht zu Mehrkosten oder ge&auml;ndertem Leistungsumfang f&uuml;hren. Vorbehalten bleibt in jedem Fall die Erf&uuml;llung gesetzlicher oder regulatorischer Pflichten, denen der Auftragnemer unterliegt.</li>\n<li>Der Auftragnehmer sorgt f&uuml;r die Einhaltung der Bestimmungen dieser ADV-Vereinbarung durch die mit der Auftragsverarbeitung betrauten Mitarbeiter und anderen f&uuml;r den Auftragnehmer t&auml;tigen Personen, die Zugriff auf die personenbezogenen Daten erhalten. Der Auftragnehmer verpflichtet sich zudem, Personen mit Zugang zu den personenbezogenen Daten zur Wahrung der Vertraulichkeit (auch &uuml;ber die Dauer ihrer T&auml;tigkeit und dar&uuml;ber hinaus) zu verpflichten.</li>\n<li>Der Auftragnehmer verpflichtet sich, im Interesse der Vertraulichkeit, Integrit&auml;t und vertragsgem&auml;ssen Verf&uuml;gbarkeit der personenbezogenen Daten angemessene technische und organisatorische Massnahmen zu treffen. Der Auftragnehmer implementiert insbesondere Zugangskontrollen, Zugriffskontrollen sowie Verfahren zur regelm&auml;ssigen &uuml;berpr&uuml;fung, Bewertung und Evaluierung der Wirksamkeit der technischen und organisatorischen Massnahmen. Bei der Auswahl der Massnahmen ber&uuml;cksichtigt der Auftragnemer den Stand der Technik, die Implementierungskosten sowie die Art, den Umfang, die Umst&auml;nde und die Zwecke der Verarbeitung sowie die unterschiedliche Eintrittswahrscheinlichkeit und Schwere des Risikos f&uuml;r betroffene Personen. Die jeweils geltenden Massnahmen ergeben sich aus den aktuellen Leistungsbeschrieben vom Auftragnehmer.</li>\n<li>Der Auftragnemer verpflichtet sich, den Kunden ohne Verzug schriftlich zu informieren, wenn der Auftragnemer Kenntnis von einer Datensicherheits-Verletzung erlangt, die personenbezogene Daten betrifft. Dabei hat der Auftragnemer dem Kunden die Art und das Ausmass der Verletzung sowie m&ouml;gliche Abhilfemassnahmen mitzuteilen. Die Vertragsparteien treffen gemeinsam die erforderlichen Massnahmen, um den Schutz der personenbezogenen Daten sicherzustellen und m&ouml;gliche nachteilige Folgen f&uuml;r die betroffenen Personen zu mildern. &uuml;berdies verpflichtet sich der Auftragnemer, dem Auftraggeber auf schriftliche Anfrage ausreichende Informationen zur Verf&uuml;gung zu stellen, damit dieser seinen Pflichten gem&auml;ss EU-DSGVO oder anderen anwendbaren Datenschutzgesetzen betreffend die Meldung, Untersuchung und Dokumentation von Datensicherheits-Verletzungen erf&uuml;llen kann.</li>\n<li>Der Auftraggeber verpflichtet sich, den Kunden auf schriftliche Anfrage und gegen separate angemessene Verg&uuml;tung sowie im Rahmen der betrieblichen Ressourcen und M&ouml;glichkeiten vom Auftragnemer bei der Erf&uuml;llung von Betroffenenrechten (insbesondere Auskunfts-, Berichtigungs- und L&ouml;- schungsrechten) durch den Kunden (personenbezogene Daten betreffend) gem&auml;ss Kapitel III der EU-DSGVO (oder &auml;quivalenten Bestimmunen anderer anwendbarer Datenschutzgesetze) zu unterst&uuml;tzen. Richtet sich eine betroffene Person mit Forderungen betreffend die Erf&uuml;llung von Betroffenenrechten direkt an den Auftragnemer, wird der Auftragnemer&nbsp;die betroffene Person an den Auftraggeber verweisen. Voraussetzung daf&uuml;r ist, dass der Auftragnemer eine solche Zuordnung an den Auftraggeber gest&uuml;tzt auf die Angaben der betroffenen Person vornehmen kann.</li>\n<li>Der Auftraggeber ist verpflichtet, den Auftragnemer ohne Verzug schriftlich zu benachrichtigen, wenn der Auftragnemer eine Anfrage (z.B. ein Auskunfts- oder L&ouml;schungsbegehren) von einer betroffenen Person in Bezug auf personenbezogene Daten erh&auml;lt; vorausgesetzt eine Zuordnung an den Auftraggeber ist gest&uuml;tzt auf die Angaben der betroffenen Person m&ouml;glich.</li>\n<li>Der Auftragnemer ist auf schriftliche Anfrage und gegen separate angemessene Verg&uuml;tung sowie unter Ber&uuml;cksichtigung der betrieblichen Ressourcen und M&ouml;glichkeiten vom Auftragnemer bereit, den Kunden bei Datenschutz-Folgenabsch&auml;tzungen und bei Konsultationen der Aufsichtsbeh&ouml;rden zu unterst&uuml;tzen.</li>\n<li>Der Auftragnemer wird die personenbezogenen Daten nach Ende der Laufdauer des Vertrags gem&auml;ss den Bestimmungen des Vertrags herausgeben oder l&ouml;schen.</li>\n</ol>\n</li>\n<li>\n<strong>Beizug von Unter-Auftragsverarbeitern</strong>\n<ol>\n<li>Beansprucht der Auftraggeber Dienstleistungen vom Auftraggeber, die personenbezogene Daten betreffen und durch Dritte erbracht werden, bleibt der Auftraggeber gegen&uuml;ber dem Auftragnemer Auftragsverarbeiter und erf&uuml;llt die diesbez&uuml;glichen Pflichten aus der ADV-Vereinbarung. Der Anbieter der Drittdienstleistung, die in der Dienstleistung vom Auftragnemer&nbsp;integriert wird, ist Unter-Auftragsverarbeiter vom Auftraggeber. Davon zu unterscheiden sind F&auml;lle, in denen der Auftragnemer dem Auftraggeber einen direkten Vertragsschluss mit dem Drittdienstleister vermittelt und der Drittdienstleister direkt Auftragsverarbeiter des Auftraggebers wird. In solchen F&auml;llen hat der Auftraggeber selber daf&uuml;r besorgt zu sein, unter anwendbaren Datenschutzgesetzen allenfalls notwendige Vereinbarungen mit dem Drittdienstleister zu treffen.</li>\n<li>Der Auftragnemer ist berechtigt, Unter-Auftragsverarbeiter im Rahmen der Erbringung der Dienstleistungen vom Auftragnemer beizuziehen. Der Auftragnemer ist in solchen F&auml;llen verpflichtet, mit Unter-Auftragsverarbeitern im erforderlichen Umfang eine Vereinbarung zu treffen, die der Auftragnemer die Einhaltung der Bestimmungen dieser ADV-Vereinbarung erm&ouml;glicht.</li>\n<li>Der Auftraggeber wird den Kunden vorab in geeigneter Weise informieren, wenn der Auftragnemer nach Inkrafttreten dieser ADV-Vereinbarung in Bezug auf bestehende Dienstleistungen neue Unter-Auftragsverarbeiter beizieht oder bestehende austauscht. Wenn der Auftraggeber dem nicht innerhalb von dreissig (30) Tagen nach dem Datum der Mitteilung aus wichtigen datenschutzrechtlichen Gr&uuml;nden widerspricht, gilt der neue oder ausgetauschte Unter-Auftragsverarbeiter als genehmigt.</li>\n<li>Wenn die Unter-Auftragsverarbeitung eine &uuml;bermittlung von personenbezogenen Daten in ein Land ausserhalb des Gebiets der EU/EWR/Schweiz beinhaltet, stellt der Auftragnemer sicher, dass der Auftragnemer die Bestimmungen der EU-DSGVO (oder &auml;hnlicher Bestimmungen des Schweizer DSG) betreffend die Daten&uuml;bermittlung in ein Drittland einh&auml;lt (z.B. durch Auswahl eines Unter-Auftragsverarbeiters, der dem U.S.-Swiss Privacy Shield unterstellt ist, oder durch Miteinbezug anerkannter Standardvertragsklauseln f&uuml;r die &uuml;bermittlung personenbezogener Daten an Auftragsverarbeiter in Drittl&auml;nder).</li>\n</ol>\n</li>\n<li>\n<strong>Pflichten des Auftraggeber</strong>\n<ol>\n<li>Der Auftraggeber ist f&uuml;r die Rechtm&auml;ssigkeit der Verarbeitung der personenbezogenen Daten, einschliesslich der Zul&auml;ssigkeit der Auftrags- bzw. Unter-Auftragsverarbeitung, verantwortlich.</li>\n<li>Der Auftraggeber trifft in seinem Verantwortungsbereich selbstst&auml;ndig angemessene technische und organisatorische Massnahmen zum Schutz der personenbezogenen Daten.</li>\n<li>Der Auftraggeber verpflichtet sich, dem Auftragnemer unverz&uuml;glich zu informieren, wenn der Auftraggeber in der Leistungserbringung vom Auftragnemer Verletzungen von anwendbaren Datenschutzgesetzen feststellt.</li>\n</ol>\n</li>\n<li>\n<strong>Informations- und Pr&uuml;fungsrechte</strong>\n<ol>\n<li>Der Auftragnemer ist verpflichtet, dem Auftraggeber auf schriftliche Anfrage alle Informationen zur Verf&uuml;gung zu stellen, die dieser vern&uuml;nftigerweise zum Nachweis der Einhaltung dieser ADV-Vereinbarung gegen&uuml;ber betroffenen Personen oder Datenschutzaufsichtsbeh&ouml;rden ben&ouml;tigt.</li>\n<li>Der Auftragnemer erm&ouml;glicht dem Kunden oder einem vom Auftraggeber beauftragten und zur Vertraulichkeit verpflichteten Pr&uuml;fer, die Einhaltung dieser ADV-Vereinbarung durch den Auftragnemer zu pr&uuml;fen. Werden nach Vorlage entsprechender Nachweise Verletzungen der ADV-Vereinbarung durch den Auftragnemer festgestellt, hat der Auftragnemer unverz&uuml;glich und kostenlos geeignete Korrekturmassnahmen zu implementieren.</li>\n<li>Die vorstehenden Informations- und Pr&uuml;fungsrechte des Auftraggebers bestehen nur insoweit, als der Vertrag dem Auftraggeber keine anderen Informations- und Pr&uuml;fungsrechte einr&auml;umt, die den einschl&auml;gigen Anforderungen der anwendbaren Datenschutzgesetze entsprechen. Weiter stehen diese Informations- und Pr&uuml;fungsrechte unter dem Vorbehalt des Verh&auml;ltnism&auml;ssigkeitsgebots und der Wahrung der schutzw&uuml;rdigen Interessen (insbesondere Sicherheits- oder Geheimhaltungsinteressen) vom Auftragnemer. Vorbeh&auml;ltlich einer anderslautenden Vereinbarung zwischen den Vertragsparteien tr&auml;gt der Auftraggeber s&auml;mtliche Kosten der&nbsp;Information und Pr&uuml;fung, einschliesslich nachgewiesener interner Kosten vom Auftragnemer.</li>\n</ol>\n</li>\n<li>\n<strong>&auml;nderungen dieser ADV-Vereinbarung</strong>\n<ol>\n<li>Der Auftragnemer beh&auml;lt sich vor, diese ADV-Vereinbarung zu &auml;ndern, (a) wenn dies zur Anpassung an Rechtsentwicklungen erforderlich ist oder (b) wenn dies nicht zu einer Verschlechterung der Gesamtsicherheit der Auftragsverarbeitung f&uuml;hrt und sich (nach Ermessen vom Auftragnemer) nicht erheblich nachteilig auf die Rechte der von der Auftragsverarbeitung betroffenen Personen auswirkt.</li>\n<li>Der Auftragnemer teilt dem Kunden beabsichtigte &auml;nderungen dieser ADV-Vereinbarung gem&auml;ss Ziff. 10.1 sp&auml;testens dreissig (30) Tage vor Wirksamwerden mit. Wenn der Auftraggeber der &auml;nderung widersprechen m&ouml;chte, kann er die ADV-Vereinbarung innerhalb von dreissig (30) Tagen ab Datum der Mitteilung k&uuml;ndigen. Ohne Widerspruch innerhalb dieser Frist gilt die &auml;nderung als genehmigt.</li>\n</ol>\n</li>\n<li>\n<strong>Generelle Bestimmungen</strong>\n<ol>\n<li>In Abweichung allf&auml;lliger im Hosting-Vertrag vereinbarter Schriftformvorbehalte kann die ADV-Vereinbarung auf elektronischem Weg zwischen den Vertragsparteien vereinbart oder ge&auml;ndert werden.</li>\n<li>Verlangt diese ADV-Vereinbarung eine schriftliche Aufforderung oder Mitteilung, so gen&uuml;gt (f&uuml;r Mitteilungen an den Kunden) E-Mail an die Adresse des Kunden bzw. (f&uuml;r Mitteilungen an den Auftragnemer) E-Mail an <a href=\"mailto:[%privacymail%]\">[%privacymail%]</a> dem Schriftformerfordernis.</li>\n<li>Datenschutzrechtliche Begriffe wie \"personenbezogene Daten\", \"verarbeiten\", \"Verantwortlicher\", \"Auftragsverarbeiter\", \"Datenschutz-Folgenabsch&auml;tzung\", etc. haben die ihnen in der EU-DSGVO oder, je nach Kontext, im Schweizer DSG zugeschriebene Bedeutung. \"Datensicherheits-Verletzung\" meint \"Verletzung des Schutzes personenbezogener Daten\" (englisch: \"Personal Data Breach\").</li>\n<li>Die Vertragsparteien unterwerfen sich hiermit der im Hosting-Vertrag festgelegten Gerichtsstands-Wahl f&uuml;r s&auml;mtliche Streitigkeiten sowie Anspr&uuml;che aus oder im Zusammenhang mit dieser ADV-Vereinbarung.</li>\n<li>Sollten einzelne oder mehrere Bestimmungen der ADV-Vereinbarung unwirksam oder nichtig sein oder werden, ber&uuml;hrt dies die Wirksamkeit der &uuml;brigen Bestimmungen nicht. An die Stelle der unwirksamen oder nichtigen Bestimmungen tritt diejenige Regelung, welche die Vertragsparteien bei Kenntnis des Mangels zum Zeitpunkt des Abschlusses der ADV-Vereinbarung nach Treu und Glauben sowie nach wirtschaftlicher Betrachtungsweise getroffen h&auml;tten. Entsprechendes gilt im Fall etwaiger L&uuml;cken der ADV-Vereinbarung.</li>\n</ol>\n</li>\n</ol>\n<br />[%place%],&nbsp;[%aktdatum%]\'),
                        (2, \'english\', \'Vereinbarung zur Auftragsdatenverarbeitung \', \'zwischen:<blockquote>[%kundendaten%]</blockquote>im Folgenden: <strong>Auftraggeber</strong><br /><br /><br /><br />und<blockquote><strong>[%name%]</strong><br />[%street%] [%streetnr%]<br />[%plz%] [%place%]<br />[%country%]</blockquote>im Folgenden: <strong>Auftragnehmer</strong><br /><br /> \n<ol>\n<li>\n<strong>Gegenstand und Anwendungsbereich der ADV-Vereinbarung</strong>\n<ol>\n<li>Diese Vereinbarung zur Auftragsdatenverarbeitung (\"ADV-Vereinbarung\") regelt die Pflichten, Rollen und Zust&auml;ndigkeiten vom&nbsp;Auftragnehmer und dem Auftraggeber&nbsp;in Bezug auf die Auftragsverarbeitung.</li>\n</ol>\n</li>\n<li>\n<strong>G&uuml;ltigkeit, Laufdauer, Verh&auml;ltnis zum Hosting-Vertrag</strong>\n<ol>\n<li>Der Auftragnehmer stellt diese ADV-Vereinbarung in den Einstellungen&nbsp;zum Abschluss in Bezug auf die bestellten&nbsp;Dienstleistungen bereit. Wenn der Auftraggeber der ADV-Vereinbarung durch Aktivierung eines Best&auml;tigungsfelds \"Zustimmen\"&nbsp;zustimmt, wird die ADV-Vereinbarung f&uuml;r die Vertragsparteien zum verbindlichen Bestandteil ihrer vertraglichen Vereinbarungen betreffend die Erbringung der Dienstleistungen. Sie gilt f&uuml;r die gesamte Dauer des Vertrags und gegebenenfalls dar&uuml;ber hinaus bis zur L&ouml;schung der von der Auftragsverarbeitung betroffenen personenbezogenen Daten (vgl. Ziff. 4.2) durch den Auftragnehmer.</li>\n<li>Die Bestimmungen dieser ADV-Vereinbarung erg&auml;nzen die Bestimmungen des Hosting-Vertrags. Sie schr&auml;nken die Rechte und Pflichten der Vertragsparteien in Bezug auf die Erbringung bzw. die Inanspruchnahme der Dienstleistungen nicht ein. Ihren Regelungsgegenstand betreffend gehen die Bestimmungen dieser ADV-Vereinbarung indes den Bestimmungen des Vertrags vor.</li>\n</ol>\n</li>\n<li>\n<strong>Anwendungsbereich der ADV-Vereinbarung</strong>\n<ol>\n<li>Diese ADV-Vereinbarung gilt (sobald ihr der Auftraggeber zugestimmt hat) in Bezug auf Auftragsverarbeitungen im Rahmen der vom Auftragnehmer gem&auml;ss Vertrag erbrachten Dienstleistungen.</li>\n<li>Diese ADV-Vereinbarung gilt ausdr&uuml;cklich nicht in Bezug auf Verarbeitungen personenbezogener Daten, bei denen der Auftragnehmer die Zwecke und Mittel der Verarbeitung bestimmt und somit unter dem Schweizerischen Bundesgesetz &uuml;ber den Datenschutz (DSG) oder allenfalls anwendbaren anderen Datenschutzgesetzen (insbesondere der EU-DSGVO) verantwortlich ist.&nbsp;Solche Verarbeitungen personenbezogener Daten, die der Auftragnehmer als Verantwortlicher vornimmt (z.B. Verarbeitungen personenbezogener Daten im Rahmen von E-Mails oder zu Zwecken der Leistungsabrechnung oder der Kommunikation mit dem Kunden) nimmt der Auftragnehmer in &uuml;bereinstimmung mit der Datenschutzerkl&auml;rung vom Auftragnehemer&nbsp;und den anwendbaren Datenschutzgesetzen vor.</li>\n</ol>\n</li>\n<li>\n<strong>Angaben zur Auftragsverarbeitung</strong>\n<ol>\n<li>Gegenstand und Zweck der Auftragsverarbeitung ist die Erbringung von Dienstleistungen durch den Auftragnehmer f&uuml;r den Auftraggeber. Die Auftragsverarbeitung besteht in der Speicherung, Bereitstellung, &uuml;bermittlung und L&ouml;schung von personenbezogenen Daten gem&auml;ss den Bestimmungen des Hosting-Vertrags.</li>\n<li>Von der Auftragsverarbeitung betroffen sind personenbezogene Daten, die der Kunde gem&auml;ss seiner Wahl auf der vom Auftragnehmer&nbsp;f&uuml;r die Leistungserbringung eingesetzten Infrastruktur speichert sowie Daten von Personen, denen der Auftraggeber Zugriff auf seine E-Mails oder Dokumenten gew&auml;hrt. Dabei handelt es sich insbesondere um personenbezogene Daten, die beim Aufrufen bzw. Ausf&uuml;hren und der Nutzung von E-Mails und weiteren Applikationen &uuml;blicherweise erhoben werden. Dazu geh&ouml;ren Protokolldaten, die bei der informatorischen Nutzung eines E-Mailservers oder einer Applikation automatisiert erhoben werden (z.B. die IP-Adresse und das Betriebssystem des Ger&auml;ts des Nutzers sowie das Datum und die Zugriffszeit des Browsers), vom Nutzer eingegebene Daten sowie vom Kunden erhobene Nutzungsdaten mit Personenbezug (nachstehend \"<strong>personenbezogene Daten</strong>\").</li>\n</ol>\n</li>\n<li>\n<strong>Rollen und Zust&auml;ndigkeitsbereiche</strong>\n<ol>\n<li>Der Auftraggeber best&auml;tigt und der Auftragnehmer anerkennt, dass der Auftraggeber f&uuml;r die Verarbeitung der personenbezogenen Daten nach anwendbaren Datenschutzgesetzen verantwortlich ist und bleibt. Der Auftraggeber nimmt somit die Rolle des Verantwortlichen ein.</li>\n<li>Der Auftragnehmer anerkennt, dass der Auftraggeber in der Rolle des Verantwortlichen verpflichtet ist, der Auftragnehmer bei Inanspruchnahme von Dienstleistungen einige seiner Pflichten aus der EU-DSGVO (oder anderen allenfalls anwendbaren Datenschutzgesetzen) vertraglich zu &uuml;berbinden.</li>\n<li>Der Auftragnemer nimmt in Bezug auf die Verarbeitung betroffener personenbezogener Daten die Rolle des Auftragsverarbeiters ein. Sofern der Auftragnehmer f&uuml;r diese Auftragsverarbeitung nicht ebenfalls der EU-DSGVO (oder den anderen allenfalls anwendbaren Datenschutzgesetzen) untersteht, so nimmt der Auftragnemer diese Rolle nur auf der Grundlage der vertraglichen Pflichten von dem Auftragnemer gem&auml;ss dieser ADV-Vereinbarung ein und wird nicht alleine deswegen unter der EU-DSGVO (oder den anderen allenfalls anwendbaren Datenschutzgesetzen) verpflichtet.</li>\n</ol>\n</li>\n<li>\n<strong>Pflichten vom Auftragnehmer</strong>\n<ol>\n<li>Der Auftragnemer verpflichtet sich, die personenbezogenen Daten nur zur Erbringung der Dienstleistungen gem&auml;ss Bestellung und vertraglichen Pflichten sowie gem&auml;ss dieser ADV-Vereinbarung zu verarbeiten.</li>\n<li>Der Auftragnemer ist dazu berechtigt, personenbezogene Daten des Kunden so zu verarbeiten, wie es die Erf&uuml;llung der Leistungspflichten aus dem Vertrag sowie dieser ADV-Vereinbarung beinhaltet. Auf entsprechende Anfrage ist der Auftragnemer bereit, weitergehende, die Auftragsverarbeitung betreffende Weisungen des Auftraggebers umzusetzen. Voraussetzung daf&uuml;r ist, dass diese f&uuml;r den Auftragnemer im Rahmen der vertraglich vereinbarten Dienstleistungen umsetzbar und objektiv zumutbar sind und nicht zu Mehrkosten oder ge&auml;ndertem Leistungsumfang f&uuml;hren. Vorbehalten bleibt in jedem Fall die Erf&uuml;llung gesetzlicher oder regulatorischer Pflichten, denen der Auftragnemer unterliegt.</li>\n<li>Der Auftragnehmer sorgt f&uuml;r die Einhaltung der Bestimmungen dieser ADV-Vereinbarung durch die mit der Auftragsverarbeitung betrauten Mitarbeiter und anderen f&uuml;r den Auftragnehmer t&auml;tigen Personen, die Zugriff auf die personenbezogenen Daten erhalten. Der Auftragnehmer verpflichtet sich zudem, Personen mit Zugang zu den personenbezogenen Daten zur Wahrung der Vertraulichkeit (auch &uuml;ber die Dauer ihrer T&auml;tigkeit und dar&uuml;ber hinaus) zu verpflichten.</li>\n<li>Der Auftragnehmer verpflichtet sich, im Interesse der Vertraulichkeit, Integrit&auml;t und vertragsgem&auml;ssen Verf&uuml;gbarkeit der personenbezogenen Daten angemessene technische und organisatorische Massnahmen zu treffen. Der Auftragnehmer implementiert insbesondere Zugangskontrollen, Zugriffskontrollen sowie Verfahren zur regelm&auml;ssigen &uuml;berpr&uuml;fung, Bewertung und Evaluierung der Wirksamkeit der technischen und organisatorischen Massnahmen. Bei der Auswahl der Massnahmen ber&uuml;cksichtigt der Auftragnemer den Stand der Technik, die Implementierungskosten sowie die Art, den Umfang, die Umst&auml;nde und die Zwecke der Verarbeitung sowie die unterschiedliche Eintrittswahrscheinlichkeit und Schwere des Risikos f&uuml;r betroffene Personen. Die jeweils geltenden Massnahmen ergeben sich aus den aktuellen Leistungsbeschrieben vom Auftragnehmer.</li>\n<li>Der Auftragnemer verpflichtet sich, den Kunden ohne Verzug schriftlich zu informieren, wenn der Auftragnemer Kenntnis von einer Datensicherheits-Verletzung erlangt, die personenbezogene Daten betrifft. Dabei hat der Auftragnemer dem Kunden die Art und das Ausmass der Verletzung sowie m&ouml;gliche Abhilfemassnahmen mitzuteilen. Die Vertragsparteien treffen gemeinsam die erforderlichen Massnahmen, um den Schutz der personenbezogenen Daten sicherzustellen und m&ouml;gliche nachteilige Folgen f&uuml;r die betroffenen Personen zu mildern. &uuml;berdies verpflichtet sich der Auftragnemer, dem Auftraggeber auf schriftliche Anfrage ausreichende Informationen zur Verf&uuml;gung zu stellen, damit dieser seinen Pflichten gem&auml;ss EU-DSGVO oder anderen anwendbaren Datenschutzgesetzen betreffend die Meldung, Untersuchung und Dokumentation von Datensicherheits-Verletzungen erf&uuml;llen kann.</li>\n<li>Der Auftraggeber verpflichtet sich, den Kunden auf schriftliche Anfrage und gegen separate angemessene Verg&uuml;tung sowie im Rahmen der betrieblichen Ressourcen und M&ouml;glichkeiten vom Auftragnemer bei der Erf&uuml;llung von Betroffenenrechten (insbesondere Auskunfts-, Berichtigungs- und L&ouml;- schungsrechten) durch den Kunden (personenbezogene Daten betreffend) gem&auml;ss Kapitel III der EU-DSGVO (oder &auml;quivalenten Bestimmunen anderer anwendbarer Datenschutzgesetze) zu unterst&uuml;tzen. Richtet sich eine betroffene Person mit Forderungen betreffend die Erf&uuml;llung von Betroffenenrechten direkt an den Auftragnemer, wird der Auftragnemer&nbsp;die betroffene Person an den Auftraggeber verweisen. Voraussetzung daf&uuml;r ist, dass der Auftragnemer eine solche Zuordnung an den Auftraggeber gest&uuml;tzt auf die Angaben der betroffenen Person vornehmen kann.</li>\n<li>Der Auftraggeber ist verpflichtet, den Auftragnemer ohne Verzug schriftlich zu benachrichtigen, wenn der Auftragnemer eine Anfrage (z.B. ein Auskunfts- oder L&ouml;schungsbegehren) von einer betroffenen Person in Bezug auf personenbezogene Daten erh&auml;lt; vorausgesetzt eine Zuordnung an den Auftraggeber ist gest&uuml;tzt auf die Angaben der betroffenen Person m&ouml;glich.</li>\n<li>Der Auftragnemer ist auf schriftliche Anfrage und gegen separate angemessene Verg&uuml;tung sowie unter Ber&uuml;cksichtigung der betrieblichen Ressourcen und M&ouml;glichkeiten vom Auftragnemer bereit, den Kunden bei Datenschutz-Folgenabsch&auml;tzungen und bei Konsultationen der Aufsichtsbeh&ouml;rden zu unterst&uuml;tzen.</li>\n<li>Der Auftragnemer wird die personenbezogenen Daten nach Ende der Laufdauer des Vertrags gem&auml;ss den Bestimmungen des Vertrags herausgeben oder l&ouml;schen.</li>\n</ol>\n</li>\n<li>\n<strong>Beizug von Unter-Auftragsverarbeitern</strong>\n<ol>\n<li>Beansprucht der Auftraggeber Dienstleistungen vom Auftraggeber, die personenbezogene Daten betreffen und durch Dritte erbracht werden, bleibt der Auftraggeber gegen&uuml;ber dem Auftragnemer Auftragsverarbeiter und erf&uuml;llt die diesbez&uuml;glichen Pflichten aus der ADV-Vereinbarung. Der Anbieter der Drittdienstleistung, die in der Dienstleistung vom Auftragnemer&nbsp;integriert wird, ist Unter-Auftragsverarbeiter vom Auftraggeber. Davon zu unterscheiden sind F&auml;lle, in denen der Auftragnemer dem Auftraggeber einen direkten Vertragsschluss mit dem Drittdienstleister vermittelt und der Drittdienstleister direkt Auftragsverarbeiter des Auftraggebers wird. In solchen F&auml;llen hat der Auftraggeber selber daf&uuml;r besorgt zu sein, unter anwendbaren Datenschutzgesetzen allenfalls notwendige Vereinbarungen mit dem Drittdienstleister zu treffen.</li>\n<li>Der Auftragnemer ist berechtigt, Unter-Auftragsverarbeiter im Rahmen der Erbringung der Dienstleistungen vom Auftragnemer beizuziehen. Der Auftragnemer ist in solchen F&auml;llen verpflichtet, mit Unter-Auftragsverarbeitern im erforderlichen Umfang eine Vereinbarung zu treffen, die der Auftragnemer die Einhaltung der Bestimmungen dieser ADV-Vereinbarung erm&ouml;glicht.</li>\n<li>Der Auftraggeber wird den Kunden vorab in geeigneter Weise informieren, wenn der Auftragnemer nach Inkrafttreten dieser ADV-Vereinbarung in Bezug auf bestehende Dienstleistungen neue Unter-Auftragsverarbeiter beizieht oder bestehende austauscht. Wenn der Auftraggeber dem nicht innerhalb von dreissig (30) Tagen nach dem Datum der Mitteilung aus wichtigen datenschutzrechtlichen Gr&uuml;nden widerspricht, gilt der neue oder ausgetauschte Unter-Auftragsverarbeiter als genehmigt.</li>\n<li>Wenn die Unter-Auftragsverarbeitung eine &uuml;bermittlung von personenbezogenen Daten in ein Land ausserhalb des Gebiets der EU/EWR/Schweiz beinhaltet, stellt der Auftragnemer sicher, dass der Auftragnemer die Bestimmungen der EU-DSGVO (oder &auml;hnlicher Bestimmungen des Schweizer DSG) betreffend die Daten&uuml;bermittlung in ein Drittland einh&auml;lt (z.B. durch Auswahl eines Unter-Auftragsverarbeiters, der dem U.S.-Swiss Privacy Shield unterstellt ist, oder durch Miteinbezug anerkannter Standardvertragsklauseln f&uuml;r die &uuml;bermittlung personenbezogener Daten an Auftragsverarbeiter in Drittl&auml;nder).</li>\n</ol>\n</li>\n<li>\n<strong>Pflichten des Auftraggeber</strong>\n<ol>\n<li>Der Auftraggeber ist f&uuml;r die Rechtm&auml;ssigkeit der Verarbeitung der personenbezogenen Daten, einschliesslich der Zul&auml;ssigkeit der Auftrags- bzw. Unter-Auftragsverarbeitung, verantwortlich.</li>\n<li>Der Auftraggeber trifft in seinem Verantwortungsbereich selbstst&auml;ndig angemessene technische und organisatorische Massnahmen zum Schutz der personenbezogenen Daten.</li>\n<li>Der Auftraggeber verpflichtet sich, dem Auftragnemer unverz&uuml;glich zu informieren, wenn der Auftraggeber in der Leistungserbringung vom Auftragnemer Verletzungen von anwendbaren Datenschutzgesetzen feststellt.</li>\n</ol>\n</li>\n<li>\n<strong>Informations- und Pr&uuml;fungsrechte</strong>\n<ol>\n<li>Der Auftragnemer ist verpflichtet, dem Auftraggeber auf schriftliche Anfrage alle Informationen zur Verf&uuml;gung zu stellen, die dieser vern&uuml;nftigerweise zum Nachweis der Einhaltung dieser ADV-Vereinbarung gegen&uuml;ber betroffenen Personen oder Datenschutzaufsichtsbeh&ouml;rden ben&ouml;tigt.</li>\n<li>Der Auftragnemer erm&ouml;glicht dem Kunden oder einem vom Auftraggeber beauftragten und zur Vertraulichkeit verpflichteten Pr&uuml;fer, die Einhaltung dieser ADV-Vereinbarung durch den Auftragnemer zu pr&uuml;fen. Werden nach Vorlage entsprechender Nachweise Verletzungen der ADV-Vereinbarung durch den Auftragnemer festgestellt, hat der Auftragnemer unverz&uuml;glich und kostenlos geeignete Korrekturmassnahmen zu implementieren.</li>\n<li>Die vorstehenden Informations- und Pr&uuml;fungsrechte des Auftraggebers bestehen nur insoweit, als der Vertrag dem Auftraggeber keine anderen Informations- und Pr&uuml;fungsrechte einr&auml;umt, die den einschl&auml;gigen Anforderungen der anwendbaren Datenschutzgesetze entsprechen. Weiter stehen diese Informations- und Pr&uuml;fungsrechte unter dem Vorbehalt des Verh&auml;ltnism&auml;ssigkeitsgebots und der Wahrung der schutzw&uuml;rdigen Interessen (insbesondere Sicherheits- oder Geheimhaltungsinteressen) vom Auftragnemer. Vorbeh&auml;ltlich einer anderslautenden Vereinbarung zwischen den Vertragsparteien tr&auml;gt der Auftraggeber s&auml;mtliche Kosten der&nbsp;Information und Pr&uuml;fung, einschliesslich nachgewiesener interner Kosten vom Auftragnemer.</li>\n</ol>\n</li>\n<li>\n<strong>&auml;nderungen dieser ADV-Vereinbarung</strong>\n<ol>\n<li>Der Auftragnemer beh&auml;lt sich vor, diese ADV-Vereinbarung zu &auml;ndern, (a) wenn dies zur Anpassung an Rechtsentwicklungen erforderlich ist oder (b) wenn dies nicht zu einer Verschlechterung der Gesamtsicherheit der Auftragsverarbeitung f&uuml;hrt und sich (nach Ermessen vom Auftragnemer) nicht erheblich nachteilig auf die Rechte der von der Auftragsverarbeitung betroffenen Personen auswirkt.</li>\n<li>Der Auftragnemer teilt dem Kunden beabsichtigte &auml;nderungen dieser ADV-Vereinbarung gem&auml;ss Ziff. 10.1 sp&auml;testens dreissig (30) Tage vor Wirksamwerden mit. Wenn der Auftraggeber der &auml;nderung widersprechen m&ouml;chte, kann er die ADV-Vereinbarung innerhalb von dreissig (30) Tagen ab Datum der Mitteilung k&uuml;ndigen. Ohne Widerspruch innerhalb dieser Frist gilt die &auml;nderung als genehmigt.</li>\n</ol>\n</li>\n<li>\n<strong>Generelle Bestimmungen</strong>\n<ol>\n<li>In Abweichung allf&auml;lliger im Hosting-Vertrag vereinbarter Schriftformvorbehalte kann die ADV-Vereinbarung auf elektronischem Weg zwischen den Vertragsparteien vereinbart oder ge&auml;ndert werden.</li>\n<li>Verlangt diese ADV-Vereinbarung eine schriftliche Aufforderung oder Mitteilung, so gen&uuml;gt (f&uuml;r Mitteilungen an den Kunden) E-Mail an die Adresse des Kunden bzw. (f&uuml;r Mitteilungen an den Auftragnemer) E-Mail an <a href=\"mailto:[%privacymail%]\">[%privacymail%]</a> dem Schriftformerfordernis.</li>\n<li>Datenschutzrechtliche Begriffe wie \"personenbezogene Daten\", \"verarbeiten\", \"Verantwortlicher\", \"Auftragsverarbeiter\", \"Datenschutz-Folgenabsch&auml;tzung\", etc. haben die ihnen in der EU-DSGVO oder, je nach Kontext, im Schweizer DSG zugeschriebene Bedeutung. \"Datensicherheits-Verletzung\" meint \"Verletzung des Schutzes personenbezogener Daten\" (englisch: \"Personal Data Breach\").</li>\n<li>Die Vertragsparteien unterwerfen sich hiermit der im Hosting-Vertrag festgelegten Gerichtsstands-Wahl f&uuml;r s&auml;mtliche Streitigkeiten sowie Anspr&uuml;che aus oder im Zusammenhang mit dieser ADV-Vereinbarung.</li>\n<li>Sollten einzelne oder mehrere Bestimmungen der ADV-Vereinbarung unwirksam oder nichtig sein oder werden, ber&uuml;hrt dies die Wirksamkeit der &uuml;brigen Bestimmungen nicht. An die Stelle der unwirksamen oder nichtigen Bestimmungen tritt diejenige Regelung, welche die Vertragsparteien bei Kenntnis des Mangels zum Zeitpunkt des Abschlusses der ADV-Vereinbarung nach Treu und Glauben sowie nach wirtschaftlicher Betrachtungsweise getroffen h&auml;tten. Entsprechendes gilt im Fall etwaiger L&uuml;cken der ADV-Vereinbarung.</li>\n</ol>\n</li>\n</ol>\n<br />[%place%],&nbsp;[%aktdatum%]\');');

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsVorlageImport'] .' <br />';
            $MSG .= '</div>';
        }

        // Module importieren
        if ((isset($_REQUEST['import']) && $_REQUEST['import'] == 'module')) {
            // Alte einträge löschen
            $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_module');

            // Neue einträge importieren
            $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_module` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Name` varchar(100) DEFAULT NULL,
                      `DBTabelle` varchar(100) DEFAULT NULL,
                      `DBSearchTabelle` varchar(100) DEFAULT NULL,
                      `DBSearchType` varchar(10) DEFAULT NULL,
                      `Spalten` varchar(1000) DEFAULT NULL,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1;");
            $db->Query("INSERT INTO `bm60_mod_ds_module` (`Name`, `DBTabelle`, `DBSearchTabelle`, `DBSearchType`, `Spalten`) VALUES
                        ('Datenschutz Report', 'bm60_mod_ds_report', 'UserID', 'userid', 'a:4:{i:0;s:4:\"Lang\";i:1;s:7:\"JobDate\";i:2;s:9:\"JobUpdate\";i:3;s:12:\"DokumentSize\";}'),
                        ('Signaturen', 'bm60_signaturen', 'user', 'userid', 'a:3:{i:0;s:5:\"titel\";i:1;s:4:\"text\";i:2;s:4:\"html\";}'),
                        ('E-Mail-Aliase', 'bm60_aliase', 'user', 'userid', 'a:2:{i:0;s:5:\"email\";i:1;s:4:\"date\";}');");

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsVorlageImport'] .' <br />';
            $MSG .= '</div>';
        }

        // Impressum importieren
        if ((isset($_REQUEST['import']) && $_REQUEST['import'] == 'impressum')) {
            // Alte einträge löschen
            $db->Query('DROP TABLE IF EXISTS {pre}mod_ds_impressum');

            // Neue einträge importieren
            $db->Query("CREATE TABLE IF NOT EXISTS `{pre}mod_ds_impressum` (
                      `ID` int(11) NOT NULL AUTO_INCREMENT,
                      `Lang` varchar(10) DEFAULT NULL,
                      `Text` varchar(200) DEFAULT NULL,
                      `Content` text,
                      PRIMARY KEY (`ID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            foreach ($languages as $langID => $lang) {
                $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`Lang`, `Text`, `Content`) VALUES (\''. $langID .'\', \'ImpressumContent\', \'<div class="page-header"><h4>Diensteanbieter</h4></div>[%name%]<br />[%street%]&nbsp;[%streetnr%]<br />[%plz%]&nbsp;[%place%]<br />[%country%]<div class="page-header"><h4>Kontaktm&ouml;glichkeiten</h4></div>E-Mail-Adresse: [%contactmail%]<br />Telefon: [%phone%]<br />Kontaktformular: [%contactform%]<br />Weitere Kontaktmöglichkeiten:&nbsp;[%contactmore%]<div class="page-header"><h4>Vertretungsberechtigte</h4></div>Vertretungsberechtigt: [%authorized%]<div class="page-header"><h4>Angaben zum Unternehmen</h4></div>Umsatzsteuer Identifikationsnummer (USt-ID): [%ustid%]<br />Wirtschafts-Identifikationsnummer (W-IdNr): [%widnr%]<br />Geschäftsbereich: [%businessarea%]<br />AGB: [%agburl%]<div class="page-header"><h4>Register und Registernummer</h4></div>Unternehmensregister<br />Geführt bei: [%registrycourt%]<br />Nummer: [%registrycourtnr%]\');');
                $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`Lang`, `Text`, `Content`) VALUES (\''. $langID .'\', \'ConsumerDisputeResolution\', \'Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit, die Sie unter https://ec.europa.eu/consumers/odr/ finden. Verbraucher haben die Möglichkeit, diese Plattform für die Beilegung ihrer Streitigkeiten zu nutzen.\');');
                $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`Lang`, `Text`, `Content`) VALUES (\''. $langID .'\', \'SocialMedia\', \'Dieses Impressum gilt auch für die folgenden Social-Media-Präsenzen und Onlineprofile: \r\nhttps://facebook.com/muster_beispiel_seite\r\nhttps://instagram.com/muster_account\r\nhttps://www.xing.com/profile/Musterprofile\');');
                $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`Lang`, `Text`, `Content`) VALUES (\''. $langID .'\', \'CopyrightNotices\', \'Die Inhalte dieses Onlineangebotes wurden sorgfältig und nach unserem aktuellen Kenntnisstand erstellt, dienen jedoch nur der Information und entfalten keine rechtlich bindende Wirkung, sofern es sich nicht um gesetzlich verpflichtende Informationen (z.B. das Impressum, die Datenschutzerklärung, AGB oder verpflichtende Belehrungen von Verbrauchern) handelt. Wir behalten uns vor, die Inhalte vollständig oder teilweise zu ändern oder zu löschen, soweit vertragliche Verpflichtungen unberührt bleiben. Alle Angebote sind freibleibend und unverbindlich.\');');
                $db->Query('INSERT IGNORE INTO `{pre}mod_ds_impressum` (`Lang`, `Text`, `Content`) VALUES (\''. $langID .'\', \'PhotoCredits\', \'Bildquellen und Urheberrechtshinweise: \r\nhttps://mail.town\');');
            }

            $MSG  = '<div class="infostate-ok">';
            $MSG .= $lang_admin['dsVorlageImport'] .' <br />';
            $MSG .= '</div>';
        }

        // PHP Infos
        $phpmemory = ini_get('memory_limit');

        // PHP Erweiterungen
        if (extension_loaded('gd'))         {   $PHPExtGD = 'datenschutz_check.png';          } else {    $PHPExtGD = 'datenschutz_uncheck.png'; }
        if (extension_loaded('mbstring'))   {   $PHPExtMBSTRING = 'datenschutz_check.png';    } else {    $PHPExtMBSTRING = 'datenschutz_uncheck.png'; }

        // TCPDF Class
        $TCPDFOn = FALSE;
        if (@file_exists('../plugins/php/tcpdf/tcpdf.php')) {
            $TCPDF = $lang_admin['dsTCPDFOn'];
        } else {
            $TCPDF = $lang_admin['dsTCPDFOff'];
        }

        // assign
        $tpl->assign('PHPMemory',       $phpmemory);
        $tpl->assign('PHPExtGD',        $PHPExtGD);
        $tpl->assign('PHPExtMBSTRING',  $PHPExtMBSTRING);
        $tpl->assign('TCPDF',           $TCPDF);
        $tpl->assign('Version',         $this->version);
        $tpl->assign('Datum',           date('d.m.Y', time()));
        $tpl->assign('pageURL',         $this->_adminLink());
        $tpl->assign('MSG',             $MSG);
        $tpl->assign('page',            $this->_templatePath('datenschutz.acp.hilfe.tpl'));
    }
}

$plugins->registerPlugin('Datenschutz');

?>