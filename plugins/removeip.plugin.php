<?php
class RemoveIPPlugin extends BMPlugin 
{
	function RemoveIPPlugin()
	{
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'RemoveIP Plugin';
		$this->author			= 'Patrick Schlangen';
		$this->version			= '1.0.0';
		$this->update_url		= 'http://my.b1gmail.com/update_service/';
	}
	
	function AfterInit()
	{
		global $bm_prefs;
		
		// set IP to 0.0.0.0
		$_SERVER['REMOTE_ADDR'] = '0.0.0.0';
		
		// disable reg IP lock
		$bm_prefs['reg_iplock']	= 0;
	}
}

$plugins->registerPlugin('RemoveIPPlugin');
?>