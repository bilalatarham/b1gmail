<?php
/*
 * Copyright (c) 2018, Home of the Sebijk.com
 * http://www.sebijk.com
 */
class moduserexport extends BMPlugin
{
	/*
	* Eigenschaften des Plugins
	*/
	function moduserexport()
	{
		$this->name					= 'Benutzer-Export';
		$this->version				= '1.0.1';
		$this->designedfor			= '7.3.0';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'Home of the Sebijk.com';
		$this->web					= 'http://www.sebijk.com';
		$this->mail					= 'sebijk@web.de';

		$this->update_url			= 'http://my.b1gmail.com/update_service/';
	}

	/*
	 * installation routine
	 */
	function Install()
	{
		global $db;


		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		global $db;

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 *  Sprachvariablen
   */
    function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
    {
            global $lang_user;
            if(strpos($lang, 'deutsch') !== false) {
                    $lang_user['userdata_export'] = "Benutzerdaten exportieren";
		$lang_user['prefs_d_userdata_export']	= 'Hier können Sie Ihre Benutzerdaten im JSON-Format herunterladen.';
            }
            else {
                    $lang_user['userdata_export'] = "Export User data";
			$lang_user['prefs_d_userdata_export']	= 'Export your user data as JSON Format.';
            }
    }

	/**
	 * User area setup
	 *
	 * @param string $file
	 * @param string $action
	 */
	function FileHandler($file, $action)
	{
		global $lang_user, $thisGroup;

		if($file == 'prefs.php')
		{
			$GLOBALS['prefsItems']['userdata_export'] = true;
			$GLOBALS['prefsImages']['userdata_export'] = 'templates/modern/images/li/prefs_membership.png';
			$GLOBALS['prefsIcons']['userdata_export'] = 'templates/modern/images/li/ico_membership.png';
		}
	}

	/**
	 * User area
	 *
	 * @param string $action
	 * @return bool
	 */
	function UserPrefsPageHandler($action)
	{
		global $lang_user, $tpl, $userRow, $thisUser;

		if($action != 'userdata_export')
			return(false);

		$exportuserRow = $userRow;
		$exportuserRow['emailaliases'] = $thisUser->GetAliases();
		$exportuserRow['signatures'] = $thisUser->GetSignatures();
		$exportuserRow['pop3accounts'] = $thisUser->GetPOP3Accounts();
		$exportuserRow['autoresponders'] = $thisUser->GetAutoresponder();
		$exportuserRow['spamindexsize'] = $thisUser->GetSpamIndexSize();
		$exportuserRow['filters'] = $thisUser->GetFilters('orderpos', 'asc');
		$exportuserRow['generatedAt'] = time();
		$exportuserRow['Generator'] = "b1gMail ".$this->name." ".$this->version;
		unset($exportuserRow['id']);
		unset($exportuserRow['passwort']);
		unset($exportuserRow['passwort_salt']);
		header('Content-Type: application/json; charset=utf-8');
    		header('Content-Disposition: attachment; filename="benutzerdaten.json"');
		echo json_encode($exportuserRow);
		//print_r($exportuserRow);
	}
}
/**
 * register plugin
 */
$plugins->registerPlugin('moduserexport');
