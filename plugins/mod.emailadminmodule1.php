<?php
$MODULE_CALL = 'modMailinglist';

class modMailinglist extends BMPlugin
{
	// Informationen zum Modul
	function modMailinglist()
	{
		$reg = array();
		//ereg('([0-9]*)\.([0-9]*)', '$Revision: 1.0 $', $reg);

		$this->name		= 'eMailAdmin Module 1';
		$this->author		= 'eMailAdmin';
		$this->web			= 'http://www.emailadmin.eu/';
		$this->mail			= 'info@emailadmin.eu';
		$this->version		= "1.0";
	}

	// Install
	function Install()
	{
#		global $db;
#		$db->Query("CREATE TABLE `emp_mailinglist` (
#						  `id` int(11) NOT NULL auto_increment,
#						  `adress` varchar(255) NOT NULL default '',
#						  `name` varchar(255) NOT NULL default '',
#						  `empadmin` int(11) NOT NULL default '0',
#						  PRIMARY KEY  (`id`)
#						) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
#		$db->Query("CREATE TABLE `emp_mailinglistempf` (
#						  `id` int(11) NOT NULL auto_increment,
#						  `email` varchar(255) NOT NULL default '',
#						  `list` int(11) NOT NULL default '0',
#						  `empadmin` int(11) NOT NULL default '0',
#						  PRIMARY KEY  (`id`)
#						) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
#		$db->Query("CREATE TABLE `emp_users` (
#						  `id` int(11) NOT NULL auto_increment,
#						  `name` varchar(255) NOT NULL default '',
#						  `password` varchar(255) NOT NULL default '',
#						  `rights` int(11) NOT NULL default '0',
#						  `domains` text NOT NULL,
#						  `maxusers` bigint(20) NOT NULL default '0',
#						  `users` bigint(20) NOT NULL default '0',
#						  PRIMARY KEY  (`id`),
#						  UNIQUE KEY `name` (`name`),
#						  UNIQUE KEY `name_2` (`name`)
#						) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
#
#		$db->Query("CREATE TABLE `emp_mailinglistthreads` (
#						  `id` int(11) NOT NULL auto_increment,
#						  `subject` varchar(255) NOT NULL default '',
#						  PRIMARY KEY  (`id`)
#						) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

		return(true);
	}

	// Uninstall
	function Uninstall()
	{
		/* ... und hier gel�scht.*/
		return(true);
	}

	// Mail-Empfang
	function OnReceive2(&$mail, &$user, &$s)
	{
		global $db;
		$sql = $db->Query("SELECT adress,empadmin FROM emp_mailinglist");
		$list = false;
		while($row = $sql->FetchArray()) {
			if($user->szMail == $row["adress"]) {
			$list = true; $empadmin=$row["empadmin"]; }
		}
		$sql->Free();
		if($list){
			$subject = trim($mail->GetHeader('subject'));
			$from = Mail::ExtractMailAddress($mail->GetHeader('from'));
			$return = trim($mail->GetHeader('return-path'));
			$regs = array();
			if((ereg("\[M:([0-9]+)\]",$subject,$regs)) && (strstr($from, 'MAILER-DAEMON@') === false) && $return != "") {
				$sql = $db->Query("SELECT subject FROM `emp_mailinglistthreads` WHERE `id` = ?",$regs[1]);
				$row = $sql->FetchArray();
				if($row['subject'] != "") {
					$sql = $db->Query("SELECT name,adress,id FROM `emp_mailinglist` WHERE `adress` = ?",$user->szMail);
					$row = $sql->FetchArray();
					$mlid = $row['id'];
					$absendern = $row['name'];
					$abadress = $row['adress'];
					$sql = $db->Query("SELECT email FROM `emp_mailinglistempf` WHERE `list` = ?",$mlid);
					$i = 0;
					while($row = $sql->FetchArray()) {
					  	$user = $row["email"];
						$msg = new MIMEMail2(false, false);
						$msg->to = $user;
						$msg->subject = $subject;
						$msg->systemmail = true;
						$msg->from_email = $abadress;
						$msg->from_name = $absendern;
						$msg->Write("MAIL FROM: <" . $from . ">\r\n");
						$msg->Write("RCPT TO: <" . $user . ">\r\n");
						$msg->Data();
						$msg->Write("X-Forward-To: <" . $user . ">\r\n");
						$msg->Write("Reply-To: <" . $abadress . ">\r\n");
						$msg->Write("Return-Path: <>\r\n");

						$s->SavePos();
						$s->Reset();
						while(($buff = $s->Read()) !== null)
						{
							if(!StartsWith(strtolower($buff), 'return-path:'))
							$msg->Write($buff . "\r\n");
						}
						unset($buff);
						$s->LoadPos();

						$msg->Finish();
						unset($msg);
						$i++;
					}
					PutLog('Mailinglist: E-Mail mit dem Subject \'' . $subject . '\' wurde an ' . $i . ' Benutzer weitergeleitet. Thread war bereits vorhanden.',PRIO_NOTE, __FILE__, __LINE__);
				}
				return BM_DELETE; //Nachricht kann gel�scht werden, da verarbeitet
			} elseif((strstr($from, 'MAILER-DAEMON@') === false) && $return != "") {
			    $db->Query("INSERT INTO `emp_mailinglistthreads` ( `subject`,`empadmin` )
				VALUES ( ? );",$subject,$empadmin);
				$nsubject = "[M:" . $db->InsertId() . "] " . $subject;
				$sql = $db->Query("SELECT name,adress,id FROM `emp_mailinglist` WHERE `adress` = ?",$user->szMail);
                $row = $sql->FetchArray();
                $mlid = $row['id'];
                $absendern = $row['name'];
                $abadress = $row['adress'];
                $sql = $db->Query("SELECT email FROM `emp_mailinglistempf` WHERE `list` = ?",$mlid);
                $i = 0;
                while($row = $sql->FetchArray()) {
                    $user = $row["email"];
                    $msg = new MIMEMail2(false, false);
                    $msg->to = $user;
                    $msg->subject = $nsubject;
                    $msg->systemmail = true;
                    $msg->from_email = $abadress;
                    $msg->from_name = $absendern;
                    $msg->Write("MAIL FROM: <" . $from . ">\r\n");
                    $msg->Write("RCPT TO: <" . $user . ">\r\n");
                    $msg->Data();
                    $msg->Write("X-Forward-To: <" . $user . ">\r\n");
                    $msg->Write("Reply-To: <" . $abadress . ">\r\n");
                    $msg->Write("Return-Path: <>\r\n");
                    $msg->Write("Subject: =?ISO-8859-15?B?" . base64_encode($nsubject) . "?=\r\n");

                    $s->SavePos();
                    $s->Reset();
                    while(($buff = $s->Read()) !== null)
                    {
                        if(!StartsWith(strtolower($buff), 'return-path:') && !StartsWith(strtolower($buff), 'subject:'))
                        $msg->Write($buff . "\r\n");
                    }
                    unset($buff);
                    $s->LoadPos();

                    $msg->Finish();
                    unset($msg);
                    $i++;
                }
				PutLog('Mailinglist: E-Mail mit dem Subject \'' . $subject . '\' konnte nicht identifiziert werden. Neuer Thread wurde erstellt und die Nachricht wurde an ' . $i . ' Benutzer weitergeleitet.',PRIO_NOTE, __FILE__, __LINE__);
				return BM_DELETE; //Nachricht kann gel�scht werden, da verarbeitet
			}
		} else {
			return BM_OK; //Keine Mailinglist
		}
	}
}
/**
 * register plugin
 */
$plugins->registerPlugin('modMailinglist');
?>
