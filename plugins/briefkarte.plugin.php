<?php

/*
 * plugin briefkarte v2.4 - 22.03.2013 - 12:30:00
 * copyright on the complete sourcecode by Informant
 * >>> www.speedloc.de <<<
 */
 
/* 
 * status-feld-info f�r datenbank
 * status = 0 -> vorschau
 * status = 1 -> an provider �bertragen
 * status = 2 -> druck ok und via post versendet
 * status = 3 -> ein fehler ist aufgetreten
 */

// testmodus = 1 ::: livemodus = 0
define("TRANSFERMODE", 0);
// fpdf-path
define("_fpdfDir", B1GMAIL_DIR . "serverlib/3rdparty/fpdf/");
// fpdf-font-path
define("_fpdfFontDir", B1GMAIL_DIR . "serverlib/3rdparty/fpdf/font/");

// bibliotheken laden
if(file_exists(_fpdfDir)
    || file_exists(_fpdfDir . 'fpdf.php')
    || file_exists(_fpdfDir . 'fpdi.php'))
    {
        require(_fpdfDir . 'fpdf.php');
        require(_fpdfDir . 'fpdi.php');
    }

//pr�fen, ob FPDI Verzeichnis hochgeladen wurde und einbinden der Klasse
if(class_exists('FPDI'))
{
    class PDF extends FPDI
    {
        var $signatur;
        
        // Fusszeile
        function Footer()
        {
            //Position 1,5 cm von unten
            $this->SetY(-15);
            //Arial kursiv 8
            $this->SetFont('Arial','I',8);
            // Linienbreite einstellen, 0.2 mm (standard) 
            $this->SetLineWidth(0.2);
            // Linienfarbe auf Blau einstellen 
            //this->SetDrawColor(0, 0, 255); 
            // Linie zeichnen
            $this->Line(10, 283, 200, 283);
            //Seitenzahl
            //$this->Cell(0,10,'Seite '.$this->PageNo().'/{nb}',0,0,'C');
            $this->Cell(0, 10, $this->signatur, 0, 0, 'C');
        }
        
        //signatur holen und zuweisen
        function setSignatur($sig)
        {
            $this->signatur = $sig;
        }
    }
}

class briefkarte extends BMPlugin
{
	/*
	 * Eigenschaften des Plugins
	 */
	function briefkarte()
	{
		$this->name					= 'Brief-Karte';
		$this->version				= '2.4';
		$this->designedfor			= '7.2';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'Informant';
		$this->update_url			= 'http://my.b1gmail.com/update_service/';

		// admin pages
		$this->admin_pages			= true;
		
        
        // pr�fen, ob die zur pdf erzeugung ben�tigten bibliotheken vorhanden sind
        if(!file_exists(_fpdfDir)
		|| !file_exists(_fpdfDir . 'fpdf.php')
		|| !file_exists(_fpdfDir . 'fpdi.php'))
        {
            PutLog('FPDF-Dir is not uploaded!', PRIO_WARNING, __FILE__, __LINE__);
        }
	}
    
   	function AfterInit()
	{
		global $lang_admin;
		
		// group option
        $this->RegisterGroupOption('briefkarte', FIELD_CHECKBOX, $lang_admin['bk_titel']);
		$this->RegisterGroupOption('bk_signatur', FIELD_TEXT, $lang_admin['bk_signatur']);
        $this->admin_page_title	= $lang_admin['bk_titel2'];
	}

	/*
	 *  Link und Tabs im Adminbereich 
	 */
	function AdminHandler()
	{
		global $tpl, $lang_admin;

		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'bkstats';

		$tabs = array(
            0 => array(
				'title'		=> $lang_admin['bk_statistik'],
				'link'		=> $this->_adminLink() . '&action=bkstats&',
				'active'	=> $_REQUEST['action'] == 'bkstats'),
			1 => array(
				'title'		=> $lang_admin['bk_karte']." - ".$lang_admin['prefs'],
				'link'		=> $this->_adminLink() . '&action=bkgateways&',
				'active'	=> $_REQUEST['action'] == 'bkgateways'),
			2 => array(
				'title'		=> $lang_admin['bk_brief']." - ".$lang_admin['prefs'],
				'link'		=> $this->_adminLink() . '&action=bkbgateways&',
				'active'	=> $_REQUEST['action'] == 'bkbgateways')
			);
		$tpl->assign('tabs', $tabs);

        if($_REQUEST['action'] == 'bkstats')
			$this->_gatewaysAdminBKstats();
		if($_REQUEST['action'] == 'bkgateways')
			$this->_gatewaysAdminPost();
		if($_REQUEST['action'] == 'bkbgateways')
			$this->_gatewaysAdminBrief();
	}

	/*
	 *  Sprach variablen
	 */
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			$lang_user['bk_main']   			  = 'Post';
			$lang_user['bk_karte']  	 		  = 'Postkarte versenden';
			$lang_user['bk_brief']  	 		  = 'Brief versenden';
			$lang_user['bk_journal']  	 		  = 'Journal';
			$lang_user['bk_to']  	 			  = 'Empf&auml;nger';
			$lang_user['bk_from']  	 			  = 'Absender';
			$lang_user['bk_de']  	 			  = 'Deutschland';
			$lang_user['bk_eu']  	 			  = 'Europa';
            $lang_user['bk_welt']  	 			  = 'Welt';
            $lang_user['bk_vn']  	 			  = 'Versand nach';
            $lang_user['bk_price']  	 		  = 'Preis';
            //postkarte
			$lang_user['bk_enter']  	 		  = 'Bitte benutzen Sie die Enter-Taste in Ihrem Text nicht mehr als';
			$lang_user['bk_pricewarning']  	 	  = 'Zum versenden der Postkarte reicht Ihr aktuelles Guthaben leider nicht aus. Bitte laden Sie Ihr Konto vor dem Senden der Postkarte auf.';
			$lang_user['bk_karte_sendok']  	 	  = 'Ihre Postkarte wurde erfolgreich &uuml;bermittelt.';
			$lang_user['bk_karte_senderror']  	  = 'Ihre Postkarte konnte nicht versendet werden. Ein tempor&auml;rer interner Fehler ist aufgetreten. Bitte versuchen Sie es sp&auml;ter erneut.';
			$lang_user['bk_absender_error']		  = 'Ihr Absender besteht aus mehr als 50 Zeichen. Damit Ihre Absender-Daten richtig �bermittelt werden k�nnen, nutzen Sie bitte maximal 50 Zeichen.';
			$lang_user['bk_karte_picerror']		  = 'Sie haben kein Bild ausgew&auml;hlt oder Ihr Bild besitzt ein falsches Format! Bitte nutzen Sie ein Bild im JPG-Format.';
			$lang_user['bk_karte_dpierror']		  = 'Die Aufl&ouml;sung Ihres Bildes ist zu gering. Bitte w&auml;hlen Sie ein Bild aus, welches mindestens 150 dpi hat.';
			$lang_user['bk_vorschau']  	 		  = 'Postkarten-Vorschau &raquo;';
			$lang_user['bk_versandnach']  	 	  = 'Wohin m&ouml;chten Sie die Postkarte versenden?';
			$lang_user['bk_dateiupload']  	 	  = 'Bitte w&auml;hlen Sie ein Bild aus, welches Sie als Motiv nutzen m&ouml;chten!';
			$lang_user['bk_karte_groesseerror']	  = 'Ihr Bild ist gr&ouml;&szlig;er als 2 Megabyte. Bitte w&auml;hlen Sie ein Bild aus, welches maximal 2 Megabyte gro&szlig; ist.';
			$lang_user['bk_dateiupload_info']	  = '<b>Hinweis:</b> Bitte beachten Sie, dass Ihr Bild maximal 2 Megabyte gro&szlig; sein darf, dessen Format JPG ist und es eine Aufl&ouml;sung von mindestens 150 dpi (15x10 cm) hat.';
			$lang_user['bk_to_z0']  	 		  = 'Verein oder Firma';
			$lang_user['bk_to_z1']  	 		  = 'Anrede, Vor- und Nachname';
			$lang_user['bk_to_z2']  	 	      = 'Stra&szlig;e und Hausnummer';
			$lang_user['bk_to_z3']  	 		  = 'Postleitzahl, Ort und Land';
			$lang_user['bk_from_z0']  	 		  = 'Postleitzahl, Ort';
			$lang_user['bk_from_z1']  	 		  = 'Vor- und Nachname';
			$lang_user['paybrieffailed']		  = 'Es ist ein Fehler in der Berechnung aufgetreten. Bitte wenden Sie sich an den technischen Support.';
			$lang_user['bk_typ']  	 			  = 'Typ';
            $lang_user['bk_datum']  	 		  = 'Datum';
            $lang_user['bk_status']  	 		  = 'Status';
            $lang_user['bk_prevueb1']  	 		  = 'Vorschau der Vorderseite';
            $lang_user['bk_prevueb2']  	 		  = 'Vorschau der R&uuml;ckseite';
            $lang_user['bk_prevhinw']  	 		  = 'Die Vorschau kann vom Original abweichen!';
            //brief
            $lang_user['b_absender']  	 	      = 'Absender';
			$lang_user['b_clickhere']  	 	      = 'hier klicken';
            $lang_user['b_es_status_offen']  	  = 'Derzeit liegt noch keine Sendungs-ID vor oder die Sendungs-ID ist bekannt, jedoch ist noch keine Einschreiben-ID zugewiesen! Versuchen Sie es zu einem anderen Zeitpunkt erneut.';
			$lang_user['b_einschreibencheck']  	  = 'Einschreiben Sendungsverfolgung';
            $lang_user['b_maxseitenerror']        = 'Sie haben die maximale Anzahl von m&ouml;glichen Seiten &uuml;berschritten!. Im Duplex-Modus k&ouml;nnen Sie bis zu 99 Seiten versenden und im normalen Modus bis zu 50 Seiten.';
            $lang_user['b_einschreiben']  	      = 'Einschreiben (Einwurf)';
			$lang_user['b_einschreiben_online']   = 'Einschreiben (Online-Quittung)';
			$lang_user['b_einschreiben_rs']  	  = 'Einschreiben (mit R&uuml;ckschein)';
            $lang_user['b_einschreiben_info']  	  = 'Einschreiben - Nachweis der Zustellung durch den Zusteller!';
            $lang_user['b_empf_info']  	 	      = 'Empf&auml;nger Informationen';
            $lang_user['b_duplex']  	 	      = 'Duplex-Druck (beidseitiger Druck)';
            $lang_user['b_firma']  	 	          = 'Firma/Verein';
            $lang_user['b_deckblatt']  	 	      = 'Deckblatt mit Absender und Empf&auml;nger?';
            $lang_user['b_db_info']  	 	      = 'Deckblatt generieren?';
            $lang_user['b_versandnach']  	 	  = 'Wohin m&ouml;chten Sie den Brief versenden?';
            $lang_user['b_dateiupload']  	 	  = 'Sofern Sie Ihren Brief bereits als PDF-Datei vorliegen haben, w&auml;hlen Sie diesen bitte hier aus!';
            $lang_user['b_farbe']  	        	  = 'Bitte w&auml;hlen Sie die gew&uuml;nschte Druckart?';
            $lang_user['b_farbe1']  	          = 'farbig';
            $lang_user['b_farbe2']  	       	  = 'schwarz/weis';
            $lang_user['b_textmsg']  	       	  = 'Eingabem&ouml;glichkeit f&uuml;r zus&auml;tzlichen Text oder um den Brief direkt zu schreiben';
            $lang_user['b_weiter']  	       	  = 'Brief-Vorschau und Preis-Berechnung &raquo;';
            $lang_user['b_wrongformat']  	   	  = 'Ihre hochgeladene PDF-Datei nutzt eine Format-Version, welche derzeit nicht unterst&uuml;tzt wird. Bitte nutzen Sie eine Version bis 1.4!';
            $lang_user['b_tmperror']  	          = 'Ihr Brief konnte nicht erstellt werden. Ein tempor&auml;rer interner Fehler ist aufgetreten. Bitte versuchen Sie es sp&auml;ter erneut.';
            $lang_user['b_brief_sendok']  	 	  = 'Ihr Brief wurde erfolgreich &uuml;bermittelt.';
            $lang_user['b_brief_senderror']       = 'Ihr Brief konnte nicht versendet werden. Ein tempor&auml;rer interner Fehler ist aufgetreten. Bitte versuchen Sie es sp&auml;ter erneut.';
            $lang_user['b_pricewarning']  	 	  = 'Zum versenden des Briefes reicht Ihr aktuelles Guthaben leider nicht aus. Bitte laden Sie Ihr Konto vor dem Senden des Briefes auf.';
            $lang_user['b_preview']  	      	  = 'Briefvorschau herunterladen';
            $lang_user['b_previewtext']  	      = 'Ihr Brief wurde erfolgreich zum Versand vorbereitet. Im Folgenden k&ouml;nnen Sie eine Vorschau (PDF-Datei) des Briefes herunterladen und Sie sehen die Seitenanzahl des Briefes sowie den Preis (in Credits), der bei Versand f&auml;llig wird.';
            $lang_user['b_pages']  	      	      = 'Seiten';
            $lang_user['b_karte']  	 	      	  = 'Brief versenden';
            $lang_user['bk_status1']			  = 'wird gesendet';
            $lang_user['bk_status2']			  = 'versendet';
            $lang_user['bk_status3']			  = 'Fehler - Credits wurden zur&uuml;ckerstattet!';
            $lang_user['bk_status_err']			  = 'Fehler!';
            $lang_user['bk_typ1']  	 		      = 'Brief';
			$lang_user['bk_typ2']  	 		      = 'Postkarte';
            //admin
            $lang_admin['bk_postID_gw']  	 	  = 'Einschreiben Gateway';
            $lang_admin['bk_titel']               = 'Briefe/Postkarten?';
            $lang_admin['bk_preis_brief_es']      = 'Preis f&uuml;r Brief-Einschreiben (Einwurf)';
			$lang_admin['bk_preis_brief_es_onl']  = 'Preis f&uuml;r Brief-Einschreiben (Online-Quittung)';
			$lang_admin['bk_preis_brief_es_rs']   = 'Preis f&uuml;r Brief-Einschreiben (mit R&uuml;ckschein)';
            $lang_admin['bk_titel2']              = 'Post';
            $lang_admin['bk_statistik']	 		  = 'Statistik';
            $lang_admin['bk_brief']  	 		  = 'Brief';
			$lang_admin['bk_karte']  	 		  = 'Postkarten';
			$lang_admin['bk_gw']  	 			  = 'Gateway';
			$lang_admin['bk_preis_de']  	 	  = 'Preis f&uuml;r Deutschland';
			$lang_admin['bk_preis_eu_welt']  	  = 'Preis f&uuml;r Europa/Welt';
			$lang_admin['bk_preis_de_farbe']  	  = 'Preis f&uuml;r Farbe pro Seite';
			$lang_admin['bk_p_de']  			  = 'Preise f&uuml;r Deutschland';
			$lang_admin['bk_p_eu']  			  = 'Preise f&uuml;r Europa';
            $lang_admin['bk_p_welt']  			  = 'Preise f&uuml;r alle anderen L&auml;nder';
			$lang_admin['bk_preis_de_s1b2']  	  = 'Preis Seite 1 bis 2';
			$lang_admin['bk_preis_de_jws']  	  = 'Preis jede weitere Seite';
			$lang_admin['bk_preis_de_ps1b3']  	  = 'Porto 1 - 2 Seiten (1 - 4 Seiten duplex)';
			$lang_admin['bk_preis_de_ps4b15']  	  = 'Porto 3 - 8 Seiten (5 - 16 Seiten duplex)';
			$lang_admin['bk_preis_de_ps16b99']    = 'Porto 9 - 50 Seiten (17 - 99 Seiten duplex)';
            $lang_admin['bk_signatur']  	 	  = 'Brief-Signatur:';
            //admin statistik postkarte
            $lang_admin['bk_stats_day_ok']          = 'Gesendete Postkarten (heute)';
            $lang_admin['bk_stats_day_error']       = '&Uuml;bertragungsfehler (heute)';
            $lang_admin['bk_stats_month_ok']        = 'Gesendete Postkarten (Monat)';
            $lang_admin['bk_stats_month_error']     = '&Uuml;bertragungsfehler (Monat)';
            $lang_admin['bk_stats_all_ok']          = 'Gesendete Postkarten (gesamt)';
            $lang_admin['bk_stats_all_error']       = '&Uuml;bertragungsfehler (gesamt)';
            $lang_admin['bk_credits_day_ok']        = 'Abgerechnete Cred. (heute)';
            $lang_admin['bk_credits_day_error']     = 'R&uuml;ckerstattungen (Cred., heute)';
            $lang_admin['bk_credits_month_ok']      = 'Abgerechnete Cred. (dieser Monat)';
            $lang_admin['bk_credits_month_error']   = 'R&uuml;ckerstattungen (Cred., Monat)';
            $lang_admin['bk_credits_all_ok']        = 'Abgerechnete Cred. (insgesamt)';
            $lang_admin['bk_credits_all_error']     = 'R&uuml;ckerstattungen (Cred., gesamt)';
            //admin statistik brief
            $lang_admin['b_stats_day_ok']          = 'Gesendete Briefe (heute)';
            $lang_admin['b_stats_day_error']       = '&Uuml;bertragungsfehler (heute)';
            $lang_admin['b_stats_month_ok']        = 'Gesendete Briefe (Monat)';
            $lang_admin['b_stats_month_error']     = '&Uuml;bertragungsfehler (Monat)';
            $lang_admin['b_stats_all_ok']          = 'Gesendete Briefe (gesamt)';
            $lang_admin['b_stats_all_error']       = '&Uuml;bertragungsfehler (gesamt)';
            $lang_admin['b_credits_day_ok']        = 'Abgerechnete Cred. (heute)';
            $lang_admin['b_credits_day_error']     = 'R&uuml;ckerstattungen (Cred., heute)';
            $lang_admin['b_credits_month_ok']      = 'Abgerechnete Cred. (dieser Monat)';
            $lang_admin['b_credits_month_error']   = 'R&uuml;ckerstattungen (Cred., Monat)';
            $lang_admin['b_credits_all_ok']        = 'Abgerechnete Cred. (insgesamt)';
            $lang_admin['b_credits_all_error']     = 'R&uuml;ckerstattungen (Cred., gesamt)';
		}
		else 
		{
			$lang_user['bk_main']   			  = 'post';
			$lang_user['bk_karte']  	 		  = 'send postcard';
			$lang_user['bk_brief']  	 		  = 'send letter';
			$lang_user['bk_journal']  	 		  = 'journal';
			$lang_user['bk_to']  	 			  = 'receiver';
			$lang_user['bk_from']  	 			  = 'sender';
			$lang_user['bk_de']  	 			  = 'germany';
			$lang_user['bk_eu']  	 			  = 'europe';
            $lang_user['bk_welt']  	 			  = 'world';
            $lang_user['bk_vn']  	 			  = 'send to';
            $lang_user['bk_price']  	 		  = 'price';
            //postcard
			$lang_user['bk_enter']  	 		  = 'please use the Enter key in your text not more than';
			$lang_user['bk_pricewarning']  	 	  = 'to send the postcard in your balance reaches unfortunately not enough. Please recharge your account before sending the postcard.';
			$lang_user['bk_karte_sendok']  	 	  = 'Your postcard has been sent successfully.';
			$lang_user['bk_karte_senderror']  	  = 'Your card could not be sent. A temporary internal error has occurred. Please try again later.';
			$lang_user['bk_absender_error']		  = 'Your sender consists of more than 50 characters. So your return data is transmitted correctly, please use a maximum of 50 characters.';
			$lang_user['bk_karte_picerror']		  = 'You have not selected an image or your image has an incorrect format! Please use an image in JPG-format.';
			$lang_user['bk_karte_dpierror']		  = 'The resolution of your image is too small. Please select an image that has at least 150 dpi.';
			$lang_user['bk_vorschau']  	 		  = 'postcard Preview &raquo;';
			$lang_user['bk_versandnach']  	 	  = 'Where do you want to send the postcard?';
			$lang_user['bk_dateiupload']  	 	  = 'Please select a picture you want to use as a motive!';
			$lang_user['bk_karte_groesseerror']	  = 'Your image is larger than 2 megabytes. Please select a picture, which more than 2 megabytes in size.';
			$lang_user['bk_dateiupload_info']	  = '<b>Reference:</b> Please note that your image to a maximum of two megabytes may be large, the format is JPG and a minimum resolution of 150 dpi (15x10 cm) has.';
			$lang_user['bk_to_z0']  	 		  = 'Association or company';
			$lang_user['bk_to_z1']  	 		  = 'Title, first and last name';
			$lang_user['bk_to_z2']  	 	      = 'Street and number';
			$lang_user['bk_to_z3']  	 		  = 'Code, town and country';
			$lang_user['bk_from_z0']  	 		  = 'Postal code, City';
			$lang_user['bk_from_z1']  	 		  = 'Full Name';
			$lang_user['paybrieffailed']		  = 'It is an error in the calculation occurred. Please contact technical support.';
			$lang_user['bk_typ']  	 			  = 'type';
            $lang_user['bk_datum']  	 		  = 'date';
            $lang_user['bk_status']  	 		  = 'status';
            $lang_user['bk_prevueb1']  	 		  = 'Preview the front';
            $lang_user['bk_prevueb2']  	 		  = 'Preview the back';
            $lang_user['bk_prevhinw']  	 		  = 'This presentation can depart from the originaln!';
            //letter
            $lang_user['b_absender']  	 	      = 'sender';
			$lang_user['b_clickhere']  	 	      = 'click here';
            $lang_client['b_es_status_offen']  	  = 'Currently, no shipment ID or before the shipment ID is known, but its still not a registered ID assigned! Please check it later again.';
			$lang_user['b_einschreibencheck']  	  = 'registered mail tracking';
            $lang_user['b_maxseitenerror']        = 'You have exceeded the maximum number of possible sites!. In duplex mode, you can send up to 99 pages in normal mode and up to 50 pages.';
            $lang_user['b_einschreiben']  	      = 'enroll (objection)';
			$lang_user['b_einschreiben_online']   = 'enroll (online-receipt)';
			$lang_user['b_einschreiben_rs']  	  = 'enroll (with return receipt)';
            $lang_user['b_einschreiben_info']  	  = 'enroll - proof of service by the delivery!';
            $lang_user['b_empf_info']  	 	      = 'receiver informationen';
            $lang_user['b_duplex']  	 	      = 'duplex-print (both sides print)';
            $lang_user['b_firma']  	 	          = 'company/association';
            $lang_user['b_deckblatt']  	 	      = 'Cover page with sender and receiver?';
            $lang_user['b_db_info']  	 	      = 'Cover generate?';
            $lang_user['b_versandnach']  	 	  = 'Where do you want to send the letter?';
            $lang_user['b_dateiupload']  	 	  = 'If you already have your letter available as a PDF file, please select this from here!';
            $lang_user['b_farbe']  	        	  = 'Please select the printing method?';
            $lang_user['b_farbe1']  	          = 'colored';
            $lang_user['b_farbe2']  	       	  = 'black/white';
            $lang_user['b_textmsg']  	       	  = 'Ability to enter additional text to the letter or to write directly';
            $lang_user['b_weiter']  	       	  = 'Letter preview and price calculation &raquo;';
            $lang_user['b_wrongformat']  	   	  = 'Your uploaded PDF file uses a format version, which is not currently supported. Please use a version up to 1.4!';
            $lang_user['b_tmperror']  	          = 'Your letter could not be created. A temporary internal error has occurred. Please try again later.';
            $lang_user['b_brief_sendok']  	 	  = 'Your letter has been sent successfully.';
            $lang_user['b_brief_senderror']       = 'Your letter could not be sent. A temporary internal error has occurred. Please try again later.';
            $lang_user['b_pricewarning']  	 	  = 'To send the letter in your balance reaches unfortunately not enough. Please recharge your account before sending the letter.';
            $lang_user['b_preview']  	      	  = 'letter preview Download';
            $lang_user['b_previewtext']  	      = 'Your letter was successfully prepared for shipment. Below you can download a preview (PDF file) of the letter and see the page number of the letter and the price (in credits), which is due upon delivery.';
            $lang_user['b_pages']  	      	      = 'pages';
            $lang_user['b_karte']  	 	      	  = 'send letter';
            $lang_user['bk_status1']			  = 'is sent';
            $lang_user['bk_status2']			  = 'sent';
            $lang_user['bk_status3']			  = 'Error - Credits were returned!';
            $lang_user['bk_status_err']			  = 'error!';
            $lang_user['bk_typ1']  	 		      = 'letter';
			$lang_user['bk_typ2']  	 		      = 'postcard';
            //admin
            $lang_admin['bk_postID_gw']  	 	  = 'gateway to enroll';
            $lang_admin['bk_titel']               = 'letter/postcard?';
            $lang_admin['bk_preis_brief_es']      = 'price for registered letter (objection)';
			$lang_admin['bk_preis_brief_es_onl']  = 'price for registered letter (online receipt)';
			$lang_admin['bk_preis_brief_es_rs']   = 'price for registered letter (with return receipt)';
            $lang_admin['bk_titel2']              = 'post';
            $lang_admin['bk_statistik']	 		  = 'statistic';
            $lang_admin['bk_brief']  	 		  = 'letter';
			$lang_admin['bk_karte']  	 		  = 'postcard';
			$lang_admin['bk_gw']  	 			  = 'gateway';
			$lang_admin['bk_preis_de']  	 	  = 'Price for Germany';
			$lang_admin['bk_preis_eu']  	 	  = 'Price for Europe';
			$lang_admin['bk_preis_de_farbe']  	  = 'Price per page for color';
			$lang_admin['bk_p_de']  			  = 'Prices for Germany';
			$lang_admin['bk_p_eu']  			  = 'Prices for Europe';
            $lang_admin['bk_p_welt']  			  = 'Pricese for all other countries';
			$lang_admin['bk_preis_de_s1b2']  	  = 'Price page 1-2';
			$lang_admin['bk_preis_de_jws']  	  = 'Price each additional page';
			$lang_admin['bk_preis_de_ps1b3']  	  = 'Porto 1 - 2 pages (1 - 4 pages duplex)';
			$lang_admin['bk_preis_de_ps4b15']  	  = 'Porto 3 - 8 pages (5 - 16 pages duplex)';
			$lang_admin['bk_preis_de_ps16b99']    = 'Porto 9 - 50 pages (17 - 99 pages duplex)';
            $lang_admin['bk_signatur']  	 	  = 'letter-signature:';
            //admin statistic postcard
            $lang_admin['bk_stats_day_ok']          = 'Sent postcards (today)';
            $lang_admin['bk_stats_day_error']       = 'Transmission errors (today)';
            $lang_admin['bk_stats_month_ok']        = 'Sent postcards (month)';
            $lang_admin['bk_stats_month_error']     = 'Transmission Error (month)';
            $lang_admin['bk_stats_all_ok']          = 'Sent postcards (total)';
            $lang_admin['bk_stats_all_error']       = 'Transmission errors (total)';
            $lang_admin['bk_credits_day_ok']        = 'Budgets of Cred. (Today)';
            $lang_admin['bk_credits_day_error']     = 'Refunds (Cred. today)';
            $lang_admin['bk_credits_month_ok']      = 'Budgets of Cred. (This month)';
            $lang_admin['bk_credits_month_error']   = 'R&uuml;ckerstattungen (Cred., Monat)';
            $lang_admin['bk_credits_all_ok']        = 'Refunds (Cred., month)';
            $lang_admin['bk_credits_all_error']     = 'Refunds (Cred., total)';
            //admin statistic letter
            $lang_admin['b_stats_day_ok']          = 'Sent letters (today)';
            $lang_admin['b_stats_day_error']       = 'Transmission errors (today)';
            $lang_admin['b_stats_month_ok']        = 'Sent letters (month)';
            $lang_admin['b_stats_month_error']     = 'Transmission Error (month)';
            $lang_admin['b_stats_all_ok']          = 'Sent letters (total)';
            $lang_admin['b_stats_all_error']       = 'Transmission errors (total)';
            $lang_admin['b_credits_day_ok']        = 'Budgets of Cred. (Today)';
            $lang_admin['b_credits_day_error']     = 'Refunds (Cred. today)';
            $lang_admin['b_credits_month_ok']      = 'Budgets of Cred. (This month)';
            $lang_admin['b_credits_month_error']   = 'Refunds (Cred., month)';
            $lang_admin['b_credits_all_ok']        = 'Budgets of Cred. (Total)';
            $lang_admin['b_credits_all_error']     = 'Refunds (Cred., total)';
		}
	}

	/*
	 * installation routine
	 */	
	function Install()
	{
		global $db;

		$db->Query('CREATE TABLE IF NOT EXISTS `{pre}briefkarte_prefs` (
			`key` varchar(64) NOT NULL,
			`value` text,
			PRIMARY KEY  (`key`)) ENGINE=MyISAM;');
			
		$db->Query('CREATE TABLE IF NOT EXISTS `{pre}briefkarte_files` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`erstellt` INT(11) NOT NULL,
            `dateiname` VARCHAR(255) NOT NULL,
			`bktyp` VARCHAR(255) NOT NULL,
			`seiten` INT(11) NOT NULL DEFAULT 0,
			`land` VARCHAR(255) NOT NULL,
			`bkkey` VARCHAR(255) NOT NULL,
			`userid` INT(11) NOT NULL,
			`status` INT(1) NOT NULL,
            `deleted` INT(1) NOT NULL,
            `kosten` INT(11) NOT NULL,
            `auftrag` BIGINT(15) NOT NULL,
			PRIMARY KEY (`id`)) ENGINE=MyISAM AUTO_INCREMENT=1;');

		// beispieldaten eintragen bei erst-installation
		if(!$this->GetPref("status"))
		{
			$this->SetPref("post_gw", 'http://www.smskaufen.com/sms/post/postin.php');
			$this->SetPref("post_user", "");
			$this->SetPref("post_pw", "");

			$this->SetPref("brief_gw", 'http://www.smskaufen.com/sms/post/postin.php');
			$this->SetPref("brief_user", "");
			$this->SetPref("brief_pw", "");

			$this->SetPref("post_preis_de", (int)0);
			$this->SetPref("post_preis_eu", (int)0);

			$this->SetPref("brief_preis_s1u2", (int)0);
			$this->SetPref("brief_preis_ab3s", (int)0);
            
			$this->SetPref("brief_preis_porto_1b3s_de", (int)0);
			$this->SetPref("brief_preis_porto_4b15s_de", (int)0);
			$this->SetPref("brief_preis_porto16b99s_de", (int)0);
			$this->SetPref("brief_preis_farbe_de", (int)0);

			$this->SetPref("brief_preis_porto_1b3s_eu", (int)0);
			$this->SetPref("brief_preis_farbe_eu", (int)0);
            
            //statistik brief
            $this->SetPref("b_day_ok", (int)0);
            $this->SetPref("b_month_ok", (int)0);
            $this->SetPref("b_all_ok", (int)0);
            $this->SetPref("b_day_error", (int)0);
            $this->SetPref("b_month_error", (int)0);
            $this->SetPref("b_all_error", (int)0);
            
            //statistik postkarte
            $this->SetPref("bk_day_ok", (int)0);
            $this->SetPref("bk_month_ok", (int)0);
            $this->SetPref("bk_all_ok", (int)0);
            $this->SetPref("bk_day_error", (int)0);
            $this->SetPref("bk_month_error", (int)0);
            $this->SetPref("bk_all_error", (int)0);
            
            //statistik credits
            $this->SetPref("bk_credits_day_ok", (int)0);
            $this->SetPref("bk_credits_month_ok", (int)0);
            $this->SetPref("bk_credits_all_ok", (int)0);
            $this->SetPref("bk_credits_day_error", (int)0);
            $this->SetPref("bk_credits_month_error", (int)0);
            $this->SetPref("bk_credits_all_error", (int)0);
            
            $this->SetPref("b_credits_day_ok", (int)0);
            $this->SetPref("b_credits_month_ok", (int)0);
            $this->SetPref("b_credits_all_ok", (int)0);
            $this->SetPref("b_credits_day_error", (int)0);
            $this->SetPref("b_credits_month_error", (int)0);
            $this->SetPref("b_credits_all_error", (int)0);
            
            $this->SetPref("reset_day", (int)0);
            $this->SetPref("reset_month", (int)0);

			$this->SetPref("status", true);
		}
        
        $res = $db->Query("SHOW COLUMNS FROM {pre}briefkarte_files LIKE 'duplexdruck'");
        if($res->RowCount() == 0)       
        {
            $this->SetPref("brief_preis_porto_4b15s_eu", (int)0);
            $this->SetPref("brief_preis_porto_16b99s_eu", (int)0);
            $this->SetPref("brief_preis_porto_1b3s_welt", (int)0);
            $this->SetPref("brief_preis_porto_4b15s_welt", (int)0);
            $this->SetPref("brief_preis_porto_16b99s_welt", (int)0);
            
            $this->SetPref("bk_preis_einschreiben", (int)0);
            
            $db->Query('ALTER TABLE {pre}briefkarte_files ADD duplexdruck TINYINT(3) NOT NULL DEFAULT 0');
            $db->Query('ALTER TABLE {pre}briefkarte_files ADD einschreiben TINYINT(3) NOT NULL DEFAULT 0');
        }
        $res->Free();
        
        $res = $db->Query("SHOW COLUMNS FROM {pre}briefkarte_files LIKE 'postID_gw'");
        if($res->RowCount() == 0)       
        {
            $this->SetPref("postID_gw", 'http://www.smskaufen.com/sms/post/postcheck.php');
            
            $this->SetPref("bk_preis_einschreiben_online", (int)0);
			$this->SetPref("bk_preis_einschreiben_rs", (int)0);
        }
        $res->Free();

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		global $db;

		$db->Query('DROP TABLE {pre}briefkarte_prefs');
        $db->Query('DROP TABLE {pre}briefkarte_files');

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * set preference
	 */
	function SetPref($key, $value)
	{
		global $db;
		$db->Query('REPLACE INTO {pre}briefkarte_prefs(`key`,`value`) VALUES(?, ?)',
			$key,
			$value);
		return($db->AffectedRows() == 1);
	}

	/*
	 * get preference
	 */
	function GetPref($key)
	{
		global $db;
		$res = $db->Query('SELECT `value` FROM {pre}briefkarte_prefs WHERE `key`=?',
			$key);
		if($res->RowCount() == 1)
		{
			$row = $res->FetchArray(MYSQL_NUM);
			$res->Free();
			return($row[0]);
		} else {
			$res->Free();
			return(false);
		}
	}

	// Admin Seite Postkarte
	function _gatewaysAdminPost()
	{
		global $tpl;

		// updaten der gateway-informationen
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$this->SetPref("post_preis_de", (int)$_REQUEST['post_preis_de']);
			$this->SetPref("post_preis_eu", (int)$_REQUEST['post_preis_eu']);

			$this->SetPref("post_gw", $_REQUEST['post_gw']);
			$this->SetPref("post_user", $_REQUEST['post_user']);
			$this->SetPref("post_pw", $_REQUEST['post_pw']);
		}
		// �bergabe der werte
		$tpl->assign('post_preis_de', $this->GetPref("post_preis_de"));
		$tpl->assign('post_preis_eu', $this->GetPref("post_preis_eu"));

		$tpl->assign('post_gw', $this->GetPref("post_gw"));
		$tpl->assign('post_user', $this->GetPref("post_user"));
		$tpl->assign('post_pw', $this->GetPref("post_pw"));
		$tpl->assign('page', $this->_templatePath('briefkarte.admin.postkarte.gateways.tpl'));
	}
    
    // Admin Seite Statistik
	function _gatewaysAdminBKstats()
	{
		global $tpl;

		// �bergabe der werte
		$tpl->assign('b_day_ok', $this->GetPref("b_day_ok"));
		$tpl->assign('b_month_ok', $this->GetPref("b_month_ok"));
		$tpl->assign('b_all_ok', $this->GetPref("b_all_ok"));
		$tpl->assign('bk_day_ok', $this->GetPref("bk_day_ok"));
		$tpl->assign('bk_month_ok', $this->GetPref("bk_month_ok"));
		$tpl->assign('bk_all_ok', $this->GetPref("bk_all_ok"));
        
        $tpl->assign('b_day_error', $this->GetPref("b_day_error"));
		$tpl->assign('b_month_error', $this->GetPref("b_month_error"));
		$tpl->assign('b_all_error', $this->GetPref("b_all_error"));
		$tpl->assign('bk_day_error', $this->GetPref("bk_day_error"));
		$tpl->assign('bk_month_error', $this->GetPref("bk_month_error"));
		$tpl->assign('bk_all_error', $this->GetPref("bk_all_error"));
        
        $tpl->assign('b_credits_day_ok', $this->GetPref("b_credits_day_ok"));
		$tpl->assign('b_credits_month_ok', $this->GetPref("b_credits_month_ok"));
		$tpl->assign('b_credits_all_ok', $this->GetPref("b_credits_all_ok"));
        
        $tpl->assign('bk_credits_day_ok', $this->GetPref("bk_credits_day_ok"));
		$tpl->assign('bk_credits_month_ok', $this->GetPref("bk_credits_month_ok"));
		$tpl->assign('bk_credits_all_ok', $this->GetPref("bk_credits_all_ok"));
        
		$tpl->assign('b_credits_day_error', $this->GetPref("b_credits_day_error"));
		$tpl->assign('b_credits_month_error', $this->GetPref("b_credits_month_error"));
		$tpl->assign('b_credits_all_error', $this->GetPref("b_credits_all_error"));
        
        $tpl->assign('bk_credits_day_error', $this->GetPref("bk_credits_day_error"));
		$tpl->assign('bk_credits_month_error', $this->GetPref("bk_credits_month_error"));
		$tpl->assign('bk_credits_all_error', $this->GetPref("bk_credits_all_error"));

		$tpl->assign('page', $this->_templatePath('briefkarte.admin.stats.tpl'));
	}

	// Admin Seite Brief
	function _gatewaysAdminBrief()
	{
		global $tpl;

		// updaten der gateway-informationen
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$this->SetPref("brief_preis_s1u2", (int)$_REQUEST['bk_preis_s1u2']);
			$this->SetPref("brief_preis_ab3s", (int)$_REQUEST['bk_preis_ab3s']);
            
            // deutschland
			$this->SetPref("brief_preis_porto_1b3s_de", (int)$_REQUEST['bk_preis_de_porto_1b3s']);
			$this->SetPref("brief_preis_porto_4b15s_de", (int)$_REQUEST['bk_preis_de_porto_4b15s']);
			$this->SetPref("brief_preis_porto16b99s_de", (int)$_REQUEST['bk_preis_de_porto16b99s']);
			$this->SetPref("brief_preis_farbe_de", (int)$_REQUEST['bk_preis_de_farbe']);
            
            // europa
            $this->SetPref("brief_preis_porto_1b3s_eu", (int)$_REQUEST['bk_preis_eu_porto_1b3s']);
            $this->SetPref("brief_preis_porto_4b15s_eu", (int)$_REQUEST['brief_preis_porto_4b15s_eu']);
            $this->SetPref("brief_preis_porto_16b99s_eu", (int)$_REQUEST['brief_preis_porto_16b99s_eu']);
			$this->SetPref("brief_preis_farbe_eu", (int)$_REQUEST['bk_preis_eu_farbe']);
            
            // weltweit
            $this->SetPref("brief_preis_porto_1b3s_welt", (int)$_REQUEST['brief_preis_porto_1b3s_welt']);
            $this->SetPref("brief_preis_porto_4b15s_welt", (int)$_REQUEST['brief_preis_porto_4b15s_welt']);
            $this->SetPref("brief_preis_porto_16b99s_welt", (int)$_REQUEST['brief_preis_porto_16b99s_welt']);
            $this->SetPref("brief_preis_farbe_welt", (int)$_REQUEST['bk_preis_welt_farbe']);

            $this->SetPref("bk_preis_einschreiben", (int)$_REQUEST['bk_preis_einschreiben']);
			$this->SetPref("bk_preis_einschreiben_online", (int)$_REQUEST['bk_preis_einschreiben_online']);
			$this->SetPref("bk_preis_einschreiben_rs", (int)$_REQUEST['bk_preis_einschreiben_rs']);

            $this->SetPref("postID_gw", $_REQUEST['postID_gw']);

			$this->SetPref("brief_gw", $_REQUEST['brief_gw']);
			$this->SetPref("brief_user", $_REQUEST['brief_user']);
			$this->SetPref("brief_pw", $_REQUEST['brief_pw']);
		}
		// �bergabe der werte
		$tpl->assign('brief_preis_s1u2', $this->GetPref("brief_preis_s1u2"));
		$tpl->assign('brief_preis_ab3s', $this->GetPref("brief_preis_ab3s"));
        
		$tpl->assign('brief_preis_porto_1b3s_de', $this->GetPref("brief_preis_porto_1b3s_de"));
		$tpl->assign('brief_preis_porto_4b15s_de', $this->GetPref("brief_preis_porto_4b15s_de"));
		$tpl->assign('brief_preis_porto16b99s_de', $this->GetPref("brief_preis_porto16b99s_de"));
		$tpl->assign('brief_preis_farbe_de', $this->GetPref("brief_preis_farbe_de"));

		$tpl->assign('brief_preis_porto_1b3s_eu', $this->GetPref("brief_preis_porto_1b3s_eu"));
        $tpl->assign('brief_preis_porto_4b15s_eu', $this->GetPref("brief_preis_porto_4b15s_eu"));
        $tpl->assign('brief_preis_porto_16b99s_eu', $this->GetPref("brief_preis_porto_16b99s_eu"));
		$tpl->assign('brief_preis_farbe_eu', $this->GetPref("brief_preis_farbe_eu"));
        
        $tpl->assign('brief_preis_porto_1b3s_welt', $this->GetPref("brief_preis_porto_1b3s_welt"));
        $tpl->assign('brief_preis_porto_4b15s_welt', $this->GetPref("brief_preis_porto_4b15s_welt"));
        $tpl->assign('brief_preis_porto_16b99s_welt', $this->GetPref("brief_preis_porto_16b99s_welt"));
		$tpl->assign('brief_preis_farbe_welt', $this->GetPref("brief_preis_farbe_welt"));
        
        $tpl->assign('bk_preis_einschreiben', $this->GetPref("bk_preis_einschreiben"));
		$tpl->assign('bk_preis_einschreiben_online', $this->GetPref("bk_preis_einschreiben_online"));
		$tpl->assign('bk_preis_einschreiben_rs', $this->GetPref("bk_preis_einschreiben_rs"));

        $tpl->assign('postID_gw', $this->GetPref("postID_gw"));

		$tpl->assign('brief_gw', $this->GetPref("brief_gw"));
		$tpl->assign('brief_user', $this->GetPref("brief_user"));
		$tpl->assign('brief_pw', $this->GetPref("brief_pw"));
		$tpl->assign('page', $this->_templatePath('briefkarte.admin.brief.gateways.tpl'));
	}

	/*
	 * getUserPages
	 */
	function getUserPages($loggedin)
	{
		global $lang_user;
        
		if(!$loggedin || !$this->GetGroupOptionValue('briefkarte'))
			return(array());

		return(array(
			'briefkarte' => array(
					'icon'		=> 'modbriefkarte',
					'link'		=> 'start.php?action=briefkarte&bktyp=brief&sid=',
					'text'		=> $lang_user['bk_main'],
					'iconDir'	=> './plugins/templates/images/',
					'order'		=> 205
				)
			));
	}

	function OnCron()
	{
		global $db;
        
        $stunde = date('G');
        $tag = date('j');

        // l�scht alte eintr�ge (�lter als 24h) aus der datenbank und die dazugeh�rigen dateien aus dem data-verzeichnis
		if($stunde == 4)
		{
			$res = $db->Query("SELECT `id` FROM {pre}briefkarte_files WHERE erstellt < ? AND status = ?", 
				time()-86400, 0);
                
			while($row = $res->FetchArray(MYSQL_ASSOC))
			{
                // postkarten-files aus data-verzeichnis l�schen
                $filename = DataFilename($row['id'], 'bkp');
    			// pr�fen, ob datei existiert
    			if(file_exists($filename))
    			{
    				unlink($filename);
    			}
                // brief-files aus data-verzeichnis l�schen
                $filename2 = DataFilename($row['id'], 'bkb');
    			// pr�fen, ob datei existiert
    			if(file_exists($filename2))
    			{
    				unlink($filename2);
    			}
			}
			$res->Free();
            
			$db->Query("DELETE FROM {pre}briefkarte_files WHERE erstellt < ? AND status = ?", 
				time()-86400, 0);
		}
        
        // statistik-reset
        if($stunde >= 0 && $stunde <= 1)
        {
            // tagesstatistik resetten
            if(!$this->GetPref("reset_day"))
            {
                $this->SetPref("bk_day_ok", (int)0);
                $this->SetPref("bk_day_error", (int)0);
                $this->SetPref("b_day_ok", (int)0);
                $this->SetPref("b_day_error", (int)0);
                $this->SetPref("bk_credits_day_ok", (int)0);
                $this->SetPref("bk_credits_day_error", (int)0);
                $this->SetPref("b_credits_day_ok", (int)0);
                $this->SetPref("b_credits_day_error", (int)0);
                
                $this->SetPref("reset_day", (int)1);
            }
            
            // monatsstatistik resetten
            if(!$this->GetPref("reset_month") && $tag == 1)
            {
                $this->SetPref("bk_month_ok", (int)0);
                $this->SetPref("bk_month_error", (int)0);
                $this->SetPref("b_month_ok", (int)0);
                $this->SetPref("b_month_error", (int)0);
                $this->SetPref("bk_credits_month_ok", (int)0);
                $this->SetPref("bk_credits_month_error", (int)0);
                $this->SetPref("b_credits_month_ok", (int)0);
                $this->SetPref("b_credits_month_error", (int)0);

                $this->SetPref("reset_month", (int)1);
            }
        }
        
        if($stunde >= 2 && $stunde <= 3)
        {
            if($this->GetPref("reset_day"))
            {                
                $this->SetPref("reset_day", (int)0);
            }
            
            if($this->GetPref("reset_month") && $tag == 1)
            {
                $this->SetPref("reset_month", (int)0);
            }
        }
	}

	// l�scht alle daten des users aus der datenbank und aus dem data-verzeichnis, welcher aus dem system gel�scht wurde
	function OnDeleteUser($id)
	{
		global $db;
		
		// l�scht tmp-files
		$res = $db->Query('SELECT `id` FROM {pre}briefkarte_files WHERE userid = ?', 
			$id);
		while($row = $res->FetchArray(MYSQL_ASSOC))
		{
            // postkarten-file aus data-verzeichnis l�schen
            $filename = DataFilename($row['id'], 'bkp');
			// pr�fen, ob datei existiert
			if(file_exists($filename))
			{
				unlink($filename);
			}
            // brief-file aus data-verzeichnis l�schen
            $filename2 = DataFilename($row['id'], 'bkb');
			// pr�fen, ob datei existiert
			if(file_exists($filename2))
			{
				unlink($filename2);
			}
		}
		$res->Free();
        
		$db->Query("DELETE FROM {pre}briefkarte_files WHERE userid = ?", 
			$id);
	}

	function FileHandler($file, $action)
	{
		global $db, $tpl, $userRow, $thisUser, $lang_user, $bm_prefs, $groupRow;

		// status bei druck der postkarte/des briefes von smskaufen abfangen und statistik updaten
		if($file == 'index.php')
		{
			if(isset($_REQUEST['auftrag']) && isset($_REQUEST['statustext']) && isset($_REQUEST['code']))
			{
				$auftrag = $_REQUEST['auftrag'];
				$statustext = $_REQUEST['statustext'];
				$code = $_REQUEST['code'];
				
                $res = $db->Query('SELECT bktyp, land, userid, kosten, status FROM {pre}briefkarte_files WHERE `id` = ? AND auftrag=?',
                    (int) $code,
                    $auftrag);
                if($res->RowCount() != 1) 
				    die('No Hacking please!');
				$row = $res->FetchArray();
				$res->free();
                
                // erfolgreich ausgedruckt und per post versendet
                // strtolower() wandelt die buchstaben komplett in kleinbuchstaben um
				if(strtolower($statustext) == 'ok')
                {
                    if($row['bktyp'] == "brief")
                        PutLog('letter send-status for post <' . $code . '> (order-number: ' . $auftrag . '; status: ' . $statustext . ')', PRIO_NOTE, __FILE__, __LINE__);
                    else
                        PutLog('postcard send-status for post <' . $code . '> (order-number: ' . $auftrag . '; status: ' . $statustext . ')', PRIO_NOTE, __FILE__, __LINE__);

					$db->Query('UPDATE {pre}briefkarte_files SET status=? WHERE id=? AND auftrag=?',
						(int) 2,
						(int) $code,
                        $auftrag);
				}
                else
                {
                    // schutz vor doppelter gutschrift
                    if($row['status'] == 3 || $row['status'] == 2)
                        die('No Hacking please!');
                    
                    if($row['bktyp'] == "brief")
                    {
                        //statistik updaten (�bertragungsfehler)
                        $this->bkStats(0, 0, "brief");
                        PutLog('send-status for letter <' . $code . '> (user: ' . $row['userid'] . '; order-number: ' . $auftrag . '; status: ' . $statustext . ')', PRIO_WARNING, __FILE__, __LINE__);
                    }
                    else
                    {
                        //statistik updaten (�bertragungsfehler)
                        $this->bkStats(0, 0, "postkarte");
                        PutLog('send-status for postcard <' . $code . '> (user: ' . $row['userid'] . '; order-number: ' . $auftrag . '; status: ' . $statustext . ')', PRIO_WARNING, __FILE__, __LINE__);
                    }
					
					$db->Query('UPDATE {pre}briefkarte_files SET status=? WHERE id=? AND auftrag=?',
						(int) 3,
						(int) $code,
                        $auftrag);
					
                    //r�ckerstattung bei fehler
					$userObject = _new('BMUser', array($row['userid']));
                    if($row['bktyp'] == "postkarte")
                    {
                        $userObject->Debit($this->GetPref("post_preis_".$row['land']));
                        //statistik updaten
                        $this->bkStats(0, $this->GetPref("post_preis_".$row['land']), "postkarte");
                    }
                    else
                    {
                        $userObject->Debit($row['kosten']);
                        //statistik updaten
                        $this->bkStats(0, $row['kosten'], "brief");
                    }
				}
			}
		}

		// daten holen und �bergeben
		if($file == 'start.php' && $action == 'getBKFile')
		{
			$res = $db->Query('SELECT * FROM {pre}briefkarte_files WHERE bkkey = ? AND `id` = ? AND userid = ?',
				$_REQUEST['bkkey'],
				$_REQUEST['id'],
				$thisUser->_id);

			if($res->RowCount() == 1)
			{
				$row = $res->FetchArray();
				$res->free();
                
                // dateinamen generieren
                if($row['bktyp'] == "postkarte")
    				$filename = DataFilename($row['id'], 'bkp');
                elseif($row['bktyp'] == "brief")
                    $filename = DataFilename($row['id'], 'bkb');
			
				// pr�fen, ob vorschau-datei existiert
				if(file_exists($filename))
				{				
					if($row['bktyp'] == "postkarte")
					{
						header('Content-Type: image/jpeg');
                        //pr�fen, ob download als journal angefordert wird
                        if($_REQUEST['journal'])
                            header('Content-Disposition: attachment; filename="postcard-'.$row['erstellt'].'.jpg"');
						header('Content-Length: ' . filesize($filename));
					}
                    elseif($row['bktyp'] == "brief")
					{
						header('Content-Type: application/pdf');
                        //pr�fen, ob download als vorschau oder journal angefordert wird
                        if($_REQUEST['journal'])
                            header('Content-Disposition: attachment; filename="letter-'.$row['erstellt'].'.pdf"');
                        else
                            header('Content-Disposition: attachment; filename="letter-preview.pdf"');
						header('Content-Length: ' . filesize($filename));
					}
					// datei-ausgabe
					readfile($filename);
					exit();
				}
			}
			// id gibt es nicht, oder nicht vom user
			DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
			exit();
		}
        
		// main
		if($file == 'start.php' && $action == 'briefkarte')
		{
			if(!$this->GetGroupOptionValue('briefkarte', $groupRow['id']))
				return;

			$erstellt = time();

			// brief
			if(isset($_REQUEST['bktyp']) && $_REQUEST['bktyp'] == 'brief')
			{
                // brief senden
                if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'BriefSenden')
                {				
                    //kosten aus datenbank holen
                    $res = $db->Query('SELECT kosten FROM {pre}briefkarte_files WHERE bkkey = ? AND `id` = ?',
					$_REQUEST['bkey'],
					$_REQUEST['bid']);
                    $row = $res->FetchArray();
					$bkosten = $row['kosten'];
					$res->Free();
 
                    // sicherheitspr�fung, ob user genug credits hat
                    if(!$this->GetBriefCredits($bkosten))
                        die('No Hacking please!');
                    					
                    // send brief
                    $bsend = $this->sendBrief($_REQUEST['b_farbe'], $_REQUEST['b_land'], $_REQUEST['bkey'], $_REQUEST['bid'], $_REQUEST['b_einschreiben'], $_REQUEST['b_duplex']);
  
                    // erfolgreicher versand -> berechnung des briefes
					if(strlen($bsend) == 13)
					{
                        //brief berechnen
						$userObject = _new('BMUser', array($thisUser->_id));
                        $userObject->Debit($bkosten*(-1));
                        
                        //statistik updaten
                        $this->bkStats(1, $bkosten, "brief");

                        // status-update auf erfolgreich �bertragen setzen
                        $db->Query('UPDATE {pre}briefkarte_files SET status=?, auftrag=? WHERE bkkey=? AND `id`=?', 1, $bsend, $_REQUEST['bkey'], $_REQUEST['bid']);

						PutLog('Sent letter by user <#' . $thisUser->_id . '> (order-number: ' . $bsend . '; charged: Yes)', PRIO_NOTE, __FILE__, __LINE__);

						$tpl->assign('title', 		$lang_user['bk_brief']);
						$tpl->assign('msg', 		$lang_user['b_brief_sendok']);
						$tpl->assign('backLink', 	'start.php?action=briefkarte&bktyp=brief&sid=' . session_id());
						$tpl->assign('pageContent', 'li/msg.tpl');
                        $tpl->display('li/index.tpl');
                        exit();
					}
                    else
                    {
						PutLog('Error sending letter! (user: #' . $thisUser->_id . ', error-code: ' . $bsend . ')', PRIO_WARNING, __FILE__, __LINE__);
						$tpl->assign('msg', $lang_user['b_brief_senderror']);
                        $tpl->assign('backLink', 	'start.php?action=briefkarte&bktyp=brief&sid=' . session_id());
				        $tpl->assign('pageContent', 'li/error.tpl');
                        $tpl->display('li/index.tpl');
                        exit();
					}
				}
				// vorschau l�schen (daten aus db und aus tmp-verzeichnis l�schen)
				if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'BriefRemove')
				{
				    $res = $db->Query('SELECT id FROM {pre}briefkarte_files WHERE bkkey = ? AND id = ?',
						$_REQUEST['bkey'],
						$_REQUEST['bid']);
					$row = $res->FetchArray();
					$res->Free();

					// file aus data-verzeichnis l�schen
                    $filename = DataFilename($row['id'], 'bkb');
        			// pr�fen, ob datei existiert
        			if(file_exists($filename))
        			{
        				unlink($filename);
        			}
                    
                    //file aus datenbank l�schen
					$db->Query("DELETE FROM {pre}briefkarte_files WHERE bkkey = ? AND id = ?", $_REQUEST['bkey'], $_REQUEST['bid']);
				}
             
                //variable f�r datei-exist-pr�fung setzen
                $fileExists = 0;
             
                // vorschau
                if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'BriefPreview')
                {
                    $farbe = $_REQUEST['b_farbe'];
                    $land = $_REQUEST['b_land'];
                    
					// einschreiben
                    if($_REQUEST['b_einschreiben'])
                        $einschreiben = 1;
					elseif($_REQUEST['b_einschreiben_online'])
						$einschreiben = 2;
					elseif($_REQUEST['b_einschreiben_rs'])
						$einschreiben = 3;
                    else
                        $einschreiben = 0;
                        
                    // sicherheitspr�fung - au�er deutschland alle anderen l�ndern das einschreiben auf 0 setzen
                    if($land != "de")
                        $einschreiben = 0;
                        
                    if($_REQUEST['b_duplex'])
                        $duplex = 1;
                    else
                        $duplex = 0;
                                        
                    $tempFileID = RequestTempFile($userRow['id'], time()+4*TIME_ONE_HOUR);
                    $tempFileName = TempFileName($tempFileID);
                                           
                    if(fopen($tempFileName, 'w+'))
                    {
                        $text = trim($_REQUEST['b_text']);
                        $text = $this->bkConvert($text);
						
                        // pr�fen, ob datei existiert und wenn ja, inhalt der pdf-datei in tmp-datei schreiben
                        if(file_exists($_FILES['b_datei']['tmp_name']) && fopen($_FILES['b_datei']['tmp_name'], 'r+'))
                        {
                            $file = fopen($_FILES['b_datei']['tmp_name'], 'r+');
                            while(!feof($file))
                            {
                                $inhalt .= fgets($file, 4096);
                            }
                            fclose($file);
                            
                            $fp = fopen($tempFileName, 'w+');
                            fwrite($fp, $inhalt);
                            fclose($fp);
                            
                            $fileExists = 1;
                        }
                    }
                    
                    // daten vom deckblatt abfangen
                    $db_abs = utf8_decode($_REQUEST['b_absender']);
                    $db_firma = utf8_decode($_REQUEST['b_empf_firma']);
                    $db_name = utf8_decode($_REQUEST['b_empf_vorname'])." ".utf8_decode($_REQUEST['b_empf_nachname']);
                    $db_strhnr = utf8_decode($_REQUEST['b_empf_strhnr']);
                    $db_plzort = utf8_decode($_REQUEST['b_empf_plz'])." ".utf8_decode($_REQUEST['b_empf_ort']);
                    $db_land = utf8_decode($_REQUEST['b_empf_land']);
                    
                    // pdf-generieren
                    $result = $this->createPDFtmp($text, $tempFileName, $tempFileID, $fileExists, $land, $farbe, $_REQUEST['b_deckblatt'], $db_abs, $db_firma, $db_name, $db_strhnr, $db_plzort, $db_land, $einschreiben, $duplex);
                    
                    //pr�fen, ob $result nicht leer ist
                    if(count($result) <= 1)
                    {
                        $tpl->assign('msg', $lang_user['b_tmperror']);
                        $tpl->assign('pageContent', 'li/error.tpl');
                        $tpl->display('li/index.tpl');
                        exit();
                    }

                    // seitenanzahl pr�fen - zuviele seiten (�ber 99 seiten mit beidseitigem druck oder �ber 50 seiten mit einseitigem druck)
                    if(($result[3] > 99 && $duplex) || ($result[3] > 50 && !$duplex))
                    {
                        $tpl->assign('msg', $lang_user['b_maxseitenerror']);
                        $tpl->assign('pageContent', 'li/error.tpl');
                        $tpl->display('li/index.tpl');
                        exit();
                    }
                    //vorschau anzeigen
                    else
                    {
                        //daten aus $result holen
                        $seitenAnz = $result[3];
                        $briefkosten = $result[2];
                        $bid = $result[1];
                        $bkey = $result[0];
                        //print_r($result);
                        
                        // bildlink f�r vorschau erstellen
                        $pdflink = $bm_prefs['selfurl'] . "start.php?action=getBKFile&bkkey=" . $bkey . "&id=" . $bid .  "&sid=" . session_id();
                                                
						$tpl->assign('b_land', $_REQUEST['b_land']);
                        $tpl->assign('b_farbe', $_REQUEST['b_farbe']);
                        $tpl->assign('b_einschreiben', $einschreiben);
                        $tpl->assign('b_duplex', $duplex);
                        $tpl->assign('b_seiten', $seitenAnz);
                        $tpl->assign('bid', $bid);
                        $tpl->assign('bkey', $bkey);
                        $tpl->assign('pdflink', $pdflink);
                        $tpl->assign('accBalance', $thisUser->GetBalance());
                        $tpl->assign('briefkosten', $briefkosten);
                        $tpl->assign('accCredits', $this->GetBriefCredits($briefkosten));
                        $tpl->assign('pageContent', $this->_templatePath('briefkarte.user.brief.preview.tpl'));
                    }
                }
                else
                {
                    $b_absender = $userRow['vorname']." ".$userRow['nachname'].", ".$userRow['strasse']." ".$userRow['hnr'].", ".$userRow['plz']." ".$userRow['ort'];
                    $tpl->assign('b_absender', $b_absender);
                    $tpl->assign('pageContent', $this->_templatePath('briefkarte.user.brief.tpl')); 
                }
            }
			// einschreiben-check
			elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == 'checkeinschreiben')
			{
				$auftrags_id = $_REQUEST['aid'];
				$user_id = $_REQUEST['uid'];
				
				if($user_id != $thisUser->_id)
					exit('No Hacking please!');
				
				$postId = $this->getPostId($auftrags_id);

                // Auftrag: rausschneiden
                $suchmuster = "Auftrag:";
                $postId = str_ireplace($suchmuster, "", $postId);
                		
                $tpl->assign('postID', $postId);
				$tpl->assign('pageContent', $this->_templatePath('briefkarte.user.einschreiben.outbox.tpl'));
			}
			// gesendete objekte
			elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == 'outbox')
			{
				// bk-daten array erzeugen
    			$bkoutbox = array();
    			
    			// bk-daten auslesen + sortieren + max. 15 ausgeben sowie als gel�scht markierte �berspringen
    			$res = $db->Query('SELECT * FROM {pre}briefkarte_files WHERE userid = ? AND deleted = ? AND status != ? ORDER BY erstellt DESC LIMIT 15', $thisUser->_id, 0, 0);
    			while($row = $res->FetchArray(MYSQL_ASSOC))
    			{
    				$bkoutbox[$row['id']] = $row;
    			}
    			$res->Free();
    			
    			$tpl->assign('bkoutbox', $bkoutbox);
				$tpl->assign('pageContent', $this->_templatePath('briefkarte.user.outbox.tpl'));
			}
			// bk durch user als gel�scht markieren
			elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == 'deletebk')
			{
				$db->Query('UPDATE {pre}briefkarte_files SET `deleted` = ? WHERE `id`= ? AND userid = ?', 1, $_REQUEST['id'], $thisUser->_id);
                header('Location: start.php?action=briefkarte&do=outbox&sid=' . session_id());
			}
            // adressbuch f�r Postkarte generieren
            elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == 'addressBook')
            {
                if(!class_exists('BMAddressbook'))
                    include(B1GMAIL_DIR . 'serverlib/addressbook.class.php');
                $adressbuch = _new('BMAddressbook', array($userRow['id']));
                
                $adressen = array();
        		$addressBook = $adressbuch->GetAddressBook('*', -1, 'nachname', 'asc');
        		foreach($addressBook as $id=>$entry)
        		{
                    // nur eintr�ge anzeigen, welche ben�tigte daten enthalten
        			if(trim($entry['plz']) != '' 
                    && trim($entry['strassenr']) != '' 
                    && trim($entry['ort']) != '')
                    {
        				$adressen[] = array('firstname' 		=> $entry['vorname'],
        										'lastname'		=> $entry['nachname'],
        										'strassenr'		=> $entry['strassenr'],
        										'ort'	    	=> $entry['plz']." ".$entry['ort'],
        										'id'			=> $entry['id']);
                    }
        		}
        		// display page
        		$tpl->assign('adressen', $adressen);
        		$tpl->display($this->_templatePath('briefkarte.user.addressbook.tpl'));
                exit();
            }
            // adressbuch f�r Brief generieren
            elseif(isset($_REQUEST['do']) && $_REQUEST['do'] == 'addressBookBrief')
            {
                if(!class_exists('BMAddressbook'))
                    include(B1GMAIL_DIR . 'serverlib/addressbook.class.php');
                $adressbuch = _new('BMAddressbook', array($userRow['id']));
                
                $adressen = array();
        		$addressBook = $adressbuch->GetAddressBook('*', -1, 'nachname', 'asc');
        		foreach($addressBook as $id=>$entry)
        		{
                    // nur eintr�ge anzeigen, welche ben�tigte daten enthalten
        			if(trim($entry['plz']) != '' 
                    && trim($entry['strassenr']) != '' 
                    && trim($entry['ort']) != '')
                    {
        				$adressen[] = array('firstname' 		=> $entry['vorname'],
        										'lastname'		=> $entry['nachname'],
        										'strassenr'		=> $entry['strassenr'],
        										'ort'	    	=> $entry['plz']." ".$entry['ort'],
        										'id'			=> $entry['id']);
                    }
        		}
        		// display page
        		$tpl->assign('adressen', $adressen);
        		$tpl->display($this->_templatePath('briefkarte.user.addressbook.brief.tpl'));
                exit();
            }
			// postkarten-eingabe-formular
			else
			{				
				$tpl->assign('bk_pk_feld_0', '');
				$tpl->assign('bk_pk_feld_1', $userRow['vorname']." ".$userRow['nachname']);
				$tpl->assign('pageContent', $this->_templatePath('briefkarte.user.postkarte.tpl'));

				// vorschau der postkarte
				if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'preview')
				{
                    //pfr�fen, ob bild existiert
                    if(file_exists($_FILES['bk_datei']['tmp_name']))
                    {
                        $dateityp = $this->DetectFileType($_FILES['bk_datei']['tmp_name']);
                        $dateiname = $erstellt . "_" . $_FILES['bk_datei']['name'];
                    }
                    else
                        $dateityp = "noPic";
                    
					$bkkey = GenerateRandomKey('bkp');

                    $tempFileID = RequestTempFile($userRow['id'], time()+4*TIME_ONE_HOUR);
                    $tempFileName = TempFileName($tempFileID);
					
					if($dateityp == "image/jpeg")
					{
						if($_FILES['bk_datei']['size'] < 2048000)
						{
							if(!empty($dateiname))
							{
                                if(fopen($tempFileName, 'w+'))
                                {
                                    $file = fopen($_FILES['bk_datei']['tmp_name'], 'r+');
                                    while(!feof($file))
                                    {
                                        $inhalt .= fgets($file, 4096);
                                    }
                                    fclose($file);
                                    
                                    $fp = fopen($tempFileName, 'w+');
                                    fwrite($fp, $inhalt);
                                    fclose($fp);
                                }
                             
                                $dpi = $this->dpi_auslesen_jpg($tempFileName);

								if($dpi >= 150)
								{
									$db->Query('INSERT INTO {pre}briefkarte_files (erstellt, bktyp, dateiname, seiten, land, bkkey, userid, status, deleted, kosten, auftrag) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
										$erstellt,
										'postkarte',
										$dateiname,
										0,
										$_REQUEST['bk_land'],
										$bkkey,
										$thisUser->_id,
                                        0,
                                        0,
                                        $this->GetPref("post_preis_".$_REQUEST['bk_land']),
                                        0);

									$newid = $db->InsertId();
									$verz = DataFilename($newid, 'bkp');
                                    //datei in data-folder datei kopieren
                                    copy($tempFileName, $verz);

									// bildlink f�r vorschau erstellen
									$bildlink = $bm_prefs['selfurl'] . "start.php?action=getBKFile&bkkey=" . $bkkey . "&id=" . $newid .  "&sid=" . session_id();

									$tpl->assign('bk_postkarte_bild', $bildlink);
									$tpl->assign('bkkey', $bkkey);
									$tpl->assign('bkid', $newid);
									$tpl->assign('bk_pk_feld_0', $_REQUEST['bk_pk_feld_0']);
									$tpl->assign('bk_pk_feld_1', $_REQUEST['bk_pk_feld_1']);
									$tpl->assign('bk_pk_feld_4', $_REQUEST['bk_pk_feld_4']);
									$tpl->assign('bk_pk_feld_5', $_REQUEST['bk_pk_feld_5']);
									$tpl->assign('bk_pk_feld_6', $_REQUEST['bk_pk_feld_6']);
									$tpl->assign('bk_pk_feld_7', $_REQUEST['bk_pk_feld_7']);
									// mit nl2br() enter(s) �bergeben
                                    $tpl->assign('bk_text_pk', nl2br($_REQUEST['bk_text_pk']));
                                    // originalen text f�r die �bermittlung an smskaufen �bergeben
                                    $tpl->assign('bk_text_pk_org', $_REQUEST['bk_text_pk']);
									$tpl->assign('bk_land', $_REQUEST['bk_land']);
									$tpl->assign('accCredits', $this->GetPostkartenCredits($_REQUEST['bk_land']));
									$tpl->assign('pageContent', $this->_templatePath('briefkarte.user.postkarte.preview.tpl'));
								}
								else
								{
									// bild l�schen, da es den anforderungen nicht entspricht
                                    ReleaseTempFile($userRow['id'], $tempFileID);
									$error_msg = "tolowdpi";
								}
							}
						}
						else
						{
							$error_msg = "tobig";
                            ReleaseTempFile($userRow['id'], $tempFileID);
						}
					}
					else
					{
						$error_msg = "wrongtype";
                        ReleaseTempFile($userRow['id'], $tempFileID);
					}
				}
				// postkarte senden
				if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'PostkarteSenden')
				{
					if($_REQUEST['bk_pk_feld_0'] != '')
						$from = $_REQUEST['bk_pk_feld_0'].", ";

					if($_REQUEST['bk_pk_feld_1'] != '')	
						$from .= $_REQUEST['bk_pk_feld_1'];

					// sicherheitsabfrage f�r maximal 57 zeichen beim absender
					if(strlen($from) > 57)
						die('Sorry, to long sender-information!');

                    // sicherheitspr�fung, ob user genug credits hat
                    if(!$this->GetPostkartenCredits($_REQUEST['bk_land']))
                        die('No Hacking please!');

					if($_REQUEST['bk_pk_feld_4'] != '') {
						$to = $_REQUEST['bk_pk_feld_4']."\n".$_REQUEST['bk_pk_feld_5']."\n".$_REQUEST['bk_pk_feld_6']."\n".$_REQUEST['bk_pk_feld_7'];
					} else {
						$to = $_REQUEST['bk_pk_feld_5']."\n".$_REQUEST['bk_pk_feld_6']."\n".$_REQUEST['bk_pk_feld_7'];
					}      
                            
					// senden
					$pksend = $this->sendPostkarte(utf8_decode($from), utf8_decode($to), utf8_decode($_REQUEST['bk_text_pk']), utf8_decode($_REQUEST['bk_land']), $_REQUEST['bkkey'], $_REQUEST['bkid']);

					// erfolgreicher versand -> berechnung der postkarte
					if(strlen($pksend) == 13)
					{
						$kosten = $this->paySendPostkarte($_REQUEST['bk_land']);

                        // status-update auf db �bertragen
                        $db->Query('UPDATE {pre}briefkarte_files SET status=?, auftrag=? WHERE bkkey=? AND `id`=?', 1, $pksend, $_REQUEST['bkkey'], $_REQUEST['bkid']);

                        //statistik updaten
                        $this->bkStats(1, $kosten, "postkarte");
                        
						PutLog('Sent postcard by user <#' . $thisUser->_id . '> (order-number: ' . $pksend . '; charged: Yes)', PRIO_NOTE, __FILE__, __LINE__);

						$tpl->assign('title', 		$lang_user['bk_karte']);
						$tpl->assign('msg', 		$lang_user['bk_karte_sendok']);
						$tpl->assign('backLink', 	'start.php?action=briefkarte&sid=' . session_id());
						$tpl->assign('pageContent', 'li/msg.tpl');
					}
                    else
                    {
						PutLog('Error sending postcard! (user: #' . $thisUser->_id . ', error-code: ' . $pksend . ')', PRIO_WARNING, __FILE__, __LINE__);
						$error_msg = "bknotsend";
					}
				}
				// vorschau l�schen (daten aus db und aus tmp-verzeichnis l�schen)
				if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'PostkarteRemove')
				{
					$res = $db->Query('SELECT dateiname FROM {pre}briefkarte_files WHERE bkkey = ? AND id = ?',
						$_REQUEST['bkkey'],
						$_REQUEST['bkid']);
					$row = $res->FetchArray(); 
					$dateiname = $row['dateiname'];
					$res->Free();
                    
                    // file aus data-verzeichnis l�schen
                    $filename = DataFilename($row['id'], 'bkp');
        			// pr�fen, ob datei existiert
        			if(file_exists($filename))
        			{
        				unlink($filename);
        			}
                    
					// bild aus db l�schen
					$db->Query("DELETE FROM {pre}briefkarte_files WHERE bkkey = ? AND id = ?", $_REQUEST['bkkey'], $_REQUEST['bkid']);
				}
			}

			if($error_msg == "wrongtype")
			{
				$tpl->assign('msg', $lang_user['bk_karte_picerror']);
				$tpl->assign('pageContent', 'li/error.tpl');
			}
			elseif($error_msg == "tolowdpi")
			{
				$tpl->assign('msg', $lang_user['bk_karte_dpierror']);
				$tpl->assign('pageContent', 'li/error.tpl');			
			}
			elseif($error_msg == "tobig")
			{
				$tpl->assign('msg', $lang_user['bk_karte_groesseerror']);
				$tpl->assign('pageContent', 'li/error.tpl');			
			}
			elseif($error_msg == "bknotsend")
			{
				$tpl->assign('msg', $lang_user['bk_karte_senderror']);
                $tpl->assign('backLink', 	'start.php?action=briefkarte&&sid=' . session_id());
				$tpl->assign('pageContent', 'li/error.tpl');			
			}
			
			$tpl->assign('activeTab', 		'briefkarte');
			$tpl->assign('pageToolbarFile', 'li/sms.toolbar.tpl');
			$tpl->assign('pageMenuFile',	$this->_templatePath('briefkarte.user.sidebar.tpl'));
			$tpl->assign('accBalance', 		$thisUser->GetBalance());
			$tpl->assign('defaultTpl',		$defaultTemplate);
			$tpl->display('li/index.tpl');
		}
	}

	// credit-�berpr�fung f�r postkarten-versand
	function GetPostkartenCredits($land)
	{
		global $thisUser;

		// pr�ft ob genug credits vorhanden sind
		if($thisUser->GetBalance() >= $this->GetPref("post_preis_".$land))
			return true; 
		return false;
	}
    
    // credit-�berpr�fung f�r brief-versand
	function GetBriefCredits($credits)
	{
		global $thisUser;

		// pr�ft ob genug credits vorhanden sind
		if($thisUser->GetBalance() >= $credits)
			return true; 
		return false;
	}

	// dateityp ermitteln und zur�ckgeben
	function DetectFileType($file)
	{
        $image_data = getimagesize($file);
        
        // Hinweis zu den g�ngigsten Bildtypen, neben image/png und image/gif muss f�r JPG Bilder immer image/jpeg, also die lange Form genutzt werden. 
        // Wer auf der sicheren Seite sein will f�gt zus�tzlich noch image/pjpeg seiner Abfrage hinzu. 
        // Das p steht f�r progressiv und gerade der IE sendet diesen Typ bei JPGs gerne mit.
        if ($image_data['mime'] == 'image/jpeg' || $image_data['mime'] == 'image/pjpeg')
			return('image/jpeg');
		else 
			return('application/octet-stream');
	}

	// dpi-gr��e des jpg-bildes auslesen
	function dpi_auslesen_jpg($datei)
	{
	   $fh = fopen($datei, 'r');
	   $header = fread($fh, 16);
	   fclose($fh);
	   $aufloesung = unpack('x14/ndpi', $header);

	   return $aufloesung['dpi'];
	}
	
	// versendete postkarte berechnen
	function paySendPostkarte($land)
	{
		global $thisUser;

		$userObject = _new('BMUser', array($thisUser->_id));
		$userObject->Debit($this->GetPref("post_preis_".$land)*(-1));

        $kosten = $this->GetPref("post_preis_".$land);
		return $kosten;
    }
	
	// versendeten brief berechnen
	function paySendBrief($land, $seiten, $farbe, $einschreiben, $duplex)
	{
		global $thisUser, $lang_user;

		$preis_s1u2 = $this->GetPref("brief_preis_s1u2");
		$preis_ab3s = $this->GetPref("brief_preis_ab3s");
		$preis_porto_1b4s = $this->GetPref("brief_preis_porto_1b3s_".$land);
		$preis_porto_5b16s = $this->GetPref("brief_preis_porto_4b15s_".$land);
		$preis_porto17b99s = $this->GetPref("brief_preis_porto_16b99s_".$land);
		$preis_farbe = $this->GetPref("brief_preis_farbe_".$land);
        
        $preis_einschreiben = 0;        
        
        if($einschreiben == 1)
            $preis_einschreiben = $this->GetPref("bk_preis_einschreiben");
		if($einschreiben == 2)
            $preis_einschreiben = $this->GetPref("bk_preis_einschreiben_online");
		if($einschreiben == 3)
            $preis_einschreiben = $this->GetPref("bk_preis_einschreiben_rs");

		// farbe hat wert 0 (s/w) oder 1 (farbe)
		$farbe = $farbe * $preis_farbe;
		$farbe = $farbe * $seiten;

        // 2-seitiger druck
        if($duplex)
        {
            if($seiten > 0 && $seiten <= 4)
    			$preis = $preis_s1u2 + $preis_porto_1b4s + $farbe + $preis_einschreiben;
    		elseif($seiten >= 5 && $seiten <= 16)
    			$preis = ($preis_s1u2 + $preis_porto_5b16s + $farbe + $preis_einschreiben) + (($seiten-2) * $preis_ab3s);
    		elseif($seiten >= 17 && $seiten <= 99)
    			$preis = ($preis_s1u2 + $preis_porto17b99s + $farbe + $preis_einschreiben) + (($seiten-2) * $preis_ab3s);
    		else
    		{
    			$tpl->assign('msg', $lang_user['paybrieffailed']);
    			$tpl->assign('pageContent', 'li/error.tpl');
    			$tpl->display('li/index.tpl');
    			exit();
    		}
    		return $preis;
        }
        else
        {
            if($seiten > 0 && $seiten <= 2)
    			$preis = $preis_s1u2 + $preis_porto_1b4s + $farbe + $preis_einschreiben;
    		elseif($seiten >= 3 && $seiten <= 8)
    			$preis = ($preis_s1u2 + $preis_porto_5b16s + $farbe + $preis_einschreiben) + (($seiten-2) * $preis_ab3s);
    		elseif($seiten >= 9 && $seiten <= 50)
    			$preis = ($preis_s1u2 + $preis_porto7b99s + $farbe + $preis_einschreiben) + (($seiten-2) * $preis_ab3s);
    		else
    		{
    			$tpl->assign('msg', $lang_user['paybrieffailed']);
    			$tpl->assign('pageContent', 'li/error.tpl');
    			$tpl->display('li/index.tpl');
    			exit();
    		}
    		return $preis;
        }
	}
	
	// postkarte versenden
	function sendPostkarte($from, $to, $msg, $land, $bkkey, $bkid)
	{
		global $db, $bm_prefs;

		if($land == "de") {
			$land = 0;
		} else {
			$land = 1;
		}

		$res = $db->Query('SELECT id, bktyp FROM {pre}briefkarte_files WHERE bkkey = ? AND `id` = ?',
			$bkkey,
			$bkid);
		$row = $res->FetchArray(); 
		//$dateiname = $row['dateiname'];
		$res->Free();
        
        // dateinamen generieren
		$filename = DataFilename($row['id'], 'bkp');
	
		// pr�fen, ob datei existiert
		if(file_exists($filename))
		{				
			if($row['bktyp'] == "postkarte")
			{
				$datei = $filename;
			}			
		}
        
        $time = time();
        // datei-kopie vom original f�r schnittstelle im tmp-verzeichnis erstellen
		copy($datei, B1GMAIL_DIR."temp/postkarte_".$time.".jpg");
		// rechte setzen
		chmod(B1GMAIL_DIR."temp/postkarte_".$time.".jpg",0777);
        //neuen pfad zuweisen
        $datei = B1GMAIL_DIR."temp/postkarte_".$time.".jpg";
        
		$feed = $bm_prefs["selfurl"]."index.php";
			
		$form["text"] = $msg;
		$form["empfaenger"] = $to;
		$form["absender"] = $from;
		$form["feed"] = $feed;
		$form["id"] = $this->GetPref("post_user");
		$form["pw"] = $this->GetPref("post_pw");
		$form["art"] = "p";
		$form["mode"] = TRANSFERMODE;
		// werte von land: 1 (europa) - 0 (deutschland)
		$form["ausland"] = $land;
		$form["code"] = $bkid;
		
		if(@filesize($datei))
		{
			// das @ ist entscheidend, damit es als file �bertragen wird.
			$form["document"] = "@".$datei;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->GetPref("post_gw"));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $form);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$result = curl_exec($ch);
			curl_close($ch);
		}
		else
		{
			// keine datei vorhanden/hochgeladen
			$result = "123";
		}
        
        // tmp-bild l�schen
	    unlink($datei);
				
		return $result;
	}
    
    //brief tempor�r speichern
    function saveBrief($land, $kosten, $dateiname, $seitenAnz, $duplex, $einschreiben)
    {
        global $db, $thisUser;
        
        $erstellt = time();
        $bkkey = GenerateRandomKey('bkb');
        
        $db->Query('INSERT INTO {pre}briefkarte_files (erstellt, bktyp, dateiname, seiten, land, bkkey, userid, status, deleted, kosten, auftrag, duplexdruck, einschreiben) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
						$erstellt,
						'brief',
						$dateiname,
						$seitenAnz,
						$land,
						$bkkey,
						$thisUser->_id,
                        0,
                        0,
                        $kosten,
                        0,
                        $duplex,
                        $einschreiben);
                        
        $newid = $db->InsertId();
		$verz = DataFilename($newid, 'bkb');
        copy($dateiname, $verz);
        
        if($newid)
        {
            $daten = array($bkkey, $newid);
            return $daten;
        }
    }
        
    function checkBrief($seiten, $farbe, $land, $einschreiben, $duplex)
    {
        //preis berechnen
        $credits = $this->paySendBrief($land, $seiten, $farbe, $einschreiben, $duplex);
        
        //pr�fen, ob user genug credits hat
        $pay = $this->GetBriefCredits($credits);
        
        return $credits;
    }
    
    function createPDFtmp($text, $tempFileName, $tempFileID, $fileExists, $land, $farbe, $deckblatt, $db_abs, $db_firma, $db_name, $db_strhnr, $db_plzort, $db_land, $einschreiben, $duplex)
    {
        global $userRow, $lang_user, $tpl, $groupRow;
        
        // set locale to english for FPDF comma stuff...
		$locales = array('en_US', 'en_EN', 'en', 'english');
		foreach($locales as $locale)
			if(setlocale(LC_NUMERIC, $locale))
				break;
                
        // nur text ist vorhanden
        if(!$fileExists)
		{
			ReleaseTempFile($userRow['id'], $tempFileID);
            
            if($text != '')
            {
                $tempFileID = RequestTempFile($userRow['id'], time()+4*TIME_ONE_HOUR);
                $tempFileName = TempFileName($tempFileID);
                
                //pr�fen, ob usergruppe signatur hat
                if($this->GetGroupOptionValue('bk_signatur', $groupRow['id']))
                {
                    $pdf = new PDF();
                    //signatur �bergeben
                    $pdf->setSignatur($this->GetGroupOptionValue('bk_signatur', $groupRow['id']));
                }
                else
                    $pdf = new FPDI();
                    
                //deckblatt hinzuf�gen
                if($deckblatt)
                {
                    $pdf->AddPage();
                    
                    $y_kopf = 45;
                    $pdf->SetFont('Arial','',7);
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->Cell(100,5,$db_abs,0,0);
                    
                    $y_kopf = 50;
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->SetFont('Arial','',12);
                    $pdf->Cell(87, 37, '', 1, 0, 'L');
                    
                    $y_kopf = 55;
                    $pdf->SetFont('Arial','',12);
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->Cell(100,5,$db_firma,0,0);
                    
                    $y_kopf = 60;
                    $pdf->SetFont('Arial','',12);
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->Cell(100,5,$db_name,0,0);
                    
                    $y_kopf = 65;
                    $pdf->SetFont('Arial','',12);
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->Cell(100,5,$db_strhnr,0,0);
                    
                    $y_kopf = 70;
                    $pdf->SetFont('Arial','',12);
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->Cell(100,5,$db_plzort,0,0);
                    
                    $y_kopf = 75;
                    $pdf->SetFont('Arial','',12);
                    $pdf->SetY($y_kopf);
                    $pdf->SetX(15);
                    $pdf->Cell(100,5,$db_land,0,0);
                
                    // text f�r deckblatt
                    $pdf->SetY(100);
                    $pdf->SetX(10);
                    $pdf->SetFont('Arial','',8); 
                    // Seitenabstand definieren (links, oben, rechts)
                    $pdf->SetMargins(10, 10, 10);
                    // Automatischen Seitenumbruch aktivieren (1,5 cm)
                    $pdf->SetAutoPageBreak(true, 15); 
                    // Formatierten Text ausgeben mit automatischen Zeilenumbruch
                    $pdf->MultiCell(0, 5, $text);
                }
                else
                {
                    // Schriftart festlegen
                    $pdf->SetFont('Arial','',8);            
                    // Seite hinzuf�gen
                    $pdf->AddPage();
                    // Seitenabstand definieren (links, oben, rechts)
                    $pdf->SetMargins(10, 10, 10);
                    // Automatischen Seitenumbruch aktivieren (1,5 cm)
                    $pdf->SetAutoPageBreak(true, 15); 
                    // Formatierten Text ausgeben mit automatischen Zeilenumbruch
                    $pdf->MultiCell(0, 5, $text);
                }
                
                // F = speichern (auf server) ::: D = downloaden (an client senden)
                $pdf->Output($tempFileName, 'F');
                // gesamt-seitenanzahl ermitteln
                $seitenAnz = $pdf->PageNo();
                
                $result = $this->checkBrief($seitenAnz, $farbe, $land, $einschreiben, $duplex);
                
                $kosten = $result;
                $dateiname = $tempFileName;
                
                //brief speichern
                $daten = $this->saveBrief($land, $kosten, $dateiname, $seitenAnz, $duplex, $einschreiben);

                if(count($daten) > 0)
                {
                    //bkkey, id, kosten, seitenanzahl
                    $inhalt = array($daten[0], $daten[1], $result, $seitenAnz);
                    return $inhalt;
                }
                else
                    return false;
            }
            else
                return false;
		}
        // datei muss existieren und text kann vorhanden sein
        else
        {                   
            if(fopen($tempFileName, 'r'))
            {
                $tempFileIDnew = RequestTempFile($userRow['id'], time()+4*TIME_ONE_HOUR);
                $tempFileNameNew = TempFileName($tempFileIDnew);
                
                // erste zeile aus tmp-datei auslesen
        		$fp = fopen($tempFileName, 'r');
        		$firstLine = fgets($fp, 128);
        		fclose($fp);
                
                // check type + version
        		if(substr($firstLine, 0, 5) == '%PDF-'
        			&& sscanf($firstLine, '%%PDF-%d.%d', $pdfMajor, $pdfMinor) == 2
        			&& $pdfMajor <= 1 && $pdfMinor <= 4)
        		{
                    //pr�fen, ob usergruppe signatur hat
                    if($this->GetGroupOptionValue('bk_signatur', $groupRow['id']))
                    {
                        $pdf = new PDF();
                        //signatur �bergeben
                        $pdf->setSignatur($this->GetGroupOptionValue('bk_signatur', $groupRow['id']));
                    }
                    else
                        $pdf = new FPDI();
                        
                    //deckblatt hinzuf�gen
                    if($deckblatt)
                    {
                        $pdf->AddPage();
                        
                        $y_kopf = 45;
                        $pdf->SetFont('Arial','',7);
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->Cell(100,5,$db_abs,0,0);
                        
                        $y_kopf = 50;
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->SetFont('Arial','',12);
                        $pdf->Cell(87, 37, '', 1, 0, 'L');
                        
                        $y_kopf = 55;
                        $pdf->SetFont('Arial','',12);
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->Cell(100,5,$db_firma,0,0);
                        
                        $y_kopf = 60;
                        $pdf->SetFont('Arial','',12);
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->Cell(100,5,$db_name,0,0);
                        
                        $y_kopf = 65;
                        $pdf->SetFont('Arial','',12);
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->Cell(100,5,$db_strhnr,0,0);
                        
                        $y_kopf = 70;
                        $pdf->SetFont('Arial','',12);
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->Cell(100,5,$db_plzort,0,0);
                        
                        $y_kopf = 75;
                        $pdf->SetFont('Arial','',12);
                        $pdf->SetY($y_kopf);
                        $pdf->SetX(15);
                        $pdf->Cell(100,5,$db_land,0,0);
                    
                        // text f�r deckblatt
                        $pdf->SetY(100);
                        $pdf->SetX(10);
                        $pdf->SetFont('Arial','',8); 
                        // Seitenabstand definieren (links, oben, rechts)
                        $pdf->SetMargins(10, 10, 10);
                        // Automatischen Seitenumbruch aktivieren (1,5 cm)
                        $pdf->SetAutoPageBreak(true, 15); 
                        // Formatierten Text ausgeben mit automatischen Zeilenumbruch
                        $pdf->MultiCell(0, 5, $text);
                    }
                        
                    // seitenanzahl aus dem hochgeladenen pdf holen
    				$sAnzUp = @$pdf->setSourceFile($tempFileName);                                
                    $pdf->setSourceFile($tempFileName);
                    
                    // alle seiten aus dem hochgeladenen pdf zur neuen pdf hinzuf�gen
                    $i = 1;
                    while($i <= $sAnzUp)
                    {
                        $pdf->AddPage();
                        // Automatischen Seitenumbruch aktivieren (1,5 cm)
                        $pdf->SetAutoPageBreak(true, 15);
                        $tplIdx = $pdf->importPage($i);
                        $pdf->useTemplate($tplIdx, 0, 0);
                        $i+=1;
                    }                                                

                    if($text != '' && !$deckblatt)
                    {
                        // Schriftart festlegen
                        $pdf->SetFont('Arial','',8);            
                        // Seite hinzuf�gen
                        $pdf->AddPage();
                        // Seitenabstand definieren (links, oben, rechts)
                        $pdf->SetMargins(10, 10, 10);
                        // Automatischen Seitenumbruch aktivieren (1,5 cm)
                        $pdf->SetAutoPageBreak(true, 15); 
                        // Formatierten Text ausgeben mit automatischen Zeilenumbruch
                        $pdf->MultiCell(0, 5, $text); 
                    }
                    // F = speichern (auf server) ::: D = downloaden (an client senden)
                    $pdf->Output($tempFileNameNew, 'F');
                    // gesamt-seitenanzahl ermitteln
                    $seitenAnz = $pdf->PageNo();
                    
                    $result = $this->checkBrief($seitenAnz, $farbe, $land, $einschreiben, $duplex);
                
                    $kosten = $result;
                    $dateiname = $tempFileNameNew;
                    
                    //brief speichern
                    if($result)
                        $daten = $this->saveBrief($land, $kosten, $dateiname, $seitenAnz, $duplex, $einschreiben);
                    
                    if(count($daten) > 0)
                    {
                        //bkkey, id, kosten, seitenanzahl
                        $inhalt = array($daten[0], $daten[1], $result, $seitenAnz);
                        return $inhalt;
                    }
                    else
                        return false;
                }
                else
                {
                    //falsches pdf-format
                    ReleaseTempFile($userRow['id'], $tempFileID);
                    $tpl->assign('msg', $lang_user['b_wrongformat']);
				    $tpl->assign('pageContent', 'li/error.tpl');
                }
            }
            else
                return false;
        }
    }
    
    // brief versenden
	function sendBrief($farbe, $land, $bkey, $bid, $einschreiben, $duplex)
	{
		global $db, $bm_prefs, $userRow;

		if($land == "de") {
			$land = 0;
		} 
        else
        {
			$land = 1;
		}
        
        //2-seitiger druck
        if($duplex)
            $duplexmode = 0;
        else
            $duplexmode = 1;
            
        //einschreiben
        if($einschreiben == 1)
            $einschreiben = 1;
		if($einschreiben == 2)
            $einschreiben = 2;
		if($einschreiben == 3)
            $einschreiben = 3;
        else
            $einschreiben = 0;
        
        //farbig, wenn farbe=1 ist
        if($farbe == 1)
            $farbe = "f";
        else
            $farbe = ''; 

		$res = $db->Query('SELECT id, bktyp FROM {pre}briefkarte_files WHERE bkkey = ? AND `id` = ?',
			$bkey,
			$bid);
		$row = $res->FetchArray(); 
		$res->Free();
        
		// dateinamen generieren
		$filename = DataFilename($row['id'], 'bkb');
	
		// pr�fen, ob datei existiert
		if(file_exists($filename))
		{				
			if($row['bktyp'] == "brief")
			{
				$datei = $filename;
			}			
		}

		$feed = $bm_prefs["selfurl"]."index.php";

		$form["feed"] = $feed;
        $form["color"] = $farbe;
		$form["id"] = $this->GetPref("brief_user");
		$form["pw"] = $this->GetPref("brief_pw");
		$form["art"] = "b";
		$form["mode"] = TRANSFERMODE;
		// werte von land: 1 (europa) - 0 (deutschland)
		$form["ausland"] = $land;
		$form["code"] = $bid;
        $form["duplexaus"] = $duplexmode;
        $form["einschreiben"] = $einschreiben;
		
		// r�ckschein bei einschreiben mit r�ckschein
		if($einschreiben == 3)
		{
			$form["adr_name"] = utf8_decode($userRow['nachname']);
			$form["adr_vorname"] = utf8_decode($userRow['vorname']);
			$form["adr_strasse"] = utf8_decode($userRow['strasse'])." ".utf8_decode($userRow['hnr']);
			$form["adr_ort"] = utf8_decode($userRow['plz'])." ".utf8_decode($userRow['ort']);
		}

		if(filesize($datei))
		{
			// das @ ist entscheidend, damit es als file �bertragen wird.
			$form["document"] = "@".$datei;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->GetPref("brief_gw"));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $form);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$result = curl_exec($ch);
			curl_close($ch);
		}
		else
		{
			// keine datei vorhanden/hochgeladen
			$result = "123";
		}
				
		return $result;
	}
    
    //statistik updaten
    function bkStats($status, $credits, $typ)
    {
        if($status)
        {
            if($typ == "brief")
            {
                $this->SetPref("b_day_ok", $this->GetPref("b_day_ok")+1);
                $this->SetPref("b_month_ok", $this->GetPref("b_month_ok")+1);
                $this->SetPref("b_all_ok", $this->GetPref("b_all_ok")+1);
                
                $this->SetPref("b_credits_day_ok", $this->GetPref("b_credits_day_ok")+$credits);
                $this->SetPref("b_credits_month_ok", $this->GetPref("b_credits_month_ok")+$credits);
                $this->SetPref("b_credits_all_ok", $this->GetPref("b_credits_all_ok")+$credits);
            }
            else
            {
                $this->SetPref("bk_day_ok", $this->GetPref("bk_day_ok")+1);
                $this->SetPref("bk_month_ok", $this->GetPref("bk_month_ok")+1);
                $this->SetPref("bk_all_ok", $this->GetPref("bk_all_ok")+1);
                
                $this->SetPref("bk_credits_day_ok", $this->GetPref("bk_credits_day_ok")+$credits);
                $this->SetPref("bk_credits_month_ok", $this->GetPref("bk_credits_month_ok")+$credits);
                $this->SetPref("bk_credits_all_ok", $this->GetPref("bk_credits_all_ok")+$credits);
            }
        }
        else
        {
            if($typ == "brief")
            {
                $this->SetPref("b_day_error", $this->GetPref("b_day_error")+1);
                $this->SetPref("b_month_error", $this->GetPref("b_month_error")+1);
                $this->SetPref("b_all_error", $this->GetPref("b_all_error")+1);
                
                $this->SetPref("b_credits_day_error", $this->GetPref("b_credits_day_error")+$credits);
                $this->SetPref("b_credits_month_error", $this->GetPref("b_credits_month_error")+$credits);
                $this->SetPref("b_credits_all_error", $this->GetPref("b_credits_all_error")+$credits);
            }
            else
            {
                $this->SetPref("bk_day_error", $this->GetPref("bk_day_error")+1);
                $this->SetPref("bk_month_error", $this->GetPref("bk_month_error")+1);
                $this->SetPref("bk_all_error", $this->GetPref("bk_all_error")+1);
                
                $this->SetPref("bk_credits_day_error", $this->GetPref("bk_credits_day_error")+$credits);
                $this->SetPref("bk_credits_month_error", $this->GetPref("bk_credits_month_error")+$credits);
                $this->SetPref("bk_credits_all_error", $this->GetPref("bk_credits_all_error")+$credits);
            }
        }
    }
    
	function getPostId($aid)
	{
        if(!class_exists('BMHTTP'))
            include(B1GMAIL_DIR . 'serverlib/http.class.php');

        $bUser = $this->GetPref("brief_user");
		$bPw = $this->GetPref("brief_pw");
        $bGw = $this->GetPref("postID_gw");
        
        // gateway zusammenstellen        
        $bGw = $bGw."?id=".$bUser."&pw=".$bPw."&auftrag=".$aid;
                
		// werte�bergabe an den gateway						
		$http = _new('BMHTTP', array($bGw));
		$result = $http->DownloadToString();

        return $result;
	}
	
    function bkConvert($text)
	{
		global $currentCharset;
		
		if(function_exists('CharsetDecode'))
			return(CharsetDecode($text, false, 'ISO-8859-15'));
		
		if(in_array(strtolower($urrentCharset), array('utf8', 'utf-8')) && function_exists('utf8_decode'))
			return(utf8_decode($text));
		
		return($text);
	}
}

$plugins->registerPlugin('briefkarte');

?>