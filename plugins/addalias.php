<?php
/*
 * Plugin addalias
 */
class addalias extends BMPlugin 
{
	/*
	* Eigenschaften des Plugins
	*/
	function addalias()
	{
		$this->name					= "Add Alias";
		$this->version				= '1.1.0';
		$this->designedfor			= '7.3.0';
		$this->type					= BMPLUGIN_DEFAULT;
		
		$this->author				= 'dotaachen';
		$this->mail					= 'b1g@dotaachen.net';
		$this->web 					= 'http://b1g.dotaachen.net';	

		$this->update_url			= 'http://my.b1gmail.com/update_service/';
		$this->website				= 'http://my.b1gmail.com/details/80/';

		$this->admin_pages			= true;
		$this->admin_page_title		= "Add Alias";
		$this->admin_page_icon		= "addalias_icon.png";
	}

	/*
	*  Link  und Tabs im Adminbereich 
	*/
	function AdminHandler()
	{
		global $tpl, $lang_admin;

		// Plugin aufruf ohne Action
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'page1';

		// Tabs im Adminbereich
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['create'],
				'link'		=> $this->_adminLink() . '&action=page1&',
				'active'	=> $_REQUEST['action'] == 'page1',
				'icon'		=> '../plugins/templates/images/addalias_logo.png'
			),
			1 => array(
				'title'		=> $lang_admin['faq'],
				'link'		=> $this->_adminLink() . '&action=page2&',
				'active'	=> $_REQUEST['action'] == 'page2',
				'icon'		=> './templates/images/faq32.png'
			),
		);
		$tpl->assign('tabs', $tabs);

		// Plugin aufruf mit Action 
		if($_REQUEST['action'] == 'page1') {
			$tpl->assign('page', $this->_templatePath('addalias1.pref.tpl'));
			$this->_Page1();
		} else if($_REQUEST['action'] == 'page2') {
			$tpl->assign('page', $this->_templatePath('addalias2.pref.tpl'));
		}
	}
	
	/*
	*  Sprach variablen
	*/
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		global $lang_user;

		$lang_admin['addalias_name']	=	"Add Alias";
		$lang_admin['addalias_text']	=	"Mit diesem Plugin k&ouml;nnen Sie einzelnen Benutzern einen Alias erstellen.";

		$lang_admin['addresstaken']		=	$lang_user['addresstaken'];
		$lang_admin['alias']			=	$lang_user['alias'];
		$lang_admin['aliastype_1']		= 	$lang_user['aliastype_1'];
		$lang_admin['aliastype_2']		=	$lang_user['aliastype_2'];
	}

	/*
	 * installation routine
	 */
	function Install()
	{
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return true;
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	* Gruppen, Benutzer aufwaehlen und Alias erstellen
	*/
	function _Page1()
	{
		global $tpl, $db, $bm_prefs;

		// variable fuer template
		$tpl_use = 0;
		// gruppen und users arrays
		$gruppen = array();
		$users = array();

		// array gruppen fuellen
		$res = $db->Query('SELECT id, titel FROM {pre}gruppen ORDER by titel ASC');
		while($row = $res->FetchArray())
		{
			$gruppen[$row['id']] = array(
				'id'		=> $row['id'],
				'titel'		=> $row['titel'],
			);
		}
		$res->Free();

		// wenn gruppe_hidden benutzt wird, gruppe fuellen
		if($_REQUEST['gruppe_hidden'] != "")
		{
			$_REQUEST['gruppe'] = $_REQUEST['gruppe_hidden'];
		}

		// wenn gruppe alle dann alle user abfraqen
		if($_REQUEST['gruppe'] == -1)
		{
			$res = $db->Query('SELECT id, email FROM {pre}users ORDER by email ASC');
		} else {
			$res = $db->Query('SELECT id, email FROM {pre}users WHERE gruppe=? ORDER by email ASC', 
				(int ) $_REQUEST['gruppe']);
		}

		// array users fuellen
		while($row = $res->FetchArray())
		{
			$users[$row['id']] = array(
				'id'		=> $row['id'],
				'email'		=> $row['email'],
			);
		}
		$res->Free();

		// template variable je nach fortschritt aendern
		if(isset($_REQUEST['gruppe']))
		{
			$tpl_use = 1;

			$_REQUEST['gruppe_hidden'] = $_REQUEST['gruppe'];
		}
		if(isset($_REQUEST['user']))
		{
			$tpl_use = 2;

			$domainList		= $bm_prefs['domains'];
			if(!is_array($domainList))
			{
				$domainList = explode(':', $domainList);
			}
			// array domainlist fuellen
			$res = $db->Query('SELECT id, saliase FROM {pre}gruppen ORDER by titel ASC');
			while($row = $res->FetchArray())
			{
				if($row['saliase']!="")
				{
					$domainList2 = explode(':', $row['saliase']);
					foreach($domainList2 as $domain)
					{
						if(!in_array($domain, $domainList))
						{
							$domainList[] = $domain;
						}
					}
					
				}
			}
			$res->Free();
	
			$tpl->assign('domainList', $domainList);

			$_REQUEST['user_hidden'] = $_REQUEST['user'];
		}

		// wenn email_domain gefuellt dann db speichern
		if(isset($_REQUEST['email_domain']))
		{
			$tpl_use = 3;
			$tpl_email_locked = false;

			// --b1gmail code anfang--
			if($_REQUEST['typ_1_email'] != "")
			{
				$emailAddress = $_REQUEST['typ_1_email'];
			} else {
				$emailAddress = $_REQUEST['email_local'] . '@' . $_REQUEST['email_domain'];
			}

			// emailaddress besetzt durch users
			$res = $db->Query('SELECT id FROM {pre}users WHERE email=?', 
				$emailAddress);
			if($res->RowCount() >= 1)
			{
				$tpl_email_locked = true;
			}
			$res->Free();

			// emailaddress besetzt durch aliase
			$res = $db->Query('SELECT id FROM {pre}aliase WHERE email=?', 
				$emailAddress);
			if($res->RowCount() >= 1)
			{
				$tpl_email_locked = true;
			}
			$res->Free();

			if($tpl_email_locked == false)
			{
				if($_REQUEST['typ_1_email'] != "")
				{
					// add sender
					$db->Query('INSERT INTO {pre}aliase(email,user,type,date) VALUES(?,?,?,?)',
						$emailAddress,
						(int) $_REQUEST['user_hidden'],
						(int) 1,
						(int) time());
				} else {
					// add empfaenger und sender
					$db->Query('INSERT INTO {pre}aliase(email,user,type,date) VALUES(?,?,?,?)',
						$emailAddress,
						(int) $_REQUEST['user_hidden'],
						(int) 3,
						(int) time());
				}
			}
			// --b1gmail code ende--
			$_REQUEST['gruppe_hidden'] = "";
		}

		// tpl variablen uebergeben
		$tpl->assign('gruppen', $gruppen);
		$tpl->assign('users', $users);	
		$tpl->assign('selected_gruppe', $_REQUEST['gruppe_hidden']);
		$tpl->assign('selected_user', $_REQUEST['user_hidden']);
		$tpl->assign('tpl_use', $tpl_use);
		$tpl->assign('tpl_email_locked', $tpl_email_locked);
	}
}
/*
 * register plugin
 */
$plugins->registerPlugin('addalias');
?>