<?php 
/*
 * 
 * (c) Informant
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: powersms.plugin.php,v 2.1 2013/05/12 11:30:00 Informant $
 *
 */

class powerSMS extends BMPlugin 
{	
	function powerSMS()
	{		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Power-SMS';
		$this->author			= 'Informant';
		$this->version			= '2.1';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
		
		// admin pages
		$this->admin_pages		= true;
		$this->admin_page_title	= 'Power-SMS';
		$this->admin_page_icon	= "modlargesms16.png";
	}
	
	function AfterInit()
	{
		global $lang_admin;
		
		// group option
		$this->RegisterGroupOption('largeSMS', FIELD_CHECKBOX, 'Power-SMS?');
		$this->RegisterGroupOption('freeSMSday', FIELD_TEXT, $lang_admin['largesmsday']);
		$this->RegisterGroupOption('freeSMSmonth', FIELD_TEXT, $lang_admin['largesmsmonth']);
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			// user
			$lang_user['sendlargesms']			= 'Power-SMS senden';
			$lang_user['sendfreesms']			= 'Free-SMS senden';
			$lang_user['sendlargesmsready']		= 'Gesendete Power-SMS';
			$lang_user['freesms_heute']			= 'Free-SMS heute';
			$lang_user['freesms_monat']			= 'Monat';
			$lang_user['freesmswarning_rs']		= 'Sie haben vor kurzem bereits eine Free-SMS versendet. Um Missbrauch vorzubeugen, k&ouml;nnen Sie innerhalb von %%stunden%% Stunde(n) nur eine Free-SMS versenden. Bitte versuchen Sie es sp&auml;ter erneut.';
			$lang_user['powersmsakt_preis']		= 'aktuelle Kosten';
			$lang_user['sendlargesmsok']		= 'Ihre Power-SMS wurde erfolgreich versendet.';
			$lang_user['sendfreesmsok']			= 'Ihre Free-SMS wurde erfolgreich versendet.';
			$lang_user['sendfreesmsokfailed']	= 'Ihre Free-SMS konnte nicht gesendet werden. Ein tempor&auml;rer interner Fehler ist aufgetreten. Bitte versuchen Sie es sp&auml;ter erneut.';
			$lang_user['sendlargesmsokfailed']	= 'Ihre Power-SMS konnte nicht gesendet werden. M&ouml;gliche Ursache ist ein zu kleines Guthaben oder ein tempor&auml;rer interner Fehler. Bitte versuchen Sie es sp&auml;ter erneut.';
			$lang_user['changeAbs']				= 'SMS-Absender &auml;ndern?';
			$lang_user['largesmsanz']			= 'SMS-Anzahl:';
			$lang_user['send2sms']				= 'SMS absenden';
			$lang_user['freesmswarning_month']	= 'Bedauerlicherweise ist Ihr Free-SMS Kontingent f&uuml;r diesen Monat bereits aufgebraucht. Im n&auml;chsten Monat k&ouml;nnen Sie wieder Free-SMS versenden.';
			$lang_user['freesmswarning_day']	= 'Bedauerlicherweise ist Ihr Free-SMS Kontingent f&uuml;r heute aufgebraucht. Morgen k&ouml;nnen Sie wieder Free-SMS versenden.';
			$lang_user['largesmspricewarning']	= 'Zum Senden dieser Power-SMS reicht Ihr aktuelles Guthaben von %%usercredits%% Credit(s) leider nicht aus. Bitte laden Sie Ihr Konto vor dem Senden der Power-SMS auf.';
			$lang_user['largesmsinfo']			= '<b>Hinweis:</b> Bitte beachten Sie, dass nicht alle Handytypen SMS mit &Uuml;berl&auml;nge unterst&uuml;tzen!';
			// admin
			$lang_admin['largesms']				= '&Uuml;berlange-SMS';
			$lang_admin['p_freesms']			= 'Free-SMS';
			$lang_admin['freeSMSanzeigen']		= 'Free-SMS anzeigen?';
			$lang_admin['largesmsday']			= 'Free-SMS/Tag?';
			$lang_admin['largesmsmonth']		= 'Free-SMS/Monat?';
			$lang_admin['largeSMSprice']		= 'Preis pro SMS';
			$lang_admin['largeSMSgw']			= 'Large-SMS Gateway';
			$lang_admin['freeSMSgw']			= 'Free-SMS Gateway';
			$lang_admin['largesmsgwvar']		= 'Gateway-Variablen';
			$lang_admin['largesmsgwoptvar']		= 'Optionale-Variablen';
			$lang_admin['largesmsgwfrom']		= 'Absender';
			$lang_admin['largesmsgwto']			= 'Empf&auml;nger';
			$lang_admin['largesmsgwmsg']		= 'Nachricht';
			$lang_admin['largesmsgwuser']		= 'Benutzer';
			$lang_admin['largesmsgwpw']			= 'Passwort';
			$lang_admin['largesmsgwuserid']		= 'User-ID';
			$lang_admin['largesmsgwemail']		= 'User E-Mail';
			$lang_admin['smsreloadsperre']		= 'Reloadsperre';
			$lang_admin['smsreloadsperreh']		= 'Stunde(n)';
		}
		else
		{
			$lang_user['sendlargesms']			= 'send power-SMS';
			$lang_user['sendfreesms']			= 'send free-SMS';
			$lang_user['sendlargesmsready']		= 'sent power-SMS';
			$lang_user['freesms_heute']			= 'free-sms today';
			$lang_user['freesms_monat']			= 'month';
			$lang_user['freesmswarning_rs']		= 'They have sent recently, already has a free SMS. To prevent abuse, you can send within %%stunden%% hour (s) only one free SMS. Please try again later.';
			$lang_user['powersmsakt_preis']		= 'current costs';
			$lang_user['sendlargesmsok']		= 'Their power-SMS was successfully sent.';
			$lang_user['sendfreesmsok']			= 'Their free-SMS was successfully sent.';
			$lang_user['sendfreesmsokfailed']	= 'Your Free SMS could not be sent. A temporary internal error has occurred. Please try again later.';
			$lang_user['sendlargesmsokfailed']	= 'Their power-SMS could not be sent. Possible cause is too small a credit or a temporary internal error. Please try again later.';
			$lang_user['changeAbs']				= 'Change SMS Sender?';
			$lang_user['largesmsanz']			= 'Number of SMS:';
			$lang_user['send2sms']				= 'send sms';
			$lang_user['freesmswarning_month']	= 'Unfortunately, your free SMS quota is used up for this month already. The next month, you can again send free SMS.';
			$lang_user['freesmswarning_day']	= 'Unfortunately, your free SMS quota is used up for today. Tomorrow you can again send free SMS.';
			$lang_user['largesmspricewarning']	= 'To transmit this power extends SMS in your balance of %%usercredits%% Credit(s) does not, unfortunately. Please download to your account before sending the SMS Power.';
			$lang_user['largesmsinfo']			= '<b>Note:</ b> Please note that not all mobile phones support SMS over length!';
			$lang_admin['largesms']				= 'large sms';
			$lang_admin['p_freesms']			= 'free-sms';
			$lang_admin['freeSMSanzeigen']		= 'view free-sms?';
			$lang_admin['largesmsday']			= 'free-sms/day?';
			$lang_admin['largesmsmonth']		= 'free-sms/month?';
			$lang_admin['largeSMSprice']		= 'Price per SMS';
			$lang_admin['largeSMSgw']			= 'large-sms Gateway';
			$lang_admin['freeSMSgw']			= 'Free-SMS Gateway';
			$lang_admin['largesmsgwvar']		= 'gateway-variable';
			$lang_admin['largesmsgwoptvar']		= 'optional-variable';
			$lang_admin['largesmsgwfrom']		= 'sender';
			$lang_admin['largesmsgwto']			= 'receivers';
			$lang_admin['largesmsgwmsg']		= 'message';
			$lang_admin['largesmsgwuser']		= 'user';
			$lang_admin['largesmsgwpw']			= 'password';
			$lang_admin['largesmsgwuserid']		= 'user-ID';
			$lang_admin['largesmsgwemail']		= 'user mail';
			$lang_admin['smsreloadsperre']		= 'reload lock';
			$lang_admin['smsreloadsperreh']		= 'hour(s)';
		}
	}

	function Install()
	{
		global $db;
		
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}largesms (`rgw` VARCHAR(5) NOT NULL, `user` VARCHAR(255) NOT NULL, `pw` VARCHAR(255) NOT NULL, `gateway` VARCHAR(1500) NOT NULL, largesmsprice INT(5) NOT NULL)');
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}largesmssend (`id` INT(11) NOT NULL AUTO_INCREMENT, `userid` INT(11) NOT NULL, `from` VARCHAR(64) NOT NULL, `to` VARCHAR(64) NOT NULL, `text` VARCHAR(1550) NOT NULL, `datum` INT(11) NOT NULL, `deleted` INT(4) NOT NULL DEFAULT 0, PRIMARY KEY (`id`))');
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}freesms (`status` INT(4) NOT NULL, `gateway` VARCHAR(1500) NOT NULL, `user` VARCHAR(255) NOT NULL, `pw` VARCHAR(255) NOT NULL, `rgw` VARCHAR(5), `signatur` VARCHAR(255), `reloadsperre` INT(11) NOT NULL)');
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}powersms_status (status INT(4) NOT NULL, status_month INT(4) NOT NULL, status_day INT(4) NOT NULL)');
		
		$res = $db->Query('SELECT status FROM {pre}powersms_status Limit 1');
		$row = $res->FetchArray(); 
		$status = $row['status'];
		$res->Free();
		
		if($status != 1)
		{
			$db->Query("INSERT INTO {pre}largesms(rgw, user, pw, gateway, largesmsprice) VALUES('OK', '', '', 'http://www.smskaufen.com/sms/gateway/sms.php?id=%%user%%&pw=%%pass%%&text=%%msg%%&empfaenger=%%to%%&absender=%%from%%&type=4', 7)");
			$db->Query("INSERT INTO {pre}freesms(status, rgw, user, pw, gateway, reloadsperre) VALUES(0, 'OK', '', '', 'http://www.smskaufen.com/sms/gateway/sms.php?id=%%user%%&pw=%%pass%%&text=%%msg%%&empfaenger=%%to%%&absender=%%from%%&type=13', 0)");
			$db->Query("INSERT INTO {pre}powersms_status(status, status_month, status_day) VALUES(1, 0, 0)");
		}
		
		PutLog('Power-SMS - Plugin was successfull installed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		global $db;
		
		$db->Query('DROP TABLE {pre}largesms');
		$db->Query('DROP TABLE {pre}largesmssend');
		$db->Query('DROP TABLE {pre}freesms');
		$db->Query('DROP TABLE {pre}powersms_status');
		// alle key-eintr�ge l�schen
		$db->Query('DELETE FROM {pre}userprefs WHERE `key` = ?', 'freeSMSday');
		$db->Query('DELETE FROM {pre}userprefs WHERE `key` = ?', 'freeSMSmonth');
		$db->Query('DELETE FROM {pre}userprefs WHERE `key` = ?', 'freeSMSreloadsperre');
				
		PutLog('PowerSMS - Plugin was successfull removed!', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	function AdminHandler()
	{
		global $tpl, $plugins, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'largeSMSgateways';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['largesms'],
				'link'		=> $this->_adminLink() . '&action=largeSMSgateways&',
				'active'	=> $_REQUEST['action'] == 'largeSMSgateways',
				'icon'		=> '../plugins/templates/images/modlargesms32.png'),
			1 => array(
				'title'		=> $lang_admin['p_freesms'],
				'link'		=> $this->_adminLink() . '&action=freeSMSgateways&',
				'active'	=> $_REQUEST['action'] == 'freeSMSgateways',
				'icon'		=> '../plugins/templates/images/modlargesms32.png')
			);

		$tpl->assign('tabs', $tabs);
		
		if($_REQUEST['action'] == 'largeSMSgateways')
			$this->_gatewaysAdminPage();
		if($_REQUEST['action'] == 'freeSMSgateways')
			$this->_gatewaysFreeSMSAdminPage();
	}

	function _gatewaysAdminPage()
	{
		global $tpl, $db, $bm_prefs;
		
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}largesms SET rgw=?, `user`=?, `pw`=?, gateway=?, largesmsprice=?',
				$_REQUEST['rueckgabewert'],
				$_REQUEST['user'],
				$_REQUEST['pass'],
				$_REQUEST['gw'],
				$_REQUEST['largeSMSprice']);
				
			header('Location: plugin.page.php?plugin=powerSMS&action=largeSMSgateways&sid=' . session_id());
			exit();
		}
		
		$res = $db->Query('SELECT * FROM {pre}largesms');
		$row = $res->FetchArray(); 
		$rgw = $row['rgw'];
		$preis = $row['largesmsprice'];
		$user = $row['user'];
		$pw = $row['pw'];
		$gw = $row['gateway'];
		$res->Free();

		$tpl->assign('rueckgabewert', $rgw);
		$tpl->assign('largeSMSprice', $preis);
		$tpl->assign('user', $user);
		$tpl->assign('pass', $pw);
		$tpl->assign('gateway', $gw);
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('largesms.admin.gateways.tpl'));
		return(true);
	}
	
	function _gatewaysFreeSMSAdminPage()
	{
		global $tpl, $db, $bm_prefs;
		
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}freesms SET status=?, rgw=?, `user`=?, `pw`=?, gateway=?, signatur=?, reloadsperre=?',
				isset($_REQUEST['status']),
				$_REQUEST['rueckgabewert'],
				$_REQUEST['user'],
				$_REQUEST['pass'],
				$_REQUEST['gw'],
				$_REQUEST['signatur'],
				$_REQUEST['reloadsperre']);
				
			header('Location: plugin.page.php?plugin=powerSMS&action=freeSMSgateways&sid=' . session_id());
			exit();
		}
		
		$res = $db->Query('SELECT * FROM {pre}freesms');
		$row = $res->FetchArray();
		$status = $row['status'];
		$rgw = $row['rgw'];
		$user = $row['user'];
		$pw = $row['pw'];
		$gw = $row['gateway'];
		$signatur = $row['signatur'];
		$reloadsperre = $row['reloadsperre'];
		$res->Free();

		$tpl->assign('reloadsperre', $reloadsperre);
		$tpl->assign('signatur', $signatur);
		$tpl->assign('status', $status);
		$tpl->assign('rueckgabewert', $rgw);
		$tpl->assign('user', $user);
		$tpl->assign('pass', $pw);
		$tpl->assign('gateway', $gw);
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('freesms.admin.gateways.tpl'));
		return(true);
	}
	
	function BeforeDisplayTemplate($resourceName, &$tpl)
	{
		global $db, $tpl, $groupRow, $userRow;
		
		$res = $db->Query('SELECT status FROM {pre}freesms');
		$row = $res->FetchArray();
		$status = $row['status'];
		$res->Free();

		// sms sidebar
		if ($resourceName == 'li/index.tpl' && preg_match('/sms.php/',$_SERVER['REQUEST_URI']) && $this->GetGroupOptionValue('largeSMS', $groupRow['id']))
		{
			if(($groupRow['smsvalidation'] == 'yes' && $userRow['sms_validation'] > 0) || $groupRow['smsvalidation'] == 'no')
			{
				$tpl->assign('freesms_status', $status);
				$tpl->assign('pageMenuFile', $this->_templatePath('largesms.sidebar.tpl'));
			}
		}
		
		// gesendete sms
		if ($resourceName == 'li/index.tpl' && preg_match('/sms.php\?action=outbox/',$_SERVER['REQUEST_URI']) && $this->GetGroupOptionValue('largeSMS', $groupRow['id']))
		{
			if(($groupRow['smsvalidation'] == 'yes' && $userRow['sms_validation'] > 0) || $groupRow['smsvalidation'] == 'no')
			{
				$tpl->assign('freesms_status', $status);
				$tpl->assign('pageMenuFile', $this->_templatePath('largesms.sidebar.tpl'));
				$tpl->assign('pageContent', $this->_templatePath('largesms.outbox.tpl'));
			}
		}
	}
	
	function OnCron()
	{
		global $db;
		
		$res = $db->Query('SELECT status_month, status_day FROM {pre}powersms_status');
		$row = $res->FetchArray();
		$status_month = $row['status_month'];
		$status_day = $row['status_day'];
		$res->Free();
		
		$aktTag = date("j");
		$aktZeit = date("G");
		
		// monatliches free-sms kontingent auf 0 setzen
		if($aktTag == 1)
		{
			if($status_month == 0 && $aktZeit == 0)
			{
				$db->Query('UPDATE {pre}powersms_status SET status_month = ? WHERE status_month = ?', 1, 0);
				
				$sql = $db->Query('SELECT value FROM {pre}userprefs WHERE `key` = ?', 'freeSMSmonth');
				while($row = $sql->FetchArray(MYSQLI_ASSOC))
				{
					$db->Query('UPDATE {pre}userprefs SET value = ? WHERE `key` = ? ', 0, 'freeSMSmonth');
				}
				$sql->Free();
			}
			if($status_month == 1 && $aktZeit == 3)
			{
				$db->Query('UPDATE {pre}powersms_status SET status_month = ? WHERE status_month = ?', 0, 1);
			}
		}
		// t�gliches free-sms kontingent auf 0 setzen
		if($status_day == 0 && $aktZeit == 0)
		{
			$db->Query('UPDATE {pre}powersms_status SET status_day = ? WHERE status_day = ?', 1, 0);
			
			$sql = $db->Query('SELECT value FROM {pre}userprefs WHERE `key` = ?', 'freeSMSday');
			while($row = $sql->FetchArray(MYSQLI_ASSOC))
			{
				$db->Query('UPDATE {pre}userprefs SET value = ? WHERE `key` = ? ', 0, 'freeSMSday');
			}
			$sql->Free();
		}
		if($status_day == 1 && $aktZeit == 3)
		{
			$db->Query('UPDATE {pre}powersms_status SET status_day = ? WHERE status_day = ?', 0, 1);
		}
	}
	
	function GetMaxSMSChars()
	{
		global $groupRow;
		
		return(1530 - strlen($groupRow['sms_sig']));
	}
	
	function GetSigZei()
	{
		global $groupRow;
		
		return(strlen($groupRow['sms_sig']));
	}
			
	// credit-check
	function GetCredits()
	{
		global $db, $thisUser;
		
		$res = $db->Query('SELECT largesmsprice FROM {pre}largesms');
		$row = $res->FetchArray(); 
		$LargeSmsPrice = $row['largesmsprice'];
		$res->Free();

		// holt user-credits und user-monats-credits
		$credits = $thisUser->GetBalance();
		
		// pr�ft ob genug credits vorhanden sind
		if($credits >= $LargeSmsPrice)
			return true; 
		return false;
	}
	
	// free-sms senden
	function sendFreeSMS($to, $text)
	{
		global $db, $userRow;
		
		$res = $db->Query('SELECT user, pw, gateway FROM {pre}freesms');
		$row = $res->FetchArray(); 
		$user = $row['user'];
		$pw = $row['pw'];
		$gw = $row['gateway'];
		$res->Free();
		
		if(!class_exists('BMHTTP'))
			include(B1GMAIL_DIR . 'serverlib/http.class.php');
			
		$gw = str_replace("%%user%%", $user, $gw);
		$gw = str_replace("%%pass%%", $pw, $gw);
		$gw = str_replace("%%to%%", $to, $gw);
		$gw = str_replace("%%msg%%", urlencode($text), $gw);
		$gw = str_replace("%%userid%%", $userRow['id'], $gw);
		$gw = str_replace("%%useremail%%", urlencode($userRow['email']), $gw);
		
		$http = _new('BMHTTP', array($gw));
		$result = $http->DownloadToString();
	
		// die ersten 3 zeichen rausschneiden
		$result = substr($result, 0, 3);
	
		return $result;
	}
	
	// large-sms senden
	function sendLargeSMS($from, $to, $text, $userid)
	{
		global $db, $userRow;
		
		$res = $db->Query('SELECT rgw, user, pw, gateway FROM {pre}largesms');
		$row = $res->FetchArray(); 
		$rgw = $row['rgw'];
		$user = $row['user'];
		$pw = $row['pw'];
		$gw = $row['gateway'];
		$res->Free();
	
		if(!class_exists('BMHTTP'))
			include(B1GMAIL_DIR . 'serverlib/http.class.php');

		$gw = str_replace("%%user%%", $user, $gw);
		$gw = str_replace("%%pass%%", $pw, $gw);
		$gw = str_replace("%%from%%", $from, $gw);
		$gw = str_replace("%%to%%", $to, $gw);
		$gw = str_replace("%%msg%%", urlencode($text), $gw);
		$gw = str_replace("%%userid%%", $userid, $gw);
		$gw = str_replace("%%useremail%%", urlencode($userRow['email']), $gw);

		// werte�bergabe an den gateway						
		$http = _new('BMHTTP', array($gw));
		
		// r�ckgabewert abfangen
		$result = $http->DownloadToString();

		// die ersten 3 zeichen rausschneiden
		$result = substr($result, 0, 3);
		
		if($result == $rgw)
		{
			PutLog('User ('.$userid.') sent Power-SMS from <'.$from.'> to <'.$to.'> (type: Power-SMS; status: OK; charged: OK)', PRIO_NOTE, __FILE__, __LINE__);
			return true;
		}
		else
		{
			PutLog('Failed to send Power-SMS by User ('.$userid.') from <'.$from.'> to <'.$to.'> (type: Power-SMS; status: FAILED; charged: NO)', PRIO_WARNING, __FILE__, __LINE__);
			return false;
		}
	}
	
	function payLargeSMS($payAnz)
	{
		global $thisUser, $db;
		
		$res = $db->Query('SELECT largesmsprice FROM {pre}largesms');
		$row = $res->FetchArray(); 
		$LargeSmsPrice = $row['largesmsprice'];
		$res->Free();
		
		// sms-anzahl * preis pro sms
		$payAnz = $LargeSmsPrice * $payAnz;
		
		$userObject = _new('BMUser', array($thisUser->_id));
		$userObject->Debit($payAnz*(-1));
	}
	
	function getLargeSMSanz($laenge)
	{
		global $groupRow;
		
		$SigZei = strlen($groupRow['sms_sig']);

		$anz = 0;
		
		if($laenge > 0 && $laenge <= 160-$SigZei)
			$anz = 1;
		else if($laenge > 160-$SigZei && $laenge <= 306-$SigZei)
			$anz = 2;
		else if($laenge > 306-$SigZei && $laenge <= 459-$SigZei)
			$anz = 3;
		else if($laenge > 459-$SigZei && $laenge <= 612-$SigZei)
			$anz = 4;
		else if($laenge > 612-$SigZei && $laenge <= 765-$SigZei)
			$anz = 5;
		else if($laenge > 765-$SigZei && $laenge <= 918-$SigZei)
			$anz = 6;
		else if($laenge > 918-$SigZei && $laenge <= 1071-$SigZei)
			$anz = 7;
		else if($laenge > 1071-$SigZei && $laenge <= 1224-$SigZei)
			$anz = 8;
		else if($laenge > 1224-$SigZei && $laenge <= 1377-$SigZei)
			$anz = 9;
		else if($laenge > 1377-$SigZei && $laenge <= 1530-$SigZei)
			$anz = 10;
			
		$payAnz = $anz;
					
		return $payAnz;
	}
	
	// l�scht die power-sms eines users, welcher aus dem system gel�scht wird
	function OnDeleteUser($id)
	{
		global $db;
		
		$res = $db->Query('SELECT * FROM {pre}largesmssend WHERE userid = ?', $id);
		while($row = $res->FetchArray(MYSQLI_ASSOC))
		{
			$db->Query("DELETE FROM {pre}largesmssend WHERE userid = ?", $id);
		}
		$res->Free();
	}
	
	// erfolgreich versendete sms speichern
	function saveSMS($from, $to, $text, $userid)
	{
		global $db;

		$timestamp = time();
		$db->Query('INSERT INTO {pre}largesmssend (`userid`, `from`, `to`, `text`, `datum`) VALUES (?, ?, ?, ?, ?)', $userid, $from, $to, $text, $timestamp);
	}
	
	function FileHandler($file, $action)
	{
		global $db, $tpl, $userRow, $thisUser, $lang_user, $bm_prefs, $groupRow;

		// sms-outbox
		if($file == 'sms.php' && $action == 'outbox')
		{
			if(!$this->GetGroupOptionValue('largeSMS', $groupRow['id']))
				return;
			
			// sms durch user als gel�scht markieren
			if($_REQUEST['do'] == 'deletelargesms')
			{
				$db->Query('UPDATE {pre}largesmssend SET `deleted` = ? WHERE `id`= ? AND userid = ?', 1, $_REQUEST['id'], $thisUser->_id);
			}

			// sms ans tpl �bergeben
			$largesmsoutbox = array();
			
			// sms-daten auslesen + sortieren + max. 15 ausgeben sowie als gel�scht markierte �berspringen
			$res = $db->Query('SELECT * FROM {pre}largesmssend WHERE userid = ? AND deleted != ? ORDER BY datum DESC LIMIT 15', $thisUser->_id, 1);
			while($row = $res->FetchArray(MYSQLI_ASSOC))
			{
				$largesmsoutbox[$row['id']] = $row;
			}
			$res->Free();
			
			$tpl->assign('largesmsoutbox', $largesmsoutbox);
		}
		
		// free-sms
		if($file == 'sms.php' && $action == 'freesms')
		{
			$res = $db->Query('SELECT * FROM {pre}freesms');
			$row = $res->FetchArray(); 
			$signatur = $row['signatur'];
			$rgw = $row['rgw'];
			$status = $row['status'];
			$reloadsperre = $row['reloadsperre'];
			$res->Free();
			
			if(!$this->GetGroupOptionValue('largeSMS', $groupRow['id']) || !$status)
				return;
				
			$userObject = _new('BMUser', array($thisUser->_id));
			$UserFreeSMSday = $userObject->GetPref('freeSMSday');
			$UserFreeSMSmonth = $userObject->GetPref('freeSMSmonth');
			
			$UserReloadsperre = $userObject->GetPref('freeSMSreloadsperre');
			
			$GrpFreeSMSday = $this->GetGroupOptionValue('freeSMSday');
			$GrpFreeSMSmonth = $this->GetGroupOptionValue('freeSMSmonth');
			
			$aktTime = time();
			$lang_user['freesmswarning_rs'] = str_replace("%%stunden%%", $reloadsperre, $lang_user['freesmswarning_rs']);
			
			// reloadsperre pr�fen
			if($UserReloadsperre >= ($aktTime-($reloadsperre*3600)) && $reloadsperre != 0)
			{
				$freeCredits = false;
				$lang_user['freesmswarning'] = $lang_user['freesmswarning_rs'];
			}
			else
			{
				// pr�fen, ob user noch freie monats- bzw. tagescredits hat
				if($UserFreeSMSmonth == $GrpFreeSMSmonth)
				{
					$freeCredits = false;
					$lang_user['freesmswarning'] = $lang_user['freesmswarning_month'];
				}
				elseif($UserFreeSMSday == $GrpFreeSMSday)
				{
					$freeCredits = false;
					$lang_user['freesmswarning'] = $lang_user['freesmswarning_day'];
				}
				else
				{
					$freeCredits = true;
				}
			}
							
			if($signatur)
				$maxChars = 160 - strlen($signatur);
			else
				$maxChars = 160;
			
			$tpl->assign('pageContent', $this->_templatePath('freesms.compose.tpl'));
				
			if($_REQUEST['do'] == 'sendFreeSMS')
			{
				// signatur anh�ngen
				if($signatur)
					$text = $_REQUEST['FreeSmsText'].$signatur;
				else
					$text = $_REQUEST['FreeSmsText'];
				
				// pr�fen, ob handy-vorwahlen vorgegeben sind
				if($_REQUEST['to_pre'])
				{
					// vorgegebene vorwahl + eingegebene rufnummer
					$to = $_REQUEST['to_pre'].$_REQUEST['to_no'];
				}
				else
				{
					$to = $_REQUEST['to'];
				}
				
				$freeSMSresult = $this->sendFreeSMS($to, $text);
				
				if($freeSMSresult == $rgw)
				{
					// erfolgreich versendete SMS in sms-ausgang speichern
					$timestamp = time();
					$aktdate = date("nY");
					
					$db->Query("INSERT INTO {pre}smsend(user, monat, price, isSMS, `from`, `to`, text, statusid, date, deleted) VALUES(?,?,?,?,?,?,?,?,?,?)", $thisUser->_id, $aktdate, 0, 1, 'Free-SMS', $to, $text, -1, $timestamp, 0);
					
					// statistik updaten
					Add2Stat('sms');
					
					// free-tages-credits des users updaten
					$newDayAnz = $UserFreeSMSday + 1;
					$userObject->SetPref('freeSMSday', $newDayAnz);
					$newMonthAnz = $UserFreeSMSmonth + 1;
					$userObject->SetPref('freeSMSmonth', $newMonthAnz);
					
					// reloadsperre setzen
					$userObject->SetPref('freeSMSreloadsperre', $timestamp);
					
					PutLog('User ('.$thisUser->_id.') sent Free-SMS to <'.$to.'> (type: Free-SMS; status: OK)', PRIO_NOTE, __FILE__, __LINE__);
					
					$tpl->assign('title', 		$lang_user['sendfreesms']);
					$tpl->assign('msg', 		$lang_user['sendfreesmsok']);
					$tpl->assign('backLink', 	'sms.php?action=freesms&sid=' . session_id());
					$tpl->assign('pageContent', 'li/msg.tpl');
				}
				else
				{
					PutLog('Failed to send Free-SMS by User ('.$thisUser->_id.') to <'.$to.'> (type: Free-SMS; status: FAILED)', PRIO_WARNING, __FILE__, __LINE__);

					$tpl->assign('msg', $lang_user['sendfreesmsokfailed']);
					$tpl->assign('pageContent', 'li/error.tpl');
				}
			}

			$tpl->assign('GrpFreeSMSday', $GrpFreeSMSday);
			$tpl->assign('GrpFreeSMSmonth', $GrpFreeSMSmonth);
			$tpl->assign('UserFreeSMSday', $UserFreeSMSday);
			$tpl->assign('UserFreeSMSmonth', $UserFreeSMSmonth);
			$tpl->assign('freeCredits', $freeCredits);
			$tpl->assign('maxChars', $maxChars);
			$tpl->assign('activeTab', 		'sms');
			$tpl->assign('pageTitle', $lang_user['sendfreesms']);
			$tpl->assign('pageToolbarFile', 'li/sms.toolbar.tpl');
			$tpl->assign('accBalance', $thisUser->GetBalance());
			$tpl->display('li/index.tpl');
		}
		
		// large-sms-send
		if($file == 'sms.php' && $action == 'largesms')
		{
			if(!$this->GetGroupOptionValue('largeSMS', $groupRow['id']))
				return;

			$res = $db->Query('SELECT largesmsprice FROM {pre}largesms');
			$row = $res->FetchArray(); 
			$creditwert = $row['largesmsprice'];
			$res->Free();
			
			$usercredits = $thisUser->GetBalance();
			
			$lang_user['largesmspricewarning'] = str_replace("%%usercredits%%", $usercredits, $lang_user['largesmspricewarning']);

			$eigSMSabs = $groupRow['sms_ownfrom'];
			
			// page output
			$tpl->assign('largesmspricewarning', $lang_user['largesmspricewarning']);
			$tpl->assign('creditwert', $creditwert);
			$tpl->assign('eigSMSabs', $eigSMSabs);
			$tpl->assign('smsFrom', $userRow['mail2sms_nummer']);
			$tpl->assign('pageTitle', $lang_user['sendlargesms']);
			$tpl->assign('maxChars', $this->GetMaxSMSChars());
			$tpl->assign('SMSanz', 0);
			$tpl->assign('SigZei', $this->GetSigZei());
			$tpl->assign('activeTab', 		'sms');
			$tpl->assign('pageToolbarFile', 'li/sms.toolbar.tpl');
			$tpl->assign('accBalance', $thisUser->GetBalance());
			$tpl->assign('accCredits', $this->GetCredits());
			$tpl->assign('pageContent', $this->_templatePath('largesms.compose.tpl'));
			
			if($_REQUEST['do'] == 'sendLargeSMS')
			{
				$res = $db->Query('SELECT mail2sms_nummer FROM {pre}users WHERE `id`=?', $thisUser->_id);
				$row = $res->FetchArray(); 
				$from = $row['mail2sms_nummer'];
				$res->Free();
				
				if($eigSMSabs == 'no')
					$from = $groupRow['sms_from'];
				
				// pr�fen, ob handy-vorwahlen vorgegeben sind
				if($_REQUEST['to_pre'])
				{
					// vorgegebene vorwahl + eingegebene rufnummer
					$to = $_REQUEST['to_pre'].$_REQUEST['to_no'];
				}
				else
				{
					$to = $_REQUEST['to'];
				}

				// leerzeichen am anfang und am ende rausschneiden
				$text = trim($_REQUEST['LargeSmsText']);

				// textl�nge (ohne signatur) ermitteln und berechnen, wieviel sms berechnet werden m�ssen
				$laenge = strlen($text);
				$payAnz = $this->getLargeSMSanz($laenge);
				
				// sms-signatur anh�ngen
				if($groupRow['sms_sig'])
					$text = $text.$groupRow['sms_sig'];
			
				// sicherheitsabfrage, ob das guthaben zum versand der "real"-sms wirklich ausreicht
				if($thisUser->GetBalance() < ($creditwert*$payAnz) || $payAnz == 0)
				{
					$tpl->assign('msg', $lang_user['sendlargesmsokfailed']);
					$tpl->assign('pageContent', 'li/error.tpl');
					$tpl->display('li/index.tpl');
					exit();
				}
						
				// r�ckgabe vom sms-versand
				$result = $this->sendLargeSMS($from, $to, $text, $thisUser->_id);

				if($result)
				{
					// versendete SMS berechnen ($payAnz = anzahl der zu berechnenden sms)
					$pay = $this->payLargeSMS($payAnz);
					
					// erfolgreich versendete SMS speichern
					$savedSMS = $this->saveSMS($from, $to, $text, $thisUser->_id);
					
					// statistik updaten
					$zae = 1;
					while($zae <= $payAnz)
					{
						Add2Stat('sms');
						$zae++;
					}
					
					$tpl->assign('title', 		$lang_user['sendlargesms']);
					$tpl->assign('msg', 		$lang_user['sendlargesmsok']);
					$tpl->assign('backLink', 	'sms.php?action=largesms&sid=' . session_id());
					$tpl->assign('pageContent', 'li/msg.tpl');
				}
				else
				{
					$tpl->assign('msg', $lang_user['sendlargesmsokfailed']);
					$tpl->assign('pageContent', 'li/error.tpl');
				}
			}
			$tpl->display('li/index.tpl');
		}
	}
}

$plugins->registerPlugin('powerSMS');

?>
