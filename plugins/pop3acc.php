<?php
/*
 * Plugin pop3acc
 */
class pop3acc extends BMPlugin 
{
	/*
	 * Eigenschaften des Plugins
	 */
	function pop3acc()
	{
		$this->name					= 'POP3-Sammeldienste';
		$this->version				= '1.1.0';
		$this->designedfor			= '7.3.0';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'dotaachen';
		$this->mail					= 'b1g@dotaachen.net';
		$this->web 					= 'http://b1g.dotaachen.net';

		$this->update_url			= 'http://my.b1gmail.com/update_service/';
		$this->website				= 'http://my.b1gmail.com/details/74/';

		$this->admin_pages			= true;
		$this->admin_page_title		= 'POP3-Sammeldienste';
		$this->admin_page_icon		= "pop3acc_icon.png";
	}

	/*
	 *  Link  und Tabs im Adminbereich 
	 */
	function AdminHandler()
	{
		global $tpl, $lang_admin;

		// Plugin aufruf ohne Action
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'page1';

		// Tabs im Adminbereich
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['overview'],
				'link'		=> $this->_adminLink() . '&action=page1&',
				'active'	=> $_REQUEST['action'] == 'page1',
				'icon'		=> '../plugins/templates/images/pop3acc_logo.png'
			),
			1 => array(
				'title'		=> $lang_admin['all'],
				'link'		=> $this->_adminLink() . '&action=page2&',
				'active'	=> $_REQUEST['action'] == 'page2',
				'icon'		=> '../plugins/templates/images/pop3acc_logo.png'
			),
			2 => array(
				'title'		=> $lang_admin['pop3acc_refresh'],
				'link'		=> $this->_adminLink() . '&action=page3&',
				'active'	=> $_REQUEST['action'] == 'page3',
				'icon'		=> './templates/images/ico_prefs_receiving.png'
			),
		);
		$tpl->assign('tabs', $tabs);

		// Plugin aufruf mit Action 
		if($_REQUEST['action'] == 'page1') {
			$tpl->assign('page', $this->_templatePath('pop3acc1.pref.tpl'));
			$this->_Page1();
		} else if($_REQUEST['action'] == 'page2') {
			$tpl->assign('page', $this->_templatePath('pop3acc2.pref.tpl'));
			$this->_Page2();
		} else if($_REQUEST['action'] == 'page3') {
			$tpl->assign('page', $this->_templatePath('pop3acc3.pref.tpl'));
			$this->_Page3();
		}
	}

	/*
	 *  Sprach variablen
	 */
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		global $lang_user;

		$lang_admin['pop3acc_name']				= 'POP3-Sammeldienste';
		$lang_admin['pop3acc_text']				= 'Zeigt eine kleine &Uuml;bersicht, welcher Benutzer ein POP3 Sammeldienst besitzt.';
		$lang_admin['pop3acc_refresh']			= 'Abrufen';
		$lang_admin['pop3acc_starttext']		= 'Hier k&ouml;nnen Sie E-Mails von allen externen POP3-Accounts einsammeln.';
		
		$lang_admin['lastfetch']				= $lang_user['lastfetch'];
		$lang_admin['extpop3']					= $lang_user['extpop3'];
		$lang_admin['keepmails']				= $lang_user['keepmails'];
	}

	/*
	 * installation routine
	 */	
	function Install()
	{
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return true;
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return true;
	}

	/*
	 *  Abfragen aller pop3
	 */
	function _Page1()
	{
		global $tpl, $db;

		// delete
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'delete')
		{
			$db->Query('DELETE FROM {pre}pop3 WHERE id=?',
				(int) $_REQUEST['id']);

			if($db->AffectedRows() == 1)
			{
				$db->Query('DELETE FROM {pre}uidindex WHERE pop3=?',
					(int) $_REQUEST['id']);
			}
		}
		
		// sort options
		$sortBy = isset($_REQUEST['sortBy'])
					? $_REQUEST['sortBy']
					: 'id';
		$sortOrder = isset($_REQUEST['sortOrder'])
						? strtolower($_REQUEST['sortOrder'])
						: 'asc';

		// pop3acc und pop3user abfragen und auf Page1 ausgeben
		$pop3acc =  $pop3user = array();
		$res = $db->Query('SELECT id, email, gruppe FROM {pre}users ORDER by ' . $sortBy . ' ' . $sortOrder);
		while($row = $res->FetchArray())
		{
			$res2 = $db->Query('SELECT * FROM {pre}pop3 WHERE user=?', 
				(int) $row['id']);

			if($res2->RowCount() >= 1)
			{
				while($row2 = $res2->FetchArray())
				{
					$pop3acc[$row2['id']] = array(
						'user_id'	=> $row['id'],
						'id'		=> $row2['id'],
						'user'		=> $row2['p_user'],
						'host'		=> $row2['p_host'],
						'port'		=> $row2['p_port'],
						'ssl'		=> $row2['p_ssl'],
						'last'		=> $row2['last_fetch'],
						'keep'		=> $row2['p_keep']
					);
				}

				$res3 = $db->Query('SELECT ownpop3 FROM {pre}gruppen WHERE id=?', 
					(int) $row['gruppe']);
				$gruppe = $res3->FetchArray();
				$res3->Free();

				$pop3user[$row['id']] = array(
					'id'		=> $row['id'],
					'email'		=> $row['email'],
					'count'		=> $res2->RowCount(),
					'gruppe_p'	=> $gruppe[ownpop3],		
				);
			}
			$res2->Free();
		}
		$res->Free();

		$tpl->assign('sortBy', $sortBy);
		$tpl->assign('sortOrder', $sortOrder);

		$tpl->assign('pop3acc', $pop3acc);
		$tpl->assign('pop3user', $pop3user);
	}

	/*
	 *  Abfragen aller pop3
	 */
	function _Page2()
	{
		global $tpl, $db;
		// pop3acc array
		$pop3acc = array();

		// sort options
		$sortBy = isset($_REQUEST['sortBy'])
					? $_REQUEST['sortBy']
					: 'email';
		$sortOrder = isset($_REQUEST['sortOrder'])
						? strtolower($_REQUEST['sortOrder'])
						: 'asc';

		$res = $db->Query('SELECT id, email FROM {pre}users ORDER by  ' . $sortBy . ' ' . $sortOrder);
		while($row = $res->FetchArray())
		{
			$res2 = $db->Query('SELECT * FROM {pre}pop3 WHERE user=?', 
			$row['id']);

			if($res2->RowCount() >= 1)
			{
				while($row2 = $res2->FetchArray())
				{
					$pop3acc[$row2['id']] = array(
						'id'		=> $row['id'],
						'email'		=> $row['email'],
						'pop3_id'	=> $row2['id'],
						'host'		=> $row2['p_host'],
						'port'		=> $row2['p_port'],
						'ssl'		=> $row2['p_ssl'],
						'last'		=> $row2['last_fetch'],
						'keep'		=> $row2['p_keep']
					);
				}
			}
			$res2->Free();
		}
		$res->Free();

		$tpl->assign('sortBy', $sortBy);
		$tpl->assign('sortOrder', $sortOrder);

		$tpl->assign('pop3acc', $pop3acc);
	}

	/*
	 * Abfragen der pop3accs
	 */	
	function _Page3()
	{
		global $tpl;
		$tpl->assign('start', isset($_REQUEST['start']));
	}
}
/*
 * register plugin
 */
$plugins->registerPlugin('pop3acc');
?>