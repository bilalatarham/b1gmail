<?php
class proflatsmallmenu extends BMPlugin
{
	function proflatsmallmenu()
	{
		
		$this->name        = 'Verkleinerung des proflat-Menüs';
		$this->author      = 'Martin Buchalik';
		$this->web         = 'http://martin-buchalik.de';
		$this->mail        = 'support@martin-buchalik.de';
		$this->version     = '1.0.1';
		$this->designedfor = '7.3.0';
		$this->type        = BMPLUGIN_DEFAULT;
		
		$this->admin_pages = false;
	}
	
	
	/* ===== Installation ===== */
	
	function Install()
	{
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	
	/* ===== Uninstall ===== */
	
	function Uninstall()
	{
		global $db;
		$db->Query('DELETE FROM {pre}userprefs WHERE `key`=?', 'proflat_smallmenu');
		
		PutLog('Plugin "' . $this->name . ' - ' . $this->version . '" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return (true);
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if ($lang == 'deutsch') {
			
			$lang_user['proflat_smallmenu']         = 'Hauptmenü';
			$lang_user['proflat_smallmenu_no'] = 'Einträge untereinander';
			$lang_user['proflat_smallmenu_yes'] = 'Einträge nebeneinander';
			
			
			
		} else {
			$lang_user['proflat_smallmenu']         = 'Main menu';
			$lang_user['proflat_smallmenu_no'] = 'Entries below the others';
			$lang_user['proflat_smallmenu_yes'] = 'Entries next to the others';
			
		}
		
	}
	
	/* ===== change settings when the proflat_userprefs form is submitted ===== */
	function FileHandler($file, $action)
	{
		global $thisUser;
		if ($file == 'prefs.php' && $action == 'userprefs_proflat' && isset($_REQUEST['do'])) {			
			if ($_REQUEST['do'] == 'save' && isset($_POST['reset']) && $_POST['reset'] == 'yes') {
				$thisUser->DeletePref('proflat_smallmenu');
			}
			elseif ($_REQUEST['do'] == 'save' && isset($_POST['smallmenu'])) {
				if($_POST['smallmenu'] == "no")
					$smallmenu = "no";
				else
					$smallmenu = "yes";
					
				$thisUser->setPref('proflat_smallmenu', $smallmenu);
			}
		}
	}
	
	function BeforeDisplayTemplate($resourceName, &$tpl)
	{
		global $thisUser;
		if(!isset($thisUser))
			return;
		
		//by default, the small menu is activated
		if($thisUser->getPref('proflat_smallmenu') != "no")
				$smallmenu = true;
			else
				$smallmenu = false;
				
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'userprefs_proflat') {
			$tpl->addCSSFile("li", "./plugins/css/proflat_smallmenu.settings.css");
			$tpl->registerHook("userprefs.proflat.tpl:afterTable", $this->_templatePath('../../plugins/templates/proflat_smallmenu.tpl'));
		}
		
		$tpl->assign('proflat_smallmenu', $smallmenu);
		
		if($smallmenu)
			$tpl->addCSSFile("li", "./plugins/css/proflat_smallmenu.css");
	}
}

/* ===== register plugin ===== */
$plugins->registerPlugin('proflatsmallmenu');
?>