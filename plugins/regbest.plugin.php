<?php 
/*
 * 
 * (c) Smart-Mail.de
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 * $Id: regbest.plugin.php,v 2.4 2013/09/22 13:30:00 Informant $
 *
 */

/**
 * Registrierungsbestätigung für alternative E-Mail-Adresse
 *
 */
class RegBestAltMailPlugin extends BMPlugin 
{
	function RegBestAltMailPlugin()
	{
		global $lang_user;
		
		$this->type				= BMPLUGIN_DEFAULT;
		$this->name				= 'Reg-Best-Alt-Mail';
		$this->author			= 'Informant';
		$this->version			= '2.4';
		$this->update_url 		= 'http://my.b1gmail.com/update_service/';
		
		// admin pages
		$this->admin_pages		= true;
		$this->admin_page_title	= 'Reg-Best-Alt-Mail';
		$this->admin_page_icon	= "mod_regbestaltmail_16.png";
	}
	
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			$lang_admin['vorschau']				= 'E-Mail-Vorschau';
			$lang_admin['clickhereVar']			= 'hier klicken';
			$lang_admin['back']					= 'zur&uuml;ck';
			$lang_admin['ueb1']					= 'Vorschau der Best&auml;tigungsmail f&uuml;r die alternative E-Mail-Adresse';
			$lang_admin['ueb2']					= 'Hier k&ouml;nnen Sie die Registrierungsbest&auml;tigung f&uuml;r die alternative E-Mail-Adresse des Kunden aktivieren/deaktivieren und entsprechende Konfigurationen f&uuml;r die Best&auml;tigungsmail vornehmen!';
			$lang_admin['regakt']				= 'Regbest. aktivieren?';
			$lang_admin['betreff']				= 'Best&auml;tigungsmail-Betreff';
			$lang_admin['absender']				= 'Absender-Adresse';
			$lang_admin['anbieter']				= 'Anbieter-Name';
			$lang_admin['mailtext']				= 'Alternativen E-Mail-Text ab&auml;ndern?';
			$lang_admin['variablen']			= 'Hierzu stehen Ihnen folgende Variablen zur Verf&uuml;gung';
			$lang_custom['regbestaltmailTXT']	= 'Sehr geehrte Damen und Herren,' . "\n\n"
												. 'vielen Dank f&uuml;r die Registrierung bei %%pt%%' . "\n"
												. 'Ihre Account wurde soeben freigeschaltet.' . "\n"
												. 'Sie k&ouml;nnen sich auf http://www.%%domain%% direkt in Ihren Account einloggen!' . "\n"
												. '-------------------------------------------------------' . "\n"
												. 'Ihre Daten lauten:' . "\n"
												. 'E-Mail-Adresse: %%usermail%%' . "\n"
												. 'Passwort: [von Ihnen vergeben]' . "\n"
												. '-------------------------------------------------------' . "\n"
												. 'Bei Fragen stehen wir Ihnen gern zur Verf&uuml;gung.' . "\n\n"
												. 'Mit freundlichen Gr&uuml;&szlig;en' . "\n"
												. 'Ihr %%pt%% - Service-Team' . "\n\n\n"
												. '(Diese E-Mail wurde automatisch erstellt!)';
		}
		else
		{
			$lang_admin['vorschau']				= 'E-Mail-Preview';
			$lang_admin['clickhereVar']			= 'click here';
			$lang_admin['back']					= 'back';
			$lang_admin['ueb1']					= 'Preview the confirmation-email for the alternative e-mail-address';
			$lang_admin['ueb2']					= 'Here you can do the registration confirmation for the alternate e-mail-address of the customer to enable/disable and corresponding configurations for the confirmation e-mail settings!';
			$lang_admin['regakt']				= 'Regbest. aktivate?';
			$lang_admin['betreff']				= 'Confirmation mail-subject';
			$lang_admin['absender']				= 'Sender-Address';
			$lang_admin['anbieter']				= 'Providername';
			$lang_admin['mailtext']				= 'Change the alternative e-mail-text?';
			$lang_admin['variablen']			= 'There are the following variables available';
		}
	}

	function Install()
	{
		global $db, $bm_prefs, $currentLanguage;

		// neue tabelle für inhalte anlegen
		$db->Query('Create TABLE IF NOT EXISTS {pre}regbest(absender VARCHAR(100), betreff VARCHAR(255), pt VARCHAR(250))');
		// bestätigung gesendet (ja/nein)
		$db->Query('ALTER TABLE {pre}users ADD regbestgesendet tinyint(4) NOT NULL DEFAULT 0');
		// aktivierung reg-bestätigung (ja/nein)
		$db->Query('ALTER TABLE {pre}prefs ADD regbestaktiviert tinyint(4) NOT NULL DEFAULT 1');
		// den usern eine spalte "mailtext" mit leerem inhalt hinzufügen
		$db->Query('ALTER TABLE {pre}users ADD mailtext VARCHAR(1500) NOT NULL');

		// projekt-titel
		$pt = $bm_prefs['titel'];
		// mail-betreff
		$betreff = "Ihre Registrierung bei $pt!";
		// system-absender
		$mailabs = $bm_prefs['passmail_abs'];

		$db->Query("INSERT INTO {pre}regbest (absender, betreff, pt) VALUES (?, ?, ?)", $mailabs, $betreff, $pt);
		PutLog('Reg-Best-Alt-Mail was successfull installed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	function Uninstall()
	{
		global $db;
		
		// einträge löschen
		$db->Query('ALTER TABLE {pre}users DROP regbestgesendet');
		$db->Query('ALTER TABLE {pre}prefs DROP regbestaktiviert');
		$db->Query('ALTER TABLE {pre}users DROP mailtext');
		$db->Query('DROP TABLE {pre}regbest');
		PutLog('Reg-Best-Alt-Mail was successfull removed!',
					PRIO_PLUGIN,
					__FILE__,
					__LINE__);
		return(true);
	}
	
	
	/**
	 * admin handler
	 */
	function AdminHandler()
	{
		global $tpl, $plugins, $lang_admin;
		
		if(!isset($_REQUEST['action']))
			$_REQUEST['action'] = 'prefs';
		
		$tabs = array(
			0 => array(
				'title'		=> $lang_admin['prefs'],
				'link'      => $this->_adminLink() . '&action=prefs&',
				'active'	=> $_REQUEST['action'] == 'prefs',
				'icon'		=> '../plugins/templates/images/mod_regbestaltmail_32.png'
			),
			1 => array(
				'title'		=> $lang_admin['vorschau'],
				'link'      => $this->_adminLink() . '&action=prefs2&',
				'active'	=> $_REQUEST['action'] == 'prefs2',
				'icon'		=> '../plugins/templates/images/mod_regbestaltmail_32.png'
			)
		);

		$tpl->assign('tabs', $tabs);
		
		if($_REQUEST['action'] == 'prefs')
			$this->_prefsPage();
		elseif($_REQUEST['action'] == 'prefs2')
			$this->_prefsVorschau();
	}
	
	function _prefsPage()
	{
		global $db, $tpl;

		// speichern
		if(isset($_REQUEST['do']) && $_REQUEST['do'] == 'save')
		{
			$db->Query('UPDATE {pre}prefs SET regbestaktiviert=?',
				isset($_REQUEST['regbestaktiv']) ? 1 : 0
				);
			$db->Query('UPDATE {pre}regbest SET betreff=?',
				$_REQUEST['betreff']
				);
			$db->Query('UPDATE {pre}regbest SET pt=?',
				$_REQUEST['pt']
				);
			$db->Query('UPDATE {pre}regbest SET absender=?',
				$_REQUEST['absender']
				);
		}
		
		// regbestaktiviert abrufen und übergeben
		$res = $db->Query('SELECT regbestaktiviert FROM {pre}prefs');
		$row = $res->FetchArray(); 
		$regbest = $row['regbestaktiviert'];
		$res->Free();

		// regbest-daten auslesen und übergeben
		$res2 = $db->Query('SELECT * FROM {pre}regbest');
		$row2 = $res2->FetchArray(); 
		$pt = $row2['pt'];
		$betreff = $row2['betreff'];
		$mailabs = $row2['absender'];
		$res2->Free();
			
		// an template übergeben
		$tpl->assign('regbest', $regbest);
		$tpl->assign('pt', $pt);
		$tpl->assign('betreff', $betreff);
		$tpl->assign('absender', $mailabs);
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('regbest.plugin.prefs.tpl'));
		return(true);
	}

	function _prefsVorschau()
	{
		global $tpl, $lang_custom;
		
		$mailtext = $lang_custom['regbestaltmailTXT'];
		$mailtext = str_replace("\n", "<br>", $mailtext);

		// an template übergeben
		$tpl->assign('mailtext', str_replace("\n", "<br>", $lang_custom['regbestaltmailTXT']));
		$tpl->assign('pageURL', $this->_adminLink());
		$tpl->assign('page', $this->_templatePath('regbest.plugin.vorschau.tpl'));
		return(true);
	}

	function OnSignup($userid, $usermail)
	{
		global $bm_prefs, $db;

		$bestakt = $bm_prefs['regbestaktiviert'];
		// no = sofort aktiviert - locked = manuelle freischaltung
		$status = $bm_prefs['usr_status'];

		if($bestakt == 1)
		{
			// alles vom user, wo regbestgesendet = 0 ist, wo altmail nicht leer ist und wo die id=user-id ist
			$sql = $db->Query("SELECT * FROM {pre}users WHERE regbestgesendet = 0 AND altmail IS NOT NULL AND id = ?", $userid);
			$row = $sql->FetchArray();

			$regbest = $row['regbestgesendet'];
			// alternativ-email-adresse
			$mailaddy = $row['altmail'];
			// erste domain aus der datenbank der variable zuweisen
			list($domain) = $bm_prefs['domains'];

			$sql2 = $db->Query("SELECT * FROM {pre}regbest");
			$row2 = $sql2->FetchArray();

			// projekt-titel
			$pt = $row2['pt'];
			// mail-betreff
			$betreff = $row2['betreff'];
			// mail-absender
			$mailabs = $row2['absender'];

			if($mailaddy != '')
			{
				if($status == "no")
				{		
					// bestätigungsmail senden				
					// ersetzt die variable %%pt%%, %%domain%% und %%usermail%% im regbestaltmailTXT
					$vars = array('pt' => $pt,
							 	  'domain' => $domain,
								  'usermail' => $usermail); 
					// spam-report senden mit systemmail-einstellungen
					SystemMail($mailabs, $mailaddy, $betreff, 'regbestaltmailTXT', $vars, $userid);

					// regbestgesendet auf gesendet - ja(1) setzen
					$db->Query("UPDATE {pre}users SET regbestgesendet = 1 WHERE id = ?", $userid);
					// mailtext löschen, da dieser bereits gesendet wurde
					$db->Query("UPDATE {pre}users SET mailtext = ''");
				}
				elseif($status == "locked")
				{
					// mailtext als gespeichert markieren, bis er manuell aktiviert wird
					$db->Query("UPDATE {pre}users SET mailtext = ? WHERE id = ?", 'saved', $userid);

					PutLog("Save <regbestaltmailTXT> for the user <" . $usermail . "> (" . $userid . "), because this user must be activated manually",
							PRIO_NOTE,
							__FILE__,
							__LINE__);
				}
			}
			$sql->Free();
			$sql2->Free();
		}
	}

	function OnCron()
	{
		global $bm_prefs, $db;

		$bestakt = $bm_prefs['regbestaktiviert'];

		if($bestakt == 1)
		{
			// alles vom user, wo regbestgesendet = 0 ist und wo altmail nicht leer ist
			$sql = $db->Query("SELECT * FROM {pre}users WHERE regbestgesendet = 0 AND altmail IS NOT NULL");

			$sql2 = $db->Query("SELECT * FROM {pre}regbest");
			$row2 = $sql2->FetchArray();

			// projekt-titel
			$pt = $row2['pt'];
			// mail-betreff
			$betreff = $row2['betreff'];
			// mail-absender
			$mailabs = $row2['absender'];

			while($row = $sql->FetchArray(MYSQLI_ASSOC))
			{
				$regbest = $row['regbestgesendet'];
				// alternativ-email-adresse
				$mailaddy = $row['altmail'];
				// mail-text vom manuell aktivierten user
				$mailtext = $row['mailtext'];
				// locked = nicht aktiviert, yes = gesperrt und no = freigeschaltet/aktiviert
				$status = $row['gesperrt'];
				// user-mail
				$usermail = $row['email'];
				// user-id
				$userid = $row['id'];
				// erste domain aus der datenbank der variable zuweisen

#				$handle = fopen ("blabla.txt", "w");
#				fwrite ($handle, $bm_prefs['domains']);
#				fclose ($handle);
#				list($domain) = explode(':', $bm_prefs['domains']);
				list($domain) = array_values($bm_prefs['domains'])[0];
				if($regbest == 0 && $status == "no" && $mailaddy != '' && $mailtext != '')
				{
					// bestätigungsmail senden				
					// ersetzt die variable %%pt%%, %%domain%% und %%usermail%% im regbestaltmailTXT
					$vars = array('pt' => $pt,
							 	  'domain' => $domain,
								  'usermail' => $usermail); 
					// spam-report senden mit systemmail-einstellungen
					SystemMail($mailabs, $mailaddy, $betreff, 'regbestaltmailTXT', $vars, $userid);

					// regbestgesendet auf gesendet - ja(1) setzen
					$db->Query("UPDATE {pre}users SET regbestgesendet = 1 WHERE id = ?", $userid);
					// mailtext löschen, da dieser bereits gesendet wurde
					$db->Query("UPDATE {pre}users SET mailtext = ''");
				}
				elseif($regbest == 0 && $status == "no" && $mailtext == '')
				{
					// regbestgesendet auf gesendet - ja(1) setzen, bei den usern, wo keine alternative email-adresse vorhanden ist
					$db->Query("UPDATE {pre}users SET regbestgesendet = 1 WHERE regbestgesendet = 0");
				}
			}
			$sql->Free();
			$sql2->Free();
		}
	}
}

$plugins->registerPlugin('RegBestAltMailPlugin');

?>
