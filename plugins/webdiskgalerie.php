<?php
/*
 * Plugin webdiskgalerie
*/
/*
 * Variablen des Plugins
*/
define('WEBDISKGAL_ITEM_FOLDER',		1);
define('WEBDISKGAL_ITEM_FILE',			2);
// 0 = cache off 
// 1 = cache on
define('WEBDISK_GAL_CACHE',				1);

class webdiskgalerie extends BMPlugin 
{
	/*
	* Eigenschaften des Plugins
	*/
	function webdiskgalerie()
	{
		$this->name					= 'Webdisk Galerie';
		$this->version				= '2.1.1';
		$this->designedfor			= '7.2.0';
		$this->type					= BMPLUGIN_DEFAULT;

		$this->author				= 'Darkcentrino & Informant';
		$this->update_url			= 'http://my.b1gmail.com/update_service/';

		$this->RegisterGroupOption('webdiskgalerie', FIELD_CHECKBOX, 'Galerie?');
	}

	/*
	*  Sprach variablen
	*/
	function OnReadLang(&$lang_user, &$lang_client, &$lang_custom, &$lang_admin, $lang)
	{
		if($lang == 'deutsch')
		{
			$lang_user['webdiskgalerie_galerie']   		= 'Galerie';
			$lang_user['webdiskgalerie_galerie_view']	= 'Galerie anzeigen';
			$lang_user['webdiskgalerie_galerie_deview']	= 'Galerie ausblenden';
			$lang_user['webdiskgalerie_galerie_mode']	= 'Galerie-Modus';
			$lang_user['webdiskgalerie_nopic']			= 'Bedauerlicherweise sind in dieser Galerie noch keine Bilder verf&uuml;gbar.';
			$lang_user['webdiskgalerie_share']			= 'Ihre Galerie ist derzeit freigegeben und &ouml;ffentlich unter folgender Adresse erreichbar:';
			$lang_user['webdiskgalerie_sls']   			= 'Slideshow starten';
		} else {
			$lang_user['webdiskgalerie_galerie']		= 'gallery';
			$lang_user['webdiskgalerie_galerie_view']	= 'view gallery';
			$lang_user['webdiskgalerie_galerie_deview']	= 'hide gallery';
			$lang_user['webdiskgalerie_galerie_mode']	= 'gallery-mode';
			$lang_user['webdiskgalerie_nopic']			= 'sorry, in this gallery are no pictures available';
			$lang_user['webdiskgalerie_share']			= 'your gallery is currently released and publicly accessible at the following address:';
			$lang_user['webdiskgalerie_sls']   			= 'start slideshow';
		}
	}

	/*
	 * installation routine
	 */	
	function Install()
	{
		global $db;

		$db->Query('ALTER TABLE `{pre}diskfolders` ADD `galerie` enum(\'yes\',\'no\') NOT NULL DEFAULT \'no\'');
		$db->Query('CREATE TABLE IF NOT EXISTS {pre}galtmbfiles (
			`id_dsk` INT(11) NOT NULL,
			`id_tmb` INT(11) NOT NULL)');

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich installiert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}

	/*
	 * uninstallation routine
	 */
	function Uninstall()
	{
		global $db;

		$db->Query('ALTER TABLE `{pre}diskfolders` DROP `galerie`');
		$db->Query('DROP TABLE {pre}galtmbfiles');

		PutLog('Plugin "'. $this->name .' - '. $this->version .'" wurde erfolgreich deinstalliert.', PRIO_PLUGIN, __FILE__, __LINE__);
		return(true);
	}
	
	function setTransparency($new_image, $image_source) 
    { 
		$transparencyIndex = imagecolortransparent($image_source); 
		$transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255); 
             
		if($transparencyIndex >= 0)
		{ 
			$transparencyColor = imagecolorsforindex($image_source, $transparencyIndex);    
		} 
            
		$transparencyIndex = imagecolorallocate($new_image, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']); 
		imagefill($new_image, 0, 0, $transparencyIndex); 
		imagecolortransparent($new_image, $transparencyIndex); 
    }
	
    function createGalCache($userID)
    {
        global $db;
        
        if($userID != -1)
            $res = $db->Query("SELECT `id`, contenttype FROM {pre}diskfiles WHERE (contenttype LIKE 'image/%' OR dateiname LIKE '%.png' OR dateiname LIKE '%.jpg' OR dateiname LIKE '%.jpeg' OR dateiname LIKE '%.gif') AND contenttype <> 'image/bmp' AND user = ?", $userID);
        else
            $res = $db->Query("SELECT `id`, contenttype FROM {pre}diskfiles WHERE (contenttype LIKE 'image/%' OR dateiname LIKE '%.png' OR dateiname LIKE '%.jpg' OR dateiname LIKE '%.jpeg' OR dateiname LIKE '%.gif') AND contenttype <> 'image/bmp'");
		while($row = $res->FetchArray(MYSQLI_ASSOC))
		{
			// neuen dateinamen generieren und pr�fen, ob dieser bereits existiert
			$filename = DataFilename($row['id'], 'tmb');
			if(!file_exists($filename))
			{
				// vorhandene (originale) datei holen
				$filename = DataFilename($row['id'], 'dsk');

				$w = 100;
				$h = 100;

				// get image size of img
				$x = @getimagesize($filename);
				
				// image width
				$sw = $x[0];
				// image height
				$sh = $x[1];
				
				// get the smaller resulting image dimension if both height
				// and width are set and $constrain is also set
				$hx = (100 / ($sw / $w)) * .01;
				$hx = @round ($sh * $hx);
		
				$wx = (100 / ($sh / $h)) * .01;
				$wx = @round ($sw * $wx);

				if ($hx < $h)
				{
					$h = (100 / ($sw / $w)) * .01;
					$h = @round ($sh * $h);
				}
				else
				{
					$w = (100 / ($sh / $h)) * .01;
					$w = @round ($sw * $w);
				}
				
				// php-memory_limit (speicher) pr�fen
				$memory = ini_get('memory_limit');
				$wert = "GB";
				
				if(preg_match("/$wert/i", $memory))
				{
					$memory = substr($memory, 0, -2);  // schneidet GB ab
					$memory = $memory*1024; // in MB umrechnen
				}
				else
					$memory = substr($memory, 0, -1);  // schneidet K oder M ab

				// php-memory_limit (speicher) pr�fen und wenn n�tig erh�hen, damit die bilder �ber den arbeitsspeiher umconvertiert werden k�nnen
				if($memory < 128)
					ini_set("memory_limit", "128M");	
					
				$im = @ImageCreateFromJPEG($filename) or // Read JPEG Image
				$im = @ImageCreateFromPNG($filename) or // or PNG Image
				$im = @ImageCreateFromGIF($filename) or // or GIF Image
				$im = false; // If image is not JPEG, PNG, or GIF
	
				// thumb-file erstellen
				if($im)
				{
					// Create the resized image destination
					$thumb = @ImageCreateTrueColor ($w, $h);
					
					// transparenz pr�fen
					$this->setTransparency($thumb, $im);
					
					// Copy from image source, resize it, and paste to image destination
					@ImageCopyResampled($thumb, $im, 0, 0, 0, 0, $w, $h, $sw, $sh);
					
					$verz = DataFilename($row['id'], 'tmb');
					
					// komprimierte datei in neue datei speichern
					if(strpos($row['contenttype'], 'gif') !== false) {
						@ImageGIF($thumb, $verz);
					} else {
						@ImagePNG($thumb, $verz);
					}

					// ids in datenbank speichern
					$db->Query("INSERT INTO {pre}galtmbfiles(id_dsk, id_tmb) VALUES(?,?)", 
						$row['id'], 
						$row['id']);
				}
				// speicher nach der konvertierung jedes bildes wieder freigeben
				imagedestroy($im);
				imagedestroy($thumb);
			}
		}
		$res->Free();
    }
    
	/*
	* bilder cachen, wenn cachen aktiviert ist
	*/
	function OnCron()
	{
		$stunde = date("G");
        		
		// cachen zwischen 4 und 5 uhr und datei l�schen, wenn original-datei nicht mehr existiert
		if(WEBDISK_GAL_CACHE && $stunde >= 4 && $stunde <= 5)
		{
            $this->createGalCache(-1);
          
			// tmb-dateien l�schen, von der keine original-datei mehr existiert
			$res = $db->Query("SELECT * FROM {pre}galtmbfiles WHERE NOT EXISTS (SELECT `id` FROM {pre}diskfiles WHERE {pre}galtmbfiles.id_dsk = {pre}diskfiles.id)");
			while($row = $res->FetchArray(MYSQLI_ASSOC))
			{
				$db->Query("DELETE FROM {pre}galtmbfiles WHERE id_tmb=?", $row['id_dsk']);
				// datei aus dem verzeichnis l�schen
				$verz = DataFilename($row['id_tmb'], 'tmb');
				unlink($verz);
			}
			$res->Free();
		}
	}

	/*
	*  Anzeigen der Seiten
	*/
	function FileHandler($file, $action)
	{
		global $tpl, $db, $groupRow, $userRow, $thisUser, $lang_user, $bm_prefs;

		if($file == 'webdisk.php')
		{
			if(!$this->GetGroupOptionValue('webdiskgalerie', $groupRow['id']))
				return;

			$webdisk 		= _new('BMWebdisk', array($userRow['id']));
			$folderID 		= !isset($_REQUEST['folder']) ? 0 : (int)$_REQUEST['folder'];
			$folderPath 	= $webdisk->GetFolderPath($folderID);
			$spaceLimit 	= $webdisk->GetSpaceLimit();
			$usedSpace 		= $webdisk->GetUsedSpace();
			$tpl->assign('activeTab', 'webdisk');
			$tpl->assign('pageMenuFile', 	'li/webdisk.sidebar.tpl');
			$tpl->assign('pageToolbarFile', $this->_templatePath('webdiskgalerie.webdisk.toolbar.tpl'));
			$tpl->assign('viewMode', 		($viewMode = $thisUser->GetPref('webdiskViewMode')) === false ? 'icons' : $viewMode);
			$tpl->assign('spaceUsed', 		$usedSpace);
			$tpl->assign('trafficUsed', 	$userRow['traffic_down'] + $userRow['traffic_up']);
			$tpl->assign('clipboard', 		isset($_SESSION['clipboard']) && is_array($_SESSION['clipboard']) && count($_SESSION['clipboard']) == 1);
			$tpl->assign('spaceLimit', 		$spaceLimit);
			$tpl->assign('trafficLimit', 	$groupRow['traffic']);
			$tpl->assign('folderID', 		$folderID);
			$tpl->assign('currentPath', 	$folderPath);
			$tpl->assign('userAgent',		$_SERVER['HTTP_USER_AGENT']);
			$tpl->assign('dndKey',			isset($_COOKIE['sessionSecret_' . session_id()]) ? $_COOKIE['sessionSecret_' . session_id()] : '');
			$tpl->assign('allowShare',		$groupRow['share'] == 'yes');

			$folderInfo = $webdisk->GetFolderInfo((int)$folderID);
			$tpl->assign('isGalerie', 	$folderInfo['galerie'] == 'yes');
			$tpl->assign('galerieURL', $bm_prefs['selfurl'].'index.php?action=galerie&gal='.$folderID);

			$titlePath = '/';
			foreach($folderPath as $folderBit)
				$titlePath .= $folderBit['title'] . '/';

			if($action == 'saveShareSettings' && isset($_REQUEST['id']) && $groupRow['share'] == 'yes')
			{
                //cache generieren, sobald user eine galerie freigibt
                if(WEBDISK_GAL_CACHE)
                    $this->createGalCache($thisUser->_id);
                
				$db->Query('UPDATE {pre}diskfolders SET galerie=? WHERE id=? AND user=?',
					isset($_REQUEST['galerie_modus']) ? 'yes' : 'no',
					(int)$_REQUEST['id'],
					$userRow['id']);
			}

			if($action == 'webdiskgalerie' OR (isset($_REQUEST['do']) && $_REQUEST['do'] == 'changeViewMode' && $_REQUEST['viewmode'] == "galerie"))
			{
				if($groupRow['traffic'] <= 0 || ($userRow['traffic_down']+$userRow['traffic_up']) <= $groupRow['traffic'])
				{
					$tpl->addJSFile("li", 	"./plugins/js/prototype.js");
					$tpl->addJSFile("li", 	"./plugins/js/scriptaculous.js?load=effects,builder");
					$tpl->addJSFile("li", 	"./plugins/js/lightbox.js");

					$tpl->addCSSFile("li", 	"./plugins/css/webdiskgalerie_gal.css");

					//$tpl->assign('shareURL', sprintf('%sshare/?user=%s', $bm_prefs['selfurl'], $userRow['email']));
					$tpl->assign('isShared', $folderInfo['galerie'] == 'yes');
					$tpl->assign('pageTitle', $titlePath);
					//$tpl->assign('folderContent', $folderContent);
					$tpl->assign('images',$this->_GetImages($webdisk, $folderID));
					$tpl->assign('folders',$this->_GetFolders($webdisk, $folderID, true));
					$tpl->assign('folders_count', count($this->_GetFolders($webdisk, $folderID, true)));
					$tpl->assign('viewMode', 'galerie');
					$tpl->assign('pageContent', $this->_templatePath('webdiskgalerie.webdisk.folder.tpl'));
					$tpl->display('li/index.tpl');
					exit();
				} else {
					// not enough traffic
					$tpl->assign('msg', $lang_user['notraffic'] . '.');
					$tpl->assign('pageContent', 'li/error.tpl');
					$tpl->display('li/index.tpl');
					exit();
				}
			}
			
			if($action == 'getpicfile' AND isset($_REQUEST['picid']))
			{
				$picID = $_REQUEST['picid'];
				$res = $db->Query('SELECT dateiname, contenttype FROM {pre}diskfiles WHERE id=? AND user=? Limit 1', 
					$picID,
					$userRow['id']);
				if($res->RowCount() == 1)
				{
					$row = $res->FetchArray();
					$res->free();
					// dateinamen generieren
					$filename = DataFilename($picID, 'dsk');

					// pr�fen, ob datei existiert
					if(file_exists($filename))
					{
						$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
							(int) $webdisk->GetFileSize($picID),
							(int) $userRow['id']);

					    header('Content-Type: '.$row['contenttype']);
					    // datei-ausgabe
					    readfile($filename);
					    exit();
					}
				}

				// id gibt es nicht ,oder nicht vom user
				DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
				exit();
			}
			
			if($action == 'getthumbfile' AND isset($_REQUEST['picid']))
			{
				$picID = $_REQUEST['picid'];
				$res = $db->Query('SELECT dateiname, contenttype FROM {pre}diskfiles WHERE id=? AND user=? Limit 1', 
					$picID,
					$userRow['id']);
				if($res->RowCount() == 1)
				{
					$row = $res->FetchArray();
					$res->free();
					// dateinamen generieren
					$filename = DataFilename($picID, 'tmb');

					// pr�fen, ob datei existiert
					if(file_exists($filename))
					{
						$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
							(int) $webdisk->GetFileSize($picID),
							(int) $userRow['id']);

						if(strpos($row['contenttype'], 'gif') !== false) {
							header('Content-Type: image/gif');
						} else {
							header('Content-Type: image/png');
						}

					    // datei-ausgabe
					    readfile($filename);
					    exit();
					} else {
						$filename = DataFilename($picID, 'dsk');
						if(file_exists($filename))
						{
							$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
								(int) $webdisk->GetFileSize($picID),
								(int) $userRow['id']);

						    header('Content-Type: '.$row['contenttype']);
						    // datei-ausgabe
						    readfile($filename);
						    exit();
						}
					}
				}

				// id gibt es nicht ,oder nicht vom user
				DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
				exit();
			}
		}
		
		if($file == 'index.php')
		{
			if($action == 'galerie' AND isset($_REQUEST['gal']))
			{
				if(!class_exists('BMWebdisk'))
					include(B1GMAIL_DIR . 'serverlib/webdisk.class.php');

				$res = $db->Query('SELECT user, titel, share_pw FROM {pre}diskfolders WHERE id=? AND galerie=? Limit 1', 
					(int) $_REQUEST['gal'],
					'yes');

				if($res->RowCount() == 1)
				{
					$row = $res->FetchArray();
					$res->free();

					$thisUser = _new('BMUser', array($row['user']));
					$userRow = $thisUser->Fetch();
					$thisGroup = $thisUser->GetGroup();
					$groupRow = $thisGroup->Fetch();
					$webdisk = _new('BMWebdisk', array($row['user']));

					$folderPath = $webdisk->GetFolderPath((int) $_REQUEST['gal']);
					$folderInfoAct = $webdisk->GetFolderInfo((int)$_REQUEST['gal']);

					$pw 		= !isset($_REQUEST['pw']) ? md5("") : md5($_REQUEST['pw']);
					$pw 		= !isset($_REQUEST['pw2']) ? $pw : $_REQUEST['pw2'];
					if(md5($folderInfoAct['share_pw']) != $pw)
					{
						$tpl->assign('folder', (int)$_REQUEST['gal']);
						$tpl->assign('galerie', true);
						$tpl->display($this->_templatePath('webdiskgalerie.dialog.password.tpl'));
						exit();
					}

					if($groupRow['traffic'] <= 0 || ($userRow['traffic_down']+$userRow['traffic_up']) <= $groupRow['traffic'])
					{
						$folderInfoParent = $webdisk->GetFolderInfo((int)$folderInfoAct['parent']);
						if($folderInfoParent != false)
						{
							$tpl->assign('parentGal', 	true);
						} else {
							$tpl->assign('parentGal', 	false);
						}

						$tpl->assign('pw', 	$pw);
						$tpl->assign('parentID', 	$folderInfoAct['parent']);
						$tpl->assign('currentPath', 	$folderPath);
						$tpl->assign('pageTitle', $row['titel']);
						$tpl->assign('folderTitel', $row['titel']);
						$tpl->assign('images', $this->_GetImages($webdisk, $_REQUEST['gal']));
						$tpl->assign('folders', $this->_GetFolders($webdisk, $_REQUEST['gal'], false));
						$tpl->assign('gal',$_REQUEST['gal']);
						$tpl->assign('picscount',count($this->_GetImages($webdisk, $_REQUEST['gal'])));
						$tpl->assign('folders_count', count($this->_GetFolders($webdisk, $_REQUEST['gal'], false)));
						
						$tpl->assign('isgalerie', 	true);
						$tpl->assign('page', $this->_templatePath('webdiskgalerie.index.galerie.tpl'));
						$tpl->display($this->_templatePath('webdiskgalerie.index.tpl'));
						exit();
					} else {
						// not enough traffic
						$tpl->assign('msg', $lang_user['notraffic'] . '.');
						$tpl->assign('page', $this->_templatePath('webdiskgalerie.webdisk.error.tpl'));
						$tpl->display('nli/index.tpl');
						exit();
					}
				} else {
					// id gibt es nicht ,oder keine galerie
					DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
					exit();
				}
			}

			if($action == 'slideshow' AND isset($_REQUEST['gal']))
			{
				if(!class_exists('BMWebdisk'))
					include(B1GMAIL_DIR . 'serverlib/webdisk.class.php');

				$res = $db->Query('SELECT user, titel, share_pw FROM {pre}diskfolders WHERE id=? AND galerie=? Limit 1', 
					(int) $_REQUEST['gal'],
					'yes');

				if($res->RowCount() == 1)
				{
					$row = $res->FetchArray();
					$res->free();

					$thisUser = _new('BMUser', array($row['user']));
					$userRow = $thisUser->Fetch();
					$thisGroup = $thisUser->GetGroup();
					$groupRow = $thisGroup->Fetch();
					$webdisk = _new('BMWebdisk', array($row['user']));

					$folderPath = $webdisk->GetFolderPath((int) $_REQUEST['gal']);
					$folderInfoAct = $webdisk->GetFolderInfo((int)$_REQUEST['gal']);

					$titlePath = '/';
					foreach($folderPath as $folderBit)
						$titlePath .= $folderBit['title'] . '/';

					$pw 		= !isset($_REQUEST['pw']) ? md5("") : md5($_REQUEST['pw']);
					$pw 		= !isset($_REQUEST['pw2']) ? $pw : $_REQUEST['pw2'];
					if(md5($folderInfoAct['share_pw']) != $pw)
					{
							$tpl->assign('folder', (int)$_REQUEST['gal']);
						$tpl->assign('galerie', false);
						$tpl->display($this->_templatePath('webdiskgalerie.dialog.password.tpl'));
						exit();
					}

					if($groupRow['traffic'] <= 0 || ($userRow['traffic_down']+$userRow['traffic_up']) <= $groupRow['traffic'])
					{
						$tpl->assign('pw', 	$pw);
						$tpl->assign('currentPath', 	$folderPath);
						$tpl->assign('pageTitle', $row['titel']);
						$tpl->assign('folderTitel', $row['titel']);
						$tpl->assign('images', $this->_GetImages($webdisk, $_REQUEST['gal']));
						
						$tpl->assign('isslideshow', 	true);
						$tpl->assign('page', $this->_templatePath('webdiskgalerie.index.slideshow.tpl'));
						$tpl->display($this->_templatePath('webdiskgalerie.index.tpl'));
						exit();
					} else {
						// not enough traffic
						$tpl->assign('msg', $lang_user['notraffic'] . '.');
						$tpl->assign('page', $this->_templatePath('webdiskgalerie.webdisk.error.tpl'));
						$tpl->display('nli/index.tpl');
						exit();
					}
				} else {
					// id gibt es nicht ,oder keine galerie
					DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
					exit();
				}
			}

			if($action == 'getpicfile' AND isset($_REQUEST['picid']))
			{
				if(!class_exists('BMWebdisk'))
					include(B1GMAIL_DIR . 'serverlib/webdisk.class.php');

				$picID = $_REQUEST['picid'];
				$res = $db->Query('SELECT user, dateiname, contenttype, ordner FROM {pre}diskfiles WHERE id=? Limit 1', 
					$picID);

				if($res->RowCount() == 1)
				{
					$row = $res->FetchArray();
					$res->free();

					$webdisk = _new('BMWebdisk', array($row['user']));

					$folderInfo = $webdisk->GetFolderInfo((int)$row['ordner']);
					if($folderInfo['galerie'] == 'yes' AND md5($folderInfo['share_pw']) == $_REQUEST['pw'])
					{
						// dateinamen generieren
						$filename = DataFilename($picID, 'dsk');

						// pr�fen, ob datei existiert
						if(file_exists($filename))
						{
							$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
								(int) $webdisk->GetFileSize($picID),
								(int) $row['user']);

						    header('Content-Type: '.$row['contenttype']);
						    // datei-ausgabe
						    readfile($filename);
						    exit();
						}
					}
				}

				// id gibt es nicht oder ist nicht vom user
				DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
				exit();
			}

			if($action == 'getthumbfile' AND isset($_REQUEST['picid']))
			{
				if(!class_exists('BMWebdisk'))
					include(B1GMAIL_DIR . 'serverlib/webdisk.class.php');

				$picID = $_REQUEST['picid'];
				$res = $db->Query('SELECT user, dateiname, contenttype, ordner FROM {pre}diskfiles WHERE id=? Limit 1', 
					$picID);
				if($res->RowCount() == 1)
				{
					$row = $res->FetchArray();
					$res->free();
					
					$webdisk = _new('BMWebdisk', array($row['user']));

					$folderInfo = $webdisk->GetFolderInfo((int)$row['ordner']);
					if($folderInfo['galerie'] == 'yes' AND md5($folderInfo['share_pw']) == $_REQUEST['pw'])
					{
						// dateinamen generieren
						$filename = DataFilename($picID, 'tmb');

						// pr�fen, ob datei existiert
						if(file_exists($filename))
						{
							$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
								(int) $webdisk->GetFileSize($picID),
								(int) $row['user']);

							if(strpos($row['contenttype'], 'gif') !== false) {
								header('Content-Type: image/gif');
							} else {
								header('Content-Type: image/png');
							}

						    // datei-ausgabe
					    	readfile($filename);
						    exit();
						} else {
							$filename = DataFilename($picID, 'dsk');
							if(file_exists($filename))
							{
								$db->Query('UPDATE {pre}users SET traffic_down=traffic_down+? WHERE id=?',
									(int) $webdisk->GetFileSize($picID),
									(int) $row['user']);

							    header('Content-Type: '.$row['contenttype']);
							    // datei-ausgabe
							    readfile($filename);
							    exit();
							}
						}
					}
				}

				// id gibt es nicht ,oder nicht vom user
				DisplayError(__LINE__, "Unauthorized", "You are not authrized to view or change this dataset or page. Possible reasons are too few permissions or an expired session.", "Diese Seite existiert nicht oder nicht mehr. Bitte &uuml;berpr&uuml;fen Sie die Adresse.", __FILE__, __LINE__);
				exit();
			}
		}
	}
	
	function BeforeDisplayTemplate($resourceName, &$tpl)
	{
		global $tpl, $groupRow, $userRow;

		if ($resourceName == 'li/index.tpl' && preg_match('/webdisk.php/',$_SERVER['REQUEST_URI']) && $this->GetGroupOptionValue('webdiskgalerie', $groupRow['id']))
		{
			if((isset($_REQUEST['action']) && $_REQUEST['action'] == 'webdiskgalerie') OR (isset($_REQUEST['do']) && $_REQUEST['do'] == 'changeViewMode' && $_REQUEST['viewmode'] == "galerie")) {
				$mode 	= "deview";
			} else {
				$mode 	= "view";
			}
			$tpl->assign('galerie_sidebar_mode', 	$mode);
			$tpl->registerHook("webdisk.sidebar.tpl:foot", $this->_templatePath('webdiskgalerie.webdisk.sidebar.tpl'));
			$tpl->assign('pageToolbarFile', $this->_templatePath('webdiskgalerie.webdisk.toolbar.tpl'));

			if(preg_match('/shareFolder/',$_SERVER['REQUEST_URI']) && isset($_REQUEST['id']) && $groupRow['share'] == 'yes')
			{
				$webdisk 	= _new('BMWebdisk', array($userRow['id']));
				$folderInfo = $webdisk->GetFolderInfo((int)$_REQUEST['id']);
				if($folderInfo !== false)
				{
					$tpl->assign('galerie_modus', 	$folderInfo['galerie'] == 'yes');
					$tpl->assign('pageContent', 	$this->_templatePath('webdiskgalerie.webdisk.share.tpl'));
				}
			}
		}
	}

	function _GetFolders($webdisk, $folderID, $galeriemode)
	{
		$folders = array();

		foreach($webdisk->GetFolderContent($folderID) AS $row)
		{
			if($row['type'] == WEBDISKGAL_ITEM_FOLDER)
			{
				$folderInfo = $webdisk->GetFolderInfo((int)$row['id']);
				if(($folderInfo['share'] == 'yes' AND $folderInfo['galerie'] == 'yes') OR $galeriemode)
				{
					$folders[] = array('id'=> $row['id'],
                                       'title'=> $row['title'], 
                                       'share'=> $row['share'], 
                                       'ext'=> $row['ext'], 
                                       'type'=> $row['type'], 
                                       'created'=> $row['created']); 
				}
			}
		}
		return $folders;
	}

	function _GetImages($webdisk, $folderID)
	{
		$images = array();

		foreach($webdisk->GetFolderContent($folderID) AS $row)
		{
			if($row['type'] == WEBDISKGAL_ITEM_FILE) {
				if(strpos(strtoupper($row['ctype']), 'IMAGE') !== false
					OR strpos(strtoupper($row['ext']), 'PNG') !== false
					OR strpos(strtoupper($row['ext']), 'JPG') !== false
					OR strpos(strtoupper($row['ext']), 'JPEG') !== false
					OR strpos(strtoupper($row['ext']), 'GIF') !== false)
				{
					$zeichen = strripos($row['title'], '.');
					$title = substr($row['title'], 0, $zeichen);
					$images[] = array(
						'id'		=> $row['id'],
						'title'		=> $title
					);
				}
			}
		}
		return $images;
	}
}

$plugins->registerPlugin('webdiskgalerie');
?>
