<?php 
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

include('./serverlib/init.inc.php');

/**
 * file handler for modules
 */
ModuleFunction('FileHandler',
	array(substr(__FILE__, strlen(dirname(__FILE__))+1),
	isset($_REQUEST['action']) ? $_REQUEST['action'] : ''));

/**
 * check referer
 */
if(!isset($_SERVER['HTTP_REFERER'])
	|| strpos(strtolower($_SERVER['HTTP_REFERER']), strtolower($_SERVER['HTTP_HOST'])) === false)
{
	if($bm_prefs['cookie_lock'] == 'yes')
	{
		$ok = false;
		foreach($_COOKIE as $key=>$val)
			if(substr($key, 0, strlen('sessionSecret_')) == 'sessionSecret_')
				$ok = true;
		if(!$ok)
			die('Access denied');
	}
}

/**
 * deref code
 */
$url = $_SERVER['REQUEST_URI'];
$sepPos = strpos($url, '?');
if($sepPos !== false)
{
	$targetURL = substr($url, $sepPos+1);
	$tpl->assign('url', HTMLFormat($targetURL));
	$tpl->display('nli/deref.tpl');
}
?>