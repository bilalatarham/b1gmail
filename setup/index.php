<?php
/*
 * b1gMail
 * (c) 2002-2018 B1G Software
 *
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

// init
include('./common.inc.php');

//  operations per call
define('CALL_OPS',					50);

// message flags (needed while updating)
define('FLAG_UNREAD',				1);
define('FLAG_ANSWERED',				2);
define('FLAG_FORWARDED',			4);
define('FLAG_DELETED',				8);
define('FLAG_FLAGGED',				16);
define('FLAG_SEEN',					32);
define('FLAG_ATTACHMENT',			64);
define('FLAG_INFECTED',				128);
define('FLAG_SPAM',					256);
define('FLAG_CERTMAIL',				512);

// folders (needed while updating)
define('FOLDER_INBOX',				0);
define('FOLDER_OUTBOX',				-2);
define('FOLDER_DRAFTS',				-3);
define('FOLDER_SPAM',				-4);
define('FOLDER_TRASH',				-5);
define('FOLDER_ROOT',				-128);

// filter stuff (needed while updating)
define('BMOP_EQUAL',				1);
define('BMOP_NOTEQUAL',				2);
define('BMOP_CONTAINS',				3);
define('BMOP_NOTCONTAINS',			4);
define('BMOP_STARTSWITH',			5);
define('BMOP_ENDSWITH',				6);
define('MAILFIELD_SUBJECT',			1);
define('MAILFIELD_FROM',			2);
define('MAILFIELD_TO',				3);
define('MAILFIELD_CC',				4);
define('MAILFIELD_BCC',				5);
define('MAILFIELD_READ',			6);
define('MAILFIELD_ANSWERED',		7);
define('MAILFIELD_FORWARDED',		8);
define('MAILFIELD_PRIORITY',		9);
define('MAILFIELD_ATTACHMENT',		10);
define('MAILFIELD_FLAGGED',			11);
define('MAILFIELD_FOLDER',			12);
define('MAILFIELD_ATTACHLIST',		13);
define('FILTER_ACTION_MOVETO',		1);
define('FILTER_ACTION_BLOCK',		2);
define('FILTER_ACTION_DELETE',		3);
define('FILTER_ACTION_MARKREAD',	4);
define('FILTER_ACTION_MARKSPAM',	5);
define('FILTER_ACTION_MARK',		6);
define('FILTER_ACTION_STOP',		7);

// calendar stuff (needed while updating)
define('CLNDR_WHOLE_DAY',					1);
define('CLNDR_REMIND_EMAIL',				2);
define('CLNDR_REMIND_SMS',					4);
define('CLNDR_REPEATING_UNTIL_ENDLESS',		1);
define('CLNDR_REPEATING_UNTIL_COUNT',		2);
define('CLNDR_REPEATING_UNTIL_DATE',		4);
define('CLNDR_REPEATING_DAILY',				8);
define('CLNDR_REPEATING_WEEKLY',			16);
define('CLNDR_REPEATING_MONTHLY_MDAY',		32);
define('CLNDR_REPEATING_MONTHLY_WDAY',		64);

// steps
define('STEP_SELECT_LANGUAGE',		0);
define('STEP_WELCOME',				1);
define('STEP_SYSTEMCHECK',			3);
define('STEP_INSTALLTYPE',			4);

// fresh install
define('STEP_SERIAL',				5);
define('STEP_MYSQL',				6);
define('STEP_CHECK_MYSQL',			7);
define('STEP_CHECK_EMAIL',			8);
define('STEP_INSTALL',				9);

// update
define('STEP_UPDATE_MYSQL',			105);
define('STEP_UPDATE_CHECK_MYSQL',	106);
define('STEP_UPDATE_UPDATE',		107);
define('STEP_UPDATE_STEP',			108);
define('STEP_UPDATE_DONE',			109);

// invoice
$defaultInvoice = '<table width=\"100%\">\n    <tbody>\n        <tr>\n            <td style=\"font-family: Arial;\" align=\"left\">\n            	<h2>{$service_title}</h2>\n            </td>\n            <td style=\"font-family: Arial;\" align=\"right\">\n		{$service_title}<br>Bitte passen<br>Sie die Absender-Adresse<br>in der Rechnungsvorlage an.<br>\n	   </td>\n        </tr>\n        <tr style=\"font-family: Arial;\">\n            <td colspan=\"2\"><hr style=\"height: 1px;\" color=\"#666666\" noshade=\"noshade\" width=\"100%\"><br></td>\n        </tr>\n        <tr>\n            <td style=\"font-family: Arial;\" align=\"left\">\n            <table style=\"border: 1px solid rgb(0, 0, 0);\" bgcolor=\"#666666\" cellpadding=\"10\" cellspacing=\"0\" width=\"100%\">\n                <tbody>\n                    <tr>\n\n                        <td bgcolor=\"#ffffff\">{$vorname} {$nachname}<br>{$strasse} {$nr}<br>{$plz} {$ort}<br>{$land}</td>\n                    </tr>\n                </tbody>\n            </table>\n            </td>\n            <td style=\"font-family: Arial;\" align=\"right\">\n 			<b style=\"font-family: Arial;\">{lng p=\"date\"}: </b><span style=\"font-family: Arial;\">{$datum}</span><br style=\"font-family: Arial;\">\n 			<b style=\"font-family: Arial;\">{lng p=\"invoiceno\"}: </b><span style=\"font-family: Arial;\">{$rgnr}</span><br style=\"font-family: Arial;\">\n			<b style=\"font-family: Arial;\">{lng p=\"customerno\"}: </b><span style=\"font-family: Arial;\">{$kdnr}</span><br>\n	   </td>\n        </tr>\n        <tr style=\"font-family: Arial;\">\n            <td colspan=\"2\">\n            <p>&nbsp;</p>\n            <b><br>{lng p=\"yourinvoice\"}</b>\n            <p>{lng p=\"dearsirormadam\"},</p>\n            <p>{lng p=\"invtext\"}:</p>\n\n            <p>\n            <table cellpadding=\"4\" cellspacing=\"0\" width=\"100%\">\n                <tbody>\n                    <tr>\n                        <td width=\"10%\">{lng p=\"pos\"}</td>\n                        <td width=\"10%\">{lng p=\"count\"}</td>\n                        <td width=\"50%\">{lng p=\"descr\"}</td>\n                        <td width=\"15%\">{lng p=\"ep\"} ({$currency})</td>\n                        <td width=\"15%\">{lng p=\"gp\"} ({$currency})</td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"5\"><hr style=\"height: 1px;\" color=\"#666666\" noshade=\"noshade\" width=\"100%\"></td>\n                    </tr>\n{foreach from=$cart item=pos}\n                    <tr>\n                        <td>{$pos.pos}</td>\n                        <td>{$pos.count}</td>\n                        <td>{text value=$pos.text}</td>\n                        <td>{$pos.amount}</td>\n                        <td>{$pos.total}</td>\n                    </tr>\n{/foreach}\n                    <tr>\n                        <td colspan=\"5\"><hr style=\"height: 1px;\" color=\"#666666\" noshade=\"noshade\" width=\"100%\"></td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"4\" align=\"right\">{lng p=\"gb\"} ({lng p=\"net\"}):</td>\n                        <td>{$netto}</td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"4\" align=\"right\">{lng p=\"vat\"} {$mwstsatz}%:</td>\n\n                        <td>{$mwst}</td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"4\" align=\"right\">{lng p=\"gb\"} ({lng p=\"gross\"}):</td>\n                        <td>{$brutto}</td>\n                    </tr>\n                </tbody>\n\n            </table>\n            </p>\n            <p>{$zahlungshinweis}<br></p>\n            <p>{lng p=\"kindregards\"}</p>\n            <p>{$service_title}</p>\n            <p>&nbsp;</p>\n            </td>\n\n        </tr>\n        <tr style=\"font-family: Arial;\">\n            <td colspan=\"2\"><hr style=\"height: 1px;\" color=\"#666666\" noshade=\"noshade\" width=\"100%\"></td>\n        </tr>\n        <tr style=\"font-family: Arial;\">\n            <td colspan=\"2\"><small>{lng p=\"invfooter\"}<br><br>{if $ktonr}<b>{lng p=\"bankacc\"}: </b>{lng p=\"kto_nr\"} {$ktonr} ({lng p=\"kto_inh\"} {$ktoinhaber}), {lng p=\"kto_blz\"} {$ktoblz} ({$ktoinstitut}){if $ktoiban}, {lng p=\"kto_iban\"} {$ktoiban}, {lng p=\"kto_bic\"} {$ktobic}{/if}{/if}<br></small></td>\n        </tr>\n\n    </tbody>\n</table>\n\n';

// step?
if(!isset($_REQUEST['step']))
	$step = STEP_SELECT_LANGUAGE;
else
	$step = (int)$_REQUEST['step'];

// read language file
ReadLanguage();

// header
if($step != STEP_UPDATE_STEP)
	pageHeader();

/**
 * select language
 */
if($step == STEP_SELECT_LANGUAGE)
{
	$nextStep = STEP_WELCOME;
	?>
	<h1><?php echo($lang_setup['selectlanguage']); ?></h1>

	<?php echo($lang_setup['selectlanguage_text']); ?>

	<blockquote>
		<input type="radio" id="lang_deutsch" name="lng" value="deutsch"<?php if($lang=='deutsch') echo ' checked="checked"'; ?> />
			<label for="lang_deutsch">Deutsch</label>
		<br />
		<input type="radio" id="lang_english" name="lng" value="english"<?php if($lang=='english') echo ' checked="checked"'; ?> />
			<label for="lang_english">English</label>
	</blockquote>
	<?php
}

/**
 * welcome / license
 */
else if($step == STEP_WELCOME)
{
	$nextStep = STEP_SYSTEMCHECK;
	?>
	<h1><?php echo($lang_setup['welcome']); ?></h1>

	<?php echo($lang_setup['welcome_text']); ?>

	<br /><br />
	<?php echo($lang_setup['license_text']); ?>

	<br /><br />
	<textarea id="license" readonly="readonly"><?php echo($lang_setup['license']); ?></textarea>

	<blockquote>
		<input type="radio" id="license_accept" name="license" value="1" onclick="document.getElementById('next_button').disabled=false;" />
			<label for="license_accept"><?php echo($lang_setup['license_accept']); ?></label>
		<br />
		<input type="radio" id="license_deny" name="license" value="0" onclick="document.getElementById('next_button').disabled=true;" />
			<label for="license_deny"><?php echo($lang_setup['license_deny']); ?></label>
	</blockquote>
	<?php
}

/**
 * system check
 */
else if($step == STEP_SYSTEMCHECK)
{
	$nextStep = STEP_SERIAL;
	?>
	<h1><?php echo($lang_setup['syscheck']); ?></h1>

	<?php echo($lang_setup['syscheck_text']); ?>

	<br /><br />
	<table class="list">
		<tr>
			<th width="180">&nbsp;</th>
			<th><?php echo($lang_setup['required']); ?></th>
			<th><?php echo($lang_setup['available']); ?></th>
			<th width="60">&nbsp;</th>
		</tr>
		<tr>
			<th><?php echo($lang_setup['phpversion']); ?></th>
			<td>5.3.0</td>
			<td><?php echo(phpversion()); ?></td>
			<td><img src="../admin/templates/images/<?php if((int)str_replace('.', '', phpversion()) >= 530) echo 'ok'; else { echo 'error'; $nextStep = STEP_SYSTEMCHECK; } ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['mysqlext']); ?></th>
			<td><?php echo($lang_setup['yes']); ?></td>
			<td><?php echo(function_exists('mysqli_connect') ? $lang_setup['yes'] : $lang_setup['no']); ?></td>
			<td><img src="../admin/templates/images/<?php if(function_exists('mysqli_connect')) echo 'ok'; else { echo 'error'; $nextStep = STEP_SYSTEMCHECK; } ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<?php
		$chmodCommands = array();
		foreach($writeableFiles as $file)
		{
			if(!is_writeable('../' . $file))
			{
				$chmodMode = is_dir('../' . $file) ? '0777' : '0666';
				if(!isset($chmodCommands[$chmodMode])) $chmodCommands[$chmodMode] = array();
				$chmodCommands[$chmodMode][] = $file;
			}
			?>
		<tr>
			<th><?php echo($file); ?></th>
			<td><?php echo($lang_setup['writeable']); ?></td>
			<td><?php echo(is_writeable('../' . $file) ? $lang_setup['writeable'] : $lang_setup['notwriteable']); ?></td>
			<td><img src="../admin/templates/images/<?php if(is_writeable('../' . $file)) echo 'ok'; else { echo 'error'; $nextStep = STEP_SYSTEMCHECK; } ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
			<?php
		}
		?>
	</table>

	<?php
	if($nextStep != STEP_SERIAL)
	{
		?>
	<br />
	<div style="text-align:center;display:;" id="chmodCommandsNote">
		<a href="#" onclick="document.getElementById('chmodCommandsNote').style.display='none';document.getElementById('chmodCommands').style.display='';"><?php echo $lang_setup['showchmod']; ?></a>
	</div>
	<div style="display:none;" id="chmodCommands">
		<textarea readonly="readonly" class="installLog" style="height:120px;"><?php
			foreach($chmodCommands as $mode => $files)
			{
				echo 'chmod ' . $mode;
				foreach($files as $file)
				{
					echo " \\\n\t" . $file;
				}
				echo "\n";
			}
		?></textarea>
	</div>
		<?php
	}
	?>

	<br />
	<?php echo($nextStep == STEP_SERIAL ? $lang_setup['checkok_text'] : $lang_setup['checkfail_text']);?>
	<?php
}

//
//
//				FRESH INSTALL
//
//

/**
 * serial number
 */
else if($step == STEP_SERIAL)
{
	$nextStep = STEP_MYSQL;
	?>
	<h1><?php echo($lang_setup['licensing']); ?></h1>

	<?php echo($lang_setup['licensing_text']); ?>

	<blockquote>
		<label for="license_nr"><?php echo($lang_setup['license_nr']); ?></label><br />
			<input id="license_nr" type="text" readonly="readonly" disabled="disabled" value="SL300919" size="32" />
		<br /><br />
		<label for="serial_1"><?php echo($lang_setup['serial']); ?></label><br />
			<input id="serial_1" type="text" name="serial_1" size="5" maxlength="4" onkeyup="SerialNextField(1,event)" />
				-
			<input id="serial_2" type="text" name="serial_2" size="5" maxlength="4" onkeypress="SerialPrevField(2,event)" onkeyup="SerialNextField(2,event)" />
				-
			<input id="serial_3" type="text" name="serial_3" size="5" maxlength="4" onkeypress="SerialPrevField(3,event)" onkeyup="SerialNextField(3,event)" />
				-
			<input id="serial_4" type="text" name="serial_4" size="5" maxlength="4" onkeypress="SerialPrevField(4,event)" onkeyup="SerialNextField(4,event)" />
	</blockquote>

	<input type="hidden" name="type" value="fresh" />

	<script language="javascript">
	<!--
		function SerialPrevField(cur, event)
		{
			var curField = document.getElementById('serial_'+cur);

			curField.value = curField.value.toUpperCase();

			if(event && event.keyCode == 8 && curField.value.length == 0
			   && cur > 1)
			{
			    document.getElementById('serial_'+(cur-1)).focus();
			}
		}

	    function SerialNextField(cur, event)
	    {
			var curField = document.getElementById('serial_'+cur);

			curField.value = curField.value.toUpperCase();

			if(cur < 4 && curField.value.length == 4)
			{
				if(event && (event.keyCode < 48 || event.keyCode > 122))
					return;
			    document.getElementById('serial_'+(cur+1)).focus();
			}
	    }
	//-->
	</script>
	<?php
}

/**
 * mysql login
 */
else if($step == STEP_MYSQL)
{
	$break = false;

	/**
	 * IMPORTANT INFORMATION ABOUT B1GMAIL LICENSING
	 *
	 * We do know that it is easy to circumvent the following licensing checks.
	 * It is easy to remove the whole check or to write key-generators for
	 * our serial numbers.
	 *
	 * It is that easy because we do not believe in encrypting or compiling our
	 * PHP source code because we want to give our customers as much flexibility
	 * as possible and because we want to make enhancements like hacks and plugins
	 * possible.
	 *
	 * We trust in you to be honest and to buy a license if you use b1gMail.
	 * By doing so, you are not only ensuring the further development of b1gMail.
	 * By owning a valid license, you get many benefits, like free E-Mail support,
	 * access to the latest extensions, updates, patches and important security
	 * updates and the ability to have influence in new releases.
	 *
	 * Thanks for your support and for making b1gMail possible!
	 *
	 * The b1gMail team
	 */
	if(isset($_REQUEST['serial']))
		$serial = explode('-', $_REQUEST['serial']);
	else
		$serial = array(strtoupper($_REQUEST['serial_1']),
						strtoupper($_REQUEST['serial_2']),
						strtoupper($_REQUEST['serial_3']),
						strtoupper($_REQUEST['serial_4']));
	$check = strtoupper(md5($serial[1] . $serial[0] . $serial[2]));
	if(strlen(implode('-', $serial)) != 19
		|| $serial[3][0] != $check[5]
		|| $serial[3][1] != $check[23]
		|| $serial[3][2] != $check[8]
		|| $serial[3][3] != $check[31])
	{
		$nextStep = STEP_SERIAL;
		?>
		<h1><?php echo($lang_setup['licensing']); ?></h1>

		<?php echo($lang_setup['wrongserial']); ?>
		<?php
		$break = true;
	}
	else
	{
		$serial = implode('-', $serial);
	}

	if(!$break)
	{
		$nextStep = STEP_CHECK_MYSQL;
		?>
		<h1><?php echo($lang_setup['db']); ?></h1>

		<?php echo($lang_setup['dbfresh_text']); ?>

		<blockquote>
			<label for="mysql_host"><?php echo($lang_setup['mysql_host']); ?></label><br />
				<input id="mysql_host" name="mysql_host" type="text" value="" size="32" />
			<br /><br />
			<label for="mysql_user"><?php echo($lang_setup['mysql_user']); ?></label><br />
				<input id="mysql_user" name="mysql_user" type="text" value="" size="32" />
			<br /><br />
			<label for="mysql_pass"><?php echo($lang_setup['mysql_pass']); ?></label><br />
				<input id="mysql_pass" name="mysql_pass" type="text" value="" size="32" />
			<br /><br />
			<label for="mysql_db"><?php echo($lang_setup['mysql_db']); ?></label><br />
				<input id="mysql_db" name="mysql_db" type="text" value="" size="32" />
		</blockquote>

		<input type="hidden" name="serial" value="<?php echo($serial); ?>" />
		<?php
	}
}

/**
 * check mysql login
 */
else if($step == STEP_CHECK_MYSQL)
{
	if($connection = CheckMySQLLogin($_REQUEST['mysql_host'], $_REQUEST['mysql_user'], $_REQUEST['mysql_pass'],
						$_REQUEST['mysql_db']))
	{
		// b1gMail already installed here?
		$b1gMailInDB = false;
		$res = mysqli_query($connection, 'SHOW TABLES');
		while($row = mysqli_fetch_array($res, MYSQLI_NUM))
			if($row[0] == 'bm60_prefs')
			{
				$b1gMailInDB = true;
				break;
			}
		mysqli_free_result($res);
		mysqli_close($connection);

		if($b1gMailInDB)
		{
			$nextStep = STEP_MYSQL;
			?>
			<h1><?php echo($lang_setup['db']); ?></h1>

			<?php echo($lang_setup['dbexists_text']); ?>
			<?php
		}
		else
		{
			$nextStep = STEP_CHECK_EMAIL;
			?>
			<h1><?php echo($lang_setup['emailcfg']); ?></h1>

			<?php echo($lang_setup['emailcfg_text']); ?>

			<br /><br />
			<fieldset>
				<legend><?php echo($lang_setup['setupmode']); ?></legend>

				<?php echo($lang_setup['mode_note']); ?><br /><br />

				<input type="radio" id="setup_mode_public" name="setup_mode" value="public" checked="checked" />
				<label for="setup_mode_public"><?php echo($lang_setup['mode_public']); ?></label><br />
				<blockquote>
					<?php echo($lang_setup['mode_public_desc']); ?><br />
				</blockquote>

				<input type="radio" id="setup_mode_private" name="setup_mode" value="private" />
				<label for="setup_mode_private"><?php echo($lang_setup['mode_private']); ?></label>
				<blockquote>
					<?php echo($lang_setup['mode_private_desc']); ?><br />
				</blockquote>
			</fieldset>

			<br />
			<fieldset>
				<legend><?php echo($lang_setup['receiving']); ?></legend>

				<input type="radio" id="receive_method_pop3" name="receive_method" value="pop3" checked="checked" />
				<label for="receive_method_pop3"><?php echo($lang_setup['pop3gateway']); ?></label>
				<blockquote>
					<label for="pop3_host"><?php echo($lang_setup['pop3_host']); ?></label><br />
						<input id="pop3_host" name="pop3_host" type="text" value="" size="32" />
					<br /><br />
					<label for="pop3_user"><?php echo($lang_setup['pop3_user']); ?></label><br />
						<input id="pop3_user" name="pop3_user" type="text" value="" size="32" />
					<br /><br />
					<label for="pop3_pass"><?php echo($lang_setup['pop3_pass']); ?></label><br />
						<input id="pop3_pass" name="pop3_pass" type="text" value="" size="32" />
				</blockquote>

				<input type="radio" id="receive_method_pipe" name="receive_method" value="pipe" />
				<label for="receive_method_pipe"><?php echo($lang_setup['pipe']); ?></label>
			</fieldset>

			<br />
			<fieldset>
				<legend><?php echo($lang_setup['sending']); ?></legend>

				<input type="radio" id="send_method_phpmail" name="send_method" value="php" checked="checked" />
				<label for="send_method_phpmail"><?php echo($lang_setup['phpmail']); ?></label><br />

				<input type="radio" id="send_method_smtp" name="send_method" value="smtp" />
				<label for="send_method_smtp"><?php echo($lang_setup['smtp']); ?></label>
				<blockquote>
					<label for="smtp_host"><?php echo($lang_setup['smtp_host']); ?></label><br />
						<input id="smtp_host" name="smtp_host" type="text" value="localhost" size="32" />
				</blockquote>

				<input type="radio" id="send_method_sendmail" name="send_method" value="sendmail" />
				<label for="send_method_sendmail"><?php echo($lang_setup['sendmail']); ?></label>
				<blockquote>
					<label for="sendmail_path"><?php echo($lang_setup['sendmail_path']); ?></label><br />
						<input id="sendmail_path" name="sendmail_path" type="text" value="/usr/sbin/sendmail" size="32" />
				</blockquote>
			</fieldset>

			<input type="hidden" name="mysql_host" value="<?php echo(htmlentities($_REQUEST['mysql_host'])); ?>" />
			<input type="hidden" name="mysql_user" value="<?php echo(htmlentities($_REQUEST['mysql_user'])); ?>" />
			<input type="hidden" name="mysql_pass" value="<?php echo(htmlentities($_REQUEST['mysql_pass'])); ?>" />
			<input type="hidden" name="mysql_db" value="<?php echo(htmlentities($_REQUEST['mysql_db'])); ?>" />
			<?php
		}
	}
	else
	{
		$nextStep = STEP_MYSQL;
		?>
		<h1><?php echo($lang_setup['db']); ?></h1>

		<?php echo($lang_setup['dbfail_text']); ?>
		<?php
	}

	?>
	<input type="hidden" name="serial" value="<?php echo($_REQUEST['serial']); ?>" />
	<?php
}

/**
 * check email config
 */
else if($step == STEP_CHECK_EMAIL)
{
	if($_REQUEST['receive_method'] != 'pop3'
		|| CheckPOP3Login($_REQUEST['pop3_host'], $_REQUEST['pop3_user'], $_REQUEST['pop3_pass']))
	{
		if($_REQUEST['send_method'] != 'sendmail'
			|| (file_exists($_REQUEST['sendmail_path']) && is_executable($_REQUEST['sendmail_path'])))
		{
			$nextStep = STEP_INSTALL;
			?>
			<h1><?php echo($lang_setup['misc']); ?></h1>

			<?php echo($lang_setup['misc_text']); ?>

			<blockquote>
				<label for="adminpw"><?php echo($lang_setup['adminpw']); ?></label><br />
					<input id="adminpw" name="adminpw" type="text" value="<?php echo(GeneratePW()); ?>" size="32" />
				<br /><br />
				<label for="domains"><?php echo($lang_setup['domains']); ?></label><br />
					<textarea id="domains" name="domains" style="width:220px;height:80px;">example.com
example.net
example.org</textarea>
				<br /><br />
				<label for="url"><?php echo($lang_setup['url']); ?></label><br />
					<input id="url" name="url" type="text" value="http://<?php echo($_SERVER['HTTP_HOST'] . preg_replace('/\/setup\/index\.php(.*)/', '/', $_SERVER['REQUEST_URI'])); ?>" size="32" />
			</blockquote>

			<input type="hidden" name="setup_mode" value="<?php echo(htmlentities($_REQUEST['setup_mode'])); ?>" />
			<input type="hidden" name="receive_method" value="<?php echo(htmlentities($_REQUEST['receive_method'])); ?>" />
			<input type="hidden" name="send_method" value="<?php echo(htmlentities($_REQUEST['send_method'])); ?>" />
			<input type="hidden" name="pop3_host" value="<?php echo(htmlentities($_REQUEST['pop3_host'])); ?>" />
			<input type="hidden" name="pop3_user" value="<?php echo(htmlentities($_REQUEST['pop3_user'])); ?>" />
			<input type="hidden" name="pop3_pass" value="<?php echo(htmlentities($_REQUEST['pop3_pass'])); ?>" />
			<input type="hidden" name="smtp_host" value="<?php echo(htmlentities($_REQUEST['smtp_host'])); ?>" />
			<input type="hidden" name="sendmail_path" value="<?php echo(htmlentities($_REQUEST['sendmail_path'])); ?>" />
			<?php
		}
		else
		{
			$nextStep = STEP_CHECK_MYSQL;
			?>
			<h1><?php echo($lang_setup['emailcfg']); ?></h1>

			<?php echo($lang_setup['emailcfgsmfail_text']); ?>
			<?php
		}
	}
	else
	{
		$nextStep = STEP_CHECK_MYSQL;
		?>
		<h1><?php echo($lang_setup['emailcfg']); ?></h1>

		<?php echo($lang_setup['emailcfgpop3fail_text']); ?>
		<?php
	}

	?>
	<input type="hidden" name="mysql_host" value="<?php echo(htmlentities($_REQUEST['mysql_host'])); ?>" />
	<input type="hidden" name="mysql_user" value="<?php echo(htmlentities($_REQUEST['mysql_user'])); ?>" />
	<input type="hidden" name="mysql_pass" value="<?php echo(htmlentities($_REQUEST['mysql_pass'])); ?>" />
	<input type="hidden" name="mysql_db" value="<?php echo(htmlentities($_REQUEST['mysql_db'])); ?>" />
	<input type="hidden" name="serial" value="<?php echo($_REQUEST['serial']); ?>" />
	<?php
}

/**
 * install!
 */
else if($step == STEP_INSTALL)
{
	include('../serverlib/database.struct.php');
	include('./data/rootcerts.data.php');

	// prepare structure
	$databaseStructure = unserialize(base64_decode($databaseStructure));

	// sanitize input
	if(substr($_REQUEST['url'], -1) != '/')
		$_REQUEST['url'] .= '/';
	$domains = explode("\n", str_replace(array("\r", '@', ',', ';', ':', ' '), '', $_REQUEST['domains']));
	foreach($domains as $key=>$val)
	{
		$val = trim($val);
		if(strlen($val) < 2)
			unset($domains[$key]);
		else
			$domains[$key] = $val;
	}
	$domains = count($domains) > 0 ? $domains : array('example.com');
	list($firstDomain) = $domains;

	// start installation log
	ob_start();

	// status
	$dbStructResult 	= 'error';
	$defaultConfigResut	= 'error';
	$adminAccountResult	= 'error';
	$defaultGroupResut 	= 'error';
	$exampleDataResult 	= 'error';
	$postmasterResut 	= 'error';
	$configResult 		= 'error';

	// install
	$connection = mysqli_connect($_REQUEST['mysql_host'], $_REQUEST['mysql_user'], $_REQUEST['mysql_pass'], $_REQUEST['mysql_db']);
	if($connection)
	{
		if(mysqli_select_db($connection, $_REQUEST['mysql_db']))
		{
			// install routine uses iso-8859-1 encoding
			mysqli_set_charset($connection, 'latin1');

			// disable strict mode
			@mysqli_query($connection, 'SET SESSION sql_mode=\'\'');

			// get MySQL version
			$result = mysqli_query($connection, 'SELECT VERSION()');
			list($mysqlVersion) = mysqli_fetch_array($result, MYSQLI_NUM);
			mysqli_free_result($result);

			// setup mode?
			$setupMode = $_REQUEST['setup_mode'];
			if(!in_array($setupMode, array('public', 'private')))
				$setupMode = 'public';

			// install in utf-8 mode?
			$utf8Mode = (function_exists('mb_convert_encoding') || function_exists('iconv'))
							&& in_array(CompareVersions($mysqlVersion, '4.1.1'), array(VERSION_IS_NEWER, VERSION_IS_EQUAL));

			// create db structure
			$dbStructResult = 'ok';
			$result = CreateDatabaseStructure($connection, $databaseStructure, $utf8Mode, $_REQUEST['mysql_db']);
			foreach($result as $query=>$queryResult)
				if($queryResult !== true)
				{
					$dbStructResult = 'warning';
					echo 'Failed to execute structure query: ' . $queryResult . "\n";
				}

			// create default config
			$blobDBSupport = class_exists('SQLite3') || (class_exists('PDO') && in_array('sqlite', PDO::getAvailableDrivers()));
			$gzSupport = function_exists('gzcompress') && function_exists('gzuncompress');
			$hostName = @gethostbyname($_SERVER['SERVER_ADDR']);
			if(!$hostName || trim($hostName) == '' || $hostName == $_SERVER['SERVER_ADDR'])
				$hostName = $_SERVER['HTTP_HOST'];
    		$dataFolder = preg_replace('/\/setup\/index\.php(.*)/', '/data/', str_replace('\\', '/', $_SERVER['SCRIPT_FILENAME']));
			$selfFolder = preg_replace('/\/setup\/index\.php(.*)/', '/', str_replace('\\', '/', $_SERVER['SCRIPT_FILENAME']));
			$prefsQuery = sprintf('INSERT INTO bm60_prefs(template,language,serial,selfurl,mobile_url,send_method,smtp_host,sendmail_path,receive_method,pop3_host,pop3_user,pop3_pass,passmail_abs,titel,datafolder,selffolder,b1gmta_host,dnsbl,signup_dnsbl,smsreply_abs,widget_order_start,widget_order_organizer,structstorage,search_in,db_is_utf8,rgtemplate,pay_emailfrom,pay_emailfromemail,regenabled,contactform_to,ap_autolock_notify_to,blobstorage_provider,blobstorage_provider_webdisk,blobstorage_compress,blobstorage_webdisk_compress) '
							. 'VALUES(\'modern\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,%d,\'%s\',\'%s\')',
							$_REQUEST['lng'] == 'deutsch' ? 'deutsch' : 'english',
							SQLEscape($_REQUEST['serial'], $connection),
							SQLEscape($_REQUEST['url'], $connection),
							SQLEscape($_REQUEST['url'].'m/', $connection),
							SQLEscape($_REQUEST['send_method'], $connection),
							SQLEscape($_REQUEST['smtp_host'], $connection),
							SQLEscape($_REQUEST['sendmail_path'], $connection),
							SQLEscape($_REQUEST['receive_method'], $connection),
							SQLEscape($_REQUEST['pop3_host'], $connection),
							SQLEscape($_REQUEST['pop3_user'], $connection),
							SQLEscape($_REQUEST['pop3_pass'], $connection),
							SQLEscape('"Postmaster ' . $firstDomain . '" <postmaster@' . EncodeDomain($firstDomain) . '>', $connection),
							SQLEscape($firstDomain . ' Mail', $connection),
							SQLEscape($dataFolder, $connection),
							SQLEscape($selfFolder, $connection),
							$hostName,
							'ix.dnsbl.manitu.net:zen.spamhaus.org',
							'dnsbl.tornevall.org',
							SQLEscape('postmaster@' . EncodeDomain($firstDomain), $connection),
							'BMPlugin_Widget_Welcome,BMPlugin_Widget_EMail,BMPlugin_Widget_Websearch;BMPlugin_Widget_Mailspace,,BMPlugin_Widget_Quicklinks;BMPlugin_Widget_Webdiskspace,,',
							'BMPlugin_Widget_Websearch,BMPlugin_Widget_Calendar,BMPlugin_Widget_Notes;,BMPlugin_Widget_Tasks,',
							!ini_get('safe_mode') ? 'yes' : 'no',
							SQLEscape('a:8:{s:5:"mails";s:2:"on";s:11:"attachments";s:2:"on";s:3:"sms";s:2:"on";s:8:"calendar";s:2:"on";s:5:"tasks";s:2:"on";s:11:"addressbook";s:2:"on";s:5:"notes";s:2:"on";s:7:"webdisk";s:2:"on";}', $connection),
							$utf8Mode ? 1 : 0,
                            $defaultInvoice,
                            SQLEscape($lang_setup['accounting'], $connection),
							SQLEscape('postmaster@' . EncodeDomain($firstDomain), $connection),
							$setupMode == 'public' ? 'yes' : 'no',
							SQLEscape('postmaster@' . EncodeDomain($firstDomain), $connection),
							SQLEscape('postmaster@' . EncodeDomain($firstDomain), $connection),
							$blobDBSupport ? 1 : 0,
							$blobDBSupport ? 1 : 0,
							$gzSupport ? 'yes' : 'no',
							$gzSupport ? 'yes' : 'no');
			if(mysqli_query($connection, $prefsQuery))
				$defaultConfigResut = 'ok';
			else
			{
				echo 'Failed to create default config: ' . mysqli_error($connection) .  "\n";
			}

			// create admin account
			$adminSalt = GeneratePW();
			$adminPW = md5($_REQUEST['adminpw'] . $adminSalt);
			if(mysqli_query($connection, sprintf('REPLACE INTO bm60_admins(`adminid`,`username`,`firstname`,`lastname`,`password`,`password_salt`,`type`,`notes`) VALUES '
							. '(1,\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',0,\'\')',
							'admin',
							'Super',
							'Administrator',
							$adminPW,
							SQLEscape($adminSalt, $connection))))
				$adminAccountResult = 'ok';
			else
			{
				echo 'Failed to create admin account: ' . mysqli_error($connection) .  "\n";
			}

			// create default group
			$groupQuery = sprintf('INSERT INTO bm60_gruppen(id,titel,ftsearch) VALUES(1,\'%s\',\'%s\')',
				SQLEscape($lang_setup['defaultgroup'], $connection),
				$blobDBSupport ? 'yes' : 'no');
			if(mysqli_query($connection, $groupQuery))
				$defaultGroupResut = 'ok';
			else
			{
				echo 'Failed to create default group: ' . mysqli_error($connection) .  "\n";
			}

			// create domains
			$domainPos = 0;
			foreach($domains as $domain)
			{
				$domainQuery = sprintf('INSERT INTO bm60_domains(`domain`,`pos`) VALUES(\'%s\',%d)',
					SQLEscape(EncodeDomain($domain), $connection),
					$domainPos += 10);
				mysqli_query($connection, $domainQuery);
			}

			// create postmaster
			$salt = GeneratePW();
			$postmasterQuery = sprintf('INSERT INTO bm60_users(id,email,vorname,nachname,passwort,passwort_salt,gruppe,preview,plaintext_courier,soforthtml) '
								. 'VALUES(1,\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',1,\'yes\',\'yes\',\'yes\')',
								SQLEscape('postmaster@' . EncodeDomain($firstDomain), $connection),
								'Postmaster',
								SQLEscape($firstDomain, $connection),
								md5(md5($_REQUEST['adminpw']).$salt),
								SQLEscape($salt, $connection));
			if(mysqli_query($connection, $postmasterQuery))
				$postmasterResut = 'ok';
			else
			{
				echo 'Failed to create postmaster: ' . mysqli_error($connection) .  "\n";
			}

			// create postmaster aliases
			foreach($domains as $domain)
				if($domain != $firstDomain)
				{
					$aliasQuery = sprintf('INSERT INTO bm60_aliase(email,user) VALUES(\'%s\',1)',
						SQLEscape('postmaster@' . EncodeDomain($domain), $connection));
					mysqli_query($connection, $aliasQuery);
				}

			// install default data
			$exampleDataResult = 'ok';
			foreach($exampleData as $query)
				if(!mysqli_query($connection, $query))
				{
					echo 'Failed to execute example data query: ' . mysqli_error($connection) . "\n";
					$exampleDataResult = 'warning';
				}

			// install default root certs
			foreach($rootCertsData as $query)
				if(!mysqli_query($connection, $query))
				{
					echo 'Failed to execute root cert insert query: ' . mysqli_error($connection) . "\n";
					$exampleDataResult = 'warning';
				}

			// remove outdated root certificates
			mysqli_query($connection, 'DELETE FROM bm60_certificates WHERE `type`=0 AND `userid`=0 AND `validto`<'.time());

			// install template prefs
			if($setupMode == 'private')
			{
				mysqli_query($connection, 'INSERT INTO bm60_templateprefs(`template`,`key`,`value`) VALUES(\'modern\',\'hideSignup\',\'1\')');
			}

			// convert language files?
			if($utf8Mode)
			{
				$langFiles = array();
				$d = dir('../languages/');
				while($entry = $d->read())
				{
					if($entry == '.' || $entry == '..')
						continue;

					if(substr($entry, -9) == '.lang.php')
						$langFiles[] = '../languages/' . $entry;
				}

				foreach($langFiles as $file)
				{
					$info = GetLanguageInfo($file);
					if(!isset($info['charset']))
						continue;

					$charset = strtolower($info['charset']);
					if($charset == 'utf8' || $charset == 'utf-8')
						continue;

					// read file contents
					$fp = @fopen($file, 'rb+');
					if(!$fp || !is_resource($fp))
					{
						echo 'Failed to convert language file to UTF-8: ' . $file . "\n";
						continue;
					}
					$contents = fread($fp, filesize($file));

					// convert contents to utf-8
					$contents = ConvertEncoding($contents, $charset, 'UTF-8');

					// manipulate locales
					$locales = array();
					$oldLocales = explode('|', $info['locale']);
					foreach($oldLocales as $locale)
					{
						$locale = preg_replace('/\..*/i', '.UTF-8', $locale);
						if(!in_array($locale, $locales))
							$locales[] = $locale;
					}

					// manipulate lang def line
					$newLangDef = sprintf('// b1gMailLang::%s::%s::%s::%s::UTF-8::%s',
						$info['title'],
						$info['author'],
						$info['authorMail'],
						$info['authorWeb'],
						implode('|', $locales));
					$contents = str_replace($info['langDefLine'], $newLangDef . "\n" . '// Converted to UTF-8 by setup/index.php at ' . date('r'), $contents);

					// save
					fseek($fp, 0, SEEK_SET);
					ftruncate($fp, 0);
					fwrite($fp, $contents);
					fclose($fp);
				}
			}

			// create config file
			$configFile = sprintf("<?php\n// Generated %s\n\$mysql = array(\n\t'host'\t\t=> '%s',\n\t'user'\t\t=> '%s',\n\t'pass'\t\t=> '%s',\n\t'db'\t\t=> '%s',\n\t'prefix'\t=> '%s'\n);\n?>",
				date('r'),
				addslashes($_REQUEST['mysql_host']),
				addslashes($_REQUEST['mysql_user']),
				addslashes($_REQUEST['mysql_pass']),
				addslashes($_REQUEST['mysql_db']),
				'bm60_');
			$fp = fopen('../serverlib/config.inc.php', 'w');
			if($fp)
			{
				fwrite($fp, $configFile);
				fclose($fp);

				$configResult = 'ok';
			}
			else
				echo 'Failed to open config.inc.php for writing' . "\n";
			@mail('v7-install@b1g.de', 'b1gMail 7.4 Installation', sprintf('Host: %s; URI: %s; Serial: %s; License no: %s', $_SERVER['HTTP_HOST'], preg_replace('/\/setup\/index\.php(.*)/', '/', $_SERVER['REQUEST_URI']), $_REQUEST['serial'], 'SL300919'));
		}
		else
			echo 'MySQL database selection failed' . "\n";
	}
	else
		echo 'MySQL connection failed' . "\n";

	// finish installation log
	$installLog = trim(strip_tags(ob_get_contents()));
	ob_end_clean();
	?>
	<h1><?php echo($lang_setup['installing']); ?></h1>

	<?php echo($lang_setup['installing_text']); ?>

	<br /><br />
	<table class="list">
		<tr>
			<th><?php echo(sprintf($lang_setup['inst_dbstruct'], $databaseStructureVersion)); ?></th>
			<td><img src="../admin/templates/images/<?php echo($dbStructResult); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['inst_defaultcfg']); ?></th>
			<td><img src="../admin/templates/images/<?php echo($defaultConfigResut); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['inst_admin']); ?></th>
			<td><img src="../admin/templates/images/<?php echo($adminAccountResult); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['inst_defaultgroup']); ?></th>
			<td><img src="../admin/templates/images/<?php echo($defaultGroupResut); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['inst_postmaster']); ?></th>
			<td><img src="../admin/templates/images/<?php echo($postmasterResut); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['inst_exdata']); ?></th>
			<td><img src="../admin/templates/images/<?php echo($exampleDataResult); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<th><?php echo($lang_setup['inst_config']); ?></th>
			<td><img src="../admin/templates/images/<?php echo($configResult); ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
	</table>
	<br />

	<?php
	if($installLog != '')
	{
		echo $lang_setup['log_text'];
		?>
		<textarea readonly="readonly" class="installLog"><?php echo(htmlentities($installLog)); ?></textarea>
		<br /><br />
		<?php
	}

	echo $lang_setup['finished_text'];
	?>
	<blockquote>
		<b><?php echo($lang_setup['userlogin']); ?></b><br />
		<a target="_blank" href="<?php echo($_REQUEST['url']); ?>"><?php echo($_REQUEST['url']); ?></a><br /><br />

		<b><?php echo($lang_setup['adminlogin']); ?></b><br />
		<a target="_blank" href="<?php echo($_REQUEST['url']); ?>admin/"><?php echo($_REQUEST['url']); ?>admin/</a><br /><br />

		<b><?php echo($lang_setup['adminuser']); ?></b><br />
		admin<br /><br />

		<b><?php echo($lang_setup['adminpw']); ?></b><br />
		<?php echo(htmlentities($_REQUEST['adminpw'])); ?>
	</blockquote>
	<?php
}

// footer
pageFooter();
?>